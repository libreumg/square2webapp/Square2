package square2.rest.client;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import square2.modules.model.ProfileBean;

/**
 * 
 * @author henkej
 *
 */
public class TestAdminRestClient {
  /**
   * test
   */
  @Disabled
  @Test
  public void testGetProfile() {
    AdminRestClient arc = new AdminRestClient("http://127.0.0.1:5762");
    ProfileBean bean = arc.getProfile("henkej");
    assertEquals("Jörg", bean.getForename());
    assertEquals("Henke", bean.getSurname());
  }
}
