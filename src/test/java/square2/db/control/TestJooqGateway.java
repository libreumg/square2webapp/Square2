package square2.db.control;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jooq.JSON;
import org.junit.jupiter.api.Test;

import square2.modules.model.AclBean;
import square2.modules.model.ProfileBean;

/**
 * 
 * @author henkej
 *
 */
public class TestJooqGateway {
	/**
	 * test convertJsonToProfileBeans
	 */
	@Test
	public void testConvertJsonToProfileBeans() {
		ProfileBean u123 = new ProfileBean();
		u123.setForename("user 123");
		u123.setSurname("surname of 123");
		u123.setUsername("theuser123");
		u123.setUsnr(123);

		ProfileBean u456 = new ProfileBean();
		u456.setForename("user 456");
		u456.setSurname("surname of 456");
		u456.setUsername("theuser456");
		u456.setUsnr(456);

		ProfileBean u789 = new ProfileBean();
		u789.setForename("user 789");
		u789.setSurname("surname of 789");
		u789.setUsername("theuser789");
		u789.setUsnr(789);

		Map<Integer, ProfileBean> map = new HashMap<>();
		map.put(123, u123);
		map.put(456, u456);
		map.put(789, u789);

		JSON json = JSON.valueOf("[123, 456, 789]");
		json.toString();
		JooqGateway gw = new JooqGateway(null) {
			@Override
			public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
				return null;
			}
		};
		gw.setCacheAllUsers(map);
		List<ProfileBean> profiles = gw.convertJsonToProfileBeans(json);

		assertTrue(profiles.size() == 3);
		assertEquals("theuser789", profiles.get(2).getUsername());
		assertEquals("theuser456", profiles.get(1).getUsername());
		assertEquals("theuser123", profiles.get(0).getUsername());
	}
}
