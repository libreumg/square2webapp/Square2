package square2.db.control.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestJSONBConverter {
  /**
   * test
   */
  @Test
  public void testValidateJSONB() {
    List<Exception> exceptions = new ArrayList<>();
    assertTrue(JSONBConverter.validateJSONB(exceptions, null));
    assertTrue(JSONBConverter.validateJSONB(exceptions, "[]"));
    assertTrue(JSONBConverter.validateJSONB(exceptions, "\"a valid string\""));
    assertTrue(JSONBConverter.validateJSONB(exceptions, "[\"a valid list entry\"]"));
    assertTrue(JSONBConverter.validateJSONB(exceptions, "[1, 2, 3]"));
    assertTrue(JSONBConverter.validateJSONB(exceptions, "{\"a valid\": \"map entry\"}"));
    assertTrue(JSONBConverter.validateJSONB(exceptions, "{\"one\": 1, \"two\": 2}"));
    assertEquals(0, exceptions.size());
    assertFalse(JSONBConverter.validateJSONB(exceptions, ""));
    assertEquals(1, exceptions.size());
    assertEquals("Unexpected token END OF FILE at position 0.", exceptions.get(0).getMessage());
    exceptions.clear();
    assertFalse(JSONBConverter.validateJSONB(exceptions, "invalid json"));
    assertEquals(1, exceptions.size());
    assertEquals("Unexpected character (i) at position 0.", exceptions.get(0).getMessage());
    exceptions.clear();
    assertFalse(JSONBConverter.validateJSONB(exceptions, "[invalid list entry]"));
    assertEquals(1, exceptions.size());
    assertEquals("Unexpected character (i) at position 1.", exceptions.get(0).getMessage());
    exceptions.clear();
    assertFalse(JSONBConverter.validateJSONB(exceptions, "{invalid map entry}"));
    assertEquals(1, exceptions.size());
    assertEquals("Unexpected character (i) at position 1.", exceptions.get(0).getMessage());
    exceptions.clear();
    assertFalse(JSONBConverter.validateJSONB(exceptions, "{\"error\": invalid map entry}"));
    assertEquals(1, exceptions.size());
    assertEquals("Unexpected character (i) at position 10.", exceptions.get(0).getMessage());
  }
}
