package square2.db.cache.vargroup;

import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.jooq.JSONB;
import org.junit.jupiter.api.Test;

import square2.modules.model.vargroup.VargroupElementBean;

/**
 * 
 * @author henkej
 *
 */
public class TestVargroupElementCache {
	private VargroupElementBean newBean(Integer keyElement, JSONB translation, Integer keyParentElement, Boolean chosen,
			Boolean isVariable) {
		VargroupElementBean bean = new VargroupElementBean(keyElement, translation, keyParentElement, chosen);
		bean.setIsVariable(isVariable);
		return bean;
	}

	/**
	 * test
	 */
	@Test
	public void testGetAmounts() {
		List<VargroupElementBean> content = new ArrayList<>();
		content.add(newBean(12, JSONB.valueOf("{\"en\": \"test\"}"), null, false, false));
		content.add(newBean(123, JSONB.valueOf("{\"en\": \"test\"}"), 12, false, false));
		content.add(newBean(124, JSONB.valueOf("{\"en\": \"test\"}"), 12, false, false));
		content.add(newBean(1234, JSONB.valueOf("{\"en\": \"test\"}"), 123, true, true));
		content.add(newBean(1235, JSONB.valueOf("{\"en\": \"test\"}"), 123, false, true));
		content.add(newBean(1246, JSONB.valueOf("{\"en\": \"test\"}"), 123, false, true));
		content.add(newBean(1247, JSONB.valueOf("{\"en\": \"test\"}"), 123, false, true));

		int[] amounts = new VargroupElementCache(content).getAmounts(12);
		assertTrue(amounts[0] == 2);
		assertTrue(amounts[1] == 4);
		assertTrue(amounts[2] == 1);
	}
}
