package square2.r.control.cmdbuilder;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import com.google.gson.JsonObject;

/**
 * 
 * @author henkej
 *
 */
public class TestRCommandBuilder {
	
	/**
	 * only temporary, remove
	 */
	@Test
	public void testJsonObjectAdd() {
		JsonObject o = new JsonObject();
		o.addProperty("x", "y");
		o.addProperty("x", "z");
		assertEquals("{\"x\":\"z\"}", o.toString());
		
		o = new JsonObject();
		// test if add("x", "y") and add("x", "z") results in {"x": "z"} and not in {"x": "y", "x": "z"}; same to addProperty
		JsonObject y = new JsonObject();
		y.addProperty("value", "y");
		JsonObject z = new JsonObject();
		z.addProperty("value", "z");
		o.add("x", y);
		o.add("x", z);
		assertEquals("{\"x\":{\"value\":\"z\"}}", o.toString());
	}
	
	/**
	 * test basic functionality
	 */
	@Test
	public void testToString() {
		RCommandBuilder cmd = new RCommandBuilder(true);
		List<Argument> args = new ArrayList<>();
		args.add(new StringArgument("key", "myKey"));
		args.add(new IntegerArgument("num", 42));
		args.add(new BooleanArgument("doso", false));
		cmd.appendFunction("call_get_key_value", args.toArray(new Argument[]{}));
		assertEquals("try(call_get_key_value(key = 'myKey', num = 42, doso = FALSE)\n)", cmd.toString());
		
		cmd = new RCommandBuilder(true);
		args = new ArrayList<>();
		args.add(new StringArgument("key", null, false));
		args.add(new IntegerArgument("num", null, false));
		args.add(new BooleanArgument("doso", null, false));
		cmd.appendFunction("call_get_key_value", args.toArray(new Argument[]{}));
		assertEquals("try(call_get_key_value()\n)", cmd.toString());
		
		cmd = new RCommandBuilder(true);
		args = new ArrayList<>();
		args.add(new StringArgument("reportconfig", "{}"));
		args.add(new StringArgument("format", "fmt"));
		args.add(new BooleanArgument("read_from_db", true));
		args.add(new BooleanArgument("return_raw", false));
		args.add(new BooleanArgument("debug", false));
		args.add(new BooleanArgument("show_errors", true));
		args.add(new BooleanArgument("admin", true));
		cmd.appendFunction("call_renderer", args.toArray(new Argument[] {}));
		assertEquals("try(call_renderer(reportconfig = '{}', format = 'fmt', read_from_db = TRUE, return_raw = FALSE, debug = FALSE, show_errors = TRUE, admin = TRUE)\n)", cmd.toString());
		
//		cmd = new RCommandBuilder(false);
//		assertEquals("q(save = FALSE)", cmd.toString());
//		
//		cmd = new RCommandBuilder(false);
//		args = new ArrayList<>();
//		args.add(new StringArgument("x", "y"));
//		cmd.appendFunction("c", args.toArray(new Argument[] {}));
//		assertEquals("c(x = 'y')\nq(save = FALSE)", cmd.toString());
	}
}
