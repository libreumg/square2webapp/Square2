package square2.help;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestHandsetRuleHelper {
	/**
	 * test
	 */
	@Test
	public void testGenerateMatrixColumnAcronym() {
		assertNull(HandsetRuleHelper.generateMatrixColumnAcronym(null));
		assertEquals("", HandsetRuleHelper.generateMatrixColumnAcronym(""));
		assertEquals(" 	 ", HandsetRuleHelper.generateMatrixColumnAcronym(" 	 "));
		assertEquals("A", HandsetRuleHelper.generateMatrixColumnAcronym("acc_"));
		assertEquals("AVarcomp", HandsetRuleHelper.generateMatrixColumnAcronym("acc_varcomp"));
		assertEquals("MMFGMisF", HandsetRuleHelper.generateMatrixColumnAcronym("com_my_funny_great_missing_function"));
		// TODO: add all the replacement rules from the issue https://gitlab.com/umg_hgw/sq2/square2/-/issues/910
	}
}
