package square2.help;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestLimitValidator {
	/**
	 * text regex pattern for validator
	 */
	@Test
	public void testRegexPattern() {
		Pattern pattern = Pattern.compile(LimitValidator.PATTERN);
		assertTrue(pattern.matcher(">0").matches());
		assertTrue(pattern.matcher(">1").matches());
		assertTrue(pattern.matcher(">1.1").matches());
		assertTrue(pattern.matcher(">1.12").matches());
		assertTrue(pattern.matcher(">12.12").matches());
		assertTrue(pattern.matcher(">-1.12").matches());
		assertTrue(pattern.matcher(">-11.12").matches());
		assertTrue(pattern.matcher(">=1.12").matches());
		assertTrue(pattern.matcher("<=1.12").matches());
		assertTrue(pattern.matcher("<=-1.12").matches());
		assertTrue(pattern.matcher("<0").matches());
		assertFalse(pattern.matcher("< 0").matches());
		assertFalse(pattern.matcher(" <0").matches());
		assertFalse(pattern.matcher("<0 ").matches());
		assertFalse(pattern.matcher("< 0 ").matches());
		assertFalse(pattern.matcher("0").matches());
		assertFalse(pattern.matcher("1.2").matches());
		assertFalse(pattern.matcher(">.0").matches());
	}
}
