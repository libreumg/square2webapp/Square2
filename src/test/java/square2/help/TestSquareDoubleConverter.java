package square2.help;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestSquareDoubleConverter {
	/**
	 * test
	 */
	@Test
	public void testGetAsObject() {
		assertNull(new SquareDoubleConverter().getAsObject(null, null, null));
		assertNull(new SquareDoubleConverter().getAsObject(null, null, ""));
		assertNull(new SquareDoubleConverter().getAsObject(null, null, "  "));
		assertEquals(Double.valueOf(1), new SquareDoubleConverter().getAsObject(null, null, "1"));
		assertEquals(Double.valueOf(1.2), new SquareDoubleConverter().getAsObject(null, null, "1,2"));
		assertEquals(Double.valueOf(1.2), new SquareDoubleConverter().getAsObject(null, null, "1.2"));
		assertEquals(Double.valueOf(1000.2), new SquareDoubleConverter().getAsObject(null, null, "1000.2"));
		assertEquals(Double.valueOf(1000.2), new SquareDoubleConverter().getAsObject(null, null, "1000,2"));
		assertEquals(Double.valueOf(1000.2), new SquareDoubleConverter().getAsObject(null, null, "1.000,2"));
	}
}
