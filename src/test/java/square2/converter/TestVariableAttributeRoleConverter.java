package square2.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import square2.modules.model.study.VariableAttributeRoleBean;

/**
 * 
 * @author henkej
 *
 */
public class TestVariableAttributeRoleConverter {

  /**
   * test
   */
  @Test
  public void testGetAsObject() {
    VariableAttributeRoleConverter converter = new VariableAttributeRoleConverter();
    assertNull(converter.getAsObject(null, null, null));
    assertNull(converter.getAsObject(null, null, ""));
    assertEquals(1, converter.getAsObject(null, null, "{\"pk\":1}").getPk());
    assertEquals("myName", converter.getAsObject(null, null, "{\"name\":\"myName\"}").getName());
    assertEquals("myWidget", converter.getAsObject(null, null, "{\"inputwidget\":\"myWidget\"}").getInputwidget());
    assertEquals("myRegEx", converter.getAsObject(null, null, "{\"regexpattern\":\"myRegEx\"}").getRegexpattern());
    assertFalse(converter.getAsObject(null, null, "{\"hasPrimaryAttribute\":false}").getHasPrimaryAttribute());
    assertTrue(converter.getAsObject(null, null, "{\"hasLocale\":true}").getHasLocale());
  }

  /**
   * test
   */
  @Test
  public void testGetAsString() {
    VariableAttributeRoleConverter converter = new VariableAttributeRoleConverter();
    VariableAttributeRoleBean bean = new VariableAttributeRoleBean(1, "myName", "myWidget", "myRegEx", false, false);
    assertEquals("{\"pk\":1,\"name\":\"myName\",\"inputwidget\":\"myWidget\",\"regexpattern\":\"myRegEx\",\"hasPrimaryAttribute\":false,\"hasLocale\":false}", converter.getAsString(null, null, bean));
    assertNull(converter.getAsString(null, null, null));
  }
}
