package square2.converter;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.jooq.exception.DataAccessException;
import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestLimitJsonConverter {
	/**
	 * test
	 */
	@Test
	public void testToJson() {
		LimitJsonConverter c = new LimitJsonConverter();
		assertEquals("{\"hard_up\": null, \"soft_up\": null, \"hard_low\": null, \"soft_low\": null}",
				c.toJson(null, null, null, null).toString());
		assertEquals("{\"hard_up\": \">1.2\", \"soft_up\": null, \"hard_low\": null, \"soft_low\": null}",
				c.toJson(null, null, null, ">1.2").toString());
		assertEquals("{\"hard_up\": \">1.2\", \"soft_up\": \">=1\", \"hard_low\": \"<=-1.2\", \"soft_low\": \">0\"}",
				c.toJson(">0", ">=1", "<=-1.2", ">1.2").toString());
		try {
			c.toJson("0.0", ">=1", "<=-1.2", ">1.2");
			assertFalse(true);
		} catch (DataAccessException e) {
			assertEquals("wrong format: The input of 0.0 is not allowed.", e.getMessage());
			assertTrue(true);
		}
	}
}
