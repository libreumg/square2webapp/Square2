package square2.modules.model.report;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

import square2.modules.model.datamanagement.ReleaseBean;

/**
 * 
 * @author henkej
 *
 */
public class TestReportModel {

	/**
	 * test
	 */
	@Test
	public void testFilterReleasesByVariablegroup() {
		ReportModel model = new ReportModel();
		
		List<ReleaseBean> releases = new ArrayList<>();
		releases.add(new ReleaseBean(1));
		releases.add(new ReleaseBean(2));
		Map<Integer, List<Integer>> reportReleaseMap = new HashMap<>();
		List<Integer> list42 = new ArrayList<>();
		list42.add(4);
		list42.add(2);
		reportReleaseMap.put(0, new ArrayList<>());
		reportReleaseMap.put(42, list42);
		
		List<ReleaseBean> list = model.filterReleasesByVariablegroup(0, releases, reportReleaseMap);
	
		assertEquals(0, list.size());

		List<ReleaseBean> list2 = model.filterReleasesByVariablegroup(42, releases, reportReleaseMap);
		
		assertEquals(1, list2.size());
		assertEquals(Integer.valueOf(2), list2.get(0).getPk());
	}
}
