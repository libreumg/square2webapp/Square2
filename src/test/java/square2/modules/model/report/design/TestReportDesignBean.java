package square2.modules.model.report.design;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestReportDesignBean {

	/**
	 * test
	 */
	@Test
	public void testReorderSnipplets() {
		ReportDesignBean bean = new ReportDesignBean(null, 0, 0, null, 0);
		SnippletBean bean1 = new SnippletBean(1, 1, "first", null, null);
		SnippletBean bean2 = new SnippletBean(2, 2, "second", null, null);
		SnippletBean bean3 = new SnippletBean(3, 3, "third", null, null);
		SnippletBean bean4 = new SnippletBean(4, 4, "fourth", null, null);
		SnippletBean bean5 = new SnippletBean(5, 5, "fifth", null, null);

		bean.getSnipplets().add(bean1);
		bean.getSnipplets().add(bean2);
		bean.getSnipplets().add(bean3);
		bean.getSnipplets().add(bean4);
		bean.getSnipplets().add(bean5);

		SnippletBean s = new SnippletBean(3, 3, "third", null, null);
		s.setOrderNrNew(1);
		bean.reorderSnipplets(s);

		assertEquals(Integer.valueOf(1), bean3.getOrderNr());
		assertEquals(Integer.valueOf(2), bean1.getOrderNr());
		assertEquals(Integer.valueOf(3), bean2.getOrderNr());
		assertEquals(Integer.valueOf(4), bean4.getOrderNr());
		assertEquals(Integer.valueOf(5), bean5.getOrderNr());

		bean1.setOrderNr(1);
		bean2.setOrderNr(2);
		bean3.setOrderNr(3);
		bean4.setOrderNr(4);
		bean5.setOrderNr(5);

		assertEquals(Integer.valueOf(1), bean1.getOrderNr());
		assertEquals(Integer.valueOf(2), bean2.getOrderNr());
		assertEquals(Integer.valueOf(3), bean3.getOrderNr());
		assertEquals(Integer.valueOf(4), bean4.getOrderNr());
		assertEquals(Integer.valueOf(5), bean5.getOrderNr());

		s.setOrderNrNew(5);
		bean.reorderSnipplets(s);

		assertEquals(Integer.valueOf(1), bean1.getOrderNr());
		assertEquals(Integer.valueOf(2), bean2.getOrderNr());
		assertEquals(Integer.valueOf(3), bean4.getOrderNr());
		assertEquals(Integer.valueOf(4), bean5.getOrderNr());
		assertEquals(Integer.valueOf(5), bean3.getOrderNr());
	}
}
