package square2.modules.model.report.analysis;

import static org.junit.jupiter.api.Assertions.assertNull;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestAnalysisDefaultBean {
	/**
	 * test
	 */
	@Test
	public void testGetJSONBValue() {
		AnalysisDefaultBean bean = new AnalysisDefaultBean(null, null, null, null, null, null, null, null, null);
		bean.setValue(null);
		assertNull(bean.getJSONBValue());
		bean.setValue("");
		assertNull(bean.getJSONBValue());
		bean.setValue("  ");
		assertNull(bean.getJSONBValue());
		bean.setValue("null");
		assertNull(bean.getJSONBValue());
	}
}
