package square2.modules.model.datamanagement;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;
import java.util.Set;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestDatamanagementModel {
	/**
	 * test
	 */
	@Test
	public void testToText() {
		Set<Integer> textSet = new HashSet<>();
		textSet.add(1);
		assertEquals("'this is crap'", DatamanagementModel.toText("this is crap", textSet, 1));
		assertEquals("'this is crap'", DatamanagementModel.toText("'this is crap", textSet, 1));
		assertEquals("'this is crap'", DatamanagementModel.toText("this is crap'", textSet, 1));
		assertEquals("'this is crap'", DatamanagementModel.toText("'this is crap'", textSet, 1));
		assertEquals("'it\"s a test'", DatamanagementModel.toText("it's a test", textSet, 1));
		assertEquals("null", DatamanagementModel.toText(null, textSet, 1));
		assertEquals("null", DatamanagementModel.toText("", textSet, 1));
		assertEquals("null", DatamanagementModel.toText("   ", textSet, 1));
	}
}
