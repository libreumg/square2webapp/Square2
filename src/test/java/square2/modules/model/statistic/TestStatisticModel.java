package square2.modules.model.statistic;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.HashSet;

import org.jooq.exception.DataAccessException;
import org.junit.jupiter.api.Test;

import square2.db.control.GatewayException;

/**
 * 
 * @author henkej
 *
 */
public class TestStatisticModel {
	/**
	 * test
	 * 
	 * @throws GatewayException
	 *           for any error
	 */
	@Test
	public void testCheckFunctionHasNoCallLoops() throws GatewayException {
		FunctionBean bean = new FunctionBean(1, null, true, null);
		bean.setFunctionId(1);
		bean.getReference().add(new FunctionBean(2, null, true, null));
		bean.getReference().get(0).setFunctionId(2);
		bean.getReference().get(0).setName("correct");

		assertEquals(2, new StatisticModel().checkFunctionHasNoCallLoops(bean, new HashSet<Integer>()).size());

		bean.getReference().add(new FunctionBean(3, null, true, null));
		bean.getReference().get(1).setFunctionId(2);
		bean.getReference().get(1).setName("incorrect");

		try {
			new StatisticModel().checkFunctionHasNoCallLoops(bean, new HashSet<Integer>());
		} catch (DataAccessException e) {
			assertEquals("recursive function call detected, aborting. Name of duplicated function is incorrect",
					e.getMessage());
		}
	}
}
