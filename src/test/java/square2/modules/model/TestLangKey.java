package square2.modules.model;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestLangKey {

	/**
	 * test the language key generation
	 */
	@Test
	public void testGenerateKey() {
		assertEquals("translation.key.0", LangKey.generateKey(0));
		assertEquals("translation.key.1", LangKey.generateKey(1));
		assertEquals("translation.key.42", LangKey.generateKey(42));
		assertEquals("my.very.own.key", LangKey.generateKey("my.very.own.key"));
		assertEquals("my.very.own.key", LangKey.generateKey(42, "my.very.own.key"));
		assertEquals("translation.key.42", LangKey.generateKey(42, " "));
		assertEquals("translation.key.42", LangKey.generateKey(42, null));
		assertEquals("my.very.own.key", LangKey.generateKey(null, "my.very.own.key"));
	}
}
