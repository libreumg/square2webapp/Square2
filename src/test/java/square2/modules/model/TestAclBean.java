package square2.modules.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;

/**
 * 
 * @author henkej
 *
 */
public class TestAclBean {
	/**
	 * test
	 */
	@Test
	public void testBasics() {
		AclBean bean = new AclBean();
		assertFalse(bean.getRead());
		assertFalse(bean.getWrite());
		assertFalse(bean.getExecute());
		bean.setRead(true);
		assertTrue(bean.getRead());
		bean.setWrite(true);
		assertTrue(bean.getWrite());
		bean.setExecute(true);
		assertTrue(bean.getExecute());
	}

	/**
	 * test
	 */
	@Test
	public void testGetAcl() {
		AclBean bean = new AclBean();
		assertNull(bean.getAcl());
		bean.setRead(true);
		assertEquals(EnumAccesslevel.r, bean.getAcl());
		bean.setWrite(true);
		assertEquals(EnumAccesslevel.rw, bean.getAcl());
		bean.setExecute(true);
		assertEquals(EnumAccesslevel.rwx, bean.getAcl());
		bean.setRead(false);
		assertEquals(EnumAccesslevel.wx, bean.getAcl());
		bean.setWrite(false);
		assertEquals(EnumAccesslevel.x, bean.getAcl());
		bean.setRead(true);
		assertEquals(EnumAccesslevel.rx, bean.getAcl());
	}

	/**
	 * test
	 */
	@Test
	public void testSetAcl() {
		AclBean bean = new AclBean();
		bean.setAcl(EnumAccesslevel.rwx);
		assertTrue(bean.getRead());
		assertTrue(bean.getWrite());
		assertTrue(bean.getExecute());
		bean.setAcl((EnumAccesslevel) null);
		assertFalse(bean.getRead());
		assertFalse(bean.getWrite());
		assertFalse(bean.getExecute());
	}

	/**
	 * test
	 */
	@Test
	public void testGetString() {
		AclBean bean = new AclBean();
		assertEquals("", bean.getString());
		bean.setRead(true);
		assertEquals("r", bean.getString());
		bean.setWrite(true);
		assertEquals("rw", bean.getString());
		bean.setExecute(true);
		assertEquals("rwx", bean.getString());
		bean.setRead(false);
		assertEquals("wx", bean.getString());
		bean.setWrite(false);
		assertEquals("x", bean.getString());
		bean.setRead(true);
		assertEquals("rx", bean.getString());
	}

	/**
	 * test
	 */
	@Test
	public void testSetString() {
		AclBean bean = new AclBean();
		bean.setString("rwx");
		assertTrue(bean.getRead());
		assertTrue(bean.getWrite());
		assertTrue(bean.getExecute());
		bean.setString("");
		assertFalse(bean.getRead());
		assertFalse(bean.getWrite());
		assertFalse(bean.getExecute());
		bean.setString("r");
		assertTrue(bean.getRead());
		assertFalse(bean.getWrite());
		assertFalse(bean.getExecute());
		bean.setString("w");
		assertFalse(bean.getRead());
		assertTrue(bean.getWrite());
		assertFalse(bean.getExecute());
		bean.setString("x");
		assertFalse(bean.getRead());
		assertFalse(bean.getWrite());
		assertTrue(bean.getExecute());
		bean.setString("rw");
		assertTrue(bean.getRead());
		assertTrue(bean.getWrite());
		assertFalse(bean.getExecute());
		bean.setString("rx");
		assertTrue(bean.getRead());
		assertFalse(bean.getWrite());
		assertTrue(bean.getExecute());
		bean.setString("wx");
		assertFalse(bean.getRead());
		assertTrue(bean.getWrite());
		assertTrue(bean.getExecute());
	}
}
