package square2.modules.model;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestLevelNumberBean {
  /**
   * test lower than function
   */
  @Test
  public void testLowerThan() {
    LevelNumberBean expectedBean = new LevelNumberBean("1.2.3");
    assertTrue(expectedBean.greaterOrEqualThan(new LevelNumberBean("1")));
    assertTrue(expectedBean.greaterOrEqualThan(new LevelNumberBean("1.2")));
    assertTrue(expectedBean.greaterOrEqualThan(new LevelNumberBean("1.2.2")));
    assertTrue(expectedBean.greaterOrEqualThan(new LevelNumberBean("1.2.2.9")));
    assertTrue(expectedBean.greaterOrEqualThan(new LevelNumberBean("1.2.3")));
    assertFalse(expectedBean.greaterOrEqualThan(new LevelNumberBean("1.3")));
    assertFalse(expectedBean.greaterOrEqualThan(new LevelNumberBean("1.2.4")));
    assertFalse(expectedBean.greaterOrEqualThan(new LevelNumberBean("1.2.3.1")));
    assertFalse(expectedBean.greaterOrEqualThan(new LevelNumberBean("1.3.3")));
    assertTrue(expectedBean.greaterOrEqualThan(new LevelNumberBean("1.1.2")));
    assertTrue(expectedBean.greaterOrEqualThan(new LevelNumberBean("1.1.12")));
  }
}
