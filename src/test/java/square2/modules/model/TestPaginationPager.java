package square2.modules.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestPaginationPager {
	/**
	 * test
	 */
	@Test
	public void testRenderPageButton() {
		PaginationPager pp = new PaginationPager();
		pp.setPagelength(2);
		pp.setOffset(2);
		pp.setMaxPagesOnButtonList(3);
		pp.increasePaginationPage(20);
		
		assertFalse(pp.renderPageButton(0));
		assertFalse(pp.renderPageButton(1));
		assertFalse(pp.renderPageButton(2));
		assertTrue(pp.renderPageButton(3));
		assertTrue(pp.renderPageButton(4));
		assertTrue(pp.renderPageButton(5));
		assertFalse(pp.renderPageButton(6));
		assertFalse(pp.renderPageButton(7));
		assertFalse(pp.renderPageButton(8));
		assertFalse(pp.renderPageButton(9));
		assertFalse(pp.renderPageButton(10));
		assertFalse(pp.renderPageButton(11));
		assertFalse(pp.renderPageButton(12));
		assertFalse(pp.renderPageButton(13));
		assertFalse(pp.renderPageButton(14));
		assertFalse(pp.renderPageButton(15));
		assertFalse(pp.renderPageButton(16));
		assertFalse(pp.renderPageButton(17));
		assertFalse(pp.renderPageButton(18));
		assertFalse(pp.renderPageButton(19));
		assertFalse(pp.renderPageButton(20));
	}
	
	/**
	 * test
	 */
	@Test
	public void testGetMaxPaginatorPages() {
		PaginationPager pp = new PaginationPager();
		pp.setPagelength(2);
		pp.setOffset(2);
		pp.setMaxPagesOnButtonList(3);
		
		assertEquals(Integer.valueOf(1), pp.getMaxPaginatorPages(4));
		assertEquals(Integer.valueOf(1), pp.getMaxPaginatorPages(5));
		assertEquals(Integer.valueOf(1), pp.getMaxPaginatorPages(6));
		assertEquals(Integer.valueOf(2), pp.getMaxPaginatorPages(7));
		assertEquals(Integer.valueOf(2), pp.getMaxPaginatorPages(12));
		assertEquals(Integer.valueOf(3), pp.getMaxPaginatorPages(13));
		assertEquals(Integer.valueOf(3), pp.getMaxPaginatorPages(17));
	}
}
