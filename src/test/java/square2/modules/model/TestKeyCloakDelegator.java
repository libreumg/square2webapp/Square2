package square2.modules.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

import javax.ws.rs.ProcessingException;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestKeyCloakDelegator {

	/**
	 * test login failures
	 * 
	 * @throws Exception on errors
	 */
	@Test
//	@Disabled
	public void testLogin() throws Exception {
		try {
			assertNull(new KeyCloakDelegator("dummy", "user", "http://invalid.url/", "ship", "Square2").login());
		} catch (ProcessingException e) {
			assertTrue(true);
		}

		File file = new File("/etc/tomcatsquare2/square2.properties");
		if (file.exists()) {
			Properties props = new Properties();
			props.load(new FileInputStream(file));
			assertTrue(props.containsKey("KEYCLOAK_URL"));
			assertTrue(props.containsKey("KEYCLOAK_REALM"));
			assertTrue(props.containsKey("KEYCLOAK_CLIENTID"));
//		assertTrue(props.containsKey("KEYCLOAK_SECRET"));

			String url = props.getProperty("KEYCLOAK_URL");
			String realm = props.getProperty("KEYCLOAK_REALM");
			String clientId = props.getProperty("KEYCLOAK_CLIENTID");
//		String secret = props.getProperty("KEYCLOAK_SECRET");

			KeyCloakDelegator kcd = new KeyCloakDelegator("square2unittest", "password", url, realm, clientId);

			String token = kcd.login();

			assertNotNull(token);

			assertEquals("Square2", kcd.getForename());
			assertEquals("UnitTest", kcd.getSurname());
			assertEquals(0, kcd.getUsnr());
			assertTrue(kcd.getRoles().contains("vargroup"));

			kcd.logout();
		} else {
			System.out.println("did not find " + file.getAbsolutePath() + ", ignoring keycloak connection test");
		}
	}
}
