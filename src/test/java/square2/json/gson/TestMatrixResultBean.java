package square2.json.gson;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.HashMap;

import org.jooq.exception.DataAccessException;
import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestMatrixResultBean {

	/**
	 * test
	 */
	@Test
	public void testConvertHex2Bit() {
		MatrixResultBean bean = new MatrixResultBean();
		bean.setVars(new ArrayList<>());
		assertNull(bean.convertHex2Bit(null));
		assertEquals("", bean.convertHex2Bit(""));
		assertEquals("0000", bean.convertHex2Bit("0"));
		assertEquals("0001", bean.convertHex2Bit("1"));
		assertEquals("0010", bean.convertHex2Bit("2"));
		assertEquals("0011", bean.convertHex2Bit("3"));
		assertEquals("0100", bean.convertHex2Bit("4"));
		assertEquals("0101", bean.convertHex2Bit("5"));
		assertEquals("0110", bean.convertHex2Bit("6"));
		assertEquals("0111", bean.convertHex2Bit("7"));
		assertEquals("1000", bean.convertHex2Bit("8"));
		assertEquals("1001", bean.convertHex2Bit("9"));
		assertEquals("1010", bean.convertHex2Bit("a"));
		assertEquals("1011", bean.convertHex2Bit("b"));
		assertEquals("1100", bean.convertHex2Bit("c"));
		assertEquals("1101", bean.convertHex2Bit("d"));
		assertEquals("1110", bean.convertHex2Bit("e"));
		assertEquals("1111", bean.convertHex2Bit("f"));

		assertEquals("1010", bean.convertHex2Bit("A"));
		assertEquals("1011", bean.convertHex2Bit("B"));
		assertEquals("1100", bean.convertHex2Bit("C"));
		assertEquals("1101", bean.convertHex2Bit("D"));
		assertEquals("1110", bean.convertHex2Bit("E"));
		assertEquals("1111", bean.convertHex2Bit("F"));
		
		assertEquals("1010111111111110", bean.convertHex2Bit("Affe"));
		
		String expected = "10101111"
			+ "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"	
			+ "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"	
			+ "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"	
			+ "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000"	
			+ "0010000000000000";
		String given = "af000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000";
		assertEquals(expected, bean.convertHex2Bit(given));
}
	
	/**
	 * test
	 */
	@Test
	public void testGetResBit() {
		MatrixResultBean bean = new MatrixResultBean();
		bean.setVars(new ArrayList<>());
		bean.getVars().add("bla");
		bean.setRes(new HashMap<>());
		bean.getRes().put("f1", "7A"); // 01111010
		bean.getRes().put("f2", "C"); // 1100
		assertFalse(bean.getResBit("f1", 0));
		assertTrue(bean.getResBit("f1", 1));
		assertTrue(bean.getResBit("f1", 2));
		assertTrue(bean.getResBit("f1", 3));
		assertTrue(bean.getResBit("f1", 4));
		assertFalse(bean.getResBit("f1", 5));
		assertTrue(bean.getResBit("f1", 6));
		assertFalse(bean.getResBit("f1", 7));
		assertTrue(bean.getResBit("f2", 0));
		assertTrue(bean.getResBit("f2", 1));
		assertFalse(bean.getResBit("f2", 2));
		assertFalse(bean.getResBit("f2", 3));
		try {
			bean.getResBit("f2", 4);
		} catch (DataAccessException e) {
			assertEquals("1100 is shorter than position 4", e.getMessage());
		}
		
		bean.getRes().clear();
		bean.getRes().put("f1", "af000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000"); // 10101111
		bean.getRes().put("f2", "fe000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000002000"); // 11111110
		assertTrue(bean.getResBit("f1", 0));
		assertFalse(bean.getResBit("f1", 1));
		assertTrue(bean.getResBit("f1", 2));
		assertFalse(bean.getResBit("f1", 3));
		assertTrue(bean.getResBit("f1", 4));
		assertTrue(bean.getResBit("f1", 5));
		assertTrue(bean.getResBit("f1", 6));
		assertTrue(bean.getResBit("f1", 7));
		assertTrue(bean.getResBit("f2", 0));
		assertTrue(bean.getResBit("f2", 1));
		assertTrue(bean.getResBit("f2", 2));
		assertTrue(bean.getResBit("f2", 3));
		assertTrue(bean.getResBit("f2", 4));
		assertTrue(bean.getResBit("f2", 5));
		assertTrue(bean.getResBit("f2", 6));
		assertFalse(bean.getResBit("f2", 7));
	}
}
