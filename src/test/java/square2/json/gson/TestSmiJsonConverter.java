package square2.json.gson;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import square2.modules.model.report.ajax.AjaxRequestBean;

/**
 * 
 * @author henkej
 *
 */
public class TestSmiJsonConverter {

  /**
   * test
   * 
   * @throws ShipJsonParseException on parse errors
   */
  @Test
  public void testExtractStrings() throws ShipJsonParseException {
    List<String> list = new SmiJsonConverter().extractStrings("oneString", "single");
    assertNotNull(list);
    assertEquals(1, list.size());
    assertEquals("oneString", list.get(0));
    
    List<String> stringList = new ArrayList<>();
    stringList.add("blurb");
    stringList.add("squash");
    list = new SmiJsonConverter().extractStrings(stringList, "multiple");
    assertNotNull(list);
    assertEquals(2, list.size());
    assertEquals("blurb", list.get(0));
    assertEquals("squash", list.get(1));
  }

  /**
   * test
   * 
   * @throws ShipJsonParseException on parse errors
   */
  @Test
  public void testAjaxRequestBean() throws ShipJsonParseException {
    // @formatter:off
    String jsonMultiple = "{" + 
        "\"version\": \"" + SmiJsonConverter.JSON_VERSION + "\"," + 
        "\"unique_name\": [ \"t1.introage\", \"t1.introsex\" ]," + 
        "\"matrixcolumn_name\": [ \"acc_margins_0\" ]," + 
        "\"matrix_element\": {" + 
        "\"selected\": true," +
        "\"params\": {" + 
        "\"group_vars\": {" + 
        "\"value\": \"t1.introbeg\"," + 
        "\"refers_metadata\": false}," + 
        "\"time_vars\": {" + 
        "\"value\": \"datetime\"," + 
        "\"refers_metadata\": true}," + 
        "\"co_vars\": {" + 
        "\"value\": \"t1.rrgrid\"," + 
        "\"refers_metadata\": false}," + 
        "\"min_obs_level\": {" + 
        "\"value\": 42" + 
        "}}}}";
    // @formatter:on
    AjaxRequestBean arBean = new SmiJsonConverter().toAjaxRequestBean(jsonMultiple, "multiple", "selected", "element",
        "insert", 42d);

    assertEquals("multiple", arBean.getCardinality());
    assertEquals("selected", arBean.getTargetColumns());
    assertEquals("element", arBean.getTargetTable());
    assertEquals("insert", arBean.getCrud());
    assertEquals(42l, arBean.getIndex());

    assertEquals(SmiJsonConverter.JSON_VERSION, arBean.getVersion());
    assertEquals(2, arBean.getUniqueNames().size());
    assertEquals("t1.introage", arBean.getUniqueNames().get(0));
    assertEquals("t1.introsex", arBean.getUniqueNames().get(1));
    assertEquals(1, arBean.getMatrixcolumnNames().size());
    assertEquals("acc_margins_0", arBean.getMatrixcolumnNames().get(0));
    assertTrue(arBean.getMatrixElement().getSelected());
    assertEquals(4, arBean.getMatrixElement().getParams().size());
    assertNotNull(arBean.getMatrixElement().getParams().get("group_vars"));
    assertEquals("\"t1.introbeg\"", arBean.getMatrixElement().getParams().get("group_vars").getValue());
    assertFalse(arBean.getMatrixElement().getParams().get("group_vars").getRefersMetadata());
    assertNotNull(arBean.getMatrixElement().getParams().get("time_vars"));
    assertEquals("\"datetime\"", arBean.getMatrixElement().getParams().get("time_vars").getValue());
    assertTrue(arBean.getMatrixElement().getParams().get("time_vars").getRefersMetadata());
    assertNotNull(arBean.getMatrixElement().getParams().get("co_vars"));
    assertEquals("\"t1.rrgrid\"", arBean.getMatrixElement().getParams().get("co_vars").getValue());
    assertFalse(arBean.getMatrixElement().getParams().get("co_vars").getRefersMetadata());
    assertNotNull(arBean.getMatrixElement().getParams().get("min_obs_level"));
    assertEquals("42.0", arBean.getMatrixElement().getParams().get("min_obs_level").getValue());
    assertNull(arBean.getMatrixElement().getParams().get("min_obs_level").getRefersMetadata());

    // @formatter:off
    String jsonSingle = "{" + 
        "\"version\": \"" + SmiJsonConverter.JSON_VERSION + "\"," + 
        "\"unique_name\": \"t1.introage\"," + 
        "\"matrixcolumn_name\": \"acc_margins_0\"," + 
        "\"matrix_element\": {" + 
        "\"selected\": true}}";
    // @formatter:on
    arBean = new SmiJsonConverter().toAjaxRequestBean(jsonSingle, "single", "selected", "element", "insert", 42d);

    assertEquals("single", arBean.getCardinality());
    assertEquals("selected", arBean.getTargetColumns());
    assertEquals("element", arBean.getTargetTable());
    assertEquals("insert", arBean.getCrud());
    assertEquals(42l, arBean.getIndex());

    assertEquals(SmiJsonConverter.JSON_VERSION, arBean.getVersion());
    assertEquals(1, arBean.getUniqueNames().size());
    assertEquals("t1.introage", arBean.getUniqueNames().get(0));
    assertEquals(1, arBean.getMatrixcolumnNames().size());
    assertEquals("acc_margins_0", arBean.getMatrixcolumnNames().get(0));
    assertTrue(arBean.getMatrixElement().getSelected());
    assertEquals(0, arBean.getMatrixElement().getParams().size());
  }
}
