package square2.json;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestJsonMatrixBean {

	/**
	 * test
	 */
	@Test
	public void testNullScaleBreaks() {
		JsonMatrixBean bean = new JsonMatrixBean();

		MatrixColumn allScales = new MatrixColumn("all_scales", "contains all", 1);
		allScales.getScales().addAll(Arrays.asList(new String[] {"text", "datetime", "nominal", "ordinal", "binary", "count", "numeric"}));

		MatrixColumn someScales = new MatrixColumn("some_scales", "contains nominal and datetime", 3);
		someScales.getScales().add("nominal");
		someScales.getScales().add("datetime");

		bean.getColumns().add(allScales);
		bean.getColumns().add(new MatrixColumn("no_scales", "contains none", 2));
		bean.getColumns().add(someScales);

		MatrixData textVar = new MatrixData("textvar");
		textVar.setScale("text");
		textVar.addFunctionFlag("all_scales", true);
		textVar.addFunctionFlag("some_scales", true);
		textVar.addFunctionFlag("no_scales", true);

		MatrixData nominalVar = new MatrixData("nominalvar");
		nominalVar.setScale("nominal");
		nominalVar.addFunctionFlag("all_scales", true);
		nominalVar.addFunctionFlag("some_scales", true);
		nominalVar.addFunctionFlag("no_scales", true);

		MatrixData ordinalVar = new MatrixData("ordinalvar");
		ordinalVar.setScale("ordinal");
		ordinalVar.addFunctionFlag("all_scales", true);
		ordinalVar.addFunctionFlag("some_scales", true);
		ordinalVar.addFunctionFlag("no_scales", true);

		bean.getDataRecords().add(textVar);
		bean.getDataRecords().add(ordinalVar);
		bean.getDataRecords().add(nominalVar);

		bean.nullScaleBreaks();

		assertEquals(3, bean.getDataRecords().size());

		assertEquals("textvar", bean.getDataRecords().get(0).getVarId());
		assertEquals(3, bean.getDataRecords().get(0).getFunctionFlags().size());
		assertTrue(bean.getDataRecords().get(0).getFunctionFlags().get(0).getValue());
		assertNull(bean.getDataRecords().get(0).getFunctionFlags().get(1).getValue());
		assertNull(bean.getDataRecords().get(0).getFunctionFlags().get(2).getValue());

		assertEquals("ordinalvar", bean.getDataRecords().get(1).getVarId());
		assertEquals(3, bean.getDataRecords().get(1).getFunctionFlags().size());
		assertTrue(bean.getDataRecords().get(1).getFunctionFlags().get(0).getValue());
		assertNull(bean.getDataRecords().get(1).getFunctionFlags().get(1).getValue());
		assertNull(bean.getDataRecords().get(1).getFunctionFlags().get(2).getValue());

		assertEquals("nominalvar", bean.getDataRecords().get(2).getVarId());
		assertEquals(3, bean.getDataRecords().get(2).getFunctionFlags().size());
		assertTrue(bean.getDataRecords().get(2).getFunctionFlags().get(0).getValue());
		assertTrue(bean.getDataRecords().get(2).getFunctionFlags().get(1).getValue());
		assertNull(bean.getDataRecords().get(2).getFunctionFlags().get(2).getValue());
	}
}
