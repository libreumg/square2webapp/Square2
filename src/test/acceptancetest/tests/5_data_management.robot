*** Settings ***
Documentation    Testing data management functionality
...
...
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/data_management_basic.robot
Resource          ${BASEDIR}/lib/data_management_rights_roles.robot

*** Test Cases ***
Data Management Basic
    [Setup]    Data Management Basic Setup
    Release Creation
#    Release Deletion
    [Teardown]    Log Out And Close Browser

Data Management Rights Roles
    [Setup]     Data Management Rights Roles Setup
    Data Management Rights Roles Test
    [Teardown]    Data Management Rights Roles Teardown