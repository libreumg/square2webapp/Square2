*** Settings ***
Documentation    Testing study overview functionality
...
...
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/study_overview_basic.robot
Resource          ${BASEDIR}/lib/study_overview_rights_roles.robot


*** Test Cases ***
Basic Test
    [Setup]    Study Overview Basic Setup
    Studygroup Creation
    Study Creation
    [Teardown]     Study Overview Basic Teardown

Rights and Roles
    [Setup]    Study Overview Rights Roles Setup
    Study Overview Rights Roles
    [Teardown]     Study Overview Rights Roles Teardown


