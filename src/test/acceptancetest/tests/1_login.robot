*** Settings ***
Documentation     Testing valid and invalid logins
...
...
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/login.robot
Resource          ${BASEDIR}/lib/util.robot
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/login_page    login    ${False}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/topmenu    topmenu


*** Test Cases ***
Invalid Login
    [Tags]    LOGIN
    User Is at login
    User Enters Invalid Credentials
    They Should Not Be Able To Login
    [Teardown]    Close Browser

Valid login
    [Tags]    LOGIN
    User Is At Login
    User Enters Valid Credentials
    They Should Be Logged In
    No Error Should Be Notified
    [Teardown]    Log Out And Close Browser

Logout
    [Tags]    LOGOUT
    User Is Logged In
    When They Logout
    They Should Be Logged Out
    No Error Should Be Notified
    [Teardown]    Close browser


*** Keywords ***
User Is At Login
    Open Browser To Login Page
    Wait Until Page Contains Element    ${login_input_username}

User Enters Invalid Credentials
    Input Username    invalid
    Input Password    combination
    Submit Credentials

They Should Not Be Able To Login
    Error Should Be Notified

User Enters Valid Credentials
    Input Username    ${USER}
    Input Password    ${PASSWORD}
    Submit Credentials

They Should Be Logged In
    Welcome Page Should Be Open

User Is Logged In
    Open Browser To Login Page
    Wait Until Page Contains Element    ${login_input_username}
    User Enters Valid Credentials

When They Logout
    Mouse Over      ${topmenu_button_openVersion}
    Click Link      ${topmenu_button_logout}

They Should Be Logged Out
    Page Should Not Contain Element   ${topmenu_button_openVersion}
    Element Should Be Visible   ${login_input_username}
