*** Settings ***
Documentation    Cleans everything up (this means running everything from singlemodule in the correct order)
...
...
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/topmenu.robot
Resource          ${BASEDIR}/lib/data_management_basic.robot
Resource          ${BASEDIR}/lib/variable_selection_basic.robot
Resource          ${BASEDIR}/lib/study_structure_basic.robot
Resource          ${BASEDIR}/lib/function_selection_basic.robot
Resource          ${BASEDIR}/lib/quality_reports_basic.robot
Suite Setup       Open Browser And Login
Suite Teardown    Log Out And Close Browser


*** Test Cases ***
Basic Test Chain Deletion
    #Quality Reports Basic Setup
    #Quality Report Deletion
    Function Selection Basic Setup
    Function Selection Deletion
    Data Management Basic Setup
    Release Deletion
    Variable Selection Basic Setup
    Variable Selection Deletion
 #   Data Deletion
 #   Missinglist Deletion



