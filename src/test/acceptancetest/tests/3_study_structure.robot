*** Settings ***
Documentation    Testing study structure functionality
...
...
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/study_structure_basic.robot
Resource          ${BASEDIR}/lib/study_structure_rights_roles.robot


*** Test Cases ***
Study Structure Basic
    [Setup]    Study Structure Basic Setup
    Study Selection
    Department Creation
    Variable Creation
    Variable Deletion
    Department Deletion
#    Missinglist Creation
#    Data Entering
#    Data Deletion
#    Missinglist Deletion
    [Teardown]    Study Structure Basic Teardown

Study Structure Rights Roles
    [Setup]    Study Structure Rights Roles Setup
    Study Structure Variable Rights Roles
    Study Structure Department Rights Roles
    [Teardown]    Study Structure Rights Roles Teardown


