*** Settings ***
Documentation    Testing data management functionality
...
...
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/quality_reports_basic.robot

*** Test Cases ***
Quality Report Basics
    [Setup]    Quality Reports Basic Setup
    Quality Report Creation
    Quality Report Calculation
#    Quality Report Document Generation
#    Quality Report Deletion
    [Teardown]    Close All Browsers




