# TODO
"""Specify persistent environment for rights/roles testing"""

# User
RR_USER = "testperson"
RR_PASSWORD = "1234"
RR_FULLNAME = "Testimonie Testa"


# Study Overview
RR_STUDYGROUP_NAME="ROBOT"
RR_STUDY_NAME = "ROBOT-1"

STUDYGROUP_NAME = "COHORT"
STUDY_NAME = "DATACOLL"
STUDY_START_DATE = "01.01.1999"
STUDY_END_DATE = "10.10.2009"
STUDY_PARTICIPANT_COUNT = 100
STUDY_UNIQUE_NAME = "datacoll1"


# Study Structure
RR_DEPT_TECHNAME = "rr_department1"
RR_VAR_TECHNAME = "rr_variable1"
RR_VARIABLE_PATH = "ROBOT/ROBOT-1/"

DEPT_TECHNAME = "department1"
VAR_TECHNAME = "variable1"
VAR_DTYPE = "string"
VAR_COLNAME = "column"

# Variable Selection
RR_VARIABLESELECTION_NAME = "rr_variableselection"
VARIABLESELECTION_NAME = "selection1"

# Data Management
RELEASE_NAME = "release1"
SHORT_NAME = "release1"
DATE = "10.03.2021"
DATABASE_TABLE = "robot_import"
MERGECOLUMN = "robot0.v00001"


