*** Settings ***
Documentation    Testing data management functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Resource          ${BASEDIR}/lib/data_management.robot
Resource          ${BASEDIR}/lib/user_rights_roles.robot



*** Keywords ***
Data Management Rights Roles Setup
    Set Screenshot Directory    ${SCREENSHOTS}/datamanagement_rights_roles
    Import Variables    ${BASEDIR}/data/rights_roles.py
    Import Variables    ${BASEDIR}/data/data.py
    Set Screenshot Directory    ${SCREENSHOTS}/datamanagement_rightsroles
    Open Browser And Log In
    User Views Data Management Welcome
    Add New Release    ${RELEASE_NAME}    ${DATA_DEPARTMENT}    ${SHORT_NAME}    ${DATABASE_TABLE}    ${MERGECOLUMN}    ${DATE}
    Log Out And Close Browser

Give User Rights To Release
    [Arguments]    ${release}    ${user}    ${change}=${False}    ${delete_previous}=${False}
    Open Browser And Login
    User Views Data Management Welcome
    Open Rights and Roles Panel From Datatable Element    ${release}
    IF    ${delete_previous}
        Delete Rights Entry    ${user}
        Wait Until Page Does Not Contain Element    //td[contains(text(), '${user}')]
    END
    Give User Rights    ${user}   write=${change}    read_exists=${False}    use_exists=${False}
    Exit Rights And Roles Panel
    Log Out And Close Browser

Check Release Rights
    [Arguments]   ${release}    ${user}    ${password}   ${change}=${False}
    Open Browser And Login As    ${user}    ${password}
    User Views Data Management Welcome
    IF    ${change}
        Page Should Contain    ${release}
        Page Should Contain Element    ${general_template_button_edit.format('${release}')}
    ELSE
        Page Should Contain    ${release}
        Page Should Not Contain Element    ${general_template_button_edit.format('${release}')}
    END
    Log Out And Close Browser

Data Management Rights Roles Test
    Give User Rights To Release    ${RELEASE_NAME}    ${RR_FULLNAME}    change=${False}
    Check Release Rights       ${RELEASE_NAME}    ${RR_USER}    ${RR_PASSWORD}    change=${False}
    Give User Rights To Release    ${RELEASE_NAME}    ${RR_FULLNAME}    change=${True}    delete_previous=${True}
    Check Release Rights       ${RELEASE_NAME}    ${RR_USER}    ${RR_PASSWORD}    change=${True}

Data Management Rights Roles Teardown
    Open Browser And Log In
    User Views Data Management Welcome
    Delete Release    ${RELEASE_NAME}
    Log Out And Close Browser


