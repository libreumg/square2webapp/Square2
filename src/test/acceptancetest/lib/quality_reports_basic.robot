*** Settings ***
Documentation    Testing quality report functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Resource          ${BASEDIR}/lib/quality_reports.robot

*** Keywords ***
Quality Report Creation
    User Views Quality Reports Welcome
    Create Report    ${REPORT_NAME}    ${FUNSELEC_NAME}    ${VARSELEC_NAME}    ${RELEASE_NAME}
    Page Should Contain    ${REPORT_NAME}
    No Error Should Be Notified

Quality Report Deletion
    User Views Quality Reports Welcome
    Delete Report    ${REPORT_NAME}
    Page Should Not Contain    ${REPORT_NAME}
    No Error Should Be Notified

Quality Report Calculation
    User Views Quality Reports Welcome
    Start Report Calculation    ${CALCULATED_REPORT}
    No Error Should Be Notified
    Poll Report Calculation     ${CALCULATED_REPORT}
    No Error Should Be Notified

Quality Report Document Generation
    User Views Quality Reports Welcome
    Start Report Document Generation    ${CALCULATED_REPORT}
    No Error Should Be Notified

Quality Report Document Verification
    User Views Quality Reports Welcome
    Compare Open Report To Sample Images    ${CALCULATED_REPORT}

Quality Reports Basic Setup
    Set Screenshot Directory    ${SCREENSHOTS}/quality_reports_basic
    Close All Browsers
    Open Browser And Login
    Import Variables    ${BASEDIR}/data/quality_reports.py
    Import Variables    ${BASEDIR}/data/data.py
    Import Variables    ${BASEDIR}/data/report_document_urls.py



