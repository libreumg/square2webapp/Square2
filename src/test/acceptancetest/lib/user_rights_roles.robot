*** Settings ***
Documentation     Utility keywords that are used to control parts of the software not currently under test
...
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           ${BASEDIR}/lib/SeleniumUtil.py
Resource          ${BASEDIR}/lib/util.robot
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/utility/rights_roles    rr    ${True}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/    general    ${False}



*** Keywords ***
Open Rights and Roles Panel From Datatable Element
    [Arguments]    ${TABLE_ELEMENT}
    #Scroll Element Into View    ${general_template_btnRightsRoles.format('${TABLE_ELEMENT}')}
    Scroll Until Element Uncovered    ${general_template_btnRightsRoles.format('${TABLE_ELEMENT}')}
    #Scroll Element In Div Into View    ${general_template_btnRightsRoles.format('${TABLE_ELEMENT}')}    //div[contains(@class, 'ui-datatable-tablewrapper')]
    Click Button    ${general_template_btnRightsRoles.format('${TABLE_ELEMENT}')}

Exit Rights and Roles Panel
    Click Button    ${rr_button_back}

Give Rights To Element From Dropdown
    [Arguments]    ${element}    ${dropdown}    ${read}=${False}    ${write}=${False}    ${use}=${False}    ${strict}=${False}   ${read_exists}=${True}    ${use_exists}=${True}
    Select Key From Dropdown    ${dropdown}    ${element}    ${strict}
    IF    ${read_exists} == True
        IF    ${read} == False
            Click Element    ${rr_checkbox_read}
        END
    END
    IF    ${write} == False
        Click Element    ${rr_checkbox_write}
    END
    IF    ${use_exists} == True
        IF    ${use} == False
            Click Element    ${rr_checkbox_use}
        END
    END
    Click Button    ${rr_button_add}


Delete Rights Entry
    [Arguments]    ${user_or_group}
    Click Button                 ${general_template_btnDelete.format('${user_or_group}')}
    Click First Interactable     ${general_button_approveDelete}


Give User Rights
    [Arguments]    ${user}    ${read}=${False}    ${write}=${False}    ${use}=${False}    ${read_exists}=${True}    ${use_exists}=${True}
    Give Rights To Element From Dropdown    ${user}    newuser    read=${read}    write=${write}    use=${use}     read_exists=${read_exists}    use_exists=${use_exists}


Give Group Rights
    [Arguments]    ${group}    ${read}=${False}    ${write}=${False}    ${use}=${False}    ${read_exists}=${True}    ${use_exists}=${True}
    Give Rights To Element From Dropdown    ${group}    newgrp    read=${read}    write=${write}    use=${use}    read_exists=${read_exists}    use_exists=${use_exists}








