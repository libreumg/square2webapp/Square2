*** Settings ***
Documentation    Testing study structure functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Library           csvdatareader.py
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/topmenu.robot
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/variable_selection   \    ${True}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/    general    ${False}

*** Keywords ***
User Views Variable Selection Welcome
    Wait Until Page Contains Element     ${topmenu_button_vargroup}
    Click Element    ${topmenu_button_vargroup}
    Wait Until Page Contains Element    ${formbutton_add}

Variable Selection Should Be Displayed With Variables
    [Arguments]    ${selection}    ${var_name}
    Page Should Contain    ${selection}
    Wait Until Page Contains Element   ${template_button_edit.format('${selection}')}
    Click Button    ${template_button_edit.format('${selection}')}
    Wait Until Page Contains    ${var_name}
    Click Button    ${edit_button_cancel}
    Wait Until Page Contains Element   ${template_button_edit.format('${selection}')}


Create Variable Selection
    [Arguments]    ${name}    ${cancel}=${True}
    Click Button    ${formbutton_add}
    Wait Until Page Contains Element    ${add_input_name}
    Input Text    ${add_input_name}    ${name}
    Input Text    ${add_textarea_description}    This is a generic test description.
    Click Button    ${add_button_add}
    No Error Should Be Notified
    Wait Until Page Contains Element    ${edit_button_cancel}
    No Error Should Be Notified
    IF    ${cancel}
        Click Button    ${edit_button_cancel}
        Wait Until Page Contains Element    ${formbutton_add}
        No Error Should Be Notified
    END

Add Variables To Open Selection
    [Arguments]    ${path}
    ${elements}=    Evaluate    $path.split('/')
    Wait Until Page Contains Element    ${edit_button_editContent}
    Click Button    ${edit_button_editContent}
    FOR    ${element}    IN    @{elements}
        No Error Should Be Notified
        Wait Until Page Contains Element    ${edit_content_template_button_gotoDept.format('${element}')}
        Click Element    ${edit_content_template_button_gotoDept.format('${element}')}
    END
    No Error Should Be Notified
    Wait Until Page Contains Element    ${edit_content_button_selectDirectChildren}
    Click Element    ${edit_content_button_selectDirectChildren}
    Wait Until Page Contains Element    ${edit_content_button_approveNewVars}
    Scroll Element In Div Into View    ${edit_content_button_approveNewVars}    //*[@id = 'form:primodaldialog']
    Click Element    ${edit_content_button_approveNewVars}
    Wait Until Page Contains Element    ${edit_button_cancel}
    No Error Should Be Notified
    Click Element    ${edit_button_cancel}

Delete Variable Selection
    [Arguments]    ${name}
    Wait Until Page Contains Element    ${general_template_btnDelete.format('${name}')}
    Click Element    ${general_template_btnDelete.format('${name}')}
    Click First Interactable    ${general_button_ApproveDelete}
