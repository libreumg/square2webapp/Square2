*** Settings ***
Documentation    Testing variable selection functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Library           csvdatareader.py
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/study_structure.robot
Resource          ${BASEDIR}/lib/user_rights_roles.robot
Variables         ${BASEDIR}/data/rights_roles.py
Variables         ${BASEDIR}/data/study_structure.py
*** Variables ***
${DEPT_TECHNAME}=    "rrdept"
${VAR_TECHNAME}=     "rrvar"
${VAR_COLUMN}=       "rrcolumn"
${VAR_DTYPE}=        "string"


*** Keywords ***
Study Structure Rights Roles Setup
    Set Screenshot Directory    ${SCREENSHOTS}/study_structure_rights_roles
    Import Variables    ${BASEDIR}/data/rights_roles.py
    Open Browser And Log In
    User Views Studystructure Welcome
    User Selects Study    ${RR_STUDY_NAME}
    Create Department    ${DEPT_TECHNAME}
    User Views Studystructure Welcome
    User Selects Study    ${RR_STUDY_NAME}
    Create Variable    ${VAR_TECHNAME}    ${VAR_COLUMN}    ${VAR_DTYPE}
    Log Out And Close Browser

Study Structure Rights Roles Teardown
    Open Browser And Log In
    User Views Studystructure Welcome
    User Selects Study    ${RR_STUDY_NAME}
    Delete Element    ${DEPT_TECHNAME}
    Delete Element    ${VAR_TECHNAME}
    Log Out And Close Browser

##
## ATTENTION: DOES NOT WORK, SEEMS TO ALWAYS GIVE READ RIGHTS??? (MAYBE BECAUSE OF STUDY RIGHTS - INVESTIGATE!)
##
##
Give User Rights To Element
    [Arguments]    ${user}    ${element}    ${study}    ${read}=${False}    ${write}=${False}    ${use}=${False}  ${delete_previous}=${False}  ${enter_dept}=${False}   ${dept_name}=${None}
    Open Browser And Log In
    User Views Studystructure Welcome
    User Selects Study    ${study}
    IF    ${enter_dept}
        Enter Department    ${dept_name}
    END
    Open Rights and Roles Panel From Datatable Element    ${element}
    IF    ${delete_previous}
        Delete Rights Entry    ${user}
        Wait Until Page Does Not Contain Element    //td[contains(text(), '${user}')]
    END
    Give User Rights    ${user}    read=${read}   write=${write}   use=${use}    read_exists=${True}
    Exit Rights And Roles Panel
    Log Out And Close Browser

Check Rights
    [Arguments]   ${user}     ${password}    ${element}    ${study}    ${read}=${False}    ${use}=${False}    ${write}=${False}     ${enter_dept}=${False}   ${dept_name}=${None}
    Open Browser And Login As    ${user}    ${password}
    User Views Studystructure Welcome
    User Selects Study    ${study}
    IF    ${enter_dept}
        Enter Department    ${dept_name}
    END
    IF    ${read}
        Page Should Contain    ${element}
    ELSE
        Page Should Not Contain    ${element}
    END
    IF    ${write}
        Page Should Contain Element    ${general_template_button_edit.format('${element}')}
    ELSE
        Page Should Not Contain Element    ${general_template_button_edit.format('${study}')}
    END
    #IF    ${use}
        #NEED MORE SPECS HERE
    #ELSE
    #END
    Log Out And Close Browser


Study Structure Variable Rights Roles
    Give User Rights To Element    Testimonie Testa    ${VAR_TECHNAME}    ${RR_STUDY_NAME}
    Check Rights    ${RR_USER}    ${RR_PASSWORD}    ${VAR_TECHNAME}    ${RR_STUDY_NAME}
    Give User Rights To Element    Testimonie Testa    ${VAR_TECHNAME}    ${RR_STUDY_NAME}   read=${True}    write=${False}    use=${False}    delete_previous=${True}
    Check Rights    ${RR_USER}    ${RR_PASSWORD}    ${VAR_TECHNAME}    ${RR_STUDY_NAME}   read=${True}    write=${False}    use=${False}
    Give User Rights To Element    Testimonie Testa    ${VAR_TECHNAME}    ${RR_STUDY_NAME}   read=${True}    write=${True}    use=${False}    delete_previous=${True}
    Check Rights    ${RR_USER}    ${RR_PASSWORD}    ${VAR_TECHNAME}    ${RR_STUDY_NAME}   read=${True}    write=${True}    use=${False}

Study Structure Department Rights Roles
    Give User Rights To Element    Testimonie Testa    ${DEPT_TECHNAME}    ${RR_STUDY_NAME}
    Check Rights    ${RR_USER}    ${RR_PASSWORD}    ${DEPT_TECHNAME}    ${RR_STUDY_NAME}
    Give User Rights To Element    Testimonie Testa    ${DEPT_TECHNAME}    ${RR_STUDY_NAME}   read=${True}    write=${False}    use=${False}    delete_previous=${True}
    Check Rights    ${RR_USER}    ${RR_PASSWORD}    ${DEPT_TECHNAME}    ${RR_STUDY_NAME}   read=${True}    write=${False}    use=${False}
    Give User Rights To Element    Testimonie Testa    ${DEPT_TECHNAME}    ${RR_STUDY_NAME}   read=${True}    write=${True}    use=${False}    delete_previous=${True}
    Check Rights    ${RR_USER}    ${RR_PASSWORD}    ${DEPT_TECHNAME}    ${RR_STUDY_NAME}   read=${True}    write=${True}    use=${False}






