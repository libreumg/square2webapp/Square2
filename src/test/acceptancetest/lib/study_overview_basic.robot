*** Settings ***
Documentation     Resource file exposing keywords specific to the study overview test suite
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           ${BASEDIR}/lib/SeleniumUtil.py
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/study_overview.robot


*** Keywords ***
Studygroup Creation
    User Views The Study Manager
    Add Studygroup    ${STUDYGROUP_NAME}
    The Studygroup Should Be Present    ${STUDYGROUP_NAME}
    No Error Should Be Notified

Study Creation
    User Is At Study Overview
    Add Data Collection    ${STUDY_NAME}    ${STUDY_START_DATE}    ${STUDY_END_DATE}    ${STUDY_PARTICIPANT_COUNT}    ${STUDY_UNIQUE_NAME}    ${STUDYGROUP_NAME}
    The Data Collection Should Be Present    ${STUDY_NAME}
    No Error Should Be Notified

Studygroup Deletion
    User Views The Study Manager
    Delete Studygroup    ${STUDYGROUP_NAME}
    No Error Should Be Notified

Study Overview Basic Teardown
    Studygroup Deletion
    Log Out And Close Browser

Study Overview Basic Setup
    Set Screenshot Directory    ${SCREENSHOTS}/study_overview_basic
    Close All Browsers
    Import Variables    ${BASEDIR}/data/study_overview.py
    Open Browser And Log In




