*** Settings ***
Documentation     Resource file exposing functionality for using the topmenu (e.g Go To Studyoverview Page)
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/topmenu

*** Keywords ***
Go To Studyoverview
    Click Link    ${button_cohort}

Go To Studystructure
    Click Link    ${button_study}

Go To Variable Selection
    Click Link    ${button_vargroup}

Go To Datamanagement
    Click Link    ${button_dataMgmt}

Go To Statistics
    Click Link    ${button_statistics}

Go To Function Selection
    Click Link    ${button_functionSelection}

Go To Reports
    Click Link    ${button_templateReport}

Go To Administration
    Click Link    ${buttomn_admin}

Go To Impressum
    Mouse Over    ${button_openVersion}
    Click Link    ${button_impressum}


Topmenu Should Look Like
    [Arguments]    ${topmenu}
    FOR   ${visible}    ${element}    IN
        ...    ${topmenu.get("studyoverview", False)}           ${button_cohort}
        ...    ${topmenu.get("studystructure", False)}         ${button_study}
        ...    ${topmenu.get("variableselection", False)}         ${button_vargroup}
        ...    ${topmenu.get("datamanagement", False)}          ${button_dataMgmt}
        ...    ${topmenu.get("statistics", False)}             ${button_statistic}
        ...    ${topmenu.get("functionselection", False)}         ${button_functionSelection}
        ...    ${topmenu.get("qualityreports", False)}         ${button_templateReport}
        ...    ${topmenu.get("admin", False)}             ${button_admin}
        IF    ${visible}
            Element Should Be Visible    ${element}
        ELSE
            Page Should Not Contain ELement    ${element}
        END
   END



