import json
import os

"""
Variable File for importing from the xpath repository in acceptancetest/xpaths.
If subdir is not False, this will import all the subdirectories of the specified directory recursively.
Directory names such as "../studygroup/add/elements.json" will be prepended to the variable name like so:
    ${studygroup_add_elementname}
"""

def variable_file_to_dict(path, prefix):
    if not path.endswith(".json"):
        if path.endswith("/"):
            path = path + "elements.json"
        else:
            path = path + "/elements.json"
    vardict = json.load(open(path))
    if prefix != "":
        keys = vardict.copy()
        for key in keys:
            vardict[prefix + "_" + key] = vardict.pop(key)
    return vardict


def get_variables(path="./", prefix="", subdirs=False):
    vardict = dict()
    if subdirs:
        path = path.replace("elements.json", "")
        walked = os.walk(path)
        for root, directories, files in walked:
            for file in files:
                inner_path = root + "/" + file
                inner_prefix = ""
                if prefix != "":
                    inner_prefix = prefix + "_"
                inner_prefix += root.replace(path, "").lstrip("/").replace("/", "_")
                #print(f"evaluating variable file '{inner_path}' with prefix '{inner_prefix}'")
                inner_vardict = variable_file_to_dict(inner_path, inner_prefix)
                vardict.update(inner_vardict)
    else:
        vardict = variable_file_to_dict(path, prefix)
    return vardict

