*** Settings ***
Documentation     Keywords for controlling the admin panel. Specifically, adding and deleting roles for users.
...
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Resource          ${BASEDIR}/lib/util.robot
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/topmenu    topmenu
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/admin    \    ${True}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/    general    ${False}



*** Keywords ***
Open Admin User Submenu
    Click Element    ${topmenu_button_admin}
    # as to not hover over the top left impressum menu
    Move Mouse To Position    10    35
    Mouse Over    ${submenu_users}

View Admin Role Section
    Open Admin User Submenu
    Click Element    ${button_roles}

View Admin User Section
    Open Admin User Submenu
    Click Element    ${button_users}

View Admin Group Section
    Open Admin User Submenu
    Click Element    ${button_groups}

Add New Role
    [Arguments]    ${user}    ${group}    ${role}    ${read}=${False}    ${write}=${False}    ${use}=${False}   ${user_strict}=${False}    ${group_strict}=${False}    ${role_strict}=${False}
    View Admin Role Section
    Scroll Element Into View    ${roles_button_openPanel}
    Click Element    ${roles_button_openPanel}
    #The naming of the dropdowns is extremely counter-intuitive. Beware!
    Scroll Until Element Uncovered    ${general_template_labelDropdown.format('usnr')}
    Select Key From Dropdown    usnr    ${user}     ${user_strict}
    Scroll Until Element Uncovered    ${general_template_labelDropdown.format('role')}
    Select Key From Dropdown    role    ${group}    ${group_strict}
    Scroll Until Element Uncovered    ${general_template_labelDropdown.format('right')}
    Select Key From Dropdown    right   ${role}     ${role_strict}
    IF    ${read} == False
        Click Element    ${roles_checkbox_read}
    END
    IF    ${write} == False
        Click Element    ${roles_checkbox_write}
    END
    IF    ${use} == False
        Click Element    ${roles_checkbox_use}
    END
    Click Button    ${roles_button_add}

#
# NOTE: Does not work for groups!
#
Delete Role
    [Arguments]    ${user}    ${role}
    View Admin Role Section
    #Beware: Filter selectors are hacky and id-less because we will be ditching this module in the near future
    Input Text    ${roles_filter_user}    ${user}
    Press Keys    ${roles_filter_user}     RETURN
    Input Text    ${roles_filter_role}    ${role}
    Press Keys    ${roles_filter_role}     RETURN
    Sleep         600ms
    Click Element    ${general_template_btnDelete.format('${user}')}
    Click First Interactable    ${general_button_ApproveDelete}







