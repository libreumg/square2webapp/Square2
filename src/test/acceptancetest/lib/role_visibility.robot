*** Settings ***
Documentation    Keywords for testing i
...
...
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/topmenu.robot
Resource          ${BASEDIR}/lib/admin.robot

*** Keywords ***
Check Role Visibility
    [Documentation]    Takes a list of roles, a dictionary for what the topmenu should look like, a user and a password and a so called delete filter that
    ...                should be a uniquely identifying part of the usernames associated full name.
    ...                Tests if the user, assigned the roles from rolelist, can see the specified topmenu when logged in
    ...                CAUTION: role selection is strict (eg. only selects if the text on the dropdown label matches exactly) because of roles that contain very similar parts.
    [Arguments]    ${rolelist}    ${topmenu}     ${user}    ${password}     ${deletefilter}=Test
    Open Browser And Login
    FOR    ${role}    IN    @{rolelist}
        Add New Role    ${user}    ---   ${role}    True    True    True    role_strict=True
    END
    Log Out And Close Browser
    Open Browser And Login As    ${user}    ${password}
    Topmenu Should Look Like     ${topmenu}
    Log Out And Close Browser
    [Teardown]    Visibility Teardown    ${rolelist}    ${deletefilter}


Visibility Teardown
    [Arguments]    ${rolelist}    ${deletefilter}=Test
    Open Browser And Login
    FOR    ${role}    IN    @{rolelist}
        Delete Role    ${deletefilter}    ${role}
    END
    Log Out And Close Browser

Role Visibility Setup
    Set Screenshot Directory    ${SCREENSHOTS}/role_visibility
