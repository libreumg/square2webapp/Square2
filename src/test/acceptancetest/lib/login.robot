*** Settings ***
Documentation     Resource file exposing keywords specific to the login test suite
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/login_page    login
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/topmenu    topmenu

*** Keywords ***
Go To Login Page
    Go To    ${START URL}
    Wait Until Page Contains Element   ${login_input_username}
    #Location Should Be    ${LOGIN URL}

Input Username
    [Arguments]    ${username}
    Input Text     ${login_input_username}    ${username}

Input Password
    [Arguments]    ${password}
    Input Text     ${login_input_password}    ${password}

Submit Credentials
    Click Button    ${login_button_login}

Welcome Page Should Be Open
    Element Should Be Visible   ${topmenu_button_openversion}

Log In As
    [Arguments]    ${user}    ${pass}
    Go To Login Page
    Input Username     ${user}
    Input Password    ${pass}
    Submit Credentials
    Welcome Page Should Be Open

