*** Settings ***
Documentation    Testing data management functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/function_selection.robot


*** Keywords ***
Function Selection Creation
    User Views Function Selection Welcome
    Create Function Selection    ${FUNCTIONSELECTION_NAME}    False
    Add All Functions To Open Selection
    Function Selection Should Be Displayed    ${FUNCTIONSELECTION_NAME}
    No Error Should Be Notified

Function Selection Deletion
    [Tags]    singlemodule
    User Views Function Selection Welcome
    Delete Function Selection    ${FUNCTIONSELECTION_NAME}
    Page Should Not Contain    ${FUNCTIONSELECTION_NAME}
    No Error Should Be Notified

Function Selection Basic Setup
    Set Screenshot Directory    ${SCREENSHOTS}/function_selection_basic
    Close All Browsers
    Open Browser And Login
    Import Variables    ${BASEDIR}/data/function_selection.py
    Import Variables    ${BASEDIR}/data/data.py