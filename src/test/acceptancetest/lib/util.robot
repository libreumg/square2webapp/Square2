*** Settings ***
Documentation     Utility keywords that are used to control parts of the software not currently tested
...
...               This is e.g. used for logging in in the test buildup and logging out in teardown
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Resource          ${BASEDIR}/lib/login.robot
Resource          ${BASEDIR}/lib/browser.robot
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/login_page    login
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/topmenu    topmenu
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/    general    ${False}



*** Keywords ***
Open Browser And Login
    Open Browser To Login Page
    Wait Until Page Contains Element    ${login_input_username}
    Log In As    ${USER}    ${PASSWORD}
    Element Should Be Visible   ${topmenu_button_openVersion}

Open Browser And Login As
    [Arguments]    ${USER}    ${PASSWORD}
    Open Browser To Login Page
    Wait Until Page Contains Element    ${login_input_username}
    Log In As    ${USER}    ${PASSWORD}
    Element Should Be Visible   ${topmenu_button_openVersion}

Log Out And Close Browser
    Mouse Over      ${topmenu_button_openVersion}
    Click Link      ${topmenu_button_logout}
    Wait Until Element Is Visible    ${login_input_username}
    Element Should Be Visible        ${login_input_username}
    Element Should Be Visible        ${login_input_password}
    Close All Browsers

Error Should Be Notified
    Element Should Be Visible   css:.ui-messages-error

No Error Should Be Notified
    Element Should Not Be Visible   css:.ui-messages-error
    Page Should Not Contain Element    css:.ui-messages-error

Wait For Ajax Request
    [Arguments]    ${timeout}=10
    Wait Until Page Does Not Contain Element     //*[contains(@id, 'ajaxrequestopen')]    timeout=${timeout}

Select Key From Dropdown
    [Arguments]    ${dropdown}    ${key}    ${strict}=${False}
    TRY
        Scroll Element In Div Into View    ${general_template_labelDropdown.format('${dropdown}')}     //*[@id='form:primodaldialog']
    EXCEPT
        Log     Select Key From Dropdown tried to scroll //*[@id='form:primodaldialog'] but it did not exist    DEBUG
    END
    Click Element    ${general_template_labelDropdown.format('${dropdown}')}
    Scroll Element In Div Into View    ${general_template_elementDropdown.format('${dropdown}','${key}')}    ${general_template_scrollableDropdown.format('${dropdown}')}
    IF    ${strict}
        Click Element     ${general_template_elementDropdown_strict.format('${dropdown}','${key}')}
    ELSE
        Click Element     ${general_template_elementDropdown.format('${dropdown}','${key}')}
    END

