*** Settings ***
Documentation    Testing data management functionality
...
...
Library           SeleniumLibrary       screenshot_root_directory=${SCREENSHOTS}
Library           SeleniumUtil.py
Library    XML
Resource          ${BASEDIR}/lib/browser.robot
Resource          ${BASEDIR}/lib/util.robot
Resource          ${BASEDIR}/lib/topmenu.robot
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/function_selection   \    ${True}
Variables         ${BASEDIR}/lib/xpaths.py    ${BASEDIR}/xpaths/    general    ${False}


*** Keywords ***
User Views Function Selection Welcome
    Wait Until Page Contains Element    ${topmenu_button_functionSelection}
    Click Element    ${topmenu_button_functionSelection}


Delete Function Selection
    [Arguments]    ${FUNCTIONSELECTION_NAME}
    Wait Until Page Contains Element    ${formbutton_add}
    Click Element    ${general_template_btnDelete.format('${FUNCTIONSELECTION_NAME}')}
    Click First Interactable    ${general_button_ApproveDelete}
    Wait Until Page Contains Element    ${formbutton_add}
    No Error Should Be Notified


Create Function Selection
    [Arguments]    ${FUNCTIONSELECTION_NAME}    ${cancel}=True
    Wait Until Page Contains Element    ${formbutton_add}
    Click Element    ${formbutton_add}
    Wait Until Page Contains Element    ${add_input_name}
    Input Text    ${add_input_name}   ${FUNCTIONSELECTION_NAME}
    Input Text    ${add_textarea_description}    Generic test description
    Click Button    ${add_button_approveFunSelec}
    Wait Until Page Contains Element    ${edit_buttongroup_addFunction}
    No Error Should Be Notified
    IF     ${cancel}
        Click Element    ${edit_button_cancel}
        Wait Until Page Contains Element    ${formbutton_add}
        No Error Should Be Notified
    END

Add All Functions To Open Selection
    Wait Until Page Contains Element    ${edit_buttongroup_addFunction}
    ${buttonElements}=    Get Web Elements    ${edit_buttongroup_addFunction}
    ${buttons}=    Evaluate    [f"//*[@id='{elem.get_attribute('id')}']" for elem in $buttonElements]
    FOR    ${button}    IN    @{buttons}
        Scroll Element In Div Into View    ${button}    //*[@id='scrollablebody']
        Click Element    ${button}
        Wait Until Page Contains Element    ${edit_buttongroup_addFunction}
        No Error Should Be Notified
    END
    Scroll To Bottom    //*[@id='scrollablebody']
    Click Element    ${edit_button_approveFunSelec}
    Wait Until Page Contains Element    ${edit_button_cancel}
    No Error Should Be Notified
    Scroll Element In Div Into View    ${edit_button_cancel}    //*[@id='form:primodaldialog']
    Click Element    ${edit_button_cancel}
    Wait Until Page Contains Element    ${formbutton_add}
    No Error Should Be Notified


Function Selection Should Be Displayed
    [Arguments]    ${FUNCTIONSELECTION_NAME}
    Page Should Contain    ${FUNCTIONSELECTION_NAME}
    Click Button    ${general_template_button_edit.format('${FUNCTIONSELECTION_NAME}')}
    Wait Until Page Contains Element     ${edit_buttongroup_editFunction}
    No Error Should Be Notified
    Click Button    ${edit_button_cancel}
    Wait Until Page Contains Element    ${formbutton_add}
    No Error Should Be Notified

