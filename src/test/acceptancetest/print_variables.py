import sys
import os
from lib.xpaths import get_variables

"""
Prints all the Variable names imported to a .robot file specified to argv[1].
argv[2]=='-p' shows all the according x-paths
"""


def main():
    basedir = os.getcwd().replace("util", "")
    file = open(sys.argv[1])
    lines = file.readlines()
    for line in lines:
        if line.startswith("Variables"):
            commands = line.split()
            if "xpaths.py" in commands[1]:
                path = commands[2].replace("${BASEDIR}", basedir)
                prefix = ""
                subdirs = False
                if len(commands) > 3:
                    prefix = commands[3]
                    print(f'evaluating with prefix "{prefix}"')
                    if prefix.startswith("subdirs"):
                        subdirs = True
                        prefix = ""
                    if len(commands) > 4:
                        subdirs = True
                        if commands[4] == "${False}":
                            subdirs = False

                variables = get_variables(path, prefix, subdirs)
                print("============================ \n")
                print(commands[2] + ":       \n")
                if len(sys.argv) > 2 and sys.argv[2] == "-p":
                    for key in variables:
                        print(f"{key}:    '{variables[key]}' \n")
                else:
                    for key in variables:
                        print(f"{key}")


if __name__ == '__main__':
    main()
