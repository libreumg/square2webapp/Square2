package square2.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author henkej
 *
 */
public class JsonMatrixBean implements Serializable {
	private static final long serialVersionUID = 1L;

	@SerializedName("colDef")
	private List<MatrixColumn> columns;
	
	@SerializedName("rowDef")
	private List<MatrixRow> rows;
	
	@SerializedName("data_records")
	private List<MatrixData> dataRecords;

	/**
	 * generate new matrix bean with empty lists
	 */
	public JsonMatrixBean() {
		this.columns = new ArrayList<>();
		this.rows = new ArrayList<>();
		this.dataRecords = new ArrayList<>();
	}

	/**
	 * set the flag to null where scale and variable do not fit
	 */
	public void nullScaleBreaks() {
		for (MatrixData bean : dataRecords) {
			String scale = bean.getScale();
			for (FunctionFlagBean ffb : bean.getFunctionFlags()) {
				String fktnName = ffb.getName();
				for (MatrixColumn column : columns) {
					if (column.getName().equals(fktnName) && !column.getScales().contains(scale)) {
						ffb.setValue(null);
					}
				}
			}
		}
	}

	/**
	 * get the data row with varid = id
	 * 
	 * @param id
	 *          the variable id
	 * @return the row of the matrix
	 */
	public MatrixData getDataRow(String id) {
		for (MatrixData bean : dataRecords) {
			if (bean.getVarId().equals(id)) {
				return bean;
			}
		}
		return null;
	}

	/**
	 * @return the columns
	 */
	public List<MatrixColumn> getColumns() {
		return columns;
	}

	/**
	 * @return the rows
	 */
	public List<MatrixRow> getRows() {
		return rows;
	}

	/**
	 * @return the data
	 */
	public List<MatrixData> getDataRecords() {
		return dataRecords;
	}
}
