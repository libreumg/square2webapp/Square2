package square2.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class MatrixColumn implements Serializable, Comparable<MatrixColumn> {
	private static final long serialVersionUID = 1L;

	private String name;
	private String description;
	private Integer orderNr;
	private List<String> scales;

	/**
	 * @param name the name
	 * @param description the description
	 * @param orderNr the order
	 */
	public MatrixColumn(String name, String description, Integer orderNr) {
		this.name = name;
		this.description = description;
		this.orderNr = orderNr;
		this.scales = new ArrayList<>();
	}

	public String toString() {
		StringBuilder buf = new StringBuilder(name == null ? "" : name);
		buf.append(",").append(orderNr).append(":");
		String separator = "";
		for (String s : scales) {
			buf.append(s).append(separator);
			separator = ",";
		}
		return buf.toString();
	}

	@Override
	public int compareTo(MatrixColumn o) {
		return orderNr == null || o == null || o.getOrderNr() == null ? 0 : orderNr.compareTo(o.getOrderNr());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the orderNr
	 */
	public Integer getOrderNr() {
		return orderNr;
	}

	/**
	 * @param orderNr the orderNr to set
	 */
	public void setOrderNr(Integer orderNr) {
		this.orderNr = orderNr;
	}

	/**
	 * @return the scales
	 */
	public List<String> getScales() {
		return scales;
	}
}
