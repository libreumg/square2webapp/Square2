package square2.json;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

/**
 * 
 * @author henkej
 *
 */
public class MatrixData implements Serializable, Comparable<MatrixData> {
	private static final long serialVersionUID = 1L;

	@SerializedName("var_id")
	private String varId;

	@SerializedName("fkScale")
	private String scale;
	private List<FunctionFlagBean> functionFlags;

	/**
	 * @param varId the varId
	 */
	public MatrixData(String varId) {
		this.varId = varId;
		this.functionFlags = new ArrayList<>();
	}

	public String toString() {
		StringBuilder buf = new StringBuilder(varId == null ? "" : varId);
		buf.append(":").append(scale).append(":");
		boolean first = true;
		for (FunctionFlagBean bean : functionFlags) {
			if (bean.getValue()) {
				buf.append(first ? "" : ",").append(bean.getName());
			}
			first = false;
		}
		return buf.toString();
	}

	/**
	 * add a function flag
	 * 
	 * @param name the name
	 * @param value the value
	 * @return this class
	 */
	public MatrixData addFunctionFlag(String name, Boolean value) {
		functionFlags.add(new FunctionFlagBean(name, value));
		return this;
	}

	/**
	 * set the value of the flag referenced by name; if not found, create one
	 * 
	 * @param name the name
	 * @param value the value
	 */
	public void setFunctionFlag(String name, Boolean value) {
		for (FunctionFlagBean bean : functionFlags) {
			if (bean.getName().equals(name)) {
				bean.setValue(value);
				return; // interrupt on found value to reduce runtime
			}
		}
		// not found (no interruption)? Then create one
		functionFlags.add(new FunctionFlagBean(name, value));
	}

	@Override
	public int compareTo(MatrixData o) {
		return varId == null || o == null || o.getVarId() == null ? 0 : varId.compareTo(o.getVarId());
	}

	/**
	 * @return the varId
	 */
	public String getVarId() {
		return varId;
	}

	/**
	 * @param varId the varId to set
	 */
	public void setVarId(String varId) {
		this.varId = varId;
	}

	/**
	 * @return the functionFlags
	 */
	public List<FunctionFlagBean> getFunctionFlags() {
		return functionFlags;
	}

	/**
	 * @return the scale
	 */
	public String getScale() {
		return scale;
	}

	/**
	 * @param scale the scale to set
	 */
	public void setScale(String scale) {
		this.scale = scale;
	}
}
