package square2.json.gson;

import java.lang.reflect.Type;
import java.util.Map.Entry;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import square2.json.MatrixData;

/**
 * 
 * @author henkej
 *
 */
public class MatrixDataGsonDeserializer implements JsonDeserializer<MatrixData> {

	@Override
	public MatrixData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		JsonObject o = json.getAsJsonObject();

		JsonElement varIdJson = o.get("var_id");
		if (varIdJson == null) {
			throw new JsonParseException("could not find var_id in json string");
		}
		MatrixData bean = new MatrixData(varIdJson.getAsString());

		for (Entry<String, JsonElement> entry : o.entrySet()) {
			if (!entry.getKey().equals("var_id")) {
				JsonElement jsonValue = entry.getValue();
				Boolean value = jsonValue.isJsonNull() ? null : jsonValue.getAsBoolean();
				bean.setFunctionFlag(entry.getKey(), value);
			}
		}
		return bean;
	}
}
