package square2.json.gson;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import square2.json.FunctionFlagBean;
import square2.json.MatrixData;

/**
 *
 * @author henkej
 *
 */
public class MatrixDataGsonSerializer implements JsonSerializer<MatrixData> {

	@Override
	public JsonElement serialize(MatrixData bean, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject result = new JsonObject();
		result.addProperty("var_id", bean.getVarId());
		for (FunctionFlagBean fBean : bean.getFunctionFlags()) {
			result.addProperty(fBean.getName(), fBean.getValue());
		}
		return result;
	}
}
