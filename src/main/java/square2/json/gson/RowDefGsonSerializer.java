package square2.json.gson;

import java.lang.reflect.Type;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;

import square2.json.MatrixRow;

/**
 * 
 * @author henkej
 *
 */
@Deprecated
public class RowDefGsonSerializer implements JsonSerializer<List<MatrixRow>> {

	@Override
	public JsonElement serialize(List<MatrixRow> list, Type typeOfSrc, JsonSerializationContext context) {
		JsonObject result = new JsonObject();
		for (MatrixRow bean : list) {
			JsonObject value = new JsonObject();
			value.addProperty("description", bean.getDescription());
			result.add(bean.getId(), value);
		}
		return result;
	}
}
