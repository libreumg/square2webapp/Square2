package square2.json.gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import square2.modules.model.report.ajax.AjaxRequestBean;
import square2.modules.model.report.ajax.JsonBean;
import square2.modules.model.report.ajax.MatrixElementBean;
import square2.modules.model.report.ajax.ParamBean;

/**
 * @author henkej
 *
 */
public class SmiJsonConverter {
  private static final Logger LOGGER = LogManager.getLogger(SmiJsonConverter.class.getSimpleName());

  /**
   * the ajax json version
   */
  public static final String JSON_VERSION = "1.3.6.34";

  /**
   * create the AjaxRequestBean from json ajax request
   * 
   * @param json the json ajax request
   * @param cardinality the cardinality
   * @param targetColumns the target columns
   * @param targetTable the target table
   * @param crud the db operation, one of insert, update or delete
   * @param index the index - use for ordering only
   * @return the AjaxRequestBean
   * @throws ShipJsonParseException on parse exceptions
   */
  public AjaxRequestBean toAjaxRequestBean(String json, String cardinality, String targetColumns, String targetTable,
      String crud, Double index) throws ShipJsonParseException {
    LinkedTreeMap<String, ?> jsonRoot = parseAndCheckVersion(json);
    String version = (String) jsonRoot.get("version");
    List<String> uniqueNames = extractStrings(jsonRoot.get("unique_name"), cardinality);
    List<String> matrixColumnNames = extractStrings(jsonRoot.get("matrixcolumn_name"), cardinality);
    LinkedTreeMap<String, ?> jsonMatrixElement = (LinkedTreeMap<String, ?>) jsonRoot.get("matrix_element"); // must not be null by definition
    Boolean selected = (Boolean) jsonMatrixElement.get("selected");
    MatrixElementBean matrixElementBean = new MatrixElementBean(selected);
    LinkedTreeMap<String, ?> jsonParams = (LinkedTreeMap<String, ?>) jsonMatrixElement.get("params");
    if (jsonParams != null) {
      Set<String> paramNames = jsonParams.keySet();
      for (String paramName : paramNames) {
        LinkedTreeMap<String, ?> paramFields = (LinkedTreeMap) jsonParams.get(paramName);
        String value = new Gson().toJson(paramFields.get("value"));
        Boolean refersMetadata = (Boolean) paramFields.get("refers_metadata");
        ParamBean paramBean = new ParamBean(value, refersMetadata);
        matrixElementBean.getParams().put(paramName, paramBean);
      }
    }
    JsonBean jsonBean = new JsonBean(version, matrixElementBean);
    jsonBean.getUniqueNames().addAll(uniqueNames == null ? new ArrayList<>() : uniqueNames);
    jsonBean.getMatrixcolumnNames().addAll(matrixColumnNames == null ? new ArrayList<>() : matrixColumnNames);
    return new AjaxRequestBean(jsonBean, cardinality, targetColumns, targetTable, crud, index);
  }

  protected LinkedTreeMap<String, ?> parseAndCheckVersion(String json) throws ShipJsonParseException {
    Gson gson = new Gson();
    LinkedTreeMap<String, ?> map = gson.fromJson(json, LinkedTreeMap.class);
    String version = (String) map.get("version");
    if (!JSON_VERSION.equals(version)) {
      throw new ShipJsonParseException(ShipJsonParseException.JSON_PARSE_ERROR.VERSION);
    }
    return map;
  }

  protected List<String> extractStrings(Object o, String cardinality) throws ShipJsonParseException {
    if ("multiple".equals(cardinality)) {
      return (List<String>) o;
    } else if ("single".contentEquals(cardinality)) {
      String s = (String) o;
      List<String> list = new ArrayList<>();
      list.add(s);
      return list;
    } else {
      StringBuilder buf = new StringBuilder("unknown cardinality: ");
      buf.append(cardinality);
      throw new ShipJsonParseException(buf.toString());
    }
  }
}
