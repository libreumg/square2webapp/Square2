package square2.json.gson;

import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

/**
 * 
 * @author henkej
 *
 */
public class MatrixResultBean {
	private static final Logger LOGGER = LogManager.getLogger(MatrixResultBean.class);
	
	private Map<String, String> res;
	private List<String> fields;
	private List<String> vars;

	/**
	 * get bit of res of function id as key
	 * 
	 * @param functionId
	 *          the key of res
	 * @param position
	 *          the position of the bit
	 * @return true or false on found values, null otherwise
	 */
	public Boolean getResBit(String functionId, Integer position) {
		String bytes = res.get(functionId);
		if (bytes == null) {
			return null;
		} else {
			String bits = convertHex2Bit(bytes);
			LOGGER.trace("{} -> {}", bytes, bits);
			if (bits.length() < position + 1) {
				throw new DataAccessException(bits + " is shorter than position " + position);
			} else {
				return '1' == bits.charAt(position);
			}
		}
	}

	/**
	 * convert hexadecimal string to bit string
	 * 
	 * @param hex
	 *          the hexadecimal string, e.g. affe
	 * @return the bit string, e.g. 1010111111111110
	 */
	public String convertHex2Bit(String hex) {
		if (hex == null) {
			return null;
		}
		StringBuilder buf = new StringBuilder();
		for (int i = 0; i < hex.length(); i++) {
			String s = hex.substring(i, i + 1);
			Byte b = Byte.parseByte(s, 16);
			buf.append(String.format("%4s", Integer.toBinaryString(b)).replace(' ', '0'));
		}
		return buf.toString();
	}

	/**
	 * @return the res
	 */
	public Map<String, String> getRes() {
		return res;
	}

	/**
	 * @return the fields
	 */
	public List<String> getFields() {
		return fields;
	}

	/**
	 * @return the vars
	 */
	public List<String> getVars() {
		return vars;
	}

	/**
	 * @param res
	 *          the res to set
	 */
	public void setRes(Map<String, String> res) {
		this.res = res;
	}

	/**
	 * @param fields
	 *          the fields to set
	 */
	public void setFields(List<String> fields) {
		this.fields = fields;
	}

	/**
	 * @param vars
	 *          the vars to set
	 */
	public void setVars(List<String> vars) {
		this.vars = vars;
	}
}
