package square2.json.gson;

/**
 * 
 * @author henkej
 *
 */
public class ShipJsonParseException extends Exception {
  private static final long serialVersionUID = 1L;

  private JSON_PARSE_ERROR jpe;
  /**
   * 
   * @author henkej
   *
   */
  public static enum JSON_PARSE_ERROR {
    /**
     * version conflict
     */
    VERSION, 
    /**
     * wrong datatype for unique name
     */
    DATATYPE_UNIQUENAME, 
    /**
     * wrong datatype for matrixcolumn name 
     */
    DATATYPE_MATRIXCOLUMN_NAME, 
    /**
     * unknown parser
     */
    UNKNOWN_PARSER,
    /**
     * other error
     */
    OTHER;
  };
  
  /**
   * @param message the message
   */
  public ShipJsonParseException(String message) {
    super(message);
    this.jpe = JSON_PARSE_ERROR.OTHER;
  }
  
  /**
   * @param jpe the json parse exception constant
   */
  public ShipJsonParseException(JSON_PARSE_ERROR jpe) {
    super(jpe == JSON_PARSE_ERROR.VERSION ? "wrong version" : "unknown parse error");
    this.jpe = jpe;
  }

  /**
   * @return the jpe
   */
  public JSON_PARSE_ERROR getJpe() {
    return jpe;
  }
}
