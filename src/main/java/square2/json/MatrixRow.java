package square2.json;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class MatrixRow implements Serializable, Comparable<MatrixRow> {
	private static final long serialVersionUID = 1L;

	private String id;
	private String name;
	private String description;
	private Integer varOrder;

	/**
	 * @param id the id
	 * @param description the description
	 * @param varOrder the variable order in this variable group
	 * @param name the variable name
	 */
	public MatrixRow(String id, String description, Integer varOrder, String name) {
		this.id = id;
		this.description = description;
		this.varOrder = varOrder;
		this.name = name;
	}

	public String toString() {
		StringBuilder buf = new StringBuilder(id == null ? "" : id);
		buf.append(":").append(name).append(",").append(varOrder);
		return buf.toString();
	}

	@Override
	public int compareTo(MatrixRow o) {
		return varOrder == null || o == null || o.getVarOrder() == null ? 0 : varOrder.compareTo(o.getVarOrder());
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the varOrder
	 */
	public Integer getVarOrder() {
		return varOrder;
	}

	/**
	 * @param varOrder the varOrder to set
	 */
	public void setVarOrder(Integer varOrder) {
		this.varOrder = varOrder;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
}
