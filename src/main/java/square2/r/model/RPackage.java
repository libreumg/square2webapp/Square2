package square2.r.model;

/**
 * 
 * @author henkej
 *
 */
public class RPackage {

	private String name;
	private String version;

	/**
	 * create a new R package
	 */
	public RPackage() {
		super();
	}

	/**
	 * create a new R package
	 * 
	 * @param name
	 *          the name of the package
	 * @param version
	 *          the version of the package
	 */
	public RPackage(String name, String version) {
		this();
		this.name = name;
		this.version = version;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version
	 *          the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

}
