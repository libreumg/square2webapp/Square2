package square2.r.model;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class DataFrame {
	private String csvFileName;
	private String dataFrameName;
	private String separator;
	private String quotator;
	private String decimalSeparator;
	private Boolean hasHeader;
	private List<String> lines;

	/**
	 * create new dataframe
	 * 
	 * @param csvFileName
	 *          name of the csv file
	 * @param dataFrameName
	 *          name of the data frame
	 * @param hasHeader
	 *          flag if this data frame has a header
	 * @param separator
	 *          the separator
	 * @param quotator
	 *          the delimiter
	 * @param decimalSeparator
	 *          the decimal separator character
	 */
	public DataFrame(String csvFileName, String dataFrameName, Boolean hasHeader, String separator, String quotator,
			String decimalSeparator) {
		this.csvFileName = csvFileName;
		this.dataFrameName = dataFrameName;
		this.hasHeader = hasHeader;
		this.separator = separator;
		this.quotator = quotator;
		this.decimalSeparator = decimalSeparator;
		lines = new ArrayList<>();
	}

	/**
	 * @return the lines
	 */
	public List<String> getLines() {
		return lines;
	}

	/**
	 * @return the csv file name
	 */
	public String getCsvFileName() {
		return csvFileName;
	}

	/**
	 * @return the data frame name
	 */
	public String getDataFrameName() {
		return dataFrameName;
	}

	/**
	 * @return the flag if this data frame has a header
	 */
	public Boolean hasHeader() {
		return hasHeader;
	}

	/**
	 * @return the separator
	 */
	public String getSeparator() {
		return separator;
	}

	/**
	 * @return the delimiter
	 */
	public String getQuotator() {
		return quotator;
	}

	/**
	 * @return the decimal separator
	 */
	public String getDecimalSeparator() {
		return decimalSeparator;
	}

	/**
	 * @param decimalSeparator
	 *          the decimal separator
	 */
	public void setDecimalSeparator(String decimalSeparator) {
		this.decimalSeparator = decimalSeparator;
	}
}
