package square2.r.model;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.help.SquareFacesContext;

/**
 * 
 * @author henkej
 *
 */
public class DataFrameStream extends DataFrame {
	private static final Logger LOGGER = LogManager.getLogger(DataFrameStream.class);

	/**
	 * @param csvFileName
	 *          the csv file name
	 * @param dataFrameName
	 *          the data frame name
	 * @param hasHeader
	 *          the flag if this data frame stream has a header
	 * @param separator
	 *          the separator
	 * @param quotator
	 *          the delimiter
	 * @param decimalSeparator
	 *          the decimal separator
	 * @param stream
	 *          the stream
	 * @param facesContext
	 *          the context of this function call
	 */
	public DataFrameStream(String csvFileName, String dataFrameName, Boolean hasHeader, String separator, String quotator,
			String decimalSeparator, InputStream stream, SquareFacesContext facesContext) {
		super(csvFileName, dataFrameName, hasHeader, separator, quotator, decimalSeparator);
		try (BufferedReader br = new BufferedReader(new InputStreamReader(stream))) {
			String line = br.readLine();
			while (line != null) {
				getLines().add(line);
				line = br.readLine();
			}
		} catch (IOException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}
}
