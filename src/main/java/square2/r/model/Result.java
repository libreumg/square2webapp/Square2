package square2.r.model;

import java.util.HashMap;
import java.util.Map;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.RList;

/**
 * 
 * @author henkej
 *
 */
public class Result {
	private Map<String, REXP> values;

	/**
	 * create new result
	 * 
	 * @param result
	 *          the R result
	 * @param isTest
	 *          a flag if this is for tests
	 * @throws REXPMismatchException
	 *           for R export mismatch errors
	 */
	public Result(REXP result, boolean isTest) throws REXPMismatchException {
		values = new HashMap<>();
		if (result == null) {
			values.put("no.result", null);
		} else if (result.isList()) {
			RList list = result.asList();
			if (list != null && list.keys() != null) {
				for (String name : list.keys()) {
					values.put(name, list.at(name));
				}
			}
		} else if (isTest) {
			values.put("test result", result);
		} else {
			values.put("unknown.result", result);
		}
	}

	/**
	 * @return the values of the result
	 */
	public Map<String, REXP> getValues() {
		return values;
	}
}
