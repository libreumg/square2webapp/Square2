package square2.r.control;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.http.Part;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RFileInputStream;
import org.rosuda.REngine.Rserve.RFileOutputStream;
import org.rosuda.REngine.Rserve.RserveException;

import square2.help.SquareFacesContext;

/**
 * 
 * @author henkej
 *
 */
public abstract class RGatewayInterface implements AutoCloseable {
	
	private static final Logger LOGGER = LogManager.getLogger(RGatewayInterface.class);

  private final SquareFacesContext facesContext;

  /**
   * @param facesContext the faces context
   */
  public RGatewayInterface(SquareFacesContext facesContext) {
    this.facesContext = facesContext;
  }

  /**
   * get the R connection from the servlet context
   * 
   * @return the R connection
   * @throws IOException on io errors
   * @throws RserveException on r connection errors
   */
  protected static final RConnection getConnection(ServletContext ctx) throws RserveException, IOException {
    RExecutor executor = new RExecutor(ctx);
    RConnection connection = executor.getConnection();
    return connection;
  }
  
  /**
   * get the R connection; consider to use loadLibrary instead that returns a connection also
   * 
   * @return the R connection
   * @throws IOException on io errors
   * @throws RserveException on r connection errors
   */
  protected RConnection getConnection() throws RserveException, IOException {
    RExecutor executor = new RExecutor(facesContext);
    RConnection connection = executor.getConnection();
    return connection;
  }

  /**
   * @return the facesContext
   */
  public SquareFacesContext getFacesContext() {
    return facesContext;
  }

  /**
   * get the squaredbconnectorfile from facesContext; defaults to /etc/squaredbconnector.properties
   * 
   * @return the content
   */
  public String getConfig() {
    if (facesContext != null) {
      return facesContext.getParam("squaredbconnectorfile", "/etc/squaredbconnector.properties");
    } else {
      return "/etc/squaredbconnector.properties";
    }
  }

  /**
   * get the squaredbconnectorsection from facesContext; defaults to prod
   * 
   * @return the section
   */
  public String getSection() {
    if (facesContext != null) {
      return facesContext.getParam("squaredbconnectorsection", "prod");
    } else {
      return "prod";
    }
  }

  /**
   * load the corresponding library
   * 
   * @param libname the name of the R library to load
   * 
   * @return the R connection
   * @throws RserveException on r connection errors
   * @throws IOException on io errors
   */
  public RConnection loadLibrary(String libname) throws RserveException, IOException {
    RConnection connection = getConnection();
    try {
      connection.parseAndEval("library(" + libname + ")");
    } catch (REngineException | REXPMismatchException e) {
      throw new RserveException(connection, "could not load library " + libname);
    }
    return connection;
  }
  
  /**
   * upload a file to the rsession and return its filename
   * 
   * @param connection the R connection
   * @param filename the expected filename
   * @param file the file
   * @return the filename
   * @throws IOException on IO errors
   */
  public String uploadFileToRSession(RConnection connection, String filename, Part file) throws IOException {
  	LOGGER.debug("try to create file {} on R server", filename);
  	RFileOutputStream os = connection.createFile(filename);
  	LOGGER.debug("file {} created, starting with streaming content...", filename);
  	long size = file.getInputStream().transferTo(os);
  	os.close();
  	file.getInputStream().close();
  	LOGGER.debug("transferred {} bytes of file stream to the R server", size);
		return new StringBuilder("file:").append(filename).toString();
  }

  /**
   * load the library of the gateway; obviously should call loadLibrary(libname)
   * 
   * @return the R connection
   * @throws RserveException on r connection errors
   * @throws IOException on io errors
   */
  public abstract RConnection loadLibrary() throws RserveException, IOException;
}
