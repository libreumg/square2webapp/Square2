package square2.r.control.cmdbuilder;

/**
 * 
 * @author henkej
 *
 */
public class FunctionArgument implements Argument {

	private final String key;
	private final String value;
	private final Boolean forgetOnNull;

	/**
	 * @param key the key
	 * @param args the arguments
	 */
	public FunctionArgument(String key, Argument... args) {
		this.key = key;
		StringBuilder buf = new StringBuilder();
		boolean isFirst = true;
		for (Argument arg : args) {
			if (isFirst) {
				isFirst = false;
			} else {
				buf.append(",");
			}
			buf.append(arg.toString());
		}
		this.value = buf.toString();
		this.forgetOnNull = null;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append(key).append("(");
		if (value != null) {
			buf.append(value);
		} else {
			buf.append("NULL");
		}
		buf.append(")");
		return buf.toString();
	}
	
	@Override
	public String getKey() {
		return key;
	}

	@Override
	public String getValue() {
		return value;
	}

	@Override
	public Boolean getForgetOnNull() {
		return forgetOnNull;
	}
}
