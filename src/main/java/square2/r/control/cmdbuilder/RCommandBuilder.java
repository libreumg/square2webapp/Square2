package square2.r.control.cmdbuilder;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author henkej
 *
 */
public class RCommandBuilder implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = LogManager.getLogger();

	private final StringBuilder buf;

	private final boolean useTry;

	/**
	 * @param useTry if true, wrap the whole R block with a try command
	 */
	public RCommandBuilder(boolean useTry) {
		this.buf = new StringBuilder();
		this.useTry = useTry;
		LOGGER.debug("surround by try: ", useTry ? "true" : "false");
		if (useTry) {
			buf.append("try(");
		}
	}

	/**
	 * append s to the command list
	 * 
	 * @param s the appendable command
	 * @return this object
	 */
	public RCommandBuilder append(String s) {
		buf.append(s);
		return this;
	}

	/**
	 * add a function to the command block
	 * 
	 * @param functionName the name of the function
	 * @param args the arguments
	 * @return this object
	 */
	public RCommandBuilder appendFunction(String functionName, Argument... args) {
		buf.append(functionName).append("(");
		boolean first = true;
		for (Argument pair : args) {
			if (!pair.forgetOnNullAndIsNull()) {
				buf.append(first ? "" : ", ").append(pair.toString());
				first = false;
			}
		}
		buf.append(")\n");
		return this;
	}

	@Override
	public String toString() {
		return new StringBuilder(buf.toString()).append(useTry ? ")" : "").toString();
	}
}
