package square2.r.control.config;

import com.google.gson.JsonObject;

/**
 * 
 * @author henkej
 *
 */
public interface ConfigurationInterface {
	/**
	 * @return the configuration
	 */
	public String getConfiguration();

	/**
	 * update a repository in the configuration
	 * 
	 * @param overwriteIfExists if the repository already exists, overwrite it (true) or use it (false)
	 * @param repositoryName the name of the repository in this configuration
	 * @param alias the alias for the repository in thn this configuration
	 * @param repositoryDefinition the definition of the repository
	 */
	public void updateRepository(boolean overwriteIfExists, String repositoryName, String alias, JsonObject repositoryDefinition);
}
