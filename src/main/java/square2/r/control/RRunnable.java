package square2.r.control;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RFileOutputStream;

/**
 * 
 * @author henkej
 *
 * @deprecated use RConnection.voidEvalDetach instead
 *
 */
@Deprecated
public class RRunnable implements Runnable {
	private final RConnection connection;
	private final List<String> commands;
	private final List<ExternalDataFile> externalDataFiles;
	private final ExceptionRaiserInterface raiser;

	/**
	 * generate new runnable for R executions
	 * 
	 * @param connection
	 *          the connection to the R server
	 * @param commands
	 *          list of R commands to be executed in that order
	 * @param externalDataFiles
	 *          list of external data files, might be null
	 * @param raiser
	 *          exception interface to catch exceptions
	 */
	public RRunnable(RConnection connection, List<String> commands, List<ExternalDataFile> externalDataFiles,
			ExceptionRaiserInterface raiser) {
		this.connection = connection;
		this.commands = commands;
		this.externalDataFiles = externalDataFiles == null ? new ArrayList<>() : externalDataFiles;
		this.raiser = raiser;
	}

	@Override
	public void run() {
		runAndReturnProtocol();
	}

	/**
	 * run directly
	 * 
	 * @return list of R messages from the result
	 */
	public List<RMessage> runAndReturnProtocol() {
		List<RMessage> result = new ArrayList<>();
		// transfer data into R workspace
		String filename = "";
		try {
			for (ExternalDataFile file : externalDataFiles) {
				filename = file.getName();
				RFileOutputStream rfos = connection.createFile(filename);
				InputStream is = file.getInputStream();
				is.transferTo(rfos);
				rfos.close();
				is.close();
				result.add(new RMessage(RMessage.LOGLEVEL_INFO, "Transferred file " + filename + " successfully."));
			}
		} catch (IOException e) {
			StringBuilder buf = new StringBuilder();
			buf.append(e.getMessage()).append(" on uploading file ").append(filename);
			raiser.raise(e, buf.toString());
			result.add(new RMessage(RMessage.LOGLEVEL_ERROR, buf.toString()));
		}

		// execute R commands
		for (String command : commands) {
			try {
				REXP rexp = connection.parseAndEval(command);
				result.add(new RMessage(RMessage.LOGLEVEL_INFO, rexp));
			} catch (REngineException | REXPMismatchException e) {
				StringBuilder buf = new StringBuilder();
				buf.append(e.getMessage()).append(", command was ").append(command);
				raiser.raise(e, buf.toString());
				result.add(new RMessage(RMessage.LOGLEVEL_ERROR, buf.toString()));
			}
		}
		return result;
	}
}
