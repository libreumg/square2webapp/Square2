package square2.r.control;

import java.io.IOException;
import java.net.UnknownHostException;

import javax.net.ssl.SSLSocketFactory;

import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

/**
 * 
 * @author henkej
 *
 */
public class SslRConnection extends RConnection {
	/**
	 * @param host
	 *          the host to connect to
	 * @param port
	 *          the port to connect to
	 * @throws RserveException
	 *           the exception
	 * @throws IOException
	 *           the exception
	 * @throws UnknownHostException
	 *           the exception
	 */
	public SslRConnection(String host, int port) throws RserveException, IOException {
		super(SSLSocketFactory.getDefault().createSocket(host, port));
	}
}
