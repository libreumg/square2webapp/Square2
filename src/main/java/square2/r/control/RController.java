package square2.r.control;

import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RFileOutputStream;
import org.rosuda.REngine.Rserve.RserveException;

import square2.help.SquareFacesContext;
import square2.r.model.DataFrame;
import square2.r.model.Result;
import square2.r.model.RuntimeFunction;

/**
 * 
 * @author henkej
 *
 */
public class RController extends RControllerInterface {
	private static final Logger LOGGER = LogManager.getLogger(RController.class);

	private List<DataFrame> dataFrames;
	private List<DataFrame> metaDataFrames;
	private RuntimeFunction function;

	/**
	 * create a new R controller
	 * 
	 * @param dataFrames
	 *          the list of data frames
	 * @param metaDataFrames
	 *          the list of meta data frames
	 * @param function
	 *          the runtime function
	 */
	public RController(List<DataFrame> dataFrames, List<DataFrame> metaDataFrames, RuntimeFunction function) {
		this.dataFrames = dataFrames;
		this.metaDataFrames = metaDataFrames;
		this.function = function;
	}

	/**
	 * transfer csv file to database and make dataframe from it
	 * 
	 * @param c
	 *          the connection
	 * @param df
	 *          the data frame
	 * @return command to execute loading transferred data frame
	 * @throws IOException
	 *           for invalid input
	 * @throws REXPMismatchException
	 *           for R export mismatch
	 * @throws REngineException
	 *           for R engine errors
	 */
	private String addDataFrame(RConnection c, DataFrame df, SquareFacesContext facesContext) throws IOException {
		if (df == null) {
			return "";
		} else if (df.getLines().size() < 2) {
			facesContext.notifyError("no statistical data found in {}", df.getDataFrameName());
			return "";
		}

		// transfer data into R workspace
		RFileOutputStream rfos = c.createFile(df.getCsvFileName());
		PrintStream ps = new PrintStream(rfos);
		for (String line : df.getLines()) {
			ps.println(line);
		}
		ps.close();

		// load transferred file into R environment
		StringBuilder buf = new StringBuilder();
		buf.append(df.getDataFrameName());
		buf.append(" <- read.table(\"").append(df.getCsvFileName()).append("\", ");
		buf.append(df.hasHeader() ? "header=T,sep=\"" : "sep=\"").append(df.getSeparator().equals("\"") ? "\\" : "")
				.append(df.getSeparator());
		buf.append("\", quote=\"").append(df.getQuotator().equals("\"") ? "\\" : "").append(df.getQuotator());
		buf.append("\", dec=\"").append(df.getDecimalSeparator());
		buf.append("\", na.strings=\"null\")");
		return buf.toString();
	}

	@Override
	public Result execute(boolean isTest, SquareFacesContext facesContext)
			throws RControllerException, REXPMismatchException, RserveException, IOException {
		if (dataFrames == null) {
			facesContext.notifyError("no statistical data found; list of data frames was null");
			return null;
		}
		List<String> runR = new ArrayList<>();
		RConnection c = getConnection(facesContext);

		// add data frames
		if (dataFrames != null) {
			for (DataFrame df : dataFrames) {
				runR.add(addDataFrame(c, df, facesContext));
			}
		}

		// add meta data frames
		if (metaDataFrames != null) {
			for (DataFrame df : metaDataFrames) {
				runR.add(addDataFrame(c, df, facesContext));
			}
		}

		// add function code as file in R workspace
		RFileOutputStream rfos = c.createFile(function.getFileName());
		LOGGER.debug(function.getFileName());
		PrintStream ps = new PrintStream(rfos);
		for (String line : function.getLines()) {
			LOGGER.debug(line);
			ps.println(line);
		}
		ps.close();

		// load function code in R environment from workspace
		StringBuilder buf = new StringBuilder();
		// TODO: get rid of calling source from remote because of app armor
		buf.append("source(\"");
		buf.append(function.getFileName());
		buf.append("\")");
		runR.add(buf.toString());

		for (String line : runR) {
			LOGGER.debug("R: {}", line);
			try {
				c.parseAndEval(line);
			} catch (REngineException e) {
				LOGGER.error("error on executing {}", line);
				throw new RControllerException(e, function.getCallStatement().concat("\non line ").concat(line));
			}
		}
		LOGGER.debug("R: {}", function.getCallStatement());

		REXP result;
		try {
			result = c.parseAndEval(function.getCallStatement());
		} catch (REngineException e) {
			LOGGER.error("error on executing {}", function.getCallStatement());
			throw new RControllerException(e, function.getCallStatement());
		} catch (REXPMismatchException e) {
			throw new RControllerException(e, function.getCallStatement());
		}

		return new Result(result, isTest);
	}

	/**
	 * @return the runtime function
	 */
	public RuntimeFunction getFunction() {
		return function;
	}
}
