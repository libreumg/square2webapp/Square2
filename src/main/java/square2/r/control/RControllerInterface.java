package square2.r.control;

import java.io.IOException;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import square2.help.SquareFacesContext;
import square2.r.model.Result;

/**
 * 
 * @author henkej
 * 
 */
public abstract class RControllerInterface {

	/**
	 * get R connection to server
	 * 
	 * @param facesContext
	 *          context of this function call
	 * @return the connection
	 * @throws RserveException
	 *           for R serve errors
	 * @throws IOException
	 *           on io errors
	 */
	protected RConnection getConnection(SquareFacesContext facesContext) throws RserveException, IOException {
		return ConnectionManager.getConnection(facesContext);
	}

	/**
	 * execute R call
	 * 
	 * @param isTest
	 *          flag to determine if this execution is a test
	 * @param facesContext
	 *          to be used for messages
	 * @return the result
	 * 
	 * @throws RserveException
	 *           for R serve errors
	 * @throws IOException
	 *           for input errors
	 * @throws REXPMismatchException
	 *           for R export mismatch errors
	 * @throws RControllerException
	 *           for R controller errors
	 */
	public abstract Result execute(boolean isTest, SquareFacesContext facesContext)
			throws RControllerException, REXPMismatchException, RserveException, IOException;
}
