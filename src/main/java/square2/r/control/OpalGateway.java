package square2.r.control;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import square2.help.SquareFacesContext;
import square2.modules.model.study.io.OpalBean;

/**
 * 
 * @author henkej
 *
 */
public class OpalGateway extends RGatewayInterface {
	private static final Logger LOGGER = LogManager.getLogger(OpalGateway.class);
	/**
	 * the R library name
	 */
	public static final String LIBNAME = "opalRepository";

	/**
	 * @param facesContext the context of the call
	 */
	public OpalGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public RConnection loadLibrary() throws RserveException, IOException {
		RConnection connection;
		try {
			connection = getConnection();
		} catch (RserveException e) {
			LOGGER.error(e.getMessage(), e);
			getFacesContext().notifyException(e);
			return null;
		}
		REXP rexp;
		try {
			rexp = connection.parseAndEval("try(library(" + LIBNAME + "))");
			if (!rexp.isString() || !LIBNAME.equals(rexp.asString())) {
				throw new IOException("loaded library " + LIBNAME + " and got " + rexp.asString());
			} else {
				LOGGER.debug("successfully loaded library {}", LIBNAME);
			}
		} catch (REngineException | REXPMismatchException e) {
			LOGGER.error(e.getMessage(), e);
			getFacesContext().notifyException(e);
			return connection;
		}
		return connection;
	}

	/**
	 * get all OPAL configuration sections (= opal connections)
	 * @param opalConfigFile the opal configuration file
	 * 
	 * @return a list of opal sections; an empty one at least
	 * @throws REXPMismatchException on expression errors
	 * @throws REngineException on R engine errors
	 * @throws IOException on io errors
	 */
	public List<String> getOpalUrls(String opalConfigFile)
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		StringBuilder buf = new StringBuilder();
		buf.append("try(getUrls(");
		if (opalConfigFile != null) {
			buf.append("config_file='").append(opalConfigFile).append("'");
		}
		buf.append("))");
		LOGGER.debug("calling R with {}", buf.toString());
		REXP result = connection.parseAndEval(buf.toString());
		if (!result.isVector()) {
			throw new IOException(result.asString());
		}
		String[] res = result.asStrings();
		List<String> list = Arrays.asList(res);
		return list;
	}
	
	/**
	 * get all opal projects of that section (= opal connection)
	 * @param opalConfigFile the opal configuration file
	 * @param section the opal configuration section
	 * 
	 * @return a list of opal projects; an empty one at least
	 * @throws REXPMismatchException on expression errors
	 * @throws REngineException on R engine errors
	 * @throws IOException on io errors
	 */
	public List<String> getOpalProjects(Object opalConfigFile, String section)
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		StringBuilder buf = new StringBuilder();
		buf.append("try(getProjects(");
		if (opalConfigFile != null) {
			buf.append("config_file='").append(opalConfigFile).append("', ");
		}
		buf.append("config_section='").append(section).append("'))");
		LOGGER.debug("calling R with {}", buf.toString());
		REXP result = connection.parseAndEval(buf.toString());
		if (!result.isVector()) {
			throw new IOException(result.asString());
		}
		String[] res = result.asStrings();
		List<String> list = Arrays.asList(res);
		return list;
	}
	
	/**
	 * get all opal tables of that project from section (= opal connection)
	 * @param opalConfigFile the opal configuration file
	 * @param section the opal configuration section
	 * @param project the opal project
	 * 
	 * @return a list of opal tables; an empty one at least
	 * @throws REXPMismatchException on expression errors
	 * @throws REngineException on R engine errors
	 * @throws IOException on io errors
	 */
	public List<String> getOpalTables(Object opalConfigFile, String section, String project)
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		StringBuilder buf = new StringBuilder();
		buf.append("try(getTables(");
		if (opalConfigFile != null) {
			buf.append("config_file='").append(opalConfigFile).append("', ");
		}
		buf.append("config_section='").append(section);
		buf.append("', project='").append(project).append("'))");
		LOGGER.debug("calling R with {}", buf.toString());
		REXP result = connection.parseAndEval(buf.toString());
		if (!result.isVector()) {
			throw new IOException(result.asString());
		}
		String[] res = result.asStrings();
		List<String> list = Arrays.asList(res);
		return list;
	}

	/**
	 * import from opal as definied in the opal bean
	 * 
	 * @param opalConfigFile the opal configuration file
	 * @param opalBean the import information
	 * @throws REXPMismatchException on expression errors
	 * @throws REngineException on R engine errors
	 * @throws IOException on io errors
	 */
	public void importFrom(Object opalConfigFile, OpalBean opalBean) throws IOException, REngineException, REXPMismatchException {
		StringBuilder buf = new StringBuilder();
		buf.append("try({");
		buf.append("repository <- opalRepository(");
		if (opalConfigFile != null) {
			buf.append("config_file='").append(opalConfigFile).append("', ");
		}
		buf.append("config_section='").append(opalBean.getUrl()).append("', ");
		buf.append("project='").append(opalBean.getProject()).append("', ");
		buf.append("table='").append(opalBean.getTable()).append("')\n");
		buf.append("library(squarerepositoryspec)\n");
		buf.append("dd <- getDD(repository = repository)\n"); // for prod, see SquareControl2Gateway
		buf.append("library(sq2psqlRepository)\n");
		String conffile = getFacesContext().getParam("squaredbconnectorfile", "/etc/squaredbconnector.properties");
		String destsect = getFacesContext().getParam("squaredbconnectorsection", "prod");
		String username = getFacesContext().getUsername();
		buf.append("repository <- sq2psqlRepository(");
		if (conffile != null) {
			buf.append("config_filename = '").append(conffile).append("', ");
		}
		buf.append("config_section = '").append(destsect).append("', report_id = NA, ignore_report = TRUE)\n");
		buf.append("res <- writeDD(repository = repository, dd = dd, owner = '").append(username).append("', parent_unique_name='").append(opalBean.getParentUniqueName()).append("')\n");
		buf.append("res})");
		LOGGER.debug("calling R with {}", buf.toString());
		RConnection connection = loadLibrary();
		REXP rexp = connection.parseAndEval(buf.toString());
		if (rexp != null && rexp.isString()) {
			getFacesContext().notifyInformation(rexp.asString());
		}
	}

	@Override
	public void close() throws Exception {
		getConnection().close();
	}
}
