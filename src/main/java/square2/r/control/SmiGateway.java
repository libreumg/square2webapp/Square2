package square2.r.control;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REXPRaw;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RFileOutputStream;
import org.rosuda.REngine.Rserve.RserveException;

import square2.help.SquareFacesContext;
import square2.modules.model.report.JsonMatrixContainerBean;

/**
 * 
 * @author henkej
 *
 */
public class SmiGateway extends RGatewayInterface {
	private static final Logger LOGGER = LogManager.getLogger(SmiGateway.class);
	/**
	 * the expected R package version
	 */
	public static final String VERSION = "1.4.7";
	/**
	 * then name of the R library
	 */
	public static final String LIBNAME = "squaremetadatainterface";

	/**
	 * @param facesContext the faces context
	 */
	public SmiGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public RConnection loadLibrary() throws RserveException, IOException {
		try {
			return loadLibrary(LIBNAME);
		} catch (RserveException | IOException e) {
			throw new IOException("could not load library " + LIBNAME);
		}
	}

	/**
	 * @return all available types that squaremetadatainterface provides
	 * @throws IOException on io errors
	 * @throws RserveException on R errors
	 */
	public List<String> getSmitypes() throws IOException, RserveException {
		List<String> list = new ArrayList<>();
		RConnection connection = loadLibrary();
		try {
			REXP rexp = connection.parseAndEval("get_filetypes()");
			String[] types = rexp.asStrings();
			for (String type : types) {
				list.add(type);
			}
		} catch (REXPMismatchException | REngineException e) {
			getFacesContext().notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * get all process variables of the underlying elements
	 * 
	 * @param parentId the id of the parent element
	 * @return a byte array containing a ods file, at least one with all the
	 * headlines
	 * @throws IOException on io errors
	 * @throws REXPMismatchException on R errors
	 * @throws REngineException on R connection errors
	 */
	public byte[] getProcessVariables(Integer parentId) throws IOException, REngineException, REXPMismatchException {
		return getTemplate("auxilliaryvariable", parentId, false);
	}

	/**
	 * call the template function
	 * 
	 * @param fktnSuffix the suffix, e.g. auxilliaryvariable or variableattribute
	 * @param parentId the id of the parent element
	 * @param includeSections if true, include sections from parentId down to the
	 * leafs
	 * @return a byte array
	 * @throws IOException on io errors
	 * @throws REngineException on R error
	 * @throws REXPMismatchException on R connection errors
	 */
	private byte[] getTemplate(String fktnSuffix, Integer parentId, Boolean includeSections)
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		String config = super.getConfig();
		String section = super.getSection();
		StringBuilder buf = new StringBuilder("try(");
		buf.append("get_template(\"");
		buf.append(fktnSuffix);
		buf.append("\", ");
		buf.append(parentId);
		if (includeSections) {
			buf.append(", include_sections = TRUE");
		}
		if (config != null && !config.isBlank()) {
			buf.append(", config = \"").append(config).append("\"");
		}
		buf.append(", section = \"").append(section).append("\"))");
		LOGGER.debug(buf.toString());
		REXP resp = connection.parseAndEval(buf.toString());
		if (resp == null) {
			throw new REXPMismatchException(resp, "return value is null");
		} else if (resp.isRaw()) {
			REXPRaw raw = (REXPRaw) resp;
			return raw.asBytes();
		} else if (resp.isString()) {
			throw new IOException(resp.asString());
		} else {
			throw new REXPMismatchException(resp,
					"return value is not of type raw but of class " + resp.getClass().getSimpleName());
		}
	}

	/**
	 * get all variable attributes of the underlying elements
	 * 
	 * @param parentId the id of the parent element
	 * @param includeSections if true, include the sections of the tree also
	 * @return a byte array containing a xlsx file, at least one with all the
	 * headlines
	 * @throws IOException on io errors
	 * @throws REngineException on R errors
	 * @throws REXPMismatchException on R connection errors
	 */
	public byte[] getVariableAttributes(Integer parentId, boolean includeSections)
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		String config = super.getConfig();
		String section = super.getSection();
		StringBuilder buf = new StringBuilder("try(");
		buf.append("download_xls(\"variableattribute\", ");
		buf.append(parentId);
		if (includeSections) {
			buf.append(", include_sections = TRUE");
		}
		if (config != null && !config.isBlank()) {
			buf.append(", config = \"").append(config).append("\"");
		}
		buf.append(", section = \"").append(section).append("\"))");
		LOGGER.debug(buf.toString());
		REXP resp = connection.parseAndEval(buf.toString());
		if (resp == null) {
			throw new REXPMismatchException(resp, "return value is null");
		} else if (resp.isRaw()) {
			REXPRaw raw = (REXPRaw) resp;
			return raw.asBytes();
		} else if (resp.isString()) {
			throw new IOException(resp.asString());
		} else {
			throw new REXPMismatchException(resp,
					"return value is not of type raw but of class " + resp.getClass().getSimpleName());
		}
	}

	/**
	 * get all variable attributes of the variable group elements
	 * 
	 * @param variablegroupId the id of the variable group
	 * @return a byte array containing a ods file, at least one with all the
	 * headlines
	 * @throws IOException on io errors
	 * @throws REngineException on R errors
	 * @throws REXPMismatchException on R connection errors
	 */
	public byte[] getVariableAttributesOfVariablegroup(Integer variablegroupId)
			throws IOException, REngineException, REXPMismatchException {
		return getTemplate("variablegroup", variablegroupId, false); // includeSections is not yet implemented in SMI
	}

	/**
	 * execute squaremetadatainterface::run(fkIostack)
	 * 
	 * @param fkIostack the pk of t_iostack
	 * @return a list of RMessages (the result of each statement)
	 * @throws RserveException on r connection errors
	 * @throws IOException on io errors
	 */
	public List<RMessage> runSmi(Integer fkIostack) throws RserveException, IOException {
		List<String> list = new ArrayList<>();
		list.add("library(" + LIBNAME + ")");
		String config = super.getConfig();
		String section = super.getSection();
		StringBuilder buf = new StringBuilder();
		buf.append("try(run(").append(fkIostack);
		if (config != null && !config.isBlank()) {
			buf.append(", config = \"").append(config).append("\"");
		}
		buf.append(", section = \"").append(section).append("\"))");
		list.add(buf.toString());
		RRunnable runnable = new RRunnable(getConnection(), list, null,
				new ExceptionRaiserForFacesMessage(getFacesContext()));
		return runnable.runAndReturnProtocol();
	}

	/**
	 * @param functionId the id of the function
	 * @return the loaded function
	 * @throws REXPMismatchException on result errors
	 * @throws REngineException on connection errors
	 * @throws IOException on io errors
	 */
	public String exportRfunction(Integer functionId) throws REXPMismatchException, REngineException, IOException {
		RConnection connection = loadLibrary();
		String config = super.getConfig();
		String section = super.getSection();
		StringBuilder buf = new StringBuilder();
		buf.append("try(export_rfunction(");
		buf.append(functionId);
		if (config != null && !config.isBlank()) {
			buf.append(", config = \"").append(config).append("\"");
		}
		buf.append(", section = \"").append(section).append("\"))");
		LOGGER.debug(buf.toString());
		REXP resp = connection.parseAndEval(buf.toString());
		if (resp == null) {
			throw new REXPMismatchException(resp, "return value is null");
		} else if (resp.isString()) {
			return resp.asString();
		} else {
			throw new REXPMismatchException(resp,
					"return value is not of type string but of class " + resp.getClass().getSimpleName());
		}
	}

	/**
	 * export an R function as an RData object to upload it elsewhere
	 * 
	 * @param functionId the id of the function
	 * @return the bytes of the RData file
	 * @throws IOException on io errors
	 * @throws REXPMismatchException on result errors
	 * @throws REngineException on connection errors
	 */
	public byte[] exportRfunctionAsRData(Integer functionId) throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		String config = super.getConfig();
		String section = super.getSection();
		StringBuilder buf = new StringBuilder();
		buf.append("try(db_select_function(pk_function=");
		buf.append(functionId);
		if (config != null && !config.isBlank()) {
			buf.append(", config = \"").append(config).append("\"");
		}
		buf.append(", section = \"").append(section).append("\"))");
		LOGGER.debug(buf.toString());
		REXP resp = connection.parseAndEval(buf.toString());
		if (resp == null) {
			throw new REXPMismatchException(resp, "return value is null");
		} else if (resp.isRaw()) {
			byte[] result = resp.asBytes();
			return result;
		} else if (resp.isString()) {
			throw new IOException(resp.asString());
		} else {
			throw new REXPMismatchException(resp,
					"return value is not of type String but of class " + resp.getClass().getSimpleName());
		}
	}

	/**
	 * import an R function given as an RData object to the database
	 * 
	 * @param is the input stream containing the bytes of the RData file
	 * @return the new id of the function
	 * @throws IOException on io errors
	 * @throws REXPMismatchException on result errors
	 * @throws REngineException on connection errors
	 */
	public Integer importRfunctionFromRData(InputStream is) throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();

		BufferedInputStream bis = new BufferedInputStream(is);
		String tmpFileName = "my_file.RData";
		RFileOutputStream rfos = connection.createFile(tmpFileName);
		byte[] b = new byte[8192];
		int c = bis.read(b);
		while (c >= 0) {
			rfos.write(b, 0, c);
			c = bis.read(b);
		}
		rfos.close();
		bis.close();
		String config = super.getConfig();
		String section = super.getSection();
		StringBuilder buf = new StringBuilder();
		buf.append("library(readr);");
		buf.append("try(db_insert_function(filename=\"").append(tmpFileName).append("\"");
		if (config != null && !config.isBlank()) {
			buf.append(", config = \"").append(config).append("\"");
		}
		buf.append(", section=\"").append(section).append("\", debug_mode = TRUE))");
		LOGGER.debug(buf.toString());
		REXP resp = connection.parseAndEval(buf.toString());
		connection.close(); // force to commit
		if (resp == null) {
			throw new REXPMismatchException(resp, "return value is null");
		} else if (resp.isInteger()) {
			return resp.asInteger();
		} else if (resp.isString()) {
			throw new IOException(resp.asString());
		} else {
			throw new REXPMismatchException(resp,
					"return value is not of type integer but of class " + resp.getClass().getSimpleName());
		}
	}

	/**
	 * get the jsonmatrix of the report with id = reportId
	 * 
	 * @param reportId the id of the report
	 * @return the jsonmatrix
	 * @throws IOException on io errors
	 * @throws REXPMismatchException on result errors
	 * @throws REngineException on connection errors
	 */
	public JsonMatrixContainerBean getJsonMatrix(Integer reportId)
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		String config = super.getConfig();
		String section = super.getSection();
		StringBuilder buf = new StringBuilder();
		buf.append("try(get_jsonmatrix(");
		buf.append(reportId);
		if (config != null && !config.isBlank()) {
			buf.append(", config = \"").append(config).append("\"");
		}
		buf.append(", section = \"").append(section).append("\"");
//    buf.append(", pretty = TRUE"); // only for debugging
		buf.append("))");
		LOGGER.debug(buf.toString());
		REXP resp = connection.parseAndEval(buf.toString());
		if (resp == null) {
			throw new REXPMismatchException(resp, "return value is null");
		} else if (resp.isString()) {
			String jsonMatrix = resp.asString();
			if (jsonMatrix.startsWith("{")) {
				return new JsonMatrixContainerBean(jsonMatrix);
			} else {
				throw new IOException("invalid json string: " + jsonMatrix);
			}
		} else {
			throw new REXPMismatchException(resp,
					"return value is not of type String but of class " + resp.getClass().getSimpleName());
		}
	}

	@Override
	public void close() throws Exception {
		getConnection().close();
	}
}
