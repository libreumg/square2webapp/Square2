package square2.r.control;

import java.io.InputStream;

/**
 * 
 * @author henkej
 *
 */
public class ExternalDataFile {
	private final String name;
	private InputStream inputStream;

	/**
	 * generate new externa data file
	 * 
	 * @param name
	 *          name of the file on the R server
	 * @param inputStream
	 *          content of the file on the R server
	 */
	public ExternalDataFile(String name, InputStream inputStream) {
		super();
		this.name = name;
		this.inputStream = inputStream;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the inputStream
	 */
	public InputStream getInputStream() {
		return inputStream;
	}
}
