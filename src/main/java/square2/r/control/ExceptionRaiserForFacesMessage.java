package square2.r.control;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * 
 * @author henkej
 *
 */
public class ExceptionRaiserForFacesMessage implements ExceptionRaiserInterface {
	private FacesContext context;

	/**
	 * generate new exception raiser for faces message
	 * 
	 * @param context
	 *          the context of this function call
	 */
	public ExceptionRaiserForFacesMessage(FacesContext context) {
		this.context = context;
	}

	@Override
	public void raise(Exception e, Object... attachment) {
		StringBuilder buf = new StringBuilder();
		buf.append(e.getMessage());
		for (Object o : attachment) {
			buf.append(o);
		}
		context.addMessage(null,
				new FacesMessage(FacesMessage.SEVERITY_ERROR, "error on executing R code", buf.toString()));
	}
}
