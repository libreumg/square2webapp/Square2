package square2.r.control;

import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;

/**
 * 
 * @author henkej
 * 
 */
public class RControllerException extends Exception {
	private static final long serialVersionUID = 6129125788444483984L;
	private final String request;

	/**
	 * create new exception
	 * 
	 * @param e
	 *          the exception to be wraped
	 * @param request
	 *          the request of this exception
	 */
	public RControllerException(REngineException e, String request) {
		super(e);
		this.request = request;
	}

	/**
	 * create new exception
	 * 
	 * @param e
	 *          the exception to be wraped
	 * @param request
	 *          the request of this exception
	 */
	public RControllerException(REXPMismatchException e, String request) {
		super(e);
		this.request = request;
	}

	/**
	 * @return the request
	 */
	public String getRequest() {
		return request;
	}
}
