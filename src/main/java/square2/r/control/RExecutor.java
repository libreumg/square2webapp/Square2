package square2.r.control;

import java.io.IOException;

import javax.servlet.ServletContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import square2.help.SquareFacesContext;

/**
 * 
 * @author henkej
 *
 */
public class RExecutor {
	private static final Logger LOGGER = LogManager.getLogger(RExecutor.class);
	private final RConnection connection;

	/**
	 * executor for R scripts
	 * 
	 * @param facesContext
	 *          the context of the function call
	 * @throws RserveException
	 *           for R serve errors
	 * @throws IOException
	 *           on io errors
	 */
	public RExecutor(SquareFacesContext facesContext) throws RserveException, IOException {
		super();
		this.connection = getRConnection(facesContext);
	}
	
	/**
	 * executor for R scripts
	 * 
	 * @param ctx
	 *          the context of the servlet
	 * @throws RserveException
	 *           for R serve errors
	 * @throws IOException
	 *           on io errors
	 */
	public RExecutor(ServletContext ctx) throws RserveException, IOException {
		super();
		this.connection = getRConnection(ctx);
	}

	/**
	 * create new connection to R server
	 * 
	 * @return connection to R server
	 * @throws DataAccessException
	 *           if anything went wrong on database side
	 * @throws NumberFormatException
	 *           for wrong numbers
	 * @throws RserveException
	 *           for R serve errors
	 * @throws IOException
	 *           on io errors
	 */
	private RConnection getRConnection(SquareFacesContext facesContext) throws RserveException, IOException {
		return ConnectionManager.getConnection(facesContext);
	}
	
	/**
	 * create new connection to R server
	 * 
	 * @return connection to R server
	 * @throws DataAccessException
	 *           if anything went wrong on database side
	 * @throws NumberFormatException
	 *           for wrong numbers
	 * @throws RserveException
	 *           for R serve errors
	 * @throws IOException
	 *           on io errors
	 */
	private RConnection getRConnection(ServletContext ctx) throws RserveException, IOException {
		return ConnectionManager.getConnection(ctx);
	}

	/**
	 * execute command encapsulated by try and eval
	 * 
	 * @param command
	 *          to be executed
	 * @return R object of the last line
	 * @throws REXPMismatchException
	 *           for R export errors
	 * @throws REngineException
	 *           for R engine errors
	 */
	public REXP runTryEval(String command) throws REXPMismatchException, REngineException {
		String rcode = new StringBuilder("try(eval(").append(command).append("),silent=TRUE)").toString();
		LOGGER.debug(rcode);
		REXP res = connection.parseAndEval(rcode);
		if (res.inherits("try-error")) {
			throw new REXPMismatchException(res, res.asString());
		}
		return res;
	}

	/**
	 * get connection to R server
	 * 
	 * @return the R server connection
	 */
	public RConnection getConnection() {
		return connection;
	}
}
