package square2.r.control;

import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPLogical;

/**
 * 
 * @author henkej
 *
 */
public class RMessage {
	/**
	 * info
	 */
	public static final String LOGLEVEL_INFO = "info";
	/**
	 * warn
	 */
	public static final String LOGLEVEL_WARN = "warn";
	/**
	 * error
	 */
	public static final String LOGLEVEL_ERROR = "error";

	private final String loglevel;
	private final String message;

	/**
	 * generate new R message
	 * 
	 * @param loglevel
	 *          the loglevel
	 * @param message
	 *          the message
	 */
	public RMessage(String loglevel, String message) {
		super();
		this.loglevel = loglevel;
		this.message = message;
	}

	/**
	 * generate new R message
	 * 
	 * @param loglevel
	 *          the loglevel
	 * @param rexp
	 *          the result
	 */
	public RMessage(String loglevel, REXP rexp) {
		super();
		this.loglevel = loglevel;
		String msg = "";
		try {
			if (rexp.isNull()) {
				msg = "NULL";
			} else if (rexp.isString()) {
				msg = rexp.asString();
			} else if (rexp.isInteger()) {
				msg = Integer.valueOf(rexp.asInteger()).toString();
			} else if (rexp.isLogical()) {
				REXPLogical logical = (REXPLogical) rexp;
				StringBuilder buf = new StringBuilder();
				for (boolean b : logical.isTRUE()) {
					buf.append(b);
				}
				msg = buf.toString();
			} else {
				msg = "no support for " + rexp.getClass().getSimpleName() + " yet, sorry";
			}
		} catch (Exception e) {
			msg = e.getMessage();
		}
		this.message = msg;
	}

	/**
	 * @return the loglevel
	 */
	public String getLoglevel() {
		return loglevel;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
}
