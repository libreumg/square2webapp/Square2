package square2.r.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.Part;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.JSONB;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

import square2.help.ContextKey;
import square2.help.SquareFacesContext;
import square2.modules.model.report.MarkdownDownloadBean;
import square2.modules.model.report.ReportStatsBean;
import square2.modules.model.report.factory.JsonDataFileTypeBean;
import square2.r.control.cmdbuilder.Argument;
import square2.r.control.cmdbuilder.BooleanArgument;
import square2.r.control.cmdbuilder.FunctionArgument;
import square2.r.control.cmdbuilder.RArgument;
import square2.r.control.cmdbuilder.RCommandBuilder;
import square2.r.control.cmdbuilder.StringArgument;
import square2.r.control.config.ConfigurationInterface;
import square2.r.control.config.SquareReportRendererConfiguration;
import square2.r.patch.NamedValuesetBean;
import square2.r.patch.RosudaPatcher;

/**
 * 
 * @author henkej
 *
 */
public class SquareControl2Gateway extends RGatewayInterface {
	private static final Logger LOGGER = LogManager.getLogger(SquareControl2Gateway.class);
	/**
	 * the R library name
	 */
	public static final String LIBNAME = "SquareControl2";
	/**
	 * minimal version
	 */
	public static final String VERSION = "1.3.12";

	/**
	 * create a new square control gateway
	 * 
	 * @param facesContext the faces context
	 */
	public SquareControl2Gateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	/**
	 * load libary from servlet context
	 * 
	 * @param ctx the servlet context
	 * @return the connection
	 * @throws IOException on io exceptions
	 */
	public static final RConnection loadLibrary(ServletContext ctx) throws IOException {
		RConnection connection;
		try {
			connection = getConnection(ctx);
		} catch (RserveException e) {
			LOGGER.error(e.getMessage(), e);
			return null;
		}
		REXP rexp;
		try {
			rexp = connection.parseAndEval("try(library(" + LIBNAME + "))");
			if (!rexp.isString() || !LIBNAME.equals(rexp.asString())) {
				throw new IOException("loaded library " + LIBNAME + " and got " + rexp.asString());
			} else {
				LOGGER.debug("successfully loaded library {}", LIBNAME);
			}
		} catch (REngineException | REXPMismatchException e) {
			LOGGER.error(e.getMessage(), e);
			return connection;
		}
		return connection;
	}

	@Override
	public RConnection loadLibrary() throws IOException {
		RConnection connection;
		try {
			connection = getConnection();
		} catch (RserveException e) {
			LOGGER.error(e.getMessage(), e);
			getFacesContext().notifyException(e);
			return null;
		}
		REXP rexp;
		try {
			rexp = connection.parseAndEval("try(library(" + LIBNAME + "))");
			if (!rexp.isString() || !LIBNAME.equals(rexp.asString())) {
				throw new IOException("loaded library " + LIBNAME + " and got " + rexp.asString());
			} else {
				LOGGER.debug("successfully loaded library {}", LIBNAME);
			}
		} catch (REngineException | REXPMismatchException e) {
			LOGGER.error(e.getMessage(), e);
			getFacesContext().notifyException(e);
			return connection;
		}
		return connection;
	}

	/**
	 * call the function print on the R server
	 * 
	 * @param configFile the config file
	 * @return the result
	 * @throws REXPMismatchException on expression errors
	 * @throws REngineException on R engine errors
	 * @throws IOException on io errors
	 */
	public String getTest(String configFile) throws REngineException, REXPMismatchException, IOException {
		RConnection connection = loadLibrary();
		StringBuilder buf = new StringBuilder("print(\"");
		buf.append("R is up and running");
		buf.append("\")");
		REXP res = connection.parseAndEval(buf.toString());
		return res.asString();
	}

	/**
	 * get all square run editions
	 * 
	 * @param listAll if true, list all editions
	 * @param wantedImplGroups a json array of wanted implementation groups with
	 * default implementations
	 * 
	 * @return a list of square run editions; an empty one at least
	 * @throws REXPMismatchException on expression errors
	 * @throws REngineException on R engine errors
	 * @throws IOException on io errors
	 */
	public List<String> getSquareRunEditions(Boolean listAll, JSONB wantedImplGroups)
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		RCommandBuilder buf = new RCommandBuilder(false);
		buf.appendFunction("as.character", new FunctionArgument("try", new FunctionArgument("names", new FunctionArgument(
				"call_list_editions4calculation", new BooleanArgument("list_all", listAll, false),
				new StringArgument("wanted_impl_groups", wantedImplGroups == null ? null : wantedImplGroups.data(), false)))));
		
		REXP result = connection.parseAndEval(buf.toString());
		connection.detach();
		// REXP result = connection.parseAndEval("c(\"SquareControlImpl\")");
		String[] strings = result.asStrings();
		List<String> list = new ArrayList<>();
		for (String s : strings) {
			LOGGER.debug("found square run edition {}", s);
			list.add(s);
		}
		return list;
	}

	/**
	 * get all external release types
	 * 
	 * @return a list of square run editions; an empty one at least
	 * @throws REXPMismatchException on expression errors
	 * @throws REngineException on R engine errors
	 * @throws IOException on io errors
	 */
	public List<JsonDataFileTypeBean> getExternalReleaseTypes()
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		REXP result = connection.parseAndEval("try(stop(\"not yet supported\"))");
		String json = result.asString();
		List<JsonDataFileTypeBean> list = new ArrayList<>();
		for (JsonDataFileTypeBean s : parseJsonDataFileTypes(json)) {
			LOGGER.debug("found square data file type {}", s.getDescription());
			list.add(s);
		}
		return list;
	}

	/**
	 * parse json data file type
	 * 
	 * @param json a json string
	 * @return the beans from the json
	 * @throws IOException on io errors
	 */
	private List<JsonDataFileTypeBean> parseJsonDataFileTypes(String json) throws IOException {
		if (json == null || json.trim().length() < 1) {
			return new ArrayList<JsonDataFileTypeBean>();
		}
		List<JsonDataFileTypeBean> list = new ArrayList<>();
		try {
			Object jsonArray = new Gson().fromJson(json, JsonDataFileTypeBean[].class);
			if (jsonArray instanceof String) {
				throw new IOException("cannot parse " + jsonArray + " to JsonDataFileTypeBean");
			}
			JsonDataFileTypeBean[] array = (JsonDataFileTypeBean[]) jsonArray;
			for (JsonDataFileTypeBean bean : array) {
				list.add(bean);
			}
		} catch (IllegalStateException e) {
			throw new IOException(e.getMessage());
		}
		return list;
	}

	private REXP execInR(RConnection con, String command) throws REngineException, REXPMismatchException {
		LOGGER.debug("execute in R: {}", command);
		return con.parseAndEval(command);
	}

	/**
	 * call the calculation
	 * 
	 * @param squareRunEdition the square run edition
	 * @param reportconfig the configuration
	 * @param externalReleaseFile the release file if any
	 * @throws IOException on IO errors
	 * @throws REXPMismatchException on R mismatch errors
	 * @throws REngineException on R engine errors
	 */
	public void callCalculation(String squareRunEdition, ConfigurationInterface reportconfig, Part externalReleaseFile)
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		if (externalReleaseFile.getSize() > 0) {
			String fileName = uploadFileToRSession(connection, "external.release.file", externalReleaseFile);

			JsonObject repoFile = new JsonObject();
			repoFile.addProperty(".class", "sq2filerepository");
			repoFile.addProperty("study_file_url", fileName);
			repoFile.addProperty("study_file_type", "xlsx");
			repoFile.addProperty("delete_on_exit", true);

			reportconfig.updateRepository(true, ".SquareStudydataLoader", "REPOFILE", repoFile);
		}
		execInR(connection, "options(lifecycle_disable_warnings = TRUE)"); // makes dplyr warnings invisible
		execInR(connection, "asyncenv <- call_create_async_env(); invisible(NULL)");

		RCommandBuilder buf = new RCommandBuilder(true);
		buf.appendFunction("call_calculation2env", new RArgument("asyncenv", "asyncenv"),
				new StringArgument("edition", squareRunEdition),
				new StringArgument("reportconfig", reportconfig.getConfiguration()));
		REXP res = execInR(connection, buf.toString());
		if (!res.isNull()) {
			LOGGER.error("error on executing {}: {}", buf.toString(), res.asString());
			throw new IOException(res.asString());
		} else {
			buf = new RCommandBuilder(true);
			buf.appendFunction("call_calculation_exec", new RArgument("asyncenv", "asyncenv"),
					new StringArgument("reportconfig", reportconfig.getConfiguration()),
					new BooleanArgument("admin", getFacesContext().getProfile().isAdmin(), false),
					new StringArgument("format", "squarereportrenderer::flex_site_board"),
					new BooleanArgument("standalone", false, false));
			LOGGER.debug("execute in R: {}", buf.toString());
			connection.voidEvalDetach(buf.toString());
			LOGGER.debug("detached R process");
		}
	}

	/**
	 * call all reports from R
	 * 
	 * @param squareRunEdition the square run edition
	 * @param reportconfig the configuration
	 * 
	 * @return list of found report file names; an empty list at least
	 * 
	 * @throws IOException on IO errors
	 * @throws REXPMismatchException on R mismatch errors
	 * @throws REngineException on R engine errors
	 */
	public List<String> callReports(String squareRunEdition, ConfigurationInterface reportconfig)
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		RCommandBuilder buf = new RCommandBuilder(true);
		buf.appendFunction("call_reports", new StringArgument("reportconfig", reportconfig.getConfiguration()),
				new StringArgument("render_args", null, false),
				new BooleanArgument("admin", getFacesContext().getProfile().isAdmin(), false));
		REXP res = execInR(connection, buf.toString());
		if (res.isNull()) { // this is the standard result if no result has been found
			return new ArrayList<>();
		} else if (!res.isVector()) {
			LOGGER.error("{} did not return a vector, but a {}", buf.toString(), res.getClass().getSimpleName());
			return new ArrayList<>();
		} else {
			LOGGER.debug("result is {}", res.asStrings().toString());
			String[] fileNames = res.asStrings();
			return Arrays.asList(fileNames);
		}
	}

	/**
	 * call a report from R
	 * 
	 * @param ctx servlet context to setup everything without faces context
	 * @param squareRunEdition the square run edition
	 * @param reportconfig the configuration
	 * @param relPath the relative path to the file
	 * 
	 * @return the report
	 * 
	 * @throws IOException on IO errors
	 * @throws REXPMismatchException on R mismatch errors
	 * @throws REngineException on R engine errors
	 */
	public static final byte[] callReport(ServletContext ctx, String squareRunEdition,
			ConfigurationInterface reportconfig, String relPath) throws IOException, REngineException, REXPMismatchException {
		RConnection connection = SquareControl2Gateway.loadLibrary(ctx);
		RCommandBuilder buf = new RCommandBuilder(true);
		buf.appendFunction("call_report", new StringArgument("reportconfig", reportconfig.getConfiguration()),
				new StringArgument("rel_path", relPath.replaceFirst("^/", ""), true), new BooleanArgument("admin", false));
		try (SquareControl2Gateway gw = new SquareControl2Gateway(null)) {
			REXP res = gw.execInR(connection, buf.toString());
			if (!res.isRaw()) {
				LOGGER.error("res is not raw, but {}: {}", res.getClass().getSimpleName(), res.toDebugString());
				throw new IOException("invalid result");
			}
			return res.asBytes();
		} catch (Exception e) {
			throw new IOException(e);
		}
	}

	/**
	 * @param bean the download information
	 * @throws REXPMismatchException on result errors
	 * @throws REngineException on connection errors
	 * @throws IOException on io errors
	 */
	public void squareRender(MarkdownDownloadBean bean) throws REXPMismatchException, REngineException, IOException {
		Object outputDir = getFacesContext().getApplicationMapValue(ContextKey.SC_OUTPUT_PATH);
		String outputPath = outputDir == null ? null : outputDir.toString();
		ConfigurationInterface reportconfig = new SquareReportRendererConfiguration(super.getConfig(), super.getSection(),
				bean.getFkReport(), outputPath);
		RCommandBuilder cmd = new RCommandBuilder(true);
		List<Argument> args = new ArrayList<>();
		args.add(new StringArgument("reportconfig", reportconfig.getConfiguration()));
		args.add(new StringArgument("format", bean.getFormat().getFormat()));
		args.add(new BooleanArgument("read_from_db", true)); // always render again
		args.add(new BooleanArgument("return_file_path", bean.getFilePath()));
		args.add(new BooleanArgument("debug", bean.getDebug()));
//		args.add(new BooleanArgument("show_errors", bean.getShowErrors()));
		args.add(new BooleanArgument("admin", getFacesContext().getProfile().isAdmin()));
		args.add(new StringArgument("output_dir", outputPath, true));
		args.add(new BooleanArgument("standalone", false)); // TODO: make the admin change this in the GUI
		cmd.appendFunction("call_renderer", args.toArray(new Argument[] {}));
		LOGGER.debug(cmd.toString());
		RConnection connection = loadLibrary();
		connection.voidEvalDetach(cmd.toString());
	}

	/**
	 * get the statistics of the run for a report
	 * 
	 * @param conf the configuration
	 * 
	 * @return the statistics
	 * @throws IOException on IO errors
	 * @throws REXPMismatchException on R mismatch exceptions
	 * @throws REngineException on R engine exceptions
	 */
	public ReportStatsBean getStats(ConfigurationInterface conf)
			throws IOException, REngineException, REXPMismatchException {
		RCommandBuilder cmd = new RCommandBuilder(true);
		cmd.appendFunction("call_progress", new StringArgument("reportconfig", conf.getConfiguration()));
		LOGGER.debug(cmd.toString());
		RConnection connection = loadLibrary();
		REXP df = connection.parseAndEval(cmd.toString());

		if (!df.isList()) {
			throw new IOException(df.toDebugString());
		} else {
			RList l = df.asList();
			List<NamedValuesetBean> mapList = new RosudaPatcher().getListOfMapsFromRlist(l);
			List<ReportStatsBean> list = new ArrayList<>();
			for (NamedValuesetBean entry : mapList) {
				Integer rid = entry.getInteger("report_id");
				Double progress = entry.getDouble("progress");
				String message = entry.getString("message");
				Boolean cancelled = entry.getBoolean("cancelled");
				ReportStatsBean bean = new ReportStatsBean(rid, progress, message, cancelled);
				list.add(bean);
			}
			return list.get(0); // use first result, all other are crap
		}
	}

	@Override
	public void close() throws Exception {
		getConnection().close();
	}
}
