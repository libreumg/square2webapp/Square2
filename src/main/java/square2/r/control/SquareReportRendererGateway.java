package square2.r.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.RList;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import square2.help.ContextKey;
import square2.help.SquareFacesContext;
import square2.modules.model.report.MarkdownDownloadBean;
import square2.modules.model.report.SquareReportRendererFormatBean;
import square2.r.control.cmdbuilder.Argument;
import square2.r.control.cmdbuilder.BooleanArgument;
import square2.r.control.cmdbuilder.RCommandBuilder;
import square2.r.control.cmdbuilder.StringArgument;
import square2.r.control.config.ConfigurationInterface;
import square2.r.control.config.SquareReportRendererConfiguration;

/**
 * 
 * @author henkej
 *
 */
public class SquareReportRendererGateway extends RGatewayInterface {
	private static final Logger LOGGER = LogManager.getLogger(SquareReportRendererGateway.class);
	/**
	 * the expected R package version
	 */
	public static final String VERSION = "2.4.2.9006";
	/**
	 * the R library name
	 */
	public static final String LIBNAME = "squarereportrenderer";

	/**
	 * @param facesContext the faces context
	 */
	public SquareReportRendererGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public RConnection loadLibrary() throws RserveException, IOException {
		try {
			return loadLibrary(LIBNAME);
		} catch (RserveException | IOException e) {
			throw new IOException("could not load library " + LIBNAME);
		}
	}

	/**
	 * @return all available types that squaremetadatainterface provides
	 * @throws IOException on io errors
	 * @throws RserveException on R errors
	 */
	public List<SquareReportRendererFormatBean> getFormats() throws IOException, RserveException {
		List<SquareReportRendererFormatBean> list = new ArrayList<>();
		RConnection connection = loadLibrary();
		try {
			RCommandBuilder rcmd = new RCommandBuilder(true);
			Argument[] args = new Argument[] { new BooleanArgument("extended", true),
					new BooleanArgument("admin", getFacesContext().getProfile().isAdmin()) };
			rcmd.appendFunction("call_render_formats", args);
			REXP rexp = connection.parseAndEval(rcmd.toString());
			if (rexp.isList()) {
				RList rlist = rexp.asList();
				String[] labels = rlist.at("label").asStrings();
				String[] formats = rlist.at("format").asStrings();
				String[] mimeTypes = rlist.at("mime_type").asStrings();
				String[] fileExtensions = rlist.at("file_extension").asStrings();
				for (int i = 0; i < formats.length; i++) {
					list.add(new SquareReportRendererFormatBean(labels[i], formats[i], mimeTypes[i], fileExtensions[i]));
				}
			} else if (rexp.isString()) {
				throw new REXPMismatchException(rexp, rexp.asString());
			} else {
				throw new REXPMismatchException(rexp, "is not the expected data frame (a list of vectors indeed)");
			}
		} catch (REXPMismatchException | REngineException e) {
			getFacesContext().notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * @param bean the download information
	 * @return the bytes
	 * @throws REXPMismatchException on result errors
	 * @throws REngineException on connection errors
	 * @throws IOException on io errors
	 * 
	 * @deprecated see SquareControl2Gateway.squareRenderer
	 */
	@Deprecated
	public byte[] squareRender(MarkdownDownloadBean bean) throws REXPMismatchException, REngineException, IOException {
		Object outputPath = getFacesContext().getApplicationMapValue(ContextKey.SR_OUTPUT_PATH);
		ConfigurationInterface reportconfig = new SquareReportRendererConfiguration(super.getConfig(), super.getSection(),
				bean.getFkReport(), outputPath == null ? "" : outputPath.toString());
		RCommandBuilder cmd = new RCommandBuilder(true);
		List<Argument> args = new ArrayList<>();
		args.add(new StringArgument("reportconfig", reportconfig.getConfiguration()));
		args.add(new StringArgument("format", bean.getFormat().getFormat()));
		args.add(new BooleanArgument("read_from_db", true)); // always render again
		args.add(new BooleanArgument("return_file_path", bean.getFilePath()));
		args.add(new BooleanArgument("debug", bean.getDebug()));
//		args.add(new BooleanArgument("show_errors", bean.getShowErrors()));
		args.add(new BooleanArgument("admin", getFacesContext().getProfile().isAdmin()));
		args.add(new BooleanArgument("standalone", false)); // TODO: make the admin change this in the GUI
		cmd.appendFunction("call_renderer", args.toArray(new Argument[] {}));
		LOGGER.debug(cmd.toString());
		RConnection connection = loadLibrary();
		// TODO: only start asynchronously, return nothing
		REXP resp = connection.parseAndEval(cmd.toString());
		if (resp == null) {
			throw new REXPMismatchException(resp, "return value is null");
		} else if (resp.isRaw()) {
			return resp.asBytes();
		} else if (resp.isString()) {
			LOGGER.warn(resp.asString() + ", call was " + cmd.toString());
			throw new IOException(resp.asString());
		} else {
			throw new REXPMismatchException(resp,
					"return value is not of type raw but of class " + resp.getClass().getSimpleName());
		}
	}

	/**
	 * get a property from the library
	 * 
	 * @param key the property key
	 * @return the value as json string
	 * @throws REXPMismatchException on result errors
	 * @throws REngineException on connection errors
	 * @throws IOException on io errors
	 */
	public String getRendererProperty(String key) throws IOException, REngineException, REXPMismatchException {
		RCommandBuilder cmd = new RCommandBuilder(true);
		cmd.appendFunction("call_get_key_value", new StringArgument("key", key));
		RConnection connection = loadLibrary();
		REXP resp = connection.parseAndEval(cmd.toString());
		if (resp == null) {
			return null;
		} else if (resp.isString()) {
			return resp.asString();
		} else {
			throw new REXPMismatchException(resp,
					"return value is not of type String but of class " + resp.getClass().getSimpleName());
		}
	}

	@Override
	public void close() throws Exception {
		getConnection().close();
	}
}
