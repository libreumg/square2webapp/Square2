package square2.r.control;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import square2.help.SquareFacesContext;
import square2.r.control.cmdbuilder.RArgument;
import square2.r.control.cmdbuilder.RCommandBuilder;
import square2.r.control.cmdbuilder.StringArgument;

/**
 * 
 * @author henkej
 *
 */
public class FunctionRegistratorGateway extends RGatewayInterface {
	private static final Logger LOGGER = LogManager.getLogger(FunctionRegistratorGateway.class);
	/**
	 * the R library name
	 */
	public static final String LIBNAME = "functionregistrator";
	/**
	 * minimal version
	 */
	public static final String VERSION = "0.1.8";

	/**
	 * create a new square control gateway
	 * 
	 * @param facesContext the faces context
	 */
	public FunctionRegistratorGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public RConnection loadLibrary() throws IOException {
		RConnection connection;
		try {
			connection = getConnection();
		} catch (RserveException e) {
			LOGGER.error(e.getMessage(), e);
			getFacesContext().notifyException(e);
			return null;
		}
		REXP rexp;
		try {
			rexp = connection.parseAndEval("try(library(" + LIBNAME + "))");
			if (!rexp.isString() || !LIBNAME.equals(rexp.asString())) {
				throw new IOException("loaded library " + LIBNAME + " and got " + rexp.asString());
			} else {
				LOGGER.debug("successfully loaded library {}", LIBNAME);
			}
		} catch (REngineException | REXPMismatchException e) {
			LOGGER.error(e.getMessage(), e);
			getFacesContext().notifyException(e);
			return connection;
		}
		return connection;
	}

	/**
	 * call register_functions
	 * 
	 * @param configFile the config file
	 * @param configSection the config section
	 * @param userlist the user as an R list with the arguments forename, surname
	 *        and username, e.g. list(forename = "Hans", surname = "Wurst", username = "wursth")
	 * @param packageName the package name, e.g. dataquieR
	 * @param baseUrl the base url, e.g.
	 * https://dataquality.ship-med.uni-greifswald.de/
	 * @param isocodes the isocodes as an R vector, e.g. c("de_DE", "en")
	 * @return the log or an empty string if not yet supported
	 * @throws REXPMismatchException on R expression mismatch errors 
	 * @throws REngineException on R engine errors
	 * @throws IOException on IO errors
	 */
	public String callRegisterFunctions(String configFile, String configSection, String userlist, String packageName,
			String baseUrl, String isocodes) throws REngineException, REXPMismatchException, IOException {
		RConnection connection = loadLibrary();
		RCommandBuilder buf = new RCommandBuilder(true);
		buf.appendFunction("call_register_functions", new StringArgument("config_file", configFile, false),
				new StringArgument("config_section", configSection),
				new RArgument("user", userlist, true),
				new StringArgument("packageName", packageName, true),
				new StringArgument("base_url", baseUrl, true), 
				new RArgument("isocodes", isocodes, true));
		LOGGER.debug(buf.toString());
		REXP rexp = connection.parseAndEval(buf.toString());
		if (rexp != null) {
			if (rexp.isString()) {
				return rexp.asString();
			}
		}
		return "";
	}

	/**
	 * call rm_functions_from_db
	 * 
	 * @param configFile the config file
	 * @param configSection the config section
	 * @return the log or an empty string if not yet supported
	 * @throws IOException on io exceptions
	 * @throws REngineException on R engine exceptions
	 * @throws REXPMismatchException on R expression mismatch exceptions
	 */
	public String callRmFunctioninputtypesFromDb(String configFile, String configSection)
			throws IOException, REngineException, REXPMismatchException {
		RConnection connection = loadLibrary();
		RCommandBuilder buf = new RCommandBuilder(true);
		buf.appendFunction("call_rm_functioninputtypes_from_db", new StringArgument("config_file", configFile, false),
				new StringArgument("config_section", configSection));
		LOGGER.debug(buf.toString());
		REXP rexp = connection.parseAndEval(buf.toString());
		if (rexp != null) {
			if (rexp.isString()) {
				return rexp.asString();
			}
		}
		return "";
	}

	/**
	 * call rm_functions_from_db
	 * 
	 * @param configFile the config file
	 * @param configSection the config section
	 * @param packageName the package name, e.g. dataquieR
	 * @return the log or an empty string if not yet supported
	 * @throws REngineException on R engine exceptions
	 * @throws REXPMismatchException on R expression mismatch exceptions
	 * @throws IOException on io exceptions
	 */
	public String callRmFunctionsFromDb(String configFile, String configSection, String packageName)
			throws REngineException, REXPMismatchException, IOException {
		RConnection connection = loadLibrary();
		RCommandBuilder buf = new RCommandBuilder(true);
		buf.appendFunction("call_rm_functions_from_db", new StringArgument("config_file", configFile, false),
				new StringArgument("config_section", configSection), new StringArgument("packageName", packageName, false));
		LOGGER.debug(buf.toString());
		REXP rexp = connection.parseAndEval(buf.toString());
		if (rexp != null) {
			if (rexp.isString()) {
				return rexp.asString();
			}
		}
		return "";
	}

	@Override
	public void close() throws Exception {
		getConnection().close();
	}
}
