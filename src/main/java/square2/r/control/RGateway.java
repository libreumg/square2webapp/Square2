package square2.r.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import square2.help.SquareFacesContext;
import square2.r.model.RPackage;

/**
 * 
 * @author henkej
 *
 */
public class RGateway extends RGatewayInterface {
	private static final Logger LOGGER = LogManager.getLogger(RGateway.class);

	/**
	 * generate a new R gateway without loading a library
	 * 
	 * @param facesContext the faces context
	 */
	public RGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public RConnection loadLibrary() throws RserveException, IOException {
		return getConnection();
	}

	/**
	 * get the package version
	 * 
	 * @param packageName the name of the package; if null, return the version of R
	 * @return the requested version
	 * @throws RserveException on r connection errors
	 * @throws IOException on io errors
	 * @throws REXPMismatchException on r expression exception
	 */
	public String getVersion(String packageName) throws RserveException, IOException, REXPMismatchException {
		RConnection connection = getConnection();
		StringBuilder buf = new StringBuilder();
		if (packageName == null) {
			buf.append("R.version.string");
		} else {
			buf.append("toString(packageVersion(\"").append(packageName).append("\"))");
		}
		REXP rexp = connection.eval(buf.toString());
		if (rexp.isString()) {
			return rexp.asString();
		} else {
			throw new REXPMismatchException(rexp, "not a string");
		}
	}

	/**
	 * get all R packages from squareControl
	 * 
	 * @return a list of R packages
	 * @throws REngineException on R connection errors
	 * @throws REXPMismatchException on R result errors
	 * @throws IOException in io errors
	 */
	public List<RPackage> initRPackagePool() throws REngineException, REXPMismatchException, IOException {
		RConnection c = getConnection();
		REXP rexp = null;
		try {
			rexp = c.parseAndEval("installed.packages()");
		} catch (REngineException | REXPMismatchException e) {
			String errorMsg = c.getLastError();
			LOGGER.error(errorMsg);
			throw new IOException(errorMsg, e);
		} finally {
			c.close();
		}
		int[] dim = rexp.dim();
		if (dim.length != 2 || dim[1] != 16) {
			throw new RuntimeException(
					"Got something strange from R asking for installed.packages(). Maybe R conventions for this function changed?");
		}
		String[] o;
		try {
			o = (String[]) rexp.asNativeJavaObject();
		} catch (REXPMismatchException e) {
			getFacesContext().notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return new ArrayList<>();
		}
		List<String> a = Arrays.asList(o);
		List<String> ps = a.subList(0, dim[0]);
		List<String> vs = a.subList(2 * dim[0], 3 * dim[0]);
		List<RPackage> list = new ArrayList<>();
		for (int i = 0; i < dim[0]; i++) {
			list.add(new RPackage(ps.get(i), vs.get(i)));
		}
		return list;
	}

	@Override
	public void close() throws Exception {
		getConnection().close();
	}
}
