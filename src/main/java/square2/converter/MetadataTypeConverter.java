package square2.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import square2.modules.model.study.MetadatatypeBean;

/**
 * 
 * @author henkej
 *
 */
@FacesConverter(value = "metadataTypeConverter")
public class MetadataTypeConverter implements Converter<MetadatatypeBean> {

	@Override
	public MetadatatypeBean getAsObject(FacesContext context, UIComponent component, String value)
			throws ConverterException {
		return value == null ? null : new MetadatatypeBean(null).fromJson(value);
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, MetadatatypeBean value)
			throws ConverterException {
		return value == null ? null : value.toJson();
	}
}
