package square2.converter;

import com.google.gson.JsonSyntaxException;

/**
 * 
 * @author henkej
 * 
 * @param <E> the class of the conversion
 *
 */
public interface JsonConverter<E> {
  /**
   * convert the corresponding bean from json
   * 
   * @param json the json representation of the object
   * @return the object
   * @throws JsonSyntaxException for Gson exceptions
   */
  public JsonConverter<E> fromJson(String json) throws JsonSyntaxException;

  /**
   * convert the bean to json
   * @return the json representation
   */
  public String toJson();
}
