package square2.converter;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;

import square2.modules.model.template.report.MatrixcolumnParameterBean;

/**
 * 
 * @author henkej
 *
 */
@FacesConverter(value = "matrixcolumnparameterbean")
public class MatrixcolumnBeanConverter implements Converter<MatrixcolumnParameterBean> {

  @Override
  public MatrixcolumnParameterBean getAsObject(FacesContext context, UIComponent component, String value)
      throws ConverterException {
    return value == null ? null : new MatrixcolumnParameterBean(null, null, null, null).fromJson(value);
  }

  @Override
  public String getAsString(FacesContext context, UIComponent component, MatrixcolumnParameterBean value)
      throws ConverterException {
    return value == null ? "" : value.toJson();
  }
}
