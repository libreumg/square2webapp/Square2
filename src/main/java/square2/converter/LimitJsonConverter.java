package square2.converter;

import javax.faces.validator.ValidatorException;

import org.jooq.exception.DataAccessException;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import square2.help.LimitValidator;

/**
 * 
 * @author henkej
 *
 */
public class LimitJsonConverter {
	/**
	 * convert limits to json
	 * 
	 * @param limitSoftLow
	 *          by comparator &lt;, &gt;, &lt;= or &gt;= prepended floating point
	 *          number, separator is a dot
	 * @param limitSoftUp
	 *          by comparator &lt;, &gt;, &lt;= or &gt;= prepended floating point
	 *          number, separator is a dot
	 * @param limitHardLow
	 *          by comparator &lt;, &gt;, &lt;= or &gt;= prepended floating point
	 *          number, separator is a dot
	 * @param limitHardUp
	 *          by comparator &lt;, &gt;, &lt;= or &gt;= prepended floating point
	 *          number, separator is a dot
	 * @return json representation of limits
	 */
	public Object toJson(String limitSoftLow, String limitSoftUp, String limitHardLow, String limitHardUp) {
		validate(limitHardUp);
		validate(limitSoftUp);
		validate(limitHardLow);
		validate(limitSoftLow);
		StringBuilder buf = new StringBuilder();
		buf.append("{\"hard_up\": ");
		appendNullable(buf, limitHardUp);
		buf.append(", \"soft_up\": ");
		appendNullable(buf, limitSoftUp);
		buf.append(", \"hard_low\": ");
		appendNullable(buf, limitHardLow);
		buf.append(", \"soft_low\": ");
		appendNullable(buf, limitSoftLow);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * append null or encapsulated value
	 * 
	 * @param buf
	 *          StringBuffer to write to
	 * @param s
	 *          to be used; if null, append null only, if not null, append " + value
	 *          + "
	 */
	private void appendNullable(StringBuilder buf, String s) {
		if (s == null) {
			buf.append("null");
		} else {
			buf.append("\"").append(s).append("\"");
		}
	}

	/**
	 * validate s to be of type comparator + numerical value
	 * 
	 * @param s
	 *          the string
	 */
	private void validate(String s) {
		try {
			new LimitValidator().validate(null, null, s);
		} catch (ValidatorException e) {
			throw new DataAccessException(e.getMessage());
		}
	}

	/**
	 * construct limits field
	 * 
	 * @param limitSoftLow
	 *          the soft low limit
	 * @param limitSoftUp
	 *          the soft up limit
	 * @param limitHardLow
	 *          the hard low limit
	 * @param limitHardUp
	 *          the hard up limit
	 * @return the json element
	 */
	public JsonElement toGson(String limitSoftLow, String limitSoftUp, String limitHardLow, String limitHardUp) {
		validate(limitHardUp);
		validate(limitSoftUp);
		validate(limitHardLow);
		validate(limitSoftLow);
		JsonObject jsonElement = new JsonObject();
		jsonElement.addProperty("soft_low", limitSoftLow);
		jsonElement.addProperty("soft_up", limitSoftUp);
		jsonElement.addProperty("hard_low", limitHardLow);
		jsonElement.addProperty("hard_up", limitHardUp);
		return jsonElement;
	}
}
