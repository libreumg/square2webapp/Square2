package square2.latex.model;

/**
 * 
 * @author henkej
 *
 */
public interface SnippletTypeInterface {
	/**
	 * generate complete latex snipplet
	 * 
	 * @return latex snipplet
	 */
	public String toLatex();
}
