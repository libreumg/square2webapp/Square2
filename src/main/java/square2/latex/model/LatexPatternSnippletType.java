package square2.latex.model;

/**
 * 
 * @author henkej
 *
 */
public class LatexPatternSnippletType implements SnippletTypeInterface {
	private final String code;
	private final String template;

	/**
	 * create new latex pattern snipplet type
	 * 
	 * @param code
	 *          the replacement for all <b>${code}</b> blocks in template
	 * @param template
	 *          the string that contains <b>${code}</b> to be replaced
	 */
	public LatexPatternSnippletType(String code, String template) {
		this.code = code;
		this.template = template;
	}

	@Override
	public String toLatex() {
		return template.replaceAll("${code}", code);
	}
}
