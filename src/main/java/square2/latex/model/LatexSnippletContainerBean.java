package square2.latex.model;

import java.util.Set;

import square2.modules.model.report.design.SnipplettypeBean;

/**
 * 
 * @author henkej
 *
 */
public class LatexSnippletContainerBean {
	private final String blockComment;
	private Boolean dirty;
	private final String snippletId;
	private final String result;
	private final SnipplettypeBean type;
	private final String latexBefore;
	private final String latexAfter;
	private final Set<String> graphicsSet;
	private final String resultAnnotation;
	private final Boolean escape;

	/**
	 * create a new latex snipplet container bean
	 * 
	 * @param index
	 *          the index
	 * @param snippletId
	 *          the id of the snipplet
	 * @param result
	 *          the result
	 * @param type
	 *          the type of the snipplet bean
	 * @param latexBefore
	 *          the latex before block, rendered before this snipplet in latex
	 * @param latexAfter
	 *          the latex after block, rendered after this snipplet in latex
	 * @param graphicsSet
	 *          the set of graphics
	 * @param resultAnnotation
	 *          the result annotation
	 * @param escape
	 *          true if this code should be escaped, false otherwise
	 * @param pkSnipplet
	 *          the id of the snipplet in the database (for debug reasons)
	 */
	public LatexSnippletContainerBean(Integer index, String snippletId, String result, SnipplettypeBean type,
			String latexBefore, String latexAfter, Set<String> graphicsSet, String resultAnnotation, Boolean escape,
			Integer pkSnipplet) {
		StringBuilder buf = new StringBuilder("% by Square²: snippletId = ");
		buf.append(snippletId);
		buf.append(", index = ").append(index);
		buf.append(", T_SNIPPLET.pk = ").append(pkSnipplet);
		buf.append(", T_SNIPPLETTYPE.pk = ").append(type == null ? null : type.getId());
		buf.append(", T_SNIPPLETTYPE.type = ").append(type == null ? null : type.getSnipplettype());
		buf.append("\n");
		this.blockComment = buf.toString();
		this.dirty = false;
		this.snippletId = snippletId;
		this.result = result;
		this.type = type;
		this.latexBefore = latexBefore;
		this.latexAfter = latexAfter;
		this.graphicsSet = graphicsSet;
		this.resultAnnotation = resultAnnotation;
		this.escape = escape;
	}

	/**
	 * @return the snipplet id
	 */
	public String getSnippletId() {
		return snippletId;
	}

	/**
	 * @return the result
	 */
	public String getResult() {
		return result;
	}

	/**
	 * @return the type
	 */
	public SnipplettypeBean getType() {
		return type;
	}

	/**
	 * @return the latex before
	 */
	public String getLatexBefore() {
		return latexBefore;
	}

	/**
	 * @return the latex after
	 */
	public String getLatexAfter() {
		return latexAfter;
	}

	/**
	 * @return the set of graphics
	 */
	public Set<String> getGraphicsSet() {
		return graphicsSet;
	}

	/**
	 * @return the dirty flag
	 */
	public Boolean getDirty() {
		return dirty;
	}

	/**
	 * @param dirty
	 *          the dirty flag
	 */
	public void setDirty(Boolean dirty) {
		this.dirty = dirty;
	}

	/**
	 * @return the block comment
	 */
	public String getBlockComment() {
		return blockComment;
	}

	/**
	 * @return the resultAnnotation
	 */
	public String getResultAnnotation() {
		return resultAnnotation;
	}

	/**
	 * @return the escape
	 */
	public Boolean getEscape() {
		return escape;
	}

}
