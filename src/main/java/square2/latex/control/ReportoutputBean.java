package square2.latex.control;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class ReportoutputBean implements Serializable {
	private static final long serialVersionUID = 2765375030046927187L;

	private final Integer fkReport;
	private String latexcode;
	private String checksum;
	private byte[] pdf;
	private Boolean isNew;

	/**
	 * @param fkReport
	 *          the id of the report
	 */
	public ReportoutputBean(Integer fkReport) {
		super();
		this.fkReport = fkReport;
	}

	/**
	 * @return the latexcode
	 */
	public String getLatexcode() {
		return latexcode;
	}

	/**
	 * @param latexcode
	 *          the latexcode to set
	 */
	public void setLatexcode(String latexcode) {
		this.latexcode = latexcode;
	}

	/**
	 * @return the checksum
	 */
	public String getChecksum() {
		return checksum;
	}

	/**
	 * @param checksum
	 *          the checksum to set
	 */
	public void setChecksum(String checksum) {
		this.checksum = checksum;
	}

	/**
	 * @return the pdf
	 */
	public byte[] getPdf() {
		return pdf;
	}

	/**
	 * @param pdf
	 *          the pdf to set
	 */
	public void setPdf(byte[] pdf) {
		this.pdf = pdf;
	}

	/**
	 * @return the fkReport
	 */
	public Integer getFkReport() {
		return fkReport;
	}

	/**
	 * @return the isNew
	 */
	public Boolean getIsNew() {
		return isNew;
	}

	/**
	 * @param isNew the isNew to set
	 */
	public void setIsNew(Boolean isNew) {
		this.isNew = isNew;
	}
}
