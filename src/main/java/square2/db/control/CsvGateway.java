package square2.db.control;

import org.jooq.Field;
import org.jooq.Record;
import org.jooq.Select;

import square2.help.SquareFacesContext;

/**
 * 
 * @author henkej
 *
 */
public abstract class CsvGateway extends JooqGateway {
	/**
	 * generate new csv gateway
	 * 
	 * @param facesContext
	 *          the context of this gateway
	 */
	public CsvGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	/**
	 * execute query and put result into a csv structured string (including line
	 * breaks)
	 * 
	 * @param sql
	 *          the jooq query to execute
	 * @param separator
	 *          the csv separator character (typically , or ; )
	 * @param encapsulator
	 *          the csv quote character (typically ' or " )
	 * @return the result as a csv
	 * 
	 *         if an sql exception appears
	 */
	public String getResultAsCsv(Select<Record> sql, String separator, String encapsulator) {
		StringBuilder buf = new StringBuilder();
		boolean headline = false;
		for (Record r : sql.fetch()) {
			boolean isFirst = true;
			if (!headline) {
				for (Field<?> field : r.fields()) {
					buf.append(isFirst ? "" : separator);
					isFirst = false;
					buf.append(encaps(field.getName(), encapsulator));
				}
				headline = true;
				buf.append("\n");
			}
			isFirst = true;
			for (Field<?> field : r.fields()) {
				Object value = field.getValue(r);
				buf.append(isFirst ? "" : separator);
				isFirst = false;
				buf.append(encaps(value == null ? "" : value.toString(), encapsulator));
			}
			buf.append("\n");
		}
		return buf.toString();

	}

	private String encaps(String s, String c) {
		return new StringBuilder(c).append(s.replaceAll(c, "\\".concat(c))).append(c).toString();
	}
}
