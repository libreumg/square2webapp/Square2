package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_ATTRTYPE;
import static de.ship.dbppsquare.square.Tables.T_CONFOUNDER;
import static de.ship.dbppsquare.square.Tables.T_ELEMENT;
import static de.ship.dbppsquare.square.Tables.T_ELEMENTSHIP;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONINPUTTYPE;
import static de.ship.dbppsquare.square.Tables.T_GROUPPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_IOMESSAGE;
import static de.ship.dbppsquare.square.Tables.T_IOSTACK;
import static de.ship.dbppsquare.square.Tables.T_IOSTACK_FILE;
import static de.ship.dbppsquare.square.Tables.T_LANGUAGEKEY;
import static de.ship.dbppsquare.square.Tables.T_METADATATYPE;
import static de.ship.dbppsquare.square.Tables.T_MISSING;
import static de.ship.dbppsquare.square.Tables.T_MISSINGTYPE;
import static de.ship.dbppsquare.square.Tables.T_PERSON;
import static de.ship.dbppsquare.square.Tables.T_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_STUDY;
import static de.ship.dbppsquare.square.Tables.T_STUDYATTR;
import static de.ship.dbppsquare.square.Tables.T_STUDYGROUP;
import static de.ship.dbppsquare.square.Tables.T_TRANSLATION;
import static de.ship.dbppsquare.square.Tables.T_USERGROUP;
import static de.ship.dbppsquare.square.Tables.T_USERPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEATTRIBUTE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEATTRIBUTEROLE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUP;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUPELEMENT;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEUSAGE;
import static de.ship.dbppsquare.square.Tables.T_WIDGETBASED;
import static de.ship.dbppsquare.square.Tables.V_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.V_STUDY;
import static de.ship.dbppsquare.square.Tables.V_VERSION;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.Field;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.InsertResultStep;
import org.jooq.InsertValuesStep2;
import org.jooq.InsertValuesStep3;
import org.jooq.InsertValuesStep4;
import org.jooq.InsertValuesStep5;
import org.jooq.JSONB;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record10;
import org.jooq.Record11;
import org.jooq.Record12;
import org.jooq.Record14;
import org.jooq.Record15;
import org.jooq.Record16;
import org.jooq.Record17;
import org.jooq.Record2;
import org.jooq.Record20;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.Record5;
import org.jooq.Record7;
import org.jooq.Result;
import org.jooq.SelectConditionStep;
import org.jooq.SelectHavingStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectLimitPercentStep;
import org.jooq.SelectLimitStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.SelectSeekStep1;
import org.jooq.SelectWhereStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import de.ship.dbppsquare.square.enums.EnumControlvar;
import de.ship.dbppsquare.square.enums.EnumDatatype;
import de.ship.dbppsquare.square.enums.EnumEntity;
import de.ship.dbppsquare.square.enums.EnumIsocode;
import de.ship.dbppsquare.square.enums.EnumMetadatatype;
import de.ship.dbppsquare.square.tables.TElement;
import de.ship.dbppsquare.square.tables.records.TConfounderRecord;
import de.ship.dbppsquare.square.tables.records.TElementRecord;
import de.ship.dbppsquare.square.tables.records.TElementshipRecord;
import de.ship.dbppsquare.square.tables.records.TGroupprivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TIostackFileRecord;
import de.ship.dbppsquare.square.tables.records.TIostackRecord;
import de.ship.dbppsquare.square.tables.records.TLanguagekeyRecord;
import de.ship.dbppsquare.square.tables.records.TMetadatatypeRecord;
import de.ship.dbppsquare.square.tables.records.TPrivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TStudyRecord;
import de.ship.dbppsquare.square.tables.records.TStudyattrRecord;
import de.ship.dbppsquare.square.tables.records.TStudygroupRecord;
import de.ship.dbppsquare.square.tables.records.TTranslationRecord;
import de.ship.dbppsquare.square.tables.records.TUserprivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TVariableRecord;
import de.ship.dbppsquare.square.tables.records.TVariableattributeRecord;
import de.ship.dbppsquare.square.tables.records.TVariableattributeroleRecord;
import square2.db.control.converter.EnumConverter;
import square2.db.control.model.LambdaResultWrapper;
import square2.help.SquareFacesContext;
import square2.help.ValuelistConverter;
import square2.modules.model.AclBean;
import square2.modules.model.GroupPrivilegeBean;
import square2.modules.model.cohort.CohortStudyBean;
import square2.modules.model.cohort.StudyAttributeBean;
import square2.modules.model.cohort.StudygroupBean;
import square2.modules.model.study.ConfounderBean;
import square2.modules.model.study.DeptmetaBean;
import square2.modules.model.study.ElementBean;
import square2.modules.model.study.IdLabelBean;
import square2.modules.model.study.MetadatatypeBean;
import square2.modules.model.study.UploadBean;
import square2.modules.model.study.VariableAttributeBean;
import square2.modules.model.study.VariableAttributeRoleBean;
import square2.modules.model.study.VariableBean;
import square2.modules.model.study.VariablegroupBean;
import square2.modules.model.study.VariableusageBean;

/**
 * 
 * @author henkej
 *
 */
public class StudyGateway extends CsvGateway {
	private static final Logger LOGGER = LogManager.getLogger(StudyGateway.class);

	/**
	 * generate StudyGateway
	 * 
	 * @param facesContext context of this gateway
	 */
	public StudyGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
		return getPrivilegeMap(getJooq());
	}

	/**
	 * trim s to maxSize and append ... if bigger (even with ... it's only maxSize
	 * chars long)
	 * 
	 * @param s the string to be trimmed
	 * @param maxSize the maximum size of the resulting string
	 * @return trimmed String; if it is longer than maxSize, reduce it to maxSize
	 * and replace the last 3 characters by a dot
	 */
	public static final String trimTo(String s, Integer maxSize) {
		if (s == null) {
			return null;
		}
		if (s.length() > maxSize) {
			return s.substring(0, maxSize - 3).concat("...");
		} else {
			return s;
		}
	}

	/**
	 * return s with valid chars only; valid is [a-z0-9]
	 * 
	 * @param s input to be converted
	 * @return s with valid chars
	 */
	protected String extractInvalidChars(String s) {
		return s == null ? "" : s.toLowerCase().trim().replaceAll("[^a-z0-9]", "");
	}

	/**
	 * generate dn by study name + . + element name
	 * 
	 * @param studyName corresponding study; for defining a study, use the study
	 * group here
	 * @param elementName corresponing name of element
	 * 
	 * @return dn generated by studyName and elementName
	 * 
	 * if anything went wrong on database side
	 */
	public String generateDn(String studyName, String elementName) {
		String prefix = extractInvalidChars(studyName);
		String suffix = extractInvalidChars(elementName);
		return new StringBuilder(prefix).append(".").append(suffix).toString();
	}

	/**
	 * generate unique name by study unique name + . + element name
	 * 
	 * @param studyUniqueName unique name of the study
	 * @param elementName name of the element
	 * @return the new unique name
	 */
	public String generateUniqueName(String studyUniqueName, String elementName) {
		String prefix = extractInvalidChars(studyUniqueName);
		String suffix = extractInvalidChars(elementName);
		return new StringBuilder(prefix).append(".").append(suffix).toString();
	}

	/**
	 * get all studies from db that current user is able to read
	 * 
	 * @return list of studies
	 * 
	 * if anything went wrong on database side
	 */
	public List<CohortStudyBean> getAllStudies() {
		List<CohortStudyBean> list = new ArrayList<>();
		TElement parent = T_ELEMENT.as("parent");
		try (
				SelectConditionStep<Record12<Integer, Integer, String, JSONB, Integer, String, String, LocalDateTime, LocalDateTime, Integer, EnumAccesslevel, JSONB>> sql = getJooq()
				// @formatter:off
				  .selectDistinct(T_ELEMENT.PK, 
				  		            T_ELEMENT.FK_PARENT, 
				  		            T_ELEMENT.NAME, 
				  		            T_ELEMENT.TRANSLATION,
				  		            T_ELEMENT.FK_PRIVILEGE, 
				  		            T_ELEMENT.UNIQUE_NAME,
				  		            parent.NAME,
				  		            T_STUDY.START_DATE, 
				  		            T_STUDY.DESIGNFEATURE,
				  		            T_STUDY.PARTICIPANTS, 
				  		            V_PRIVILEGE.ACL,
				  		            T_STUDYGROUP.LOCALES)
				  .from(T_STUDY)
				  .leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_STUDY.FK_ELEMENT))
				  .leftJoin(T_STUDYGROUP).on(T_STUDYGROUP.FK_ELEMENT.eq(T_ELEMENT.FK_PARENT))
				  .leftJoin(parent).on(parent.PK.eq(T_ELEMENT.FK_PARENT))
				  .leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
				  .where(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()));
    		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				JSONB locales = r.get(T_STUDYGROUP.LOCALES);
				// possible attributes not necessary yet
				CohortStudyBean bean = new CohortStudyBean(new ArrayList<>(), locales == null ? JSONB.valueOf("{}") : locales);
				bean.setId(r.get(T_ELEMENT.PK));
				bean.setParentId(r.get(T_ELEMENT.FK_PARENT));
				bean.setParentName(r.get(parent.NAME));
				bean.setName(r.get(T_ELEMENT.NAME));
				bean.setRight(r.get(T_ELEMENT.FK_PRIVILEGE));
				LocalDateTime sd = r.get(T_STUDY.START_DATE);
				Date startDate = sd == null ? null : Date.valueOf(sd.atZone(ZoneId.systemDefault()).toLocalDate());
				bean.setStartDate(startDate);
				LocalDateTime df = r.get(T_STUDY.DESIGNFEATURE);
				Date designfeature = df == null ? null : Date.valueOf(df.atZone(ZoneId.systemDefault()).toLocalDate());
				bean.setDesignfeature(designfeature);
				bean.setParticipants(r.get(T_STUDY.PARTICIPANTS));
				bean.setTranslation(r.get(T_ELEMENT.TRANSLATION));
				bean.setAcl(r.get(V_PRIVILEGE.ACL));
				bean.setUniqueName(r.get(T_ELEMENT.UNIQUE_NAME));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all technical variables from db
	 * 
	 * @return list of variables
	 * 
	 * if anything went wrong on database side
	 */
	public List<VariableBean> getAllIdVariables() {
		List<VariableBean> list = new ArrayList<>();
		try (
				SelectConditionStep<Record16<Integer, String, String, Integer, String, Integer, String, EnumDatatype, Boolean, JSONB, String, String, Integer, String, Integer, JSONB>> sql = getJooq()
				// @formatter:off
					.select(T_ELEMENT.PK, 
					        T_ELEMENT.NAME, 
					        T_ELEMENT.UNIQUE_NAME, 
					        T_ELEMENT.FK_MISSINGLIST,
					        T_ELEMENTSHIP.DN, 
					        T_VARIABLE.VAR_ORDER,
							    T_VARIABLE.COLUMN_NAME, 
							    T_VARIABLE.DATATYPE,
							    T_VARIABLE.IDVARIABLE,
							    T_ELEMENT.TRANSLATION, 
							    V_STUDY.STUDY_NAME, 
							    V_STUDY.STUDYGROUP_NAME,
							    T_VARIABLEUSAGE.PK, 
							    T_VARIABLEUSAGE.NAME, 
							    T_VARIABLEUSAGE.FK_LANG,
							    V_STUDY.LOCALES)
					.from(T_ELEMENT)
					.leftJoin(T_ELEMENTSHIP).on(T_ELEMENTSHIP.FK_ELEMENT.eq(T_ELEMENT.PK))
					.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(T_ELEMENT.PK))
					.leftJoin(T_VARIABLEUSAGE).on(T_VARIABLEUSAGE.PK.eq(T_VARIABLE.FK_VARIABLEUSAGE))
					.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_ELEMENT.PK))
					.where(T_VARIABLE.IDVARIABLE);
        // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				String columnName = r.get(T_VARIABLE.COLUMN_NAME);
				EnumDatatype datatype = r.get(T_VARIABLE.DATATYPE);
				Boolean idvariable = r.get(T_VARIABLE.IDVARIABLE);
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				String name = r.get(T_ELEMENT.NAME);
				Integer keyElement = r.get(T_ELEMENT.PK);
				Integer fkMissinglist = r.get(T_ELEMENT.FK_MISSINGLIST);
				Integer varOrder = r.get(T_VARIABLE.VAR_ORDER);
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				VariableusageBean variableusage = new VariableusageBean(r.get(T_VARIABLEUSAGE.PK));
				variableusage.setName(r.get(T_VARIABLEUSAGE.NAME));
				variableusage.setFkLang(r.get(T_VARIABLEUSAGE.FK_LANG));
				JSONB locales = r.get(V_STUDY.LOCALES);
				VariableBean bean = new VariableBean(columnName, datatype == null ? null : datatype.getLiteral(), idvariable,
						uniqueName, name, keyElement, null, translation, varOrder, study, studygroup, variableusage, fkMissinglist,
						locales);
				bean.setName(r.get(T_ELEMENTSHIP.DN));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * insert study into db
	 * 
	 * @param study to be added to the database
	 * @param aclBean for the privileges on this study
	 * @return keyElement of new study
	 * 
	 * if anything went wrong on database side
	 */
	public Integer insertStudy(CohortStudyBean study, AclBean aclBean) {
		Integer usnr = getFacesContext().getUsnr();
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				checkUniqueNameDoesntExist(DSL.using(c), study.getUniqueName());
				Integer privilegeId = addAndGetPrivilegeId(DSL.using(c), null);
				Integer langPrivilegeId = addAndGetPrivilegeId(DSL.using(c), null);
				addUserGroupPrivilege(DSL.using(c), usnr, null, privilegeId, aclBean);
				addUserGroupPrivilege(DSL.using(c), usnr, null, langPrivilegeId, aclBean);
				InsertResultStep<TElementRecord> sql = DSL.using(c)
				// @formatter:off
					.insertInto(T_ELEMENT, 
							        T_ELEMENT.FK_PARENT, 
							        T_ELEMENT.FK_PRIVILEGE, 
							        T_ELEMENT.TRANSLATION, 
							        T_ELEMENT.NAME,
							        T_ELEMENT.UNIQUE_NAME)
					.select(DSL.using(c)
						.select(DSL.val(study.getParentId()), 
								    DSL.val(privilegeId), 
								    DSL.val(study.getTranslation()),
								    DSL.concat(study.getName(), ""), 
								    DSL.val(study.getUniqueName()))
						.from(T_ELEMENT).where(T_ELEMENT.PK.eq(study.getParentId())))
					.returning(T_ELEMENT.PK);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				Integer keyElement = sql.fetchOne().getPk();
				lrw.setInteger(keyElement);

				// copy the privileges to the parent (the study, former studygroup)
				Integer parentPrivilegeId = getParentPrivilegeId(privilegeId);
				addUserGroupPrivilege(DSL.using(c), usnr, null, parentPrivilegeId, aclBean);

				java.util.Date sd = study.getStartDate();
				java.util.Date df = study.getDesignfeature();
				LocalDateTime startDate = sd == null ? null : sd.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				LocalDateTime designFeature = df == null ? null
						: df.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

				InsertValuesStep4<TStudyRecord, Integer, LocalDateTime, LocalDateTime, Integer> sql3 = DSL.using(c)
				// @formatter:off
					.insertInto(T_STUDY, T_STUDY.FK_ELEMENT, T_STUDY.START_DATE, T_STUDY.DESIGNFEATURE, T_STUDY.PARTICIPANTS)
					.values(keyElement, startDate, designFeature, study.getParticipants());
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				sql3.execute();
				UpdateConditionStep<TPrivilegeRecord> sql4 = DSL.using(c)
				// @formatter:off
				  .update(T_PRIVILEGE)
				  .set(T_PRIVILEGE.NAME, new StringBuilder("element.").append(keyElement).toString())
					.where(T_PRIVILEGE.PK.eq(privilegeId));
				// @formatter:on
				LOGGER.debug("{}", sql4.toString());
				sql4.execute();
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update study referenced by bean's study id
	 * 
	 * @param bean to be updated in the database
	 * @param isocode for translation issues; the bean contains a field translation
	 * that holds the translation for isocode
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer updateStudy(CohortStudyBean bean, String isocode) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				UpdateConditionStep<TElementRecord> sqlCore = DSL.using(c)
				// @formatter:off
					.update(T_ELEMENT)
					.set(T_ELEMENT.FK_PARENT, bean.getParentId())
					.set(T_ELEMENT.TRANSLATION, bean.getTranslation())
					.set(T_ELEMENT.NAME, bean.getName())
					.where(T_ELEMENT.PK.eq(bean.getId()));
				// @formatter:on
				LOGGER.debug("{}", sqlCore.toString());
				lrw.addToInteger(sqlCore.execute());

				java.util.Date df = bean.getDesignfeature();
				java.util.Date sd = bean.getStartDate();
				LocalDateTime designFeature = df == null ? null
						: df.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				LocalDateTime startDate = sd == null ? null : sd.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				UpdateConditionStep<TStudyRecord> sqlStudy = DSL.using(c)
				// @formatter:off
						.update(T_STUDY)
						.set(T_STUDY.DESIGNFEATURE, designFeature)
						.set(T_STUDY.PARTICIPANTS, bean.getParticipants())
						.set(T_STUDY.START_DATE, startDate)
						.where(T_STUDY.FK_ELEMENT.eq(bean.getId()));
				// @formatter:on
				LOGGER.debug("{}", sqlStudy.toString());
				lrw.addToInteger(sqlStudy.execute());

				List<String> names = new ArrayList<>();
				for (StudyAttributeBean sab : bean.getAttributes()) {
					names.add(sab.getKey());
				}
				DeleteConditionStep<TStudyattrRecord> sqlStudyAttrDel = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_STUDYATTR)
					.where(T_STUDYATTR.FK_STUDY.in(DSL.using(c)
						.select(T_STUDY.PK)
						.from(T_STUDY)
						.where(T_STUDY.FK_ELEMENT.eq(bean.getId()))))
					.and(T_STUDYATTR.FK_ATTRTYPE.notIn(DSL.using(c)
						.select(T_ATTRTYPE.PK)
						.from(T_ATTRTYPE)
						.where(T_ATTRTYPE.NAME.in(names))));
				// @formatter:on
				LOGGER.debug("{}", sqlStudyAttrDel.toString());
				lrw.addToInteger(sqlStudyAttrDel.execute());

				for (StudyAttributeBean sab : bean.getAttributes()) {
					InsertOnDuplicateSetMoreStep<TStudyattrRecord> sqlStudyAttr = DSL.using(c)
					// @formatter:off
						.insertInto(T_STUDYATTR, 
								        T_STUDYATTR.FK_STUDY, 
								        T_STUDYATTR.FK_ATTRTYPE, 
								        T_STUDYATTR.VALUE)
						.select(DSL.using(c)
							.select(T_STUDY.PK, T_ATTRTYPE.PK, DSL.val(sab.getValue()))
							.from(T_ATTRTYPE)
							.fullJoin(T_STUDY).on(T_STUDY.FK_ELEMENT.eq(bean.getId()))
							.where(T_ATTRTYPE.NAME.eq(sab.getKey())))
						.onConflict(T_STUDYATTR.FK_STUDY, T_STUDYATTR.FK_ATTRTYPE)
						.doUpdate()
						.set(T_STUDYATTR.VALUE, sab.getValue());
					// @formatter:on
					LOGGER.debug("{}", sqlStudyAttr.toString());
					lrw.addToInteger(sqlStudyAttr.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete study from db
	 * 
	 * @param bean of study to be deleted
	 * @return number of affected database rows, should be 1
	 * 
	 * if anything went wrong on database side
	 */
	public Integer deleteDataCollection(CohortStudyBean bean) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				DeleteConditionStep<TUserprivilegeRecord> sql = DSL.using(t)
				// @formatter:off
						.deleteFrom(T_USERPRIVILEGE).where(T_USERPRIVILEGE.FK_PRIVILEGE.eq(bean.getRight()));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				DeleteConditionStep<TGroupprivilegeRecord> sql2 = DSL.using(t)
				// @formatter:off
						.deleteFrom(T_GROUPPRIVILEGE).where(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(bean.getRight()));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());

				DeleteConditionStep<TStudyattrRecord> sql3 = DSL.using(t)
				// @formatter:off
					.deleteFrom(T_STUDYATTR).where(T_STUDYATTR.FK_STUDY.in(DSL.using(t)
						.select(T_STUDY.PK)
						.from(T_STUDY)
						.where(T_STUDY.FK_ELEMENT.eq(bean.getId()))
					));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				lrw.addToInteger(sql3.execute());

				// make sure this one does not appear in the list of valid studies any more
				DeleteConditionStep<TStudyRecord> sql4 = DSL.using(t)
				// @formatter:off
						.deleteFrom(T_STUDY).where(T_STUDY.FK_ELEMENT.eq(bean.getId()));
				// @formatter:on
				LOGGER.debug("{}", sql4.toString());
				lrw.addToInteger(sql4.execute());

				DeleteConditionStep<TElementRecord> sql5 = DSL.using(t)
				// @formatter:off
						.deleteFrom(T_ELEMENT).where(T_ELEMENT.PK.eq(bean.getId()));
				// @formatter:on
				LOGGER.debug("{}", sql5.toString());
				lrw.addToInteger(sql5.execute());
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * load study containing all its rights and its attributes
	 * 
	 * @param id primary key of study
	 * @return study bean if found
	 */
	public CohortStudyBean getStudy(Integer id) {
		List<StudyAttributeBean> possibleAttributes = new ArrayList<>();
		try (SelectSeekStep1<Record4<String, JSONB, JSONB, String>, Integer> sql = getJooq()
		// @formatter:off
  		.select(T_ATTRTYPE.NAME, 
  				    T_WIDGETBASED.VALUE,
  				    T_WIDGETBASED.TYPE_RESTRICTION,
  				    T_FUNCTIONINPUTTYPE.NAME)
  		.from(T_ATTRTYPE)
  		.leftJoin(T_WIDGETBASED).on(T_WIDGETBASED.PK.eq(T_ATTRTYPE.FK_WIDGETBASED))
  		.leftJoin(T_FUNCTIONINPUTTYPE).on(T_FUNCTIONINPUTTYPE.PK.eq(T_WIDGETBASED.FK_FUNCTIONINPUTTYPE))
  		.where(T_ATTRTYPE.ENTITY.eq(EnumEntity.study))
  		.orderBy(T_WIDGETBASED.ORDER_NR)
  	// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				String name = r.get(T_ATTRTYPE.NAME);
				String inputtype = r.get(T_FUNCTIONINPUTTYPE.NAME);
				JSONB typerestriction = r.get(T_WIDGETBASED.TYPE_RESTRICTION);
				JSONB value = r.get(T_WIDGETBASED.VALUE);
				StudyAttributeBean attrBean = new StudyAttributeBean(name, inputtype, typerestriction);
				attrBean.setValue(value);
				possibleAttributes.add(attrBean);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		CohortStudyBean bean = getStudyWithoutAttributes(id, possibleAttributes);
		try (SelectSeekStep1<Record4<String, JSONB, JSONB, String>, Integer> sql = getJooq()
		// @formatter:off
  		.select(T_ATTRTYPE.NAME, 
  				    T_STUDYATTR.VALUE,
  				    T_WIDGETBASED.TYPE_RESTRICTION,
  				    T_FUNCTIONINPUTTYPE.NAME)
  		.from(T_STUDYATTR)
  		.leftJoin(T_ATTRTYPE).on(T_ATTRTYPE.PK.eq(T_STUDYATTR.FK_ATTRTYPE))
  		.leftJoin(T_WIDGETBASED).on(T_WIDGETBASED.PK.eq(T_ATTRTYPE.FK_WIDGETBASED))
  		.leftJoin(T_FUNCTIONINPUTTYPE).on(T_FUNCTIONINPUTTYPE.PK.eq(T_WIDGETBASED.FK_FUNCTIONINPUTTYPE))
  		.where(T_STUDYATTR.FK_STUDY.in(getJooq()
  			.select(T_STUDY.PK)
  			.from(T_STUDY)
  			.where(T_STUDY.FK_ELEMENT.eq(id))))
  		.orderBy(T_WIDGETBASED.ORDER_NR)
  	// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				String name = r.get(T_ATTRTYPE.NAME);
				String inputtype = r.get(T_FUNCTIONINPUTTYPE.NAME);
				JSONB typerestriction = r.get(T_WIDGETBASED.TYPE_RESTRICTION);
				JSONB value = r.get(T_STUDYATTR.VALUE);
				StudyAttributeBean attrBean = new StudyAttributeBean(name, inputtype, typerestriction);
				attrBean.setValue(value);
				bean.getAttributes().add(attrBean);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return bean;
	}

	/**
	 * load study containing all its rights but not with attributes
	 * 
	 * @param id primary key of study
	 * @return study bean if found
	 */
	private CohortStudyBean getStudyWithoutAttributes(Integer id, List<StudyAttributeBean> possibleAttributes) {
		CohortStudyBean bean = new CohortStudyBean(possibleAttributes, null);
		Integer usnr = getFacesContext().getUsnr();
		TElement parent = T_ELEMENT.as("parent");
		try (
				SelectConditionStep<Record12<Integer, Integer, String, JSONB, Integer, String, String, LocalDateTime, LocalDateTime, Integer, EnumAccesslevel, JSONB>> sql = getJooq()
				// @formatter:off
					.select(T_ELEMENT.PK, 
							    T_ELEMENT.FK_PARENT, 
							    T_ELEMENT.NAME, 
							    T_ELEMENT.TRANSLATION, 
							    T_ELEMENT.FK_PRIVILEGE,
								  T_ELEMENT.UNIQUE_NAME,
								  parent.NAME,
								  T_STUDY.START_DATE, 
								  T_STUDY.DESIGNFEATURE, 
								  T_STUDY.PARTICIPANTS, 
								  V_PRIVILEGE.ACL,
								  T_STUDYGROUP.LOCALES)
					.from(T_STUDY)
					.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_STUDY.FK_ELEMENT))
					.leftJoin(T_STUDYGROUP).on(T_STUDYGROUP.FK_ELEMENT.eq(T_ELEMENT.FK_PARENT))
					.leftJoin(parent).on(parent.PK.eq(T_ELEMENT.FK_PARENT))
					.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
					.leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_ELEMENT.FK_LANG)
							.and(T_TRANSLATION.ISOCODE.cast(String.class).eq(getFacesContext().getIsocode())))
					.where(T_ELEMENT.PK.eq(id)).and(V_PRIVILEGE.FK_USNR.eq(usnr));
		    // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			// there might be multiple results only because of user and user group
			// privileges on a study; take the first hit anyway
			for (Record r : sql.fetch()) {
				LocalDateTime ldt = r.get(T_STUDY.START_DATE);
				Date startDate = ldt == null ? null : Date.valueOf(ldt.atZone(ZoneId.systemDefault()).toLocalDate());
				LocalDateTime ldt2 = r.get(T_STUDY.DESIGNFEATURE);
				Date designfeature = ldt2 == null ? null : Date.valueOf(ldt2.atZone(ZoneId.systemDefault()).toLocalDate());
				bean.setId(r.get(T_ELEMENT.PK));
				bean.setParentId(r.get(T_ELEMENT.FK_PARENT));
				bean.setParentName(r.get(parent.NAME));
				bean.setName(r.get(T_ELEMENT.NAME));
				bean.setRight(r.get(T_ELEMENT.FK_PRIVILEGE));
				bean.setStartDate(startDate);
				bean.setDesignfeature(designfeature);
				bean.setParticipants(r.get(T_STUDY.PARTICIPANTS));
				bean.setTranslation(r.get(T_ELEMENT.TRANSLATION));
				bean.setAcl(r.get(V_PRIVILEGE.ACL));
				bean.setUniqueName(r.get(T_ELEMENT.UNIQUE_NAME));
				bean.setLocales(r.get(T_STUDYGROUP.LOCALES));
				return bean;
			}
			throw new DataAccessException("no study found with id = " + id);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all study elements that the user can access
	 * 
	 * @param usnr id of the user that has privileges on the elements of the
	 * hierarchy
	 * @return list of found elements from the hierarchy that the user has access on
	 * 
	 * if anything went wrong on database side
	 */
	public List<ElementBean> getAllStudyElementsOf(Integer usnr) {
		try (
				SelectConditionStep<Record17<Integer, String, String, Integer, JSONB, Integer, String, Integer, Integer, Integer, Integer, Integer, EnumAccesslevel, String, String, String, JSONB>> sql = getJooq()
				// @formatter:off
  				.selectDistinct(T_ELEMENT.PK, 
  				                T_ELEMENT.UNIQUE_NAME, 
  				                T_ELEMENT.NAME, 
  				                T_ELEMENT.FK_PARENT, 
  				                T_ELEMENT.TRANSLATION,
  				                T_ELEMENT.FK_PRIVILEGE, 
  				                T_ELEMENT.UNIQUE_NAME,
  				                T_ELEMENT.FK_MISSINGLIST,
  				                T_VARIABLE.FK_ELEMENT, 
  				                T_VARIABLE.VAR_ORDER,
  				                T_ELEMENT.ORDER_NR, 
  				                T_VARIABLE.VAR_ORDER, 
  				                V_PRIVILEGE.ACL, 
  				                V_STUDY.STUDY_NAME,
  				                V_STUDY.STUDYGROUP_NAME,
  				                T_VARIABLEATTRIBUTE.VALUE,
  				                V_STUDY.LOCALES)
  				.from(T_STUDY)
  				.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_STUDY.FK_ELEMENT))
  				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE)).and(V_PRIVILEGE.FK_USNR.eq(usnr))
  				.leftJoin(T_GROUPPRIVILEGE).on(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
  				.leftJoin(T_USERGROUP).on(T_USERGROUP.FK_GROUP.eq(T_GROUPPRIVILEGE.FK_GROUP))
  				.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(T_ELEMENT.PK))
  				.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_ELEMENT.PK))
  				.leftJoin(T_VARIABLEATTRIBUTE).on(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(T_ELEMENT.PK)
  				                              .and(T_VARIABLEATTRIBUTE.IS_PRIMARY_ATTRIBUTE.eq(true)))                        
  				.where(T_ELEMENT.PK.isNotNull());
        // TODO: refactor - should not be possible because of T_STUDY.fk_study_hierarchy
        // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Map<Integer, ElementBean> map = new HashMap<>();
			for (Record r : sql.fetch()) {
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				String label = r.get(T_VARIABLEATTRIBUTE.VALUE);
				if (label == null) { // use the translation value until we have defined T_ELEMENTATTRIBUTE to hold
															// labels there
					label = r.get(T_TRANSLATION.VALUE);
				}
				Integer orderNr = r.get(T_ELEMENT.ORDER_NR);
				Boolean isVariable = r.get(T_VARIABLE.FK_ELEMENT) != null;
				if (isVariable) {
					orderNr = r.get(T_VARIABLE.VAR_ORDER);
				}
				Integer shipOrder = r.get(T_VARIABLE.VAR_ORDER);
				Integer keyElement = r.get(T_ELEMENT.PK);
				Integer fkPrivilege = r.get(T_ELEMENT.FK_PRIVILEGE);
				Integer fkParentElement = r.get(T_ELEMENT.FK_PARENT);
				Integer fkMissinglist = r.get(T_ELEMENT.FK_MISSINGLIST);
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				JSONB locales = r.get(V_STUDY.LOCALES);
				ElementBean bean = new ElementBean(uniqueName, keyElement, fkPrivilege, fkParentElement, translation, label,
						orderNr, shipOrder, study, studygroup, fkMissinglist, locales);
				bean.setName(r.get(T_ELEMENT.NAME));
				bean.getAccessLevel().setAcl(r.get(V_PRIVILEGE.ACL));
				bean.setIsVariable(isVariable);
				map.put(bean.getId(), bean);
			}
			List<ElementBean> list = new ArrayList<>();
			list.addAll(map.values());
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all study elements that usnr has privileges on
	 * 
	 * @param usnr to be used
	 * @return list of elements; an empty list at least
	 * 
	 * if anything went wrong on database side
	 */
	public List<ElementBean> getAllElementsOf(Integer usnr) {
		List<ElementBean> list = new ArrayList<>();
		try (
				SelectOnConditionStep<Record17<Integer, String, String, String, Integer, JSONB, Integer, Integer, Integer, Integer, Integer, Integer, String, EnumAccesslevel, String, String, JSONB>> sql = getJooq()
				// @formatter:off
  				.selectDistinct(T_ELEMENT.PK, 
  				                T_ELEMENT.UNIQUE_NAME, 
  				                T_ELEMENT.NAME, 
  				                T_ELEMENT.UNIQUE_NAME,
  				                T_ELEMENT.FK_PARENT, 
  				                T_ELEMENT.TRANSLATION,
  				                T_ELEMENT.FK_PRIVILEGE, 
  				                T_ELEMENT.FK_MISSINGLIST,
  				                T_VARIABLE.FK_ELEMENT,
  				                T_VARIABLE.VAR_ORDER, 
  				                T_ELEMENT.ORDER_NR, 
  				                T_VARIABLE.VAR_ORDER, 
  				                T_VARIABLEATTRIBUTE.VALUE,
  				                V_PRIVILEGE.ACL,
  				                V_STUDY.STUDY_NAME, 
  				                V_STUDY.STUDYGROUP_NAME,
  				                V_STUDY.LOCALES)
  				.from(T_ELEMENT)
  				.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(T_ELEMENT.PK))
  				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
  				                      .and(V_PRIVILEGE.FK_USNR.eq(usnr))
  				                      .and(V_PRIVILEGE.ACL.isNotNull()) // because of left join
  				.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_ELEMENT.PK))
  				.leftJoin(T_VARIABLEATTRIBUTE).on(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(T_ELEMENT.PK)
              .and(T_VARIABLEATTRIBUTE.IS_PRIMARY_ATTRIBUTE.eq(true)));
        // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				EnumAccesslevel acl = r.get(V_PRIVILEGE.ACL);
				Integer orderNr = r.get(T_ELEMENT.ORDER_NR);
				Boolean isVariable = r.get(T_VARIABLE.FK_ELEMENT) != null;
				if (isVariable) {
					orderNr = r.get(T_VARIABLE.VAR_ORDER);
				}
				Integer shipOrder = r.get(T_VARIABLE.VAR_ORDER);
				Integer keyElement = r.get(T_ELEMENT.PK);
				Integer fkPrivilege = r.get(T_ELEMENT.FK_PRIVILEGE);
				Integer fkMissinglist = r.get(T_ELEMENT.FK_MISSINGLIST);
				Integer fkParentElement = r.get(T_ELEMENT.FK_PARENT);
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				String label = r.get(T_VARIABLEATTRIBUTE.VALUE);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				JSONB locales = r.get(V_STUDY.LOCALES);
				if (label == null) { // use translation until T_ELEMENTATTRIBUTE exists
					label = getFacesContext().selectTranslation(translation);
				}
				ElementBean bean = new ElementBean(uniqueName, keyElement, fkPrivilege, fkParentElement, translation, label,
						orderNr, shipOrder, study, studygroup, fkMissinglist, locales);
				bean.setName(r.get(T_ELEMENT.NAME));
				bean.getAccessLevel().setAcl(acl);
				bean.setIsVariable(isVariable);
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all sub elements of element with id = elementId that usnr can access
	 * 
	 * @param elementId id of the element to look for children
	 * @return list of found children
	 * 
	 * if anything went wrong on database side
	 */
	public List<ElementBean> getAllElementsOfParent(Integer elementId) {
		List<ElementBean> list = new ArrayList<>();
		Integer usnr = getFacesContext().getUsnr();
		try (
				SelectConditionStep<Record15<Integer, String, Integer, JSONB, String, Integer, Integer, Integer, Integer, Integer, String, String, EnumAccesslevel, String, JSONB>> sql = getJooq()
				// @formatter:off
					.select(T_ELEMENT.PK, 
					        T_ELEMENT.UNIQUE_NAME, 
					        T_ELEMENT.FK_PARENT, 
					        T_ELEMENT.TRANSLATION,
					        T_ELEMENT.NAME,
					        T_ELEMENT.FK_PRIVILEGE, 
					        T_ELEMENT.FK_MISSINGLIST,
					        T_VARIABLE.FK_ELEMENT, 
					        T_VARIABLE.VAR_ORDER,
					        T_ELEMENT.ORDER_NR, 
					        V_STUDY.STUDY_NAME, 
					        V_STUDY.STUDYGROUP_NAME, 
					        V_PRIVILEGE.ACL,
					        T_VARIABLEATTRIBUTE.VALUE,
					        V_STUDY.LOCALES)
					.from(T_ELEMENT)
					.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
					                      .and(V_PRIVILEGE.FK_USNR.eq(usnr))
					.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(T_ELEMENT.PK))
					.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_ELEMENT.PK))
					.leftJoin(T_VARIABLEATTRIBUTE).on(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(T_ELEMENT.PK)
                                        .and(T_VARIABLEATTRIBUTE.IS_PRIMARY_ATTRIBUTE.eq(true)))
					.where(elementId == null ? T_ELEMENT.FK_PARENT.isNull() : T_ELEMENT.FK_PARENT.eq(elementId))
					.and(V_PRIVILEGE.ACL.isNotNull()); // because of left join
        // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {

				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				Integer orderNr = r.get(T_ELEMENT.ORDER_NR);
				Boolean isVariable = r.get(T_VARIABLE.FK_ELEMENT) != null;
				if (isVariable) {
					orderNr = r.get(T_VARIABLE.VAR_ORDER);
				}
				Integer shipOrder = r.get(T_VARIABLE.VAR_ORDER);
				Integer keyElement = r.get(T_ELEMENT.PK);
				Integer fkPrivilege = r.get(T_ELEMENT.FK_PRIVILEGE);
				Integer fkMissinglist = r.get(T_ELEMENT.FK_MISSINGLIST);
				Integer fkParentElement = r.get(T_ELEMENT.FK_PARENT);
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				String label = r.get(T_VARIABLEATTRIBUTE.VALUE);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				JSONB locales = r.get(V_STUDY.LOCALES);
				if (label == null) {
					label = getFacesContext().selectTranslation(translation); // use translation until T_ELEMENTATTRIBUTE exists
				}
				ElementBean bean = new ElementBean(uniqueName, keyElement, fkPrivilege, fkParentElement, translation, label,
						orderNr, shipOrder, study, studygroup, fkMissinglist, locales);
				bean.setName(r.get(T_ELEMENT.NAME));
				bean.getAccessLevel().setAcl(r.get(V_PRIVILEGE.ACL));
				bean.setIsVariable(isVariable);
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add a new element to the hierarchy tree
	 * 
	 * @param name of the new element
	 * @param translation of this element
	 * @param usnr of the user that has privileges on that new element (in this
	 * case, the creator)
	 * @param parentId the id of the parent element for this element
	 * @param languageKeyContainer a container to keep the generated language key;
	 * this is an out parameter and can be used after function call
	 * @param fkMissinglist the missinglist
	 * @return new id of element (key_element)
	 */
	public Integer addElement(String name, JSONB translation, Integer usnr, Integer parentId,
			LambdaResultWrapper languageKeyContainer, Integer fkMissinglist) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				Integer privilegeId = addAndGetPrivilegeId(DSL.using(c), null);
				String studyUniqueName = getStudyUniqueNameOfElement(DSL.using(c), parentId);
				String uniqueName = generateUniqueName(studyUniqueName, name);

				checkUniqueNameDoesntExist(DSL.using(c), uniqueName);

				addUserRWXPrivilege(DSL.using(c), privilegeId, usnr);

				InsertResultStep<TElementRecord> sql = DSL.using(c)
				// @formatter:off
  				.insertInto(T_ELEMENT, 
  				            T_ELEMENT.FK_PARENT, 
  				            T_ELEMENT.TRANSLATION, 
  				            T_ELEMENT.FK_PRIVILEGE, 
  				            T_ELEMENT.NAME,
  				            T_ELEMENT.UNIQUE_NAME,
  				            T_ELEMENT.FK_MISSINGLIST)
  				.values(parentId, translation, privilegeId, name, uniqueName, fkMissinglist)
  				.returning(T_ELEMENT.PK);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				Integer keyElement = sql.fetchOne().getPk();
				lrw.setInteger(keyElement);

				UpdateConditionStep<TPrivilegeRecord> sql2 = DSL.using(c)
				// @formatter:off
						.update(T_PRIVILEGE).set(T_PRIVILEGE.NAME, new StringBuilder("element.").append(keyElement).toString())
						.where(T_PRIVILEGE.PK.eq(privilegeId));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				sql2.execute();
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * check unique name not to be available in the database yet
	 * 
	 * @param jooq the jooq context
	 * @param uniqueName the unique name to be checked
	 */
	private void checkUniqueNameDoesntExist(DSLContext jooq, String uniqueName) throws DataAccessException {
		if (jooq.fetchExists(T_ELEMENT, T_ELEMENT.UNIQUE_NAME.eq(uniqueName))) {
			throw new DataAccessException(getFacesContext().translate("error.study.uniquename.exists", uniqueName));
		}
	}

	/**
	 * update element in db referenced by element's ID (= keyElement)
	 * 
	 * @param bean element to be updated
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer updateElement(ElementBean bean) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				UpdateConditionStep<TElementRecord> sql = DSL.using(c)
				// @formatter:off
					.update(T_ELEMENT)
					.set(T_ELEMENT.TRANSLATION, bean.getTranslation())
					.set(T_ELEMENT.NAME, bean.getName())
					.set(T_ELEMENT.FK_MISSINGLIST, bean.getFkMissinglist())
					.where(T_ELEMENT.PK.eq(bean.getId()));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete element from hierarchy
	 * 
	 * @param id key_element
	 * @return number of affected db rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer deleteElement(Integer id) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				lrw.setInteger(deleteElement(DSL.using(c), id));
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete all children of parent
	 * 
	 * @param jooq DSL context to be used for transactions; if no transaction is
	 * needed, use <b>getJooq()</b>
	 * @param parent id of the element for that all children should be removed
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	private Integer deleteAllChildrenOf(DSLContext jooq, Integer parent) {
		SelectConditionStep<Record2<Integer, Integer>> sql = jooq
		// @formatter:off
				.select(T_ELEMENT.PK, T_VARIABLE.FK_ELEMENT).from(T_ELEMENT).leftJoin(T_VARIABLE)
				.on(T_VARIABLE.FK_ELEMENT.eq(T_ELEMENT.PK)).where(T_ELEMENT.FK_PARENT.eq(parent));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		Integer affected = 0;
		for (Record r : sql.fetch()) {
			boolean isVariable = r.get(T_VARIABLE.FK_ELEMENT) != null;
			Integer keyElement = r.get(T_ELEMENT.PK);
			if (isVariable) {
				affected += deleteVariable(jooq, keyElement);
			} else {
				affected += deleteElement(jooq, keyElement);
			}
		}
		return affected;
	}

	/**
	 * delete element from hierarchy
	 * 
	 * @param jooq context to be used; best, wrap it by a transaction
	 * @param id key_element
	 * @return amount of affected rows
	 * 
	 * if anything went wrong on database side
	 */
	private Integer deleteElement(DSLContext jooq, Integer id) {
		Integer affected = 0;

		affected += deleteAllChildrenOf(jooq, id);

		SelectConditionStep<Record3<Integer, Integer, Integer>> sql2 = jooq
		// @formatter:off
				.select(T_ELEMENT.FK_LANG, T_ELEMENT.FK_PRIVILEGE, T_LANGUAGEKEY.FK_PRIVILEGE).from(T_ELEMENT)
				.leftJoin(T_LANGUAGEKEY).on(T_LANGUAGEKEY.PK.eq(T_ELEMENT.PK)).where(T_ELEMENT.PK.eq(id));
		// @formatter:on
		LOGGER.debug("{}", sql2.toString());
		Integer fkLang = null;
		List<Integer> privileges = new ArrayList<>();
		for (Record r : sql2.fetch()) {
			fkLang = r.get(T_ELEMENT.FK_LANG);
			privileges.add(r.get(T_ELEMENT.FK_PRIVILEGE));
			privileges.add(r.get(T_LANGUAGEKEY.FK_PRIVILEGE));
		}

		DeleteConditionStep<TTranslationRecord> sql5 = jooq
		// @formatter:off
				.deleteFrom(T_TRANSLATION).where(T_TRANSLATION.FK_LANG.eq(fkLang));
		// @formatter:on
		LOGGER.debug("{}", sql5.toString());
		affected += sql5.execute();

		DeleteConditionStep<TElementshipRecord> sql6 = jooq
		// @formatter:off
				.deleteFrom(T_ELEMENTSHIP).where(T_ELEMENTSHIP.FK_ELEMENT.eq(id));
		// @formatter:on
		LOGGER.debug("{}", sql6.toString());
		affected += sql6.execute();

		DeleteConditionStep<TElementRecord> sql7 = jooq
		// @formatter:off
				.deleteFrom(T_ELEMENT).where(T_ELEMENT.PK.eq(id));
		// @formatter:on
		LOGGER.debug("{}", sql7.toString());
		affected += sql7.execute();

		DeleteConditionStep<TTranslationRecord> sql8 = jooq
		// @formatter:off
				.deleteFrom(T_TRANSLATION).where(T_TRANSLATION.FK_LANG
						.in(jooq.select(T_LANGUAGEKEY.PK).from(T_LANGUAGEKEY).where(T_LANGUAGEKEY.FK_PRIVILEGE.in(privileges))));
		// @formatter:on
		LOGGER.debug("{}", sql8.toString());
		affected += sql8.execute();

		DeleteConditionStep<TLanguagekeyRecord> sql9 = jooq
		// @formatter:off
				.deleteFrom(T_LANGUAGEKEY).where(T_LANGUAGEKEY.PK.eq(fkLang)).or(T_LANGUAGEKEY.FK_PRIVILEGE.in(privileges));
		// @formatter:on
		LOGGER.debug("{}", sql9.toString());
		affected += sql9.execute();

		// there might be other elements with the same privileges because of imported
		// data
		if (jooq.fetchCount(T_ELEMENT, T_ELEMENT.FK_PRIVILEGE.in(privileges)) < 1) {
			DeleteConditionStep<TUserprivilegeRecord> sql3 = jooq
			// @formatter:off
					.deleteFrom(T_USERPRIVILEGE).where(T_USERPRIVILEGE.FK_PRIVILEGE.in(privileges));
			// @formatter:on
			LOGGER.debug("{}", sql3.toString());
			affected += sql3.execute();

			DeleteConditionStep<TGroupprivilegeRecord> sql4 = jooq
			// @formatter:off
					.deleteFrom(T_GROUPPRIVILEGE).where(T_GROUPPRIVILEGE.FK_PRIVILEGE.in(privileges));
			// @formatter:on
			LOGGER.debug("{}", sql4.toString());
			affected += sql4.execute();

			DeleteConditionStep<TPrivilegeRecord> sql10 = jooq
			// @formatter:off
				.deleteFrom(T_PRIVILEGE).where(T_PRIVILEGE.PK.in(privileges));
			// @formatter:on
			LOGGER.debug("{}", sql10.toString());
			affected += sql10.execute();
		}

		return affected;
	}

	/**
	 * delete variable
	 * 
	 * @param jooq context to be used
	 * @param id of variable
	 * @return number of affected rows
	 */
	private Integer deleteVariable(DSLContext jooq, Integer id) {
		Integer affected = 0;
		DeleteConditionStep<TConfounderRecord> sql = jooq
		// @formatter:off
				.deleteFrom(T_CONFOUNDER).where(T_CONFOUNDER.FK_ELEMENT.eq(id));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		affected += sql.execute();

		DeleteConditionStep<TVariableattributeRecord> sql1 = jooq
		// @formatter:off
			.deleteFrom(T_VARIABLEATTRIBUTE).where(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(id));
		// @formatter:on
		LOGGER.debug("{}", sql1.toString());
		affected += sql1.execute();

		DeleteConditionStep<TVariableRecord> sql2 = jooq
		// @formatter:off
				.deleteFrom(T_VARIABLE).where(T_VARIABLE.FK_ELEMENT.eq(id));
		// @formatter:on
		LOGGER.debug("{}", sql2.toString());
		affected += sql2.execute();

		affected += deleteElement(jooq, id);
		return affected;
	}

	/**
	 * delete variable from variable table and hierarchy
	 * 
	 * @param id primary key (key_element) of variable to be deleted
	 * @return amount of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer deleteVariable(Integer id) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				lrw.setInteger(deleteVariable(DSL.using(c), id));
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get variable from database referenced by keyElement
	 * 
	 * @param keyElement id of element in hierarchy tree to be loaded
	 * @return variable bean of element if found
	 */
	public VariableBean getVariable(Integer keyElement) {
		if (keyElement == null) {
			throw new DataAccessException("keyElement is null, aborting");
		}

		try (CloseableDSLContext jooq = getJooq()) {
			Integer usnr = getFacesContext().getUsnr();
			SelectLimitPercentStep<Record20<Integer, String, EnumDatatype, Boolean, EnumControlvar, String, Integer, Integer, JSONB, String, String, Integer, Integer, String, String, EnumAccesslevel, Integer, String, Integer, JSONB>> sql1 = jooq
			// @formatter:off
  			.select(T_VARIABLE.VAR_ORDER, 
  			        T_VARIABLE.VALUELIST, 
  			        T_VARIABLE.DATATYPE,
                T_VARIABLE.IDVARIABLE,
  			        T_VARIABLE.CONTROL_TYPE, 
  			        T_VARIABLE.COLUMN_NAME,
                T_VARIABLE.VAR_ORDER, 
  			        T_ELEMENT.FK_PARENT, 
  			        T_ELEMENT.TRANSLATION, 
  			        T_ELEMENT.NAME, 
  			        T_ELEMENT.UNIQUE_NAME, 
  			        T_ELEMENT.FK_PRIVILEGE,
  			        T_ELEMENT.FK_MISSINGLIST,
  			        V_STUDY.STUDY_NAME, 
  			        V_STUDY.STUDYGROUP_NAME, 
  			        V_PRIVILEGE.ACL, 
  			        T_VARIABLEUSAGE.PK, 
  			        T_VARIABLEUSAGE.NAME, 
  			        T_VARIABLEUSAGE.FK_LANG,
  			        V_STUDY.LOCALES)
  			.from(T_VARIABLE)
  			.leftJoin(T_VARIABLEUSAGE).on(T_VARIABLEUSAGE.PK.eq(T_VARIABLE.FK_VARIABLEUSAGE))
  			.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLE.FK_ELEMENT))
  			.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_VARIABLE.FK_ELEMENT))
  			.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_USNR.eq(usnr).and(V_PRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE)))
  			.where(T_VARIABLE.FK_ELEMENT.eq(keyElement))
  			.groupBy(T_VARIABLE.VAR_ORDER, T_VARIABLE.VALUELIST, T_VARIABLE.DATATYPE, T_VARIABLE.IDVARIABLE,
  					T_VARIABLE.CONTROL_TYPE, T_VARIABLE.COLUMN_NAME, T_VARIABLE.VAR_ORDER, T_ELEMENT.TRANSLATION,
  					T_ELEMENT.FK_PARENT, T_ELEMENT.FK_LANG, T_ELEMENT.NAME, T_ELEMENT.UNIQUE_NAME, T_ELEMENT.FK_PRIVILEGE, T_ELEMENT.FK_MISSINGLIST,
  					V_STUDY.STUDY_NAME, V_STUDY.STUDYGROUP_NAME, V_PRIVILEGE.ACL,	T_VARIABLEUSAGE.PK, T_VARIABLEUSAGE.NAME, T_VARIABLEUSAGE.FK_LANG,
  					V_STUDY.LOCALES)
  			.limit(1);
			// @formatter:on
			LOGGER.debug("{}", sql1.toString());
			Record r = sql1.fetchOne();

			SelectConditionStep<Record10<String, String, Integer, String, String, String, Boolean, Boolean, Boolean, String>> sql2 = jooq
			// @formatter:off
        .select(T_VARIABLEATTRIBUTE.NAME,
                T_VARIABLEATTRIBUTE.VALUE,
                T_VARIABLEATTRIBUTEROLE.PK,
                T_VARIABLEATTRIBUTEROLE.NAME,
                T_VARIABLEATTRIBUTEROLE.INPUTWIDGET,
                T_VARIABLEATTRIBUTEROLE.REGEXPATTERN,
                T_VARIABLEATTRIBUTEROLE.HAS_LOCALE,
                T_VARIABLEATTRIBUTEROLE.HAS_PRIMARY_ATTRIBUTE,
                T_VARIABLEATTRIBUTE.IS_PRIMARY_ATTRIBUTE,
                T_VARIABLEATTRIBUTE.LOCALE)
        .from(T_VARIABLEATTRIBUTE)
        .leftJoin(T_VARIABLEATTRIBUTEROLE).on(T_VARIABLEATTRIBUTEROLE.PK.eq(T_VARIABLEATTRIBUTE.FK_ROLE))
        .where(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(keyElement));
      // @formatter:on
			LOGGER.debug("{}", sql2.toString());
			List<VariableAttributeBean> varAttrs = new ArrayList<>();
			for (Record rec : sql2.fetch()) {
				VariableAttributeBean varAttrBean = new VariableAttributeBean();
				String name = rec.get(T_VARIABLEATTRIBUTE.NAME);
				String value = rec.get(T_VARIABLEATTRIBUTE.VALUE);
				Integer pk = rec.get(T_VARIABLEATTRIBUTEROLE.PK);
				String rolename = rec.get(T_VARIABLEATTRIBUTEROLE.NAME);
				String inputwidget = rec.get(T_VARIABLEATTRIBUTEROLE.INPUTWIDGET);
				String regexpattern = rec.get(T_VARIABLEATTRIBUTEROLE.REGEXPATTERN);
				Boolean hasLocale = rec.get(T_VARIABLEATTRIBUTEROLE.HAS_LOCALE);
				Boolean hasPrimaryAttribute = rec.get(T_VARIABLEATTRIBUTEROLE.HAS_PRIMARY_ATTRIBUTE);
				Boolean isPrimaryAttribute = rec.get(T_VARIABLEATTRIBUTE.IS_PRIMARY_ATTRIBUTE);
				String locale = rec.get(T_VARIABLEATTRIBUTE.LOCALE);
				varAttrBean.setName(name);
				varAttrBean.setValue(value);
				varAttrBean.setRole(
						new VariableAttributeRoleBean(pk, rolename, inputwidget, regexpattern, hasPrimaryAttribute, hasLocale));
				varAttrBean.setIsPrimaryAttribute(isPrimaryAttribute);
				varAttrBean.setLocale(locale);
				varAttrs.add(varAttrBean);
			}

			if (r != null) {
				String columnName = r.get(T_VARIABLE.COLUMN_NAME);
				EnumDatatype datatype = r.get(T_VARIABLE.DATATYPE);
				Boolean idvariable = r.get(T_VARIABLE.IDVARIABLE);
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				String name = r.get(T_ELEMENT.NAME);
				Integer fkMissinglist = r.get(T_ELEMENT.FK_MISSINGLIST);
				Integer shipOrder = r.get(T_VARIABLE.VAR_ORDER);
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				JSONB locales = r.get(V_STUDY.LOCALES);
				VariableusageBean variableusage = new VariableusageBean(r.get(T_VARIABLEUSAGE.PK));
				variableusage.setName(r.get(T_VARIABLEUSAGE.NAME));
				variableusage.setFkLang(r.get(T_VARIABLEUSAGE.FK_LANG));
				VariableBean bean = new VariableBean(columnName, datatype.getLiteral(), idvariable, uniqueName, name,
						keyElement, r.get(T_ELEMENT.FK_PARENT), translation, shipOrder, study, studygroup, variableusage,
						fkMissinglist, locales);
				bean.setName(name);
				bean.setRightId(r.get(T_ELEMENT.FK_PRIVILEGE));
				bean.setValuelist(
						new ValuelistConverter().fromJson(getFacesContext().getIsocode(), r.get(T_VARIABLE.VALUELIST)));
				bean.setVarOrder(r.get(T_VARIABLE.VAR_ORDER));
				bean.getMetadatalist().addAll(getMetadatalist(bean.getKeyElement()));
				bean.setControltype(r.get(T_VARIABLE.CONTROL_TYPE));
				bean.setAcl(new AclBean(r.get(V_PRIVILEGE.ACL)));
				bean.getAttributes().addAll(varAttrs);
				return bean;
			} else {
				throw new DataAccessException("no variable found for fk_element = ".concat(keyElement.toString()));
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get list of metadata for keyElement
	 * 
	 * @param keyElement id of element in hierarchy to look up for meta data
	 * @return list of found metadata beans
	 * 
	 * if anything went wrong on database side
	 */
	private List<ConfounderBean> getMetadatalist(Integer keyElement) {
		Map<Integer, ConfounderBean> tmp = new HashMap<>();
		try (SelectConditionStep<Record4<Integer, Integer, String, String>> sql = getJooq()
		// @formatter:off
			.select(T_CONFOUNDER.FK_REFERENCED_ELEMENT, 
					    T_CONFOUNDER.FK_METADATATYPE, 
					    T_METADATATYPE.NAME,
						  T_METADATATYPE.DESCRIPTION)
			.from(T_CONFOUNDER).leftJoin(T_METADATATYPE).on(T_METADATATYPE.PK.eq(T_CONFOUNDER.FK_METADATATYPE))
			.where(T_CONFOUNDER.FK_ELEMENT.eq(keyElement));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				Integer key = r.get(T_CONFOUNDER.FK_REFERENCED_ELEMENT);
				ConfounderBean bean = tmp.get(key);
				if (bean == null) {
					bean = new ConfounderBean();
					Integer pk = r.get(T_CONFOUNDER.FK_METADATATYPE);
					MetadatatypeBean mdt = new MetadatatypeBean(pk);
					mdt.setName(r.get(T_METADATATYPE.NAME));
					mdt.setDescription(r.get(T_METADATATYPE.DESCRIPTION));
					bean.setMetadataType(mdt);
					bean.setFkReferenceElement(key);
					tmp.put(key, bean);
				}
			}
			List<ConfounderBean> list = new ArrayList<>();
			list.addAll(tmp.values());
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add variable to db
	 * 
	 * @param variable element to be added to the database
	 * @param usnr id of the user that has privileges on this element; in this case,
	 * it is the creator and gets rwx
	 * 
	 * @return the id of the element (fkElement)
	 */
	public Integer addVariable(VariableBean variable, Integer usnr) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				Integer fkPrivilege = addAndGetPrivilegeId(DSL.using(c), null);
				String studyUniqueName = getStudyUniqueName(DSL.using(c), variable.getKeyParentElement());
				String uniqueName = generateUniqueName(studyUniqueName, variable.getName());
				if (checkUniqueNameExists(DSL.using(c), uniqueName)) {
					// tell the user the duplicated variable name, but check the unique name not to
					// be there twice
					throw new DataAccessException(getFacesContext().translate("error.name.exists", variable.getName()));
				}
				addUserRWXPrivilege(DSL.using(c), fkPrivilege, usnr);
				InsertResultStep<TElementRecord> sql = DSL.using(c)
				// @formatter:off
					.insertInto(T_ELEMENT, 
					            T_ELEMENT.FK_PARENT, 
					            T_ELEMENT.FK_PRIVILEGE, 
					            T_ELEMENT.NAME,
					            T_ELEMENT.TRANSLATION,
					            T_ELEMENT.UNIQUE_NAME)
					.values(variable.getKeyParentElement(), fkPrivilege, variable.getName(), variable.getTranslation(), uniqueName)
					.returning(T_ELEMENT.PK);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				Integer keyElement = sql.fetchOne().getPk();
				lrw.setInteger(keyElement);

				updatePrivilegeName(DSL.using(c), new StringBuilder("element.").append(keyElement).toString(), fkPrivilege);
				String jsonValuelist = new ValuelistConverter().toJson(getFacesContext().getIsocode(), variable.getValuelist());
				InsertValuesStep5<TVariableRecord, Integer, String, EnumDatatype, String, Boolean> sql3 = DSL.using(c)
				// @formatter:off
					.insertInto(T_VARIABLE, 
					            T_VARIABLE.FK_ELEMENT, 
					            T_VARIABLE.COLUMN_NAME, 
					            T_VARIABLE.DATATYPE, 
					            T_VARIABLE.VALUELIST,
					            T_VARIABLE.IDVARIABLE)
					.values(keyElement, variable.getColumnName(), EnumConverter.toDatatypeEnum(variable.getDatatype()), jsonValuelist, variable.getIdvariable());
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				sql3.execute();

				for (ConfounderBean bean : variable.getMetadatalist()) {
					InsertValuesStep3<TConfounderRecord, Integer, Integer, Integer> sql5 = DSL.using(c)
					// @formatter:off
						.insertInto(T_CONFOUNDER, 
						            T_CONFOUNDER.FK_ELEMENT, 
						            T_CONFOUNDER.FK_METADATATYPE,
						            T_CONFOUNDER.FK_REFERENCED_ELEMENT)
						.values(keyElement, bean.getMetadataType().getPk(), bean.getFkReferenceElement());
					// @formatter:on
					LOGGER.debug("{}", sql5.toString());
					sql5.execute();
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * check if unique name still exists
	 * 
	 * @param jooq the jooq context
	 * @param uniqueName the unique name to be checked
	 * @return true or false
	 */
	private boolean checkUniqueNameExists(DSLContext jooq, String uniqueName) {
		return jooq.fetchExists(T_ELEMENT, T_ELEMENT.UNIQUE_NAME.eq(uniqueName));
	}

	/**
	 * update variable in database
	 * 
	 * @param bean variable that should be updated
	 * 
	 * @return number of affected database changes
	 */
	public Integer updateVariable(VariableBean bean) {
		try (CloseableDSLContext jooq = getJooq()) {
			LambdaResultWrapper lrw = new LambdaResultWrapper();
			jooq.transaction(c -> {
				UpdateConditionStep<TVariableRecord> sql = DSL.using(c)
				// @formatter:off
					.update(T_VARIABLE)
          .set(T_VARIABLE.COLUMN_NAME, bean.getColumnName())
          .set(T_VARIABLE.DATATYPE, EnumConverter.toDatatypeEnum(bean.getDatatype()))
					.set(T_VARIABLE.VALUELIST,
							new ValuelistConverter().toJson(getFacesContext().getIsocode(), bean.getValuelist()))
					.set(T_VARIABLE.IDVARIABLE, bean.getIdvariable())
					.where(T_VARIABLE.FK_ELEMENT.eq(bean.getKeyElement()));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				UpdateConditionStep<TElementRecord> sql3 = DSL.using(c)
				// @formatter:off
					.update(T_ELEMENT)
					.set(T_ELEMENT.FK_PARENT, bean.getKeyParentElement())
					.set(T_ELEMENT.NAME, bean.getName())
					.set(T_ELEMENT.FK_MISSINGLIST, bean.getFkMissinglist())
					.set(T_ELEMENT.TRANSLATION, bean.getTranslation())
					.where(T_ELEMENT.PK.eq(bean.getKeyElement()));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				lrw.addToInteger(sql3.execute());

				List<String> names = new ArrayList<>();
				for (VariableAttributeBean var : bean.getAttributes()) {
					names.add(var.getName());
					InsertOnDuplicateSetMoreStep<TVariableattributeRecord> sql4 = DSL.using(c)
					// @formatter:off
            .insertInto(T_VARIABLEATTRIBUTE,
                        T_VARIABLEATTRIBUTE.FK_VARIABLE,
                        T_VARIABLEATTRIBUTE.NAME,
                        T_VARIABLEATTRIBUTE.VALUE,
                        T_VARIABLEATTRIBUTE.FK_ROLE, 
                        T_VARIABLEATTRIBUTE.IS_PRIMARY_ATTRIBUTE,
                        T_VARIABLEATTRIBUTE.LOCALE)
            .values(bean.getKeyElement(), var.getName(), var.getValue(), var.getRolePk(),
                    var.getIsPrimaryAttribute(), var.getLocale())
            .onConflict(T_VARIABLEATTRIBUTE.FK_VARIABLE, T_VARIABLEATTRIBUTE.NAME)
            .doUpdate()
            .set(T_VARIABLEATTRIBUTE.VALUE, var.getValue())
            .set(T_VARIABLEATTRIBUTE.FK_ROLE, var.getRolePk())
            .set(T_VARIABLEATTRIBUTE.IS_PRIMARY_ATTRIBUTE, var.getIsPrimaryAttribute())
            .set(T_VARIABLEATTRIBUTE.LOCALE, var.getLocale());
          // @formatter:on
					LOGGER.debug("{}", sql4.toString());
					lrw.addToInteger(sql4.execute());
				}

				DeleteConditionStep<TVariableattributeRecord> sql4 = DSL.using(c)
				// @formatter:off
          .deleteFrom(T_VARIABLEATTRIBUTE)
          .where(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(bean.getKeyElement()))
          .and(T_VARIABLEATTRIBUTE.NAME.notIn(names));
        // @formatter:on
				LOGGER.debug("{}", sql4.toString());
				lrw.addToInteger(sql4.execute());

				List<Integer> deleteMeta = new ArrayList<>();
				for (ConfounderBean mb : bean.getMetadatalist()) {
					deleteMeta.add(mb.getFkReferenceElement());

					InsertResultStep<TConfounderRecord> sql5 = DSL.using(c)
					// @formatter:off
						.insertInto(T_CONFOUNDER, 
						            T_CONFOUNDER.FK_ELEMENT, 
						            T_CONFOUNDER.FK_METADATATYPE,
						            T_CONFOUNDER.FK_REFERENCED_ELEMENT)
						.values(bean.getKeyElement(), mb.getMetadataType().getPk(), mb.getFkReferenceElement())
						.onConflict(T_CONFOUNDER.FK_ELEMENT, T_CONFOUNDER.FK_METADATATYPE)
						.doUpdate()
						.set(T_CONFOUNDER.FK_REFERENCED_ELEMENT, mb.getFkReferenceElement())
						.returning(T_CONFOUNDER.PK);
					// @formatter:on
					LOGGER.debug("{}", sql5.toString());
					sql5.fetchOne().getPk();
					lrw.addToInteger(1);
				}
				if (deleteMeta.size() > 0) // at least the variable itself is always there
				{
					DeleteConditionStep<TConfounderRecord> sql5 = DSL.using(c)
					// @formatter:off
    				.deleteFrom(T_CONFOUNDER)
    				.where(T_CONFOUNDER.FK_ELEMENT.eq(bean.getKeyElement()))
    				.and(T_CONFOUNDER.FK_REFERENCED_ELEMENT.notIn(deleteMeta));
					// @formatter:on
					LOGGER.debug("{}", sql5.toString());
					lrw.addToInteger(sql5.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all parent elements of element
	 * 
	 * @param elementId id of the element to look for parents
	 * @return list of found parents, ordered by the hierarchy, last element is the
	 * root
	 * 
	 * if anything went wrong on database side
	 */
	public List<ElementBean> getAllParentElements(Integer elementId) {
		Integer usnr = getFacesContext().getUsnr();
		TElement parents = T_ELEMENT.as("parents");
		try (
				SelectOnConditionStep<Record14<Integer, Integer, Integer, String, String, JSONB, Integer, Integer, Integer, String, String, EnumAccesslevel, Integer, JSONB>> sql = getJooq()
				// @formatter:off
					.withRecursive(DSL.name("parents").fields(T_ELEMENT.FK_PRIVILEGE.getName(), 
					                                          T_ELEMENT.PK.getName(), 
					                                          T_ELEMENT.FK_PARENT.getName(),
					                                          T_ELEMENT.TRANSLATION.getName()).as(DSL
            .select(T_ELEMENT.FK_PRIVILEGE, 
                    T_ELEMENT.PK, 
                    T_ELEMENT.FK_PARENT, 
                    T_ELEMENT.TRANSLATION)
						.from(T_ELEMENT)
						.where(T_ELEMENT.PK.eq(elementId))
						.unionAll(DSL
						  .select(T_ELEMENT.FK_PRIVILEGE, 
						           T_ELEMENT.PK, 
						           T_ELEMENT.FK_PARENT, 
						           T_ELEMENT.TRANSLATION)
							.from(DSL.table("parents"))
							.innerJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(parents.FK_PARENT)))))
					.select(parents.PK, 
					        parents.FK_PARENT, 
					        parents.FK_PRIVILEGE, 
					        T_ELEMENT.NAME, 
					        T_ELEMENT.UNIQUE_NAME,
					        parents.TRANSLATION, 
					        T_ELEMENT.ORDER_NR, 
					        T_ELEMENT.FK_MISSINGLIST,
					        T_VARIABLE.VAR_ORDER, 
					        V_STUDY.STUDY_NAME, 
					        V_STUDY.STUDYGROUP_NAME,
					        V_PRIVILEGE.ACL, 
					        T_VARIABLE.FK_ELEMENT, 
					        V_STUDY.LOCALES)
					.from(DSL.table("parents"))
					.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(parents.PK))
					.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(parents.PK))
					.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(parents.PK))
					.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
					                      .and(V_PRIVILEGE.FK_USNR.eq(usnr));
        // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Map<Integer, ElementBean> m = new HashMap<>();
			for (Record r : sql.fetch()) {
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				Integer shipOrder = r.get(T_VARIABLE.VAR_ORDER);
				Integer keyElement = r.get(T_ELEMENT.PK);
				Integer fkPrivilege = r.get(T_ELEMENT.FK_PRIVILEGE);
				Integer fkParentElement = r.get(T_ELEMENT.FK_PARENT);
				Integer orderNr = r.get(T_ELEMENT.ORDER_NR);
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				JSONB locales = r.get(V_STUDY.LOCALES);
				String label = getFacesContext().selectTranslation(translation); // use translation as label until
																																					// T_ELEMENTATTRIBUTE exists
				String varShortLabel = getFacesContext().selectTranslation(translation); // use translation as short label
				Integer fkMissinglist = r.get(T_ELEMENT.FK_MISSINGLIST);
				ElementBean bean = new ElementBean(uniqueName, keyElement, fkPrivilege, fkParentElement, translation, label,
						orderNr, shipOrder, study, studygroup, fkMissinglist, locales);
				bean.setName(r.get(T_ELEMENT.NAME));
				bean.setIsVariable(r.get(T_VARIABLE.FK_ELEMENT) != null);
				bean.getAccessLevel().setAcl(new AclBean(r.get(V_PRIVILEGE.ACL)));
				bean.setTitle(varShortLabel);
				m.put(bean.getParent(), bean);
			}
			// sort by hierarchy order, not by key_element
			List<ElementBean> sortedList = new ArrayList<>();
			sortedList.add(m.get(null));
			try {
				if (sortedList.size() > 0) {
					Integer parent = sortedList.get(0).getId();
					while (m.get(parent) != null) {
						sortedList.add(m.get(parent));
						parent = m.get(parent).getId();
					}
				}
			} catch (NullPointerException e) {
				LOGGER.warn("this element has no parents, returning empty list instead");
				return new ArrayList<>();
			}
			return sortedList;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get list of all reference variables of variable referenced by parent without
	 * technical ones
	 * 
	 * @param parent the id of the parent element
	 * @return list of beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<ElementBean> getAllReferenceVariablesByParent(Integer parent) {
		try (
				SelectLimitStep<Record14<String, String, Integer, Integer, JSONB, Integer, String, Integer, Integer, Integer, String, String, EnumControlvar, JSONB>> sql = getJooq()
				// @formatter:off
					.select(T_ELEMENT.NAME, 
					        T_ELEMENT.UNIQUE_NAME,
					        T_ELEMENT.FK_PARENT,
					        T_ELEMENT.FK_PRIVILEGE,
					        T_ELEMENT.TRANSLATION, 
					        T_ELEMENT.PK,
					        T_ELEMENT.UNIQUE_NAME, 
					        T_ELEMENT.FK_MISSINGLIST,
					        T_VARIABLE.VAR_ORDER, 
					        T_VARIABLE.VAR_ORDER,
					        V_STUDY.STUDY_NAME,  
					        V_STUDY.STUDYGROUP_NAME,
					        T_VARIABLE.CONTROL_TYPE,
					        V_STUDY.LOCALES)
					.from(T_VARIABLE)
					.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLE.FK_ELEMENT))
					.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_ELEMENT.PK))
					.where(T_ELEMENT.FK_PARENT.eq(parent))
					.orderBy(1);
        // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<ElementBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				Integer orderNr = r.get(T_VARIABLE.VAR_ORDER);
				Integer shipOrder = r.get(T_VARIABLE.VAR_ORDER);
				Integer keyElement = r.get(T_ELEMENT.PK);
				Integer fkPrivilege = r.get(T_ELEMENT.FK_PRIVILEGE);
				Integer fkParentElement = r.get(T_ELEMENT.FK_PARENT);
				Integer fkMissinglist = r.get(T_ELEMENT.FK_MISSINGLIST);
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				JSONB locales = r.get(V_STUDY.LOCALES);
				String label = getFacesContext().selectTranslation(translation); // use translation until T_ELEMENTATTRIBUTE
																																					// exists
				ElementBean bean = new ElementBean(uniqueName, keyElement, fkPrivilege, fkParentElement, translation, label,
						orderNr, shipOrder, study, studygroup, fkMissinglist, locales);
				bean.setName(r.get(T_ELEMENT.NAME));
				EnumControlvar technical = r.get(T_VARIABLE.CONTROL_TYPE);
				if (technical == null) {
					list.add(bean);
				}
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all neighbor variables without technical ones
	 * 
	 * @param keyElement id of the element
	 * @return list of element beans
	 */
	public List<ElementBean> getAllNeighbourVariables(Integer keyElement) {
		try (
				SelectLimitStep<Record12<String, String, Integer, Integer, JSONB, Integer, Integer, Integer, String, String, EnumControlvar, JSONB>> sql = getJooq()
				// @formatter:off
					.select(T_ELEMENT.NAME, 
					        T_ELEMENT.UNIQUE_NAME,
					        T_ELEMENT.FK_PARENT,
					        T_ELEMENT.FK_PRIVILEGE,
					        T_ELEMENT.TRANSLATION, 
					        T_ELEMENT.PK, 
					        T_ELEMENT.FK_MISSINGLIST,
					        T_VARIABLE.VAR_ORDER, 
					        V_STUDY.STUDY_NAME,
					        V_STUDY.STUDYGROUP_NAME, 
					        T_VARIABLE.CONTROL_TYPE,
					        V_STUDY.LOCALES)
					.from(T_VARIABLE)
					.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLE.FK_ELEMENT))
					.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_ELEMENT.PK))
					.where(T_ELEMENT.FK_PARENT.in(getJooq()
					  .select(T_ELEMENT.FK_PARENT)
					  .from(T_ELEMENT)
					  .where(T_ELEMENT.PK.eq(keyElement))))
					.orderBy(1);
        // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<ElementBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				Integer shipOrder = r.get(T_VARIABLE.VAR_ORDER);
				Integer fkKeyElement = r.get(T_ELEMENT.PK);
				Integer fkPrivilege = r.get(T_ELEMENT.FK_PRIVILEGE);
				Integer fkParentElement = r.get(T_ELEMENT.FK_PARENT);
				Integer fkMissinglist = r.get(T_ELEMENT.FK_MISSINGLIST);
				Integer varOrder = r.get(T_VARIABLE.VAR_ORDER);
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				JSONB locales = r.get(V_STUDY.LOCALES);
				String label = getFacesContext().selectTranslation(translation); // use translation until T_ELEMENTATTRIBUTE
																																					// exists
				ElementBean bean = new ElementBean(uniqueName, fkKeyElement, fkPrivilege, fkParentElement, translation, label,
						varOrder, shipOrder, study, studygroup, fkMissinglist, locales);
				bean.setName(r.get(T_ELEMENT.NAME));
				EnumControlvar technical = r.get(T_VARIABLE.CONTROL_TYPE);
				if (technical == null) {
					list.add(bean);
				}
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete content from T_METADATA
	 * 
	 * @param metadatatype the metadatatype as a string
	 * @param keyElement the element of the type
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer deleteMetadata(String metadatatype, Integer keyElement) {
		if (metadatatype == null) {
			throw new DataAccessException("metadatatype is null, aborted.");
		}
		EnumMetadatatype mdt = new EnumConverter().getEnumMetadatatype(metadatatype);
		if (mdt == null) {
			throw new DataAccessException("metadatatype " + metadatatype + " unknown, aborted.");
		}
		try (DeleteConditionStep<TConfounderRecord> sql = getJooq()
		// @formatter:off
				.deleteFrom(T_CONFOUNDER).where(T_CONFOUNDER.FK_METADATATYPE.eq(-1)) // TODO: damaged code to make it deployable
																																							// for the moment
				.and(T_CONFOUNDER.FK_ELEMENT.eq(keyElement));
		// @formatter:on
		) {
			LOGGER.info("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get department (element of hierarchy)
	 * 
	 * @param keyElement the id of the element
	 * @return the department as a bean
	 * 
	 * if anything went wrong on database side
	 */
	public ElementBean getDepartment(Integer keyElement) {
		Integer usnr = getFacesContext().getUsnr();
		try (
				SelectConditionStep<Record12<Integer, String, Integer, JSONB, String, String, Integer, Integer, String, String, EnumAccesslevel, JSONB>> sql = getJooq()
				// @formatter:off
  				.select(T_ELEMENT.FK_PARENT, 
  				        T_ELEMENT.UNIQUE_NAME, 
  				        T_ELEMENT.FK_PRIVILEGE, 
  				        T_ELEMENT.TRANSLATION, 
  				        T_ELEMENT.NAME,
  				        T_ELEMENT.UNIQUE_NAME,
  				        T_ELEMENT.ORDER_NR,
  				        T_ELEMENT.FK_MISSINGLIST,
  				        V_STUDY.STUDY_NAME,
  				        V_STUDY.STUDYGROUP_NAME,
  				        V_PRIVILEGE.ACL,
  				        V_STUDY.LOCALES)
  				.from(T_ELEMENT)
  				.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_ELEMENT.PK))
  				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE)).and(V_PRIVILEGE.FK_USNR.eq(usnr))
  				.where(T_ELEMENT.PK.eq(keyElement));
        // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			// only because of group privileges, there might be multiple results; use first
			// one
			for (Record r : sql.fetch()) {
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				Integer shipOrder = r.get(T_ELEMENT.ORDER_NR);
				Integer fkPrivilege = r.get(T_ELEMENT.FK_PRIVILEGE);
				Integer fkParentElement = r.get(T_ELEMENT.FK_PARENT);
				Integer orderNr = r.get(T_ELEMENT.ORDER_NR);
				Integer fkMissinglist = r.get(T_ELEMENT.FK_MISSINGLIST);
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				JSONB locales = r.get(V_STUDY.LOCALES);
				String label = getFacesContext().selectTranslation(translation); // use translation until T_ELEMENTATTRIBUTE
																																					// exists
				ElementBean bean = new ElementBean(uniqueName, keyElement, fkPrivilege, fkParentElement, translation, label,
						orderNr, shipOrder, study, studygroup, fkMissinglist, locales);
				bean.setMissinglistReadable(getMissinglistReadable(fkMissinglist));
				bean.setName(r.get(T_ELEMENT.NAME));
				bean.setIsVariable(false);
				bean.getAccessLevel().setAcl(r.get(V_PRIVILEGE.ACL));
				return bean;
			}
			throw new DataAccessException("no such element found with key_element = " + keyElement);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get a readable version of the missinglist - simply list all missings
	 * 
	 * @param fkMissinglist the id of the missinglist
	 * @return an overview of missings; the translation of inheritance if fkMissing
	 * is null
	 * @throws SQLException on sql errors
	 * @throws ClassNotFoundException on not found classes
	 */
	public String getMissinglistReadable(Integer fkMissinglist) throws ClassNotFoundException, SQLException {
		if (fkMissinglist == null) {
			return getFacesContext().translate("label.missinglist.inherit");
		} else {
			SelectConditionStep<Record3<String, String, String>> sql = getJooq()
			// @formatter:off
        .select(T_MISSING.CODE,
                T_MISSING.NAME_IN_STUDY,
                T_MISSINGTYPE.NAME)
        .from(T_MISSING)
        .leftJoin(T_MISSINGTYPE).on(T_MISSINGTYPE.PK.eq(T_MISSING.FK_MISSINGTYPE))
        .where(T_MISSING.FK_MISSINGLIST.eq(fkMissinglist));
      // @formatter:on
			LOGGER.debug("{}", sql.toString());
			StringBuilder buf = new StringBuilder();
			boolean first = true;
			for (Record r : sql.fetch()) {
				buf.append(first ? "" : ", ");
				buf.append(r.get(T_MISSING.NAME_IN_STUDY));
				buf.append(" (").append(r.get(T_MISSING.CODE)).append(", ").append(r.get(T_MISSINGTYPE.NAME)).append(")");
				first = false;
			}
			return buf.toString();
		}
	}

	/**
	 * get all variables with parent = id
	 * 
	 * @param id the id of the parent element
	 * @return a list of variable beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<VariableBean> getAllVariablesOf(Integer id) {
		List<VariableBean> list = new ArrayList<>();
		try (
				SelectConditionStep<Record17<String, String, Integer, Integer, JSONB, Integer, Integer, String, EnumDatatype, Boolean, String, String, String, Integer, String, Integer, JSONB>> sql = getJooq()
				// @formatter:off
  				.select(T_ELEMENT.NAME,
  				        T_ELEMENT.UNIQUE_NAME, 
  				        T_VARIABLE.VAR_ORDER, 
  				        T_ELEMENT.FK_PARENT,
  				        T_ELEMENT.TRANSLATION,
  				        T_ELEMENT.FK_MISSINGLIST,
  				        T_VARIABLE.VAR_ORDER, 
  				        T_VARIABLE.COLUMN_NAME,
  				        T_VARIABLE.DATATYPE,
                  T_VARIABLE.IDVARIABLE,
  				        V_STUDY.STUDY_NAME, 
  				        V_STUDY.STUDYGROUP_NAME,
  				        T_VARIABLEATTRIBUTE.VALUE, 
  				        T_VARIABLEUSAGE.PK, 
  				        T_VARIABLEUSAGE.NAME, 
  				        T_VARIABLEUSAGE.FK_LANG,
  				        V_STUDY.LOCALES)
  				.from(T_VARIABLE)
  				.leftJoin(T_VARIABLEUSAGE).on(T_VARIABLEUSAGE.PK.eq(T_VARIABLE.FK_VARIABLEUSAGE))
  				.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLE.FK_ELEMENT))
  				.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_VARIABLE.FK_ELEMENT))
  				.leftJoin(T_VARIABLEATTRIBUTE).on(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(T_VARIABLE.FK_ELEMENT)
                                        .and(T_VARIABLEATTRIBUTE.IS_PRIMARY_ATTRIBUTE.eq(true)))
  				.where(T_ELEMENT.FK_PARENT.eq(id));
        // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				String columnName = r.get(T_VARIABLE.COLUMN_NAME);
				EnumDatatype datatype = r.get(T_VARIABLE.DATATYPE);
				Boolean idvariable = r.get(T_VARIABLE.IDVARIABLE);
				Integer fkParent = r.get(T_ELEMENT.FK_PARENT);
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				String name = r.get(T_ELEMENT.NAME);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				Integer fkMissinglist = r.get(T_ELEMENT.FK_MISSINGLIST);
				Integer shipOrder = r.get(T_VARIABLE.VAR_ORDER);
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				JSONB locales = r.get(V_STUDY.LOCALES);
				String label = r.get(T_VARIABLEATTRIBUTE.VALUE);
				VariableusageBean variableusage = new VariableusageBean(r.get(T_VARIABLEUSAGE.PK));
				variableusage.setName(r.get(T_VARIABLEUSAGE.NAME));
				variableusage.setFkLang(r.get(T_VARIABLEUSAGE.FK_LANG));
				VariableBean bean = new VariableBean(columnName, datatype.getLiteral(), idvariable, uniqueName, name, id,
						fkParent, translation, shipOrder, study, studygroup, variableusage, fkMissinglist, locales);
				bean.setVarOrder(r.get(T_VARIABLE.VAR_ORDER));
				list.add(bean);
			}
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all elements from hierarchy tree with parent = null (this means study
	 * groups)
	 * 
	 * @return a list of study group beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<StudygroupBean> getAllStudygroups() {
		TElement S = T_ELEMENT.as("s");
		Name STUDIES = DSL.name("studies");
		List<StudygroupBean> list = new ArrayList<>();
		try (SelectHavingStep<Record5<Integer, String, JSONB, String[], JSONB>> sql = getJooq()
		// @formatter:off
			.select(T_ELEMENT.PK, 
			        T_ELEMENT.NAME, 
			        T_ELEMENT.TRANSLATION,
			        DSL.arrayAgg(S.NAME).as(STUDIES),
			        T_STUDYGROUP.LOCALES)
			.from(T_ELEMENT)
			.leftJoin(T_STUDYGROUP).on(T_STUDYGROUP.FK_ELEMENT.eq(T_ELEMENT.PK))
			.leftJoin(S).on(S.FK_PARENT.eq(T_ELEMENT.PK))
			.where(T_ELEMENT.FK_PARENT.isNull())
			.groupBy(T_ELEMENT.PK, T_ELEMENT.NAME, T_ELEMENT.TRANSLATION, T_STUDYGROUP.LOCALES);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				StringBuilder buf = new StringBuilder();
				boolean first = true;
				for (String s : r.get(STUDIES, String[].class)) {
					buf.append(first ? "" : ",").append(s);
					first = false;
				}
				String result = buf.toString();
				if ("null".equals(result)) {
					result = " - ";
				}
				list.add(new StudygroupBean(r.get(T_ELEMENT.PK), r.get(T_ELEMENT.NAME), r.get(T_ELEMENT.TRANSLATION), result,
						r.get(T_STUDYGROUP.LOCALES)));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * save metadata of bean
	 * 
	 * @param metadatalist the list of meta data beans
	 * @param keyElement the id of the element
	 * 
	 * if anything went wrong on database side
	 */
	public void saveMetadata(List<ConfounderBean> metadatalist, Integer keyElement) {
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				List<Integer> refs = new ArrayList<>();
				for (ConfounderBean bean : metadatalist) {
					InsertResultStep<TConfounderRecord> sql = DSL.using(c)
					// @formatter:off
						.insertInto(T_CONFOUNDER, 
						            T_CONFOUNDER.FK_ELEMENT, 
						            T_CONFOUNDER.FK_METADATATYPE,
						            T_CONFOUNDER.FK_REFERENCED_ELEMENT)
						.values(keyElement, bean.getMetadataType().getPk(), bean.getFkReferenceElement())
						.onConflict(T_CONFOUNDER.FK_ELEMENT, T_CONFOUNDER.FK_METADATATYPE)
						.doUpdate()
						.set(T_CONFOUNDER.FK_REFERENCED_ELEMENT, bean.getFkReferenceElement())
						.returning(T_CONFOUNDER.PK);
					// @formatter:on
					LOGGER.debug("{}", sql.toString());
					Integer pk = sql.fetchOne().getPk();
					if (pk == null) {
						throw new DataAccessException("returning pk is null for " + sql.toString());
					}
					refs.add(bean.getFkReferenceElement());
				}
				Integer queries = metadatalist.size();
				if (queries > 0) {
					DeleteConditionStep<TConfounderRecord> sql = DSL.using(c)
					// @formatter:off
						.deleteFrom(T_CONFOUNDER)
						.where(T_CONFOUNDER.FK_ELEMENT.eq(keyElement))
						.and(T_CONFOUNDER.FK_REFERENCED_ELEMENT.notIn(refs));
					// @formatter:on
					LOGGER.debug("{}", sql.toString());
					sql.execute();
				}
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get summary of metadata from this parentId's children
	 * 
	 * @param parentId the id of the parent element
	 * @return a list ofdept meta beans
	 * @throws GatewayException if there is a gateway error
	 */
	public List<DeptmetaBean> getSummarizedMetadata(Integer parentId) throws GatewayException {
		Integer totalVariables = getCountVariablesUnder(parentId);

		String R = "r";
		Field<Integer> K = DSL.field("k", Integer.class);
		Field<Integer> P = DSL.field("p", Integer.class);
		Field<Integer> AMOUNT = DSL.field("amount", Integer.class);
		Field<String[]> REFERENCED = DSL.field("referenced", String[].class);
		Field<Integer[]> AFFECTED = DSL.field("affected", Integer[].class);

		TElement REF = T_ELEMENT.as("ref");

		try (CloseableDSLContext jooq = getJooq()) {

			SelectHavingStep<Record7<Integer, String, Integer, String, Integer, String[], Integer[]>> sql = jooq
			// @formatter:off
				.withRecursive(R, K.getName(), P.getName()).as(jooq
				  .select(T_ELEMENT.PK, 
				          T_ELEMENT.FK_PARENT)
				  .from(T_ELEMENT)
				  .where(T_ELEMENT.FK_PARENT.eq(parentId))
					.unionAll(jooq
					  .select(T_ELEMENT.PK, 
					          T_ELEMENT.FK_PARENT)
					  .from(T_ELEMENT)
					  .innerJoin(R).on(T_ELEMENT.FK_PARENT.eq(K))))
				.select(T_CONFOUNDER.FK_REFERENCED_ELEMENT, 
				        T_METADATATYPE.NAME, 
				        T_METADATATYPE.PK, 
				        T_ELEMENT.UNIQUE_NAME,
				        DSL.count(T_CONFOUNDER.FK_REFERENCED_ELEMENT).as(AMOUNT), 
				        DSL.arrayAgg(REF.UNIQUE_NAME).as(REFERENCED),
				        DSL.arrayAgg(REF.PK).as(AFFECTED))
				.from(R)
				.leftJoin(T_CONFOUNDER).on(T_CONFOUNDER.FK_ELEMENT.eq(K))
				.leftJoin(T_METADATATYPE).on(T_METADATATYPE.PK.eq(T_CONFOUNDER.FK_METADATATYPE))
				.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_CONFOUNDER.FK_REFERENCED_ELEMENT))
				.leftJoin(REF).on(REF.PK.eq(T_CONFOUNDER.FK_ELEMENT))
				.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(K))
				.where(T_CONFOUNDER.FK_REFERENCED_ELEMENT.isNotNull())
				.groupBy(T_CONFOUNDER.FK_REFERENCED_ELEMENT, T_METADATATYPE.NAME, T_ELEMENT.UNIQUE_NAME, T_METADATATYPE.PK);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			List<DeptmetaBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_CONFOUNDER.FK_REFERENCED_ELEMENT);
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				String typeName = r.get(T_METADATATYPE.NAME);
				Integer fkMetadatatype = r.get(T_METADATATYPE.PK);
				// omit translation of this number
				String myAmount = new StringBuilder(" ").append(r.get(AMOUNT)).append(" / ").append(totalVariables).toString();
				String[] referenced = r.get(REFERENCED);
				Integer[] affected = r.get(AFFECTED);
				List<String> affectedUniqueNames = Arrays.asList(referenced);
				List<Integer> affectedPks = Arrays.asList(affected);
				list.add(new DeptmetaBean(pk, uniqueName, typeName, myAmount, affectedUniqueNames, fkMetadatatype, affectedPks,
						true));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * count magnitude of variables in this department
	 * 
	 * @param parentId id of the department
	 * @return number of found variables; at least 0
	 */
	private Integer getCountVariablesUnder(Integer parentId) {
		String X = "x";
		Field<Integer> K = DSL.field("k", Integer.class);
		Field<Integer> P = DSL.field("p", Integer.class);
		Field<Integer> AMOUNT = DSL.field("amount", Integer.class);
		try (CloseableDSLContext jooq = getJooq()) {
			SelectOnConditionStep<Record1<Integer>> sql = jooq
			// @formatter:off
				.withRecursive(X, K.getName(), P.getName())
				.as(jooq
				  .select(T_ELEMENT.PK, 
				          T_ELEMENT.FK_PARENT)
				  .from(T_ELEMENT)
				  .where(T_ELEMENT.PK.eq(parentId))
					.unionAll(jooq
					  .select(T_ELEMENT.PK, 
					          T_ELEMENT.FK_PARENT)
					  .from(T_ELEMENT)
					  .innerJoin(X).on(K.eq(T_ELEMENT.FK_PARENT))))
				.select(DSL.count().as(AMOUNT))
				.from(X)
				.innerJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(K));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			return sql.fetchOne().get(AMOUNT);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all the study variables under id
	 * 
	 * @param id the id of the parent element
	 * @return a list of found element beans
	 */
	public List<ElementBean> getAllStudyVariablesUnder(Integer id) {
		TElement R = T_ELEMENT.as("r");
		try (CloseableDSLContext jooq = getJooq()) {
			SelectSeekStep1<Record12<Integer, Integer, Integer, String, String, JSONB, Integer, Integer, String, String, String, JSONB>, String> sql = jooq
			// @formatter:off
				.withRecursive(R.getName())
				.as(jooq
					.select(T_ELEMENT.PK, 
					        T_ELEMENT.FK_PARENT, 
					        T_ELEMENT.FK_PRIVILEGE, 
					        T_ELEMENT.NAME, 
					        T_ELEMENT.UNIQUE_NAME,
					        T_ELEMENT.TRANSLATION,
					        T_ELEMENT.FK_MISSINGLIST)
						.from(T_ELEMENT)
						.where(T_ELEMENT.FK_PARENT.eq(id))
						.unionAll(jooq
							.select(T_ELEMENT.PK, 
							        T_ELEMENT.FK_PARENT, 
							        T_ELEMENT.FK_PRIVILEGE,
							        T_ELEMENT.NAME,
							        T_ELEMENT.UNIQUE_NAME, 
							        T_ELEMENT.TRANSLATION,
							        T_ELEMENT.FK_MISSINGLIST)
							.from(T_ELEMENT)
							.innerJoin("r").on(R.PK.eq(T_ELEMENT.FK_PARENT))))
				.select(R.PK, 
				        R.FK_PRIVILEGE, 
				        R.FK_PARENT, 
				        R.NAME,
				        R.UNIQUE_NAME,
				        R.TRANSLATION, 
				        R.FK_MISSINGLIST,
				        T_VARIABLE.VAR_ORDER,
				        V_STUDY.STUDY_NAME,
				        V_STUDY.STUDYGROUP_NAME, 
				        T_VARIABLEATTRIBUTE.VALUE, 
				        V_STUDY.LOCALES)
				.from(T_VARIABLE)
				.leftJoin(R.getName()).on(R.PK.eq(T_VARIABLE.FK_ELEMENT))
				.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(R.PK))
        .leftJoin(T_VARIABLEATTRIBUTE).on(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(T_VARIABLE.FK_ELEMENT)
            .and(T_VARIABLEATTRIBUTE.IS_PRIMARY_ATTRIBUTE.eq(true)))
				.where(R.PK.isNotNull())
				.and(T_VARIABLE.CONTROL_TYPE.isNull())
				.orderBy(R.NAME);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			List<ElementBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				String uniqueName = r.get(R.UNIQUE_NAME);
				Integer fkElement = r.get(R.PK);
				Integer privilegeId = r.get(R.FK_PRIVILEGE);
				Integer fkParentElement = r.get(R.FK_PARENT);
				Integer fkMissinglist = r.get(R.FK_MISSINGLIST);
				Integer orderNr = r.get(T_VARIABLE.VAR_ORDER);
				Integer shipOrder = orderNr; // TODO: get rid of the shipOrder, should be done by importing from SHIP instead
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				String label = r.get(T_VARIABLEATTRIBUTE.VALUE);
				String varShortLabel = label; // for now, use label instead of possible short label from attributes (naming
																			// not specific enough yet)
				JSONB translation = r.get(R.TRANSLATION);
				JSONB locales = r.get(V_STUDY.LOCALES);
				ElementBean bean = new ElementBean(uniqueName, fkElement, privilegeId, fkParentElement, translation, label,
						orderNr, shipOrder, study, studygroup, fkMissinglist, locales);
				bean.setName(r.get(R.NAME));
				bean.setTitle(varShortLabel);
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all variables under parent id
	 * 
	 * @param id of parent
	 * @param includeTechnical include technical variables?
	 * @return a list of elements, at least an empty one s
	 */
	public List<ElementBean> getAllVariablesUnder(Integer id, boolean includeTechnical) {
		TElement R = T_ELEMENT.as("r");
		try (CloseableDSLContext jooq = getJooq()) {
			SelectSeekStep1<Record12<Integer, Integer, Integer, String, String, JSONB, Integer, Integer, String, String, String, JSONB>, String> sql = getJooq()
			// @formatter:off
				.withRecursive(R.getName()).as(jooq
				  .select(T_ELEMENT.PK, 
				          T_ELEMENT.FK_PARENT, 
				          T_ELEMENT.FK_PRIVILEGE,
				          T_ELEMENT.TRANSLATION,
				          T_ELEMENT.NAME, 
				          T_ELEMENT.UNIQUE_NAME,
				          T_ELEMENT.FK_MISSINGLIST)
				  .from(T_ELEMENT)
					.where(T_ELEMENT.FK_PARENT.eq(id))
					.unionAll(jooq
					  .select(T_ELEMENT.PK, 
					          T_ELEMENT.FK_PARENT, 
					          T_ELEMENT.FK_PRIVILEGE,  
					          T_ELEMENT.TRANSLATION,
					          T_ELEMENT.NAME, 
					          T_ELEMENT.UNIQUE_NAME,
					          T_ELEMENT.FK_MISSINGLIST)
					  .from(T_ELEMENT)
					  .innerJoin("r").on(R.PK.eq(T_ELEMENT.FK_PARENT))))
				.select(R.PK, 
				        R.FK_PRIVILEGE, 
				        R.FK_PARENT, 
				        R.NAME, 
				        R.UNIQUE_NAME,
				        R.TRANSLATION,
				        R.FK_MISSINGLIST,
				        T_VARIABLE.VAR_ORDER,
				        V_STUDY.STUDY_NAME, 
				        V_STUDY.STUDYGROUP_NAME, 
				        T_VARIABLEATTRIBUTE.VALUE,
				        V_STUDY.LOCALES)
				.from(T_VARIABLE)
				.leftJoin(R.getName()).on(R.PK.eq(T_VARIABLE.FK_ELEMENT))
				.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(R.PK))
				.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(R.PK))
        .leftJoin(T_VARIABLEATTRIBUTE).on(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(T_VARIABLE.FK_ELEMENT)
            .and(T_VARIABLEATTRIBUTE.IS_PRIMARY_ATTRIBUTE.eq(true)))
				.where(R.PK.isNotNull())
				.and(includeTechnical ? T_ELEMENT.PK.eq(T_ELEMENT.PK) : T_VARIABLE.CONTROL_TYPE.isNull())
				.orderBy(T_ELEMENT.NAME);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			List<ElementBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				String uniqueName = r.get(R.UNIQUE_NAME);
				Integer fkElement = r.get(R.PK);
				Integer privilegeId = r.get(R.FK_PRIVILEGE);
				Integer fkParentElement = r.get(R.FK_PARENT);
				Integer fkMissinglist = r.get(R.FK_MISSINGLIST);
				Integer orderNr = r.get(T_VARIABLE.VAR_ORDER);
				Integer shipOrder = orderNr; // TODO: get rid of the shipOrder, should be done by importing from SHIP instead
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				String label = r.get(T_VARIABLEATTRIBUTE.VALUE);
				JSONB translation = r.get(R.TRANSLATION);
				JSONB locales = r.get(V_STUDY.LOCALES);
				ElementBean bean = new ElementBean(uniqueName, fkElement, privilegeId, fkParentElement, translation, label,
						orderNr, shipOrder, study, studygroup, fkMissinglist, locales);
				bean.setName(r.get(R.NAME));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * upsert the deptmeta bean
	 * 
	 * @param list the list of dept metas
	 * @return the number of affected database rows
	 */
	public Integer upsertMetadata(List<DeptmetaBean> list) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				for (DeptmetaBean bean : list) {
					for (Integer fkElement : bean.getAffectedPks()) {
						if (bean.getPk() != null) {
							InsertOnDuplicateSetMoreStep<TConfounderRecord> sql = DSL.using(c)
							// @formatter:off
								.insertInto(T_CONFOUNDER, 
								            T_CONFOUNDER.FK_ELEMENT, 
								            T_CONFOUNDER.FK_REFERENCED_ELEMENT,
								            T_CONFOUNDER.FK_METADATATYPE)
								.values(fkElement, bean.getPk(), bean.getFkMetadatatype())
								.onConflict(T_CONFOUNDER.FK_ELEMENT, T_CONFOUNDER.FK_METADATATYPE).doUpdate()
								.set(T_CONFOUNDER.FK_REFERENCED_ELEMENT, bean.getPk());
							// @formatter:on
							LOGGER.debug("{}", sql.toString());
							lrw.addToInteger(sql.execute());
						} else {
							LOGGER.warn("found a metadata reference with referenced_element = null: " + bean.toString());
						}
					}
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add confounder to all variables that do not yet have this confounder
	 * reference
	 * 
	 * @param bean the confounder representated as a bean
	 * @return number of affected database rows
	 * @deprecated replaced by upsertMetadata(List<DeptmetaBean>)
	 */
	@Deprecated
	public Integer addMetadataToAll(ConfounderBean bean) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				for (ElementBean variable : bean.getSelectedVars()) {
					InsertOnDuplicateSetMoreStep<TConfounderRecord> sql = DSL.using(c)
					// @formatter:off
						.insertInto(T_CONFOUNDER, 
						            T_CONFOUNDER.FK_ELEMENT, 
						            T_CONFOUNDER.FK_REFERENCED_ELEMENT,
						            T_CONFOUNDER.FK_METADATATYPE)
						.values(variable.getId(), bean.getFkReferenceElement(), bean.getMetadataType().getPk())
						.onConflict(T_CONFOUNDER.FK_ELEMENT, T_CONFOUNDER.FK_METADATATYPE).doUpdate()
						.set(T_CONFOUNDER.FK_REFERENCED_ELEMENT, bean.getFkReferenceElement());
					// @formatter:on
					LOGGER.debug("{}", sql.toString());
					lrw.addToInteger(sql.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete all metadata references of type under parent
	 * 
	 * @param referenceVariable the id of the referencing variable
	 * @param type the type
	 * @param parent the id of the parent
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer deleteAllMetadata(Integer referenceVariable, EnumMetadatatype type, Integer parent) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				Name R = DSL.name("r");
				Field<Integer> K = DSL.field("k", Integer.class);
				Field<Integer> P = DSL.field("p", Integer.class);

				DeleteConditionStep<TConfounderRecord> sql2 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_CONFOUNDER)
					.where(DSL.trueCondition()) // TODO: made it deployable for the moment
//  			.where(T_METADATA.METADATATYPE.eq(type))
					.and(T_CONFOUNDER.FK_REFERENCED_ELEMENT.eq(referenceVariable))
					.and(T_CONFOUNDER.FK_ELEMENT.in(DSL.using(c)
					  .withRecursive(R, K.getUnqualifiedName(), P.getUnqualifiedName())
						.as(DSL.using(c)
						  .select(T_ELEMENT.PK, 
						          T_ELEMENT.FK_PARENT)
						  .from(T_ELEMENT)
							.where(T_ELEMENT.FK_PARENT.eq(parent))
							.unionAll(DSL.using(c)
							  .select(T_ELEMENT.PK, 
							          T_ELEMENT.FK_PARENT)
							  .from(T_ELEMENT)
							  .join(R).on(T_ELEMENT.FK_PARENT.eq(K))))
						.select(K)
						.from(R)));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all variables (only id and lang key)
	 * 
	 * @param includeTechnical include technical variables?
	 * @return a list of element beans, at least an empty one
	 * 
	 * if anything went wrong on database side
	 */
	public List<ElementBean> getAllVariables(boolean includeTechnical) {
		try (
				SelectConditionStep<Record11<JSONB, String, Integer, String, String, Integer, Integer, String, String, String, JSONB>> sql = getJooq()
				// @formatter:off
					.select(T_ELEMENT.TRANSLATION, 
					        T_ELEMENT.UNIQUE_NAME, 
					        T_ELEMENT.PK,
					        T_ELEMENT.NAME, 
					        T_ELEMENT.UNIQUE_NAME,
					        T_ELEMENT.ORDER_NR, 
					        T_ELEMENT.FK_MISSINGLIST,
					        T_VARIABLEATTRIBUTE.VALUE, 
					        V_STUDY.STUDY_NAME, 
					        V_STUDY.STUDYGROUP_NAME,
					        V_STUDY.LOCALES)
					.from(T_ELEMENT)
					.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_ELEMENT.PK))
					.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(T_ELEMENT.PK))
	        .leftJoin(T_VARIABLEATTRIBUTE).on(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(T_VARIABLE.FK_ELEMENT)
	            .and(T_VARIABLEATTRIBUTE.IS_PRIMARY_ATTRIBUTE.eq(true)))
					.where(includeTechnical ? T_ELEMENT.PK.eq(T_ELEMENT.PK) : T_VARIABLE.CONTROL_TYPE.isNull());
        // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<ElementBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				Integer keyElement = r.get(T_ELEMENT.PK);
				String name = r.get(T_ELEMENT.NAME);
				Integer orderNr = r.get(T_ELEMENT.ORDER_NR);
				Integer shipOrder = r.get(T_ELEMENT.ORDER_NR);
				Integer fkMissinglist = r.get(T_ELEMENT.FK_MISSINGLIST);
				String label = r.get(T_VARIABLEATTRIBUTE.VALUE);
				String study = r.get(V_STUDY.STUDY_NAME);
				String studygroup = r.get(V_STUDY.STUDYGROUP_NAME);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				JSONB locales = r.get(V_STUDY.LOCALES);
				list.add(new ElementBean(uniqueName, keyElement, translation, name, label, orderNr, shipOrder, study,
						studygroup, fkMissinglist, locales));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get acl of corresponding study; if none is found, an empty acl is given
	 * 
	 * @param elementId the id of the element
	 * @param usnr the id of the user
	 * @return the acl bean
	 */
	public AclBean getStudyAcl(Integer elementId, Integer usnr) {
		try (SelectConditionStep<Record1<EnumAccesslevel>> sql = getJooq()
		// @formatter:off
			.select(V_PRIVILEGE.ACL)
			.from(V_STUDY)
			.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(V_STUDY.FK_ELEMENT))
			.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
			.where(V_STUDY.KEY_ELEMENT.eq(elementId))
			.and(V_PRIVILEGE.FK_USNR.eq(usnr));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			AclBean acl = new AclBean();
			for (Record r : sql.fetch()) {
				acl.mergeAcl(acl.getAcl(), r.get(V_PRIVILEGE.ACL));
			}
			return acl;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add user and/or group privilege for study
	 * 
	 * @param newUsnr the id of the user
	 * @param privileges the list of privileges
	 * @param newGroup the new group
	 * @param newAcl the new acl
	 * @return affected database lines
	 * 
	 * on database errors
	 */
	public Integer addUserGroupPrivilegeForStudy(Integer newUsnr, Integer newGroup, List<Integer> privileges,
			AclBean newAcl) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		Name X = DSL.name("X");
		Field<Integer> X_r = DSL.field("r", Integer.class);
		Field<Integer> X_k = DSL.field("k", Integer.class);
		Field<Integer> X_p = DSL.field("p", Integer.class);
		Field<String> X_d = DSL.field("d", String.class);
		try (CloseableDSLContext jooq = getJooq()) {
			SelectJoinStep<Record2<Integer, String>> sql = jooq
			// @formatter:off
				.withRecursive(X, 
				               X_k.getUnqualifiedName(), 
				               X_p.getUnqualifiedName(), 
				               X_r.getUnqualifiedName(),
				               X_d.getUnqualifiedName())
				.as(getJooq()
				  .select(T_ELEMENT.PK, 
				          T_ELEMENT.FK_PARENT, 
				          T_ELEMENT.FK_PRIVILEGE,
				          T_ELEMENT.UNIQUE_NAME)
					.from(T_ELEMENT)
					.where(T_ELEMENT.FK_PRIVILEGE.in(privileges))
					.unionAll(getJooq()
					  .select(T_ELEMENT.PK, 
					          T_ELEMENT.FK_PARENT, 
					          T_ELEMENT.FK_PRIVILEGE, 
					          T_ELEMENT.UNIQUE_NAME)
						.from(T_ELEMENT)
						.innerJoin(X).on(T_ELEMENT.PK.eq(X_p))))
				.selectDistinct(X_r, X_d)
				.from(X);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());

			Result<Record2<Integer, String>> result = sql.fetch();
			jooq.transaction(t -> {
				Integer counter = 0;
				for (Record r : result) {
					Integer priv = r.get(X_r);
					AclBean acl = new AclBean(EnumAccesslevel.r);
					if (privileges.contains(priv)) // in parent elements, do now allow write or use hierarchically
					{
						acl = newAcl;
					}
					if (priv != null) {
						counter += super.addUserGroupPrivilege(DSL.using(t), newUsnr, newGroup, priv, acl);
					} else {
						String uniqueName = r.get(X_d);
						notifyWarning("no privilege defined for {0}", uniqueName);
					}
				}
				lrw.setInteger(counter);
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return lrw.getInteger();
	}

	/**
	 * get all privilege ids of children of element that has fkPrivilege
	 * 
	 * @param fkPrivilege privilege pk of parent element
	 * @return set of privilege pks of all children
	 * 
	 * if anything went wrong on database side
	 */
	public Set<Integer> getAllChildrenPrivilegeIds(Integer fkPrivilege) {
		Name X = DSL.name("X");
		Field<Integer> X_r = DSL.field("r", Integer.class);
		Field<Integer> X_k = DSL.field("k", Integer.class);
		Field<Integer> X_p = DSL.field("p", Integer.class);
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record1<Integer>> sql = jooq
			// @formatter:off
				.withRecursive(X, 
				               X_k.getUnqualifiedName(), 
				               X_p.getUnqualifiedName(), 
				               X_r.getUnqualifiedName())
				.as(jooq
				  .select(T_ELEMENT.PK, 
				          T_ELEMENT.FK_PARENT, 
				          T_ELEMENT.FK_PRIVILEGE)
				  .from(T_ELEMENT)
				  .where(T_ELEMENT.FK_PRIVILEGE.eq(fkPrivilege))
				  .unionAll(jooq
				    .select(T_ELEMENT.PK, 
				            T_ELEMENT.FK_PARENT, 
				            T_ELEMENT.FK_PRIVILEGE)
				    .from(T_ELEMENT)
				    .innerJoin(X).on(T_ELEMENT.FK_PARENT.eq(X_k))))
				.selectDistinct(X_r).from(X).where(X_r.isNotNull());
			// this might happen as fk_privilege is still nullable in T_ELEMENT
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Set<Integer> result = new HashSet<>();
			for (Record r : sql.fetch()) {
				result.add(r.get(X_r));
			}
			return result;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * remove privilege from study and all of its children
	 * 
	 * @param bean containing all information needed to remove privileges
	 * @return amount of affected rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer removeUserGroupPrivilege(GroupPrivilegeBean bean) {
		Set<Integer> privileges = getAllChildrenPrivilegeIds(bean.getFkPrivilege());
		privileges.add(getParentPrivilegeId(bean.getFkPrivilege()));
		return super.removeUserGroupPrivilege(bean.getFkUsnr(), bean.getFkGroup(), privileges);
	}

	/**
	 * update privileges of elements children
	 * 
	 * @param bean the group privilege bean
	 * @param privileges the list of privileges
	 * @return the number of affected rows in the database
	 * 
	 * if anything went wrong on database side
	 */
	public Integer updateUserGroupPrivileges(GroupPrivilegeBean bean, List<Integer> privileges) {
		List<GroupPrivilegeBean> list = new ArrayList<>();
		list.add(bean);
		for (Integer privilege : privileges) {
			if (privilege != null) {
				GroupPrivilegeBean b = new GroupPrivilegeBean(null, bean.getIsGroup(), privilege);
				b.setFkGroup(bean.getFkGroup());
				b.setFkUsnr(bean.getFkUsnr());
				b.getAcl().setAcl(bean.getAcl()); // might be the same instance as all of them are equal
				list.add(b);
			}
		}
		return super.upsertUserGroupPrivileges(list);
	}

	/**
	 * get number of departments under the one defined by id
	 * 
	 * @param id the id of the parent element
	 * @return number of departments; 0 at least
	 */
	public Integer getNumberOfSections(Integer id) {
		Name X = DSL.name("X");
		Field<Integer> X_k = DSL.field("k", Integer.class);
		Field<Integer> X_p = DSL.field("p", Integer.class);
		Field<Integer> X_c = DSL.field("c", Integer.class);
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record1<Integer>> sql = jooq
			// @formatter:off
				.withRecursive(X, 
				               X_k.getUnqualifiedName(), 
				               X_p.getUnqualifiedName())
				.as(jooq
				  .select(T_ELEMENT.PK, 
				          T_ELEMENT.FK_PARENT)
				  .from(T_ELEMENT)
				  .where(T_ELEMENT.PK.eq(id))
					.unionAll(jooq
					  .select(T_ELEMENT.PK, 
					          T_ELEMENT.FK_PARENT)
					  .from(T_ELEMENT)
					  .innerJoin(X).on(T_ELEMENT.FK_PARENT.eq(X_k))))
				.select(DSL.count(X_k).as(X_c))
				.from(X)
				.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(X_k))
				.where(T_VARIABLE.FK_ELEMENT.isNull());
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			return sql.fetchOne().get(X_c);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get number of variables under the defined department
	 * 
	 * @param id the id of the department
	 * @return number of variables; 0 at least
	 */
	public Integer getNumberOfVariables(Integer id) {
		Name X = DSL.name("X");
		Field<Integer> X_k = DSL.field("k", Integer.class);
		Field<Integer> X_p = DSL.field("p", Integer.class);
		Field<Integer> X_c = DSL.field("c", Integer.class);
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record1<Integer>> sql = jooq
			// @formatter:off
				.withRecursive(X, 
				               X_k.getUnqualifiedName(), 
				               X_p.getUnqualifiedName())
				.as(jooq
				  .select(T_ELEMENT.PK, 
				          T_ELEMENT.FK_PARENT)
				  .from(T_ELEMENT)
				  .where(T_ELEMENT.PK.eq(id))
					.unionAll(jooq
					  .select(T_ELEMENT.PK, 
					          T_ELEMENT.FK_PARENT)
					  .from(T_ELEMENT)
					  .innerJoin(X).on(T_ELEMENT.FK_PARENT.eq(X_k))))
				.select(DSL.count(T_VARIABLE.FK_ELEMENT).as(X_c))
				.from(X)
				.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(X_k))
				.where(T_VARIABLE.FK_ELEMENT.isNotNull());
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			return sql.fetchOne().get(X_c);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get percentage of covered variables by variable groups
	 * 
	 * @param id the id of the department
	 * @return percentage of variables in a variable group, 0 at least
	 */
	public Integer getPercentageOfCovered(Integer id) {
		Name X = DSL.name("X");
		Field<Integer> X_k = DSL.field("k", Integer.class);
		Field<Integer> X_p = DSL.field("p", Integer.class);
		Field<Integer> X_vars = DSL.field("vars", Integer.class);
		Field<Integer> X_grps = DSL.field("grps", Integer.class);
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record2<Integer, Integer>> sql = jooq
			// @formatter:off
				.withRecursive(X, 
				               X_k.getUnqualifiedName(), 
				               X_p.getUnqualifiedName())
				.as(jooq
				  .select(T_ELEMENT.PK, 
				          T_ELEMENT.FK_PARENT)
				  .from(T_ELEMENT)
				  .where(T_ELEMENT.PK.eq(id))
					.unionAll(jooq
					  .select(T_ELEMENT.PK, 
					          T_ELEMENT.FK_PARENT)
					  .from(T_ELEMENT)
					  .innerJoin(X).on(T_ELEMENT.FK_PARENT.eq(X_k))))
				.select(DSL.count(T_VARIABLEGROUPELEMENT.FK_ELEMENT).as(X_grps), 
				        DSL.count(T_VARIABLE.FK_ELEMENT).as(X_vars))
				.from(X)
				.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(X_k))
				.leftJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.FK_ELEMENT.eq(X_k))
				.where(T_VARIABLE.FK_ELEMENT.isNotNull());
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Record r = sql.fetchOne();
			Integer groups = r.get(X_grps);
			Integer vars = r.get(X_vars);
			return vars < 1 ? 0 : Float.valueOf((groups.floatValue() / vars.floatValue()) * 100).intValue();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all metadata types
	 * 
	 * @return list of metadata types
	 */
	public List<MetadatatypeBean> getMetadatatypes() {
		try (SelectJoinStep<Record3<Integer, String, String>> sql = getJooq()
		// @formatter:off
			.select(T_METADATATYPE.PK, 
			        T_METADATATYPE.NAME, 
			        T_METADATATYPE.DESCRIPTION)
			.from(T_METADATATYPE);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<MetadatatypeBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_METADATATYPE.PK);
				MetadatatypeBean bean = new MetadatatypeBean(pk);
				bean.setName(r.get(T_METADATATYPE.NAME));
				bean.setDescription(r.get(T_METADATATYPE.DESCRIPTION));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete the metadatatype and all of its metadata
	 * 
	 * @param bean the metadatatype
	 * @return number of affected database rows
	 */
	public Integer deleteMetadatatype(TMetadatatypeRecord bean) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				DeleteConditionStep<TConfounderRecord> sql = DSL.using(t)
				// @formatter:off
					.deleteFrom(T_CONFOUNDER)
					.where(T_CONFOUNDER.FK_METADATATYPE.eq(bean.getPk()));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				DeleteConditionStep<TMetadatatypeRecord> sql2 = DSL.using(t)
				// @formatter:off
					.deleteFrom(T_METADATATYPE)
					.where(T_METADATATYPE.PK.eq(bean.getPk()));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add or update the metadatatypeBean; new beans have pk = null
	 * 
	 * @param bean the bean to be upserted
	 * @return number of affected database rows
	 */
	public Integer upsertMetadatatype(MetadatatypeBean bean) {
		try (CloseableDSLContext jooq = getJooq()) {
			if (bean.getPk() == null) {
				InsertValuesStep2<TMetadatatypeRecord, String, String> sql = jooq
				// @formatter:off
					.insertInto(T_METADATATYPE, 
					            T_METADATATYPE.NAME, 
					            T_METADATATYPE.DESCRIPTION)
					.values(bean.getName(), bean.getDescription());
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				return sql.execute();
			} else {
				UpdateConditionStep<TMetadatatypeRecord> sql = jooq
				// @formatter:off
					.update(T_METADATATYPE)
					.set(T_METADATATYPE.NAME, bean.getName())
					.set(T_METADATATYPE.DESCRIPTION, bean.getDescription())
					.where(T_METADATATYPE.PK.eq(bean.getPk()));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				return sql.execute();
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all variable usages from db
	 * 
	 * @return list of found variable usages
	 */
	public List<VariableusageBean> getAllVariableusages() {
		try (SelectJoinStep<Record3<Integer, String, Integer>> sql = getJooq()
		// @formatter:off
			.select(T_VARIABLEUSAGE.PK,
			        T_VARIABLEUSAGE.NAME, 
			        T_VARIABLEUSAGE.FK_LANG)
			.from(T_VARIABLEUSAGE);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<VariableusageBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				VariableusageBean bean = new VariableusageBean(r.get(T_VARIABLEUSAGE.PK));
				bean.setName(r.get(T_VARIABLEUSAGE.NAME));
				bean.setFkLang(r.get(T_VARIABLEUSAGE.FK_LANG));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * insert into t_iostack and t_iostack_file
	 * 
	 * @param uploadBean then bean containing the file and its content
	 * @param interpretation the interpretation
	 * @return the pk of t_iostack
	 */
	public Integer insertIoStackFile(UploadBean uploadBean, String interpretation) {
		Integer usnr = getFacesContext().getUsnr();
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				Integer dbVersion = DSL.using(c).selectFrom(V_VERSION).fetchOne().getDbVersion();
				InsertResultStep<TIostackRecord> sql = DSL.using(c)
				// @formatter:off
					.insertInto(T_IOSTACK, 
					            T_IOSTACK.FILETYPE, 
					            T_IOSTACK.INTERPRETATION, 
					            T_IOSTACK.DB_VERSION,
					            T_IOSTACK.FK_USNR)
					.values(uploadBean.getType(), interpretation, dbVersion, usnr)
					.returning(T_IOSTACK.PK);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				Integer pk_iostack = sql.fetchOne().getPk();
				lrw.setInteger(pk_iostack);

				InsertValuesStep2<TIostackFileRecord, Integer, byte[]> sql2 = DSL.using(c)
				// @formatter:off
					.insertInto(T_IOSTACK_FILE, 
					            T_IOSTACK_FILE.FK_IOSTACK, 
					            T_IOSTACK_FILE.FILE)
					.values(pk_iostack, uploadBean.getBytes());
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				sql2.execute();
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all messages of fkIostack from t_iomessage and put them to as messages to
	 * the faces context
	 * 
	 * @param fkIostack the pk of t_iostack
	 */
	public void selectIoMessage(Integer fkIostack) {
		try (SelectConditionStep<Record2<String, String>> sql = getJooq()
		// @formatter:off
			.select(T_IOMESSAGE.LOGLEVEL, 
			        T_IOMESSAGE.LOGMESSAGE)
			.from(T_IOMESSAGE)
			.where(T_IOMESSAGE.FK_IOSTACK.eq(fkIostack));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				getFacesContext().notifyMessage(r.get(T_IOMESSAGE.LOGLEVEL), r.get(T_IOMESSAGE.LOGMESSAGE));
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add a study group
	 * 
	 * @param studygroup the study group
	 */
	public void addStudygroup(StudygroupBean studygroup) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				InsertResultStep<TElementRecord> sql = DSL.using(t)
				// @formatter:off
					.insertInto(T_ELEMENT, 
					            T_ELEMENT.NAME, 
					            T_ELEMENT.UNIQUE_NAME,
					            T_ELEMENT.TRANSLATION)
					.values(studygroup.getName(), studygroup.getName(), studygroup.getTranslation())
					.returning(T_ELEMENT.PK);
		    // @formatter:on
				LOGGER.debug("{}", sql.toString());
				Integer pk = sql.fetchOne().get(T_ELEMENT.PK);

				InsertValuesStep2<TStudygroupRecord, Integer, JSONB> sql2 = DSL.using(t)
				// @formatter:off
				  .insertInto(T_STUDYGROUP,
				  		        T_STUDYGROUP.FK_ELEMENT,
				  		        T_STUDYGROUP.LOCALES)
				  .values(pk, studygroup.getLocales());
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update a study group
	 * 
	 * @param studygroup the study group
	 */
	public void updateStudygroup(StudygroupBean studygroup) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				UpdateConditionStep<TElementRecord> sql = DSL.using(t)
				// @formatter:off
					.update(T_ELEMENT)
					.set(T_ELEMENT.NAME, studygroup.getName())
					.set(T_ELEMENT.UNIQUE_NAME, studygroup.getName())
					.set(T_ELEMENT.TRANSLATION, studygroup.getTranslation())
					.where(T_ELEMENT.PK.eq(studygroup.getPk()));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				InsertOnDuplicateSetMoreStep<TStudygroupRecord> sql2 = DSL.using(t)
				// @formatter:off
					.insertInto(T_STUDYGROUP, 
							        T_STUDYGROUP.FK_ELEMENT, 
							        T_STUDYGROUP.LOCALES)
					.values(studygroup.getPk(), studygroup.getLocales())
					.onConflict(T_STUDYGROUP.FK_ELEMENT)
					.doUpdate()
					.set(T_STUDYGROUP.LOCALES, studygroup.getLocales());
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete a study group and all its children
	 * 
	 * @param bean the study group bean
	 * @return number of affected database rows
	 */
	public Integer deleteStudygroup(StudygroupBean bean) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				SelectConditionStep<Record2<Integer, Integer>> sql = DSL.using(t)
				// @formatter:off
          .select(T_ELEMENT.PK, 
                  T_ELEMENT.FK_PRIVILEGE)
          .from(T_ELEMENT)
          .where(T_ELEMENT.FK_PARENT.eq(bean.getPk()));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				for (Record r : sql.fetch()) {
					Integer fkPrivilege = r.get(T_ELEMENT.FK_PRIVILEGE);
					Integer fkParent = r.get(T_ELEMENT.PK);

					DeleteConditionStep<TUserprivilegeRecord> sql1 = DSL.using(t)
					// @formatter:off
						.deleteFrom(T_USERPRIVILEGE)
						.where(T_USERPRIVILEGE.FK_PRIVILEGE.eq(fkPrivilege));
					// @formatter:on
					LOGGER.debug("{}", sql1.toString());
					lrw.addToInteger(sql1.execute());

					DeleteConditionStep<TGroupprivilegeRecord> sql2 = DSL.using(t)
					// @formatter:off
						.deleteFrom(T_GROUPPRIVILEGE)
						.where(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(fkPrivilege));
					// @formatter:on
					LOGGER.debug("{}", sql2.toString());
					lrw.addToInteger(sql2.execute());

					// make sure this one does not appear in the list of valid studies any more
					DeleteConditionStep<TStudyRecord> sql3 = DSL.using(t)
					// @formatter:off
						.deleteFrom(T_STUDY)
						.where(T_STUDY.FK_ELEMENT.eq(fkParent));
					// @formatter:on
					LOGGER.debug("{}", sql3.toString());
					lrw.addToInteger(sql3.execute());

					DeleteConditionStep<TElementRecord> sql4 = DSL.using(t)
					// @formatter:off
						.deleteFrom(T_ELEMENT)
						.where(T_ELEMENT.PK.eq(fkParent));
					// @formatter:on
					LOGGER.debug("{}", sql4.toString());
					lrw.addToInteger(sql4.execute());
				}
				DeleteConditionStep<TStudygroupRecord> sql5 = DSL.using(t)
				// @formatter:off
					.deleteFrom(T_STUDYGROUP)
					.where(T_STUDYGROUP.FK_ELEMENT.eq(bean.getPk()));
				// @formatter:on
				LOGGER.debug("{}", sql5.toString());
				lrw.addToInteger(sql5.execute());

				DeleteConditionStep<TElementRecord> sql6 = DSL.using(t)
				// @formatter:off
					.deleteFrom(T_ELEMENT)
					.where(T_ELEMENT.PK.eq(bean.getPk()));
				// @formatter:on
				LOGGER.debug("{}", sql6.toString());
				lrw.addToInteger(sql6.execute());
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * list the variable groups of the variable that it is in
	 * 
	 * @param id the id of the variable
	 * @return a list of variable groups; an empty one at least
	 */
	public List<VariablegroupBean> getVariablegroupsOfVariable(Integer id) {
		Name X = DSL.name("x");
		Field<String> X_USER = DSL.field(DSL.name("x", "user"), String.class);
		Field<String> USERS = DSL.field("users", String.class);
		Field<Integer> X_FK_VARIABLEGROUP = DSL.field("fk_variablegroup", Integer.class);
		try (CloseableDSLContext jooq = getJooq()) {
			SelectHavingStep<Record4<Integer, String, String, String>> sql = jooq
			// @formatter:off
				.with(X, 
				      X_FK_VARIABLEGROUP.getUnqualifiedName(), 
				      X_USER.getUnqualifiedName())
				.as(jooq
					.select(T_VARIABLEGROUP.PK,
								  DSL.concat(T_PERSON.FORENAME, DSL.val(" "), T_PERSON.SURNAME).as(X_USER.getUnqualifiedName()))
					.from(T_VARIABLEGROUPELEMENT)
					.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP))
					.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE))
					.leftJoin(T_PERSON).on(T_PERSON.USNR.eq(V_PRIVILEGE.FK_USNR))
					.where(T_VARIABLEGROUPELEMENT.FK_ELEMENT.eq(id)))
				.select(T_VARIABLEGROUP.PK, 
				        T_VARIABLEGROUP.NAME, 
				        T_VARIABLEGROUP.DESCRIPTION,
				        DSL.listAgg(X_USER, ", ").withinGroupOrderBy(X_USER).as(USERS))
				.from(X)
				.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(X_FK_VARIABLEGROUP))
				.groupBy(T_VARIABLEGROUP.PK, T_VARIABLEGROUP.NAME, T_VARIABLEGROUP.DESCRIPTION);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			List<VariablegroupBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				VariablegroupBean bean = new VariablegroupBean();
				bean.setName(r.get(T_VARIABLEGROUP.NAME));
				bean.setDescription(r.get(T_VARIABLEGROUP.DESCRIPTION));
				bean.setVariablegroupId(r.get(T_VARIABLEGROUP.PK));
				bean.setUsers(r.get(USERS));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all variable attribute roles from db
	 * 
	 * @return a list of variable attribute role beans; an empty one at least
	 */
	public List<VariableAttributeRoleBean> getAllVariableAttributeRoles() {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectWhereStep<TVariableattributeroleRecord> sql = jooq.selectFrom(T_VARIABLEATTRIBUTEROLE);
			LOGGER.debug("{}", sql.toString());
			List<VariableAttributeRoleBean> list = new ArrayList<>();
			for (TVariableattributeroleRecord r : sql.fetch()) {
				list.add(new VariableAttributeRoleBean(r.getPk(), r.getName(), r.getInputwidget(), r.getRegexpattern(),
						r.getHasPrimaryAttribute(), r.getHasLocale()));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * check if the parent of this element is a study group
	 * 
	 * @param id the id of the element
	 * @return true or false
	 */
	public Boolean checkIfParentIsStudy(Integer id) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record1<Integer>> sql = jooq.select(T_STUDY.PK).from(T_STUDY)
					.where(T_STUDY.FK_ELEMENT.eq(id));
			LOGGER.debug("{}", sql.toString());
			return sql.fetch().size() > 0;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get the privilege of the parent element; if not exists, create a new one
	 * 
	 * @param fkPrivilege the privilege of the element
	 * @return the id of the parent privilege
	 */
	public Integer getParentPrivilegeId(Integer fkPrivilege) {
		try (CloseableDSLContext jooq = getJooq()) {
			TElement parent = T_ELEMENT.as("parent");
			SelectConditionStep<Record2<Integer, Integer>> sql = jooq
			// @formatter:off
					.select(parent.PK,
							    parent.FK_PRIVILEGE)
					.from(parent)
					.leftJoin(T_ELEMENT).on(T_ELEMENT.FK_PARENT.eq(parent.PK))
					.where(T_ELEMENT.FK_PRIVILEGE.eq(fkPrivilege));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Integer fkPrivilegeOfParent = null;
			Integer fkParent = null;
			for (Record r : sql.fetch()) {
				fkPrivilegeOfParent = r.get(parent.FK_PRIVILEGE);
				fkParent = r.get(parent.PK);
			}
			if (fkParent == null) {
				throw new DataAccessException(
						"element with privilege " + (fkPrivilege == null ? "null" : fkPrivilege) + " has no parent");
			}
			if (fkPrivilegeOfParent == null) {
				String name = "parent.of.privilege." + fkPrivilege;
				InsertResultStep<TPrivilegeRecord> sql2 = jooq
				// @formatter:off
					.insertInto(T_PRIVILEGE, 
							        T_PRIVILEGE.NAME)
					.values(name)
					.returning(T_PRIVILEGE.PK);
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				fkPrivilegeOfParent = sql2.fetchOne().get(T_PRIVILEGE.PK);
				UpdateConditionStep<TElementRecord> sql3 = jooq
				// @formatter:off
					.update(T_ELEMENT)
					.set(T_ELEMENT.FK_PRIVILEGE, fkPrivilegeOfParent)
					.where(T_ELEMENT.PK.eq(fkParent));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				sql3.execute();
			}
			return fkPrivilegeOfParent;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get the ID of the elemnt with the given unique name
	 * 
	 * @param uniqueName the unique name
	 * 
	 * @return the ID or null if not found
	 */
	public Integer getIdOfUniqueName(String uniqueName) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<TElementRecord> sql = jooq
			// @formatter:off
				.selectFrom(T_ELEMENT)
				.where(T_ELEMENT.UNIQUE_NAME.eq(uniqueName));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Result<TElementRecord> results = sql.fetch();
			if (results.size() < 1) {
				return null;
			} else {
				return results.get(0).getPk();
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get a list of all possible time variables for a study (data collection)
	 * 
	 * @param id the id of the study wave
	 * @param isocode the current isocode from the session
	 * @return the list of found time variables
	 */
	public List<IdLabelBean> getPossibleTimevars(Integer id, EnumIsocode isocode) {
		try (CloseableDSLContext jooq = getJooq()) {
			TElement parent = T_ELEMENT.as("parent");
			SelectSeekStep1<Record5<Integer, String, String, String, String>, Integer> sql = jooq
			// @formatter:off
				.select(T_ELEMENT.PK, 
						    T_ELEMENT.UNIQUE_NAME, 
						    parent.NAME, 
						    T_VARIABLEATTRIBUTE.VALUE, 
						    T_TRANSLATION.VALUE)
				.from(T_VARIABLE)
				.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_VARIABLE.FK_ELEMENT))
				.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLE.FK_ELEMENT))
				.leftJoin(parent).on(parent.PK.eq(T_ELEMENT.FK_PARENT))
				.leftJoin(T_VARIABLEATTRIBUTE).on(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(T_VARIABLE.FK_ELEMENT).and(T_VARIABLEATTRIBUTE.NAME.lower().eq("varlabel")))
				.leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_ELEMENT.FK_LANG).and(T_TRANSLATION.ISOCODE.eq(isocode)))
				.where(V_STUDY.FK_ELEMENT.eq(id))
				.and(T_VARIABLE.DATATYPE.eq(EnumDatatype.datetime))
				.orderBy(T_ELEMENT.ORDER_NR);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			List<IdLabelBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer fkElement = r.get(T_ELEMENT.PK);
				String uniqueName = r.get(T_ELEMENT.UNIQUE_NAME);
				String parentName = r.get(parent.NAME);
				String label = r.get(T_VARIABLEATTRIBUTE.VALUE);
				String translation = r.get(T_TRANSLATION.VALUE);
				StringBuilder buf = new StringBuilder(uniqueName);
				if (parentName != null) {
					buf.append(": ").append(parentName);
				}
				if (label != null) {
					buf.append(": ").append(label);
				} else {
					buf.append(": ").append(translation);
				}
				list.add(new IdLabelBean(fkElement, buf.toString()));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get the variable that this study (data collection) refers to for the time var
	 * 
	 * @param id the id of the study wave
	 * @return the variable bean if found, null otherwise
	 */
	public Integer getCurrentTimeVarOfStudy(Integer id) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record1<Integer>> sql = jooq
			// @formatter:off
				.select(T_CONFOUNDER.FK_REFERENCED_ELEMENT)
				.from(T_CONFOUNDER)
				.leftJoin(T_METADATATYPE).on(T_METADATATYPE.PK.eq(T_CONFOUNDER.FK_METADATATYPE))
				.where(T_METADATATYPE.NAME.eq("timevar"))
				.and(T_CONFOUNDER.FK_ELEMENT.eq(id));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				return r.get(T_CONFOUNDER.FK_REFERENCED_ELEMENT);
			}
			return null; // not found case
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update the time variable for that study
	 * 
	 * @param studyId the id of the study (wave) element
	 * @param timeVarId the id of the time variable
	 * @return number of affected database rows, should be 1
	 */
	public Integer updateTimeVar(Integer studyId, Integer timeVarId) {
		try (CloseableDSLContext jooq = getJooq()) {
			if (timeVarId == null) {
				DeleteConditionStep<TConfounderRecord> sql = jooq
				// @formatter:off
				  .deleteFrom(T_CONFOUNDER)
				  .where(T_CONFOUNDER.FK_ELEMENT.eq(studyId))
				  .and(T_CONFOUNDER.FK_METADATATYPE.in(jooq
				  	.select(T_METADATATYPE.PK)
				  	.from(T_METADATATYPE)
				  	.where(T_METADATATYPE.NAME.eq("timevar"))));
				// @formatter:off
				LOGGER.debug("{}", sql.toString());
				return sql.execute();
			} else {
				InsertOnDuplicateSetMoreStep<TConfounderRecord> sql = jooq
				// @formatter:off
					.insertInto(T_CONFOUNDER,
							        T_CONFOUNDER.FK_ELEMENT,
							        T_CONFOUNDER.FK_METADATATYPE,
							        T_CONFOUNDER.FK_REFERENCED_ELEMENT)
					.select(jooq
						.select(DSL.val(studyId, Integer.class), T_METADATATYPE.PK, DSL.val(timeVarId, Integer.class))
						.from(T_METADATATYPE)
						.where(T_METADATATYPE.NAME.eq("timevar")))
					.onConflict(T_CONFOUNDER.FK_ELEMENT, T_CONFOUNDER.FK_METADATATYPE)
					.doUpdate()
					.set(T_CONFOUNDER.FK_REFERENCED_ELEMENT, timeVarId);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				return sql.execute();
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
