package square2.db.control;

import org.jooq.exception.DataAccessException;

/**
 * 
 * @author henkej
 *
 */
public class ParametrizedDataAccessException extends DataAccessException {
	private static final long serialVersionUID = 1L;

	private final Object[] params;

	/**
	 * creates new DataAccessException with parameters
	 * 
	 * @param e
	 *          the exception
	 */
	public ParametrizedDataAccessException(DataAccessException e) {
		super(e.getMessage());
		params = new Object[] {};
	}

	/**
	 * creates new DataAccessException with parameters
	 * 
	 * @param message
	 *          the error message
	 */
	public ParametrizedDataAccessException(String message) {
		super(message);
		params = new Object[] {};
	}

	/**
	 * creates new DataAccessException with parameters
	 * 
	 * @param message
	 *          the error message
	 * @param params
	 *          the parameters
	 */
	public ParametrizedDataAccessException(String message, Object... params) {
		super(message);
		this.params = params;
	}

	/**
	 * get the message parameters
	 * 
	 * @return the message parameters
	 */
	public Object[] getParams() {
		return params;
	}
}
