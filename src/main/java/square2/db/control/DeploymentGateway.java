package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_LANGUAGEKEY;
import static de.ship.dbppsquare.square.Tables.T_TRANSLATION;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.Record;
import org.jooq.Record4;
import org.jooq.SelectConditionStep;

import de.ship.dbppsquare.square.enums.EnumIsocode;
import square2.modules.model.LangBean;
import square2.modules.model.LangKey;
import square2.modules.model.SquareLocale;

/**
 * 
 * @author henkej
 *
 */
public class DeploymentGateway {
	private static final Logger LOGGER = LogManager.getLogger(DeploymentGateway.class);

	private CloseableDSLContext jooq;

	/**
	 * generate new deployment gateway
	 * 
	 * @param jooq
	 *          the dsl context of the gateway
	 */
	public DeploymentGateway(CloseableDSLContext jooq) {
		this.jooq = jooq;
	}

	/**
	 * get all translations from db
	 * 
	 * @param locales
	 *          a set of available locales
	 * @return the translations bean
	 * 
	 *         if anything went wrong on database side
	 */
	public LangBean getAllTranslations(SquareLocale... locales) {
		List<String> isos = new ArrayList<>();
		for (SquareLocale l : locales) {
			isos.add(l.get());
		}
		SelectConditionStep<Record4<EnumIsocode, Integer, String, String>> sql = jooq
		// @formatter:off
			.select(T_TRANSLATION.ISOCODE, 
							T_TRANSLATION.FK_LANG, 
							T_TRANSLATION.VALUE, 
							T_LANGUAGEKEY.NAME)
			.from(T_TRANSLATION)
			.leftJoin(T_LANGUAGEKEY).on(T_LANGUAGEKEY.PK.eq(T_TRANSLATION.FK_LANG))
			.where(T_TRANSLATION.ISOCODE.cast(String.class).in(isos));
		// @formatter:on
		LOGGER.debug("{}", sql);
		LangBean bean = new LangBean(locales);
		for (Record r : sql.fetch()) {
			String value = r.get(T_TRANSLATION.VALUE);
			Integer key = r.get(T_TRANSLATION.FK_LANG);
			String name = r.get(T_LANGUAGEKEY.NAME);
			EnumIsocode isoCode = r.get(T_TRANSLATION.ISOCODE);
			if (isoCode != null) {
				bean.add(isoCode.getLiteral(), LangKey.generateKey(key, name), value);
			}
		}
		return bean;
	}

	/**
	 * checks if the user can login; it not, returns null
	 * 
	 * @param username the user
	 * @param password the password or null, if no check is needed
	 * @return the username or null
	 */
	public String getUsernameIfExistsInDatabase(String username, String password) {
		// TODO: implement
		return null;
	}
}
