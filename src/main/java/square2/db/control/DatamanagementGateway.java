package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_ELEMENT;
import static de.ship.dbppsquare.square.Tables.T_GROUPPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_PERSON;
import static de.ship.dbppsquare.square.Tables.T_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_RELEASE;
import static de.ship.dbppsquare.square.Tables.T_REPORTDATARELEASE;
import static de.ship.dbppsquare.square.Tables.T_SCALE;
import static de.ship.dbppsquare.square.Tables.T_STUDY;
import static de.ship.dbppsquare.square.Tables.T_USERPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUP;
import static de.ship.dbppsquare.square.Tables.V_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.V_STUDY;
import static de.ship.dbppsquare.square.Tables.V_TREE;
import static de.ship.dbppsquare.square.Tables.V_VARIABLEGROUPRELEASE;

import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertResultStep;
import org.jooq.JSONB;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record12;
import org.jooq.Record14;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.SelectConditionStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import de.ship.dbppsquare.square.tables.records.TGroupprivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TPrivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TReleaseRecord;
import de.ship.dbppsquare.square.tables.records.TReportdatareleaseRecord;
import de.ship.dbppsquare.square.tables.records.TUserprivilegeRecord;
import square2.db.control.model.LambdaResultWrapper;
import square2.db.control.model.TableColumnBean;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.datamanagement.ColumnBean;
import square2.modules.model.datamanagement.ElementBean;
import square2.modules.model.datamanagement.ReleaseBean;
import square2.modules.model.datamanagement.ReleaseTableBean;

/**
 * 
 * @author henkej
 *
 */
public class DatamanagementGateway extends JooqGateway {
	private static final Logger LOGGER = LogManager.getLogger(DatamanagementGateway.class);

	/**
	 * generate new datamanagement gateway
	 * 
	 * @param facesContext the context of this gateway
	 */
	public DatamanagementGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
		return getPrivilegeMap(getJooq());
	}

	/**
	 * add release to db
	 * 
	 * @param bean the release bean
	 * @param usnr the id of the user
	 * 
	 * if anything went wrong on database side
	 * @return true for success, false for error
	 */
	public boolean addRelease(ReleaseBean bean, Integer usnr) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		lrw.setBool(false);
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				SelectConditionStep<Record3<String, String, JSONB>> sql = DSL.using(c)
				// @formatter:off
					.select(T_PERSON.FORENAME,
							    T_PERSON.SURNAME,
							    T_PERSON.CONTACT)
					.from(T_RELEASE)
					.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_RELEASE.FK_PRIVILEGE))
					.leftJoin(T_PERSON).on(T_PERSON.USNR.eq(V_PRIVILEGE.FK_USNR))
					.where(T_RELEASE.NAME.eq(bean.getName()));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				StringBuilder buf = new StringBuilder();
				boolean first = true;
				for (Record r : sql.fetch()) {
					buf.append(first ? "" : ", ");
					first = false;
					buf.append(r.get(T_PERSON.FORENAME));
					buf.append(" ");
					buf.append(r.get(T_PERSON.SURNAME));
					JSONB jsonb = r.get(T_PERSON.CONTACT);
					JsonElement contact = new JsonParser().parse(jsonb.toString());
					if (contact != null && contact.isJsonArray()) {
						JsonArray array = contact.getAsJsonArray();
						buf.append(" (").append(array.getAsString()).append(")");
					}
				}
				String users = buf.toString();
				if (users != null && !users.isEmpty()) {
					getFacesContext().notifyError("warn.name.exists.usedby", bean.getName(), users);
					lrw.setBool(false);
				} else {
					bean.setFkPrivilege(addAndGetPrivilegeId(DSL.using(c), null));
					LocalDateTime releasedate = bean.getReleasedate() == null ? null
							: bean.getReleasedate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
					InsertResultStep<TReleaseRecord> sql1 = DSL.using(c)
					// @formatter:off
						.insertInto(T_RELEASE, 
												T_RELEASE.FK_ELEMENT, 
												T_RELEASE.NAME, 
												T_RELEASE.SHORTNAME,
												T_RELEASE.DESCRIPTION, 
												T_RELEASE.ROLLING, 
												T_RELEASE.RELEASEDATE,
												T_RELEASE.LOCATION,
												T_RELEASE.BACKUP,
												T_RELEASE.FK_PRIVILEGE, 
												T_RELEASE.ACTIVE, 
												T_RELEASE.MERGECOLUMN)
						.values(bean.getFkElement(), 
										bean.getName(), 
										bean.getShortname(), 
										bean.getDescription(),
										bean.getRolling(),
										releasedate,
										bean.getLocation(), 
										bean.getBackup(), 
										bean.getFkPrivilege(), 
										bean.getActive(),
										bean.getMergecolumn())
						.returning(T_RELEASE.PK);
					// @formatter:on
					LOGGER.debug("{}", sql1.toString());
					Integer releasePk = sql1.fetchOne().getPk();
					bean.resetPk(releasePk);

					addUserRWXPrivilege(DSL.using(c), bean.getFkPrivilege(), usnr);

					UpdateConditionStep<TPrivilegeRecord> sql2 = DSL.using(c)
					// @formatter:off
						.update(T_PRIVILEGE)
						.set(T_PRIVILEGE.NAME, new StringBuilder("release").append(releasePk).toString())
						.where(T_PRIVILEGE.PK.eq(bean.getFkPrivilege()));
					// @formatter:on
					LOGGER.debug("{}", sql2.toString());
					sql2.execute();
					lrw.setBool(true);
				}
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return lrw.getBool();
	}

	/**
	 * update release referenced by bean's id
	 * 
	 * @param bean the release bean
	 * @return amount of affected lines in db; if < 0, an error has been added to
	 * the faces messages already
	 */
	public Integer updateRelease(ReleaseBean bean) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record3<String, String, JSONB>> sql = jooq
			// @formatter:off
				.select(T_PERSON.FORENAME,
						    T_PERSON.SURNAME,
						    T_PERSON.CONTACT)
				.from(T_RELEASE)
				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_RELEASE.FK_PRIVILEGE))
				.leftJoin(T_PERSON).on(T_PERSON.USNR.eq(V_PRIVILEGE.FK_USNR))
				.where(T_RELEASE.NAME.eq(bean.getName()))
				.and(T_RELEASE.PK.notEqual(bean.getPk()));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			StringBuilder buf = new StringBuilder();
			boolean first = true;
			for (Record r : sql.fetch()) {
				buf.append(first ? "" : ", ");
				first = false;
				buf.append(r.get(T_PERSON.FORENAME));
				buf.append(" ");
				buf.append(r.get(T_PERSON.SURNAME));
				JSONB jsonb = r.get(T_PERSON.CONTACT);
				JsonElement contact = new JsonParser().parse(jsonb.toString());
				if (contact != null && contact.isJsonArray()) {
					JsonArray array = contact.getAsJsonArray();
					buf.append(" (").append(array.getAsString()).append(")");
				}
			}
			String users = buf.toString();
			if (users != null && !users.isEmpty()) {
				getFacesContext().notifyError("warn.name.exists.usedby", bean.getName(), users);
				return -1;
			} else {
				LocalDateTime releasedate = bean.getReleasedate() == null ? null
						: bean.getReleasedate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
				UpdateConditionStep<TReleaseRecord> sql2 = jooq
				// @formatter:off
					.update(T_RELEASE)
					.set(T_RELEASE.FK_ELEMENT, bean.getFkElement())
					.set(T_RELEASE.NAME, bean.getName())
					.set(T_RELEASE.SHORTNAME, bean.getShortname())
					.set(T_RELEASE.DESCRIPTION, bean.getDescription())
					.set(T_RELEASE.ROLLING, bean.getRolling())
					.set(T_RELEASE.RELEASEDATE, releasedate)
					.set(T_RELEASE.LOCATION, bean.getLocation())
					.set(T_RELEASE.BACKUP, bean.getBackup())
					.set(T_RELEASE.FK_PRIVILEGE, bean.getFkPrivilege())
					.set(T_RELEASE.MERGECOLUMN, bean.getMergecolumn())
					.set(T_RELEASE.ACTIVE, bean.getActive())
					.where(T_RELEASE.PK.eq(bean.getPk()));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				return sql2.execute();
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get release referenced by pk
	 * 
	 * @param pk the id of the release
	 * @return the release bean
	 * 
	 * if anything went wrong on database side
	 */
	public ReleaseBean getRelease(Integer pk) {
		try (
				SelectConditionStep<Record12<Integer, String, String, String, Boolean, LocalDateTime, String, Boolean, Integer, Boolean, Integer, EnumAccesslevel>> sql = getJooq()
				// @formatter:off
			.select(T_RELEASE.FK_ELEMENT, 
							T_RELEASE.NAME, 
							T_RELEASE.SHORTNAME, 
							T_RELEASE.DESCRIPTION,
							T_RELEASE.ROLLING, 
							T_RELEASE.RELEASEDATE, 
							T_RELEASE.LOCATION, 
							T_RELEASE.BACKUP,
							T_RELEASE.FK_PRIVILEGE, 
							T_RELEASE.ACTIVE, 
							T_RELEASE.MERGECOLUMN, 
							V_PRIVILEGE.ACL)
			.from(T_RELEASE)
			.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_RELEASE.FK_PRIVILEGE)).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()))
			.where(T_RELEASE.PK.eq(pk));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Record r = sql.fetchOne();
			if (r == null) {
				throw new DataAccessException("no such release with pk = " + pk);
			} else {
				ReleaseBean bean = new ReleaseBean(pk);
				bean.setFkElement(r.get(T_RELEASE.FK_ELEMENT));
				bean.setName(r.get(T_RELEASE.NAME));
				bean.setShortname(r.get(T_RELEASE.SHORTNAME));
				bean.setDescription(r.get(T_RELEASE.DESCRIPTION));
				bean.setRolling(r.get(T_RELEASE.ROLLING));
				LocalDateTime ldt = r.get(T_RELEASE.RELEASEDATE);
				Date releasedate = ldt == null ? null : Date.valueOf(ldt.atZone(ZoneId.systemDefault()).toLocalDate());
				bean.setReleasedate(releasedate);
				bean.setLocation(r.get(T_RELEASE.LOCATION));
				bean.setBackup(r.get(T_RELEASE.BACKUP));
				bean.setFkPrivilege(r.get(T_RELEASE.FK_PRIVILEGE));
				bean.setActive(r.get(T_RELEASE.ACTIVE));
				bean.setMergecolumn(r.get(T_RELEASE.MERGECOLUMN));
				bean.setAcl(new AclBean(r.get(V_PRIVILEGE.ACL)));
				return bean;
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all releases
	 * 
	 * @param activeOnly if true, gets only the active ones
	 * @param restrictUsnr if true, get only the records that the current user has
	 * any privilege on
	 * @return a list of release beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<ReleaseBean> getAllReleases(boolean activeOnly, boolean restrictUsnr) {
		try (
				SelectConditionStep<Record14<Integer, String, Integer, String, String, String, Boolean, LocalDateTime, String, Boolean, Integer, Boolean, Integer, EnumAccesslevel>> sql = getJooq()
				// @formatter:off
					.select(T_RELEASE.FK_ELEMENT, 
									T_ELEMENT.NAME,
									T_RELEASE.PK, 
									T_RELEASE.NAME, 
									T_RELEASE.SHORTNAME, 
									T_RELEASE.DESCRIPTION, 
									T_RELEASE.ROLLING,
									T_RELEASE.RELEASEDATE, 
									T_RELEASE.LOCATION, 	
									T_RELEASE.BACKUP, 
									T_RELEASE.FK_PRIVILEGE,
									T_RELEASE.ACTIVE, 
									T_RELEASE.MERGECOLUMN, 
									V_PRIVILEGE.ACL)
					.from(T_RELEASE)
					.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_RELEASE.FK_ELEMENT))
					.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_RELEASE.FK_PRIVILEGE)).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()))
					.where(T_RELEASE.ACTIVE.in(activeOnly, true)); // this way, activeOny is respected to filter if set; otherwise, false and true are accepted
				// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<ReleaseBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				ReleaseBean bean = new ReleaseBean(r.get(T_RELEASE.PK));
				bean.setFkElement(r.get(T_RELEASE.FK_ELEMENT));
				bean.setStudyName(r.get(T_ELEMENT.NAME));
				bean.setName(r.get(T_RELEASE.NAME));
				bean.setShortname(r.get(T_RELEASE.SHORTNAME));
				bean.setDescription(r.get(T_RELEASE.DESCRIPTION));
				bean.setRolling(r.get(T_RELEASE.ROLLING));
				LocalDateTime ldt = r.get(T_RELEASE.RELEASEDATE);
				Date releasedate = ldt == null ? null : Date.valueOf(ldt.atZone(ZoneId.systemDefault()).toLocalDate());
				bean.setReleasedate(releasedate);
				bean.setLocation(r.get(T_RELEASE.LOCATION));
				bean.setBackup(r.get(T_RELEASE.BACKUP));
				bean.setFkPrivilege(r.get(T_RELEASE.FK_PRIVILEGE));
				bean.setActive(r.get(T_RELEASE.ACTIVE));
				bean.setMergecolumn(r.get(T_RELEASE.MERGECOLUMN));
				bean.setAcl(new AclBean(r.get(V_PRIVILEGE.ACL)));
				if (!restrictUsnr || bean.getAcl().isNotEmpty()) {
					list.add(bean);
				}
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete release referenced by pk and its rights
	 * 
	 * @param pk the id of the release
	 * @param fkPrivilege the id of the privileeg
	 * @return number of affected rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer deleteRelease(Integer pk, Integer fkPrivilege) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				DeleteConditionStep<TUserprivilegeRecord> sql = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_USERPRIVILEGE)
					.where(T_USERPRIVILEGE.FK_PRIVILEGE.eq(fkPrivilege));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				DeleteConditionStep<TGroupprivilegeRecord> sql1 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_GROUPPRIVILEGE)
					.where(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(fkPrivilege));
				// @formatter:on
				LOGGER.debug("{}", sql1.toString());
				lrw.addToInteger(sql1.execute());

				DeleteConditionStep<TReportdatareleaseRecord> sql2 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_REPORTDATARELEASE)
					.where(T_REPORTDATARELEASE.FK_RELEASE.eq(pk));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());

				DeleteConditionStep<TReleaseRecord> sql3 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_RELEASE)
					.where(T_RELEASE.PK.eq(pk));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				lrw.addToInteger(sql3.execute());

				DeleteConditionStep<TPrivilegeRecord> sql4 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_PRIVILEGE)
					.where(T_PRIVILEGE.PK.eq(fkPrivilege));
				// @formatter:on
				LOGGER.debug("{}", sql4.toString());
				lrw.addToInteger(sql4.execute());
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return lrw.getInteger();
	}

	/**
	 * load release table information
	 * 
	 * @param pk the id of the release
	 * 
	 * @return the list of release tables
	 */
	public List<ReleaseTableBean> loadReleaseTables(Integer pk) {
		final Map<String, ReleaseTableBean> collection = new HashMap<>();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				SelectConditionStep<Record1<String>> sql = DSL.using(c)
				// @formatter:off
					.select(T_RELEASE.LOCATION)
					.from(T_RELEASE)
					.where(T_RELEASE.PK.eq(pk));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				String schemaName = sql.fetchOne(T_RELEASE.LOCATION);
				for (TableColumnBean tcb : getAllTableColumnsOfSchema(DSL.using(c), schemaName)) {
					ReleaseTableBean bean = collection.get(tcb.getTableName());
					if (bean == null) {
						bean = new ReleaseTableBean(tcb.getTableName(), tcb.getRowcount());
						collection.put(tcb.getTableName(), bean);
					}
					bean.getColumns().add(new ColumnBean(tcb.getColumnName(), tcb.getDataType()));
				}
			});
			List<ReleaseTableBean> list = new ArrayList<>(collection.values());
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * execute lines of script
	 * 
	 * @param script the sql script to be executed in the database
	 * 
	 * if anything went wrong on database side
	 */
	public void runScript(List<String> script) {
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				for (String line : script) {
					LOGGER.debug("{}", line);
					DSL.using(c).execute(line);
				}
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get types from columns - look into T_VARIABLE and find scale_niveaus
	 * 
	 * @param columns the columns
	 * @param studyId the id of the study
	 * @return a map of column types
	 * 
	 * if anything went wrong on database side
	 */
	public Map<String, String> getTypesFromColumns(List<String> columns, Integer studyId) {
		Map<String, String> result = new HashMap<>();
		try (SelectConditionStep<Record2<String, String>> sql = getJooq()
		// @formatter:off
			.select(T_ELEMENT.NAME, 
							T_SCALE.NAME)
			.from(T_VARIABLE)
			.leftJoin(T_SCALE).on(T_SCALE.PK.eq(T_VARIABLE.FK_SCALE))
			.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLE.FK_ELEMENT))
			.leftJoin(V_STUDY).on(V_STUDY.KEY_ELEMENT.eq(T_VARIABLE.FK_ELEMENT))
			.where(V_STUDY.FK_ELEMENT.eq(studyId))
			.and(T_ELEMENT.NAME.in(columns));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				String scaleName = r.get(T_SCALE.NAME);
				String varname = r.get(T_ELEMENT.NAME);
				if (scaleName == null) {
					result.put(varname, "scale is null");
				} else if ("text".equals(scaleName)) {
					result.put(varname, "text");
				} else if ("nominal".equals(scaleName) || "ordinal".equals(scaleName) || "binary".equals(scaleName)) {
					result.put(varname, "integer");
				} else if ("count".equals(scaleName) || "metric".equals(scaleName)) {
					result.put(varname, "double precision");
				} else if ("datetime".equals(scaleName)) {
					result.put(varname, "timestamp");
				} else {
					result.put(varname, "unknown scale " + scaleName);
				}
			}
			return result;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all elements that are studies or departments from db that the user has
	 * usage rights on
	 * 
	 * @return list of found elements
	 * 
	 * if anything went wrong on database side
	 */
	public List<ElementBean> getElements() {
		List<ElementBean> list = new ArrayList<>();
		Integer usnr = getFacesContext().getUsnr();
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record4<String, String, Integer, JSONB>> sql = jooq
			// @formatter:off
				.select(T_ELEMENT.NAME, 
								V_TREE.TREE,
								T_ELEMENT.PK.as("id"),
								T_ELEMENT.TRANSLATION)
				.from(T_ELEMENT)
				.leftJoin(V_TREE).on(V_TREE.FK_ELEMENT.eq(T_ELEMENT.PK))
				.innerJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
				.where(V_PRIVILEGE.FK_USNR.eq(usnr))
				.and(T_ELEMENT.PK.notIn(jooq
					.select(T_VARIABLE.FK_ELEMENT)
					.from(T_VARIABLE)))
				.and(T_ELEMENT.FK_PARENT.in(jooq
					.select(T_STUDY.FK_ELEMENT)
					.from(T_STUDY))
				.or(T_ELEMENT.PK.in(jooq
					.select(T_STUDY.FK_ELEMENT)
					.from(T_STUDY))));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				Integer id = r.get("id", Integer.class);
				String name = r.get(T_ELEMENT.NAME);
				JSONB translationJson = r.get(T_ELEMENT.TRANSLATION);
				String translation = translationJson == null ? null
						: getFacesContext().selectTranslation(translationJson.data());
				String description = new StringBuilder().append(translation).append(" (").append(r.get(V_TREE.TREE)).append(")")
						.toString();
				list.add(new ElementBean(id, name, description));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get the unique name of the id
	 * 
	 * @param id the id of the element
	 * @return the unique name or null
	 */
	public String getUniqueName(Integer id) {
		try (SelectConditionStep<Record1<String>> sql = getJooq()
		// @formatter:off
        	.select(T_ELEMENT.UNIQUE_NAME)
        	.from(T_ELEMENT)
        	.where(T_ELEMENT.PK.eq(id));
    		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Record r = sql.fetchOne();
			if (r == null) {
				throw new DataAccessException("no such element with pk = " + id);
			} else {
				return r.get(T_ELEMENT.UNIQUE_NAME);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get summary of release
	 * 
	 * @param pk the id of the release
	 * @return a summary
	 */
	public List<String> getVariablegroupNamesUsingRelease(Integer pk) {
		Integer[] pks = new Integer[] { pk };
		try (SelectConditionStep<Record1<String>> sql = getJooq()
		// @formatter:off
        	.select(T_VARIABLEGROUP.NAME)
        	.from(V_VARIABLEGROUPRELEASE)
        	.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(V_VARIABLEGROUPRELEASE.FK_VARIABLEGROUP))
        	.where(V_VARIABLEGROUPRELEASE.RELEASE_PKS.contains(pks));
    		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Set<String> set = new HashSet<>();
			for (Record1<String> r : sql.fetch()) {
				set.add(r.get(T_VARIABLEGROUP.NAME));
			}
			List<String> list = new ArrayList<>(set);
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
