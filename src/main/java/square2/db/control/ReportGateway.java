package square2.db.control;

import static de.ship.dbppsquare.square.Tables.R_MATRIXCANDIDATE;
import static de.ship.dbppsquare.square.Tables.T_CALCPERF;
import static de.ship.dbppsquare.square.Tables.T_FUNCTION;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONINPUTTYPE;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONLIST;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONOUTPUT;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONOUTPUTTYPE;
import static de.ship.dbppsquare.square.Tables.T_HTMLCOMPONENT;
import static de.ship.dbppsquare.square.Tables.T_MATRIXCOLUMN;
import static de.ship.dbppsquare.square.Tables.T_MATRIXCOLUMNPARAMETER;
import static de.ship.dbppsquare.square.Tables.T_MATRIXELEMENTPARAMETER;
import static de.ship.dbppsquare.square.Tables.T_MATRIXELEMENTVALUE;
import static de.ship.dbppsquare.square.Tables.T_RELEASE;
import static de.ship.dbppsquare.square.Tables.T_REPORT;
import static de.ship.dbppsquare.square.Tables.T_REPORTDATARELEASE;
import static de.ship.dbppsquare.square.Tables.T_REPORTPERIOD;
import static de.ship.dbppsquare.square.Tables.T_REPORTPROPERTY;
import static de.ship.dbppsquare.square.Tables.T_SNIPPLET;
import static de.ship.dbppsquare.square.Tables.T_SNIPPLETTYPE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUP;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUPELEMENT;
import static de.ship.dbppsquare.square.Tables.T_WIDGETBASED;
import static de.ship.dbppsquare.square.Tables.V_ORPHANED_FUNCTIONS;
import static de.ship.dbppsquare.square.Tables.V_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.V_REPORT;
import static de.ship.dbppsquare.square.Tables.V_REPORTSNIPPLET;
import static de.ship.dbppsquare.squareout.Tables.T_CALCULATIONRESULT;
import static de.ship.dbppsquare.squareout.Tables.T_REPORTDOCUMENT;
import static de.ship.dbppsquare.squareout.Tables.T_REPORTOUTPUT;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.Field;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.InsertOnDuplicateStep;
import org.jooq.InsertResultStep;
import org.jooq.InsertReturningStep;
import org.jooq.InsertValuesStep2;
import org.jooq.InsertValuesStep3;
import org.jooq.JSONB;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record10;
import org.jooq.Record12;
import org.jooq.Record13;
import org.jooq.Record15;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.Record5;
import org.jooq.Record7;
import org.jooq.Record8;
import org.jooq.Record9;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.SelectOrderByStep;
import org.jooq.SelectSeekStep1;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import de.ship.dbppsquare.square.enums.EnumResultconverter;
import de.ship.dbppsquare.square.tables.records.TCalcperfRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixcolumnparameterRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixelementparameterRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixelementvalueRecord;
import de.ship.dbppsquare.square.tables.records.TReportRecord;
import de.ship.dbppsquare.square.tables.records.TReportdatareleaseRecord;
import de.ship.dbppsquare.square.tables.records.TReportperiodRecord;
import de.ship.dbppsquare.square.tables.records.TReportpropertyRecord;
import de.ship.dbppsquare.square.tables.records.TSnippletRecord;
import de.ship.dbppsquare.squareout.tables.records.TCalculationresultRecord;
import de.ship.dbppsquare.squareout.tables.records.TReportdocumentRecord;
import de.ship.dbppsquare.squareout.tables.records.TReportoutputRecord;
import square2.db.control.model.LambdaResultWrapper;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.datamanagement.ReleaseBean;
import square2.modules.model.report.CalculationResultFileBean;
import square2.modules.model.report.EnumShowNames;
import square2.modules.model.report.FunctionSelectionBean;
import square2.modules.model.report.NamedReportBean;
import square2.modules.model.report.PerformanceBean;
import square2.modules.model.report.ReportBean;
import square2.modules.model.report.design.AnaparfunBean;
import square2.modules.model.report.design.CodelineBean;
import square2.modules.model.report.design.ReportDesignBean;
import square2.modules.model.report.design.ReportPeriodBean;
import square2.modules.model.report.design.SnippletBean;
import square2.modules.model.report.design.SnipplettypeBean;
import square2.modules.model.report.property.ReportPropertyBean;
import square2.modules.model.report.property.WidgetbasedBean;
import square2.modules.model.template.report.MatrixcolumnParameterBean;

/**
 * 
 * @author henkej
 *
 */
public class ReportGateway extends JooqGateway {
	private static final Logger LOGGER = LogManager.getLogger(ReportGateway.class);

	// cache
	private Map<Integer, String> anaparfunMap;
	private Map<Integer, String> functionoutputMap;

	/**
	 * generate new report gateway
	 * 
	 * @param facesContext the context for this gateway
	 */
	public ReportGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
		return super.getPrivilegeMap(getJooq());
	}

	/**
	 * convert localdatetime to date
	 * 
	 * @param ldt the local date time to convert
	 * @return the date object of ldt or null
	 */
	private Date ldt2date(LocalDateTime ldt) {
		return ldt == null ? null : Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
	}

	/**
	 * get all report templates
	 * 
	 * @return list of report templates
	 */
	public List<ReportDesignBean> getAllReportTemplates() {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record5<Integer, String, String, Integer, LocalDateTime>> sql = jooq
			// @formatter:off
				.select(T_REPORT.PK, 
								T_REPORT.NAME, 
								T_REPORT.DESCRIPTION, 
								T_REPORT.FK_FUNCTIONLIST,
								T_REPORT.LASTCHANGE)
				.from(T_REPORT)
				.where(T_REPORT.FK_PARENT.isNull());
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			List<ReportDesignBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pagelength = getFacesContext().getProfile().getConfigInteger("config.template.paginator.pagelength",
						20);
				LocalDateTime lc = r.get(T_REPORT.LASTCHANGE);
				Timestamp lastchange = lc == null ? null : Timestamp.valueOf(lc);
				ReportDesignBean bean = new ReportDesignBean(new AclBean(), null, pagelength, lastchange, 0);
				bean.setId(r.get(T_REPORT.PK));
				bean.setName(r.get(T_REPORT.NAME));
				bean.setDescription(r.get(T_REPORT.DESCRIPTION));
				bean.setFkFunctionlist(r.get(T_REPORT.FK_FUNCTIONLIST));
				SelectConditionStep<Record3<Integer, Integer, String>> sql2 = jooq
				// @formatter:off
					.select(T_MATRIXCOLUMN.PK, 
									T_FUNCTION.PK, 
									T_FUNCTION.NAME)
					.from(T_REPORT)
					.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(T_REPORT.FK_FUNCTIONLIST))
					.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
					.where(T_REPORT.PK.eq(bean.getId()));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				for (Record3<Integer, Integer, String> r2 : sql2.fetch()) {
					bean.getAnaparfuns()
							.add(new AnaparfunBean(r2.get(T_MATRIXCOLUMN.PK), r2.get(T_FUNCTION.PK), r2.get(T_FUNCTION.NAME)));
				}
				SelectConditionStep<Record13<Integer, String, Integer, Integer, Integer, Integer, String, String, Integer, String, String, String, String>> sql3 = jooq
				// @formatter:off
					.select(T_SNIPPLET.PK, 
							T_SNIPPLET.MARKDOWNTEXT,
							T_SNIPPLET.FK_FUNCTIONOUTPUT,
							T_SNIPPLET.FK_MATRIXCOLUMN,
							T_SNIPPLET.ORDER_NR, 
							T_SNIPPLETTYPE.PK,
							T_SNIPPLETTYPE.TYPE,
							T_SNIPPLETTYPE.TEMPLATE,
							T_SNIPPLETTYPE.ORDER_NR,
							T_HTMLCOMPONENT.NAME,
							T_MATRIXCOLUMN.NAME,
							T_MATRIXCOLUMN.DESCRIPTION,
							T_FUNCTION.VIGNETTE)
					.from(T_SNIPPLET)
					.leftJoin(T_SNIPPLETTYPE).on(T_SNIPPLETTYPE.PK.eq(T_SNIPPLET.FK_SNIPPLETTYPE))
					.leftJoin(T_HTMLCOMPONENT).on(T_HTMLCOMPONENT.PK.eq(T_SNIPPLETTYPE.FK_HTMLCOMPONENT))
					.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_SNIPPLET.FK_MATRIXCOLUMN))
					.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
					.where(T_SNIPPLET.FK_REPORT.eq(bean.getId()));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				for (Record r3 : sql3.fetch()) {
					Integer snippletPk = r3.get(T_SNIPPLET.PK);
					Integer orderNr = r3.get(T_SNIPPLET.ORDER_NR);
					String markdowntext = r3.get(T_SNIPPLET.MARKDOWNTEXT);
					Integer typePk = r3.get(T_SNIPPLETTYPE.PK);
					Integer snipplettypeOrderNr = r3.get(T_SNIPPLETTYPE.ORDER_NR);
					String type = r3.get(T_SNIPPLETTYPE.TYPE);
					String templ = r3.get(T_SNIPPLETTYPE.TEMPLATE);
					String htmlComponent = r3.get(T_HTMLCOMPONENT.NAME);
					Integer fkMatrixcolumn = r3.get(T_SNIPPLET.FK_MATRIXCOLUMN);
					Integer fkFunctionoutput = r3.get(T_SNIPPLET.FK_FUNCTIONOUTPUT);
					String nameMatrixcolumn = r3.get(T_MATRIXCOLUMN.NAME);
					String descriptionMatrixcolumn = r3.get(T_MATRIXCOLUMN.DESCRIPTION);
					String vignette = r3.get(T_FUNCTION.VIGNETTE);
					MatrixcolumnParameterBean mcBean = new MatrixcolumnParameterBean(fkMatrixcolumn, nameMatrixcolumn,
							descriptionMatrixcolumn, vignette);
					SnippletBean sBean = new SnippletBean(snippletPk, orderNr, markdowntext, fkFunctionoutput, mcBean);
					sBean.setType(new SnipplettypeBean(typePk, snipplettypeOrderNr, type, templ, htmlComponent));
					bean.getSnipplets().add(sBean);
				}
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all reports from db; if chooseTemplates is true, get only the ones that
	 * have no variablegroup assigned, otherwise, get the ones that do have
	 * variablegroups
	 * 
	 * @param chooseTemplates flag to determine if templates should be chosen
	 * @param respectVariableGroupPrivileges because report templates do not know
	 * any variable group and can therefore not been filtered by that
	 * @return a list of report beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<ReportDesignBean> getAllReportsOld(Boolean chooseTemplates, Boolean respectVariableGroupPrivileges) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectOrderByStep<Record7<String, Integer, String, Integer, LocalDateTime, EnumAccesslevel, Integer>> sql = jooq
			// @formatter:off
				.selectDistinct(V_REPORT.DESCRIPTION, 
												V_REPORT.PK, 
												V_REPORT.NAME,
												V_REPORT.FK_FUNCTIONLIST,
												V_REPORT.LASTCHANGE,
												V_PRIVILEGE.ACL,
												T_VARIABLEGROUP.FK_PRIVILEGE)
				.from(V_REPORT)
				.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(V_REPORT.FK_VARIABLEGROUP))
				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE))
				.and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()))
				.where(chooseTemplates ? V_REPORT.FK_VARIABLEGROUP.isNull() : V_REPORT.FK_VARIABLEGROUP.isNotNull())
				.unionAll(jooq
					.select(T_REPORT.DESCRIPTION, 
									T_REPORT.PK, 
									T_REPORT.NAME, 
									T_REPORT.FK_FUNCTIONLIST,
									T_REPORT.LASTCHANGE,
									DSL.field("null", EnumAccesslevel.class), 
									DSL.field("null", Integer.class))
					.from(T_REPORT)
					.where(T_REPORT.FK_VARIABLEGROUP.isNull()));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Map<Integer, ReportDesignBean> tmp = new LinkedHashMap<>();
			for (Record r : sql.fetch()) {
				Integer reportId = r.get(V_REPORT.PK);
				if (reportId == null) {
					StringBuilder buf = new StringBuilder("pk of report is null for record ");
					buf.append(r.toString());
					buf.append(" from query ");
					buf.append(sql.toString());
					throw new DataAccessException(buf.toString());
				}
				EnumAccesslevel acl = r.get(V_PRIVILEGE.ACL);
				ReportDesignBean bean = tmp.get(reportId);
				Integer vargroupPrivilege = r.get(T_VARIABLEGROUP.FK_PRIVILEGE);
				LocalDateTime lc = r.get(V_REPORT.LASTCHANGE);
				Timestamp lastchange = lc == null ? null : Timestamp.valueOf(lc);
				if (bean == null) {
					Integer pagelength = getFacesContext().getProfile().getConfigInteger("config.template.paginator.pagelength",
							20);
					bean = new ReportDesignBean(new AclBean(acl), vargroupPrivilege, pagelength, lastchange, 0);
					bean.setId(reportId);
					bean.setName(r.get(V_REPORT.NAME));
					bean.setDescription(r.get(V_REPORT.DESCRIPTION));
					bean.setFkFunctionlist(r.get(V_REPORT.FK_FUNCTIONLIST));
					if (bean.getAcl().getExecute() || !respectVariableGroupPrivileges) {
						tmp.put(reportId, bean);
					} else if (chooseTemplates && vargroupPrivilege == null) {
						tmp.put(reportId, bean);
					}
				}
			}
			return new ArrayList<>(tmp.values());
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all reports from db; if chooseTemplates is true, get only the ones that
	 * have no variablegroup assigned, otherwise, get the ones that do have
	 * variablegroups
	 * 
	 * @param chooseTemplates flag to determine if templates should be chosen
	 * @param respectVariableGroupPrivileges because report templates do not know
	 * any variable group and can therefore not been filtered by that
	 * @return a list of report beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<ReportBean> getAllReports(Boolean chooseTemplates, Boolean respectVariableGroupPrivileges) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectOrderByStep<Record7<String, Integer, String, Integer, LocalDateTime, EnumAccesslevel, Integer>> sql = jooq
			// @formatter:off
				.selectDistinct(V_REPORT.DESCRIPTION, 
												V_REPORT.PK, 
												V_REPORT.NAME,
												V_REPORT.FK_FUNCTIONLIST,
												V_REPORT.LASTCHANGE,
												V_PRIVILEGE.ACL,
												T_VARIABLEGROUP.FK_PRIVILEGE)
				.from(V_REPORT)
				.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(V_REPORT.FK_VARIABLEGROUP))
				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE))
				.and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()))
				.where(chooseTemplates ? V_REPORT.FK_VARIABLEGROUP.isNull() : V_REPORT.FK_VARIABLEGROUP.isNotNull())
				.unionAll(jooq
					.select(T_REPORT.DESCRIPTION, 
									T_REPORT.PK, 
									T_REPORT.NAME, 
									T_REPORT.FK_FUNCTIONLIST,
									T_REPORT.LASTCHANGE,
									DSL.field("null", EnumAccesslevel.class), 
									DSL.field("null", Integer.class))
					.from(T_REPORT)
					.where(T_REPORT.FK_VARIABLEGROUP.isNull()));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Map<Integer, ReportBean> tmp = new LinkedHashMap<>();
			for (Record r : sql.fetch()) {
				Integer reportId = r.get(V_REPORT.PK);
				if (reportId == null) {
					StringBuilder buf = new StringBuilder("pk of report is null for record ");
					buf.append(r.toString());
					buf.append(" from query ");
					buf.append(sql.toString());
					throw new DataAccessException(buf.toString());
				}
				EnumAccesslevel acl = r.get(V_PRIVILEGE.ACL);
				ReportBean bean = tmp.get(reportId);
				Integer vargroupPrivilege = r.get(T_VARIABLEGROUP.FK_PRIVILEGE);
				LocalDateTime lc = r.get(V_REPORT.LASTCHANGE);
				Timestamp lastchange = lc == null ? null : Timestamp.valueOf(lc);
				if (bean == null) {
					AclBean aclBean = new AclBean(acl);
					bean = new ReportBean(reportId);
					bean.setName(r.get(V_REPORT.NAME));
					bean.setDescription(r.get(V_REPORT.DESCRIPTION));
					bean.setFkFunctionlist(r.get(V_REPORT.FK_FUNCTIONLIST));
					bean.setLastchange(lastchange);
					if (aclBean.getExecute() || !respectVariableGroupPrivileges) {
						tmp.put(reportId, bean);
					} else if (chooseTemplates && vargroupPrivilege == null) {
						tmp.put(reportId, bean);
					}
				}
			}
			return new ArrayList<>(tmp.values());
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get report template referenced by repirtId with its snipplets
	 * 
	 * @param reportId the id of the report
	 * @return the template as a report bean
	 * 
	 * if anything went wrong on database side
	 */
	public ReportDesignBean getReportTemplate(Integer reportId) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record5<Integer, String, String, Integer, LocalDateTime>> sql = jooq
			// @formatter:off
				.select(T_REPORT.PK, 
								T_REPORT.NAME, 
								T_REPORT.DESCRIPTION, 
								T_REPORT.FK_FUNCTIONLIST,
								T_REPORT.LASTCHANGE)
				.from(T_REPORT)
				.where(T_REPORT.PK.eq(reportId));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			ReportDesignBean bean = null;
			for (Record r : sql.fetch()) {
				Integer pagelength = getFacesContext().getProfile().getConfigInteger("config.template.paginator.pagelength",
						20);
				LocalDateTime lc = r.get(T_REPORT.LASTCHANGE);
				Timestamp lastchange = lc == null ? null : Timestamp.valueOf(lc);
				bean = new ReportDesignBean(new AclBean(), null, pagelength, lastchange, 0);
				bean.setId(r.get(T_REPORT.PK));
				bean.setName(r.get(T_REPORT.NAME));
				bean.setDescription(r.get(T_REPORT.DESCRIPTION));
				bean.setFkFunctionlist(r.get(T_REPORT.FK_FUNCTIONLIST));
			}
			if (bean == null) {
				throw new DataAccessException("no report found with id = " + reportId);
			}
			SelectSeekStep1<Record13<Integer, String, Integer, Integer, Integer, String, String, Integer, Integer, String, String, String, String>, Integer> sql2 = jooq
			// @formatter:off
				.select(T_SNIPPLET.PK, 
					    T_SNIPPLET.MARKDOWNTEXT,
					    T_SNIPPLET.FK_FUNCTIONOUTPUT,
					    T_SNIPPLET.FK_MATRIXCOLUMN,
					    T_SNIPPLET.FK_SNIPPLETTYPE, 
					    T_SNIPPLETTYPE.TEMPLATE,
					    T_SNIPPLETTYPE.TYPE, 
					    T_SNIPPLETTYPE.ORDER_NR,
					    T_SNIPPLET.ORDER_NR,
					    T_MATRIXCOLUMN.NAME,
					    T_MATRIXCOLUMN.DESCRIPTION,
					    T_FUNCTION.VIGNETTE,
					    T_HTMLCOMPONENT.NAME)
				.from(T_SNIPPLET)
				.leftJoin(T_SNIPPLETTYPE).on(T_SNIPPLETTYPE.PK.eq(T_SNIPPLET.FK_SNIPPLETTYPE))
				.leftJoin(T_HTMLCOMPONENT).on(T_HTMLCOMPONENT.PK.eq(T_SNIPPLETTYPE.FK_HTMLCOMPONENT))
	      .leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_SNIPPLET.FK_MATRIXCOLUMN))
	      .leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
				.where(T_SNIPPLET.FK_REPORT.eq(bean.getId()))
				.orderBy(T_SNIPPLET.ORDER_NR);
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());
			for (Record r : sql2.fetch()) {
				Integer id = r.get(T_SNIPPLET.PK);
				Integer fkMatrixcolumn = r.get(T_SNIPPLET.FK_MATRIXCOLUMN);
				String name = r.get(T_MATRIXCOLUMN.NAME);
				String description = r.get(T_MATRIXCOLUMN.DESCRIPTION);
				String vignette = r.get(T_FUNCTION.VIGNETTE);
				MatrixcolumnParameterBean mcBean = new MatrixcolumnParameterBean(fkMatrixcolumn, name, description, vignette);
				SnippletBean snip = new SnippletBean(id, r.get(T_SNIPPLET.ORDER_NR), r.get(T_SNIPPLET.MARKDOWNTEXT),
						r.get(T_SNIPPLET.FK_FUNCTIONOUTPUT), mcBean);

				Integer pk = r.get(T_SNIPPLET.FK_SNIPPLETTYPE);
				Integer orderNr = r.get(T_SNIPPLETTYPE.ORDER_NR);
				String type = r.get(T_SNIPPLETTYPE.TYPE);
				String template = r.get(T_SNIPPLETTYPE.TEMPLATE);
				String htmlComponent = r.get(T_HTMLCOMPONENT.NAME);
				snip.setType(new SnipplettypeBean(pk, orderNr, type, template, htmlComponent));
				bean.getSnipplets().add(snip);
			}
			return bean;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get report referenced by reportId with its snipplets
	 * 
	 * @param reportId the id of the report
	 * @return the report bean
	 * 
	 * if anything went wrong on database side
	 */
	public ReportDesignBean getReportDesignBean(Integer reportId) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record10<Integer, String, String, Integer, Integer, Integer, LocalDateTime, EnumAccesslevel, Integer, String>> sql = jooq
			// @formatter:off
				.select(V_REPORT.PK, 
								V_REPORT.NAME, 
								V_REPORT.DESCRIPTION, 
								V_REPORT.FK_PARENT,
								V_REPORT.FK_FUNCTIONLIST,
								V_REPORT.FK_VARIABLEGROUP,
								V_REPORT.LASTCHANGE,
								V_PRIVILEGE.ACL,
								T_VARIABLEGROUP.FK_PRIVILEGE, 
								T_REPORT.NAME)
				.from(V_REPORT)
				.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(V_REPORT.FK_VARIABLEGROUP))
				.leftJoin(T_REPORT).on(T_REPORT.PK.eq(V_REPORT.FK_PARENT))
				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE)).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()))
				.where(V_REPORT.PK.eq(reportId));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			ReportDesignBean bean = null;
			for (Record r : sql.fetch()) {
				if (bean == null) {
					Integer pagelength = getFacesContext().getProfile().getConfigInteger("config.template.paginator.pagelength",
							20);
					LocalDateTime lc = r.get(V_REPORT.LASTCHANGE);
					Timestamp lastchange = lc == null ? null : Timestamp.valueOf(lc);
					bean = new ReportDesignBean(new AclBean(r.get(V_PRIVILEGE.ACL)), r.get(T_VARIABLEGROUP.FK_PRIVILEGE),
							pagelength, lastchange, 0);
					bean.setId(r.get(V_REPORT.PK));
					bean.setName(r.get(V_REPORT.NAME));
					bean.setDescription(r.get(V_REPORT.DESCRIPTION));
					bean.setParent(r.get(V_REPORT.FK_PARENT));
					bean.setParentName(r.get(T_REPORT.NAME));
					bean.setFkFunctionlist(r.get(V_REPORT.FK_FUNCTIONLIST));
					bean.setVariablegroup(r.get(V_REPORT.FK_VARIABLEGROUP));
				}
			}
			if (bean == null) {
				// there are no reports
				return null;
			} else if (bean.getId() == null) {
				// there are no snipplets in this report - maybe it has been created right now?
				return bean;
			}
			Map<Integer, SnippletBean> tmp = new LinkedHashMap<>();

			SelectConditionStep<Record13<Integer, String, Integer, Integer, Integer, String, String, Integer, Integer, String, String, String, String>> sql1 = jooq
			// @formatter:off
				.select(T_SNIPPLET.PK,
				        T_SNIPPLET.MARKDOWNTEXT,
				        T_SNIPPLET.FK_FUNCTIONOUTPUT,
				        T_SNIPPLET.FK_MATRIXCOLUMN,
				        T_SNIPPLET.FK_SNIPPLETTYPE, 
				        T_SNIPPLETTYPE.TEMPLATE,
				        T_SNIPPLETTYPE.TYPE, 
				        T_SNIPPLETTYPE.ORDER_NR,
				        T_SNIPPLET.ORDER_NR,
	              T_MATRIXCOLUMN.NAME,
	              T_MATRIXCOLUMN.DESCRIPTION,
	              T_FUNCTION.VIGNETTE,
				        T_HTMLCOMPONENT.NAME)
				.from(T_SNIPPLET)
				.leftJoin(T_SNIPPLETTYPE).on(T_SNIPPLETTYPE.PK.eq(T_SNIPPLET.FK_SNIPPLETTYPE))
				.leftJoin(T_HTMLCOMPONENT).on(T_HTMLCOMPONENT.PK.eq(T_SNIPPLETTYPE.FK_HTMLCOMPONENT))
	      .leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_SNIPPLET.FK_MATRIXCOLUMN))
	      .leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
				.where(T_SNIPPLET.FK_REPORT.eq(bean.getId()));
			// @formatter:on		
			LOGGER.debug("{}", sql1.toString());
			for (Record r : sql1.fetch()) {
				Integer id = r.get(T_SNIPPLET.PK);
				SnippletBean snip = tmp.get(id);
				if (snip == null) {
					Integer fkMatrixcolumn = r.get(T_SNIPPLET.FK_MATRIXCOLUMN);
					String name = r.get(T_MATRIXCOLUMN.NAME);
					String description = r.get(T_MATRIXCOLUMN.DESCRIPTION);
					String vignette = r.get(T_FUNCTION.VIGNETTE);
					MatrixcolumnParameterBean mcBean = new MatrixcolumnParameterBean(fkMatrixcolumn, name, description, vignette);
					snip = new SnippletBean(id, r.get(T_SNIPPLET.ORDER_NR), r.get(T_SNIPPLET.MARKDOWNTEXT),
							r.get(T_SNIPPLET.FK_FUNCTIONOUTPUT), mcBean);
					Integer pk = r.get(T_SNIPPLET.FK_SNIPPLETTYPE);
					Integer stOrder = r.get(T_SNIPPLETTYPE.ORDER_NR);
					String type = r.get(T_SNIPPLETTYPE.TYPE);
					String templ = r.get(T_SNIPPLETTYPE.TEMPLATE);
					String htmlComponent = r.get(T_HTMLCOMPONENT.NAME);
					snip.setType(new SnipplettypeBean(pk, stOrder, type, templ, htmlComponent));
					tmp.put(id, snip);
				}
			}
			for (Map.Entry<Integer, SnippletBean> entry : tmp.entrySet()) {
				bean.getSnipplets().add(entry.getValue());
			}
			// ensure sorting order
			bean.getSnipplets().sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return bean;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get report referenced by reportId with its snipplets
	 * 
	 * @param reportId the id of the report
	 * @return the report bean or null if not found
	 */
	public NamedReportBean getReport(Integer reportId) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record12<Integer, String, String, Integer, Integer, Integer, Integer, LocalDateTime, EnumAccesslevel, Integer, String, String>> sql = jooq
			// @formatter:off
				.select(V_REPORT.PK, 
								V_REPORT.NAME, 
								V_REPORT.DESCRIPTION, 
								V_REPORT.FK_PARENT,
								V_REPORT.FK_LATEXDEF,
								V_REPORT.FK_FUNCTIONLIST,
								V_REPORT.FK_VARIABLEGROUP,
								V_REPORT.LASTCHANGE,
								V_PRIVILEGE.ACL,
								T_VARIABLEGROUP.FK_PRIVILEGE,
								T_VARIABLEGROUP.NAME,
								T_FUNCTIONLIST.NAME)
				.from(V_REPORT)
				.leftJoin(T_FUNCTIONLIST).on(T_FUNCTIONLIST.PK.eq(V_REPORT.FK_FUNCTIONLIST))
				.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(V_REPORT.FK_VARIABLEGROUP))
				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE)).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()))
				.where(V_REPORT.PK.eq(reportId));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Record r = sql.fetchOne();
			if (r == null) {
				throw new DataAccessException("no such report with id = " + (reportId == null ? "null" : reportId));
			} else {
				ReportBean bean = new ReportBean(reportId);
				bean.setName(r.get(V_REPORT.NAME));
				bean.setDescription(r.get(V_REPORT.DESCRIPTION));
				bean.setFkParent(r.get(V_REPORT.FK_PARENT));
				bean.setFkLatexdef(r.get(V_REPORT.FK_LATEXDEF));
				bean.setFkFunctionlist(r.get(V_REPORT.FK_FUNCTIONLIST));
				bean.setFkVariablegroupdef(r.get(V_REPORT.FK_VARIABLEGROUP));
				LocalDateTime lc = r.get(V_REPORT.LASTCHANGE);
				Timestamp lastchange = lc == null ? null : Timestamp.valueOf(lc);
				bean.setLastchange(lastchange);

				SelectConditionStep<Record2<Integer, String>> sql2 = jooq
				// @formatter:off
					.select(T_RELEASE.PK,
							    T_RELEASE.NAME)
					.from(T_REPORTDATARELEASE)
					.leftJoin(T_RELEASE).on(T_RELEASE.PK.eq(T_REPORTDATARELEASE.FK_RELEASE))
					.where(T_REPORTDATARELEASE.FK_REPORT.eq(reportId));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				for (Record r2 : sql2.fetch()) {
					ReleaseBean b = new ReleaseBean(r2.get(T_RELEASE.PK));
					b.setName(r2.get(T_RELEASE.NAME));
					bean.getReleases().add(b);
				}

				SelectConditionStep<Record3<Integer, LocalDateTime, LocalDateTime>> sql3 = jooq
				// @formatter:off
					.select(T_REPORTPERIOD.PK,
									T_REPORTPERIOD.DATE_FROM,
									T_REPORTPERIOD.DATE_UNTIL)
					.from(T_REPORTPERIOD)
					.where(T_REPORTPERIOD.FK_REPORT.eq(reportId));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				for (Record r3 : sql3.fetch()) {
					ReportPeriodBean b = new ReportPeriodBean(r3.get(T_REPORTPERIOD.PK));
					b.setDateFrom(ldt2date(r3.get(T_REPORTPERIOD.DATE_FROM)));
					b.setDateUntil(ldt2date(r3.get(T_REPORTPERIOD.DATE_UNTIL)));
					bean.getIntervals().add(b);
				}

				String variablegroupdefName = r.get(T_VARIABLEGROUP.NAME);
				String functinonlistName = r.get(T_FUNCTIONLIST.NAME);
				return new NamedReportBean(bean, functinonlistName, variablegroupdefName, null);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * create a new report
	 * 
	 * @param reportName name of the new report
	 * @param description description of the new report
	 * @param fkFunctionlist reference to the function selection
	 * @param fkVariablegroupdef reference to the variable group that is used
	 * @return new ID of the report (primary key from the database)
	 */
	public Integer createReport(String reportName, String description, Integer fkFunctionlist,
			Integer fkVariablegroupdef) {
		LambdaResultWrapper resultWrapper = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {

				InsertResultStep<TReportRecord> sql = DSL.using(c)
				// @formatter:off
					.insertInto(T_REPORT, 
											T_REPORT.NAME, 
											T_REPORT.DESCRIPTION, 
											T_REPORT.FK_VARIABLEGROUP, 
											T_REPORT.FK_FUNCTIONLIST)
					.values(reportName, description, fkVariablegroupdef, fkFunctionlist)
					.returning(T_REPORT.PK);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				resultWrapper.setInteger(sql.fetchOne().getPk());

				InsertOnDuplicateStep<TMatrixelementvalueRecord> sql2 = DSL.using(c)
				// @formatter:off
          .insertInto(T_MATRIXELEMENTVALUE,
                      T_MATRIXELEMENTVALUE.FK_REPORT,
                      T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN,
                      T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT)
          .select(DSL.using(c)
            .select(R_MATRIXCANDIDATE.FK_REPORT, R_MATRIXCANDIDATE.FK_MATRIXCOLUMN, R_MATRIXCANDIDATE.FK_VARIABLEGROUPELEMENT)
            .from(R_MATRIXCANDIDATE)
            .where(R_MATRIXCANDIDATE.FK_REPORT.eq(resultWrapper.getInteger()))
            .and(DSL.coalesce(R_MATRIXCANDIDATE.EXCLUDED, DSL.val(true)).eq(true))
          );
        // @formatter:on
				LOGGER.debug("{}", sql2.toString());
				sql2.execute();
			});
			return resultWrapper.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * create a report template
	 * 
	 * @param reportName name of the new template
	 * @param analysisTemplate reference to analysis template
	 * @param description description of the new template
	 * @return the primary key of this new template
	 * 
	 * if anything went wrong on database side
	 */
	public Integer createReportTemplate(String reportName, Integer analysisTemplate, String description) {
		try (InsertResultStep<TReportRecord> sql = getJooq()
		// @formatter:off
			.insertInto(T_REPORT, 
									T_REPORT.NAME, 
									T_REPORT.DESCRIPTION,
									T_REPORT.FK_FUNCTIONLIST)
			.values(reportName, description, analysisTemplate)
			.returning(T_REPORT.PK);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.fetchOne().getPk();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * remove report from db. This includes removal of analysis matrix, snipplets
	 * and calculations
	 * 
	 * @param reportId id of the report to be removed
	 * @return number of affected database rows
	 */
	public Integer removeReport(Integer reportId) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				DeleteConditionStep<TCalculationresultRecord> sql = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_CALCULATIONRESULT)
					.where(T_CALCULATIONRESULT.FK_REPORT.eq(reportId))
					.or(T_CALCULATIONRESULT.FK_MATRIXELEMENTVALUE.in(DSL.using(c)
						.select(T_MATRIXELEMENTVALUE.PK)
						.from(T_MATRIXELEMENTVALUE)
						.where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(reportId))));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				DeleteConditionStep<TSnippletRecord> sql1 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_SNIPPLET)
					.where(T_SNIPPLET.FK_REPORT.eq(reportId));
				// @formatter:on
				LOGGER.debug("{}", sql1.toString());
				lrw.addToInteger(sql1.execute());

				DeleteConditionStep<TMatrixelementparameterRecord> sql2 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTPARAMETER)
					.where(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE.in(DSL.using(c)
						.select(T_MATRIXELEMENTVALUE.PK)
						.from(T_MATRIXELEMENTVALUE)
						.where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(reportId))));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());

				DeleteConditionStep<TMatrixelementvalueRecord> sql3 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTVALUE)
					.where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(reportId));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				lrw.addToInteger(sql3.execute());

				DeleteConditionStep<TReportdatareleaseRecord> sql4 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_REPORTDATARELEASE)
					.where(T_REPORTDATARELEASE.FK_REPORT.eq(reportId));
				// @formatter:on
				LOGGER.debug("{}", sql4.toString());
				lrw.addToInteger(sql4.execute());

				DeleteConditionStep<TReportperiodRecord> sql5 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_REPORTPERIOD)
					.where(T_REPORTPERIOD.FK_REPORT.eq(reportId));
				// @formatter:on
				LOGGER.debug("{}", sql5.toString());
				lrw.addToInteger(sql5.execute());

				DeleteConditionStep<TCalcperfRecord> sql6 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_CALCPERF)
					.where(T_CALCPERF.FK_REPORT.eq(reportId));
				// @formatter:on
				LOGGER.debug("{}", sql6.toString());
				lrw.addToInteger(sql6.execute());

				DeleteConditionStep<TMatrixcolumnparameterRecord> sql7 = DSL.using(c)
				// @formatter:off
          .deleteFrom(T_MATRIXCOLUMNPARAMETER)
          .where(T_MATRIXCOLUMNPARAMETER.FK_REPORT.eq(reportId));
        // @formatter:on
				LOGGER.debug("{}", sql7.toString());
				lrw.addToInteger(sql7.execute());

				DeleteConditionStep<TReportoutputRecord> sql8 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_REPORTOUTPUT)
					.where(T_REPORTOUTPUT.FK_REPORT.eq(reportId));
				// @formatter:on
				LOGGER.debug("{}", sql8.toString());
				lrw.addToInteger(sql8.execute());

				DeleteConditionStep<TReportdocumentRecord> sql9 = DSL.using(c)
				// @formatter:off
				  .deleteFrom(T_REPORTDOCUMENT)
				  .where(T_REPORTDOCUMENT.FK_REPORT.eq(reportId));
				// @formatter:on
				LOGGER.debug("{}", sql9.toString());
				lrw.addToInteger(sql9.execute());

				DeleteConditionStep<TReportRecord> sql10 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_REPORT)
					.where(T_REPORT.PK.eq(reportId));
				// @formatter:on
				LOGGER.debug("{}", sql10.toString());
				lrw.addToInteger(sql10.execute());
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all reports with its snipplets; if a snipplet is a reference to an
	 * analysis function, replace it by its results
	 * 
	 * @param reportId the id of the report
	 * @return the report bean
	 * 
	 * if anything went wrong on database side
	 */
	public ReportDesignBean getReportWithResults(Integer reportId) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record9<Integer, Integer, Integer, String, String, String, String, Integer, Boolean>> sql = jooq
			// @formatter:off
				.select(T_VARIABLEGROUPELEMENT.FK_ELEMENT, 
								T_CALCULATIONRESULT.FK_MATRIXCOLUMN, 
								T_CALCULATIONRESULT.FK_FUNCTIONOUTPUT,
								T_CALCULATIONRESULT.RESULT,
								T_CALCULATIONRESULT.RESULT_ANNOTATION,
								T_FUNCTIONOUTPUTTYPE.LATEX_BEFORE, 
								T_FUNCTIONOUTPUTTYPE.LATEX_AFTER, 
								T_VARIABLEGROUPELEMENT.VARORDER,
								T_FUNCTION.VARLIST)
				.from(T_CALCULATIONRESULT)
				.leftJoin(T_MATRIXELEMENTVALUE).on(T_MATRIXELEMENTVALUE.PK.eq(T_CALCULATIONRESULT.FK_MATRIXELEMENTVALUE))
				.leftJoin(T_REPORT).on(T_REPORT.PK.eq(T_MATRIXELEMENTVALUE.FK_REPORT))
				.leftJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.PK.eq(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT).and(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(T_REPORT.FK_VARIABLEGROUP)))
				.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_CALCULATIONRESULT.FK_MATRIXCOLUMN))
				.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
				.leftJoin(T_FUNCTIONOUTPUT).on(T_FUNCTIONOUTPUT.FK_FUNCTION.eq(T_MATRIXCOLUMN.FK_FUNCTION)).and(T_FUNCTIONOUTPUT.PK.eq(T_CALCULATIONRESULT.FK_FUNCTIONOUTPUT))
				.leftJoin(T_FUNCTIONOUTPUTTYPE).on(T_FUNCTIONOUTPUTTYPE.PK.eq(T_FUNCTIONOUTPUT.TYPE))
				.where(T_CALCULATIONRESULT.FK_REPORT.eq(reportId));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Map<String, Map<Integer, CodelineBean>> calcRes = new HashMap<>();
			for (Record r : sql.fetch()) {
				Integer fId = r.get(T_CALCULATIONRESULT.FK_MATRIXCOLUMN);
				Integer foId = r.get(T_CALCULATIONRESULT.FK_FUNCTIONOUTPUT);
				Integer fkElement = r.get(T_VARIABLEGROUPELEMENT.FK_ELEMENT); // can be null for functions that do not serve on
																																			// single variables
				Boolean varlist = r.get(T_FUNCTION.VARLIST); // if this is meant for variable references, varlist must be
				// false; null and true is meant for non variable referenced
				// results
				if (varlist == null || varlist) {
					fkElement = 0; // does not matter as we do never look up the variable reference on calcRes
				}
				if (fId == null) {
					LOGGER.warn("anaparfun is null in result");
				}
				String sId = new StringBuilder().append(fId).append(":").append(foId).toString();

				Map<Integer, CodelineBean> innerMap = calcRes.get(sId);
				if (innerMap == null) {
					innerMap = new HashMap<>();
					calcRes.put(sId, innerMap);
				}
				CodelineBean codelineBean = new CodelineBean(r.get(T_VARIABLEGROUPELEMENT.VARORDER),
						r.get(T_CALCULATIONRESULT.RESULT), r.get(T_FUNCTIONOUTPUTTYPE.LATEX_BEFORE),
						r.get(T_FUNCTIONOUTPUTTYPE.LATEX_AFTER), r.get(T_CALCULATIONRESULT.RESULT_ANNOTATION));
				innerMap.put(fkElement, codelineBean);
			}

			SelectConditionStep<Record9<Integer, String, String, Integer, Integer, Integer, LocalDateTime, EnumAccesslevel, Integer>> sql2 = jooq
			// @formatter:off
				.select(V_REPORT.PK,
								V_REPORT.NAME, 
								V_REPORT.DESCRIPTION, 
								V_REPORT.FK_PARENT,
								V_REPORT.FK_FUNCTIONLIST,
								V_REPORT.FK_VARIABLEGROUP,
								V_REPORT.LASTCHANGE,
								V_PRIVILEGE.ACL,
								T_VARIABLEGROUP.FK_PRIVILEGE)
				.from(V_REPORT)
				.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(V_REPORT.FK_VARIABLEGROUP))
				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE)).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()))
				.where(V_REPORT.PK.eq(reportId));
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());
			ReportDesignBean bean = null;
			for (Record r : sql2.fetch()) {
				if (bean == null) {
					Integer pagelength = getFacesContext().getProfile().getConfigInteger("config.template.paginator.pagelength",
							20);
					LocalDateTime lc = r.get(V_REPORT.LASTCHANGE);
					Timestamp lastchange = lc == null ? null : Timestamp.valueOf(lc);
					bean = new ReportDesignBean(new AclBean(r.get(V_PRIVILEGE.ACL)), r.get(T_VARIABLEGROUP.FK_PRIVILEGE),
							pagelength, lastchange, 0);
					bean.setId(r.get(V_REPORT.PK));
					bean.setName(r.get(V_REPORT.NAME));
					bean.setDescription(r.get(V_REPORT.DESCRIPTION));
					bean.setParent(r.get(V_REPORT.FK_PARENT));
					bean.setFkFunctionlist(r.get(V_REPORT.FK_FUNCTIONLIST));
					bean.setVariablegroup(r.get(V_REPORT.FK_VARIABLEGROUP));
				}
			}
			if (bean == null) {
				// there are no reports
				return null;
			} else if (bean.getId() == null) {
				// there are no snipplets in this report - maybe it has been created right now?
				return bean;
			}
			SelectSeekStep1<Record15<Integer, String, Integer, Integer, Integer, String, String, Integer, Integer, String, String, String, String, Integer, String>, Integer> sql3 = jooq
			// @formatter:off
				.select(V_REPORTSNIPPLET.REPORT,
								V_REPORTSNIPPLET.NAME, 
								V_REPORTSNIPPLET.ORDER_NR,
								V_REPORTSNIPPLET.SNIPPLET_ID, 
								V_REPORTSNIPPLET.SNIPPLETTYPE_ID, 
								V_REPORTSNIPPLET.TYPE,
								V_REPORTSNIPPLET.MARKDOWNTEXT,
								V_REPORTSNIPPLET.FK_FUNCTIONOUTPUT,
								V_REPORTSNIPPLET.FK_MATRIXCOLUMN,
								V_REPORTSNIPPLET.TEMPLATE,
	              T_MATRIXCOLUMN.NAME,
	              T_MATRIXCOLUMN.DESCRIPTION,
	              T_FUNCTION.VIGNETTE,
	              T_SNIPPLETTYPE.ORDER_NR,
								T_HTMLCOMPONENT.NAME)
				.from(V_REPORTSNIPPLET)
				.leftJoin(T_SNIPPLETTYPE).on(T_SNIPPLETTYPE.PK.eq(V_REPORTSNIPPLET.SNIPPLETTYPE_ID))
				.leftJoin(T_HTMLCOMPONENT).on(T_HTMLCOMPONENT.PK.eq(T_SNIPPLETTYPE.FK_HTMLCOMPONENT))
	      .leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(V_REPORTSNIPPLET.FK_MATRIXCOLUMN))
	      .leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
				.where(V_REPORTSNIPPLET.REPORT.eq(bean.getId()))
				.orderBy(V_REPORTSNIPPLET.ORDER_NR);
			// @formatter:on
			LOGGER.debug("{}", sql3.toString());
			Map<Integer, SnippletBean> tmp = new LinkedHashMap<>();
			for (Record r : sql3.fetch()) {
				Integer id = r.get(V_REPORTSNIPPLET.SNIPPLET_ID);
				SnippletBean snip = tmp.get(id);
				if (snip == null) {
					Integer fkMatrixcolumn = r.get(V_REPORTSNIPPLET.FK_MATRIXCOLUMN);
					String name = r.get(T_MATRIXCOLUMN.NAME);
					String description = r.get(T_MATRIXCOLUMN.DESCRIPTION);
					String vignette = r.get(T_FUNCTION.VIGNETTE);
					MatrixcolumnParameterBean mcBean = new MatrixcolumnParameterBean(fkMatrixcolumn, name, description, vignette);
					snip = new SnippletBean(id, r.get(V_REPORTSNIPPLET.ORDER_NR), r.get(V_REPORTSNIPPLET.MARKDOWNTEXT),
							r.get(V_REPORTSNIPPLET.FK_FUNCTIONOUTPUT), mcBean);
					Integer typePk = r.get(V_REPORTSNIPPLET.SNIPPLETTYPE_ID);
					Integer stOrder = r.get(T_SNIPPLETTYPE.ORDER_NR);
					String type = r.get(V_REPORTSNIPPLET.TYPE);
					String templ = r.get(V_REPORTSNIPPLET.TEMPLATE);
					String htmlComponent = r.get(T_HTMLCOMPONENT.NAME);
					snip.setType(new SnipplettypeBean(typePk, stOrder, type, templ, htmlComponent));
					tmp.put(id, snip);
				}
			}
			for (Map.Entry<Integer, SnippletBean> entry : tmp.entrySet()) {
				bean.getSnipplets().add(entry.getValue());
			}

			// replace all report result snipplet definitions by the real calculated values
			for (SnippletBean b : bean.getSnipplets()) {
				if (b.getType().getSnipplettype().equals(SnipplettypeBean.REXPORT)
						|| b.getType().getSnipplettype().equals(SnipplettypeBean.BASE64)
						|| b.getType().getSnipplettype().equals(SnipplettypeBean.GRAPH2X2)) {
					String oldCode = new StringBuilder("").append(b.getFkFunctionoutput()).append(":")
							.append(b.getMatrixcolumnBean().getFkMatrixcolumn()).toString();
					Map<Integer, CodelineBean> valuesMap = calcRes.get(oldCode);
					if (valuesMap == null) {
						StringBuilder error = new StringBuilder("");
						error.append("Could not replace ").append(tryToTranslateResultCodes(oldCode))
								.append(" by calculation results");
						b.setMarkdowntext(error.toString(), false);
					} else {
						b.getCodeLines().addAll(valuesMap.values());
						b.getCodeLines().sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
						b.setMarkdowntext(null, false); // ensure markdowntext is null; this is the check for a decision to use
																						// codeLines
					}
				} else if (SnipplettypeBean.FIXEDTEXT.equals(b.getType().getSnipplettype())) {
					b.setEscape(false);
				} else if (SnipplettypeBean.TEXT.equals(b.getType().getSnipplettype())) {
					b.setEscape(false);
				}
			}

			// ensure sorting order
			bean.getSnipplets().sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return bean;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * try to translate XXX&colon;YYY style result codes by interpreting XXX as
	 * anaparfun pk and YYY as functionputput pk
	 * 
	 * @param code the code
	 * @return the translation or the code
	 * 
	 * if anything went wrong on database side
	 */
	private String tryToTranslateResultCodes(String code) {
		try (CloseableDSLContext jooq = getJooq()) {
			if (anaparfunMap == null) {
				anaparfunMap = new HashMap<>();
				SelectOrderByStep<Record2<Integer, String>> sql = jooq
				// @formatter:off
					.select(T_MATRIXCOLUMN.PK, 
									T_MATRIXCOLUMN.NAME)
					.from(T_MATRIXCOLUMN);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				for (Record r : sql.fetch()) {
					anaparfunMap.put(r.get(T_MATRIXCOLUMN.PK), r.get(T_MATRIXCOLUMN.NAME));
				}
			}
			if (functionoutputMap == null) {
				functionoutputMap = new HashMap<>();
				SelectJoinStep<Record2<Integer, String>> sql2 = jooq
				// @formatter:off
					.select(T_FUNCTIONOUTPUT.PK, 
									T_FUNCTIONOUTPUT.NAME)
					.from(T_FUNCTIONOUTPUT);
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				for (Record r : sql2.fetch()) {
					functionoutputMap.put(r.get(T_FUNCTIONOUTPUT.PK), r.get(T_FUNCTIONOUTPUT.NAME));
				}
			}
			if (code != null && code.matches("\\d+:\\d+")) {
				String[] parts = code.split(":");
				Integer anaparfun = Integer.valueOf(parts[0]);
				Integer functionoutput = Integer.valueOf(parts[1]);
				return new StringBuilder().append(anaparfunMap.get(anaparfun)).append(":")
						.append(functionoutputMap.get(functionoutput)).toString();
			} else {
				return code;
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update report
	 * 
	 * @param bean the report bean to be updated
	 * @param definitionOnly if true, do updates on name and description, if false,
	 * do updates on the snipplets also
	 * @return the number of database lines
	 * 
	 * if anything went wrong on database side
	 */
	public Integer updateReport(ReportDesignBean bean, Boolean definitionOnly) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				UpdateConditionStep<TReportRecord> sql = DSL.using(t)
				// @formatter:off
			  	.update(T_REPORT)
			  	.set(T_REPORT.NAME, bean.getName())
			  	.set(T_REPORT.DESCRIPTION, bean.getDescription())
			  	.set(T_REPORT.FK_PARENT, bean.getParent())
			  	.set(T_REPORT.FK_FUNCTIONLIST, bean.getFkFunctionlist())
			  	.set(T_REPORT.FK_VARIABLEGROUP, bean.getVariablegroup())
			  	.where(T_REPORT.PK.eq(bean.getId()))
			  	.and(T_REPORT.NAME.ne(bean.getName()) // do updates on changes only
			  	  .or(T_REPORT.DESCRIPTION.ne(bean.getDescription()))
			  	  .or(T_REPORT.FK_PARENT.ne(bean.getParent()))
			  	  .or(T_REPORT.FK_FUNCTIONLIST.ne(bean.getFkFunctionlist()))
			  	  .or(T_REPORT.FK_VARIABLEGROUP.ne(bean.getVariablegroup())));
			  // @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());
				if (!definitionOnly) {
					List<Integer> snippletPks = new ArrayList<>();
					for (SnippletBean s : bean.getSnipplets()) {
						if (s.getId() == null) {
							InsertResultStep<TSnippletRecord> sql2 = DSL.using(t)
							// @formatter:off
								.insertInto(T_SNIPPLET,
										        T_SNIPPLET.MARKDOWNTEXT,
										        T_SNIPPLET.FK_FUNCTIONOUTPUT,
										        T_SNIPPLET.FK_MATRIXCOLUMN,
										        T_SNIPPLET.FK_SNIPPLETTYPE,
										        T_SNIPPLET.ORDER_NR,
										        T_SNIPPLET.FK_REPORT)
								.values(s.getMarkdowntext(), s.getFkFunctionoutput(), s.getMatrixcolumnBean().getFkMatrixcolumn(), s.getType().getId(), s.getOrderNr(), bean.getId())
								.returning(T_SNIPPLET.PK);
							// @formatter:on
							LOGGER.debug("{}", sql2.toString());
							lrw.addToInteger(1);
							snippletPks.add(sql2.fetchOne().get(T_SNIPPLET.PK));
						} else {
							UpdateConditionStep<TSnippletRecord> sql2 = DSL.using(t)
							// @formatter:off
				      	.update(T_SNIPPLET)
				      	.set(T_SNIPPLET.MARKDOWNTEXT, s.getMarkdowntext())
				      	.set(T_SNIPPLET.FK_FUNCTIONOUTPUT, s.getFkFunctionoutput())
				      	.set(T_SNIPPLET.FK_MATRIXCOLUMN, s.getMatrixcolumnBean().getFkMatrixcolumn())
				      	.set(T_SNIPPLET.FK_SNIPPLETTYPE, s.getType().getId())
				      	.set(T_SNIPPLET.ORDER_NR, s.getOrderNr())
				      	.where(T_SNIPPLET.PK.eq(s.getId()))
				      	.and(T_SNIPPLET.MARKDOWNTEXT.ne(s.getMarkdowntext()) // do updates on changes only
				      	  .or(T_SNIPPLET.FK_FUNCTIONOUTPUT.ne(s.getFkFunctionoutput()))
				      	  .or(T_SNIPPLET.FK_MATRIXCOLUMN.ne(s.getMatrixcolumnBean().getFkMatrixcolumn()))
				          .or(T_SNIPPLET.FK_SNIPPLETTYPE.ne(s.getType().getId()))
				      	  .or(T_SNIPPLET.ORDER_NR.ne(s.getOrderNr())));
				      // @formatter:on
							LOGGER.debug("{}", sql2.toString());
							lrw.addToInteger(sql2.execute());
							snippletPks.add(s.getId());
						}
					}
					DeleteConditionStep<TSnippletRecord> sql2 = DSL.using(t)
					// @formatter:off
						.deleteFrom(T_SNIPPLET)
						.where(T_SNIPPLET.FK_REPORT.eq(bean.getId()))
						.and(T_SNIPPLET.PK.notIn(snippletPks));
					// @formatter:on
					LOGGER.debug("{}", sql2.toString());
					lrw.addToInteger(sql2.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all report names of reports that use template with fkFunctionlist
	 * 
	 * @param fkFunctionlist the id of the functionlist
	 * @return list of found report names
	 * 
	 * if anything went wrong on database side
	 */
	public List<String> getAllReportNamesOfTemplate(Integer fkFunctionlist) {
		try (SelectConditionStep<Record1<String>> sql = getJooq()
		// @formatter:off
			.select(T_REPORT.NAME)
			.from(T_REPORT)
			.where(T_REPORT.FK_FUNCTIONLIST.eq(fkFunctionlist));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<String> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				list.add(r.get(T_REPORT.NAME));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all report names of reports that use template with report
	 * 
	 * @param report the id of the report
	 * @return list of found report names
	 * 
	 * if anything went wrong on database side
	 */
	public List<String> getAllReportNamesOfReportTemplate(Integer report) {
		try (SelectConditionStep<Record1<String>> sql = getJooq()
		// @formatter:off
			.select(T_REPORT.NAME)
			.from(T_REPORT)
			.where(T_REPORT.FK_PARENT.eq(report));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<String> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				list.add(r.get(T_REPORT.NAME));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all unused function from this analysis template (unused is not used in
	 * analysis matrix for one variable)
	 * 
	 * @param reportId to be used as reference
	 * @return name of functions that are not used
	 * 
	 * if anything went wrong on database side
	 */
	public List<String> checkUnusedFunctions(Integer reportId) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record2<Integer[], Integer[]>> sql = jooq
			// @formatter:off
				.select(V_ORPHANED_FUNCTIONS.NEEDED_FUNCTION, 
								V_ORPHANED_FUNCTIONS.USED_FUNCTION)
				.from(T_REPORT)
				.leftJoin(V_ORPHANED_FUNCTIONS).on(V_ORPHANED_FUNCTIONS.PK.eq(T_REPORT.PK))
				.where(T_REPORT.PK.eq(reportId));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Set<Integer> orphanedIds = new HashSet<>();
			for (Record r : sql.fetch()) {
				Integer[] needed = r.get(V_ORPHANED_FUNCTIONS.NEEDED_FUNCTION);
				Integer[] used = r.get(V_ORPHANED_FUNCTIONS.USED_FUNCTION);
				Set<Integer> nSet = new HashSet<>();
				if (needed != null) {
					nSet.addAll(Arrays.asList(needed));
				}
				Set<Integer> uSet = new HashSet<>();
				if (used != null) {
					uSet.addAll(Arrays.asList(used));
				}
				nSet.removeAll(uSet);
				orphanedIds = nSet;
			}
			List<String> list = new ArrayList<>();
			if (orphanedIds.size() > 0) {
				SelectConditionStep<Record1<String>> sql2 = jooq
				// @formatter:off
					.select(T_FUNCTION.NAME)
					.from(T_FUNCTION)
					.where(T_FUNCTION.PK.in(orphanedIds));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				for (Record r : sql2.fetch()) {
					list.add(r.get(T_FUNCTION.NAME));
				}
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * copy report template
	 * 
	 * @param report the report bean
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer copyReportTemplate(ReportDesignBean report) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				String newNameAsString = report.getName().concat("_copy");
				Field<String> newName = DSL.val(newNameAsString);
				InsertResultStep<TReportRecord> sql = DSL.using(t)
				// @formatter:off
					.insertInto(T_REPORT,
											T_REPORT.NAME, 
											T_REPORT.DESCRIPTION,
											T_REPORT.FK_FUNCTIONLIST,
											T_REPORT.FK_LATEXDEF, 
											T_REPORT.FK_PARENT, 
											T_REPORT.FK_VARIABLEGROUP)
					.select(DSL.using(t)
						.select(newName, 
										T_REPORT.DESCRIPTION, 
										T_REPORT.FK_FUNCTIONLIST,
										T_REPORT.FK_LATEXDEF,
										T_REPORT.FK_PARENT,
										T_REPORT.FK_VARIABLEGROUP)
						.from(T_REPORT)
						.where(T_REPORT.PK.eq(report.getId())))
					.returning(T_REPORT.PK);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				Integer pk = sql.fetchOne().getPk();
				lrw.addToInteger(1); // because of the where condition, this can be only one line; none would cause a
															// NPE in the line above
				Field<Integer> newPk = DSL.val(pk);
				InsertOnDuplicateStep<TSnippletRecord> sql2 = DSL.using(t)
				// @formatter:off
					.insertInto(T_SNIPPLET,
											T_SNIPPLET.CODE,
											T_SNIPPLET.FK_REPORT,
											T_SNIPPLET.FK_SNIPPLETTYPE,
											T_SNIPPLET.ORDER_NR,
											T_SNIPPLET.FK_FUNCTIONOUTPUT,
											T_SNIPPLET.FK_MATRIXCOLUMN,
											T_SNIPPLET.MARKDOWNTEXT)
					.select(DSL.using(t)
						.select(T_SNIPPLET.CODE,
										newPk,
										T_SNIPPLET.FK_SNIPPLETTYPE,
										T_SNIPPLET.ORDER_NR,
										T_SNIPPLET.FK_FUNCTIONOUTPUT,
										T_SNIPPLET.FK_MATRIXCOLUMN,
										T_SNIPPLET.MARKDOWNTEXT)
						.from(T_SNIPPLET)
						.where(T_SNIPPLET.FK_REPORT.eq(report.getId())));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());
				report.setId(pk); // for further work outside of this function with that bean
				report.setName(newNameAsString);
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * clone a report and return the id of the clone
	 * 
	 * @param bean the report to be cloned
	 * @param newReportName name of cloned report
	 * @param asTemplate if true, remove the parent reference
	 * @return the id of the cloned one
	 */
	public Integer cloneReport(ReportBean bean, String newReportName, Boolean asTemplate) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				SelectConditionStep<Record1<String>> sql = DSL.using(c)
				// @formatter:off
					.select(T_REPORT.NAME)
					.from(T_REPORT)
					.where(T_REPORT.NAME.eq(newReportName));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				Record rec = sql.fetchOne();
				String validNewReportName = newReportName;
				if (rec != null) {
					validNewReportName = newReportName + "_2";
				}

				InsertResultStep<TReportRecord> sql2 = DSL.using(c)
				// @formatter:off
					.insertInto(T_REPORT, 
											T_REPORT.NAME, 
											T_REPORT.DESCRIPTION, 
											T_REPORT.FK_FUNCTIONLIST,
											T_REPORT.FK_LATEXDEF, 
											T_REPORT.FK_PARENT,
											T_REPORT.FK_VARIABLEGROUP)
					.select(DSL.using(c)
						.select(DSL.val(validNewReportName, String.class), 
										T_REPORT.DESCRIPTION,
										T_REPORT.FK_FUNCTIONLIST,
										T_REPORT.FK_LATEXDEF,
										DSL.val(asTemplate ? null : bean.getPk(), Integer.class),
										T_REPORT.FK_VARIABLEGROUP)
						.from(T_REPORT)
						.where(T_REPORT.PK.eq(bean.getPk())))
					.returning(T_REPORT.PK);
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				Integer newPk = sql2.fetchOne().getPk();
				lrw.setInteger(newPk);

				InsertOnDuplicateStep<TSnippletRecord> sql3 = DSL.using(c)
				// @formatter:off
					.insertInto(T_SNIPPLET, 
											T_SNIPPLET.FK_SNIPPLETTYPE,
											T_SNIPPLET.CODE,
											T_SNIPPLET.ORDER_NR,
											T_SNIPPLET.FK_REPORT)
					.select(DSL.using(c)
						.select(T_SNIPPLET.FK_SNIPPLETTYPE, 
										T_SNIPPLET.CODE, 
										T_SNIPPLET.ORDER_NR, 
										DSL.val(newPk))
						.from(T_SNIPPLET)
						.where(T_SNIPPLET.FK_REPORT.eq(bean.getPk())));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				sql3.execute();

				if (!asTemplate) {
					Field<Integer> reportField = DSL.val(newPk, Integer.class);
					InsertOnDuplicateStep<TMatrixcolumnparameterRecord> sql4 = DSL.using(c)
					// @formatter:off
            .insertInto(T_MATRIXCOLUMNPARAMETER,
                        T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN,
                        T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT,
                        T_MATRIXCOLUMNPARAMETER.VALUE,
                        T_MATRIXCOLUMNPARAMETER.FIXED,
                        T_MATRIXCOLUMNPARAMETER.REFERS_METADATA,
                        T_MATRIXCOLUMNPARAMETER.FK_REPORT)
            .select(DSL.using(c)
              .select(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN,
                      T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT,
                      T_MATRIXCOLUMNPARAMETER.VALUE,
                      T_MATRIXCOLUMNPARAMETER.FIXED,
                      T_MATRIXCOLUMNPARAMETER.REFERS_METADATA,
                      reportField)
              .from(T_MATRIXCOLUMNPARAMETER)
              .where(T_MATRIXCOLUMNPARAMETER.FK_REPORT.eq(bean.getPk())));
          // @formatter:on
					LOGGER.debug("{}", sql4.toString());
					sql4.execute();
				}
			});
			return asTemplate ? null : lrw.getInteger(); // return null if this is a new template
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all reports that the user can access with its releases and intervals
	 * 
	 * @param usnr the id of the user that might use the reports
	 * @return a list of found reports, an empty one at least
	 */
	public List<ReportBean> getAllReportsOf(Integer usnr) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectOnConditionStep<Record3<Integer, String, Integer>> sql2 = jooq
			// @formatter:off
				.select(T_RELEASE.PK,
						    T_RELEASE.NAME,
						    T_REPORTDATARELEASE.FK_REPORT)
				.from(T_REPORTDATARELEASE)
				.leftJoin(T_RELEASE).on(T_RELEASE.PK.eq(T_REPORTDATARELEASE.FK_RELEASE));
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());
			Map<Integer, List<ReleaseBean>> releases = new HashMap<>();
			for (Record r : sql2.fetch()) {
				Integer fkReport = r.get(T_REPORTDATARELEASE.FK_REPORT);
				ReleaseBean bean = new ReleaseBean(r.get(T_RELEASE.PK));
				bean.setName(r.get(T_RELEASE.NAME));
				List<ReleaseBean> list = releases.get(fkReport);
				if (list == null) {
					list = new ArrayList<>();
					releases.put(fkReport, list);
				}
				list.add(bean);
			}

			SelectJoinStep<Record4<Integer, LocalDateTime, LocalDateTime, Integer>> sql3 = jooq
			// @formatter:off
				.select(T_REPORTPERIOD.PK,
								T_REPORTPERIOD.DATE_FROM,
								T_REPORTPERIOD.DATE_UNTIL,
								T_REPORTPERIOD.FK_REPORT)
				.from(T_REPORTPERIOD);
			// @formatter:on
			LOGGER.debug("{}", sql3.toString());
			Map<Integer, List<ReportPeriodBean>> intervals = new HashMap<>();
			for (Record r : sql3.fetch()) {
				Integer fkReport = r.get(T_REPORTPERIOD.FK_REPORT);
				ReportPeriodBean bean = new ReportPeriodBean(r.get(T_REPORTPERIOD.PK));
				bean.setDateFrom(ldt2date(r.get(T_REPORTPERIOD.DATE_FROM)));
				bean.setDateUntil(ldt2date(r.get(T_REPORTPERIOD.DATE_UNTIL)));
				List<ReportPeriodBean> list = intervals.get(fkReport);
				if (list == null) {
					list = new ArrayList<>();
					intervals.put(fkReport, list);
				}
				list.add(bean);
			}

			SelectConditionStep<Record12<Integer, String, String, Integer, Integer, LocalDateTime, String, String, String, LocalDateTime, LocalDateTime, String>> sql = jooq
			// @formatter:off
				.select(T_REPORT.PK,
						    T_REPORT.NAME,
						    T_REPORT.DESCRIPTION,
						    T_REPORT.FK_FUNCTIONLIST,
						    T_REPORT.FK_VARIABLEGROUP,
						    T_REPORT.LASTCHANGE,
						    T_REPORT.LATEXCODE,
						    T_REPORT.RUNLOG,
						    T_REPORT.RUNSUM,
						    T_REPORT.STARTED,
						    T_REPORT.STOPPED,
						    T_VARIABLEGROUP.NAME)
				.from(T_REPORT)
				.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(T_REPORT.FK_VARIABLEGROUP))
				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE))
				.where(V_PRIVILEGE.FK_USNR.eq(usnr));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			List<ReportBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_REPORT.PK);
				ReportBean bean = new ReportBean(pk);
				bean.setName(r.get(T_REPORT.NAME));
				bean.setDescription(r.get(T_REPORT.DESCRIPTION));
				bean.setFkFunctionlist(r.get(T_REPORT.FK_FUNCTIONLIST));
				bean.setFkVariablegroupdef(r.get(T_REPORT.FK_VARIABLEGROUP));
				LocalDateTime lc = r.get(T_REPORT.LASTCHANGE);
				Timestamp lastchange = lc == null ? null : Timestamp.valueOf(lc);
				bean.setLastchange(lastchange);
				bean.setLatexcode(r.get(T_REPORT.LATEXCODE));
				bean.setRunlog(r.get(T_REPORT.RUNLOG));
				bean.setRunsum(r.get(T_REPORT.RUNSUM));
				bean.setStarted(ldt2date(r.get(T_REPORT.STARTED)));
				bean.setStopped(ldt2date(r.get(T_REPORT.STOPPED)));
				bean.setVargroupname(r.get(T_VARIABLEGROUP.NAME));
				List<ReleaseBean> releaseBeans = releases.get(pk);
				if (releaseBeans != null) {
					bean.getReleases().addAll(releaseBeans);
				}
				List<ReportPeriodBean> intervalBeans = intervals.get(pk);
				if (intervalBeans != null) {
					bean.getIntervals().addAll(intervalBeans);
				}
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all report reference names like name of the report itself, the analysis
	 * template and the variablegroup
	 * 
	 * @param bean the report to refer to
	 * @return a map of found names; the map should have all the EnumShowNames keys
	 */
	public Map<EnumShowNames, String> getAllReportReferenceNames(ReportBean bean) {
		try (SelectConditionStep<Record3<String, String, String>> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTIONLIST.NAME, 
					    T_VARIABLEGROUP.NAME,
					    T_VARIABLEGROUP.DESCRIPTION)
			.from(T_FUNCTIONLIST)
			.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(bean.getFkVariablegroupdef()))
			.where(T_FUNCTIONLIST.PK.eq(bean.getFkFunctionlist()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Record r = sql.fetchOne();
			Map<EnumShowNames, String> map = new EnumMap<>(EnumShowNames.class);
			map.put(EnumShowNames.FUNCTIONLISTNAME, r.get(T_FUNCTIONLIST.NAME));
			StringBuilder buf = new StringBuilder();
			buf.append(r.get(T_VARIABLEGROUP.NAME));
			buf.append(" (");
			buf.append(r.get(T_VARIABLEGROUP.DESCRIPTION));
			buf.append(")");
			map.put(EnumShowNames.VARIABLEGROUPNAME, buf.toString());
			return map;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update the report bean
	 * 
	 * @param bean the report to be updated
	 * @return number of affected database rows
	 */
	public Integer updateReport(NamedReportBean bean) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				UpdateConditionStep<TReportRecord> sql = DSL.using(t)
				// @formatter:off
					.update(T_REPORT)
					.set(T_REPORT.NAME, bean.getName())
					.set(T_REPORT.DESCRIPTION, bean.getDescription())
					.set(T_REPORT.FK_PARENT, bean.getFkParent())
					.set(T_REPORT.FK_LATEXDEF, bean.getFkLatexdef())
					.set(T_REPORT.FK_FUNCTIONLIST, bean.getFkFunctionlist())
					.set(T_REPORT.FK_VARIABLEGROUP, bean.getFkVariablegroupdef())
					.where(T_REPORT.PK.eq(bean.getPk()))
					.and(T_REPORT.NAME.ne(bean.getName()) // do updates on changes only
					  .or(T_REPORT.DESCRIPTION.ne(bean.getDescription()))
					  .or(T_REPORT.FK_PARENT.ne(bean.getFkParent()))
					  .or(T_REPORT.FK_LATEXDEF.ne(bean.getFkLatexdef()))
					  .or(T_REPORT.FK_FUNCTIONLIST.ne(bean.getFkFunctionlist()))
					  .or(T_REPORT.FK_VARIABLEGROUP.ne(bean.getFkVariablegroupdef())));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				List<Integer> releaseIds = new ArrayList<>();
				for (ReleaseBean rBean : bean.getReleases()) {
					InsertReturningStep<TReportdatareleaseRecord> sql2 = DSL.using(t)
					// @formatter:off
						.insertInto(T_REPORTDATARELEASE,
												T_REPORTDATARELEASE.FK_REPORT,
												T_REPORTDATARELEASE.FK_RELEASE)
						.values(bean.getPk(), rBean.getPk())
						.onConflict(T_REPORTDATARELEASE.FK_REPORT, T_REPORTDATARELEASE.FK_RELEASE)
						.doNothing();
					// @formatter:on
					LOGGER.debug("{}", sql2.toString());
					lrw.addToInteger(sql2.execute());
					releaseIds.add(rBean.getPk());
				}
				DeleteConditionStep<TReportdatareleaseRecord> sql3 = DSL.using(t)
				// @formatter:off
					.deleteFrom(T_REPORTDATARELEASE)
					.where(T_REPORTDATARELEASE.FK_REPORT.eq(bean.getPk()))
					.and(T_REPORTDATARELEASE.FK_RELEASE.notIn(releaseIds));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				lrw.addToInteger(sql3.execute());

				// TODO: find a smart way to delete only the ones that are not in
				// bean.getIntervals()
				DeleteConditionStep<TReportperiodRecord> sql4 = DSL.using(t)
				// @formatter:off
					.deleteFrom(T_REPORTPERIOD)
					.where(T_REPORTPERIOD.FK_REPORT.eq(bean.getPk()));
				// @formatter:on
				LOGGER.debug("{}", sql4.toString());
				lrw.addToInteger(sql4.execute());
				for (ReportPeriodBean cBean : bean.getIntervals()) {
					Date fromDate = cBean.getDateFrom();
					Date untilDate = cBean.getDateUntil();
					LocalDateTime dateFrom = fromDate == null ? null
							: fromDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
					LocalDateTime dateUntil = untilDate == null ? null
							: untilDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
					InsertReturningStep<TReportperiodRecord> sql5 = DSL.using(t)
					// @formatter:off
						.insertInto(T_REPORTPERIOD,
												T_REPORTPERIOD.DATE_FROM,
												T_REPORTPERIOD.DATE_UNTIL,
												T_REPORTPERIOD.FK_REPORT)
						.values(dateFrom, dateUntil, bean.getPk())
						.onConflict(T_REPORTPERIOD.FK_REPORT, T_REPORTPERIOD.DATE_FROM, T_REPORTPERIOD.DATE_UNTIL)
						.doNothing();
					// @formatter:on
					LOGGER.debug("{}", sql5.toString());
					lrw.addToInteger(sql5.execute());
				}

				for (SnippletBean sBean : bean.getSnipplets()) {
					UpdateConditionStep<TSnippletRecord> sql6 = DSL.using(t)
					// @formatter:off
						.update(T_SNIPPLET)
						.set(T_SNIPPLET.MARKDOWNTEXT, sBean.getMarkdowntext())
						.set(T_SNIPPLET.FK_FUNCTIONOUTPUT, sBean.getFkFunctionoutput())
						.set(T_SNIPPLET.FK_MATRIXCOLUMN, sBean.getMatrixcolumnBean().getFkMatrixcolumn())
						.where(T_SNIPPLET.PK.eq(sBean.getId()))
						.and(T_SNIPPLET.MARKDOWNTEXT.ne(sBean.getMarkdowntext())) // do updates on changes only
   					    .and(T_SNIPPLET.FK_FUNCTIONOUTPUT.ne(sBean.getFkFunctionoutput()))
					    .and(T_SNIPPLET.FK_MATRIXCOLUMN.ne(sBean.getMatrixcolumnBean().getFkMatrixcolumn()));
					// @formatter:on
					LOGGER.debug("{}", sql6.toString());
					lrw.addToInteger(sql6.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get performance information from the database
	 * 
	 * @param reportId the id of the corresponding report
	 * @return a list of performance beans; an empty list at least
	 */
	public List<PerformanceBean> getPerformance(Integer reportId) {
		try (
				SelectConditionStep<Record7<String, Integer, BigDecimal, BigDecimal, BigDecimal, BigDecimal, BigDecimal>> sql = getJooq()
				// @formatter:off
					.select(T_FUNCTION.NAME,
									T_CALCPERF.VAR_MAGNITUDE, 
									T_CALCPERF.USER_SELF, 
									T_CALCPERF.SYS_SELF,
									T_CALCPERF.ELAPSED,
									T_CALCPERF.USER_CHILD,
									T_CALCPERF.SYS_CHILD)
					.from(T_CALCPERF)
					.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_CALCPERF.FK_FUNCTION))
					.where(T_CALCPERF.FK_REPORT.eq(reportId))
					.and(T_CALCPERF.VAR_MAGNITUDE.isNotNull());
				// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<PerformanceBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				PerformanceBean bean = new PerformanceBean();
				bean.setFunctionname(r.get(T_FUNCTION.NAME));
				bean.setVarmagnitude(r.get(T_CALCPERF.VAR_MAGNITUDE));
				bean.setUserself(r.get(T_CALCPERF.USER_SELF));
				bean.setSysself(r.get(T_CALCPERF.SYS_SELF));
				bean.setElapsed(r.get(T_CALCPERF.ELAPSED));
				bean.setUserchild(r.get(T_CALCPERF.USER_CHILD));
				bean.setSyschild(r.get(T_CALCPERF.SYS_CHILD));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all calculation results from db
	 * 
	 * @param reportId the id of the report
	 * @return the list of found downloadable calculation results; an empty one at
	 * least
	 */
	public List<CalculationResultFileBean> getCalculationResults(Integer reportId) {
		try (SelectConditionStep<Record3<String, String, String>> sql = getJooq()
		// @formatter:off
			.select(T_CALCULATIONRESULT.RESULT,
							T_MATRIXCOLUMN.NAME,
						  T_FUNCTIONOUTPUT.NAME)
			.from(T_CALCULATIONRESULT)
			.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_CALCULATIONRESULT.FK_MATRIXCOLUMN))
			.leftJoin(T_FUNCTIONOUTPUT).on(T_FUNCTIONOUTPUT.PK.eq(T_CALCULATIONRESULT.FK_FUNCTIONOUTPUT))
			.where(T_CALCULATIONRESULT.RESULT_CONVERTER.cast(String.class).eq(EnumResultconverter.base64.getLiteral()))
			.and(T_CALCULATIONRESULT.FK_REPORT.eq(reportId));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<CalculationResultFileBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				CalculationResultFileBean bean = new CalculationResultFileBean();
				StringBuilder buf = new StringBuilder();
				buf.append(r.get(T_MATRIXCOLUMN.NAME));
				buf.append(".");
				buf.append(r.get(T_FUNCTIONOUTPUT.NAME));
				bean.setName(buf.toString());
				bean.setFileContent(r.get(T_CALCULATIONRESULT.RESULT));
				bean.setMimetype("application/x-r-data");
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * @param bean the report bean
	 * @return a list of all report snipplets with its results
	 */
	public List<SnippletBean> getAllSnipplets(ReportBean bean) {
		try (
				SelectConditionStep<Record13<Integer, Integer, Integer, String, Integer, Integer, String, String, String, Integer, Integer, String, String>> sql = getJooq()
				// @formatter:off
			.select(T_SNIPPLET.PK,
					T_SNIPPLET.FK_REPORT,
					T_SNIPPLET.ORDER_NR,
					T_SNIPPLET.MARKDOWNTEXT,
					T_SNIPPLET.FK_FUNCTIONOUTPUT,
					T_SNIPPLET.FK_MATRIXCOLUMN,
          T_MATRIXCOLUMN.NAME,
          T_MATRIXCOLUMN.DESCRIPTION,
          T_FUNCTION.VIGNETTE,
					T_SNIPPLETTYPE.PK,
					T_SNIPPLETTYPE.ORDER_NR,
					T_SNIPPLETTYPE.TYPE,
					T_SNIPPLETTYPE.TEMPLATE)
			.from(T_SNIPPLET)
			.leftJoin(T_SNIPPLETTYPE).on(T_SNIPPLETTYPE.PK.eq(T_SNIPPLET.FK_SNIPPLETTYPE))
			.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_SNIPPLET.FK_MATRIXCOLUMN))
			.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
			.where(T_SNIPPLET.FK_REPORT.eq(bean.getPk()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<SnippletBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer id = r.get(T_SNIPPLET.PK);
				Integer orderNr = r.get(T_SNIPPLET.ORDER_NR);
				String markdowntext = r.get(T_SNIPPLET.MARKDOWNTEXT);
				Integer fkFunctionoutput = r.get(T_SNIPPLET.FK_FUNCTIONOUTPUT);
				Integer fkMatrixcolumn = r.get(T_SNIPPLET.FK_MATRIXCOLUMN);
				Integer idType = r.get(T_SNIPPLETTYPE.PK);
				Integer stOrder = r.get(T_SNIPPLETTYPE.ORDER_NR);
				String snipplettype = r.get(T_SNIPPLETTYPE.TYPE);
				String snipplettemplate = r.get(T_SNIPPLETTYPE.TEMPLATE);
				String name = r.get(T_MATRIXCOLUMN.NAME);
				String description = r.get(T_MATRIXCOLUMN.DESCRIPTION);
				String vignette = r.get(T_FUNCTION.VIGNETTE);
				MatrixcolumnParameterBean mcBean = new MatrixcolumnParameterBean(fkMatrixcolumn, name, description, vignette);
				SnippletBean sBean = new SnippletBean(id, orderNr, markdowntext, fkFunctionoutput, mcBean);
				SnipplettypeBean tBean = new SnipplettypeBean(idType, stOrder, snipplettype, snipplettemplate, null);
				sBean.setType(tBean);
				list.add(sBean);
			}
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get started and stopped of report; may be removed as soon as v_report
	 * contains started and stopped
	 * 
	 * @param pk the id of the report
	 * @return an empty report bean with only started and stopped filled
	 */
	public ReportBean getReportTimes(Integer pk) {
		try (SelectConditionStep<Record2<LocalDateTime, LocalDateTime>> sql = getJooq()
		// @formatter:off
			.select(T_REPORT.STARTED,
              T_REPORT.STOPPED)
			.from(T_REPORT)
			.where(T_REPORT.PK.eq(pk));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			ReportBean bean = new ReportBean(pk);
			for (Record r : sql.fetch()) {
				bean.setStarted(ldt2date(r.get(T_REPORT.STARTED)));
				bean.setStopped(ldt2date(r.get(T_REPORT.STOPPED)));
			}
			return bean;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all report names from the database
	 * 
	 * @return list of found report names
	 */
	public Set<String> getAllReportNames() {
		try (SelectJoinStep<Record1<String>> sql = getJooq()
		// @formatter:off
			.select(T_REPORT.NAME)
			.from(T_REPORT);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Set<String> list = new HashSet<>();
			for (Record r : sql.fetch()) {
				list.add(r.get(T_REPORT.NAME));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * list all reports that depend on the one of fkParent
	 * 
	 * @param fkParent the id of the parent report
	 * @return list of children reports; an empty list at least
	 */
	public List<ReportBean> getAllReportsOfParent(Integer fkParent) {
		try (SelectConditionStep<Record2<Integer, String>> sql = getJooq()
		// @formatter:off
			.select(T_REPORT.PK,
					    T_REPORT.NAME)
			.from(T_REPORT)
			.where(T_REPORT.FK_PARENT.eq(fkParent));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<ReportBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_REPORT.PK);
				String name = r.get(T_REPORT.NAME);
				ReportBean bean = new ReportBean(pk);
				bean.setName(name);
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * reset the calculation
	 * 
	 * @param bean of the report
	 */
	public void resetCalculation(NamedReportBean bean) {
		try (UpdateConditionStep<TReportRecord> sql = getJooq()
		// @formatter:off
			.update(T_REPORT)
			.set(T_REPORT.STARTED, (LocalDateTime) null)
			.set(T_REPORT.STOPPED, (LocalDateTime) null)
			.where(T_REPORT.PK.eq(bean.getPk()))
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Integer affected = sql.execute();
			if (affected != 1) {
				throw new DataAccessException("affected data sets != 1, but" + affected);
			} else {
				bean.setStarted(null);
				bean.setStopped(null);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * @param pkReport the id of the report
	 * @param releases the releases to add
	 * @return the number of added entries; should be the number of releases
	 */
	public Integer addReleasesToReport(Integer pkReport, List<ReleaseBean> releases) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				for (ReleaseBean bean : releases) {
					InsertValuesStep2<TReportdatareleaseRecord, Integer, Integer> sql = DSL.using(t)
					// @formatter:off
            .insertInto(T_REPORTDATARELEASE,
                        T_REPORTDATARELEASE.FK_RELEASE,
                        T_REPORTDATARELEASE.FK_REPORT)
            .values(bean.getPk(), pkReport);
          // @formatter:on
					LOGGER.debug("{}", sql.toString());
					lrw.addToInteger(sql.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add report periods to the report
	 * 
	 * @param pkReport the id of the report
	 * @param intervals the report periods list
	 * @return the number of affected database rows, should be equal to the number
	 * of intervals
	 */
	public Integer addIntervalsToReport(Integer pkReport, List<ReportPeriodBean> intervals) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				for (ReportPeriodBean bean : intervals) {
					InsertValuesStep3<TReportperiodRecord, Integer, LocalDateTime, LocalDateTime> sql = DSL.using(t)
					// @formatter:off
            .insertInto(T_REPORTPERIOD,
                        T_REPORTPERIOD.FK_REPORT,
                        T_REPORTPERIOD.DATE_FROM,
                        T_REPORTPERIOD.DATE_UNTIL)
            .values(pkReport, bean.getLdtDateFrom(), bean.getLdtDateUntil());
          // @formatter:on
					LOGGER.debug("{}", sql.toString());
					lrw.addToInteger(sql.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get number of variables in this variablegroup
	 * 
	 * @param fkVariablegroup the id of the variable group
	 * @return the number of variables in this variable group
	 */
	public Integer getNumberOfTotalVariables(Integer fkVariablegroup) {
		try {
			SelectConditionStep<Record1<Integer>> sql = getJooq()
			// @formatter:off
        .selectCount()
        .from(T_VARIABLEGROUPELEMENT)
        .where(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(fkVariablegroup));
      // @formatter:on
			LOGGER.debug("{}", sql.toString());
			return sql.fetchOne(0, int.class);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			LOGGER.error(e.getMessage());
			throw new DataAccessException(e.getMessage());
		}
	}

	/**
	 * get number of selected variables
	 * 
	 * @param fkReport the report id
	 * @return the number of selected variables found in t_matrixelementvalue
	 */
	public Integer getNumberOfSelectedVariables(Integer fkReport) {
		try {
			SelectJoinStep<Record1<Integer>> sql = getJooq()
			// @formatter:off
        .with("x", "n")
        .as(getJooq()
          .select(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT)
          .from(T_MATRIXELEMENTVALUE)
          .where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(fkReport))
          .groupBy(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT))
        .selectCount()
        .from("x");
      // @formatter:on
			LOGGER.debug("{}", sql.toString());
			return sql.fetchOne(0, int.class);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			LOGGER.error(e.getMessage());
			throw new DataAccessException(e.getMessage());
		}
	}

	/**
	 * get all report properties where fkReport fits
	 * 
	 * @param fkReport the id of the report
	 * @return the map of found records; an empty maps at least
	 */
	public Map<String, ReportPropertyBean> getAllProperties(Integer fkReport) {
		try {
			SelectConditionStep<Record8<Integer, String, JSONB, Integer, JSONB, JSONB, Integer, String>> sql = getJooq()
			// @formatter:off
        .select(T_REPORTPROPERTY.PK,
                T_WIDGETBASED.KEY,
                T_REPORTPROPERTY.VALUE,
                T_WIDGETBASED.ORDER_NR,
                T_WIDGETBASED.TYPE_RESTRICTION,
                T_WIDGETBASED.VALUE,
                T_REPORTPROPERTY.FK_WIDGETBASED,
                T_FUNCTIONINPUTTYPE.NAME)
        .from(T_REPORTPROPERTY)
        .leftJoin(T_WIDGETBASED).on(T_WIDGETBASED.PK.eq(T_REPORTPROPERTY.FK_WIDGETBASED))
        .leftJoin(T_FUNCTIONINPUTTYPE).on(T_FUNCTIONINPUTTYPE.PK.eq(T_WIDGETBASED.FK_FUNCTIONINPUTTYPE))
        .where(T_REPORTPROPERTY.FK_REPORT.eq(fkReport));
      // @formatter:on
			LOGGER.debug("{}", sql.toString());
			Map<String, ReportPropertyBean> map = new HashMap<>();
			for (Record r : sql.fetch()) {
				JSONB value = r.get(T_REPORTPROPERTY.VALUE);
				JSONB defaultValue = r.get(T_WIDGETBASED.VALUE);
				JSONB typeRestriction = r.get(T_WIDGETBASED.TYPE_RESTRICTION);
				ReportPropertyBean bean = new ReportPropertyBean(r.get(T_REPORTPROPERTY.PK), r.get(T_WIDGETBASED.KEY));
				bean.setValue(value == null ? "" : value.data());
				bean.setDefaultValue(defaultValue == null ? "" : defaultValue.data());
				bean.setOrderNr(r.get(T_WIDGETBASED.ORDER_NR));
				bean.setTypeRestriction(typeRestriction == null ? "" : typeRestriction.data());
				bean.setFunctioninputtypeName(r.get(T_FUNCTIONINPUTTYPE.NAME));
				bean.setFkWidgetbased(r.get(T_REPORTPROPERTY.FK_WIDGETBASED));
				map.put(bean.getKey(), bean);
			}
			return map;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			LOGGER.error(e.getMessage());
			throw new DataAccessException(e.getMessage());
		}
	}

	/**
	 * get all widgetbased entries of the database
	 * 
	 * @return a list of all found widgetbased entries; an empty list at least
	 */
	public List<WidgetbasedBean> getAllWidgetbased() {
		try {
			SelectSeekStep1<Record4<Integer, String, Integer, String>, Integer> sql = getJooq()
			// @formatter:off
        .select(T_WIDGETBASED.PK,
                T_WIDGETBASED.KEY,
                T_WIDGETBASED.FK_FUNCTIONINPUTTYPE,
                T_FUNCTIONINPUTTYPE.NAME)
        .from(T_WIDGETBASED)
        .leftJoin(T_FUNCTIONINPUTTYPE).on(T_FUNCTIONINPUTTYPE.PK.eq(T_WIDGETBASED.FK_FUNCTIONINPUTTYPE))
        .orderBy(T_WIDGETBASED.ORDER_NR);
      // @formatter:on
			LOGGER.debug("{}", sql.toString());
			List<WidgetbasedBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_WIDGETBASED.PK);
				String name = r.get(T_WIDGETBASED.KEY);
				Integer fkFunctioninputtype = r.get(T_WIDGETBASED.FK_FUNCTIONINPUTTYPE);
				String functioninputtypeName = r.get(T_FUNCTIONINPUTTYPE.NAME);
				list.add(new WidgetbasedBean(pk, name, fkFunctioninputtype, functioninputtypeName));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			LOGGER.error(e.getMessage());
			throw new DataAccessException(e.getMessage());
		}
	}

	/**
	 * upsert the properties of the current report
	 * 
	 * @param reportId the ID of the report
	 * @param reportProperties the report properties
	 * @return the number of affected database columns
	 */
	public Integer upsertProperties(Integer reportId, Collection<ReportPropertyBean> reportProperties) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try {
			List<Integer> fkWidgetbased = new ArrayList<>();
			for (ReportPropertyBean bean : reportProperties) {
				fkWidgetbased.add(bean.getFkWidgetbased());
			}
			getJooq().transaction(t -> {
				DeleteConditionStep<TReportpropertyRecord> sql = DSL.using(t)
				// @formatter:off
          .deleteFrom(T_REPORTPROPERTY)
          .where(T_REPORTPROPERTY.FK_REPORT.eq(reportId))
          .and(T_REPORTPROPERTY.FK_WIDGETBASED.notIn(fkWidgetbased));
        // @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				for (ReportPropertyBean bean : reportProperties) {
					JSONB value = JSONB.jsonb(bean.getValue());
					InsertOnDuplicateSetMoreStep<TReportpropertyRecord> sql2 = DSL.using(t)
					// @formatter:off
            .insertInto(T_REPORTPROPERTY,
                        T_REPORTPROPERTY.FK_REPORT,
                        T_REPORTPROPERTY.FK_WIDGETBASED,
                        T_REPORTPROPERTY.VALUE)
            .values(reportId, bean.getFkWidgetbased(), value)
            .onConflict(T_REPORTPROPERTY.FK_REPORT, T_REPORTPROPERTY.FK_WIDGETBASED)
            .doUpdate()
            .set(T_REPORTPROPERTY.VALUE, value);
          // @formatter:on
					LOGGER.debug("{}", sql2.toString());
					lrw.addToInteger(sql2.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			LOGGER.error(e.getMessage());
			throw new DataAccessException(e.getMessage());
		}
	}

	/**
	 * get all function selections from the database
	 * 
	 * @return the list of function selection beans; an empty one at least
	 */
	public List<FunctionSelectionBean> getFunctionSelections() {
		try {
			SelectSeekStep1<Record2<Integer, String>, String> sql = getJooq()
			// @formatter:off
        .select(T_FUNCTIONLIST.PK,
                T_FUNCTIONLIST.NAME)
        .from(T_FUNCTIONLIST)
        .orderBy(T_FUNCTIONLIST.NAME);
      // @formatter:on
			LOGGER.debug("{}", sql.toString());
			List<FunctionSelectionBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_FUNCTIONLIST.PK);
				String name = r.get(T_FUNCTIONLIST.NAME);
				list.add(new FunctionSelectionBean(pk, name));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			LOGGER.error(e.getMessage());
			throw new DataAccessException(e.getMessage());
		}
	}
}
