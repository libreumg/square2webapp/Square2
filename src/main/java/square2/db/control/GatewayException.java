package square2.db.control;

/**
 * 
 * @author henkej
 *
 */
public class GatewayException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * create a new gateway exception
	 * 
	 * @param messageParts
	 *          array of message parts
	 */
	public GatewayException(String... messageParts) {
		super(concatenate(messageParts));
	}

	private static final String concatenate(String[] messageParts) {
		StringBuilder buf = new StringBuilder();
		for (String s : messageParts) {
			buf.append(s);
		}
		return buf.toString();
	}
}
