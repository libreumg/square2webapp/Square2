package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_CALCPERF;
import static de.ship.dbppsquare.square.Tables.T_ELEMENT;
import static de.ship.dbppsquare.square.Tables.T_GROUPPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_LASTCHANGE;
import static de.ship.dbppsquare.square.Tables.T_MATRIXELEMENTPARAMETER;
import static de.ship.dbppsquare.square.Tables.T_MATRIXELEMENTVALUE;
import static de.ship.dbppsquare.square.Tables.T_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_REPORT;
import static de.ship.dbppsquare.square.Tables.T_REPORTDATARELEASE;
import static de.ship.dbppsquare.square.Tables.T_SNIPPLET;
import static de.ship.dbppsquare.square.Tables.T_USERPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUP;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUPELEMENT;
import static de.ship.dbppsquare.square.Tables.V_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.V_STUDY;
import static de.ship.dbppsquare.square.Tables.V_VARIABLEGROUP;
import static de.ship.dbppsquare.squareout.Tables.T_CALCULATIONRESULT;
import static de.ship.dbppsquare.squareout.Tables.T_REPORTOUTPUT;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertOnDuplicateStep;
import org.jooq.InsertResultStep;
import org.jooq.JSONB;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Record5;
import org.jooq.Record6;
import org.jooq.Record7;
import org.jooq.SelectConditionStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import de.ship.dbppsquare.square.tables.records.TCalcperfRecord;
import de.ship.dbppsquare.square.tables.records.TGroupprivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixelementparameterRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixelementvalueRecord;
import de.ship.dbppsquare.square.tables.records.TPrivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TReportRecord;
import de.ship.dbppsquare.square.tables.records.TReportdatareleaseRecord;
import de.ship.dbppsquare.square.tables.records.TSnippletRecord;
import de.ship.dbppsquare.square.tables.records.TUserprivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TVariablegroupRecord;
import de.ship.dbppsquare.square.tables.records.TVariablegroupelementRecord;
import de.ship.dbppsquare.squareout.tables.records.TCalculationresultRecord;
import de.ship.dbppsquare.squareout.tables.records.TReportoutputRecord;
import square2.db.control.model.LambdaResultWrapper;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.report.design.ReportDesignBean;
import square2.modules.model.study.ListElement;
import square2.modules.model.study.VariablegroupBean;
import square2.modules.model.vargroup.VargroupElementBean;
import square2.modules.model.vargroup.VariablegroupElementBean;

/**
 * 
 * @author henkej
 *
 */
public class VargroupGateway extends CsvGateway {
	private static final Logger LOGGER = LogManager.getLogger(VargroupGateway.class);

	/**
	 * create new variable group gateway
	 * 
	 * @param facesContext the context of this class
	 */
	public VargroupGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() {
		// no right management needed
		return new HashMap<>();
	}

	/**
	 * get all variable groups where usnr has read rights
	 * 
	 * @param usnr to be filtered by
	 * @param includeDependings if true, include all depending objects on variable
	 * groups
	 * @return the list of found variable group beans sorted by its name; an empty
	 * list at least
	 * 
	 * if anything went wrong on database side
	 */
	public List<VariablegroupBean> getVariablegrouplist(Integer usnr, boolean includeDependings) {
		List<VariablegroupBean> list = new ArrayList<>();
		try (SelectConditionStep<Record6<Integer, String, String, Integer, Long, EnumAccesslevel>> sql = getJooq()
		// @formatter:off
			.select(V_VARIABLEGROUP.PK, 
							V_VARIABLEGROUP.NAME,
							V_VARIABLEGROUP.DESCRIPTION,
							V_VARIABLEGROUP.FK_PRIVILEGE, 
							V_VARIABLEGROUP.ELEMENTS,
							V_PRIVILEGE.ACL)
			.from(V_VARIABLEGROUP)
			.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(V_VARIABLEGROUP.FK_PRIVILEGE).and(V_PRIVILEGE.FK_USNR.eq(usnr)))
			.where(V_PRIVILEGE.ACL.isNotNull()); // because of left join
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				VariablegroupBean bean = new VariablegroupBean();
				bean.setVariablegroupId(r.get(V_VARIABLEGROUP.PK));
				bean.setName(r.get(V_VARIABLEGROUP.NAME));
				bean.setDescription(r.get(V_VARIABLEGROUP.DESCRIPTION));
				bean.setFkRight(r.get(V_VARIABLEGROUP.FK_PRIVILEGE));
				bean.setElements(r.get(V_VARIABLEGROUP.ELEMENTS));
				bean.getAcl().setAcl(r.get(V_PRIVILEGE.ACL));
				list.add(bean);
			}
			// add all depending objects
			if (includeDependings) {
				addAllDependings(list);
			}
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * delete variable group referenced by id and all its dependencies
	 * 
	 * @param id the id of the variable group
	 * @param privilegeId the id of the privilege for that variable group
	 * 
	 * if anything went wrong on database side
	 */
	public void deleteVariablegroup(Integer id, Integer privilegeId) {
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				DeleteConditionStep<TReportdatareleaseRecord> sql = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_REPORTDATARELEASE)
					.where(T_REPORTDATARELEASE.FK_REPORT.in(DSL.using(c)
						.select(T_REPORT.PK)
						.from(T_REPORT)
						.where(T_REPORT.FK_VARIABLEGROUP.eq(id))));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				sql.execute();

				DeleteConditionStep<TCalcperfRecord> sql1 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_CALCPERF)
					.where(T_CALCPERF.FK_REPORT.in(DSL.using(c)
						.select(T_REPORT.PK)
						.from(T_REPORT)
						.where(T_REPORT.FK_VARIABLEGROUP.eq(id))));
				// @formatter:on
				LOGGER.debug("{}", sql1.toString());
				sql1.execute();

				DeleteConditionStep<TMatrixelementparameterRecord> sql2 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTPARAMETER)
					.where(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE.in(DSL.using(c)
						.select(T_MATRIXELEMENTVALUE.PK)
						.from(T_MATRIXELEMENTVALUE)
						.leftJoin(T_REPORT).on(T_REPORT.PK.eq(T_MATRIXELEMENTVALUE.FK_REPORT))
						.where(T_REPORT.FK_VARIABLEGROUP.eq(id))));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				sql2.execute();

				DeleteConditionStep<TMatrixelementvalueRecord> sql3 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTVALUE)
					.where(T_MATRIXELEMENTVALUE.FK_REPORT.in(DSL.using(c)
						.select(T_REPORT.PK)
						.from(T_REPORT)
						.where(T_REPORT.FK_VARIABLEGROUP.eq(id))));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				sql3.execute();

				DeleteConditionStep<TSnippletRecord> sql4 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_SNIPPLET)
					.where(T_SNIPPLET.FK_REPORT.in(DSL.using(c)
						.select(T_REPORT.PK)
						.from(T_REPORT)
						.where(T_REPORT.FK_VARIABLEGROUP.eq(id))));
				// @formatter:on
				LOGGER.debug("{}", sql4.toString());
				sql4.execute();

				DeleteConditionStep<TReportoutputRecord> sql5 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_REPORTOUTPUT)
					.where(T_REPORTOUTPUT.FK_REPORT.in(DSL.using(c)
						.select(T_REPORT.PK)
						.from(T_REPORT)
						.where(T_REPORT.FK_VARIABLEGROUP.eq(id))));
				// @formatter:on
				LOGGER.debug("{}", sql5.toString());
				sql5.execute();

				DeleteConditionStep<TCalculationresultRecord> sql6 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_CALCULATIONRESULT)
					.where(T_CALCULATIONRESULT.FK_REPORT.in(DSL.using(c)
						.select(T_REPORT.PK)
						.from(T_REPORT)
						.where(T_REPORT.FK_VARIABLEGROUP.eq(id))));
				// @formatter:on
				LOGGER.debug("{}", sql6.toString());
				sql6.execute();

				DeleteConditionStep<TReportRecord> sql7 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_REPORT)
					.where(T_REPORT.FK_VARIABLEGROUP.eq(id));
				// @formatter:on
				LOGGER.debug("{}", sql7.toString());
				sql7.execute();

				DeleteConditionStep<TVariablegroupelementRecord> sql8 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_VARIABLEGROUPELEMENT)
					.where(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(id));
				// @formatter:on
				LOGGER.debug("{}", sql8.toString());
				sql8.execute();

				DeleteConditionStep<TVariablegroupRecord> sql9 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_VARIABLEGROUP)
					.where(T_VARIABLEGROUP.PK.eq(id));
				// @formatter:on
				LOGGER.debug("{}", sql9.toString());
				sql9.execute();

				DeleteConditionStep<TGroupprivilegeRecord> sql10 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_GROUPPRIVILEGE)
					.where(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(privilegeId));
				// @formatter:on
				LOGGER.debug("{}", sql10.toString());
				sql10.execute();

				DeleteConditionStep<TUserprivilegeRecord> sql11 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_USERPRIVILEGE)
					.where(T_USERPRIVILEGE.FK_PRIVILEGE.eq(privilegeId));
				// @formatter:on
				LOGGER.debug("{}", sql11.toString());
				sql11.execute();

				DeleteConditionStep<TPrivilegeRecord> sql12 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_PRIVILEGE)
					.where(T_PRIVILEGE.PK.eq(privilegeId));
				// @formatter:on
				LOGGER.debug("{}", sql12.toString());
				sql12.execute();
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get name of variable group
	 * 
	 * @param id the id of the variable group
	 * @return the name of the variable group
	 * 
	 * if anything went wrong on database side
	 */
	public String getVariablegroupName(Integer id) {
		try (SelectConditionStep<Record1<String>> sql = getJooq()
		// @formatter:off
			.select(T_VARIABLEGROUP.NAME)
			.from(T_VARIABLEGROUP)
			.where(T_VARIABLEGROUP.PK.eq(id));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			String name = sql.fetchOne(T_VARIABLEGROUP.NAME);
			if (name == null) {
				throw new DataAccessException("no variablegroup found with id = " + id);
			}
			return name;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}

	}

	/**
	 * get variable group bean referenced by id from db
	 * 
	 * @param id of variable group
	 * @param includeDependings flag to determine if depending variable groups
	 * should be added to that bean
	 * @return the variable group
	 * 
	 * if anything went wrong on database side
	 */
	public VariablegroupBean getVariablegroup(Integer id, boolean includeDependings) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record5<String, String, Integer, Long, EnumAccesslevel>> sql = jooq
			// @formatter:off
				.select(V_VARIABLEGROUP.NAME, 
								V_VARIABLEGROUP.DESCRIPTION,
								V_VARIABLEGROUP.FK_PRIVILEGE,
								V_VARIABLEGROUP.ELEMENTS, 
								V_PRIVILEGE.ACL)
				.from(V_VARIABLEGROUP)
				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(V_VARIABLEGROUP.FK_PRIVILEGE))
				.where(V_VARIABLEGROUP.PK.eq(id))
				.and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());

			VariablegroupBean bean = new VariablegroupBean();
			bean.setVariablegroupId(id);
			for (Record r : sql.fetch()) {
				bean.setName(r.get(V_VARIABLEGROUP.NAME));
				bean.setDescription(r.get(V_VARIABLEGROUP.DESCRIPTION));
				bean.setFkRight(r.get(V_VARIABLEGROUP.FK_PRIVILEGE));
				bean.setElements(r.get(V_VARIABLEGROUP.ELEMENTS));
				bean.getAcl().setAcl(r.get(V_PRIVILEGE.ACL));
			}

			SelectConditionStep<Record3<Integer, String, JSONB>> sql2 = jooq
			// @formatter:off
				.select(T_ELEMENT.PK, 
								T_ELEMENT.NAME,
								T_ELEMENT.TRANSLATION)
				.from(T_VARIABLEGROUPELEMENT)
				.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
				.where(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(id));
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());

			for (Record r : sql2.fetch()) {
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				bean.getListVariables()
						.add(new ListElement(r.get(T_ELEMENT.PK), translation, r.get(T_ELEMENT.NAME), true, false));
			}

			SelectConditionStep<Record3<Integer, String, LocalDateTime>> sql3 = jooq
			// @formatter:off
				.select(T_REPORT.PK, 
								T_REPORT.NAME,
								T_REPORT.LASTCHANGE)
				.from(T_REPORT)
				.where(T_REPORT.FK_VARIABLEGROUP.eq(id));
			// @formatter:on
			LOGGER.debug("{}", sql3.toString());

			for (Record r : sql3.fetch()) {
				Integer reportPk = r.get(T_REPORT.PK);
				String reportName = r.get(T_REPORT.NAME);
				Integer pagelength = getFacesContext().getProfile().getConfigInteger("config.template.paginator.pagelength", 20);
				LocalDateTime ldt = r.get(T_REPORT.LASTCHANGE);
				Timestamp lastchange = ldt == null ? null : Timestamp.valueOf(ldt);
				ReportDesignBean reportBean = new ReportDesignBean(null, null, pagelength, lastchange, 0);
				reportBean.setId(reportPk);
				reportBean.setName(reportName);
				bean.getReports().add(reportBean);
			}

			if (includeDependings) {
				List<VariablegroupBean> list = new ArrayList<>();
				list.add(bean);
				addAllDependings(list);
			}
			return bean;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all elements that usnr can access, this means, all elements under studies
	 * he is using and that he has access to, but not technical variables
	 * 
	 * @param usnr the id of the current user
	 * @param variablegroupId the id of the variable group
	 * @return a list of elements in this variable group
	 * 
	 * if anything went wrong on database side
	 */
	public List<VargroupElementBean> getAllElementsOf(Integer usnr, Integer variablegroupId) {
		List<VargroupElementBean> list = new ArrayList<>();
		if (usnr == null) {
			throw new DataAccessException("usnr is null");
		}
		try (
				SelectConditionStep<Record7<Integer, Integer, JSONB, String, Integer, Integer, Integer>> sql = getJooq()
				// @formatter:off
			.select(T_ELEMENT.PK, 
							T_ELEMENT.FK_PARENT,
							T_ELEMENT.TRANSLATION, 
							T_ELEMENT.NAME,
							T_ELEMENT.FK_PRIVILEGE, 
							T_VARIABLE.FK_ELEMENT, 
							T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP)
			.from(T_ELEMENT)
			.fullOuterJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(T_ELEMENT.PK))
			.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_ELEMENT.FK_PRIVILEGE))
			.leftJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.FK_ELEMENT.eq(T_ELEMENT.PK)).and(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(variablegroupId))
			.where(V_PRIVILEGE.FK_USNR.eq(usnr))
			.and(V_PRIVILEGE.ACL.isNotNull()) // because of left join
			.and(T_VARIABLE.CONTROL_TYPE.isNull());
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				boolean chosen = variablegroupId.equals(r.get(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP));
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				VargroupElementBean bean = new VargroupElementBean(r.get(T_ELEMENT.PK), translation,
						r.get(T_ELEMENT.FK_PARENT), chosen);
				bean.setName(r.get(T_ELEMENT.NAME));
				bean.setIsVariable(r.get(T_VARIABLE.FK_ELEMENT) != null);
				list.add(bean);
			}
			LOGGER.debug("found {} elements", list.size());
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add bean to db
	 * 
	 * @param bean the variable group bean
	 * @param usnr the id of the user
	 * @return pk of table (ID of variable group)
	 */
	public Integer addVariablegroup(VariablegroupBean bean, Integer usnr) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				Integer newPrivilegeId = addAndGetPrivilegeId(DSL.using(c), null);
				addUserRWXPrivilege(DSL.using(c), newPrivilegeId, usnr);
				InsertResultStep<TVariablegroupRecord> sql = DSL.using(c)
				// @formatter:off
					.insertInto(T_VARIABLEGROUP, 
											T_VARIABLEGROUP.NAME,
											T_VARIABLEGROUP.DESCRIPTION, 
											T_VARIABLEGROUP.FK_PRIVILEGE)
					.values(bean.getName(), bean.getDescription(), newPrivilegeId)
					.returning(T_VARIABLEGROUP.PK);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				Integer pk = sql.fetchOne().getPk();
				lrw.setInteger(pk);
				String privName = new StringBuilder("variablegroup.").append(pk).toString();
				UpdateConditionStep<TPrivilegeRecord> sql2 = DSL.using(c)
				// @formatter:off
					.update(T_PRIVILEGE)
					.set(T_PRIVILEGE.NAME, privName)
					.where(T_PRIVILEGE.PK.eq(newPrivilegeId));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				sql2.execute();
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update definition of variable group bean
	 * 
	 * @param bean the variable group bean
	 * @return magnitude of affected rows
	 * 
	 * if anything went wrong on database side
	 */
	public int updateDefinition(VariablegroupBean bean) {
		try (UpdateConditionStep<TVariablegroupRecord> sql = getJooq()
		// @formatter:off
			.update(T_VARIABLEGROUP)
			.set(T_VARIABLEGROUP.NAME, bean.getName())
			.set(T_VARIABLEGROUP.DESCRIPTION, bean.getDescription())
			.where(T_VARIABLEGROUP.PK.eq(bean.getVariablegroupId()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * check if a refresh is needed for table T_QS_HIERARCHY and T_VARIABLE and
	 * T_VARIABLEGROUP; if so, return new lastchange date, otherwise, return old one
	 * 
	 * @param lastchange the lastchange
	 * 
	 * @return new lastchange if found
	 * 
	 * if anything went wrong on database side
	 */
	public Date getRefreshNeeded(Date lastchange) {
		if (lastchange == null) {
			return new Date();
		}
		LocalDateTime lc = lastchange.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		try (SelectConditionStep<Record1<LocalDateTime>> sql = getJooq()
		// @formatter:off
			.select(T_LASTCHANGE.LASTCHANGE)
			.from(T_LASTCHANGE)
			.where(T_LASTCHANGE.LASTCHANGE.lessThan(lc))
			.and(T_LASTCHANGE.TABLENAME.in(T_ELEMENT.getName(), T_VARIABLE.getName(), T_VARIABLEGROUPELEMENT.getName()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				LocalDateTime ldt = r.get(T_LASTCHANGE.LASTCHANGE);
				Date foundChange = ldt == null ? new Date() : Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
				if (foundChange.after(lastchange)) {
					lastchange = foundChange;
				}
			}
			return lastchange;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete all dirty marked variables that are not chosen and insert all dirty
	 * variables that are chosen; if an insert fails, ignore it on duplication error
	 * 
	 * @param allDirtyVariables list of dirty variables
	 * @param variablegroupId id of the variable group
	 * @return number of affected rows in db
	 * 
	 * if anything went wrong on database side
	 */
	public Integer changeVariablegroupContent(List<VargroupElementBean> allDirtyVariables, Integer variablegroupId) {
		List<Integer> deleteCandidates = new ArrayList<Integer>();
		List<Integer> insertCandidates = new ArrayList<Integer>();
		for (VargroupElementBean bean : allDirtyVariables) {
			if (bean.getChosen()) {
				insertCandidates.add(bean.getKeyElement());
			} else {
				deleteCandidates.add(bean.getKeyElement());
			}
		}
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				Integer affected = 0;
				if (deleteCandidates.size() > 0) {

					DeleteConditionStep<TMatrixelementparameterRecord> sql = DSL.using(c)
					// @formatter:off
						.deleteFrom(T_MATRIXELEMENTPARAMETER)
						.where(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE.in(DSL.using(c)
							.select(T_MATRIXELEMENTVALUE.PK)
							.from(T_MATRIXELEMENTVALUE)
							.rightJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(variablegroupId)
									                              .and(T_VARIABLEGROUPELEMENT.FK_ELEMENT.in(deleteCandidates)))));
					// @formatter:on
					LOGGER.debug("{}", sql.toString());
					affected += sql.execute();

					DeleteConditionStep<TMatrixelementvalueRecord> sql1 = DSL.using(c)
					// @formatter:off
						.deleteFrom(T_MATRIXELEMENTVALUE)
						.where(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT.in(DSL.using(c)
							.select(T_VARIABLEGROUPELEMENT.PK)
							.from(T_VARIABLEGROUPELEMENT)
							.where(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(variablegroupId))
							.and(T_VARIABLEGROUPELEMENT.FK_ELEMENT.in(deleteCandidates))));
					// @formatter:on
					LOGGER.debug("{}", sql1.toString());
					affected += sql1.execute();

					DeleteConditionStep<TVariablegroupelementRecord> sql2 = DSL.using(c)
					// @formatter:off
						.deleteFrom(T_VARIABLEGROUPELEMENT)
						.where(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(variablegroupId))
						.and(T_VARIABLEGROUPELEMENT.FK_ELEMENT.in(deleteCandidates));
					// @formatter:on
					LOGGER.debug("{}", sql2.toString());
					affected += sql2.execute();
				}
				if (insertCandidates.size() > 0) {
					for (Integer keyElement : insertCandidates) {
						Integer varOrder = keyElement; // for now, use keyElement as varOrder; no ordering defined in web
						// mask yet
						InsertOnDuplicateStep<TVariablegroupelementRecord> sql = DSL.using(c)
						// @formatter:off
							.insertInto(T_VARIABLEGROUPELEMENT, 
													T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP,
													T_VARIABLEGROUPELEMENT.FK_ELEMENT, 
													T_VARIABLEGROUPELEMENT.VARORDER)
							.select(DSL.using(c)
								.select(DSL.val(variablegroupId), 
												DSL.val(keyElement), 
												DSL.val(varOrder))
								.whereNotExists(DSL.using(c)
									.selectOne()
									.from(T_VARIABLEGROUPELEMENT)
									.where(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(variablegroupId))
									.and(T_VARIABLEGROUPELEMENT.FK_ELEMENT.eq(keyElement))));
						// @formatter:on
						LOGGER.debug("{}", sql.toString());
						affected += sql.execute();
					}
				}
				lrw.setInteger(affected);
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get privilege id for vargroup referenced by id
	 * 
	 * @param id id of the variable group
	 * @return the id of the privilege for this variable group; no such found will
	 * throw a DataBaseException
	 * 
	 * if anything went wrong on database side
	 */
	public Integer getPrivilegeId(Integer id) {
		try (SelectConditionStep<Record1<Integer>> sql = getJooq()
		// @formatter:off
			.select(T_VARIABLEGROUP.FK_PRIVILEGE)
			.from(T_VARIABLEGROUP)
			.where(T_VARIABLEGROUP.PK.eq(id));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Integer result = sql.fetchOne().get(T_VARIABLEGROUP.FK_PRIVILEGE);
			if (result == null) {
				throw new DataAccessException("no privilege id found for variable group id " + id);
			}
			return result;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all object names of depending objects on each element of list
	 * 
	 * @param list the list of variable group beans
	 * 
	 * if anything went wrong on database side
	 */
	private void addAllDependings(List<VariablegroupBean> list) {
		List<Integer> ids = new ArrayList<>();
		for (VariablegroupBean bean : list) {
			ids.add(bean.getVariablegroupId());
		}
		Map<Integer, List<String>> deps = new HashMap<>();
		try (SelectConditionStep<Record2<Integer, String>> sql = getJooq()
		// @formatter:off
			.select(T_REPORT.FK_VARIABLEGROUP, 
							T_REPORT.NAME)
			.from(T_REPORT)
			.where(T_REPORT.FK_VARIABLEGROUP.in(ids));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				Integer fkVariablegroupdef = r.get(T_REPORT.FK_VARIABLEGROUP);
				List<String> l = deps.get(fkVariablegroupdef);
				if (l == null) {
					l = new ArrayList<>();
					deps.put(fkVariablegroupdef, l);
				}
				l.add(r.get(T_REPORT.NAME));
			}
			for (VariablegroupBean bean : list) {
				List<String> depsList = deps.get(bean.getVariablegroupId());
				if (depsList != null) {
					bean.getDependings().addAll(depsList);
				}
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get pk of variable group of report referenced by reportId
	 * 
	 * @param reportId to be used as reference
	 * @return variable group ID
	 */
	public Integer getVariablegroupIdByReport(Integer reportId) {
		try (SelectConditionStep<Record1<Integer>> sql = getJooq()
		// @formatter:off
			.select(T_REPORT.FK_VARIABLEGROUP)
			.from(T_REPORT)
			.where(T_REPORT.PK.eq(reportId));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Record1<Integer> r = sql.fetchOne();
			return r == null ? null : r.get(T_REPORT.FK_VARIABLEGROUP);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all elements from variable group
	 * 
	 * @param id of variable group
	 * @return list of found elements, an empty list at least
	 * 
	 * if anything went wrong on database side
	 */
	public List<VariablegroupElementBean> getVariablegroupElements(Integer id) {
		try (SelectConditionStep<Record5<Integer, Integer, String, JSONB, Integer>> sql = getJooq()
		// @formatter:off
			.select(T_VARIABLEGROUPELEMENT.PK, 
							T_VARIABLEGROUPELEMENT.FK_ELEMENT,
							T_ELEMENT.NAME,
							T_ELEMENT.TRANSLATION,
							T_VARIABLEGROUPELEMENT.VARORDER)
			.from(T_VARIABLEGROUPELEMENT)
			.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
			.where(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(id));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<VariablegroupElementBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_VARIABLEGROUPELEMENT.PK);
				Integer fkElement = r.get(T_VARIABLEGROUPELEMENT.FK_ELEMENT);
				String name = r.get(T_ELEMENT.NAME);
				JSONB translation = r.get(T_ELEMENT.TRANSLATION);
				VariablegroupElementBean bean = new VariablegroupElementBean(pk, fkElement, name, translation);
				bean.setVarorder(r.get(T_VARIABLEGROUPELEMENT.VARORDER));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update variable order of list
	 * 
	 * @param list of variablegroup elements
	 * @return number of affected rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer updateVarorder(List<VariablegroupElementBean> list) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		lrw.setInteger(0);
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				for (VariablegroupElementBean bean : list) {
					UpdateConditionStep<TVariablegroupelementRecord> sql = DSL.using(t)
					// @formatter:off
						.update(T_VARIABLEGROUPELEMENT)
						.set(T_VARIABLEGROUPELEMENT.VARORDER, bean.getVarorder())
						.where(T_VARIABLEGROUPELEMENT.PK.eq(bean.getPk()))
						.and(T_VARIABLEGROUPELEMENT.VARORDER.ne(bean.getVarorder())); // reduce database operations for no changes
					// @formatter:on
					LOGGER.debug("{}", sql.toString());
					lrw.addToInteger(sql.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get orders from T_VARIABLE.var_order prefixed by form order prefixed by study
	 * order for all variables in variablegroup of variablegroupPk that have no
	 * order; else use the order of the variablegroup
	 * 
	 * @param list the list of variable group elements, used as input output
	 * parameter
	 * @param variablegroupPk the id of the variable group
	 * 
	 * if anything went wrong on database side
	 */
	public void setOrderOfStudyFormVariable(List<VariablegroupElementBean> list, Integer variablegroupPk) {
		List<Integer> variables = new ArrayList<>();
		Map<Integer, VariablegroupElementBean> map = new HashMap<>();
		for (VariablegroupElementBean bean : list) {
			variables.add(bean.getFkElement());
			map.put(bean.getFkElement(), bean);
		}
		try (SelectConditionStep<Record5<Integer, Integer, Integer, Integer, Integer>> sql = getJooq()
		// @formatter:off
			.select(T_VARIABLE.FK_ELEMENT, // element of variable
							T_VARIABLE.VAR_ORDER, 
							T_ELEMENT.FK_PARENT, // element of form
							V_STUDY.FK_ELEMENT, // element of study
							T_VARIABLEGROUPELEMENT.VARORDER) // variable order in variable group
			.from(T_VARIABLEGROUPELEMENT)
			.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
			.leftJoin(V_STUDY).on(V_STUDY.FK_ELEMENT.eq(T_VARIABLE.FK_ELEMENT))
			.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLE.FK_ELEMENT))
			.where(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(variablegroupPk));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Map<Integer, Integer> studyMap = new HashMap<>();
			Map<Integer, Integer> formMap = new HashMap<>();
			for (Record r : sql.fetch()) {
				Integer keyElement = r.get(T_VARIABLE.FK_ELEMENT);
				Integer varOrder = r.get(T_VARIABLE.VAR_ORDER);
				Integer studyOrder = r.get(V_STUDY.FK_ELEMENT); // we don't have a study order, so use key_element instead
				Integer formOrder = r.get(T_ELEMENT.FK_PARENT); // we don't have a form order, so use key_element
				Integer groupOrder = r.get(T_VARIABLEGROUPELEMENT.VARORDER);
				Integer study = remap(studyMap, studyOrder);
				Integer form = remap(formMap, formOrder);
				VariablegroupElementBean bean = map.get(keyElement);
				if (groupOrder == null) {
					varOrder = varOrder == null ? 0 : varOrder;
					Integer order = varOrder + (100000 * form) + (1000000 * study);
					bean.setVarorder(order); // do this only if there is no order yet
				}
				map.put(keyElement, bean);
			}
			list.clear();
			list.addAll(map.values());
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * ensure to have small numbers for ordering; input is order, output is
	 * corresponding small number of map
	 * 
	 * @param map the map to be remapped
	 * @param order the order of mapping
	 * @return small order number
	 */
	private Integer remap(Map<Integer, Integer> map, Integer order) {
		Integer newOrder = map.get(order);
		if (newOrder == null) {
			newOrder = map.size();
			map.put(order, newOrder);
		}
		return newOrder;
	}
}
