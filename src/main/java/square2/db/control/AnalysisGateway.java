package square2.db.control;

import static de.ship.dbppsquare.square.Tables.R_MATRIXCANDIDATE;
import static de.ship.dbppsquare.square.Tables.T_ELEMENT;
import static de.ship.dbppsquare.square.Tables.T_ELEMENTSHIP;
import static de.ship.dbppsquare.square.Tables.T_FUNCTION;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONCATEGORY;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONINPUT;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONINPUTTYPE;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONLIST;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONOUTPUT;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONSCALE;
import static de.ship.dbppsquare.square.Tables.T_MATRIXCOLUMN;
import static de.ship.dbppsquare.square.Tables.T_MATRIXCOLUMNEXCLUDESCALE;
import static de.ship.dbppsquare.square.Tables.T_MATRIXCOLUMNPARAMETER;
import static de.ship.dbppsquare.square.Tables.T_MATRIXELEMENTPARAMETER;
import static de.ship.dbppsquare.square.Tables.T_MATRIXELEMENTVALUE;
import static de.ship.dbppsquare.square.Tables.T_METADATATYPE;
import static de.ship.dbppsquare.square.Tables.T_REPORT;
import static de.ship.dbppsquare.square.Tables.T_SCALE;
import static de.ship.dbppsquare.square.Tables.T_SNIPPLET;
import static de.ship.dbppsquare.square.Tables.T_TRANSLATION;
import static de.ship.dbppsquare.square.Tables.T_VARIABLE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEATTRIBUTE;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUP;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUPELEMENT;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEUSAGE;
import static de.ship.dbppsquare.square.Tables.V_PRIVILEGE;
import static de.ship.dbppsquare.squareout.Tables.T_CALCULATIONRESULT;
import static de.ship.dbppsquare.squareout.Tables.T_REPORTOUTPUT;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.Configuration;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.InsertOnDuplicateStep;
import org.jooq.InsertResultStep;
import org.jooq.InsertReturningStep;
import org.jooq.InsertValuesStep4;
import org.jooq.JSONB;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record12;
import org.jooq.Record13;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.Record5;
import org.jooq.Record6;
import org.jooq.Record8;
import org.jooq.Record9;
import org.jooq.SelectConditionStep;
import org.jooq.SelectHavingStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectSeekStep1;
import org.jooq.SelectSeekStep2;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import com.google.gson.JsonArray;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import de.ship.dbppsquare.square.enums.EnumAnadef;
import de.ship.dbppsquare.square.tables.records.TFunctionlistRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixcolumnRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixcolumnexcludescaleRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixcolumnparameterRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixelementparameterRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixelementvalueRecord;
import de.ship.dbppsquare.square.tables.records.TReportRecord;
import de.ship.dbppsquare.square.tables.records.TScaleRecord;
import de.ship.dbppsquare.square.tables.records.TSnippletRecord;
import de.ship.dbppsquare.squareout.tables.records.TCalculationresultRecord;
import de.ship.dbppsquare.squareout.tables.records.TReportoutputRecord;
import ship.jsf.components.selectFilterableMenu.FilterableList;
import ship.jsf.components.selectFilterableMenu.FilterableListInterface;
import ship.jsf.components.selectFilterableMenu.KeyValueBeanWrapper;
import square2.db.control.model.LambdaResultWrapper;
import square2.help.HandsetRuleHelper;
import square2.help.SquareFacesContext;
import square2.help.ValuelistConverter;
import square2.json.JsonMatrixBean;
import square2.json.MatrixColumn;
import square2.json.MatrixData;
import square2.json.MatrixRow;
import square2.modules.model.AclBean;
import square2.modules.model.AnaparfunOutputBean;
import square2.modules.model.admin.FunctioncategoryBean;
import square2.modules.model.functionselection.FunctionselectionBean;
import square2.modules.model.report.ajax.AjaxRequestBean;
import square2.modules.model.report.ajax.ParamBean;
import square2.modules.model.report.analysis.AnalysisDefaultBean;
import square2.modules.model.report.analysis.AnalysisFunctionBean;
import square2.modules.model.report.analysis.AnalysisMatrix;
import square2.modules.model.report.analysis.AnalysisMatrixBean;
import square2.modules.model.report.analysis.AnalysisTemplate;
import square2.modules.model.report.analysis.AnalysisVariableParameterBean;
import square2.modules.model.report.analysis.AnaparfunBean;
import square2.modules.model.report.analysis.AnaparfunFlagBean;
import square2.modules.model.report.analysis.AnavarparBean;
import square2.modules.model.report.analysis.DefaultParameter;
import square2.modules.model.report.analysis.FunctionVariablePairBean;
import square2.modules.model.report.analysis.MatrixBean;
import square2.modules.model.report.analysis.MatrixColumnExcludeScaleBean;
import square2.modules.model.report.analysis.MatrixLine;
import square2.modules.model.report.analysis.ParameterFunction;
import square2.modules.model.report.analysis.VariableBean;
import square2.modules.model.report.analysis.VariablegroupBean;
import square2.modules.model.report.analysis.matrix.MatrixCheckbox;
import square2.modules.model.report.analysis.matrix.MatrixDisabled;
import square2.modules.model.report.analysis.matrix.MatrixEmptyCell;
import square2.modules.model.report.analysis.matrix.MatrixFunctionButton;
import square2.modules.model.report.analysis.matrix.MatrixTitle;
import square2.modules.model.report.analysis.matrix.MatrixVariableButton;
import square2.modules.model.report.analysis.matrix.MatrixVarlistsVariableLabel;
import square2.modules.model.statistic.FunctionBean;
import square2.modules.model.statistic.FunctionInputBean;
import square2.modules.model.statistic.FunctionScale;
import square2.modules.model.statistic.FunctioninputtypeBean;
import square2.modules.model.template.report.FunctionoutputBean;
import square2.modules.model.template.report.MatrixcolumnParameterBean;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisGateway extends JooqGateway {
	private static final Logger LOGGER = LogManager.getLogger(AnalysisGateway.class);

	/**
	 * generate new AnalysisGateway
	 * 
	 * @param facesContext the context of this gateway
	 */
	public AnalysisGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	/**
	 * get analysis template referenced by pk
	 * 
	 * @param pk ID of the analysis template
	 * @return the analysis template if found
	 * 
	 * if anything went wrong on database side
	 */
	public AnalysisTemplate getAnalysisTemplate(Integer pk) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record13<Integer, String, Integer, LocalDateTime, Boolean, String, Integer, String, JSONB, Integer, String, Integer, String>> sql = jooq
			// @formatter:off
				.select(T_FUNCTION.PK, 
						    T_FUNCTION.NAME, 
						    T_FUNCTION.FK_PRIVILEGE, 
						    T_FUNCTION.CREATIONDATE,
						    T_FUNCTION.USE_INTERVALS, 
						    T_FUNCTION.VIGNETTE,
						    T_FUNCTIONINPUT.PK,
						    T_FUNCTIONINPUT.INPUT_NAME, 
						    T_FUNCTIONINPUT.DEFAULT_VALUE,
						    T_FUNCTIONINPUT.FK_TYPE, 
						    T_FUNCTIONINPUTTYPE.NAME, 
						    T_FUNCTIONINPUTTYPE.FK_LANG,
						    T_TRANSLATION.VALUE)
				.from(T_FUNCTION)
				.leftJoin(T_FUNCTIONINPUT).on(T_FUNCTIONINPUT.FK_FUNCTION.eq(T_FUNCTION.PK))
				.leftJoin(T_FUNCTIONINPUTTYPE).on(T_FUNCTIONINPUTTYPE.PK.eq(T_FUNCTIONINPUT.FK_TYPE))
				.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.FK_FUNCTION.eq(T_FUNCTION.PK))
				.leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_FUNCTIONINPUTTYPE.FK_LANG)).and(T_TRANSLATION.ISOCODE.cast(String.class).eq(getFacesContext().getIsocode()))
				.where(T_FUNCTION.ACTIVATED.eq(true))
				.and(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(pk));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Map<Integer, FunctionBean> functionBeans = new HashMap<>();
			StatisticGateway gw = new StatisticGateway(getFacesContext());
			for (Record r : sql.fetch()) {
				FunctioninputtypeBean fType = new FunctioninputtypeBean(r.get(T_FUNCTIONINPUT.FK_TYPE), null,
						r.get(T_TRANSLATION.VALUE));
				fType.setName(r.get(T_FUNCTIONINPUTTYPE.NAME));
				Integer id = r.get(T_FUNCTION.PK);
				FunctionBean fb = functionBeans.get(id);
				if (fb == null) {
					List<FunctionScale> scales = gw.getAllFunctionscales(id);
					LocalDateTime ldt = r.get(T_FUNCTION.CREATIONDATE);
					Date creationDate = ldt == null ? null : Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
					fb = new FunctionBean(r.get(T_FUNCTION.FK_PRIVILEGE), creationDate, r.get(T_FUNCTION.USE_INTERVALS), scales);
					fb.setFunctionId(id);
					fb.setName(r.get(T_FUNCTION.NAME));
					fb.setVignette(r.get(T_FUNCTION.VIGNETTE));
				}
				FunctionInputBean fib = new FunctionInputBean(r.get(T_FUNCTIONINPUT.PK));
				fib.setName(r.get(T_FUNCTIONINPUT.INPUT_NAME));
				JSONB standard = r.get(T_FUNCTIONINPUT.DEFAULT_VALUE);
				fib.setStandard(standard == null ? null : standard.data());
				fib.setFkType(fType);
				fb.getInput().add(fib);
				functionBeans.put(id, fb);
			}
			AnalysisTemplate result = null;
			SelectConditionStep<Record2<String, String>> sql2 = jooq
			// @formatter:off
				.select(T_FUNCTIONLIST.NAME, 
						T_FUNCTIONLIST.DESCRIPTION)
				.from(T_FUNCTIONLIST)
				.where(T_FUNCTIONLIST.PK.eq(pk));
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());
			for (Record r : sql2.fetch()) {
				result = new AnalysisTemplate(pk);
				result.setName(r.get(T_FUNCTIONLIST.NAME));
				result.setDescription(r.get(T_FUNCTIONLIST.DESCRIPTION));
			}
			if (result == null) {
				throw new DataAccessException(
						"no such function list found for pk = ".concat(pk == null ? "null" : pk.toString()));
			}
			SelectConditionStep<Record2<Integer, Integer>> sql3 = jooq
			// @formatter:off
				.select(T_MATRIXCOLUMN.PK, 
						    T_MATRIXCOLUMN.FK_FUNCTION)
				.from(T_MATRIXCOLUMN)
				.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(pk));
			// @formatter:on
			LOGGER.debug("{}", sql3.toString());
			for (Record r : sql3.fetch()) {
				Integer anaparfun = r.get(T_MATRIXCOLUMN.PK);
				ParameterFunction pf = new ParameterFunction(functionBeans.get(r.get(T_MATRIXCOLUMN.FK_FUNCTION)));
				SelectConditionStep<Record5<Integer, Integer, JSONB, EnumAnadef, String>> sql4 = jooq
				// @formatter:off
					.select(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN, 
							    T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT, 
							    T_MATRIXCOLUMNPARAMETER.VALUE, 
							    T_MATRIXCOLUMNPARAMETER.FIXED,
							    T_FUNCTIONINPUT.INPUT_NAME)
					.from(T_MATRIXCOLUMNPARAMETER)
					.leftJoin(T_FUNCTIONINPUT).on(T_FUNCTIONINPUT.PK.eq(T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT))
					.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN))
					.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(pk));
				// @formatter:on
				LOGGER.debug("{}", sql4.toString());
				for (Record ri : sql4.fetch()) {
					Integer fkAnaparfun = ri.get(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN);
					if (fkAnaparfun.equals(anaparfun)) {
						DefaultParameter d = new DefaultParameter(ri.get(T_FUNCTIONINPUT.INPUT_NAME),
								ri.get(T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT));
						d.setFixed(ri.get(T_MATRIXCOLUMNPARAMETER.FIXED));
						JSONB standard = ri.get(T_MATRIXCOLUMNPARAMETER.VALUE);
						d.setValue(standard == null ? null : standard.data());
						pf.getDefaults().add(d);
					}
				}
				SelectConditionStep<Record4<Integer, Integer, String, Boolean>> sql5 = jooq
				// @formatter:off
					.select(T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN, 
									T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE, 
									T_SCALE.NAME,
									T_MATRIXCOLUMNEXCLUDESCALE.VALUE)
					.from(T_MATRIXCOLUMNEXCLUDESCALE)
					.leftJoin(T_SCALE).on(T_SCALE.PK.eq(T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE))
					.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN))
					.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(pk));
				// @formatter:on
				LOGGER.debug("{}", sql5.toString());
				for (Record ri : sql5.fetch()) {
					Boolean value = ri.get(T_MATRIXCOLUMNEXCLUDESCALE.VALUE);
					TScaleRecord scale = new TScaleRecord();
					scale.setPk(ri.get(T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE));
					scale.setName(ri.get(T_SCALE.NAME));
					pf.getVargroup().put(scale, value);
				}
				result.getParameterFunctions().add(pf);
			}
			return result;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * @deprecated no use any more
	 */
	@Deprecated
	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() {
		LOGGER.error("not yet implemented");
		return new HashMap<>();
	}

	/**
	 * get all analysis matrixes
	 * 
	 * @param report if set, use only analysis matrixes of that report (that have a
	 * variable group)
	 * @return list of analysis matrixes
	 * 
	 * if anything went wrong on database side
	 */
	public List<AnalysisMatrix> getAllAnalysisMatrixes(Integer report) {
		try (SelectConditionStep<Record3<Integer, String, EnumAccesslevel>> sql = getJooq()
		// @formatter:off
			.select(T_REPORT.PK, 
					    T_REPORT.NAME, 
					    V_PRIVILEGE.ACL)
			.from(T_REPORT)
			.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(T_REPORT.FK_VARIABLEGROUP))
			.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE)).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()))
			.where(T_REPORT.PK.eq(report == null ? T_REPORT.PK : DSL.value(report, Integer.class)))
			.and(V_PRIVILEGE.EXECUTE);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<AnalysisMatrix> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				EnumAccesslevel aclEnum = r.get(V_PRIVILEGE.ACL);
				AclBean acl = new AclBean(aclEnum);
				if (acl.getExecute()) {
					AnalysisMatrix bean = new AnalysisMatrix(r.get(T_REPORT.NAME));
					bean.setId(r.get(T_REPORT.PK));
					list.add(bean);
				}
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all templates
	 *
	 * @return the list of all templates
	 * 
	 * if anything went wrong on database side
	 */
	public List<FunctionselectionBean> getAllTemplates() {
		try (SelectSeekStep1<Record4<Integer, String, String, Integer>, String> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTIONLIST.PK, 
							T_FUNCTIONLIST.NAME, 
							T_FUNCTIONLIST.DESCRIPTION, 
							T_FUNCTIONLIST.FK_PARENT)
			.from(T_FUNCTIONLIST)
			.orderBy(T_FUNCTIONLIST.NAME);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<FunctionselectionBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				FunctionselectionBean bean = new FunctionselectionBean(r.get(T_FUNCTIONLIST.PK));
				bean.setName(r.get(T_FUNCTIONLIST.NAME));
				bean.setDescription(r.get(T_FUNCTIONLIST.DESCRIPTION));
				bean.setIsAbstract(r.get(T_FUNCTIONLIST.FK_PARENT) == null);
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all templates in a filterable way
	 * 
	 * @return filterable list interface of analysis templates
	 * 
	 * if anything went wrong on database side
	 */
	public FilterableListInterface getAllTemplatesFilterable() {
		FilterableListInterface list = new FilterableList();
		for (FunctionselectionBean bean : getAllTemplates()) {
			list.add(new KeyValueBeanWrapper(bean.getFkFunctionlist(), bean.getName()));
		}
		return list;
	}

	/**
	 * create analysisTemplate named by name and return new function list pk
	 * 
	 * @param bean the template bean with name and description
	 * 
	 * @return new id
	 */
	public Integer createTemplate(FunctionselectionBean bean) {
		if (checkFunctionlistNameExists(bean.getName()) > 0) {
			throw new DataAccessException(getFacesContext().translate("error.name.exists", bean.getName()));
		}
		try (InsertResultStep<TFunctionlistRecord> sql = getJooq()
		// @formatter:off
			.insertInto(T_FUNCTIONLIST, 
									T_FUNCTIONLIST.NAME, 
									T_FUNCTIONLIST.DESCRIPTION)
			.values(bean.getName(), bean.getDescription())
			.returning(T_FUNCTIONLIST.PK);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.fetchOne().getPk();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	private Integer checkFunctionlistNameExists(FunctionselectionBean bean) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record1<Integer>> sql = jooq
			// @formatter:off
				.select(T_FUNCTIONLIST.PK)
				.from(T_FUNCTIONLIST)
				.where(T_FUNCTIONLIST.NAME.eq(bean.getName()))
				.and(T_FUNCTIONLIST.PK.ne(bean.getFkFunctionlist()));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			return sql.fetch().size();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	private Integer checkFunctionlistNameExists(String name) {
		try (CloseableDSLContext jooq = getJooq()) {
			return jooq.fetchCount(T_FUNCTIONLIST, T_FUNCTIONLIST.NAME.eq(name));
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add function to template
	 * 
	 * @param fkFunctionlist the function list id
	 * @param functionId the function id
	 * @param title the title of the anaparfun
	 * @param orderNr order number for the analysis matrix
	 * @param functionName the name of the function (to be used for the acronym)
	 */
	public void addFunctionToTemplate(Integer fkFunctionlist, Integer functionId, String title, Integer orderNr, String functionName) {
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				List<FunctionInputBean> inputs = new StatisticGateway(getFacesContext()).getAllFunctionInputs(functionId);
				SelectConditionStep<Record1<Integer>> sql = DSL.using(c)
				// @formatter:off
					.selectCount()
					.from(T_MATRIXCOLUMN)
					.where(T_MATRIXCOLUMN.FK_FUNCTION.eq(functionId))
					.and(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(fkFunctionlist));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				StringBuilder nameBuf = new StringBuilder(title);
				nameBuf.append("_").append(sql.fetchOne().get(0));
				String acronym = HandsetRuleHelper.generateMatrixColumnAcronym(functionName);
				
				SelectConditionStep<Record1<String>> sql0 = DSL.using(c)
				// @formatter:off
					.select(T_MATRIXCOLUMN.ACRONYM)
					.from(T_MATRIXCOLUMN)
					.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(fkFunctionlist));
				// @formatter:on
				LOGGER.debug("{}", sql0.toString());
				List<String> acronyms = sql0.fetch(T_MATRIXCOLUMN.ACRONYM);
				while (acronyms.contains(acronym)) {
					acronym = acronym + "+";
				}
				
				InsertResultStep<TMatrixcolumnRecord> sql1 = DSL.using(c)
				// @formatter:off
					.insertInto(T_MATRIXCOLUMN, 
											T_MATRIXCOLUMN.FK_FUNCTIONLIST, 
											T_MATRIXCOLUMN.FK_FUNCTION, 
											T_MATRIXCOLUMN.NAME,
											T_MATRIXCOLUMN.ORDER_NR,
											T_MATRIXCOLUMN.ACRONYM)
					.values(fkFunctionlist, functionId, nameBuf.toString(), orderNr, acronym)
					.returning(T_MATRIXCOLUMN.PK);
				// @formatter:on
				LOGGER.debug("{}", sql1.toString());
				TMatrixcolumnRecord rec = sql1.fetchOne();
				Integer nextAnaparfun = rec != null ? rec.getPk() : null;
				if (nextAnaparfun != null) {
					for (FunctionInputBean fib : inputs) {
						String standard = fib.getStandard();
						InsertValuesStep4<TMatrixcolumnparameterRecord, Integer, Integer, JSONB, EnumAnadef> sql2 = DSL.using(c)
						// @formatter:off
							.insertInto(T_MATRIXCOLUMNPARAMETER, 
													T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN, 
													T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT, 
													T_MATRIXCOLUMNPARAMETER.VALUE,
													T_MATRIXCOLUMNPARAMETER.FIXED)
							.values(nextAnaparfun, fib.getPk(), standard == null ? null : JSONB.jsonb(standard), EnumAnadef.matrix);
						// @formatter:on
						LOGGER.debug("{}", sql2.toString());
						sql2.execute();
					}
				}
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * remove function from template
	 * 
	 * @param anaparfun the analysis function id
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer removeFunctionFromTemplate(Integer anaparfun) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				DeleteConditionStep<TMatrixelementparameterRecord> sql = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTPARAMETER)
					.where(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE.in(DSL.using(c)
						.select(T_MATRIXELEMENTVALUE.PK)
						.from(T_MATRIXELEMENTVALUE)
						.where(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.eq(anaparfun))));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				DeleteConditionStep<TMatrixelementvalueRecord> sql2 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTVALUE)
					.where(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.eq(anaparfun));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());

				DeleteConditionStep<TMatrixcolumnparameterRecord> sql3 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXCOLUMNPARAMETER)
					.where(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN.eq(anaparfun));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				lrw.addToInteger(sql3.execute());

				DeleteConditionStep<TMatrixcolumnexcludescaleRecord> sql4 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXCOLUMNEXCLUDESCALE)
					.where(T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN.eq(anaparfun));
				// @formatter:on
				LOGGER.debug("{}", sql4.toString());
				lrw.addToInteger(sql4.execute());

				DeleteConditionStep<TSnippletRecord> sql5 = DSL.using(c)
				// @formatter:off
        	.deleteFrom(T_SNIPPLET)
        	.where(T_SNIPPLET.FK_MATRIXCOLUMN.eq(anaparfun));
        // @formatter:on
				LOGGER.debug("{}", sql5.toString());
				lrw.addToInteger(sql5.execute());

				DeleteConditionStep<TMatrixcolumnRecord> sql6 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXCOLUMN)
					.where(T_MATRIXCOLUMN.PK.eq(anaparfun));
				// @formatter:on
				LOGGER.debug("{}", sql6.toString());
				lrw.addToInteger(sql6.execute());
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all analysis matrix beans for user referenced by usnr
	 * 
	 * @return the list of the analysis matrix beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<AnalysisMatrixBean> getAnalysismatrixBeans() {
		try (SelectConditionStep<Record4<Integer, String, LocalDateTime, EnumAccesslevel>> sql = getJooq()
		// @formatter:off
			.select(T_REPORT.PK, 
							T_REPORT.NAME,
							T_REPORT.LASTCHANGE,
							V_PRIVILEGE.ACL)
			.from(T_REPORT)
			.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(T_REPORT.FK_VARIABLEGROUP))
			.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE)).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()))
			.where(T_REPORT.FK_VARIABLEGROUP.isNotNull()); // do not use report templates
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<AnalysisMatrixBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer anamat = r.get(T_REPORT.PK);
				String name = r.get(T_REPORT.NAME);
				LocalDateTime ldt = r.get(T_REPORT.LASTCHANGE);
				Timestamp lastchange = ldt == null ? null : Timestamp.valueOf(ldt);
				EnumAccesslevel enumAcl = r.get(V_PRIVILEGE.ACL);
				AclBean acl = new AclBean(enumAcl);
				if (acl.getExecute()) {
					list.add(new AnalysisMatrixBean(anamat, name, lastchange, acl));
				}
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all function beans from database if fkFunctionlist is defined
	 * 
	 * @param reportId id of the report
	 * @return list of function beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<AnalysisFunctionBean> getAllFunctionBeansByMatrix(Integer reportId) {
		try (
				SelectHavingStep<Record13<Integer, String, String, String, String, Integer, String, String, String, Boolean, String, Integer, Integer>> sql = getJooq()
				// @formatter:off
					.select(T_FUNCTION.PK, 
									T_FUNCTION.NAME, 	
									T_FUNCTION.SUMMARY, 
									T_FUNCTION.DESCRIPTION, 
									T_FUNCTION.VIGNETTE,
									T_MATRIXCOLUMN.PK,
									T_MATRIXCOLUMN.NAME,
									T_MATRIXCOLUMN.ACRONYM,
									T_MATRIXCOLUMN.DESCRIPTION,
									T_FUNCTION.VARLIST, 
									T_MATRIXCOLUMN.REQUIRED_EXECUTIONS,
									T_MATRIXCOLUMN.ORDER_NR,
									DSL.count(T_MATRIXELEMENTVALUE.PK).as("c"))
					.from(T_MATRIXCOLUMN)
					.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
					.leftJoin(T_REPORT).on(T_REPORT.FK_FUNCTIONLIST.eq(T_MATRIXCOLUMN.FK_FUNCTIONLIST))
					.leftJoin(T_MATRIXELEMENTVALUE).on(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.eq(T_MATRIXCOLUMN.PK)
					                               .and(T_MATRIXELEMENTVALUE.FK_REPORT.eq(T_REPORT.PK)))
					.where(T_REPORT.PK.eq(reportId))
					.groupBy(T_FUNCTION.PK, 
                  T_FUNCTION.NAME,  
                  T_FUNCTION.SUMMARY, 
                  T_FUNCTION.DESCRIPTION, 
                  T_FUNCTION.VIGNETTE,
                  T_MATRIXCOLUMN.PK,
                  T_MATRIXCOLUMN.NAME,
                  T_MATRIXCOLUMN.DESCRIPTION,
                  T_FUNCTION.VARLIST, 
                  T_MATRIXCOLUMN.REQUIRED_EXECUTIONS,
                  T_MATRIXCOLUMN.ORDER_NR);
				// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<AnalysisFunctionBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer fkAnaparfun = r.get(T_MATRIXCOLUMN.PK);
				Integer fkFunction = r.get(T_FUNCTION.PK);
				String name = r.get(T_FUNCTION.NAME);
				String alias = r.get(T_MATRIXCOLUMN.NAME);
				String summary = r.get(T_FUNCTION.SUMMARY);
				String description = r.get(T_MATRIXCOLUMN.DESCRIPTION);
				List<FunctioncategoryBean> category = new StatisticGateway(getFacesContext()).getFunctioncategories(fkFunction);
				Boolean varlist = r.get(T_FUNCTION.VARLIST);
				String requiredExecutions = r.get(T_MATRIXCOLUMN.REQUIRED_EXECUTIONS);
				Integer orderNr = r.get(T_MATRIXCOLUMN.ORDER_NR);
				String vignette = r.get(T_FUNCTION.VIGNETTE);
				String fktnDescr = r.get(T_FUNCTION.DESCRIPTION);
				Integer affected = r.get("c", Integer.class);
				String acronym = r.get(T_MATRIXCOLUMN.ACRONYM);
				list.add(new AnalysisFunctionBean(fkAnaparfun, fkFunction, name, alias, summary, description, category, varlist,
						requiredExecutions, orderNr, vignette, fktnDescr, affected, acronym));
			}
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all function beans from database if fkFunctionlist is defined
	 * 
	 * @param pk id of the template
	 * @return list of function beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<AnalysisFunctionBean> getAllFunctionBeansByTemplate(Integer pk) {
		try (
				SelectHavingStep<Record12<Integer, String, String, String, String, String, Integer, String, String, Boolean, String, Integer>> sql = getJooq()
				// @formatter:off
					.select(T_FUNCTION.PK, 
									T_FUNCTION.NAME, 
									T_FUNCTION.SUMMARY, 
									T_FUNCTION.DESCRIPTION,
									T_FUNCTION.VIGNETTE,
									T_MATRIXCOLUMN.DESCRIPTION, 
									T_MATRIXCOLUMN.PK,
									T_MATRIXCOLUMN.NAME,
									T_MATRIXCOLUMN.ACRONYM,
									T_FUNCTION.VARLIST, 
									T_MATRIXCOLUMN.REQUIRED_EXECUTIONS,
									T_MATRIXCOLUMN.ORDER_NR)
					.from(T_FUNCTION)
					.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.FK_FUNCTION.eq(T_FUNCTION.PK))
					.leftJoin(T_FUNCTIONLIST).on(T_FUNCTIONLIST.PK.eq(T_MATRIXCOLUMN.FK_FUNCTIONLIST))
					.where(T_FUNCTIONLIST.PK.eq(pk));
				// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<AnalysisFunctionBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer fkAnaparfun = r.get(T_MATRIXCOLUMN.PK);
				Integer fkFunction = r.get(T_FUNCTION.PK);
				String name = r.get(T_FUNCTION.NAME);
				String alias = r.get(T_MATRIXCOLUMN.NAME);
				String summary = r.get(T_FUNCTION.SUMMARY);
				String description = r.get(T_MATRIXCOLUMN.DESCRIPTION);
				List<FunctioncategoryBean> category = new StatisticGateway(getFacesContext()).getFunctioncategories(fkFunction);
				Boolean varlist = r.get(T_FUNCTION.VARLIST);
				String requiredExecutions = r.get(T_MATRIXCOLUMN.REQUIRED_EXECUTIONS);
				Integer orderNr = r.get(T_MATRIXCOLUMN.ORDER_NR);
				String vignette = r.get(T_FUNCTION.VIGNETTE);
				String fktnDescr = r.get(T_FUNCTION.DESCRIPTION);
				String acronym = r.get(T_MATRIXCOLUMN.ACRONYM);
				list.add(new AnalysisFunctionBean(fkAnaparfun, fkFunction, name, alias, summary, description, category, varlist,
						requiredExecutions, orderNr, vignette, fktnDescr, 0, acronym)); // on templates, no selection is allowed
			}
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all analysis function beans that are useable
	 * 
	 * @return list of analysis function beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<FunctionBean> getAllFunctionBeans() {
		try (
				SelectConditionStep<Record8<Integer, String, String, String, Integer, LocalDateTime, Boolean, String>> sql = getJooq()
				// @formatter:off
			.select(T_FUNCTION.PK, 
							T_FUNCTION.NAME, 
							T_FUNCTION.DESCRIPTION, 
							T_FUNCTION.SUMMARY,
							T_FUNCTION.FK_PRIVILEGE, 
							T_FUNCTION.CREATIONDATE, 
							T_FUNCTION.USE_INTERVALS,
							T_FUNCTION.VIGNETTE)
			.from(T_FUNCTION)
			.where(T_FUNCTION.ACTIVATED.eq(true));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<FunctionBean> list = new ArrayList<>();
			StatisticGateway gw = new StatisticGateway(getFacesContext());
			for (Record r : sql.fetch()) {
				Integer functionId = r.get(T_FUNCTION.PK);
				LocalDateTime ldt = r.get(T_FUNCTION.CREATIONDATE);
				Date creationDate = ldt == null ? null : Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
				List<FunctionScale> scales = gw.getAllFunctionscales(functionId);
				FunctionBean bean = new FunctionBean(r.get(T_FUNCTION.FK_PRIVILEGE), creationDate,
						r.get(T_FUNCTION.USE_INTERVALS), scales);
				bean.setFunctionId(functionId);
				bean.setName(r.get(T_FUNCTION.NAME));
				bean.setDescription(r.get(T_FUNCTION.DESCRIPTION));
				bean.setSummary(r.get(T_FUNCTION.SUMMARY));
				bean.setCategory(new StatisticGateway(getFacesContext()).getFunctioncategories(bean.getFunctionId()));
				bean.setVignette(r.get(T_FUNCTION.VIGNETTE));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * remove template if and only if it doesn't contain to any report
	 * 
	 * @param fkFunctionlist the function list id
	 * 
	 * if anything went wrong on database side
	 * @return number of affected database rows
	 */
	public Integer removeTemplate(Integer fkFunctionlist) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				if (DSL.using(c).fetchCount(DSL.selectFrom(T_REPORT).where(T_REPORT.FK_FUNCTIONLIST.eq(fkFunctionlist))) > 0) {
					throw new DataAccessException(getFacesContext().translate("error.analysis.template.remove.matricesexist"));
				} else if (DSL.using(c)
						.fetchCount(DSL.selectFrom(T_FUNCTIONLIST).where(T_FUNCTIONLIST.FK_PARENT.eq(fkFunctionlist))) > 0) {
					StringBuilder buf = new StringBuilder();
					SelectConditionStep<TFunctionlistRecord> sql = DSL.using(c)
					// @formatter:off		
						.selectFrom(T_FUNCTIONLIST)
						.where(T_FUNCTIONLIST.FK_PARENT.eq(fkFunctionlist));
					// @formatter:on
					LOGGER.debug("{}", sql.toString());
					for (String s : sql.fetch(T_FUNCTIONLIST.NAME)) {
						buf.append(s).append(", ");
					}
					String children = buf.toString();
					throw new DataAccessException(getFacesContext().translate("error.analysis.template.remove.isparentof",
							children.substring(0, children.length() - 2))); // loose last comma
				}

				DeleteConditionStep<TReportoutputRecord> sql = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_REPORTOUTPUT)
					.where(T_REPORTOUTPUT.FK_REPORT.in(DSL.using(c)
						.select(T_REPORT.PK)
						.from(T_REPORT)
						.where(T_REPORT.FK_FUNCTIONLIST.eq(fkFunctionlist))
					));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				DeleteConditionStep<TMatrixcolumnparameterRecord> sql1 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXCOLUMNPARAMETER)
					.where(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN.in(DSL.using(c)
						.select(T_MATRIXCOLUMN.PK)
						.from(T_MATRIXCOLUMN)
						.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(fkFunctionlist))));
				// @formatter:on
				LOGGER.debug("{}", sql1.toString());
				lrw.addToInteger(sql1.execute());

				DeleteConditionStep<TMatrixcolumnexcludescaleRecord> sql2 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXCOLUMNEXCLUDESCALE)
					.where(T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN.in(DSL.using(c)
						.select(T_MATRIXCOLUMN.PK)
						.from(T_MATRIXCOLUMN)
						.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(fkFunctionlist))));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());

				DeleteConditionStep<TMatrixelementparameterRecord> sql3 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTPARAMETER)
					.where(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE.in(DSL.using(c)
						.select(T_MATRIXELEMENTVALUE.PK)
						.from(T_MATRIXCOLUMN)
						.leftJoin(T_MATRIXELEMENTVALUE).on(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.eq(T_MATRIXCOLUMN.PK))
						.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(fkFunctionlist))));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				lrw.addToInteger(sql3.execute());

				DeleteConditionStep<TMatrixelementvalueRecord> sql4 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTVALUE)
					.where(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.in(DSL.using(c)
						.select(T_MATRIXCOLUMN.PK)
						.from(T_MATRIXCOLUMN)
						.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(fkFunctionlist))));
				// @formatter:on
				LOGGER.debug("{}", sql4.toString());
				lrw.addToInteger(sql4.execute());

				DeleteConditionStep<TSnippletRecord> sql5 = DSL.using(c)
				// @formatter:off
        	.deleteFrom(T_SNIPPLET)
        	.where(T_SNIPPLET.FK_MATRIXCOLUMN.in(DSL.using(c)
        		.select(T_MATRIXCOLUMN.PK)
        		.from(T_MATRIXCOLUMN)
        		.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(fkFunctionlist))));
        // @formatter:on
				LOGGER.debug("{}", sql5.toString());
				lrw.addToInteger(sql5.execute());

				DeleteConditionStep<TMatrixcolumnRecord> sql6 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXCOLUMN)
					.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(fkFunctionlist));
				// @formatter:on
				LOGGER.debug("{}", sql6.toString());
				lrw.addToInteger(sql6.execute());

				DeleteConditionStep<TFunctionlistRecord> sql7 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTIONLIST)
					.where(T_FUNCTIONLIST.PK.eq(fkFunctionlist));
				// @formatter:on
				LOGGER.debug("{}", sql7.toString());
				lrw.addToInteger(sql7.execute());
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all analysis defaults for anaparfun
	 * 
	 * @param bean analysis function bean
	 * @param ignoreForced if true, do not return entries where
	 * T_MATRIXCOLUMNPARAMETER.forced is true
	 * @return list of analysis defaults
	 * 
	 * if anything went wrong on database side
	 */
	public List<AnalysisDefaultBean> getAllAnalysisDefaults(AnalysisFunctionBean bean, boolean ignoreForced) {
		Integer anaparfun = bean.getAnaparfun();
		Name VALUE = DSL.name("value");
		Name FIXED = DSL.name("fixed");
		Name REFERS_METADATA = DSL.name("refers_metadata");
		try (SelectHavingStep<Record8<String, JSONB, EnumAnadef, Boolean, Integer, String, JSONB, String>> sql = getJooq()
		// @formatter:off
			.selectDistinct(T_FUNCTIONINPUT.INPUT_NAME, 
							        DSL.coalesce(T_MATRIXCOLUMNPARAMETER.VALUE, T_FUNCTIONINPUT.DEFAULT_VALUE).as(VALUE), 
							        DSL.coalesce(T_MATRIXCOLUMNPARAMETER.FIXED, EnumAnadef.matrix).as(FIXED), 
							        DSL.coalesce(T_MATRIXCOLUMNPARAMETER.REFERS_METADATA, T_FUNCTIONINPUT.REFERS_METADATA).as(REFERS_METADATA),
							        T_FUNCTIONINPUT.PK,
							        T_FUNCTIONINPUT.DESCRIPTION,
							        T_FUNCTIONINPUT.TYPE_RESTRICTION, 
							        T_FUNCTIONINPUTTYPE.NAME)
			.from(T_FUNCTIONINPUT)
			.leftJoin(T_FUNCTIONINPUTTYPE).on(T_FUNCTIONINPUTTYPE.PK.eq(T_FUNCTIONINPUT.FK_TYPE))
			.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.FK_FUNCTION.eq(T_FUNCTIONINPUT.FK_FUNCTION))
			.leftJoin(T_MATRIXCOLUMNPARAMETER).on(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN.eq(T_MATRIXCOLUMN.PK)
			                                  .and(T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT.eq(T_FUNCTIONINPUT.PK))
			                                  .and(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN.eq(anaparfun))
			                                  .and(T_MATRIXCOLUMNPARAMETER.FK_REPORT.isNull()))
			.where(T_FUNCTIONINPUT.FK_FUNCTION.eq(bean.getId()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Map<String, AnalysisDefaultBean> map = new HashMap<>();
			for (Record r : sql.fetch()) {
				String typeName = r.get(T_FUNCTIONINPUTTYPE.NAME);
				String inputName = r.get(T_FUNCTIONINPUT.INPUT_NAME);
				Integer pk = r.get(T_FUNCTIONINPUT.PK);
				String description = r.get(T_FUNCTIONINPUT.DESCRIPTION);
				EnumAnadef fixed = r.get(FIXED, EnumAnadef.class);
				Boolean refersMetadata = r.get(REFERS_METADATA, Boolean.class);
				JSONB typeRestriction = r.get(T_FUNCTIONINPUT.TYPE_RESTRICTION);
				Boolean expectUniqueName = ("variable".equals(typeName) || "variable list".equals(typeName)) && !refersMetadata;
				AnalysisDefaultBean defBean = new AnalysisDefaultBean(anaparfun, pk, inputName, description, fixed,
						expectUniqueName, refersMetadata, typeRestriction, typeName);
				JSONB value = r.get(VALUE, JSONB.class);
				defBean.setValue(value == null ? null : value.data());
				boolean addIt = !ignoreForced || !(defBean.getFixed().equals(EnumAnadef.template));
				if (addIt) {
					enrichTypeRestriction(typeName, defBean);
					LOGGER.debug("add anavarpar {} with default value {}", defBean.getName(), defBean.getValue());
					map.put(defBean.getName(), defBean);
				}
			}
			List<AnalysisDefaultBean> list = new ArrayList<>();
			list.addAll(map.values());
			LOGGER.debug("the list of anavarpars contains: {}", list.toString());
			// sort by name to fit to the values from T_MATRIXELEMENTPARAMETER in the matrix
			// overview
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all excluded scales for this matrixcolumn
	 * 
	 * @param matrixcolumn id of the corresponding function in the analysis matrix
	 * @return list of found scale beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<MatrixColumnExcludeScaleBean> getAllAnalysisScales(Integer matrixcolumn) {
		List<MatrixColumnExcludeScaleBean> list = new ArrayList<>();
		try (SelectConditionStep<Record4<String, Integer, Boolean, Integer>> sql = getJooq()
		// @formatter:off
			.select(T_SCALE.NAME,
					    T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE, 
							T_MATRIXCOLUMNEXCLUDESCALE.VALUE,
							T_FUNCTIONSCALE.FK_SCALE)
			.from(T_MATRIXCOLUMNEXCLUDESCALE)
			.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN))
			.leftJoin(T_FUNCTIONSCALE).on(T_FUNCTIONSCALE.FK_FUNCTION.eq(T_MATRIXCOLUMN.FK_FUNCTION).and(T_FUNCTIONSCALE.FK_SCALE.eq(T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE)))
			.leftJoin(T_SCALE).on(T_SCALE.PK.eq(T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE))
			.where(T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN.eq(matrixcolumn));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				Boolean disabled = r.get(T_FUNCTIONSCALE.FK_SCALE) == null;
				MatrixColumnExcludeScaleBean bean = new MatrixColumnExcludeScaleBean(matrixcolumn, disabled);
				TScaleRecord excludeScale = new TScaleRecord();
				excludeScale.setName(r.get(T_SCALE.NAME));
				excludeScale.setPk(r.get(T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE));
				bean.setExcludeScale(excludeScale);
				bean.setValue(r.get(T_MATRIXCOLUMNEXCLUDESCALE.VALUE));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all analysis defaults by anaparfun's function
	 * 
	 * @param anaparfun id of the concrete function in the analysis matrix
	 * @return list of found default beans for that function
	 * 
	 * if anything went wrong on database side
	 */
	public List<AnalysisDefaultBean> getAllAnalysisDefaultsByAnamatThatAreNotFunctions(Integer anaparfun) {
		try (CloseableDSLContext jooq = getJooq()) {
			List<AnalysisDefaultBean> list = new ArrayList<>();
			SelectConditionStep<Record9<Integer, Integer, JSONB, EnumAnadef, Boolean, String, String, JSONB, String>> sql = jooq
			// @formatter:off
				.select(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN, 
								T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT, 
								T_MATRIXCOLUMNPARAMETER.VALUE, 
								T_MATRIXCOLUMNPARAMETER.FIXED,
								T_MATRIXCOLUMNPARAMETER.REFERS_METADATA,
								T_FUNCTIONINPUT.DESCRIPTION,
								T_FUNCTIONINPUT.INPUT_NAME,
								T_FUNCTIONINPUT.TYPE_RESTRICTION,
								T_FUNCTIONINPUTTYPE.NAME)
				.from(T_MATRIXCOLUMNPARAMETER)
				.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN))
				.leftJoin(T_FUNCTIONINPUT).on(T_FUNCTIONINPUT.FK_FUNCTION.eq(T_MATRIXCOLUMN.FK_FUNCTION))
				.leftJoin(T_FUNCTIONINPUTTYPE).on(T_FUNCTIONINPUTTYPE.PK.eq(T_FUNCTIONINPUT.FK_TYPE))
				.where(T_MATRIXCOLUMN.PK.eq(anaparfun))
				.and(T_FUNCTIONINPUT.FK_TYPE.notIn(jooq
					.select(T_FUNCTIONINPUTTYPE.PK)
					.from(T_FUNCTIONINPUTTYPE)))
				.and(T_FUNCTIONINPUT.PK.eq(T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				String typeName = r.get(T_FUNCTIONINPUTTYPE.NAME);
				String inputName = r.get(T_FUNCTIONINPUT.INPUT_NAME);
				Integer pk = r.get(T_FUNCTIONINPUT.PK);
				String description = r.get(T_FUNCTIONINPUT.DESCRIPTION);
				EnumAnadef fixed = r.get(T_MATRIXCOLUMNPARAMETER.FIXED);
				Boolean refersMetadata = r.get(T_MATRIXCOLUMNPARAMETER.REFERS_METADATA);
				JSONB typeRestriction = r.get(T_FUNCTIONINPUT.TYPE_RESTRICTION);
				Boolean expectUniqueName = ("variable".equals(typeName) || "variable list".equals(typeName)) && !refersMetadata;
				Integer fkMatrixcolumn = r.get(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN);
				Integer fkFunctioninput = r.get(T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT);
				AnalysisDefaultBean bean = new AnalysisDefaultBean(fkMatrixcolumn, fkFunctioninput, inputName, description,
						fixed, expectUniqueName, refersMetadata, typeRestriction, typeName);
				enrichTypeRestriction(typeName, bean);
				JSONB value = r.get(T_MATRIXCOLUMNPARAMETER.VALUE);
				bean.setValue(value == null ? null : value.data());
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * fill the levels field of the type restriction to make it usable in the
	 * inputjson dropdown components
	 * 
	 * @param typeName the type name
	 * @param bean that contains the type restriction
	 */
	private void enrichTypeRestriction(String typeName, AnalysisDefaultBean bean) {
		if ("variable roles".equals(typeName)) {
			bean.setTypeRestrictionChild("levels", getCachedVariableUsageNames());
		} else if ("variable attribute".equals(typeName)) {
			bean.setTypeRestrictionChild("levels", getCachedVariableAttributeNames());
		} else if ("variable list".equals(typeName) || "variable".equals(typeName)) {
			bean.setTypeRestrictionChild("levels", getCachedMetadatatypeNames());
		}
	}

	private JsonArray getCachedMetadatatypeNames() {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectSeekStep1<Record1<String>, String> sql = jooq
			// @formatter:off
				.select(T_METADATATYPE.NAME)
				.from(T_METADATATYPE)
				.orderBy(T_METADATATYPE.NAME);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			JsonArray array = new JsonArray();
			for (Record r : sql.fetch()) {
				array.add(r.get(T_METADATATYPE.NAME));
			}
			return array;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	private JsonArray getCachedVariableAttributeNames() {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectSeekStep1<Record1<String>, String> sql = jooq
			// @formatter:off
				.selectDistinct(T_VARIABLEATTRIBUTE.NAME)
				.from(T_VARIABLEATTRIBUTE)
				.orderBy(T_VARIABLEATTRIBUTE.NAME);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			JsonArray array = new JsonArray();
			for (Record r : sql.fetch()) {
				array.add(r.get(T_VARIABLEATTRIBUTE.NAME));
			}
			return array;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	private JsonArray getCachedVariableUsageNames() {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectSeekStep1<Record1<String>, String> sql = jooq
			// @formatter:off
				.selectDistinct(T_VARIABLEUSAGE.NAME)
				.from(T_VARIABLEUSAGE)
				.orderBy(T_VARIABLEUSAGE.NAME);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			JsonArray array = new JsonArray();
			for (Record r : sql.fetch()) {
				array.add(r.get(T_VARIABLEUSAGE.NAME));
			}
			return array;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update all analysis variable parameter beans in database
	 * 
	 * @param list list of updateable analysis variable parameter beans
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer upsertAnavarpars(List<AnalysisVariableParameterBean> list) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				List<String> uniqueNames = getAllUniqueNames(c);
				for (AnalysisVariableParameterBean bean : list) {
					if (bean.getExpectUniqueName() && !checkUniqueNames(uniqueNames, bean.getValue())) {
						getFacesContext().notifyWarning("warn.functioninputtype.variable.isnotuniquename", bean.getValue());
						throw new DataAccessException("wrong unique name"); // implicit rollback
					}
					String value = bean.getValue();
					InsertOnDuplicateSetMoreStep<TMatrixelementparameterRecord> sql = DSL.using(c)
					// @formatter:off
						.insertInto(T_MATRIXELEMENTPARAMETER, 
												T_MATRIXELEMENTPARAMETER.VALUE, 
												T_MATRIXELEMENTPARAMETER.REFERS_METADATA,
												T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE, 
												T_MATRIXELEMENTPARAMETER.FK_FUNCTIONINPUT)
						.values(value == null ? null : JSONB.jsonb(value), bean.getRefersMetadata(), bean.getAnavar(), bean.getFunctioninput())
						.onConflict(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE, T_MATRIXELEMENTPARAMETER.FK_FUNCTIONINPUT)
						.doUpdate()
						.set(T_MATRIXELEMENTPARAMETER.VALUE, value == null ? null : JSONB.jsonb(value))
						.set(T_MATRIXELEMENTPARAMETER.REFERS_METADATA, bean.getRefersMetadata());
					// @formatter:on
					LOGGER.debug("{}", sql.toString());
					lrw.addToInteger(sql.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * check if value contains unique names only
	 * 
	 * @param uniqueNames the list of valid unique names
	 * @param value the unique names to be checked, separated by ,
	 * @return true if no found unique name is not found in uniqueNames, false
	 * otherwise
	 */
	public boolean checkUniqueNames(List<String> uniqueNames, String value) {
		List<String> list = new ArrayList<>();
		if (value == null || value.isEmpty()) { // null or empty might be allowed
			return true;
		} else if (value.contains(",")) {
			String[] s = value.split(",");
			list = new ArrayList<>(Arrays.asList(s));
		} else {
			list.add(value);
		}
		uniqueNames.add(null);
		uniqueNames.add("");
		return uniqueNames.containsAll(list);
	}

	/**
	 * check if value in json format contains unique names only
	 * 
	 * @param uniqueNames the list of valid unique names
	 * @param value the unique names to be checked in json format, e.g.
	 * ["t1.rr_ps1", "t0.rr_ps0"] or "t1.rr_ps1"
	 * @return true if no found unique name is not found in uniqueNames, false
	 * otherwise
	 */
	public boolean checkJsonUniqueNames(List<String> uniqueNames, String value) {
		LOGGER.error("not yet implemented method called");
		return checkUniqueNames(uniqueNames, value); // this is only an invalid workaround
		// TODO: implement, using unit tests
	}

	/**
	 * get analysis matrix from db referenced by anamat
	 * 
	 * @param report id of the report
	 * @return the matrix bean of this report
	 * 
	 * if anything went wrong on database side
	 */
	public MatrixBean getAnalysisMatrix(Integer report) {
		try (SelectConditionStep<Record6<String, String, Integer, Integer, String, String>> sql = getJooq()
		// @formatter:off
			.select(T_REPORT.NAME, 
							T_REPORT.DESCRIPTION, 
							T_REPORT.FK_FUNCTIONLIST, 
							T_REPORT.FK_VARIABLEGROUP,
							T_FUNCTIONLIST.NAME, 
							T_VARIABLEGROUP.NAME)
			.from(T_REPORT)
			.leftJoin(T_FUNCTIONLIST).on(T_FUNCTIONLIST.PK.eq(T_REPORT.FK_FUNCTIONLIST))
			.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(T_REPORT.FK_VARIABLEGROUP))
			.where(T_REPORT.PK.eq(report));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Record r = sql.fetchOne();
			if (r == null) {
				throw new DataAccessException("no such analysis matrix bean found");
			} else {
				FunctionselectionBean bean = new FunctionselectionBean(r.get(T_REPORT.FK_FUNCTIONLIST));
				bean.setName(r.get(T_FUNCTIONLIST.NAME));
				VariablegroupBean vg = new VariablegroupBean(r.get(T_REPORT.FK_VARIABLEGROUP), r.get(T_VARIABLEGROUP.NAME));
				return new MatrixBean(report, r.get(T_REPORT.NAME), r.get(T_REPORT.DESCRIPTION), bean, vg);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update analysis matrix bean, all fields but template, this is done in
	 * updateAnalysisMatrixTemplate
	 * 
	 * @param bean the matrix bean
	 * @return number of affected database rows (should be 1)
	 * 
	 * if anything went wrong on database side
	 */
	public Integer updateAnalysisMatrix(MatrixBean bean) {
		try (UpdateConditionStep<TReportRecord> sql = getJooq()
		// @formatter:off
			.update(T_REPORT)
			.set(T_REPORT.NAME, bean.getName())
			.set(T_REPORT.DESCRIPTION, bean.getDescription())
			.set(T_REPORT.FK_VARIABLEGROUP, bean.getVariablegroup().getId())
			.where(T_REPORT.PK.eq(bean.getReport()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update analysis matrix bean template; if referenced template is a master
	 * template, replace it by a copy
	 * 
	 * @param bean the matrix bean
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer updateAnalysisMatrixTemplate(MatrixBean bean) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				Integer fkFunctionlist = createAnalysisTemplateChild(DSL.using(c), bean.getAnalysisTemplateBean());
				if (fkFunctionlist == null) {
					throw new DataAccessException("copied function list id is null, aborting...");
				}
				UpdateConditionStep<TReportRecord> sql = DSL.using(c)
				// @formatter:off
					.update(T_REPORT)
					.set(T_REPORT.FK_FUNCTIONLIST, fkFunctionlist)
					.where(T_REPORT.PK.eq(bean.getReport()));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * check template referenced by function list id - if it is a master template,
	 * create a copy and return its new id; otherwise, return function list ID
	 * 
	 * @param ctx the jooq context
	 * @param analysisTemplateBean the analysis template bean
	 * @return new functionlist id
	 */
	private Integer createAnalysisTemplateChild(DSLContext ctx, FunctionselectionBean analysisTemplateBean) {
		Integer fkFunctionlist = null;
		SelectConditionStep<Record3<String, String, Integer>> sql = ctx
		// @formatter:off
			.select(T_FUNCTIONLIST.NAME, 
							T_FUNCTIONLIST.DESCRIPTION, 
							T_FUNCTIONLIST.FK_PARENT)
			.from(T_FUNCTIONLIST)
			.where(T_FUNCTIONLIST.PK.eq(analysisTemplateBean.getFkFunctionlist()));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		for (Record r : sql.fetch()) {
			// clone only if this is a master template
			if (r.get(T_FUNCTIONLIST.FK_PARENT) == null) {
				// copy master template to new analysis template
				String newTemplateName = new StringBuilder().append(r.get(T_FUNCTIONLIST.NAME)).append("-copy").toString();
				InsertResultStep<TFunctionlistRecord> sql2 = ctx
				// @formatter:off
					.insertInto(T_FUNCTIONLIST, 
											T_FUNCTIONLIST.NAME, 
											T_FUNCTIONLIST.DESCRIPTION, 
											T_FUNCTIONLIST.FK_PARENT)
					.values(newTemplateName, r.get(T_FUNCTIONLIST.DESCRIPTION), analysisTemplateBean.getFkFunctionlist())
					.returning(T_FUNCTIONLIST.PK);
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				fkFunctionlist = sql2.fetchOne().getPk();
				SelectConditionStep<Record4<Integer, Integer, String, Integer>> sql3 = ctx
				// @formatter:off
					.select(T_MATRIXCOLUMN.PK, 
									T_MATRIXCOLUMN.FK_FUNCTION, 
									T_MATRIXCOLUMN.NAME,
									T_MATRIXCOLUMN.ORDER_NR)
					.from(T_MATRIXCOLUMN)
					.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(analysisTemplateBean.getFkFunctionlist()));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				// copy contents of master template to analysis template
				for (Record ri : sql3.fetch()) {
					Integer oldAnaparfun = ri.get(T_MATRIXCOLUMN.PK);

					InsertResultStep<TMatrixcolumnRecord> sql4 = ctx
					// @formatter:off
						.insertInto(T_MATRIXCOLUMN, 
												T_MATRIXCOLUMN.FK_FUNCTIONLIST, 
												T_MATRIXCOLUMN.FK_FUNCTION, 
												T_MATRIXCOLUMN.NAME,
												T_MATRIXCOLUMN.ORDER_NR)
						.values(fkFunctionlist, ri.get(T_MATRIXCOLUMN.FK_FUNCTION), ri.get(T_MATRIXCOLUMN.NAME), ri.get(T_MATRIXCOLUMN.ORDER_NR))
						.returning(T_MATRIXCOLUMN.PK);
					// @formatter:on
					LOGGER.debug("{}", sql4.toString());
					Integer newAnaparfun = sql4.fetchOne().getPk();

					InsertOnDuplicateStep<TMatrixcolumnexcludescaleRecord> sql5 = ctx
					// @formatter:off
						.insertInto(T_MATRIXCOLUMNEXCLUDESCALE, 
												T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN, 
												T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE, 
												T_MATRIXCOLUMNEXCLUDESCALE.VALUE)
						.select(ctx
							.select(DSL.val(newAnaparfun), 
											T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE, 
											T_MATRIXCOLUMNEXCLUDESCALE.VALUE)
							.from(T_MATRIXCOLUMNEXCLUDESCALE)
							.where(T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN.eq(oldAnaparfun)));
					// @formatter:on
					LOGGER.debug("{}", sql5.toString());
					sql5.execute();

					InsertOnDuplicateStep<TMatrixcolumnparameterRecord> sql6 = ctx
					// @formatter:off
						.insertInto(T_MATRIXCOLUMNPARAMETER, 
												T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN, 
												T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT, 
												T_MATRIXCOLUMNPARAMETER.VALUE, 
												T_MATRIXCOLUMNPARAMETER.FIXED)
						.select(ctx
							.select(DSL.val(newAnaparfun), 
											T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT, 
											T_MATRIXCOLUMNPARAMETER.VALUE, 
											T_MATRIXCOLUMNPARAMETER.FIXED)
							.from(T_MATRIXCOLUMNPARAMETER)
							.where(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN.eq(oldAnaparfun)));
					// @formatter:on
					LOGGER.debug("{}", sql6.toString());
					sql6.execute();
				}
			} else {
				// if this analysis template is not a master template, do not clone and return
				// itself
				fkFunctionlist = analysisTemplateBean.getFkFunctionlist();
			}
		}
		return fkFunctionlist;
	}

	/**
	 * copy template referenced by function list id with new name and new
	 * description belonging to usnr
	 * 
	 * @param fkFunctionlist the analysis template id
	 * @param name the name
	 * @param description the description
	 * 
	 * @return the new pk
	 */
	public Integer copyTemplate(Integer fkFunctionlist, String name, String description) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				InsertResultStep<TFunctionlistRecord> sql = DSL.using(c)
				// @formatter:off
				  .insertInto(T_FUNCTIONLIST, 
				  	          T_FUNCTIONLIST.NAME, 
				  	          T_FUNCTIONLIST.DESCRIPTION, 
				  	          T_FUNCTIONLIST.FK_PARENT)
				  .values(name, description, fkFunctionlist)
				  .returning(T_FUNCTIONLIST.PK);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				Integer newAnatemp = sql.fetchOne().getPk();
				lrw.setInteger(newAnatemp);

				SelectConditionStep<Record4<Integer, Integer, String, Integer>> sql2 = DSL.using(c)
				// @formatter:off
				  .select(T_MATRIXCOLUMN.PK, 
				  		    T_MATRIXCOLUMN.FK_FUNCTION, 
				  		    T_MATRIXCOLUMN.NAME,
				  		    T_MATRIXCOLUMN.ORDER_NR)
				  .from(T_MATRIXCOLUMN)
				  .where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(fkFunctionlist));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				for (Record r : sql2.fetch()) {
					Integer oldAnaparfun = r.get(T_MATRIXCOLUMN.PK);
					Integer oldFunctionId = r.get(T_MATRIXCOLUMN.FK_FUNCTION);
					String oldName = r.get(T_MATRIXCOLUMN.NAME);
					Integer orderNr = r.get(T_MATRIXCOLUMN.ORDER_NR);

					InsertResultStep<TMatrixcolumnRecord> sql3 = DSL.using(c)
					// @formatter:off
					  .insertInto(T_MATRIXCOLUMN, 
					  		        T_MATRIXCOLUMN.FK_FUNCTIONLIST, 
					  		        T_MATRIXCOLUMN.FK_FUNCTION, 
					  		        T_MATRIXCOLUMN.NAME,
					  		        T_MATRIXCOLUMN.ORDER_NR)
					  .values(newAnatemp, oldFunctionId, oldName, orderNr)
					  .returning(T_MATRIXCOLUMN.PK);
					// @formatter:on
					LOGGER.debug("{}", sql3.toString());
					Integer newAnaparfun = sql3.fetchOne().getPk();

					// TODO: remove, as the select can handle the missing lines
					InsertOnDuplicateStep<TMatrixcolumnparameterRecord> sql4 = DSL.using(c)
					// @formatter:off
						.insertInto(T_MATRIXCOLUMNPARAMETER, 
							        	T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN, 
							        	T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT, 
							        	T_MATRIXCOLUMNPARAMETER.VALUE, 
							        	T_MATRIXCOLUMNPARAMETER.FIXED)
						.select(DSL.using(c)
							.select(DSL.val(newAnaparfun), 
								    	T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT, 
								    	T_MATRIXCOLUMNPARAMETER.VALUE, 
								    	T_MATRIXCOLUMNPARAMETER.FIXED)
							.from(T_MATRIXCOLUMNPARAMETER)
							.where(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN.eq(oldAnaparfun)));
					// @formatter:on
					LOGGER.debug("{}", sql4.toString());
					sql4.execute();

					InsertOnDuplicateStep<TMatrixcolumnexcludescaleRecord> sql5 = DSL.using(c)
					// @formatter:off
					  .insertInto(T_MATRIXCOLUMNEXCLUDESCALE, 
					  		        T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN, 
					  		        T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE, 
					  		        T_MATRIXCOLUMNEXCLUDESCALE.VALUE)
					  .select(DSL.using(c)
					  	.select(DSL.val(newAnaparfun), 
					  					T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE, 
					  					T_MATRIXCOLUMNEXCLUDESCALE.VALUE)
							.from(T_MATRIXCOLUMNEXCLUDESCALE)
							.where(T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN.eq(oldAnaparfun)));
					// @formatter:on
					LOGGER.debug("{}", sql5.toString());
					sql5.execute();
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all variable groups the current user has usage rights on
	 * 
	 * @param requireRead if true, the user must have read privileges on the
	 * variable group
	 * @param requireWrite if write, the user must have write privileges on the
	 * variable group
	 * @param requireUse if execute, the user must have use privileges on the
	 * variable group
	 * 
	 * @return list of all variable groups
	 * 
	 * if anything went wrong on database side
	 */
	public List<VariablegroupBean> getAllVariablegroups(boolean requireRead, boolean requireWrite, boolean requireUse) {
		try (SelectHavingStep<Record3<String, String, Integer>> sql = getJooq()
		// @formatter:off
			.selectDistinct(T_VARIABLEGROUP.NAME, 
							        T_VARIABLEGROUP.DESCRIPTION, 
							        T_VARIABLEGROUP.PK)
			.from(T_VARIABLEGROUP)
			.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr())))
			.where(requireRead ? V_PRIVILEGE.READ.eq(true) : V_PRIVILEGE.READ.eq(V_PRIVILEGE.READ))
			.and(requireWrite ? V_PRIVILEGE.WRITE.eq(true) : V_PRIVILEGE.WRITE.eq(V_PRIVILEGE.WRITE))
			.and(requireUse ? V_PRIVILEGE.EXECUTE.eq(true) : V_PRIVILEGE.EXECUTE.eq(V_PRIVILEGE.EXECUTE))
			.groupBy(T_VARIABLEGROUP.NAME, T_VARIABLEGROUP.PK);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<VariablegroupBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_VARIABLEGROUP.PK);
				StringBuilder buf = new StringBuilder().append(r.get(T_VARIABLEGROUP.NAME)).append(" (");
				buf.append(r.get(T_VARIABLEGROUP.DESCRIPTION)).append(")");
				VariablegroupBean bean = new VariablegroupBean(pk, buf.toString());
				list.add(bean);
			}
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all analysis function outputs for analysis matrix referenced by report;
	 * the id of the resulting bean contains the function output and the anaparfun,
	 * the name a combination of the anaparfun's name and the function output name
	 * 
	 * @param report id or report
	 * @return analysis functions
	 * 
	 * if anything went wrong on database side
	 */
	public List<AnaparfunOutputBean> getAllAnalysisFunctionOutputs(Integer report) {
		try (SelectConditionStep<Record6<Integer, String, Integer, Integer, String, String>> sql = getJooq()
		// @formatter:off
			.select(T_MATRIXCOLUMN.PK, 
							T_MATRIXCOLUMN.NAME,
							T_MATRIXCOLUMN.ORDER_NR,
							T_FUNCTIONOUTPUT.PK, 
							T_FUNCTIONOUTPUT.NAME,
							T_FUNCTIONOUTPUT.DESCRIPTION)
			.from(T_REPORT)
			.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(T_REPORT.FK_FUNCTIONLIST))
			.leftJoin(T_FUNCTIONOUTPUT).on(T_FUNCTIONOUTPUT.FK_FUNCTION.eq(T_MATRIXCOLUMN.FK_FUNCTION))
			.where(T_REPORT.PK.eq(report));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<AnaparfunOutputBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer fkAnaparfun = r.get(T_MATRIXCOLUMN.PK);
				Integer fkFunctionoutput = r.get(T_FUNCTIONOUTPUT.PK);
				Integer orderNr = r.get(T_MATRIXCOLUMN.ORDER_NR);
				String anaparfunName = r.get(T_MATRIXCOLUMN.NAME);
				String functionoutputName = r.get(T_FUNCTIONOUTPUT.NAME);
				String functionoutputDescription = r.get(T_FUNCTIONOUTPUT.DESCRIPTION);

				AnaparfunOutputBean bean = new AnaparfunOutputBean(fkAnaparfun, fkFunctionoutput, anaparfunName,
						(functionoutputDescription == null || functionoutputDescription.isEmpty()) ? functionoutputName
								: functionoutputDescription,
						orderNr);
				list.add(bean);
				list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all analysis function outputs for analysis matrix referenced by report;
	 * the id of the resulting bean contains the function output and the anaparfun,
	 * the name a combination of the anaparfun's name and the function output name
	 * 
	 * @param report id or report
	 * @return analysis functions
	 */
	public List<MatrixcolumnParameterBean> getMatrixcolumnParameterBeans(Integer report) {
		try (CloseableDSLContext jooq = getJooq()) {
			final List<MatrixcolumnParameterBean> result = new ArrayList<>();
			jooq.transaction(c -> {
				SelectSeekStep1<Record4<Integer, String, String, String>, Integer> sql = DSL.using(c)
				// @formatter:off
					.select(T_MATRIXCOLUMN.PK, 
									T_MATRIXCOLUMN.NAME,
									T_MATRIXCOLUMN.DESCRIPTION,
									T_FUNCTION.VIGNETTE)
					.from(T_REPORT)
					.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(T_REPORT.FK_FUNCTIONLIST))
					.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
					.where(T_REPORT.PK.eq(report))
					.orderBy(T_MATRIXCOLUMN.ORDER_NR);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				List<Integer> fkMatrixcolumns = new ArrayList<>();
				List<MatrixcolumnParameterBean> list = new ArrayList<>();
				for (Record r : sql.fetch()) {
					Integer fkMatrixcolumn = r.get(T_MATRIXCOLUMN.PK);
					String name = r.get(T_MATRIXCOLUMN.NAME);
					String description = r.get(T_MATRIXCOLUMN.DESCRIPTION);
					String vignette = r.get(T_FUNCTION.VIGNETTE);
					fkMatrixcolumns.add(fkMatrixcolumn);
					list.add(new MatrixcolumnParameterBean(fkMatrixcolumn, name, description, vignette));
				}

				SelectSeekStep2<Record4<Integer, String, String, Integer>, Integer, String> sql2 = DSL.using(c)
				// @formatter:off
			  	.select(T_FUNCTIONOUTPUT.PK, 
			  					T_FUNCTIONOUTPUT.NAME,
			  					T_FUNCTIONOUTPUT.DESCRIPTION,
			  					T_MATRIXCOLUMN.PK)
			  	.from(T_MATRIXCOLUMN)
					.leftJoin(T_FUNCTIONOUTPUT).on(T_FUNCTIONOUTPUT.FK_FUNCTION.eq(T_MATRIXCOLUMN.FK_FUNCTION))
					.where(T_MATRIXCOLUMN.PK.in(fkMatrixcolumns))
					.and(T_FUNCTIONOUTPUT.NAME.notLike("%StudyData"))
					.orderBy(T_MATRIXCOLUMN.ORDER_NR, T_FUNCTIONOUTPUT.NAME);
			  // @formatter:on
				LOGGER.debug("{}", sql2.toString());
				Map<Integer, List<FunctionoutputBean>> map = new HashMap<>();
				for (Record r : sql2.fetch()) {
					Integer fkFunctionoutput = r.get(T_FUNCTIONOUTPUT.PK);
					String name = r.get(T_FUNCTIONOUTPUT.NAME);
					String description = r.get(T_FUNCTIONOUTPUT.DESCRIPTION);
					String finalName = (description == null || description.isEmpty()) ? name : description;
					Integer fkMatrixcolumn = r.get(T_MATRIXCOLUMN.PK);
					List<FunctionoutputBean> foBeans = map.get(fkMatrixcolumn);
					if (foBeans == null) {
						foBeans = new ArrayList<>();
						map.put(fkMatrixcolumn, foBeans);
					}
					foBeans.add(new FunctionoutputBean(fkFunctionoutput, finalName));
				}
				for (MatrixcolumnParameterBean b : list) {
					List<FunctionoutputBean> foBeans = map.get(b.getFkMatrixcolumn());
					if (foBeans != null) {
						foBeans.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
						b.getFunctionoutputs().addAll(foBeans);
					}
				}
				result.addAll(list);
			});
			return result;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * load analysis template from db referenced by function list id
	 * 
	 * @param pk id of the analysis template
	 * @return the analysis template if found
	 * 
	 * if anything went wrong on database side
	 */
	public FunctionselectionBean getAnalysisTemplateBean(Integer pk) {
		try (SelectConditionStep<Record2<String, String>> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTIONLIST.NAME, 
							T_FUNCTIONLIST.DESCRIPTION)
			.from(T_FUNCTIONLIST)
			.where(T_FUNCTIONLIST.PK.eq(pk));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Record r = sql.fetchOne();
			if (r == null) {
				throw new DataAccessException("no such template found with T_FUNCTIONLIST.pk = " + pk);
			} else {
				FunctionselectionBean bean = new FunctionselectionBean(pk);
				bean.setName(r.get(T_FUNCTIONLIST.NAME));
				bean.setDescription(r.get(T_FUNCTIONLIST.DESCRIPTION));
				// there can be only one as T_FUNCTIONLIST.pk is a primary key and therefore
				// unique
				return bean;
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update template bean in db referenced by function list id
	 * 
	 * @param bean to be used for update; reference is function list id
	 * @return number of affected database rows, should be 1
	 */
	public Integer updateTemplateDefinition(FunctionselectionBean bean) {
		if (checkFunctionlistNameExists(bean) > 0) {
			throw new DataAccessException(getFacesContext().translate("error.name.exists", bean.getName()));
		}
		try (UpdateConditionStep<TFunctionlistRecord> sql = getJooq()
		// @formatter:off
			.update(T_FUNCTIONLIST)
			.set(T_FUNCTIONLIST.NAME, bean.getName())
			.set(T_FUNCTIONLIST.DESCRIPTION, bean.getDescription())
			.where(T_FUNCTIONLIST.PK.eq(bean.getFkFunctionlist()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get matrix of function to variable references from db<br>
	 * <br>
	 * the result spans a matrix (read linewise) like that:<br>
	 * [null] [function1name(button)] [function2name(button)]
	 * ...[functionNname(button)]<br>
	 * [var1name][function1check(checkbox)] [function2check(checkbox)]
	 * ...[functionNcheck(checkbox)]<br>
	 * [var2name][function1check(checkbox)] [function2check(checkbox)]
	 * ...[functionNcheck(checkbox)]<br>
	 * ... [var3name][function1check(checkbox)] [function2check(checkbox)]
	 * ...[functionNcheck(checkbox)]<br>
	 * 
	 * @param variablegroupId id of the variable group
	 * @param report id of the report
	 * @return list of matrix lines
	 * @deprecated use getJsonMatrix instead
	 */
	@Deprecated
	public List<MatrixLine> getVarlistsMatrix(Integer variablegroupId, Integer report) {
		// all functions of this analysis template (in this analysis matrix)
		List<MatrixFunctionButton> anaFunctions = new ArrayList<>();
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record4<Integer, String, String, Integer>> sql = jooq
			// @formatter:off
				.select(T_MATRIXCOLUMN.PK, 
								T_MATRIXCOLUMN.NAME, 
								T_FUNCTION.DESCRIPTION,
								T_MATRIXCOLUMN.ORDER_NR)
				.from(T_MATRIXCOLUMN)
				.rightJoin(T_REPORT).on(T_REPORT.FK_FUNCTIONLIST.eq(T_MATRIXCOLUMN.FK_FUNCTIONLIST))
				.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
				.where(T_REPORT.PK.eq(report))
				.and(T_MATRIXCOLUMN.PK.isNotNull());
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				anaFunctions.add(new MatrixFunctionButton(r.get(T_MATRIXCOLUMN.PK), r.get(T_MATRIXCOLUMN.NAME),
						r.get(T_FUNCTION.DESCRIPTION), r.get(T_MATRIXCOLUMN.ORDER_NR)));
			}
			anaFunctions.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			List<Integer> anaparfunPks = new ArrayList<>();
			for (MatrixFunctionButton f : anaFunctions) {
				anaparfunPks.add(Integer.valueOf(f.getId()));
			}
			SelectConditionStep<Record3<Integer, String, Integer>> sql2 = jooq
			// @formatter:off
				.select(T_FUNCTIONSCALE.FK_SCALE, 
						    T_SCALE.NAME,
								T_MATRIXCOLUMN.PK)
				.from(T_FUNCTIONSCALE)
				.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.FK_FUNCTION.eq(T_FUNCTIONSCALE.FK_FUNCTION))
				.leftJoin(T_SCALE).on(T_SCALE.PK.eq(T_FUNCTIONSCALE.FK_SCALE))
				.where(T_MATRIXCOLUMN.PK.in(anaparfunPks));
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());
			for (Record r : sql2.fetch()) {
				Integer fId = r.get(T_MATRIXCOLUMN.PK);
				String name = r.get(T_SCALE.NAME);
				for (MatrixFunctionButton f : anaFunctions) {
					if (f.getId().equals(fId.toString())) {
						f.getScales().add(name);
					}
				}
			}
			List<MatrixVarlistsVariableLabel> variables = new ArrayList<>(); // all variables of this variable group
			Name VARLABEL = DSL.name("varlabel");
			SelectConditionStep<Record8<Integer, String, String, Integer, JSONB, String, String, String>> sql3 = jooq
			// @formatter:off
				.select(T_ELEMENT.PK, 
								T_ELEMENT.NAME, 
								T_SCALE.NAME, 
								T_VARIABLEGROUPELEMENT.VARORDER,
								T_ELEMENT.TRANSLATION, 
								T_ELEMENTSHIP.DN, 
								T_VARIABLE.VALUELIST, 
								T_VARIABLEATTRIBUTE.VALUE.as(VARLABEL))
				.from(T_VARIABLEGROUPELEMENT)
				.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
				.leftJoin(T_ELEMENTSHIP).on(T_ELEMENTSHIP.FK_ELEMENT.eq(T_ELEMENT.PK))
				.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
				.leftJoin(T_VARIABLEATTRIBUTE).on(T_VARIABLEATTRIBUTE.FK_VARIABLE.eq(T_VARIABLE.FK_ELEMENT).and(T_VARIABLEATTRIBUTE.NAME.equalIgnoreCase("varlabel")))
				.leftJoin(T_SCALE).on(T_SCALE.PK.eq(T_VARIABLE.FK_SCALE))
				.where(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(variablegroupId));
			// @formatter:on
			LOGGER.debug("{}", sql3.toString());
			for (Record r : sql3.fetch()) {
				Integer id = r.get(T_ELEMENT.PK);
				StringBuilder name = new StringBuilder();
				// definition by
				// https://shiptask.medizin.uni-greifswald.de/SQUARE/Square2/issues/121#note_8603
				name.append(r.get(T_ELEMENT.NAME)).append(" | ");
				name.append(r.get(VARLABEL));
				String scaleName = r.get(T_SCALE.NAME);
				Integer varorder = r.get(T_VARIABLEGROUPELEMENT.VARORDER);
				JSONB translationJson = r.get(T_ELEMENT.TRANSLATION);
				String translation = translationJson == null ? null
						: getFacesContext().selectTranslation(translationJson.data());
				String valuelist = new ValuelistConverter().fromJson(getFacesContext().getIsocode(),
						r.get(T_VARIABLE.VALUELIST));
				VariableBean variableBean = new VariableBean(r.get(T_ELEMENT.NAME), translation, valuelist);
				variableBean.setDn(r.get(T_ELEMENTSHIP.DN));
				variables.add(new MatrixVarlistsVariableLabel(id, scaleName == null ? null : scaleName, name.toString(),
						varorder, translation, variableBean));
			}
			variables.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			Map<Integer, List<Integer>> varFunctions = new HashMap<>();
			for (MatrixVarlistsVariableLabel vars : variables) {
				varFunctions.put(vars.getId(), new ArrayList<>());
			}
			SelectConditionStep<Record2<Integer, Integer>> sql4 = jooq
			// @formatter:off
				.select(T_VARIABLEGROUPELEMENT.FK_ELEMENT, 
								T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN)
				.from(T_MATRIXELEMENTVALUE)
				.leftJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.PK.eq(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT))
				.where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(report));
			// @formatter:on
			LOGGER.debug("{}", sql4.toString());
			for (Record r : sql4.fetch()) {
				Integer keyElement = r.get(T_VARIABLEGROUPELEMENT.FK_ELEMENT);
				Integer anaparfun = r.get(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN);
				try {
					varFunctions.get(keyElement).add(anaparfun);
				} catch (NullPointerException e) {
					getFacesContext().notifyWarning("warning.analysis.matrix.varlist.changed", keyElement);
				}
			}

			List<MatrixLine> list = new ArrayList<>();
			MatrixLine firstLine = new MatrixLine(null);
			List<MatrixFunctionButton> functionsInOrder = new ArrayList<>();
			firstLine.getCells().add(new MatrixTitle("label.analysis.matrix.table.variable")); // top left corner is label
			// "Variables"
			firstLine.getCells().add(new MatrixEmptyCell()); // second cell is empty for auto fill buttons on variable level
			for (MatrixFunctionButton functionButton : anaFunctions) {
				firstLine.getCells().add(functionButton);
				functionsInOrder.add(functionButton);
			}
			list.add(firstLine);
			for (MatrixVarlistsVariableLabel entry : variables) {
				MatrixLine line = new MatrixLine(entry.getVariable());
				line.getCells().add(entry);
				line.getCells().add(new MatrixVariableButton(entry.getId().toString()));
				for (MatrixFunctionButton fktn : functionsInOrder) {
					String id = new StringBuilder("").append(entry.getId()).append(":").append(fktn.getId()).toString();
					Integer fId = null;
					try {
						fId = Integer.valueOf(fktn.getId());
					} catch (NumberFormatException e) {
						getFacesContext().notifyError("there's a NumberFormatException on a function id ({0})", fktn.getId());
					}
					StringBuilder buf = new StringBuilder();
					buf.append(fktn.getName());
					buf.append(" \n ");
					buf.append(entry.getName());
					if (varFunctions.get(entry.getId()).contains(fId)) {
						line.getCells().add(new MatrixCheckbox(id, true, buf.toString()));
					} else {
						if (fktn.getScales().contains(entry.getScale())) {
							line.getCells().add(new MatrixCheckbox(id, false, buf.toString()));
						} else {
							line.getCells().add(new MatrixDisabled(entry.getScale(), fktn.getScales().toString()));
						}
					}
				}
				list.add(line);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update analysis matrix variable list
	 * 
	 * @param list the function variable pair beans
	 * @param report if of analyis matrix
	 * @return number of database operations (a operation on a record set)
	 */
	public Integer updateAnalysisVariables(List<FunctionVariablePairBean> list, Integer report) {
		List<Integer> fkElements = new ArrayList<>();
		for (FunctionVariablePairBean bean : list) {
			fkElements.add(bean.getVariableId());
		}
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				DeleteConditionStep<TMatrixelementparameterRecord> sql = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTPARAMETER)
					.where(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE.in(DSL.using(c)
						.select(T_MATRIXELEMENTVALUE.PK)
						.from(T_MATRIXELEMENTVALUE)
						.leftJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.PK.eq(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT))
						.where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(report))
						.and(T_VARIABLEGROUPELEMENT.FK_ELEMENT.notIn(fkElements))));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				DeleteConditionStep<TMatrixelementvalueRecord> sql2 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTVALUE)
					.where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(report))
					.and(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT.notIn(DSL.using(c)
						.select(T_VARIABLEGROUPELEMENT.PK)
						.from(T_VARIABLEGROUPELEMENT)
						.where(T_VARIABLEGROUPELEMENT.FK_ELEMENT.notIn(fkElements))));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());

				for (FunctionVariablePairBean bean : list) {
					InsertReturningStep<TMatrixelementvalueRecord> sql3 = DSL.using(c)
					// @formatter:off
						.insertInto(T_MATRIXELEMENTVALUE, 
												T_MATRIXELEMENTVALUE.FK_REPORT, 
												T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT, 
												T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN)
						.select(DSL.using(c)
							.select(DSL.val(report, Integer.class), T_VARIABLEGROUPELEMENT.PK, DSL.val(bean.getAnaparfun()))
							.from(T_VARIABLEGROUPELEMENT)
							.leftJoin(T_REPORT).on(T_REPORT.FK_VARIABLEGROUP.eq(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP))
							.where(T_VARIABLEGROUPELEMENT.FK_ELEMENT.eq(bean.getVariableId()))
							.and(T_REPORT.PK.eq(report)))
						.onConflict(T_MATRIXELEMENTVALUE.FK_REPORT, T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN, T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT)
						.doNothing();
					// @formatter:on
					LOGGER.debug("{}", sql3.toString());
					lrw.addToInteger(sql3.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all unique names
	 * 
	 * @param c the transaction lambda configuration of jooq
	 * @return a list of found unique names
	 */
	private List<String> getAllUniqueNames(Configuration c) {
		SelectJoinStep<Record1<String>> sql = DSL.using(c)
		// @formatter:off
			.select(T_ELEMENT.UNIQUE_NAME)
			.from(T_ELEMENT);
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		List<String> list = sql.fetch(T_ELEMENT.UNIQUE_NAME);
		// allow null or empty string to be a valid entry in the list; if so, default
		// values will be used in the end
		list.add(null);
		list.add("");
		return list;
	}

	/**
	 * update T_ANASCALE and T_MATRIXCOLUMNPARAMETER due to its contents
	 * 
	 * @param updateableDefaults the list of updateable defaults
	 * @param updateableScales the list of updateable scales
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer updateAnascaledef(List<AnalysisDefaultBean> updateableDefaults,
			List<MatrixColumnExcludeScaleBean> updateableScales) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				List<String> uniqueNames = getAllUniqueNames(c);
				for (AnalysisDefaultBean bean : updateableDefaults) {
					bean.setRefersMetadata("variable list".equals(bean.getTypeName()) || "variable".equals(bean.getTypeName()));
					if (!bean.getRefersMetadata() && bean.getExpectUniqueName() && !checkJsonUniqueNames(uniqueNames, bean.getValue())) {
						getFacesContext().notifyWarning("warn.functioninputtype.variable.isnotuniquename", bean.getValue());
						throw new DataAccessException("wrong unique name"); // implicit rollback
					}
					JSONB value = bean.getJSONBValue();
					InsertOnDuplicateSetMoreStep<TMatrixcolumnparameterRecord> sql2 = DSL.using(c)
					// @formatter:off
					  .insertInto(T_MATRIXCOLUMNPARAMETER, 
						  					T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN, 
							  				T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT, 
								  			T_MATRIXCOLUMNPARAMETER.VALUE, 
									  		T_MATRIXCOLUMNPARAMETER.FIXED,
									  		T_MATRIXCOLUMNPARAMETER.REFERS_METADATA)
					  .values(bean.getAnaparfun(), bean.getFkFunctioninput(), value, bean.getFixed(), bean.getRefersMetadata())
					  .onConflict(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN, T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT)
					  .where(T_MATRIXCOLUMNPARAMETER.FK_REPORT.isNull())
					  .doUpdate()
					  .set(T_MATRIXCOLUMNPARAMETER.VALUE, value)
					  .set(T_MATRIXCOLUMNPARAMETER.FIXED, bean.getFixed())
					  .set(T_MATRIXCOLUMNPARAMETER.REFERS_METADATA, bean.getRefersMetadata());
					// @formatter:on
					LOGGER.debug("{}", sql2.toString());
					lrw.addToInteger(sql2.execute());
				}
				for (MatrixColumnExcludeScaleBean bean : updateableScales) {
					if (bean.getExcludeScale() != null) {
						InsertOnDuplicateSetMoreStep<TMatrixcolumnexcludescaleRecord> sql2 = DSL.using(c)
						// @formatter:off
							.insertInto(T_MATRIXCOLUMNEXCLUDESCALE, 
													T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN,
													T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE, 
													T_MATRIXCOLUMNEXCLUDESCALE.VALUE)
							.values(bean.getAnaparfun(), bean.getExcludeScale().getPk(), bean.getValue())
							.onConflict(T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN, T_MATRIXCOLUMNEXCLUDESCALE.FK_SCALE)
							.doUpdate()
							.set(T_MATRIXCOLUMNEXCLUDESCALE.VALUE, bean.getValue());
						// @formatter:on
						LOGGER.debug("{}", sql2.toString());
						lrw.addToInteger(sql2.execute());
					}
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all analysis templates
	 * 
	 * @return all analysis templates
	 * 
	 * if anything went wrong on database side
	 */
	public List<AnalysisTemplate> getAllExecuteableAnalysisTemplates() {
		List<AnalysisTemplate> list = new ArrayList<>();
		try (SelectJoinStep<Record2<Integer, String>> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTIONLIST.PK, 
							T_FUNCTIONLIST.NAME)
			.from(T_FUNCTIONLIST);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				AnalysisTemplate bean = new AnalysisTemplate(r.get(T_FUNCTIONLIST.PK));
				bean.setName(r.get(T_FUNCTIONLIST.NAME));
				list.add(bean);
			}
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * ensure correct anavarpar table due to report's function and its anaparfun
	 * entries
	 * 
	 * @param anaparfun the id of the analysis function
	 * @return amount of affected lines in db
	 * 
	 * if anything went wrong on database side
	 */
	public Integer ensureCorrectAnavarparByAnaparfun(Integer anaparfun) {
		try (SelectConditionStep<Record1<Integer>> sql = getJooq()
		// @formatter:off
			.select(T_MATRIXELEMENTVALUE.PK)
			.from(T_MATRIXELEMENTVALUE)
			.where(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.eq(anaparfun));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<Integer> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				list.add(r.get(T_MATRIXELEMENTVALUE.PK));
			}
			return ensureCorrectMatrixElementParameter(list);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * ensure correct anavarpar table for all report functions and its anaparfun
	 * entries of this report
	 * 
	 * @param reportId the id of the report
	 * @return amount of affected lines in db
	 * 
	 * if anything went wrong on database side
	 */
	public Integer ensureCorrectMatrixElementParameterByReport(Integer reportId) {
		try (SelectConditionStep<Record1<Integer>> sql = getJooq()
		// @formatter:off
			.select(T_MATRIXELEMENTVALUE.PK)
			.from(T_MATRIXELEMENTVALUE)
			.where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(reportId));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<Integer> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				list.add(r.get(T_MATRIXELEMENTVALUE.PK));
			}
			return ensureCorrectMatrixElementParameter(list);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * fill out empty anavarpar fields
	 *
	 * @param list list of analysis variables to be checked
	 */
	private Integer ensureCorrectMatrixElementParameter(List<Integer> list) {
		try (CloseableDSLContext jooq = getJooq()) {
			InsertReturningStep<TMatrixelementparameterRecord> sql = jooq
			// @formatter:off
				.insertInto(T_MATRIXELEMENTPARAMETER, 
										T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE, 
										T_MATRIXELEMENTPARAMETER.VALUE,
										T_MATRIXELEMENTPARAMETER.REFERS_METADATA,
										T_MATRIXELEMENTPARAMETER.FK_FUNCTIONINPUT)
				.select(jooq
					.select(T_MATRIXELEMENTVALUE.PK, 
							DSL.val(null, JSONB.class), // see https://gitlab.com/umg_hgw/sq2/square2/issues/597
							DSL.val(false),
							T_FUNCTIONINPUT.PK)
					.from(T_MATRIXELEMENTVALUE)
					.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN))
					.leftJoin(T_FUNCTIONINPUT).on(T_FUNCTIONINPUT.FK_FUNCTION.eq(T_MATRIXCOLUMN.FK_FUNCTION))
					.where(T_MATRIXELEMENTVALUE.PK.in(list)))
				.onConflict(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE, T_MATRIXELEMENTPARAMETER.FK_FUNCTIONINPUT)
				.doNothing();
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all template names of templates that are children of the one referenced
	 * by fksFunctionlist
	 * 
	 * @param fkFunctionlist the id of the function list
	 * @return list of found template names
	 * 
	 * if anything went wrong on database side
	 */
	public List<String> getAllAnatempNamesOfTemplate(Integer fkFunctionlist) {
		try (SelectConditionStep<Record1<String>> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTIONLIST.NAME)
			.from(T_FUNCTIONLIST)
			.where(T_FUNCTIONLIST.FK_PARENT.eq(fkFunctionlist));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<String> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				list.add(r.get(T_FUNCTIONLIST.NAME));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get anaparfun flags from db
	 * 
	 * @param fkAnaparfun correspinding anaparfun pk
	 * @return found anaparfun flags
	 * 
	 * if anything went wrong on database side
	 */
	public AnaparfunFlagBean getAnaparfunFlags(Integer fkAnaparfun) {
		try (SelectConditionStep<Record4<Integer, Boolean, Boolean, Integer>> sql = getJooq()
		// @formatter:off
			.select(T_MATRIXCOLUMN.PK, 
							T_MATRIXCOLUMN.USE_INTERVALS, 
							T_FUNCTION.USE_INTERVALS.as("functionValue"),
							T_MATRIXCOLUMN.ORDER_NR)
			.from(T_MATRIXCOLUMN)
			.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
			.where(T_MATRIXCOLUMN.PK.eq(fkAnaparfun));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Record r = sql.fetchOne(); // must be one because of where condition; if there is no such fkAnaparfun, the
			// web application does not render the button to call this method
			Integer pkAnaparfunflag = r.get(T_MATRIXCOLUMN.PK);
			Boolean useIntervals = r.get(T_MATRIXCOLUMN.USE_INTERVALS);
			Boolean functionValue = r.get(T_FUNCTION.USE_INTERVALS.as("functionValue"));
			Integer orderNr = r.get(T_MATRIXCOLUMN.ORDER_NR);
			AnaparfunFlagBean bean = new AnaparfunFlagBean(fkAnaparfun, useIntervals, functionValue, orderNr);
			// default
			if (!bean.getFunctionValue() || pkAnaparfunflag == null) {
				bean.setUseIntervals(bean.getFunctionValue());
			}
			return bean;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update all given anaparfunFlags in db
	 * 
	 * @param bean to be updated
	 * @return number of affected rows
	 */
	public Integer updateAnaparfunflags(AnaparfunFlagBean bean) {
		try (UpdateConditionStep<TMatrixcolumnRecord> sql = getJooq()
		// @formatter:off
			.update(T_MATRIXCOLUMN)
			.set(T_MATRIXCOLUMN.USE_INTERVALS, bean.getUseIntervals())
			.where(T_MATRIXCOLUMN.PK.eq(bean.getAnaparfun()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all anavarpars of this anaparfun
	 * 
	 * @param anaparfun id of the function
	 * @return list of anavarpars
	 */
	public List<AnavarparBean> getAnavarpars(Integer anaparfun) {
		try (SelectConditionStep<Record3<Integer, JSONB, String>> sql = getJooq()
		// @formatter:off
			.select(T_MATRIXELEMENTPARAMETER.PK, 
							T_MATRIXELEMENTPARAMETER.VALUE, 
							T_FUNCTIONINPUT.INPUT_NAME)
			.from(T_FUNCTIONINPUT)
			.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.FK_FUNCTION.eq(T_FUNCTIONINPUT.FK_FUNCTION))
			.leftJoin(T_MATRIXELEMENTVALUE).on(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.eq(T_MATRIXCOLUMN.PK))
			.leftJoin(T_MATRIXELEMENTPARAMETER).on(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE.eq(T_MATRIXELEMENTVALUE.PK))
			.where(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.eq(anaparfun));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<AnavarparBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_MATRIXELEMENTPARAMETER.PK);
				String name = r.get(T_FUNCTIONINPUT.INPUT_NAME);
				JSONB value = r.get(T_MATRIXELEMENTPARAMETER.VALUE);
				AnavarparBean bean = new AnavarparBean(pk, name, value == null ? null : value.data());
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all anaparfuns for this report
	 * 
	 * @param report id of the report
	 * 
	 * @return list of found anaparfun beans
	 * 
	 * if anything went wrong on database side
	 */
	public List<AnaparfunBean> getAnaparfuns(Integer report) {
		try (SelectConditionStep<Record2<Integer, String>> sql = getJooq()
		// @formatter:off
			.select(T_MATRIXCOLUMN.PK, 
							T_MATRIXCOLUMN.NAME)
			.from(T_REPORT)
			.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(T_REPORT.FK_FUNCTIONLIST))
			.where(T_REPORT.PK.eq(report));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<AnaparfunBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_MATRIXCOLUMN.PK);
				String name = r.get(T_MATRIXCOLUMN.NAME);
				if (pk != null) {
					AnaparfunBean bean = new AnaparfunBean(pk.toString(), name);
					list.add(bean);
				}
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update T_MATRIXCOLUMN, currently only required_executions (as for the other
	 * columns, it does not make sense to change them)
	 * 
	 * @param bean the analysis function bean
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer updateAnaparfun(AnalysisFunctionBean bean) {
		try (UpdateConditionStep<TMatrixcolumnRecord> sql = getJooq()
		// @formatter:off
			.update(T_MATRIXCOLUMN)
			.set(T_MATRIXCOLUMN.REQUIRED_EXECUTIONS, bean.getRequiredExecutions())
			.where(T_MATRIXCOLUMN.PK.eq(bean.getAnaparfun()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update T_MATRIXCOLUMN.name
	 * 
	 * @param bean the bean
	 * @param functionList the function list
	 * @return number of affected database rows
	 * 
	 * if anything went wrong on database side
	 */
	public Integer updateAnaparfunNameAndDescription(AnalysisFunctionBean bean, Integer functionList) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record2<String, String>> sql = jooq
			// @formatter:off
				.select(T_MATRIXCOLUMN.ACRONYM,
								T_MATRIXCOLUMN.NAME)
				.from(T_MATRIXCOLUMN)
				.where(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(functionList))
				.and(T_MATRIXCOLUMN.PK.ne(bean.getAnaparfun()));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			List<String> acronyms = new ArrayList<>();
			List<String> aliases = new ArrayList<>();
			for (Record r : sql.fetch()) {
				acronyms.add(r.get(T_MATRIXCOLUMN.ACRONYM));
				aliases.add(r.get(T_MATRIXCOLUMN.NAME));
			}
			if (acronyms.contains(bean.getAcronym())) {
				// TODO: use an international version of this error
				throw new DataAccessException("acronym " + bean.getAcronym() + " is already in use, please use another one");
			}
			if (aliases.contains(bean.getAlias())) {
				// TODO: use an international version of this error
				throw new DataAccessException("alias " + bean.getName() + " is already in use, please use another one");
			}
			UpdateConditionStep<TMatrixcolumnRecord> sql2 = jooq
			// @formatter:off
				.update(T_MATRIXCOLUMN)
				.set(T_MATRIXCOLUMN.NAME, bean.getAlias())
				.set(T_MATRIXCOLUMN.ACRONYM, bean.getAcronym())
				.set(T_MATRIXCOLUMN.DESCRIPTION, bean.getDescription())
				.where(T_MATRIXCOLUMN.PK.eq(bean.getAnaparfun()));
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());
			return sql2.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * refresh the order of analysis function beans
	 * 
	 * @param list the list
	 * @return number of affected database rows
	 */
	public Integer updateAnaparfunOrder(List<AnalysisFunctionBean> list) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				for (AnalysisFunctionBean bean : list) {
					UpdateConditionStep<TMatrixcolumnRecord> sql = DSL.using(t)
					// @formatter:off
				  	.update(T_MATRIXCOLUMN)
				  	.set(T_MATRIXCOLUMN.ORDER_NR, bean.getOrderNr())
				  	.where(T_MATRIXCOLUMN.PK.eq(bean.getAnaparfun())
				  	.and(T_MATRIXCOLUMN.ORDER_NR.ne(bean.getOrderNr())));
				  // @formatter:on
					LOGGER.debug("{}", sql.toString());
					lrw.addToInteger(sql.execute());
				}
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * @param reportId the report id of this matrix
	 * @return the json matrix bean from db content
	 */
	public JsonMatrixBean getJsonMatrix(Integer reportId) {
		try (CloseableDSLContext jooq = getJooq()) {

			JsonMatrixBean bean = new JsonMatrixBean();
			SelectConditionStep<Record6<Integer, String, String, Integer, Integer, String>> sql = jooq
			// @formatter:off
				.select(T_MATRIXCOLUMN.PK,
						    T_MATRIXCOLUMN.NAME,
						    T_FUNCTION.SUMMARY,
						    T_MATRIXCOLUMN.ORDER_NR,
						    T_FUNCTIONSCALE.FK_SCALE,
						    T_SCALE.NAME)
				.from(T_REPORT)
				.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(T_REPORT.FK_FUNCTIONLIST))
				.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_MATRIXCOLUMN.FK_FUNCTION))
				.leftJoin(T_FUNCTIONSCALE).on(T_FUNCTIONSCALE.FK_FUNCTION.eq(T_MATRIXCOLUMN.FK_FUNCTION))
				.leftJoin(T_SCALE).on(T_SCALE.PK.eq(T_FUNCTIONSCALE.FK_SCALE))
				.where(T_REPORT.PK.eq(reportId));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Map<String, MatrixColumn> cols = new HashMap<>();
			for (Record r : sql.fetch()) {
				String name = r.get(T_MATRIXCOLUMN.NAME);
				String description = r.get(T_FUNCTION.SUMMARY);
				Integer orderNr = r.get(T_MATRIXCOLUMN.ORDER_NR);
				String scale = r.get(T_SCALE.NAME);
				MatrixColumn mc = cols.get(name);
				if (mc == null) {
					mc = new MatrixColumn(name, description, orderNr);
					cols.put(name, mc);
				}
				mc.getScales().add(scale);
			}

			Set<Integer> orderNumbers = new HashSet<>();

			for (MatrixColumn col : cols.values()) {
				if (orderNumbers.contains(col.getOrderNr())) {
					getFacesContext().notifyWarning("warning.report.anamat.ordernrnotunique", col.getOrderNr());
					Integer maximum = Collections.max(orderNumbers);
					col.setOrderNr(maximum + 1000);
				}
				orderNumbers.add(col.getOrderNr());
			}

			List<MatrixColumn> columns = new ArrayList<>();
			columns.addAll(cols.values());
			columns.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			bean.getColumns().addAll(columns);

			SelectConditionStep<Record4<String, String, JSONB, Integer>> sql1 = jooq
			// @formatter:off
				.select(T_ELEMENT.UNIQUE_NAME,
								T_ELEMENT.NAME,
						    T_ELEMENT.TRANSLATION,
						    T_VARIABLEGROUPELEMENT.VARORDER)
				.from(T_REPORT)
				.leftJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(T_REPORT.FK_VARIABLEGROUP))
				.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
				.where(T_REPORT.PK.eq(reportId));
			// @formatter:on
			LOGGER.debug("{}", sql1.toString());
			List<MatrixRow> rows = new ArrayList<>();
			for (Record r : sql1.fetch()) {
				String id = r.get(T_ELEMENT.UNIQUE_NAME);
				JSONB translationJson = r.get(T_ELEMENT.TRANSLATION);
				String description = translationJson == null ? null
						: getFacesContext().selectTranslation(translationJson.data());
				Integer varOrder = r.get(T_VARIABLEGROUPELEMENT.VARORDER);
				String varname = r.get(T_ELEMENT.NAME);
				rows.add(new MatrixRow(id, description, varOrder, varname));
			}
			rows.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			bean.getRows().addAll(rows);

			// create the empty matrix
			SelectConditionStep<Record3<String, String, String>> sql2 = jooq
			// @formatter:off
				.select(T_ELEMENT.UNIQUE_NAME,
								T_SCALE.NAME,
						    T_MATRIXCOLUMN.NAME)
				.from(T_MATRIXCOLUMN)
				.leftJoin(T_REPORT).on(T_REPORT.FK_FUNCTIONLIST.eq(T_MATRIXCOLUMN.FK_FUNCTIONLIST))
				.leftJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP.eq(T_REPORT.FK_VARIABLEGROUP))
				.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
				.leftJoin(T_VARIABLE).on(T_VARIABLE.FK_ELEMENT.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
				.leftJoin(T_SCALE).on(T_SCALE.PK.eq(T_VARIABLE.FK_SCALE))
				.where(T_REPORT.PK.eq(reportId));
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());
			for (Record r : sql2.fetch()) {
				String id = r.get(T_ELEMENT.UNIQUE_NAME);
				String fktn = r.get(T_MATRIXCOLUMN.NAME);
				String scaleName = r.get(T_SCALE.NAME);
				MatrixData data = bean.getDataRow(id);
				if (data == null) {
					data = new MatrixData(id);
					bean.getDataRecords().add(data);
				}
				data.setScale(scaleName);
				data.setFunctionFlag(fktn, false);
			}

			// fill in the matrix content from t_matrixelementvalue
			SelectConditionStep<Record2<String, String>> sql3 = jooq
			// @formatter:off
				.select(T_ELEMENT.UNIQUE_NAME,
								T_MATRIXCOLUMN.NAME)
				.from(T_MATRIXELEMENTVALUE)
				.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN))
				.leftJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.PK.eq(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT))
				.leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
				.where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(reportId));
			// @formatter:on
			LOGGER.debug("{}", sql3.toString());
			for (Record r : sql3.fetch()) {
				String id = r.get(T_ELEMENT.UNIQUE_NAME);
				String fktn = r.get(T_MATRIXCOLUMN.NAME);
				MatrixData md = bean.getDataRow(id);
				if (md != null) {
					md.setFunctionFlag(fktn, true);
				} else {
					LOGGER.debug(
							"no entry found in t_matrixelementvalue for variable {} and function {}, this is not supposed to be so",
							id, fktn);
				}
			}

			bean.nullScaleBreaks();
			return bean;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	private Set<String> getAllUniqueNames(Configuration t, Integer reportId) {
		SelectConditionStep<Record1<String>> sql = DSL.using(t)
		// @formatter:off
      .select(T_ELEMENT.UNIQUE_NAME)
      .from(T_VARIABLEGROUPELEMENT)
      .leftJoin(T_REPORT).on(T_REPORT.FK_VARIABLEGROUP.eq(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP))
      .leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
      .where(T_REPORT.PK.eq(reportId));
    // @formatter:on
		LOGGER.debug("{}", sql.toString());
		Set<String> set = new HashSet<>();
		for (Record r : sql.fetch()) {
			set.add(r.get(T_ELEMENT.UNIQUE_NAME));
		}
		return set;
	}

	private Set<String> getAllColumnNames(Configuration t, Integer reportId) {
		SelectConditionStep<Record1<String>> sql = DSL.using(t)
		// @formatter:off
      .select(T_MATRIXCOLUMN.NAME)
      .from(T_MATRIXCOLUMN)
      .leftJoin(T_REPORT).on(T_REPORT.FK_FUNCTIONLIST.eq(T_MATRIXCOLUMN.FK_FUNCTIONLIST))
      .where(T_REPORT.PK.eq(reportId));
    // @formatter:on
		LOGGER.debug("{}", sql.toString());
		Set<String> set = new HashSet<>();
		for (Record r : sql.fetch()) {
			set.add(r.get(T_MATRIXCOLUMN.NAME));
		}
		return set;
	}

	/**
	 * execute all the ajax requests
	 * 
	 * @param ajaxRequests the ajax requests
	 * @param reportId the id of the report
	 * @return the number of affected datasets; at least 0
	 */
	public Integer handleAjaxRequestBeans(List<AjaxRequestBean> ajaxRequests, Integer reportId) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		lrw.setInteger(0); // if not set to 0, it is NULL for no changes
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				Map<String, Set<String>> allowedCombinations = getAllowedCombinations(t, reportId);
				// TODO: implement usage of allowedCombinations
				Set<String> allUniqueNames = getAllUniqueNames(t, reportId);
				Set<String> allColumnNames = getAllColumnNames(t, reportId);
				ajaxRequests.sort((l1, l2) -> l1 == null ? 0 : l1.compareTo(l2)); // important: the order of the requests must
																																					// not be changed
				for (AjaxRequestBean bean : ajaxRequests) {
					if ("multiple".equals(bean.getCardinality())) {
						if (bean.getUniqueNames().size() < 1) {
							bean.getUniqueNames().addAll(allUniqueNames);
						}
						if (bean.getMatrixcolumnNames().size() < 1) {
							bean.getMatrixcolumnNames().addAll(allColumnNames);
						}
					}
					// TODO: exclude disallowed combinations by checking for allowedCombinations
					// the problem: doing so on lists might disrupt the flexibility, therefore
					// postponed (it's against invalid requests)
					String crud = bean.getCrud();
					if ("insert".equals(crud)) {
						lrw.addToInteger(insertJsonMatrixRequest(t, bean, reportId));
					} else if ("update".equals(crud)) {
						lrw.addToInteger(updateJsonMatrixRequest(t, bean, reportId));
					} else if ("delete".equals(crud)) {
						lrw.addToInteger(deleteJsonMatrixRequest(t, bean, reportId));
					} else {
						throw new DataAccessException(
								new StringBuilder("crud = ").append(crud).append(" is not supported.").toString());
					}
				}
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return lrw.getInteger();
	}

	/**
	 * @deprecated done in the upsert statement
	 * @param t
	 * @param reportId
	 * @return
	 */
	@Deprecated
	private Map<String, Set<String>> getAllowedCombinations(Configuration t, Integer reportId) {
		SelectConditionStep<Record2<String, String>> sql = DSL.using(t)
		// @formatter:off
      .select(R_MATRIXCANDIDATE.MATRIXCOLUMN_NAME, R_MATRIXCANDIDATE.UNIQUE_NAME)
      .from(R_MATRIXCANDIDATE)
      .where(R_MATRIXCANDIDATE.FK_REPORT.eq(reportId))
      .and(R_MATRIXCANDIDATE.EXCLUDED.ne(true));
    // @formatter:on
		LOGGER.debug("{}", sql.toString());
		Map<String, Set<String>> allowed = new HashMap<>();
		for (Record r : sql.fetch()) {
			String mcName = r.get(R_MATRIXCANDIDATE.MATRIXCOLUMN_NAME);
			String uniqueName = r.get(R_MATRIXCANDIDATE.UNIQUE_NAME);
			Set<String> uniqueNames = allowed.get(mcName);
			if (uniqueNames == null) {
				uniqueNames = new HashSet<>();
				allowed.put(mcName, uniqueNames);
			}
			uniqueNames.add(uniqueName);
		}
		return allowed;
	}

	private Integer insertJsonMatrixRequest(Configuration t, AjaxRequestBean bean, Integer reportId) {
		InsertReturningStep<TMatrixelementvalueRecord> sql1 = DSL.using(t)
		// @formatter:off
      .insertInto(T_MATRIXELEMENTVALUE,
                  T_MATRIXELEMENTVALUE.FK_REPORT,
                  T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN,
                  T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT)
      .select(DSL.using(t)
        .select(T_REPORT.PK,
                T_MATRIXCOLUMN.PK,
                T_VARIABLEGROUPELEMENT.PK)
        .from(T_VARIABLEGROUPELEMENT)
        .leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.NAME.in(bean.getMatrixcolumnNames()))
        .leftJoin(T_REPORT).on(T_REPORT.FK_VARIABLEGROUP.eq(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP))
        .leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
        .innerJoin(R_MATRIXCANDIDATE).on(R_MATRIXCANDIDATE.UNIQUE_NAME.eq(T_ELEMENT.UNIQUE_NAME)
                                    .and(R_MATRIXCANDIDATE.MATRIXCOLUMN_NAME.eq(T_MATRIXCOLUMN.NAME)))
        .where(T_ELEMENT.UNIQUE_NAME.in(bean.getUniqueNames()))
        .and(T_REPORT.PK.eq(reportId)))
      .onConflict(T_MATRIXELEMENTVALUE.FK_REPORT, T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN, T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT)
      .doNothing();
    // @formatter:on
		LOGGER.debug("{}", sql1.toString());
		return sql1.execute();
	}

	private Integer upsertJsonMatrixRequestOnElement(Configuration t, Integer reportId, String uniqueName,
			String matrixcolumnName, String functioninputName, String value, Boolean refersMetadata) {
		InsertReturningStep<TMatrixelementparameterRecord> sql = DSL.using(t)
		// @formatter:off
      .insertInto(T_MATRIXELEMENTPARAMETER,
                  T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE,
                  T_MATRIXELEMENTPARAMETER.FK_FUNCTIONINPUT,
                  T_MATRIXELEMENTPARAMETER.VALUE,
                  T_MATRIXELEMENTPARAMETER.REFERS_METADATA)
      .select(DSL.using(t)
        .select(T_MATRIXELEMENTVALUE.PK,
                T_FUNCTIONINPUT.PK,
                DSL.value(value == null ? null : JSONB.jsonb(value), JSONB.class),
                DSL.value(refersMetadata))
        .from(T_ELEMENT)
        .leftJoin(T_REPORT).on(T_REPORT.PK.eq(reportId))
        .leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.NAME.eq(matrixcolumnName)
                                     .and(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(T_REPORT.FK_FUNCTIONLIST)))
        .leftJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.FK_ELEMENT.eq(T_ELEMENT.PK)
                                             .and(T_REPORT.FK_VARIABLEGROUP.eq(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP)))
        .leftJoin(T_MATRIXELEMENTVALUE).on(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT.eq(T_VARIABLEGROUPELEMENT.PK)
                                           .and(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.eq(T_MATRIXCOLUMN.PK)))
        .leftJoin(T_FUNCTIONINPUT).on(T_FUNCTIONINPUT.INPUT_NAME.eq(functioninputName)
                                      .and(T_FUNCTIONINPUT.FK_FUNCTION.eq(T_MATRIXCOLUMN.FK_FUNCTION)))
        .where(T_ELEMENT.UNIQUE_NAME.eq(uniqueName)))
      .onConflict(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE, T_MATRIXELEMENTPARAMETER.FK_FUNCTIONINPUT)
      .doUpdate()
      .set(T_MATRIXELEMENTPARAMETER.VALUE, value == null ? null : JSONB.jsonb(value))
      .set(T_MATRIXELEMENTPARAMETER.REFERS_METADATA, refersMetadata);
    // @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.execute();
	}

	private Integer updateJsonMatrixRequest(Configuration t, AjaxRequestBean bean, Integer reportId) {
		Integer counter = 0;
		if ("element".equals(bean.getTargetTable())) {
			if ("single".equals(bean.getCardinality())) {
				String uniqueName = bean.getUniqueNames().get(0); // must be the first entry because of single
				String matrixcolumnName = bean.getMatrixcolumnNames().get(0); // must be the first entry because of single
				for (String functioninputName : bean.getMatrixElement().getParams().keySet()) {
					ParamBean paramBean = bean.getMatrixElement().getParams().get(functioninputName);
					if (paramBean != null) {
						String value = paramBean.getValue();
						Boolean refersMetadata = paramBean.getRefersMetadata();
						// unchecked == null means false
						refersMetadata = refersMetadata == null ? false : refersMetadata;
						counter += upsertJsonMatrixRequestOnElement(t, reportId, uniqueName, matrixcolumnName, functioninputName,
								value, refersMetadata);
					}
				}
			} else if ("multiple".equals(bean.getCardinality())) {
				for (String uniqueName : bean.getUniqueNames()) {
					for (String matrixcolumnName : bean.getMatrixcolumnNames()) {
						for (String functioninputName : bean.getMatrixElement().getParams().keySet()) {
							ParamBean paramBean = bean.getMatrixElement().getParams().get(functioninputName);
							if (paramBean != null) {
								String value = paramBean.getValue();
								Boolean refersMetadata = paramBean.getRefersMetadata();
								// unchecked == null means false
								refersMetadata = refersMetadata == null ? false : refersMetadata;
								counter += upsertJsonMatrixRequestOnElement(t, reportId, uniqueName, matrixcolumnName,
										functioninputName, value, refersMetadata);
							}
						}
					}
				}
			} else {
				throw new DataAccessException(
						new StringBuilder("unknown cardinality ").append(bean.getCardinality()).toString());
			}
		} else if ("column".equals(bean.getTargetTable())) {
			if ("single".equals(bean.getCardinality())) {
				String mcName = bean.getMatrixcolumnNames().get(0); // single forces to have exactly one value
				if (bean.getMatrixElement().getParams().size() > 0) {
					for (String fiName : bean.getMatrixElement().getParams().keySet()) {
						ParamBean paramBean = bean.getMatrixElement().getParams().get(fiName);
						if (paramBean != null) {
							String value = paramBean.getValue();
							Boolean refersMetadata = paramBean.getRefersMetadata();
							// unchecked == null means false
							refersMetadata = refersMetadata == null ? false : refersMetadata;
							InsertReturningStep<TMatrixcolumnparameterRecord> sql = DSL.using(t)
							// @formatter:off
                .insertInto(T_MATRIXCOLUMNPARAMETER,
                            T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN,
                            T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT,
                            T_MATRIXCOLUMNPARAMETER.VALUE,
                            T_MATRIXCOLUMNPARAMETER.FIXED,
                            T_MATRIXCOLUMNPARAMETER.REFERS_METADATA,
                            T_MATRIXCOLUMNPARAMETER.FK_REPORT)
                .select(DSL.using(t)
                  .select(T_MATRIXCOLUMN.PK,
                          T_FUNCTIONINPUT.PK,
                          DSL.value(value == null ? null : JSONB.jsonb(value), JSONB.class),
                          DSL.value(EnumAnadef.matrix),
                          DSL.value(refersMetadata),
                          DSL.value(reportId))
                  .from(T_REPORT)
                  .leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.NAME.eq(mcName)
                                               .and(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(T_REPORT.FK_FUNCTIONLIST)))
                  .leftJoin(T_FUNCTIONINPUT).on(T_FUNCTIONINPUT.INPUT_NAME.eq(fiName)
                                                .and(T_FUNCTIONINPUT.FK_FUNCTION.eq(T_MATRIXCOLUMN.FK_FUNCTION)))
                  .where(T_REPORT.PK.eq(reportId)))
                .onConflict(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN, T_MATRIXCOLUMNPARAMETER.FK_FUNCTIONINPUT, T_MATRIXCOLUMNPARAMETER.FK_REPORT)
                .where(T_MATRIXCOLUMNPARAMETER.FK_REPORT.isNotNull())
                .doUpdate()
                .set(T_MATRIXCOLUMNPARAMETER.VALUE, value == null ? null : JSONB.jsonb(value))
                .set(T_MATRIXCOLUMNPARAMETER.REFERS_METADATA, refersMetadata);
              // @formatter:on
							LOGGER.debug("{}", sql.toString());
							counter += sql.execute();
						}
					}
				}
			} else {
				throw new DataAccessException("unsupported combination of target_table = column and cardinality = single");
			}
		} else {
			throw new DataAccessException(
					new StringBuilder("unknown target_table value ").append(bean.getTargetTable()).toString());
		}
		return counter;
	}

	private Integer deleteJsonMatrixRequest(Configuration t, AjaxRequestBean bean, Integer reportId) {
		Integer counter = 0;

		DeleteConditionStep<TMatrixelementparameterRecord> sql1 = DSL.using(t)
		// @formatter:off
      .deleteFrom(T_MATRIXELEMENTPARAMETER)
      .where(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE.in(DSL.using(t)
        .select(T_MATRIXELEMENTVALUE.PK)
        .from(T_MATRIXELEMENTVALUE)
        .leftJoin(T_VARIABLEGROUPELEMENT).on(T_VARIABLEGROUPELEMENT.PK.eq(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT))
        .where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(reportId))
        .and(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.in(DSL.using(t)
          .select(T_MATRIXCOLUMN.PK)
          .from(T_MATRIXCOLUMN)
          .where(T_MATRIXCOLUMN.NAME.in(bean.getMatrixcolumnNames()))))
        .and(T_VARIABLEGROUPELEMENT.FK_ELEMENT.in(DSL.using(t)
          .select(T_ELEMENT.PK)
          .from(T_ELEMENT)
          .where(T_ELEMENT.UNIQUE_NAME.in(bean.getUniqueNames()))))));
    // @formatter:on
		LOGGER.debug("{}", sql1);
		counter += sql1.execute();

		DeleteConditionStep<TCalculationresultRecord> sql2 = DSL.using(t)
		// @formatter:off
      .deleteFrom(T_CALCULATIONRESULT)
      .where(T_CALCULATIONRESULT.FK_MATRIXELEMENTVALUE.in(DSL.using(t)
        .select(T_MATRIXELEMENTVALUE.PK)
        .from(T_MATRIXELEMENTVALUE)
        .where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(reportId))
        .and(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.in(DSL.using(t)
          .select(T_MATRIXCOLUMN.PK)
          .from(T_MATRIXCOLUMN)
          .where(T_MATRIXCOLUMN.NAME.in(bean.getMatrixcolumnNames()))))
        .and(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT.in(DSL.using(t)
          .select(T_VARIABLEGROUPELEMENT.PK)
          .from(T_VARIABLEGROUPELEMENT)
          .leftJoin(T_REPORT).on(T_REPORT.FK_VARIABLEGROUP.eq(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP))
          .leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
          .where(T_ELEMENT.UNIQUE_NAME.in(bean.getUniqueNames()))
          .and(T_REPORT.PK.eq(reportId))))));
    // @formatter:on
		LOGGER.debug("{}", sql2);
		counter += sql2.execute();

		DeleteConditionStep<TMatrixelementvalueRecord> sql3 = DSL.using(t)
		// @formatter:off
      .deleteFrom(T_MATRIXELEMENTVALUE)
      .where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(reportId))
      .and(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.in(DSL.using(t)
        .select(T_MATRIXCOLUMN.PK)
        .from(T_MATRIXCOLUMN)
        .where(T_MATRIXCOLUMN.NAME.in(bean.getMatrixcolumnNames()))))
      .and(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT.in(DSL.using(t)
        .select(T_VARIABLEGROUPELEMENT.PK)
        .from(T_VARIABLEGROUPELEMENT)
        .leftJoin(T_REPORT).on(T_REPORT.FK_VARIABLEGROUP.eq(T_VARIABLEGROUPELEMENT.FK_VARIABLEGROUP))
        .leftJoin(T_ELEMENT).on(T_ELEMENT.PK.eq(T_VARIABLEGROUPELEMENT.FK_ELEMENT))
        .where(T_ELEMENT.UNIQUE_NAME.in(bean.getUniqueNames()))
        .and(T_REPORT.PK.eq(reportId))));
    // @formatter:on
		LOGGER.debug("{}", sql3);
		counter += sql3.execute();
		return counter;
	}

	/**
	 * update function params
	 * 
	 * @param afb the anaparfuns
	 * @param flags the anaparfun flags
	 * @param anavarpars the anavarpars
	 * @return number of affected database rows
	 */
	public Integer updateFunctionparams(AnalysisFunctionBean afb, AnaparfunFlagBean flags,
			List<AnalysisVariableParameterBean> anavarpars) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				UpdateConditionStep<TMatrixcolumnRecord> sql = DSL.using(t)
				// @formatter:off
					.update(T_MATRIXCOLUMN)
					.set(T_MATRIXCOLUMN.REQUIRED_EXECUTIONS, afb.getRequiredExecutions())
					.where(T_MATRIXCOLUMN.PK.eq(afb.getAnaparfun()))
					.and(T_MATRIXCOLUMN.REQUIRED_EXECUTIONS.ne(afb.getRequiredExecutions()).or(T_MATRIXCOLUMN.REQUIRED_EXECUTIONS.isNull())); // do update changes only
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				UpdateConditionStep<TMatrixcolumnRecord> sql2 = DSL.using(t)
				// @formatter:off
					.update(T_MATRIXCOLUMN)
					.set(T_MATRIXCOLUMN.USE_INTERVALS, flags.getUseIntervals())
					.where(T_MATRIXCOLUMN.PK.eq(flags.getAnaparfun()))
					.and(T_MATRIXCOLUMN.USE_INTERVALS.ne(flags.getUseIntervals()).or(T_MATRIXCOLUMN.USE_INTERVALS.isNull())); // do update changes only
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());

				List<String> uniqueNames = getAllUniqueNames(t);
				for (AnalysisVariableParameterBean bean : anavarpars) {
					if (bean.getExpectUniqueName() && !checkUniqueNames(uniqueNames, bean.getValue())) {
						getFacesContext().notifyWarning("warn.functioninputtype.variable.isnotuniquename", bean.getValue());
						throw new DataAccessException("wrong unique name"); // implicit rollback
					}
					String value = bean.getValue();
					InsertOnDuplicateSetMoreStep<TMatrixelementparameterRecord> sql3 = DSL.using(t)
					// @formatter:off
						.insertInto(T_MATRIXELEMENTPARAMETER, 
												T_MATRIXELEMENTPARAMETER.VALUE, 
												T_MATRIXELEMENTPARAMETER.REFERS_METADATA,
												T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE, 
												T_MATRIXELEMENTPARAMETER.FK_FUNCTIONINPUT)
						.values(value == null ? null : JSONB.jsonb(value), bean.getRefersMetadata(), bean.getAnavar(), bean.getFunctioninput())
						.onConflict(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE, T_MATRIXELEMENTPARAMETER.FK_FUNCTIONINPUT)
						.doUpdate()
						.set(T_MATRIXELEMENTPARAMETER.VALUE, value == null ? null : JSONB.jsonb(value))
						.set(T_MATRIXELEMENTPARAMETER.REFERS_METADATA, bean.getRefersMetadata());
					// @formatter:on
					LOGGER.debug("{}", sql3.toString());
					lrw.addToInteger(sql3.execute());
				}
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return lrw.getInteger();
	}

	/**
	 * @param pk the id of the report
	 * @return true if there are checkmarks in the analysis matrix, false otherwise
	 */
	public Boolean getAnamatIsEmpty(Integer pk) {
		try (SelectConditionStep<Record1<Integer>> sql = getJooq()
		// @formatter:off
      .select(T_MATRIXELEMENTVALUE.PK)
      .from(T_MATRIXELEMENTVALUE)
      .where(T_MATRIXELEMENTVALUE.FK_REPORT.eq(pk));
    // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.fetch(T_MATRIXELEMENTVALUE.PK).size() < 1;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * @return all category names
	 */
	public List<String> getCategoryNames() {
		try (SelectSeekStep1<Record1<String>, String> sql = getJooq()
		// @formatter:off
      .selectDistinct(T_FUNCTIONCATEGORY.NAME)
      .from(T_FUNCTIONCATEGORY)
      .orderBy(T_FUNCTIONCATEGORY.NAME);
    // @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.fetch(T_FUNCTIONCATEGORY.NAME);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
