package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_CALCPERF;
import static de.ship.dbppsquare.square.Tables.T_FUNCTION;
import static de.ship.dbppsquare.square.Tables.T_MATRIXCOLUMN;
import static de.ship.dbppsquare.square.Tables.T_RELEASE;
import static de.ship.dbppsquare.square.Tables.T_REPORT;
import static de.ship.dbppsquare.square.Tables.T_REPORTDATARELEASE;
import static de.ship.dbppsquare.square.Tables.T_REPORTPERIOD;
import static de.ship.dbppsquare.square.Tables.T_VARIABLEGROUP;
import static de.ship.dbppsquare.square.Tables.V_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.V_REPORT;
import static de.ship.dbppsquare.square.Tables.V_VARIABLEGROUPRELEASE;
import static de.ship.dbppsquare.squareout.Tables.T_CALCULATIONRESULT;
import static de.ship.dbppsquare.squareout.Tables.T_REPORTOUTPUT;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertReturningStep;
import org.jooq.InsertValuesStep2;
import org.jooq.InsertValuesStep3;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Record5;
import org.jooq.Record7;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectSelectStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import de.ship.dbppsquare.square.tables.records.TReportRecord;
import de.ship.dbppsquare.square.tables.records.TReportdatareleaseRecord;
import de.ship.dbppsquare.square.tables.records.TReportperiodRecord;
import de.ship.dbppsquare.squareout.tables.records.TCalculationresultRecord;
import de.ship.dbppsquare.squareout.tables.records.TReportoutputRecord;
import square2.db.control.model.LambdaResultWrapper;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.datamanagement.ReleaseBean;
import square2.modules.model.report.PerformanceBean;
import square2.modules.model.report.analysis.AnalysisMatrix;
import square2.modules.model.report.design.ReportPeriodBean;
import square2.modules.model.report.factory.CalculationBean;

/**
 * 
 * @author henkej
 *
 */
public class CalculationGateway extends JooqGateway {
  private static final Logger LOGGER = LogManager.getLogger(CalculationGateway.class);

  /**
   * create new calculation gateway
   * 
   * @param facesContext the context of this gateway
   */
  public CalculationGateway(SquareFacesContext facesContext) {
    super(facesContext);
  }

  @Override
  public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
    return super.getPrivilegeMap(getJooq());
  }

  /**
   * remove report period interval from db
   * 
   * @param pk primary key of database entry
   * @return number of affected database rows, should be 1
   */
  public Integer removeReportPeriod(Integer pk) {
    try (DeleteConditionStep<TReportperiodRecord> sql = getJooq()
    // @formatter:off
			.deleteFrom(T_REPORTPERIOD)
			.where(T_REPORTPERIOD.PK.equal(pk));
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      return sql.execute();
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * add a calculation to the database
   * 
   * @param bean the calculation bean
   * @return number of rows in database that are affected
   * 
   * if anything went wrong on database side
   */
  public Integer addCalculation(CalculationBean bean) {
    LambdaResultWrapper lrw = new LambdaResultWrapper();
    try (CloseableDSLContext jooq = getJooq()) {
      jooq.transaction(t -> {
        List<ReleaseBean> releases = bean.getReleases();
        if (releases != null && !releases.isEmpty()) {
          for (ReleaseBean rBean : bean.getReleases()) {
            InsertValuesStep2<TReportdatareleaseRecord, Integer, Integer> sql2 = DSL.using(t)
            // @formatter:off
							.insertInto(T_REPORTDATARELEASE, 
													T_REPORTDATARELEASE.FK_REPORT, 
													T_REPORTDATARELEASE.FK_RELEASE)
							.values(bean.getReport(), rBean.getPk());
						// @formatter:on
            LOGGER.debug("{}", sql2.toString());
            lrw.addToInteger(sql2.execute());
          }
        }

        for (ReportPeriodBean iBean : bean.getIntervals()) {
          InsertValuesStep3<TReportperiodRecord, Integer, LocalDateTime, LocalDateTime> sql3 = DSL.using(t)
          // @formatter:off
						.insertInto(T_REPORTPERIOD, 
												T_REPORTPERIOD.FK_REPORT, 
												T_REPORTPERIOD.DATE_FROM,
												T_REPORTPERIOD.DATE_UNTIL)
						.values(bean.getReport(), iBean.getLdtDateFrom(), iBean.getLdtDateUntil());
					// @formatter:on
          LOGGER.debug("{}", sql3.toString());
          lrw.addToInteger(sql3.execute());
        }
      });
      return lrw.getInteger();
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * get all calculations of report
   * 
   * @param report the id of the report
   * @return a list of calculations for the report
   */
  public List<CalculationBean> getAllCalculations(Integer report) {
    try (CloseableDSLContext jooq = getJooq()) {
      SelectConditionStep<Record5<Integer, String, LocalDateTime, LocalDateTime, LocalDateTime>> sql = jooq
      // @formatter:off
				.select(T_REPORT.PK, 
								V_REPORT.NAME, 
								T_REPORT.STARTED,
								T_REPORT.STOPPED,
								T_REPORT.LASTCHANGE)
				.from(T_REPORT)
				.leftJoin(V_REPORT).on(V_REPORT.PK.eq(T_REPORT.PK))
				.leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(V_REPORT.FK_VARIABLEGROUP))
				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE)).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr()))
				.where(V_PRIVILEGE.EXECUTE);
			// @formatter:on
      LOGGER.debug("{}", sql.toString());
      List<CalculationBean> list = new ArrayList<>();
      for (Record r : sql.fetch()) {
        LocalDateTime lc = r.get(T_REPORT.LASTCHANGE);
        Date lastchange = lc == null ? null : Date.from(lc.atZone(ZoneId.systemDefault()).toInstant());
        CalculationBean bean = new CalculationBean(r.get(T_REPORT.PK), lastchange);
        lc = r.get(T_REPORT.STARTED);
        Date started = lc == null ? null : Date.from(lc.atZone(ZoneId.systemDefault()).toInstant());
        bean.setStarted(started);
        lc = r.get(T_REPORT.STOPPED);
        Date stopped = lc == null ? null : Date.from(lc.atZone(ZoneId.systemDefault()).toInstant());
        bean.setStopped(stopped);
        bean.setReportName(r.get(V_REPORT.NAME));

        SelectConditionStep<Record3<Integer, String, String>> sql2 = jooq
        // @formatter:off
					.select(T_RELEASE.PK, 
									T_RELEASE.NAME, 
									T_RELEASE.SHORTNAME)
					.from(T_REPORTDATARELEASE)
					.leftJoin(T_RELEASE).on(T_RELEASE.PK.eq(T_REPORTDATARELEASE.FK_RELEASE))
					.where(T_REPORTDATARELEASE.FK_REPORT.eq(bean.getReport()));
				// @formatter:on
        LOGGER.debug("{}", sql2.toString());
        for (Record release : sql2.fetch()) {
          ReleaseBean rBean = new ReleaseBean(release.get(T_RELEASE.PK));
          rBean.setName(release.get(T_RELEASE.NAME));
          rBean.setShortname(release.get(T_RELEASE.SHORTNAME));
          bean.getReleases().add(rBean);
        }

        SelectConditionStep<Record3<Integer, LocalDateTime, LocalDateTime>> sql3 = jooq
        // @formatter:off
					.select(T_REPORTPERIOD.PK, 
									T_REPORTPERIOD.DATE_FROM, 
									T_REPORTPERIOD.DATE_UNTIL)
					.from(T_REPORTPERIOD)
					.where(T_REPORTPERIOD.FK_REPORT.eq(bean.getReport()));
				// @formatter:on
        LOGGER.debug("{}", sql3.toString());
        for (Record rp : sql3.fetch()) {
          ReportPeriodBean iBean = new ReportPeriodBean(rp.get(T_REPORTPERIOD.PK));
          iBean.setDateFrom(date2ldt(rp.get(T_REPORTPERIOD.DATE_FROM)));
          iBean.setDateUntil(date2ldt(rp.get(T_REPORTPERIOD.DATE_UNTIL)));
          bean.getIntervals().add(iBean);
        }
        list.add(bean);
      }
      list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
      return list;
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * convert localdatetime to date
   * 
   * @param ldt the local date time to convert
   * @return the date object of ldt or null
   */
  private Date date2ldt(LocalDateTime ldt) {
    return ldt == null ? null : Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
  }

  /**
   * get report referenced by id
   * 
   * @param reportId of report
   * @param includePerformance flag to deterime if T_CALCPERF is added to the resulting bean
   * @return the calculation bean or null if none is found
   * 
   * if anything went wrong on database side
   */
  public CalculationBean getCalculation(Integer reportId, boolean includePerformance) {
    try (CloseableDSLContext jooq = getJooq()) {
      SelectConditionStep<Record5<Integer, LocalDateTime, LocalDateTime, String, LocalDateTime>> sql = jooq
      // @formatter:off
				.select(T_REPORT.PK, 
								T_REPORT.STARTED, 
								T_REPORT.STOPPED,
								T_REPORT.NAME,
								T_REPORT.LASTCHANGE)
				.from(T_REPORT)
				.where(T_REPORT.PK.eq(reportId));
			// @formatter:on
      LOGGER.debug("{}", sql.toString());
      for (Record calculations : sql.fetch()) {
        LocalDateTime ldt = calculations.get(T_REPORT.LASTCHANGE);
        Timestamp lastchange = Timestamp.valueOf(ldt);
        CalculationBean bean = new CalculationBean(calculations.get(T_REPORT.PK), lastchange);
        bean.setStarted(date2ldt(calculations.get(T_REPORT.STARTED)));
        bean.setStopped(date2ldt(calculations.get(T_REPORT.STOPPED)));
        bean.setReportName(calculations.get(T_REPORT.NAME));

        SelectConditionStep<Record3<Integer, String, String>> sql2 = jooq
        // @formatter:off
					.select(T_RELEASE.PK, 
									T_RELEASE.NAME, 
									T_RELEASE.SHORTNAME)
					.from(T_REPORTDATARELEASE)
					.leftJoin(T_RELEASE).on(T_REPORTDATARELEASE.FK_RELEASE.eq(T_RELEASE.PK))
					.where(T_REPORTDATARELEASE.FK_REPORT.eq(reportId));
				// @formatter:on
        LOGGER.debug("{}", sql2.toString());
        for (Record releases : sql2.fetch()) {
          ReleaseBean rBean = new ReleaseBean(releases.get(T_RELEASE.PK));
          rBean.setName(releases.get(T_RELEASE.NAME));
          rBean.setShortname(releases.get(T_RELEASE.SHORTNAME));
          bean.getReleases().add(rBean);
        }

        SelectConditionStep<Record3<Integer, LocalDateTime, LocalDateTime>> sql3 = jooq
        // @formatter:off
					.select(T_REPORTPERIOD.PK, 
									T_REPORTPERIOD.DATE_FROM, 
									T_REPORTPERIOD.DATE_UNTIL)
					.from(T_REPORTPERIOD)
					.where(T_REPORTPERIOD.FK_REPORT.eq(reportId));
				// @formatter:on
        LOGGER.debug("{}", sql3.toString());
        for (Record intervals : sql3.fetch()) {
          ReportPeriodBean iBean = new ReportPeriodBean(intervals.get(T_REPORTPERIOD.PK));
          iBean.setDateFrom(date2ldt(intervals.get(T_REPORTPERIOD.DATE_FROM)));
          iBean.setDateUntil(date2ldt(intervals.get(T_REPORTPERIOD.DATE_UNTIL)));
          bean.getIntervals().add(iBean);
        }

        if (includePerformance) {
          SelectConditionStep<Record7<String, Integer, BigDecimal, BigDecimal, BigDecimal, BigDecimal, BigDecimal>> sql4 = jooq
          // @formatter:off
						.select(T_FUNCTION.NAME,
										T_CALCPERF.VAR_MAGNITUDE, 
										T_CALCPERF.USER_SELF, 
										T_CALCPERF.SYS_SELF,
										T_CALCPERF.ELAPSED,
										T_CALCPERF.USER_CHILD,
										T_CALCPERF.SYS_CHILD)
						.from(T_CALCPERF)
						.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_CALCPERF.FK_FUNCTION))
						.where(T_CALCPERF.FK_REPORT.eq(reportId))
						.and(T_CALCPERF.VAR_MAGNITUDE.isNotNull());
					// @formatter:on
          LOGGER.debug("{}", sql4.toString());
          for (Record r : sql4.fetch()) {
            PerformanceBean pBean = new PerformanceBean();
            pBean.setFunctionname(r.get(T_FUNCTION.NAME));
            pBean.setVarmagnitude(r.get(T_CALCPERF.VAR_MAGNITUDE));
            pBean.setUserself(r.get(T_CALCPERF.USER_SELF));
            pBean.setSysself(r.get(T_CALCPERF.SYS_SELF));
            pBean.setElapsed(r.get(T_CALCPERF.ELAPSED));
            pBean.setUserchild(r.get(T_CALCPERF.USER_CHILD));
            pBean.setSyschild(r.get(T_CALCPERF.SYS_CHILD));
            bean.getPerformance().add(pBean);
          }
        }
        return bean;
      }
      return null;
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * get runlog for report
   * 
   * @param id the id of the report
   * @return the run log
   */
  public String getRunlog(Integer id) {
    try (SelectConditionStep<Record1<String>> sql = getJooq()
    // @formatter:off
			.select(T_REPORT.RUNLOG)
			.from(T_REPORT)
			.where(T_REPORT.PK.equal(id));
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      return sql.fetchOne().get(T_REPORT.RUNLOG);
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * get map of report releases to filter releases by report on website
   * 
   * @return list of report releases
   */
  public Map<Integer, List<Integer>> getReportReleaseMap() {
    try (SelectJoinStep<Record2<Integer, Integer[]>> sql = getJooq()
    // @formatter:off
			.select(V_VARIABLEGROUPRELEASE.FK_VARIABLEGROUP, 
							V_VARIABLEGROUPRELEASE.RELEASE_PKS)
			.from(V_VARIABLEGROUPRELEASE);
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      Map<Integer, List<Integer>> map = new HashMap<>();
      for (Record2<Integer, Integer[]> r : sql.fetch()) {
        Integer key = r.get(V_VARIABLEGROUPRELEASE.FK_VARIABLEGROUP);
        Integer[] releases = r.get(V_VARIABLEGROUPRELEASE.RELEASE_PKS);
        List<Integer> values = new ArrayList<>();
        if (releases != null) {
          values.addAll(Arrays.asList(releases));
        }
        map.put(key, values);
      }
      return map;
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * get all reports that have no calculation yet
   * 
   * @return list of reports without a calculation
   */
  public List<AnalysisMatrix> getAllReportsWithoutCalculation() {
    try (SelectConditionStep<Record3<Integer, String, String>> sql = getJooq()
    // @formatter:off
			.select(T_REPORT.PK, 
							T_REPORT.NAME, 
							T_REPORT.DESCRIPTION)
			.from(T_REPORT)
	    .leftJoin(T_VARIABLEGROUP).on(T_VARIABLEGROUP.PK.eq(T_REPORT.FK_VARIABLEGROUP))
	    .leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_VARIABLEGROUP.FK_PRIVILEGE).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr())))
			.where(T_REPORT.STOPPED.isNull())
			.and(T_REPORT.FK_PARENT.isNotNull())    
			.and(V_PRIVILEGE.EXECUTE);
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      List<AnalysisMatrix> list = new ArrayList<>();
      for (Record r : sql.fetch()) {
        AnalysisMatrix bean = new AnalysisMatrix(r.get(T_REPORT.NAME));
        bean.setId(r.get(T_REPORT.PK));
        list.add(bean);
      }
      return list;
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * reset report releases and delete all calculated data
   * 
   * @param reportId the report id
   * @param releases the release beans
   */
  public void resetReleases(Integer reportId, List<ReleaseBean> releases) {
    try (CloseableDSLContext jooq = getJooq()) {
      jooq.transaction(t -> {
        DeleteConditionStep<TReportdatareleaseRecord> sql = DSL.using(t)
        // @formatter:off
					.deleteFrom(T_REPORTDATARELEASE)
					.where(T_REPORTDATARELEASE.FK_REPORT.eq(reportId));
				// @formatter:on
        LOGGER.debug("{}", sql.toString());
        sql.execute();

        for (ReleaseBean bean : releases) {
          InsertValuesStep2<TReportdatareleaseRecord, Integer, Integer> sql1 = DSL.using(t)
          // @formatter:off
						.insertInto(T_REPORTDATARELEASE, 
												T_REPORTDATARELEASE.FK_REPORT, 
												T_REPORTDATARELEASE.FK_RELEASE)
						.values(reportId, bean.getPk());
					// @formatter:on
          LOGGER.debug("{}", sql1.toString());
          sql1.execute();
        }

        DeleteConditionStep<TCalculationresultRecord> sql2 = DSL.using(t)
        // @formatter:off
					.deleteFrom(T_CALCULATIONRESULT)
					.where(T_CALCULATIONRESULT.FK_MATRIXCOLUMN.in(DSL.using(t)
						.select(T_MATRIXCOLUMN.PK)
						.from(T_REPORT)
						.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.FK_FUNCTIONLIST.eq(T_REPORT.FK_FUNCTIONLIST))
						.where(T_REPORT.PK.eq(reportId))));
				// @formatter:on
        LOGGER.debug("{}", sql2.toString());
        sql2.execute();

        UpdateConditionStep<TReportRecord> sql3 = DSL.using(t)
        // @formatter:off
					.update(T_REPORT)
					.set(T_REPORT.LATEXCODE, (String) null)
					.set(T_REPORT.RUNLOG, (String) null)
					.set(T_REPORT.RUNSUM, (String) null)
					.set(T_REPORT.STARTED, (LocalDateTime) null)
					.set(T_REPORT.STOPPED, (LocalDateTime) null)
					.where(T_REPORT.PK.eq(reportId));
				// @formatter:on
        LOGGER.debug("{}", sql3.toString());
        sql3.execute();
      });
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * get calculation of report
   * 
   * @param reportId to be used as reference
   * @return the calculation or null
   */
  public CalculationBean getCalculationOfReport(Integer reportId) {
    try (SelectConditionStep<Record5<Integer, LocalDateTime, LocalDateTime, String, LocalDateTime>> sql = getJooq()
    // @formatter:off
				.select(T_REPORT.PK, 
								T_REPORT.STARTED, 
								T_REPORT.STOPPED, 
								T_REPORT.NAME,
								T_REPORT.LASTCHANGE)
				.from(T_REPORT)
				.where(T_REPORT.PK.eq(reportId))
				.and(T_REPORT.STOPPED.isNotNull());
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      Record5<Integer, LocalDateTime, LocalDateTime, String, LocalDateTime> r = sql.fetchOne();
      CalculationBean bean = null;
      if (r != null) {
        LocalDateTime ldt = r.get(T_REPORT.LASTCHANGE);
        Date lastchange = ldt == null ? null : Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
        bean = new CalculationBean(reportId, lastchange);
        bean.setReportName(r.get(T_REPORT.NAME));
        bean.setStarted(date2ldt(r.get(T_REPORT.STARTED)));
        bean.setStopped(date2ldt(r.get(T_REPORT.STOPPED)));
      }
      return bean;
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * check if there are any results in T_CALCULATIONresult for this report
   * 
   * @param reportId the id of the report
   * @return true or false
   * 
   * for sql errors
   */
  public Boolean hasResults(Integer reportId) {
    try (SelectConditionStep<Record1<Integer>> sql = getJooq()
    // @formatter:off
			.selectCount()
			.from(T_CALCULATIONRESULT)
			.where(T_CALCULATIONRESULT.FK_REPORT.eq(reportId));
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      return sql.fetchOne(0, int.class) > 0;
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * get calculation times for report from T_CALCPERF
   * 
   * @param reportId the report id
   * @return the calculation times in minutes or null
   * 
   * for sql errors
   */
  public String getCalculationTimes(Integer reportId) {
    try (SelectConditionStep<Record1<BigDecimal>> sql = getJooq()
    // @formatter:off
			.select(DSL.sum(T_CALCPERF.ELAPSED).as(T_CALCPERF.ELAPSED))
			.from(T_CALCPERF)
			.leftJoin(T_REPORT).on(T_REPORT.PK.eq(T_CALCPERF.FK_REPORT))
			.where(T_REPORT.PK.eq(reportId));
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      BigDecimal calcperf = new BigDecimal(0);
      for (Record r : sql.fetch()) {
        calcperf = r.get(T_CALCPERF.ELAPSED);
      }
      Double minutes = calcperf == null ? 0 : calcperf.doubleValue() / 60;
      StringBuilder buf = new StringBuilder("");
      buf.append(new DecimalFormat("#0.00").format(minutes)).append("min");
      return buf.toString();
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * remove all result of that report from the db
   * 
   * @param reportId the report id
   * @return number of affected database rows
   * 
   * for sql errors
   */
  public Integer deleteResults(Integer reportId) {
    try (DeleteConditionStep<TCalculationresultRecord> sql = getJooq()
    // @formatter:off
			.deleteFrom(T_CALCULATIONRESULT)
			.where(T_CALCULATIONRESULT.FK_REPORT.eq(reportId));
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      return sql.execute();
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * set report calculation start time to now
   * 
   * @param reportId the ID of the report
   * @return number of affected database rows, should be 1
   * 
   * for sql errors
   */
  public Integer setCalculationStartTimeToNow(Integer reportId) {
    try (UpdateConditionStep<TReportRecord> sql = getJooq()
    // @formatter:off
      .update(T_REPORT)
      .set(T_REPORT.STARTED, LocalDateTime.now())
      .set(T_REPORT.STOPPED, (LocalDateTime) null)
      .where(T_REPORT.PK.eq(reportId));
    // @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      return sql.execute();
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * add latex code to db
   * 
   * @param reportId the id of the report
   * @param latexCode the latex code
   * @return number of affected database rows, should be 1
   */
  public Integer addLatexCode(Integer reportId, String latexCode) {
    try (InsertReturningStep<TReportoutputRecord> sql = getJooq()
    // @formatter:off
			.insertInto(T_REPORTOUTPUT,
									T_REPORTOUTPUT.FK_REPORT,
									T_REPORTOUTPUT.LATEXCODE)
			.values(reportId, latexCode)
			.onConflict(T_REPORTOUTPUT.FK_REPORT, T_REPORTOUTPUT.CHECKSUM)
			.doNothing();
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      return sql.execute();
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * find out if the checksum of the latexCode is still there
   * 
   * @param reportId the id of the report
   * @param latexCode the latex code
   * @return true or false
   */
  public boolean getChecksumIsNew(Integer reportId, String latexCode) {
    try (SelectConditionStep<Record1<Integer>> sql = getJooq()
    // @formatter:off
			.selectCount()
			.from(T_REPORTOUTPUT)
			.where(T_REPORTOUTPUT.CHECKSUM.eq(DSL.md5(latexCode)))
			.and(T_REPORTOUTPUT.FK_REPORT.eq(reportId));
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      return sql.fetchOne(0, int.class) < 1;
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * get the checksum of the latex code
   * 
   * @param reportId the id of the report
   * @param latexCode the latex code
   * @return the checksum of the latex code
   */
  public String getChecksum(Integer reportId, String latexCode) {
    try (SelectSelectStep<Record1<String>> sql = getJooq()
    // @formatter:off
			.select(DSL.md5(latexCode));
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      return sql.fetchOne(0, String.class);
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * set pdf as base64 encoded byte array
   * 
   * @param fkReport the report
   * @param checksum the checksum
   * @param pdf the pdf bytes
   * @return number of affected database rows
   */
  public Integer updatePdf(Integer fkReport, String checksum, byte[] pdf) {
    String pdfString = Base64.getEncoder().encodeToString(pdf);
    try (UpdateConditionStep<TReportoutputRecord> sql = getJooq()
    // @formatter:off
			.update(T_REPORTOUTPUT)
			.set(T_REPORTOUTPUT.PDF, pdfString)
			.where(T_REPORTOUTPUT.FK_REPORT.eq(fkReport))
			.and(T_REPORTOUTPUT.CHECKSUM.eq(checksum));
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      return sql.execute();
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * get pdf from database
   * 
   * @param reportId the id of the report
   * @param checksum the checksum
   * @return the pdf content, if found; null otherwise
   */
  public byte[] getPdfOfReport(Integer reportId, String checksum) {
    try (SelectConditionStep<Record1<String>> sql = getJooq()
    // @formatter:off
			.select(T_REPORTOUTPUT.PDF)
			.from(T_REPORTOUTPUT)
			.where(T_REPORTOUTPUT.FK_REPORT.eq(reportId))
			.and(T_REPORTOUTPUT.CHECKSUM.eq(checksum));
		// @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      Record r = sql.fetchOne();
      if (r == null) {
        return new byte[] {};
      } else {
        String pdfString = r.get(0, String.class);
        if (pdfString != null && !pdfString.isEmpty()) {
          return Base64.getDecoder().decode(pdfString);
        } else {
          return new byte[] {};
        }
      }
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }
}
