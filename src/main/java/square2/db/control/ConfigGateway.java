package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_OPTION;
import static de.ship.dbppsquare.square.Tables.T_USEROPTION;
import static de.ship.dbppsquare.square.Tables.V_USEROPTION;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.JSONB;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.Result;
import org.jooq.SelectConditionStep;
import org.jooq.exception.DataAccessException;

import de.ship.dbppsquare.square.tables.records.TUseroptionRecord;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.ProfileConfigBean;

/**
 * 
 * @author henkej
 *
 */
public class ConfigGateway extends JooqGateway {
	private static final Logger LOGGER = LogManager.getLogger(ConfigGateway.class);

	/**
	 * generate new configuration gateway
	 * 
	 * @param facesContext
	 *          the context of this gateway
	 */
	public ConfigGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	/**
	 * upsert key value pair in db
	 * 
	 * @param key
	 *          the key
	 * @param value
	 *          the value
	 * @param usnr
	 *          the id of the user
	 * @return number of affected database rows
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer upsert(String key, Object value, Integer usnr) {
		String theValue = value == null ? "null" : value.toString();
		try (CloseableDSLContext jooq = getJooq()){
			SelectConditionStep<Record1<Integer>> sql = jooq
			// @formatter:off
				.select(T_OPTION.PK)
				.from(T_OPTION)
				.where(T_OPTION.KEY.eq(key));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());	
			Result<Record1<Integer>> r = sql.fetch();
			if (r.size() < 1) {
				throw new DataAccessException("no option found with key " + key);
			}
			Integer option = r.get(0).get(T_OPTION.PK);
			// @formatter:off
				InsertOnDuplicateSetMoreStep<TUseroptionRecord> sql2 = jooq.insertInto(T_USEROPTION, 
										T_USEROPTION.FK_USNR, 
										T_USEROPTION.FK_OPTION,
										T_USEROPTION.VALUE)
				.values(usnr, option, theValue)
				.onConflict(T_USEROPTION.FK_USNR, T_USEROPTION.FK_OPTION)
				.doUpdate()
				.set(T_USEROPTION.VALUE, theValue);
			// @formatter:off
			LOGGER.debug("{}", sql2.toString());
			return sql2.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get config params by user
	 * 
	 * @param usnr
	 *            the id of he user
	 * @return list of profile config beans

	 *             if anything went wrong on database side
	 */
	public List<ProfileConfigBean> getConfig(Integer usnr) {
		Map<String, Object> result = new HashMap<>();
		Map<String, String> classMap = new HashMap<>();
		Map<String, JSONB> translationMap = new HashMap<>();
		Map<String, Boolean> adminMap = new HashMap<>();

		try(
			SelectConditionStep<Record5<String, String, String, JSONB, Boolean>> sql = getJooq()
			// @formatter:off
				.select(V_USEROPTION.KEY,
								V_USEROPTION.VALUE,
								V_USEROPTION.INPUTTYPE,
								V_USEROPTION.TRANSLATION,
								V_USEROPTION.ADMIN_ONLY)
				.from(V_USEROPTION)
				.where(V_USEROPTION.USNR.eq(usnr));
			// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				result.put(r.get(V_USEROPTION.KEY), r.get(V_USEROPTION.VALUE));
				classMap.put(r.get(V_USEROPTION.KEY), r.get(V_USEROPTION.INPUTTYPE));
				translationMap.put(r.get(V_USEROPTION.KEY), r.get(V_USEROPTION.TRANSLATION));
				adminMap.put(r.get(V_USEROPTION.KEY), r.get(V_USEROPTION.ADMIN_ONLY));
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		String backgroundColor = (String) result.get("config.common.leftmenu.background");
		if (backgroundColor != null && !backgroundColor.startsWith("#")) {
		  result.put("config.common.leftmenu.background", new StringBuilder("#").append(backgroundColor).toString());
		}
		List<ProfileConfigBean> list = new ArrayList<>();
		for (Map.Entry<String, Object> entry : result.entrySet()) {
			String key = entry.getKey();
			Object value = entry.getValue();
			JSONB translation = translationMap.get(entry.getKey());
			Boolean adminOnly = adminMap.get(entry.getKey());
			Boolean isText = "text".equals(classMap.get(entry.getKey()));
			Boolean isColor = "color".equals(classMap.get(entry.getKey()));
			Boolean isNumber = "number".equals(classMap.get(entry.getKey()));
			list.add(new ProfileConfigBean(key, value, translation, adminOnly, isText, isColor, isNumber));
		}
		return list;
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() {
		return null;
	}
}
