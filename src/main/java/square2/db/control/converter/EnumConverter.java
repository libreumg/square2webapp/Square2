package square2.db.control.converter;

import java.io.IOException;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import de.ship.dbppsquare.square.enums.EnumAnadef;
import de.ship.dbppsquare.square.enums.EnumControlvar;
import de.ship.dbppsquare.square.enums.EnumDatasource;
import de.ship.dbppsquare.square.enums.EnumDatatype;
import de.ship.dbppsquare.square.enums.EnumFunctioncategory;
import de.ship.dbppsquare.square.enums.EnumIsocode;
import de.ship.dbppsquare.square.enums.EnumMetadatatype;
import de.ship.dbppsquare.square.enums.EnumMetareftype;

/**
 * 
 * @author henkej
 *
 */
public class EnumConverter {

  /**
   * get enum from literal
   * 
   * @param literal to be used
   * @return enum if found, null otherwise
   */
  public EnumFunctioncategory getEnumFunctioncategory(String literal) {
    for (EnumFunctioncategory e : EnumFunctioncategory.values()) {
      if (e.getLiteral().equalsIgnoreCase(literal)) {
        return e;
      }
    }
    return null;
  }

  /**
   * get enum from literal
   * 
   * @param literal to be used
   * @return enum if found, null otherwise
   */
  public EnumAnadef getEnumAnadef(String literal) {
    for (EnumAnadef e : EnumAnadef.values()) {
      if (e.getLiteral().equalsIgnoreCase(literal)) {
        return e;
      }
    }
    return null;
  }

  /**
   * get isocode enum from literal
   * 
   * @param literal to be used
   * @return enum if found, null otherwise
   */
  public EnumIsocode getEnumIsocode(String literal) {
    for (EnumIsocode e : EnumIsocode.values()) {
      if (e.getLiteral().equalsIgnoreCase(literal)) {
        return e;
      }
    }
    return null;
  }

  /**
   * get metareftype enum from literal
   * 
   * @param literal to be used
   * @return enum if found, null otherwise
   */
  public EnumMetareftype getEnumMetareftype(String literal) {
    for (EnumMetareftype e : EnumMetareftype.values()) {
      if (e.getLiteral().equalsIgnoreCase(literal)) {
        return e;
      }
    }
    return null;
  }

  /**
   * get controlvar enum from literal
   * 
   * @param literal to be used
   * @return enum if found, null otherwise
   */
  public EnumControlvar getEnumControlvar(String literal) {
    for (EnumControlvar e : EnumControlvar.values()) {
      if (e.getLiteral().equalsIgnoreCase(literal)) {
        return e;
      }
    }
    return null;
  }

  /**
   * get access level enum from literal
   * 
   * @param literal to be used
   * @return enum if found, null otherwise
   */
  public EnumAccesslevel getEnumAccesslevel(String literal) {
    for (EnumAccesslevel e : EnumAccesslevel.values()) {
      if (e.getLiteral().equalsIgnoreCase(literal)) {
        return e;
      }
    }
    return null;
  }

  /**
   * get enum of literal
   * 
   * @param literal to be used
   * @return enum if found, null otherwise
   */
  public EnumMetadatatype getEnumMetadatatype(String literal) {
    for (EnumMetadatatype e : EnumMetadatatype.values()) {
      if (e.getLiteral().equalsIgnoreCase(literal)) {
        return e;
      }
    }
    return null;
  }

  /**
   * get enum of literal
   * 
   * @param literal to be used
   * @return enum if found, null otherwise
   */
  public EnumDatasource getEnumDatasource(String literal) {
    for (EnumDatasource e : EnumDatasource.values()) {
      if (e.getLiteral().equalsIgnoreCase(literal)) {
        return e;
      }
    }
    return null;
  }

  /**
   * convert the datatype to its enum
   * 
   * @param datatype the datatype
   * @return the corresponding enum or null
   * @throws IOException for conversion errors
   */
  public static EnumDatatype toDatatypeEnum(String datatype) throws IOException {
    if (datatype == null) {
      return null;
    } else {
      switch (datatype) {
      case "string":
        return EnumDatatype.string;
      case "integer":
        return EnumDatatype.integer;
      case "float":
        return EnumDatatype.float_;
      case "datetime":
        return EnumDatatype.datetime;
      default:
        throw new IOException(datatype + " is no well known enum");
      }
    }
  }
}
