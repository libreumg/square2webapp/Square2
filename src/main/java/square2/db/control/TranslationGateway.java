package square2.db.control;

import java.sql.SQLException;
import java.util.Map;

import org.jooq.CloseableDSLContext;
import org.jooq.exception.DataAccessException;

import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.LangBean;
import square2.modules.model.SquareLocale;

/**
 * 
 * @author henkej
 *
 */
public class TranslationGateway extends JooqGateway {
	/**
	 * generate new translation gateway
	 * 
	 * @param facesContext
	 *          the context of this gateway
	 */
	public TranslationGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
		return null;
	}

	/**
	 * get all translations from db
	 * 
	 * @param locales
	 *          the locales
	 * @return the translations bean
	 * 
	 *         if anything went wrong on database side
	 */
	public LangBean getAllTranslations(SquareLocale... locales) {
		try (CloseableDSLContext jooq = getJooq()) {
			return new DeploymentGateway(jooq).getAllTranslations(locales);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
