package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_GROUP;
import static de.ship.dbppsquare.square.Tables.T_GROUPPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_LANGUAGEKEY;
import static de.ship.dbppsquare.square.Tables.T_LOGIN;
import static de.ship.dbppsquare.square.Tables.T_MAINTENANCE;
import static de.ship.dbppsquare.square.Tables.T_PERSON;
import static de.ship.dbppsquare.square.Tables.T_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_TRANSLATION;
import static de.ship.dbppsquare.square.Tables.T_USERGROUP;
import static de.ship.dbppsquare.square.Tables.T_USERPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.V_GROUP;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jasypt.util.password.StrongPasswordEncryptor;
import org.jooq.CloseableDSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.InsertResultStep;
import org.jooq.InsertValuesStep2;
import org.jooq.InsertValuesStep3;
import org.jooq.InsertValuesStep4;
import org.jooq.JSON;
import org.jooq.Name;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record2;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.Record5;
import org.jooq.Record9;
import org.jooq.Result;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectLimitStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.SelectOrderByStep;
import org.jooq.SelectSeekStep2;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import de.ship.dbppsquare.square.enums.EnumIsocode;
import de.ship.dbppsquare.square.tables.records.TGroupRecord;
import de.ship.dbppsquare.square.tables.records.TGroupprivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TLanguagekeyRecord;
import de.ship.dbppsquare.square.tables.records.TLoginRecord;
import de.ship.dbppsquare.square.tables.records.TMaintenanceRecord;
import de.ship.dbppsquare.square.tables.records.TPersonRecord;
import de.ship.dbppsquare.square.tables.records.TPrivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TTranslationRecord;
import de.ship.dbppsquare.square.tables.records.TUsergroupRecord;
import ship.jsf.components.selectFilterableMenu.FilterableList;
import ship.jsf.components.selectFilterableMenu.KeyValueBeanWrapper;
import square2.db.control.model.LambdaResultWrapper;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.ProfileBean;
import square2.modules.model.UserApplRightBean;
import square2.modules.model.admin.ImpressumBean;
import square2.modules.model.admin.MaintenanceBean;
import square2.modules.model.admin.UserBean;
import square2.modules.model.admin.UsergroupBean;

/**
 * 
 * @author henkej
 * 
 */
public class AdminGateway extends JooqGateway {
	private static final Logger LOGGER = LogManager.getLogger(AdminGateway.class);
	private static final String ROLE_SQUARE_ALL = "role.square.%";

	/**
	 * generate new administration gateway
	 * 
	 * @param facesContext
	 *          context of this constructor call
	 */
	public AdminGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() {
		try (CloseableDSLContext jooq = getJooq()) {
			return super.getPrivilegeMap(jooq);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all application roles
	 * 
	 * @return a filterable ist of application roles
	 * 
	 *         if anything went wrong on database side
	 */
	public FilterableList getApplRoles() {
		FilterableList list = new FilterableList();
		try (SelectLimitStep<Record2<Integer, String>> sql = getJooq()
		// @formatter:off
		  .select(T_PRIVILEGE.PK, 
		  		    T_PRIVILEGE.NAME)
		  .from(T_PRIVILEGE)
		  .where(T_PRIVILEGE.NAME.like(ROLE_SQUARE_ALL))
		  .orderBy(1);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				// remove prefix role.square.
				list.add(new KeyValueBeanWrapper(r.get(T_PRIVILEGE.PK), r.get(T_PRIVILEGE.NAME).substring(12)));
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * create login for user referenced by usnr
	 * 
	 * @param usnr
	 *          id of the user to be added
	 * @return new password of the added user
	 */
	public String createLogin(Integer usnr) {
		try (CloseableDSLContext jooq = getJooq()) {
			if (usnr == null) {
				throw new DataAccessException(getFacesContext().translate("error.mustnotbeempty", "label.name"));
			} else if (jooq.fetchCount(T_LOGIN, T_LOGIN.FK_USNR.eq(usnr)) > 0) {
				throw new DataAccessException("error.admin.adduser.exists");
			} else {
				String initial = RandomStringUtils.random(20, true, true);
				String hashed = new StrongPasswordEncryptor().encryptPassword(initial);
				Date d = new Date();
				d.setTime(d.getTime() + 604800000); // 7 days from today
				Timestamp ts = new Timestamp(d.getTime());
				LocalDateTime ldt = ts.toLocalDateTime();
				InsertValuesStep3<TLoginRecord, Integer, String, LocalDateTime> sql = jooq
				// @formatter:off
				  .insertInto(T_LOGIN, 
				  		        T_LOGIN.FK_USNR, 
				  		        T_LOGIN.PASSWORD, 
				  		        T_LOGIN.EXPIRE_ON)
				  .values(usnr, hashed, ldt);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				sql.execute();
				return initial;
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all square 2 users (this are users that have a login)
	 * 
	 * @return a list of all found users
	 * 
	 *         if anything went wrong on database side
	 */
	public List<UserBean> getSquare2users() {
		List<UserBean> list = new ArrayList<>();
		try (SelectOnConditionStep<Record5<String, String, String, Integer, LocalDateTime>> sql = getJooq()
		// @formatter:off
		  .select(T_PERSON.FORENAME, 
		  		    T_PERSON.SURNAME, 
		  		    T_PERSON.USERNAME, 
		  		    T_PERSON.USNR, 
		  		    T_LOGIN.EXPIRE_ON)
		  .from(T_PERSON)
		  .rightJoin(T_LOGIN).on(T_LOGIN.FK_USNR.eq(T_PERSON.USNR));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				UserBean bean = new UserBean();
				bean.setForename(r.get(T_PERSON.FORENAME));
				bean.setSurname(r.get(T_PERSON.SURNAME));
				bean.setUsername(r.get(T_PERSON.USERNAME));
				bean.setUsnr(r.get(T_PERSON.USNR));
				LocalDateTime ldt = r.get(T_LOGIN.EXPIRE_ON);
				LocalDate ld = ldt.toLocalDate();
				Date expire = Date.from(ld.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
				bean.setExpire(expire);
				LOGGER.debug("found Square² user {}", bean.toString());
				list.add(bean);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * extend expiration date of user
	 * 
	 * @param usnr
	 *          id of the user
	 * @param date
	 *          the new date of login expiration
	 * 
	 *          if anything went wrong on database side
	 */
	public void extendExpire(Integer usnr, Date date) {
	  LocalDateTime expire = date == null ? null : date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		try (UpdateConditionStep<TLoginRecord> sql = getJooq()
		// @formatter:off
		  .update(T_LOGIN)
		  .set(T_LOGIN.EXPIRE_ON, expire)
		  .where(T_LOGIN.FK_USNR.eq(usnr));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all users from database
	 * 
	 * @param includeSquareUsers
	 *          if true, load square users also
	 * @return the list of found users
	 * 
	 *         if anything went wrong on database side
	 */
	public List<ProfileBean> getAllUsers(boolean includeSquareUsers) {
		List<ProfileBean> list = new ArrayList<>();
		try (CloseableDSLContext jooq = getJooq()) {
			SelectJoinStep<Record4<String, String, String, Integer>> q = jooq
			// @formatter:off
				.select(T_PERSON.FORENAME, 
						    T_PERSON.SURNAME, 
						    T_PERSON.USERNAME, 
						    T_PERSON.USNR)
				.from(T_PERSON);
			// @formatter:on
			SelectSeekStep2<Record4<String, String, String, Integer>, ?, ?> step;
			if (includeSquareUsers) {
				step = q.orderBy(T_PERSON.SURNAME, T_PERSON.FORENAME);
			} else {
				step = q
				// @formatter:off
					.where(T_PERSON.USNR.notIn(jooq
						.select(T_LOGIN.FK_USNR)
						.from(T_LOGIN)))
					.orderBy(T_PERSON.SURNAME, T_PERSON.FORENAME);
				// @formatter:on
			}
			LOGGER.debug("{}", step.toString());
			for (Record r : step.fetch()) {
				ProfileBean bean = new ProfileBean();
				bean.setForename(r.get(T_PERSON.FORENAME));
				bean.setSurname(r.get(T_PERSON.SURNAME));
				bean.setUsername(r.get(T_PERSON.USERNAME));
				bean.setUsnr(r.get(T_PERSON.USNR));
				LOGGER.debug("found person {}", bean.toString());
				list.add(bean);
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * get all users from db that have a square login
	 * 
	 * @return the list of all users
	 * 
	 *         if anything went wrong on database side
	 */
	public List<ProfileBean> getAllSquareUsers() {
		List<ProfileBean> list = new ArrayList<>();
		try (SelectSeekStep2<Record4<String, String, String, Integer>, String, String> sql = getJooq()
		// @formatter:off
		  .select(T_PERSON.FORENAME, 
		  		    T_PERSON.SURNAME, 
		  		    T_PERSON.USERNAME, 
		  		    T_PERSON.USNR)
		  .from(T_PERSON)
		  .rightJoin(T_LOGIN).on(T_LOGIN.FK_USNR.eq(T_PERSON.USNR))
		  .orderBy(T_PERSON.SURNAME, T_PERSON.FORENAME);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				ProfileBean bean = new ProfileBean();
				bean.setForename(r.get(T_PERSON.FORENAME));
				bean.setSurname(r.get(T_PERSON.SURNAME));
				bean.setUsername(r.get(T_PERSON.USERNAME));
				bean.setUsnr(r.get(T_PERSON.USNR));
				LOGGER.debug("found person {}", bean.toString());
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get user privileges for roles (T_PRIVILEGE.name like 'role.square.%')
	 * 
	 * @return the list of privileges
	 * 
	 *         if anything went wrong on database side
	 */
	public List<UserApplRightBean> getUserPrivilegeRoles() {
		List<UserApplRightBean> list = new ArrayList<>();
		Name lastchange = DSL.name("lastchange");
		Name isgroup = DSL.name("isgroup");
		try (CloseableDSLContext jooq = getJooq()) {
			SelectOrderByStep<Record9<String, String, String, String, Integer, Integer, EnumAccesslevel, Boolean, LocalDateTime>> sql = jooq
			// @formatter:off
			  .select(T_PERSON.USERNAME, 
			  		    T_PERSON.FORENAME, 
			  		    T_PERSON.SURNAME, 
			  		    T_PRIVILEGE.NAME,
			  		    T_USERPRIVILEGE.FK_PRIVILEGE, 
			  		    T_USERPRIVILEGE.FK_USNR, 
			  		    T_USERPRIVILEGE.SQUARE_ACL,
			  		    DSL.val(false, Boolean.class).as(isgroup), 
			  		    T_USERPRIVILEGE.LASTCHANGE.as(lastchange))
			  .from(T_USERPRIVILEGE)
			  .leftJoin(T_PERSON).on(T_PERSON.USNR.eq(T_USERPRIVILEGE.FK_USNR))
			  .leftJoin(T_PRIVILEGE).on(T_PRIVILEGE.PK.eq(T_USERPRIVILEGE.FK_PRIVILEGE))
			  .where(T_PRIVILEGE.NAME.like(ROLE_SQUARE_ALL))
			  .unionAll(jooq
			    .select(T_GROUP.NAME, 
			    		    DSL.castNull(T_PERSON.FORENAME), 
			    		    DSL.castNull(T_PERSON.SURNAME),
			    		    T_PRIVILEGE.NAME, 
			    		    T_GROUPPRIVILEGE.FK_PRIVILEGE, 
			    		    T_GROUPPRIVILEGE.FK_GROUP,
			    		    T_GROUPPRIVILEGE.SQUARE_ACL, 
			    		    DSL.val(true, Boolean.class).as(isgroup),
			    		    T_GROUPPRIVILEGE.LASTCHANGE.as(lastchange))
			    .from(T_GROUPPRIVILEGE)
			    .leftJoin(T_GROUP).on(T_GROUP.PK.eq(T_GROUPPRIVILEGE.FK_GROUP))
			    .leftJoin(T_PRIVILEGE).on(T_PRIVILEGE.PK.eq(T_GROUPPRIVILEGE.FK_PRIVILEGE))
			    .where(T_PRIVILEGE.NAME.like(ROLE_SQUARE_ALL)));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				UserApplRightBean bean = new UserApplRightBean();
				Boolean isGroup = r.get(isgroup, Boolean.class);
				bean.setLastchange(r.get(lastchange, Timestamp.class));
				if (isGroup) {
					bean.setGroupname(r.get(T_PERSON.USERNAME));
					bean.setGroup(r.get(T_USERPRIVILEGE.FK_USNR));
				} else {
					bean.setForename(r.get(T_PERSON.FORENAME));
					bean.setSurname(r.get(T_PERSON.SURNAME));
					bean.setUsername(r.get(T_PERSON.USERNAME));
					bean.setUsnr(r.get(T_USERPRIVILEGE.FK_USNR));
				}
				bean.setRightId(r.get(T_USERPRIVILEGE.FK_PRIVILEGE));
				bean.setRightName(r.get(T_PRIVILEGE.NAME).substring(12)); // dismiss role.square.
				bean.setAcl(r.get(T_USERPRIVILEGE.SQUARE_ACL));
				LOGGER.debug("found privilege {}", bean.toString());
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add user and/or group privilege to db
	 * 
	 * @param usnr
	 *          the id of the user
	 * @param group
	 *          pk of group
	 * @param privilege
	 *          the id of the privilege
	 * @param acl
	 *          the privilege map
	 * 
	 *          if anything went wrong on database side
	 */
	public void addUserGroupPrivilege(Integer usnr, Integer group, Integer privilege, AclBean acl) {
		try (CloseableDSLContext jooq = getJooq()) {
			super.addUserGroupPrivilege(jooq, usnr, group, privilege, acl);
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete user login from db
	 * 
	 * @param usnr
	 *          if user to be deleted
	 * @return number of affected rows in database
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer deleteUserLogin(Integer usnr) {
		try (DeleteConditionStep<TLoginRecord> sql = getJooq()
		// @formatter:off
		  .deleteFrom(T_LOGIN)
		  .where(T_LOGIN.FK_USNR.eq(usnr));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * load all user groups
	 * 
	 * @return a list of all user groups
	 */
	public List<UsergroupBean> loadUsergroups() {
		try (SelectJoinStep<Record3<Integer, String, JSON>> sql = getJooq()
		// @formatter:off
			.select(V_GROUP.PK, 
					    V_GROUP.NAME, 
					    V_GROUP.USERS)
			.from(V_GROUP);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<UsergroupBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				list.add(
						new UsergroupBean(r.get(V_GROUP.PK), r.get(V_GROUP.NAME), convertJsonToProfileBeans(r.get(V_GROUP.USERS))));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * remove user from group
	 * 
	 * @param groupPk
	 *          pk of group
	 * @param usnr
	 *          unique key of user table
	 */
	public void removeUserFromGroup(Integer groupPk, Integer usnr) {
		try (DeleteConditionStep<TUsergroupRecord> sql = getJooq()
		// @formatter:off
			.deleteFrom(T_USERGROUP)
			.where(T_USERGROUP.FK_GROUP.eq(groupPk))
			.and(T_USERGROUP.FK_USNR.eq(usnr));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add user to existing group (if not exists)
	 * 
	 * @param pk
	 *          the id of the group
	 * @param usnr
	 *          the id of the user
	 * 
	 *          if anything went wrong on database side
	 */
	public void addUserToGroup(Integer pk, Integer usnr) {
		try (InsertValuesStep2<TUsergroupRecord, Integer, Integer> sql = getJooq()
		// @formatter:off
			.insertInto(T_USERGROUP, 
					        T_USERGROUP.FK_GROUP, 
					        T_USERGROUP.FK_USNR)
			.values(pk, usnr);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add group in db
	 * 
	 * @param name
	 *          to be used
	 * @return the new id of the group
	 */
	public Integer addGroup(String name) {
		try (CloseableDSLContext jooq = getJooq()) {
			Integer found = jooq.fetchCount(T_GROUP, T_GROUP.NAME.eq(name));
			if (found > 0) {
				throw new DataAccessException(getFacesContext().translate("error.duplicate.name", name));
			}
			InsertResultStep<TGroupRecord> sql = jooq
			// @formatter:off
				.insertInto(T_GROUP, 
						        T_GROUP.NAME)
				.values(name)
				.returning(T_GROUP.PK);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			return sql.fetchOne().getPk();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete group from db; also deletes all dependings
	 * 
	 * @param pk
	 *          the id of the group
	 * @return number of database rows
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer deleteGroup(Integer pk) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				DeleteConditionStep<TUsergroupRecord> sql1 = DSL.using(t)
				// @formatter:off
					.deleteFrom(T_USERGROUP)
					.where(T_USERGROUP.FK_GROUP.eq(pk));
				// @formatter:on
				LOGGER.debug("{}", sql1.toString());
				lrw.addToInteger(sql1.execute());

				DeleteConditionStep<TGroupprivilegeRecord> sql2 = DSL.using(t)
				// @formatter:off
					.deleteFrom(T_GROUPPRIVILEGE)
					.where(T_GROUPPRIVILEGE.FK_GROUP.eq(pk));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());

				DeleteConditionStep<TGroupRecord> sql3 = DSL.using(t)
				// @formatter:off
					.deleteFrom(T_GROUP)
					.where(T_GROUP.PK.eq(pk));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				lrw.addToInteger(sql3.execute());
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return lrw.getInteger();
	}

	/**
	 * get all from T_MAINTENANCE for Square²
	 * 
	 * @return the list of all maintenance beans
	 * 
	 *         if anything went wrong on database side
	 */
	public List<MaintenanceBean> getAllMaintenances() {
		try (SelectConditionStep<Record4<Integer, String, LocalDateTime, LocalDateTime>> sql = getJooq()
		// @formatter:off
		  .select(T_MAINTENANCE.PK, 
		  		    T_MAINTENANCE.MESSAGE, 
		  		    T_MAINTENANCE.DOWNTIME_START,
				      T_MAINTENANCE.DOWNTIME_END)
		  .from(T_MAINTENANCE)
		  .where(T_MAINTENANCE.AFFECTED_SQUARE.isTrue());
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<MaintenanceBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
			  LocalDateTime start = r.get(T_MAINTENANCE.DOWNTIME_START);
			  LocalDateTime end = r.get(T_MAINTENANCE.DOWNTIME_END);
			  Timestamp startTs = start == null ? null : Timestamp.valueOf(start);
			  Timestamp endTs = end == null ? null : Timestamp.valueOf(end);
				list.add(new MaintenanceBean(r.get(T_MAINTENANCE.PK), r.get(T_MAINTENANCE.MESSAGE),
						startTs, endTs));
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update maintenance bean
	 * 
	 * @param bean
	 *          the maintenance bean
	 * 
	 *          if anything went wrong on database side
	 */
	public void updateMaintenance(MaintenanceBean bean) {
	  LocalDateTime start = bean.getDowntimeStart() == null ? null : bean.getDowntimeStart().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    LocalDateTime end = bean.getDowntimeEnd() == null ? null : bean.getDowntimeEnd().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		try (UpdateConditionStep<TMaintenanceRecord> sql = getJooq()
		// @formatter:off
		  .update(T_MAINTENANCE)
		  .set(T_MAINTENANCE.MESSAGE, bean.getMessage())
		  .set(T_MAINTENANCE.DOWNTIME_START, start)
		  .set(T_MAINTENANCE.DOWNTIME_END, end)
		  .where(T_MAINTENANCE.PK.eq(bean.getPk()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add bean to db
	 * 
	 * @param bean
	 *          the maintenance bean
	 * @return number of affected database rows (should always be 1)
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer insertMaintenance(MaintenanceBean bean) {
	  LocalDateTime start = bean.getDowntimeStart() == null ? null : bean.getDowntimeStart().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    LocalDateTime end = bean.getDowntimeEnd() == null ? null : bean.getDowntimeEnd().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
		try (InsertValuesStep4<TMaintenanceRecord, String, LocalDateTime, LocalDateTime, Boolean> sql = getJooq()
		// @formatter:off
		  .insertInto(T_MAINTENANCE, 
		  		        T_MAINTENANCE.MESSAGE, 
		  		        T_MAINTENANCE.DOWNTIME_START,
		  		        T_MAINTENANCE.DOWNTIME_END, 
		  		        T_MAINTENANCE.AFFECTED_SQUARE)
		  .values(bean.getMessage(), 
		  		    start,
				      end, 
				      true);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete maintenance in db
	 * 
	 * @param pk
	 *          the maintenance id
	 * 
	 *          if anything went wrong on database side
	 */
	public void deleteMaintenance(Integer pk) {
		try (DeleteConditionStep<TMaintenanceRecord> sql = getJooq()
		// @formatter:off
		  .deleteFrom(T_MAINTENANCE)
		  .where(T_MAINTENANCE.PK.eq(pk));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get impressum of current locale
	 * 
	 * @return impressum if found, empty string otherwise
	 */
	public List<ImpressumBean> getImpressums() {
		String langkey = "label.square.impressum";
		try (SelectConditionStep<Record2<String, EnumIsocode>> sql = getJooq()
		// @formatter:off
			.select(T_TRANSLATION.VALUE, 
					    T_TRANSLATION.ISOCODE)
			.from(T_LANGUAGEKEY)
			.leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_LANGUAGEKEY.PK))
			.where(T_LANGUAGEKEY.NAME.eq(langkey));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Map<EnumIsocode, String> localeMap = new EnumMap<>(EnumIsocode.class);
			for (Record r : sql.fetch()) {
				EnumIsocode isocode = r.get(T_TRANSLATION.ISOCODE);
				String content = r.get(T_TRANSLATION.VALUE);
				localeMap.put(isocode, content);
			}
			List<ImpressumBean> list = new ArrayList<>();
			for (Map.Entry<EnumIsocode, String> entry : localeMap.entrySet()) {
				ImpressumBean bean = new ImpressumBean(entry.getKey(), entry.getValue());
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * upsert impressum of current locale
	 * 
	 * @param bean
	 *          to be used
	 * @return number of affeced rows
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer upsertImpressum(ImpressumBean bean) {
		EnumIsocode isocode = EnumIsocode.valueOf(bean.getLocale().toString());
		String langkey = "label.square.impressum";
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(t -> {
				InsertResultStep<TPrivilegeRecord> sql = DSL.using(t)
				// @formatter:off
				  .insertInto(T_PRIVILEGE, 
								      T_PRIVILEGE.NAME)
				  .values(langkey)
				  .onConflict(T_PRIVILEGE.NAME)
				  .doUpdate() // because of update, returning returns the pk; doNothing would not return anything
					.set(T_PRIVILEGE.NAME, langkey)
					.returning(T_PRIVILEGE.PK);
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				Integer newPriv = sql.fetchOne().getPk();

				InsertResultStep<TLanguagekeyRecord> sql1 = DSL.using(t)
				// @formatter:off
					.insertInto(T_LANGUAGEKEY, 
							        T_LANGUAGEKEY.FK_PRIVILEGE, 
							        T_LANGUAGEKEY.NAME)
					.values(newPriv, langkey)
					.onConflict(T_LANGUAGEKEY.NAME)
					.doUpdate() // because of update, returning returns the pk; doNothing would not return anything
					.set(T_LANGUAGEKEY.NAME, langkey)
					.returning(T_LANGUAGEKEY.PK);
				// @formatter:on
				LOGGER.debug("{}", sql1.toString());
				Integer fkLang = sql1.fetchOne().getPk();

				InsertOnDuplicateSetMoreStep<TTranslationRecord> sql2 = DSL.using(t)
				// @formatter:off
					.insertInto(T_TRANSLATION, 
							        T_TRANSLATION.ISOCODE, 
							        T_TRANSLATION.VALUE, 
							        T_TRANSLATION.FK_LANG)
					.values(isocode, bean.getContent(), fkLang)
					.onConflict(T_TRANSLATION.ISOCODE, T_TRANSLATION.FK_LANG)
					.doUpdate()
					.set(T_TRANSLATION.VALUE, bean.getContent());
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				Integer affectedRows = sql2.execute();
				lrw.setInteger(affectedRows);
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return lrw.getInteger();
	}

	/**
	 * add user to database
	 * 
	 * @param bean the user bean
	 * @return the new usnr
	 */
  public Integer addUser(UserBean bean) {
    LambdaResultWrapper lrw = new LambdaResultWrapper();
    try (CloseableDSLContext jooq = getJooq()) {
      // check for no duplicates of this user in the database
      jooq.transaction(t -> {
        Result<TPersonRecord> found = DSL.using(t)
        // @formatter:off
          .selectFrom(T_PERSON)
          .where(T_PERSON.USERNAME.eq(bean.getUsername()))
          .fetch();
        // @formatter:on
        if (found.isEmpty()) {
          InsertResultStep<TPersonRecord> sql = DSL.using(t)
          // @formatter:off
            .insertInto(T_PERSON,
                        T_PERSON.FORENAME,
                        T_PERSON.SURNAME,
                        T_PERSON.USERNAME,
                        T_PERSON.USNR)
            .select(DSL.using(t)
              .select(DSL.val(bean.getForename()), DSL.val(bean.getSurname()),
                      DSL.val(bean.getUsername()), DSL.max(T_PERSON.USNR).add(1))
              .from(T_PERSON))
            .returning(T_PERSON.USNR);
          // @formatter:on
          LOGGER.debug("{}", sql.toString());
          lrw.setInteger(sql.fetch().get(0).get(T_PERSON.USNR));
        } else {
          throw new DataAccessException(getFacesContext().translate("error.username.exists", bean.getUsername()));
        }
      });
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
    return lrw.getInteger();
  }

  /**
   * delete a person from the database
   * 
   * @param usnr the id of the person
   * @return the number of affected database lines
   */
  public Integer deletePerson(Integer usnr) {
    try (DeleteConditionStep<TPersonRecord> sql = getJooq()
    // @formatter:off
      .deleteFrom(T_PERSON)
      .where(T_PERSON.USNR.eq(usnr));
    // @formatter:on
    ) {
      LOGGER.debug("{}", sql.toString());
      return sql.execute();
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * find all usergroup names that the user is part of
   * 
   * @param usnr the id of the user
   * @return a list of usergroup names, an empty one at least
   */
	public List<String> findUsergroupNamesOfUser(Integer usnr) {
		try (SelectConditionStep<Record1<String>> sql = getJooq()
		// @formatter:off
      .select(T_GROUP.NAME)
      .from(T_USERGROUP)
      .leftJoin(T_GROUP).on(T_GROUP.PK.eq(T_USERGROUP.FK_GROUP))
      .where(T_USERGROUP.FK_USNR.eq(usnr));
    // @formatter:on
	  ) {
	    LOGGER.debug("{}", sql.toString());
	    List<String> list = new ArrayList<>();
	    for (Record r : sql.fetch()) {
	    	list.add(r.get(T_GROUP.NAME));
	    }
	    return list;
	  } catch (DataAccessException | ClassNotFoundException | SQLException e) {
	    throw new DataAccessException(e.getMessage(), e);
	  }
	}

	/**
	 * find all role names of roles that the user still has
	 * 
	 * @param usnr the id of the user
	 * @return a list of role names, an empty one at least
	 */
	public List<String> findRolenameOfUser(Integer usnr) {
		try (SelectConditionStep<Record1<String>> sql = getJooq()
		// @formatter:off
		  .select(T_PRIVILEGE.NAME)
		  .from(T_USERPRIVILEGE)
		  .leftJoin(T_PRIVILEGE).on(T_PRIVILEGE.PK.eq(T_USERPRIVILEGE.FK_PRIVILEGE))
		  .where(T_USERPRIVILEGE.FK_USNR.eq(usnr));
		// @formatter:on
		) {
		  LOGGER.debug("{}", sql.toString());
		  List<String> list = new ArrayList<>();
		  for (Record r : sql.fetch()) {
		  	list.add(r.get(T_PRIVILEGE.NAME));
		  }
		  return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
		  throw new DataAccessException(e.getMessage(), e);
		}
	}
}
