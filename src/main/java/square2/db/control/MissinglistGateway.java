package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_ELEMENT;
import static de.ship.dbppsquare.square.Tables.T_GROUPPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_MISSING;
import static de.ship.dbppsquare.square.Tables.T_MISSINGLIST;
import static de.ship.dbppsquare.square.Tables.T_MISSINGTYPE;
import static de.ship.dbppsquare.square.Tables.T_STUDYGROUP;
import static de.ship.dbppsquare.square.Tables.T_USERGROUP;
import static de.ship.dbppsquare.square.Tables.T_USERPRIVILEGE;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertResultStep;
import org.jooq.InsertReturningStep;
import org.jooq.JSONB;
import org.jooq.Record;
import org.jooq.Record4;
import org.jooq.Record8;
import org.jooq.Result;
import org.jooq.SelectConditionStep;
import org.jooq.SelectWhereStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import de.ship.dbppsquare.square.tables.TElement;
import de.ship.dbppsquare.square.tables.records.TElementRecord;
import de.ship.dbppsquare.square.tables.records.TMissingRecord;
import de.ship.dbppsquare.square.tables.records.TMissinglistRecord;
import de.ship.dbppsquare.square.tables.records.TMissingtypeRecord;
import square2.db.control.model.LambdaResultWrapper;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.cohort.StudygroupBean;
import square2.modules.model.study.MissingBean;
import square2.modules.model.study.MissinglistBean;

/**
 * @author henkej
 *
 */
public class MissinglistGateway extends JooqGateway {
  private static final Logger LOGGER = LogManager.getLogger(StudyGateway.class);

  /**
   * @param facesContext the faces context
   */
  public MissinglistGateway(SquareFacesContext facesContext) {
    super(facesContext);
  }

  @Override
  public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
    return null;
  }

  /**
   * get all study groups that the user has access to - this are the parents of the studies that he is able to see
   * 
   * @param usnr the id of the user
   * @param studygroupName the name of the study group; may be null to get all
   * @return a list of found elements; an empty list at least
   */
  public List<StudygroupBean> getAllStudyGroupElementsOf(Integer usnr, String studygroupName) {
    try (CloseableDSLContext jooq = getJooq()) {
      TElement ALIAS_STUDY = T_ELEMENT.as("study");
      SelectConditionStep<Record4<Integer, String, JSONB, JSONB>> sql = jooq
      // @formatter:off
        .selectDistinct(T_ELEMENT.PK,
                        T_ELEMENT.NAME,
                        T_ELEMENT.TRANSLATION,
                        T_STUDYGROUP.LOCALES)
        .from(T_ELEMENT)
        .leftJoin(T_STUDYGROUP).on(T_STUDYGROUP.FK_ELEMENT.eq(T_ELEMENT.PK))
        .leftJoin(ALIAS_STUDY).on(ALIAS_STUDY.FK_PARENT.eq(T_ELEMENT.PK))
        .leftJoin(T_USERPRIVILEGE).on(T_USERPRIVILEGE.FK_PRIVILEGE.eq(ALIAS_STUDY.FK_PRIVILEGE).and(T_USERPRIVILEGE.SQUARE_ACL.like("%w%")).and(T_USERPRIVILEGE.FK_USNR.eq(usnr)))
        .leftJoin(T_GROUPPRIVILEGE).on(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(ALIAS_STUDY.FK_PRIVILEGE).and(T_GROUPPRIVILEGE.SQUARE_ACL.like("%w%")))
        .leftJoin(T_USERGROUP).on(T_USERGROUP.FK_GROUP.eq(T_GROUPPRIVILEGE.FK_GROUP).and(T_USERGROUP.FK_USNR.eq(usnr)))
        .where(T_ELEMENT.FK_PARENT.isNull())
        .and(DSL.coalesce(T_USERPRIVILEGE.SQUARE_ACL, T_GROUPPRIVILEGE.SQUARE_ACL).isNotNull())
        .and(T_ELEMENT.NAME.eq(studygroupName == null ? T_ELEMENT.NAME : DSL.val(studygroupName)));
      // @formatter:on
      LOGGER.debug("{}", sql.toString());
      List<StudygroupBean> list = new ArrayList<>();
      for (Record r : sql.fetch()) {
      	// do not need list of corresponding studies here
        list.add(new StudygroupBean(r.get(T_ELEMENT.PK), r.get(T_ELEMENT.NAME), r.get(T_ELEMENT.TRANSLATION), "not loaded", r.get(T_STUDYGROUP.LOCALES)));
      }
      return list;
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * get a list of missinglists that the user has access to
   * 
   * @param usnr the id of the user
   * @param studygroupName the name of the study group; may be null to get all
   * @return a list of missinglists; an empty list at least
   */
  public List<MissinglistBean> getAllMissinglists(Integer usnr, String studygroupName) {
    List<StudygroupBean> sgs = getAllStudyGroupElementsOf(usnr, studygroupName);
    List<Integer> allowedStudygroups = new ArrayList<>();
    for (StudygroupBean bean : sgs) {
      allowedStudygroups.add(bean.getPk());
    }
    try (CloseableDSLContext jooq = getJooq()) {
      SelectConditionStep<Record8<Integer, String, String, Integer, String, String, Integer, String>> sql = jooq
      // @formatter:off
        .select(T_MISSINGLIST.PK, 
                T_MISSINGLIST.NAME, 
                T_MISSINGLIST.DESCRIPTION, 
                T_MISSING.PK,
                T_MISSING.CODE,
                T_MISSING.NAME_IN_STUDY, 
                T_MISSINGTYPE.PK, 
                T_MISSINGTYPE.NAME)
        .from(T_MISSINGLIST)
        .leftJoin(T_MISSING).on(T_MISSING.FK_MISSINGLIST.eq(T_MISSINGLIST.PK))
        .leftJoin(T_MISSINGTYPE).on(T_MISSINGTYPE.PK.eq(T_MISSING.FK_MISSINGTYPE))
        .where(T_MISSINGLIST.FK_ELEMENT.in(allowedStudygroups));
      // @formatter:on
      LOGGER.debug("{}", sql.toString());
      Map<Integer, MissinglistBean> map = new HashMap<>();
      for (Record r : sql.fetch()) {
        Integer fkMissinglist = r.get(T_MISSINGLIST.PK);
        String missinglistName = r.get(T_MISSINGLIST.NAME);
        String description = r.get(T_MISSINGLIST.DESCRIPTION);
        Integer fkMissing = r.get(T_MISSING.PK);
        String code = r.get(T_MISSING.CODE);
        String nameInStudy = r.get(T_MISSING.NAME_IN_STUDY);
        Integer fkMissingtype = r.get(T_MISSINGTYPE.PK);
        String missingtypeName = r.get(T_MISSINGTYPE.NAME);
        MissinglistBean mlb = map.get(fkMissinglist);
        if (mlb == null) {
          mlb = new MissinglistBean(fkMissinglist, missinglistName, description, new ArrayList<>());
          map.put(fkMissinglist, mlb);
        }
        mlb.getList().add(new MissingBean(fkMissing, code, nameInStudy, fkMissingtype, missingtypeName));
      }
      return new ArrayList<>(map.values());
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * remove the missinglist from the database
   * 
   * @param pk the id of the missinglist
   * @return the number of affected database rows
   */
  public Integer removeMissinglist(Integer pk) {
    LambdaResultWrapper lrw = new LambdaResultWrapper();
    try (CloseableDSLContext jooq = getJooq()) {
      jooq.transaction(t -> {
        UpdateConditionStep<TElementRecord> sql = DSL.using(t)
        // @formatter:off
          .update(T_ELEMENT)
          .set(T_ELEMENT.FK_MISSINGLIST, DSL.value(null, Integer.class))
          .where(T_ELEMENT.FK_MISSINGLIST.eq(pk));
        // @formatter:on
        LOGGER.debug("{}", sql.toString());
        lrw.addToInteger(sql.execute());

        DeleteConditionStep<TMissingRecord> sql2 = DSL.using(t)
        // @formatter:off
          .deleteFrom(T_MISSING)
          .where(T_MISSING.FK_MISSINGLIST.eq(pk));
        // @formatter:on
        LOGGER.debug("{}", sql2.toString());
        lrw.addToInteger(sql2.execute());

        DeleteConditionStep<TMissinglistRecord> sql3 = DSL.using(t)
        // @formatter:off
          .deleteFrom(T_MISSINGLIST)
          .where(T_MISSINGLIST.PK.eq(pk));
        // @formatter:on
        LOGGER.debug("{}", sql3.toString());
        lrw.addToInteger(sql3.execute());
      });
      return lrw.getInteger();
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * save the missinglist for element id
   * 
   * @param missinglist the missing list
   * @param missinglistName the name of the missinglist, must be unique
   * @param missinglistDescription the description of the missinglist
   * @param fkStudygroup the corresponding study group for that missinglist
   * @return number of affected database rows
   */
  public Integer saveMissinglist(List<MissingBean> missinglist, String missinglistName, String missinglistDescription,
      Integer fkStudygroup) {
    LambdaResultWrapper lrw = new LambdaResultWrapper();
    Iterator<MissingBean> iterator = missinglist.iterator();
    while (iterator.hasNext()) {
      MissingBean bean = iterator.next();
      if (bean.isEmpty()) {
        iterator.remove();
      }
    }
    try (CloseableDSLContext jooq = getJooq()) {
      jooq.transaction(t -> {
        // fill up missinglists pktype
        for (MissingBean bean : missinglist) {
          InsertResultStep<TMissingtypeRecord> sql = DSL.using(t)
          // @formatter:off
            .insertInto(T_MISSINGTYPE,
                        T_MISSINGTYPE.NAME)
            .values(bean.getTypeName())
            .onConflict(T_MISSINGTYPE.NAME)
            .doNothing()
            .returning(T_MISSINGTYPE.PK);
          // @formatter:on
          LOGGER.debug("{}", sql.toString());

          Result<TMissingtypeRecord> res = sql.fetch();
          Integer pkMissingtype = null;
          if (res.size() < 1) { // doNothing does not return the content of returning
            SelectConditionStep<TMissingtypeRecord> sql2 = DSL.using(t)
            // @formatter:off
              .selectFrom(T_MISSINGTYPE)
              .where(T_MISSINGTYPE.NAME.eq(bean.getTypeName()));
            // @formatter:off
            LOGGER.debug("{}", sql2.toString());
            pkMissingtype = sql2.fetchOne().getPk();
          } else {
            pkMissingtype = res.get(0).getPk();
          }
          bean.setFkMissingtype(pkMissingtype);
        }

        InsertResultStep<TMissinglistRecord> sql2 = DSL.using(t)
        // @formatter:off
          .insertInto(T_MISSINGLIST, 
                      T_MISSINGLIST.NAME,
                      T_MISSINGLIST.DESCRIPTION,
                      T_MISSINGLIST.FK_ELEMENT)
          .values(missinglistName, missinglistDescription, fkStudygroup)
          .onConflict(T_MISSINGLIST.NAME)
          .doUpdate()
          .set(T_MISSINGLIST.DESCRIPTION, missinglistDescription)
          .returning(T_MISSINGLIST.PK);
        // @formatter:off
        LOGGER.debug("{}", sql2.toString());
        Integer fkMissinglist = sql2.fetchOne().getPk();

        List<String> codelist = new ArrayList<String>();
        for (MissingBean bean : missinglist) {
          codelist.add(bean.getCode());
        }
        DeleteConditionStep<TMissingRecord> sql3 = DSL.using(t)
        // @formatter:off
          .deleteFrom(T_MISSING)
          .where(T_MISSING.FK_MISSINGLIST.eq(fkMissinglist))
          .and(T_MISSING.CODE.notIn(codelist));
        // @formatter:on
        LOGGER.debug("{}", sql3.toString());
        lrw.addToInteger(sql3.execute());

        for (MissingBean bean : missinglist) {
          InsertReturningStep<TMissingRecord> sql4 = DSL.using(t)
          // @formatter:off
            .insertInto(T_MISSING,
                        T_MISSING.CODE,
                        T_MISSING.NAME_IN_STUDY,
                        T_MISSING.FK_MISSINGLIST,
                        T_MISSING.FK_MISSINGTYPE)
            .values(bean.getCode(), bean.getNameInStudy(), fkMissinglist, bean.getPkType())
            .onConflict(T_MISSING.FK_MISSINGLIST, T_MISSING.CODE)
            .doNothing();
          // @formatter:on
          LOGGER.debug("{}", sql4.toString());
          lrw.addToInteger(sql4.execute());
        }
      });
      return lrw.getInteger();
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }

  /**
   * get all missing type names as a comma separated list
   * 
   * @return a list of missing type names
   */
  public List<String> getAllMissingtypenames() {
    try (CloseableDSLContext jooq = getJooq()) {
      SelectWhereStep<TMissingtypeRecord> sql = jooq.selectFrom(T_MISSINGTYPE);
      LOGGER.debug("{}", sql.toString());
      List<String> list = new ArrayList<>();
      for (TMissingtypeRecord r : sql.fetch()) {
        list.add(r.getName());
      }
      return list;
    } catch (DataAccessException | ClassNotFoundException | SQLException e) {
      throw new DataAccessException(e.getMessage(), e);
    }
  }
}
