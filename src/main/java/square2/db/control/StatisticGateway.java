package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_CALCPERF;
import static de.ship.dbppsquare.square.Tables.T_FUNCTION;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONCATEGORY;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONINPUT;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONINPUTTYPE;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONOUTPUT;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONOUTPUTTYPE;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONREFERENCE;
import static de.ship.dbppsquare.square.Tables.T_FUNCTIONSCALE;
import static de.ship.dbppsquare.square.Tables.T_GROUPPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_MATRIXCOLUMN;
import static de.ship.dbppsquare.square.Tables.T_MATRIXCOLUMNEXCLUDESCALE;
import static de.ship.dbppsquare.square.Tables.T_MATRIXCOLUMNPARAMETER;
import static de.ship.dbppsquare.square.Tables.T_MATRIXELEMENTPARAMETER;
import static de.ship.dbppsquare.square.Tables.T_MATRIXELEMENTVALUE;
import static de.ship.dbppsquare.square.Tables.T_PERSON;
import static de.ship.dbppsquare.square.Tables.T_PRIVILEGE;
import static de.ship.dbppsquare.square.Tables.T_REPORT;
import static de.ship.dbppsquare.square.Tables.T_SCALE;
import static de.ship.dbppsquare.square.Tables.T_SNIPPLET;
import static de.ship.dbppsquare.square.Tables.T_TRANSLATION;
import static de.ship.dbppsquare.square.Tables.T_USERPRIVILEGE;
import static de.ship.dbppsquare.square.Tables.V_FUNCTION;
import static de.ship.dbppsquare.square.Tables.V_FUNCTIONOUTPUTTYPE;
import static de.ship.dbppsquare.square.Tables.V_FUNCTIONPRIVS;
import static de.ship.dbppsquare.square.Tables.V_FUNCTIONREFERENCE;
import static de.ship.dbppsquare.square.Tables.V_FUNCTIONRIGHT;
import static de.ship.dbppsquare.square.Tables.V_PRIVILEGE;
import static de.ship.dbppsquare.squareout.Tables.T_CALCULATIONRESULT;

import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.Condition;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.InsertResultStep;
import org.jooq.InsertReturningStep;
import org.jooq.InsertValuesStep2;
import org.jooq.InsertValuesStep4;
import org.jooq.InsertValuesStep6;
import org.jooq.JSONB;
import org.jooq.Record;
import org.jooq.Record1;
import org.jooq.Record10;
import org.jooq.Record14;
import org.jooq.Record21;
import org.jooq.Record3;
import org.jooq.Record4;
import org.jooq.Record6;
import org.jooq.Record7;
import org.jooq.Record8;
import org.jooq.Record9;
import org.jooq.SelectConditionStep;
import org.jooq.SelectHavingStep;
import org.jooq.SelectJoinStep;
import org.jooq.SelectLimitStep;
import org.jooq.SelectOnConditionStep;
import org.jooq.SelectSeekStep1;
import org.jooq.SelectSeekStep2;
import org.jooq.SelectWhereStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import de.ship.dbppsquare.square.enums.EnumDatasource;
import de.ship.dbppsquare.square.tables.records.TCalcperfRecord;
import de.ship.dbppsquare.square.tables.records.TFunctionRecord;
import de.ship.dbppsquare.square.tables.records.TFunctioncategoryRecord;
import de.ship.dbppsquare.square.tables.records.TFunctioninputRecord;
import de.ship.dbppsquare.square.tables.records.TFunctioninputtypeRecord;
import de.ship.dbppsquare.square.tables.records.TFunctionoutputRecord;
import de.ship.dbppsquare.square.tables.records.TFunctionoutputtypeRecord;
import de.ship.dbppsquare.square.tables.records.TFunctionreferenceRecord;
import de.ship.dbppsquare.square.tables.records.TFunctionscaleRecord;
import de.ship.dbppsquare.square.tables.records.TGroupprivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixcolumnRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixcolumnexcludescaleRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixcolumnparameterRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixelementparameterRecord;
import de.ship.dbppsquare.square.tables.records.TMatrixelementvalueRecord;
import de.ship.dbppsquare.square.tables.records.TPrivilegeRecord;
import de.ship.dbppsquare.square.tables.records.TScaleRecord;
import de.ship.dbppsquare.square.tables.records.TUserprivilegeRecord;
import de.ship.dbppsquare.squareout.tables.records.TCalculationresultRecord;
import square2.db.control.model.EnumVarlist;
import square2.db.control.model.LambdaResultWrapper;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.ProfileBean;
import square2.modules.model.UserAccessLevelBean;
import square2.modules.model.admin.FunctioncategoryBean;
import square2.modules.model.statistic.FunctionBean;
import square2.modules.model.statistic.FunctionInputBean;
import square2.modules.model.statistic.FunctionOutputBean;
import square2.modules.model.statistic.FunctionRightBean;
import square2.modules.model.statistic.FunctionScale;
import square2.modules.model.statistic.FunctioninputtypeBean;
import square2.modules.model.statistic.FunctionoutputtypeBean;
import square2.modules.model.test.FunctionValue;

/**
 * 
 * @author henkej
 * 
 */
public class StatisticGateway extends JooqGateway {
	private static final Logger LOGGER = LogManager.getLogger(StatisticGateway.class);

	/**
	 * generate new Statistic gateway
	 * 
	 * @param facesContext
	 *          context of this gateway
	 */
	public StatisticGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
		return super.getPrivilegeMap(getJooq());
	}

	/**
	 * get all function inputs from function referenced by functionId
	 * 
	 * @param functionId
	 *          id of the function
	 * @return the list of funciton input beans
	 * 
	 *         if anything went wrong on database side
	 */
	public List<FunctionInputBean> getAllFunctionInputs(Integer functionId) {
		try (
				SelectSeekStep1<Record9<Integer, String, String, JSONB, Integer, Integer, String, Integer, String>, Integer> sql = getJooq()
				// @formatter:off
					.select(T_FUNCTIONINPUT.PK,
							    T_FUNCTIONINPUT.INPUT_NAME, 
									T_FUNCTIONINPUT.DESCRIPTION, 
									T_FUNCTIONINPUT.DEFAULT_VALUE,
									T_FUNCTIONINPUT.ORDERING,
									T_FUNCTIONINPUT.FK_TYPE,
									T_FUNCTIONINPUTTYPE.NAME,
									T_FUNCTIONINPUTTYPE.FK_LANG,
									T_TRANSLATION.VALUE)
					.from(T_FUNCTIONINPUT)
					.leftJoin(T_FUNCTIONINPUTTYPE).on(T_FUNCTIONINPUTTYPE.PK.eq(T_FUNCTIONINPUT.FK_TYPE))
					.leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_FUNCTIONINPUTTYPE.FK_LANG)).and(T_TRANSLATION.ISOCODE.cast(String.class).eq(getFacesContext().getIsocode()))
					.where(T_FUNCTIONINPUT.FK_FUNCTION.eq(functionId))
					.orderBy(T_FUNCTIONINPUT.ORDERING);
				// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<FunctionInputBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				FunctioninputtypeBean typeBean = new FunctioninputtypeBean(r.get(T_FUNCTIONINPUT.FK_TYPE), null,
						r.get(T_TRANSLATION.VALUE));
				typeBean.setName(r.get(T_FUNCTIONINPUTTYPE.NAME));
				FunctionInputBean bean = new FunctionInputBean(r.get(T_FUNCTIONINPUT.PK));
				bean.setDescription(r.get(T_FUNCTIONINPUT.DESCRIPTION));
				bean.setName(r.get(T_FUNCTIONINPUT.INPUT_NAME));
				JSONB standard = r.get(T_FUNCTIONINPUT.DEFAULT_VALUE);
				bean.setStandard(standard == null ? null : standard.data());
				bean.setFkType(typeBean);
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add functionBean to database
	 * 
	 * @param functionBean
	 *          function bean to be added
	 * @return the new id of this function
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer addFunction(FunctionBean functionBean) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record1<Integer>> sql = jooq
			// @formatter:off
				.selectCount()
				.from(T_FUNCTION)
				.where(T_FUNCTION.NAME.eq(functionBean.getName()));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			if (sql.fetchOne(0, int.class) > 0) {
				LOGGER.debug("function name {} still in use, abort adding it", functionBean.getName());
				throw new DataAccessException("function name is still in use; adding it is forbidden");
			}
			functionBean.setVarlist(EnumVarlist.FALSE.getJsfValue());

			jooq.transaction(c -> {
				// ensure to have an own right id for each function so that deleting one is
				// possible
				functionBean.resetRightId(addAndGetPrivilegeId(DSL.using(c), null));
				InsertResultStep<TFunctionRecord> sql1 = DSL.using(c)
				// @formatter:off
					.insertInto(T_FUNCTION,
											T_FUNCTION.DATASOURCE,
											T_FUNCTION.NAME,
											T_FUNCTION.CREATOR,
											T_FUNCTION.DESCRIPTION,
											T_FUNCTION.SUMMARY, 
											T_FUNCTION.BEFORE,
											T_FUNCTION.BODY,
											T_FUNCTION.AFTER,
											T_FUNCTION.TEST,
											T_FUNCTION.VARNAME,
											T_FUNCTION.VARLIST,
											T_FUNCTION.DATAFRAME,
											T_FUNCTION.VARDEF,
											T_FUNCTION.FK_PRIVILEGE,
											T_FUNCTION.VIGNETTE,
											T_FUNCTION.USE_INTERVALS)
					.values(functionBean.getDatasourceAsEnum(),
									functionBean.getName(),
									getFacesContext().getUsnr(),
									functionBean.getDescription(),
									functionBean.getSummary(), 
									functionBean.getBefore(),
									functionBean.getBody(),
									functionBean.getAfter(),
									functionBean.getTest(),
									functionBean.getVarname(),
									functionBean.getVarlistDbvalue(),
									functionBean.getDataframe(),
									functionBean.getVardef(),
									functionBean.getRight(),
									functionBean.getVignette(),
									functionBean.getUseIntervals())
					.returning(T_FUNCTION.PK);
				// @formatter:on
				LOGGER.debug("{}", sql1.toString());
				Integer functionId = sql1.fetchOne().get(T_FUNCTION.PK);
				lrw.setInteger(functionId);

				UpdateConditionStep<TPrivilegeRecord> sql2 = DSL.using(c)
				// @formatter:off
					.update(T_PRIVILEGE)
					.set(T_PRIVILEGE.NAME, new StringBuilder("function").append(functionId).toString())
					.where(T_PRIVILEGE.PK.eq(functionBean.getRight()));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				sql2.execute();

				int order = 0;
				for (FunctionInputBean bean : functionBean.getInput()) {
					order += 10;
					String standard = bean.getStandard();
					InsertValuesStep6<TFunctioninputRecord, Integer, String, JSONB, Integer, String, Integer> sql3 = DSL.using(c)
					// @formatter:off
						.insertInto(T_FUNCTIONINPUT, 
												T_FUNCTIONINPUT.FK_FUNCTION, 
												T_FUNCTIONINPUT.INPUT_NAME,
												T_FUNCTIONINPUT.DEFAULT_VALUE,
												T_FUNCTIONINPUT.ORDERING,
												T_FUNCTIONINPUT.DESCRIPTION,
												T_FUNCTIONINPUT.FK_TYPE)
						.values(functionId, bean.getName(), standard == null ? null : JSONB.jsonb(standard), order, bean.getDescription(),
									bean.getFkType().getPk());
					// @formatter:on
					LOGGER.debug("{}", sql3.toString());
					sql3.execute();
				}
				for (FunctionOutputBean o : functionBean.getOutput()) {
					InsertValuesStep4<TFunctionoutputRecord, Integer, String, String, Integer> sql3 = DSL.using(c)
					// @formatter:off
						.insertInto(T_FUNCTIONOUTPUT, 
												T_FUNCTIONOUTPUT.FK_FUNCTION,
												T_FUNCTIONOUTPUT.NAME,
												T_FUNCTIONOUTPUT.DESCRIPTION, 
												T_FUNCTIONOUTPUT.TYPE)
						.values(functionId, o.getName(), o.getDescription(), o.getUsage());
					// @formatter:on
					LOGGER.debug("{}", sql3.toString());
					sql3.execute();
				}
				for (FunctionScale scale : functionBean.getScales()) {
					if (scale.getChecked()) {
						InsertValuesStep2<TFunctionscaleRecord, Integer, Integer> sql3 = DSL.using(c)
						// @formatter:off
							.insertInto(T_FUNCTIONSCALE, 
													T_FUNCTIONSCALE.FK_FUNCTION, 
													T_FUNCTIONSCALE.FK_SCALE)
							.values(functionId, scale.getPk());
						// @formatter:on
						LOGGER.debug("{}", sql3.toString());
						sql3.execute();
					}
				}
				for (FunctionBean f : functionBean.getReference()) {
					InsertValuesStep2<TFunctionreferenceRecord, Integer, Integer> sql3 = DSL.using(c)
					// @formatter:off
						.insertInto(T_FUNCTIONREFERENCE, 
												T_FUNCTIONREFERENCE.FK_FUNCTION,
												T_FUNCTIONREFERENCE.REQUIRED_FUNCTION)
						.values(functionId, f.getFunctionId());
					// @formatter:on
					LOGGER.debug("{}", sql3.toString());
					sql3.execute();
				}
				List<String> names = new ArrayList<>();
				for (FunctioncategoryBean b : functionBean.getCategory()) {
					InsertReturningStep<TFunctioncategoryRecord> sql4 = DSL.using(c)
					// @formatter:off
						.insertInto(T_FUNCTIONCATEGORY,
								        T_FUNCTIONCATEGORY.FK_FUNCTION,
								        T_FUNCTIONCATEGORY.NAME, 
								        T_FUNCTIONCATEGORY.DESCRIPTION)
						.values(functionId, b.getName(), b.getDescription())
						.onConflict(T_FUNCTIONCATEGORY.FK_FUNCTION, T_FUNCTIONCATEGORY.NAME)
						.doNothing();
					// @formatter:on
					LOGGER.debug("{}", sql4.toString());
					sql4.execute();
					names.add(b.getName());
				}
				DeleteConditionStep<TFunctioncategoryRecord> sql5 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTIONCATEGORY)
					.where(T_FUNCTIONCATEGORY.FK_FUNCTION.eq(functionId))
					.and(T_FUNCTIONCATEGORY.NAME.notIn(names));
				// @formatter:on
				LOGGER.debug("{}", sql5.toString());
				sql5.execute();
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all functions from db
	 * 
	 * @param activeOnly
	 *          set to true if only activated functions are to be used
	 * @return the list of function beans
	 * 
	 *         if anything went wrong on database side
	 */
	public List<FunctionBean> getAllFunctions(boolean activeOnly) {
		try (SelectConditionStep<Record> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTION.PK,
							T_FUNCTION.NAME, 
							T_FUNCTION.CREATOR,
							T_FUNCTION.CREATIONDATE, 
							T_PERSON.FORENAME,
							T_PERSON.SURNAME,
							T_PERSON.USERNAME,
							T_FUNCTION.DESCRIPTION,
							T_FUNCTION.ACTIVATED,
							T_FUNCTION.SUMMARY,
							T_FUNCTION.BEFORE,
							T_FUNCTION.BODY,
							T_FUNCTION.AFTER,
							T_FUNCTION.TEST, 
							T_FUNCTION.VARNAME, 
							T_FUNCTION.VARLIST, 
							T_FUNCTION.DATAFRAME,
							T_FUNCTION.VARDEF, 
							T_FUNCTION.DATASOURCE,
							T_FUNCTION.FK_PRIVILEGE, 
							T_FUNCTION.LASTCHANGE,
							T_FUNCTION.VIGNETTE,
							V_PRIVILEGE.ACL,
							T_FUNCTION.USE_INTERVALS)
			.from(T_FUNCTION)
			.leftJoin(T_PERSON).on(T_PERSON.USNR.eq(T_FUNCTION.CREATOR))
			.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_FUNCTION.FK_PRIVILEGE).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr())))
			// in(activeOnly, true) means in(true, true) for activated functions only,
			// in(false, true) for all functions; assumed that activated is not null
			.where(T_FUNCTION.ACTIVATED.in(activeOnly, true));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<FunctionBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer functionId = r.get(T_FUNCTION.PK);
				LocalDateTime ldt = r.get(T_FUNCTION.CREATIONDATE);
				Date creationDate = ldt == null ? null : Date.valueOf(ldt.atZone(ZoneId.systemDefault()).toLocalDate());
				List<FunctionScale> scales = getAllFunctionscales(functionId);
        FunctionBean bean = new FunctionBean(r.get(T_FUNCTION.FK_PRIVILEGE), creationDate,
            r.get(T_FUNCTION.USE_INTERVALS), scales);
				bean.setFunctionId(functionId);
				bean.setAcl(new AclBean(r.get(V_PRIVILEGE.ACL)));
				bean.setName(r.get(T_FUNCTION.NAME));
				ProfileBean creator = new ProfileBean();
				creator.setUsnr(r.get(T_FUNCTION.CREATOR));
				creator.setForename(r.get(T_PERSON.FORENAME));
				creator.setSurname(r.get(T_PERSON.SURNAME));
				creator.setUsername(r.get(T_PERSON.USERNAME));
				bean.setCreator(creator);
				bean.setDescription(r.get(T_FUNCTION.DESCRIPTION));
				bean.setActivated(r.get(T_FUNCTION.ACTIVATED));
				bean.setSummary(r.get(T_FUNCTION.SUMMARY));
				bean.setBefore(r.get(T_FUNCTION.BEFORE));
				bean.setBody(r.get(T_FUNCTION.BODY));
				bean.setAfter(r.get(T_FUNCTION.AFTER));
				bean.setTest(r.get(T_FUNCTION.TEST));
				bean.setVarname(r.get(T_FUNCTION.VARNAME));
				bean.setVarlistDbValue(r.get(T_FUNCTION.VARLIST));
				bean.setDatasource(r.get(T_FUNCTION.DATASOURCE));
				bean.setDataframe(r.get(T_FUNCTION.DATAFRAME));
				bean.setVardef(r.get(T_FUNCTION.VARDEF));
				LocalDateTime ldt2 = r.get(T_FUNCTION.LASTCHANGE);
				Timestamp lastchange = ldt2 == null ? null : Timestamp.valueOf(ldt2);
				bean.setLastchange(lastchange);
				bean.getCategory().addAll(getFunctioncategories(bean.getFunctionId()));
				bean.setVignette(r.get(T_FUNCTION.VIGNETTE));
				list.add(bean);
			}
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update function
	 * 
	 * @param functionBean
	 *          contains update information; the bean is referenced by the function
	 *          name (that is unique in the database)
	 * 
	 *          if anything went wrong on database side
	 */
	public void updateFunction(FunctionBean functionBean) {
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				Integer id = functionBean.getFunctionId();
				SelectConditionStep<Record1<Integer>> sql = DSL.using(c)
				// @formatter:off
					.select(T_FUNCTION.PK)
					.from(T_FUNCTION)
					.where(T_FUNCTION.NAME.eq(functionBean.getName()));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				for (Record r : sql.fetch()) {
					Integer pk = r.get(T_FUNCTION.PK);
					if (pk == null || !pk.equals(id)) {
						throw new ParametrizedDataAccessException("error.duplicate.name", functionBean.getName());
					}
				}
				DeleteConditionStep<TMatrixelementparameterRecord> sql1 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTPARAMETER)
					.where(T_MATRIXELEMENTPARAMETER.FK_FUNCTIONINPUT.in(DSL.using(c)
						.select(T_FUNCTIONINPUT.PK)
						.from(T_FUNCTIONINPUT)
						.where(T_FUNCTIONINPUT.INPUT_NAME.notIn(functionBean.getFunctionInputNames()))
						.and(T_FUNCTIONINPUT.FK_FUNCTION.eq(id))));
				// @formatter:on
				LOGGER.debug("{}", sql1.toString());
				sql1.execute();

				DeleteConditionStep<TFunctioninputRecord> sql2 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTIONINPUT)
					.where(T_FUNCTIONINPUT.INPUT_NAME.notIn(functionBean.getFunctionInputNames()))
					.and(T_FUNCTIONINPUT.FK_FUNCTION.eq(id));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				sql2.execute();

				DeleteConditionStep<TCalculationresultRecord> sql2_5 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_CALCULATIONRESULT)
					.where(T_CALCULATIONRESULT.FK_FUNCTIONOUTPUT.in(DSL.using(c)
						.select(T_FUNCTIONOUTPUT.PK)
						.from(T_FUNCTIONOUTPUT)
						.where(T_FUNCTIONOUTPUT.NAME.notIn(functionBean.getFunctionOutputNames()))
						.and(T_FUNCTIONOUTPUT.FK_FUNCTION.eq(id))));
				// @formatter:on
				LOGGER.debug("{}", sql2_5.toString());
				sql2_5.execute();

				DeleteConditionStep<TFunctionoutputRecord> sql3 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTIONOUTPUT)
					.where(T_FUNCTIONOUTPUT.NAME.notIn(functionBean.getFunctionOutputNames()))
					.and(T_FUNCTIONOUTPUT.FK_FUNCTION.eq(id));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				sql3.execute();

				DeleteConditionStep<TFunctionreferenceRecord> sql4 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTIONREFERENCE)
					.where(T_FUNCTIONREFERENCE.REQUIRED_FUNCTION.notIn(functionBean.getReferencedFunctions()))
					.and(T_FUNCTIONREFERENCE.FK_FUNCTION.eq(id));
				// @formatter:on
				LOGGER.debug("{}", sql4.toString());
				sql4.execute();

				List<Integer> scalesList = new ArrayList<>();
				for (FunctionScale scale : functionBean.getScales()) {
					scalesList.add(scale.getPk());
				}

				DeleteConditionStep<TFunctionscaleRecord> sql5 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTIONSCALE)
					.where(T_FUNCTIONSCALE.FK_SCALE.notIn(scalesList))
					.and(T_FUNCTIONSCALE.FK_FUNCTION.eq(id));
				// @formatter:on
				LOGGER.debug("{}", sql5.toString());
				sql5.execute();

				UpdateConditionStep<TFunctionRecord> sql6 = DSL.using(c)
				// @formatter:off
					.update(T_FUNCTION)
					.set(T_FUNCTION.NAME, functionBean.getName())
					.set(T_FUNCTION.DESCRIPTION, functionBean.getDescription())
					.set(T_FUNCTION.SUMMARY, functionBean.getSummary())
					.set(T_FUNCTION.BEFORE, functionBean.getBefore())
					.set(T_FUNCTION.BODY, functionBean.getBody())
					.set(T_FUNCTION.AFTER, functionBean.getAfter())
					.set(T_FUNCTION.VARNAME, functionBean.getVarname())
					.set(T_FUNCTION.VARLIST, functionBean.getVarlistDbvalue())
					.set(T_FUNCTION.DATAFRAME, functionBean.getDataframe())
					.set(T_FUNCTION.VARDEF, functionBean.getVardef())
					.set(T_FUNCTION.DATASOURCE, functionBean.getDatasourceAsEnum())
					.set(T_FUNCTION.TEST, functionBean.getTest())
					.set(T_FUNCTION.ACTIVATED, functionBean.getActivated())
					.set(T_FUNCTION.USE_INTERVALS, functionBean.getUseIntervals())
					.set(T_FUNCTION.VIGNETTE, functionBean.getVignette())
					.where(T_FUNCTION.PK.eq(id));
				// @formatter:on
				LOGGER.debug("{}", sql6.toString());
				sql6.execute();

				int order = 0;
				for (FunctionInputBean bean : functionBean.getInput()) {
					order += 10;
					String standard = bean.getStandard();
					InsertOnDuplicateSetMoreStep<TFunctioninputRecord> sqlI = DSL.using(c)
					// @formatter:off
						.insertInto(T_FUNCTIONINPUT, 
												T_FUNCTIONINPUT.FK_FUNCTION, 
												T_FUNCTIONINPUT.INPUT_NAME,
												T_FUNCTIONINPUT.DEFAULT_VALUE, 
												T_FUNCTIONINPUT.ORDERING,
												T_FUNCTIONINPUT.DESCRIPTION,
												T_FUNCTIONINPUT.FK_TYPE)
						.values(id, 
										bean.getName(),
										standard == null ? null : JSONB.jsonb(standard), 
										order, 
										bean.getDescription(),
										bean.getFkType().getPk())
						.onConflict(T_FUNCTIONINPUT.FK_FUNCTION, T_FUNCTIONINPUT.INPUT_NAME)
						.doUpdate()
						.set(T_FUNCTIONINPUT.DEFAULT_VALUE, standard == null ? null : JSONB.jsonb(standard))
						.set(T_FUNCTIONINPUT.ORDERING, order)
						.set(T_FUNCTIONINPUT.DESCRIPTION, bean.getDescription())
						.set(T_FUNCTIONINPUT.FK_TYPE, bean.getFkType().getPk());
					// @formatter:on
					LOGGER.debug("{}", sqlI.toString());
					sqlI.execute();
				}
				for (FunctionOutputBean o : functionBean.getOutput()) {
					InsertOnDuplicateSetMoreStep<TFunctionoutputRecord> sqlI = DSL.using(c)
					// @formatter:off
						.insertInto(T_FUNCTIONOUTPUT, 
												T_FUNCTIONOUTPUT.FK_FUNCTION,
												T_FUNCTIONOUTPUT.NAME,
												T_FUNCTIONOUTPUT.DESCRIPTION, 
												T_FUNCTIONOUTPUT.TYPE)
						.values(id, o.getName(), o.getDescription(), o.getUsage())
						.onConflict(T_FUNCTIONOUTPUT.FK_FUNCTION, T_FUNCTIONOUTPUT.NAME)
						.doUpdate()
						.set(T_FUNCTIONOUTPUT.DESCRIPTION, o.getDescription())
						.set(T_FUNCTIONOUTPUT.TYPE, o.getUsage());
					// @formatter:on
					LOGGER.debug("{}", sqlI.toString());
					sqlI.execute();
				}
				for (FunctionScale scale : functionBean.getScales()) {
					if (scale.getChecked()) {
						InsertReturningStep<TFunctionscaleRecord> sqlI = DSL.using(c)
						// @formatter:off
							.insertInto(T_FUNCTIONSCALE, 
													T_FUNCTIONSCALE.FK_FUNCTION,
													T_FUNCTIONSCALE.FK_SCALE)
							.values(id, scale.getPk())
							.onConflict(T_FUNCTIONSCALE.FK_FUNCTION, T_FUNCTIONSCALE.FK_SCALE)
							.doNothing();
						// @formatter:on
						LOGGER.debug("{}", sqlI.toString());
						sqlI.execute();
					} else {
						DeleteConditionStep<TFunctionscaleRecord> sqlI = DSL.using(c)
						// @formatter:off
							.deleteFrom(T_FUNCTIONSCALE)
							.where(T_FUNCTIONSCALE.FK_SCALE.eq(scale.getPk()))
							.and(T_FUNCTIONSCALE.FK_FUNCTION.eq(id));
						// @formatter:on
						LOGGER.debug("{}", sqlI.toString());
						sqlI.execute();
					}
				}
				for (FunctionBean f : functionBean.getReference()) {
					InsertReturningStep<TFunctionreferenceRecord> sqlI = DSL.using(c)
					// @formatter:off
						.insertInto(T_FUNCTIONREFERENCE, 
												T_FUNCTIONREFERENCE.FK_FUNCTION,
												T_FUNCTIONREFERENCE.REQUIRED_FUNCTION)
						.values(id, f.getFunctionId())
						.onConflict(T_FUNCTIONREFERENCE.FK_FUNCTION, T_FUNCTIONREFERENCE.REQUIRED_FUNCTION)
						.doNothing();
					// @formatter:on
					LOGGER.debug("{}", sqlI.toString());
					sqlI.execute();
				}
				List<String> names = new ArrayList<>();
				for (FunctioncategoryBean b : functionBean.getCategory()) {
					InsertReturningStep<TFunctioncategoryRecord> sqlI = DSL.using(c)
					// @formatter:off
						.insertInto(T_FUNCTIONCATEGORY,
								        T_FUNCTIONCATEGORY.FK_FUNCTION,
								        T_FUNCTIONCATEGORY.NAME, 
								        T_FUNCTIONCATEGORY.DESCRIPTION)
						.values(id, b.getName(), b.getDescription())
						.onConflict(T_FUNCTIONCATEGORY.FK_FUNCTION, T_FUNCTIONCATEGORY.NAME)
						.doNothing();
					// @formatter:on
					LOGGER.debug("{}", sqlI.toString());
					sqlI.execute();
					names.add(b.getName());
				}
				DeleteConditionStep<TFunctioncategoryRecord> sql7 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTIONCATEGORY)
					.where(T_FUNCTIONCATEGORY.FK_FUNCTION.eq(id))
					.and(T_FUNCTIONCATEGORY.NAME.notIn(names));
				// @formatter:on
				LOGGER.debug("{}", sql7.toString());
				sql7.execute();

			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * load function bean and all it's content referenced by functionId
	 * 
	 * @param functionId
	 *          the id of the function
	 * @return the function bean
	 * 
	 *         if anything went wrong on database side
	 */
	public FunctionBean getFunctionBean(Integer functionId) {
		if (functionId == null) {
			return null;
		}
		List<FunctionScale> scales = getAllFunctionscales(functionId);

		FunctionBean bean = null;
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record21<String, String, String, String, String, String, String, String, Boolean, String, String, EnumDatasource, Integer, Boolean, LocalDateTime, Boolean, String, String, String, String, EnumAccesslevel>> sql = jooq
			// @formatter:off
				.select(T_FUNCTION.NAME,
								T_FUNCTION.DESCRIPTION,
								T_FUNCTION.SUMMARY, 
								T_FUNCTION.BEFORE,
								T_FUNCTION.BODY, 
								T_FUNCTION.AFTER,
								T_FUNCTION.TEST, 
								T_FUNCTION.VARNAME,
								T_FUNCTION.VARLIST,
								T_FUNCTION.DATAFRAME, 
								T_FUNCTION.VARDEF,
								T_FUNCTION.DATASOURCE,
								T_FUNCTION.FK_PRIVILEGE,
								T_FUNCTION.ACTIVATED,
								T_FUNCTION.CREATIONDATE,
								T_FUNCTION.USE_INTERVALS,
								T_FUNCTION.VIGNETTE,
								V_FUNCTION.BODY_FUNCTION,
								V_FUNCTION.BEFORE_FUNCTION,
								V_FUNCTION.AFTER_FUNCTION, 
								V_PRIVILEGE.ACL)
				.from(T_FUNCTION)
				.leftJoin(V_FUNCTION).on(V_FUNCTION.PK.eq(T_FUNCTION.PK))
				.leftJoin(V_FUNCTIONPRIVS).on(V_FUNCTIONPRIVS.PK.eq(T_FUNCTION.PK))
				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_FUNCTION.FK_PRIVILEGE).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr())))
				.where(T_FUNCTION.PK.eq(functionId));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
			  LocalDateTime ldt = r.get(T_FUNCTION.CREATIONDATE);
			  Date creationDate = ldt == null ? null : Date.valueOf(ldt.atZone(ZoneId.systemDefault()).toLocalDate());
				bean = new FunctionBean(r.get(T_FUNCTION.FK_PRIVILEGE), creationDate,
						r.get(T_FUNCTION.USE_INTERVALS), scales);
				bean.setFunctionId(functionId);
				bean.setAcl(new AclBean(r.get(V_PRIVILEGE.ACL)));
				bean.setName(r.get(T_FUNCTION.NAME));
				bean.setDescription(r.get(T_FUNCTION.DESCRIPTION));
				bean.setSummary(r.get(T_FUNCTION.SUMMARY));
				bean.setBefore(r.get(T_FUNCTION.BEFORE));
				bean.setBody(r.get(T_FUNCTION.BODY));
				bean.setAfter(r.get(T_FUNCTION.AFTER));
				bean.setTest(r.get(T_FUNCTION.TEST));
				bean.setVarname(r.get(T_FUNCTION.VARNAME));
				bean.setVarlistDbValue(r.get(T_FUNCTION.VARLIST));
				bean.setDatasource(r.get(T_FUNCTION.DATASOURCE));
				bean.setActivated(r.get(T_FUNCTION.ACTIVATED));
				bean.setDataframe(r.get(T_FUNCTION.DATAFRAME));
				bean.setVardef(r.get(T_FUNCTION.VARDEF));
				bean.setVignette(r.get(T_FUNCTION.VIGNETTE));
				bean.setForR(r.get(V_FUNCTION.BODY_FUNCTION));
				bean.getCategory().addAll(getFunctioncategories(bean.getFunctionId()));
			}
			if (bean == null) {
				throw new DataAccessException("no function found for functionId = ".concat(functionId.toString()));
			}
			SelectSeekStep1<Record9<Integer, String, JSONB, Integer, String, Integer, String, Integer, String>, Integer> sql2 = jooq
			// @formatter:off
				.select(T_FUNCTIONINPUT.PK,
						    T_FUNCTIONINPUT.INPUT_NAME, 
								T_FUNCTIONINPUT.DEFAULT_VALUE,
								T_FUNCTIONINPUT.ORDERING,
								T_FUNCTIONINPUT.DESCRIPTION,
								T_FUNCTIONINPUT.FK_TYPE, 
								T_FUNCTIONINPUTTYPE.NAME,
								T_FUNCTIONINPUTTYPE.FK_LANG, 
								T_TRANSLATION.VALUE)
				.from(T_FUNCTIONINPUT)
				.leftJoin(T_FUNCTIONINPUTTYPE).on(T_FUNCTIONINPUTTYPE.PK.eq(T_FUNCTIONINPUT.FK_TYPE))
				.leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_FUNCTIONINPUTTYPE.FK_LANG)).and(T_TRANSLATION.ISOCODE.cast(String.class).eq(getFacesContext().getIsocode()))
				.where(T_FUNCTIONINPUT.FK_FUNCTION.eq(functionId)).orderBy(T_FUNCTIONINPUT.ORDERING);
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());
			for (Record r : sql2.fetch()) {
				FunctioninputtypeBean fType = new FunctioninputtypeBean(r.get(T_FUNCTIONINPUT.FK_TYPE), null,
						r.get(T_TRANSLATION.VALUE));
				fType.setName(r.get(T_FUNCTIONINPUTTYPE.NAME));
				FunctionInputBean fib = new FunctionInputBean(r.get(T_FUNCTIONINPUT.PK));
				fib.setName(r.get(T_FUNCTIONINPUT.INPUT_NAME));
				JSONB standard = r.get(T_FUNCTIONINPUT.DEFAULT_VALUE);
				fib.setStandard(standard == null ? null : standard.data());
				fib.setDescription(r.get(T_FUNCTIONINPUT.DESCRIPTION));
				fib.setFkType(fType);
				bean.getInput().add(fib);
			}
			SelectConditionStep<Record4<Integer, String, String, Integer>> sql3 = jooq
			// @formatter:off
				.select(T_FUNCTIONOUTPUT.PK, 
								T_FUNCTIONOUTPUT.NAME,
								T_FUNCTIONOUTPUT.DESCRIPTION,
								T_FUNCTIONOUTPUT.TYPE)
				.from(T_FUNCTIONOUTPUT)
				.where(T_FUNCTIONOUTPUT.FK_FUNCTION.eq(functionId));
			// @formatter:on
			LOGGER.debug("{}", sql3.toString());
			for (Record r : sql3.fetch()) {
				FunctionOutputBean fob = new FunctionOutputBean(r.get(T_FUNCTIONOUTPUT.PK));
				fob.setName(r.get(T_FUNCTIONOUTPUT.NAME));
				fob.setDescription(r.get(T_FUNCTIONOUTPUT.DESCRIPTION));
				fob.setUsage(r.get(T_FUNCTIONOUTPUT.TYPE));
				bean.getOutput().add(fob);
			}
			SelectConditionStep<Record8<Integer, String, String, String, Integer, LocalDateTime, String, Boolean>> sql4 = jooq
			// @formatter:off
				.select(T_FUNCTIONREFERENCE.REQUIRED_FUNCTION,
								T_FUNCTION.NAME,
								T_FUNCTION.DESCRIPTION,
								T_FUNCTION.SUMMARY,
								T_FUNCTION.FK_PRIVILEGE, 
								T_FUNCTION.CREATIONDATE,
								T_FUNCTION.VIGNETTE,
								T_FUNCTION.USE_INTERVALS)
				.from(T_FUNCTIONREFERENCE)
				.innerJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(T_FUNCTIONREFERENCE.REQUIRED_FUNCTION))
				.where(T_FUNCTIONREFERENCE.FK_FUNCTION.eq(bean.getFunctionId()));
			// @formatter:on
			LOGGER.debug("{}", sql4.toString());
			for (Record r : sql4.fetch()) {
	      LocalDateTime ldt = r.get(T_FUNCTION.CREATIONDATE);
	      Date creationDate = ldt == null ? null : Date.valueOf(ldt.atZone(ZoneId.systemDefault()).toLocalDate());
				FunctionBean referenceBean = new FunctionBean(r.get(T_FUNCTION.FK_PRIVILEGE), creationDate,
						r.get(T_FUNCTION.USE_INTERVALS), scales);
				referenceBean.setFunctionId(r.get(T_FUNCTIONREFERENCE.REQUIRED_FUNCTION));
				referenceBean.setName(r.get(T_FUNCTION.NAME));
				referenceBean.setDescription(r.get(T_FUNCTION.DESCRIPTION));
				referenceBean.setSummary(r.get(T_FUNCTION.SUMMARY));
				referenceBean.setVignette(r.get(T_FUNCTION.VIGNETTE));
				bean.getReference().add(referenceBean);
			}
			return bean;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete function complete
	 * 
	 * @param functionId
	 *          the id of the function
	 * @param fkPrivilege
	 *          the id of the privilege
	 * @return number of affected database rows
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer deleteFunction(Integer functionId, Integer fkPrivilege) {
		try (CloseableDSLContext jooq = getJooq()) {
			SelectConditionStep<Record1<String>> sql = jooq
			// @formatter:off
				.select(T_REPORT.NAME)
				.from(T_SNIPPLET)
				.leftJoin(T_REPORT).on(T_REPORT.PK.eq(T_SNIPPLET.FK_REPORT))
				.leftJoin(T_MATRIXCOLUMN).on(T_MATRIXCOLUMN.PK.eq(T_SNIPPLET.FK_MATRIXCOLUMN))
				.where(T_MATRIXCOLUMN.FK_FUNCTION.eq(functionId));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			StringBuilder reportsBuf = new StringBuilder();
			for (Record r : sql.fetch()) {
				reportsBuf.append(r.get(T_REPORT.NAME)).append(", ");
			}
			String reports = reportsBuf.toString();
			if (reports != null && !reports.isEmpty()) {
				getFacesContext().notifyError("error.statistic.delete.existingreports", reports);
				return null;
			}
			SelectConditionStep<Record1<Integer>> sqla = jooq
			// @formatter:off
				.select(T_FUNCTIONREFERENCE.FK_FUNCTION)
				.from(T_FUNCTIONREFERENCE)
				.where(T_FUNCTIONREFERENCE.REQUIRED_FUNCTION.eq(functionId));
			// @formatter:on
			LOGGER.debug("{}", sqla.toString());
			List<Integer> usages = new ArrayList<>();
			for (Record r : sqla.fetch()) {
				usages.add(r.get(T_FUNCTIONREFERENCE.FK_FUNCTION));
			}
			StringBuilder functionUsers = new StringBuilder();
			for (Integer i : usages) {
				SelectConditionStep<Record1<String>> sql0 = jooq
				// @formatter:off
					.select(T_FUNCTION.NAME)
					.from(T_FUNCTION)
					.where(T_FUNCTION.PK.eq(i));
				// @formatter:on
				LOGGER.debug("{}", sql0.toString());
				for (Record r : sql0.fetch()) {
					functionUsers.append(r.get(T_FUNCTION.NAME));
				}
				functionUsers.append(" ");
			}
			if (usages.size() > 0) {
				getFacesContext().notifyError("error.statistic.delete.usedbyfunctions", functionUsers.toString());
				return null;
			}
			LambdaResultWrapper lrw = new LambdaResultWrapper();
			jooq.transaction(c -> {
				DeleteConditionStep<TCalcperfRecord> sql1 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_CALCPERF)
					.where(T_CALCPERF.FK_FUNCTION.eq(functionId));
				// @formatter:on
				LOGGER.debug("{}", sql1.toString());
				lrw.addToInteger(sql1.execute());

				DeleteConditionStep<TCalculationresultRecord> sql2 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_CALCULATIONRESULT)
					.where(T_CALCULATIONRESULT.FK_MATRIXELEMENTVALUE.in(DSL.using(c)
						.select(T_MATRIXELEMENTVALUE.PK)
						.from(T_MATRIXELEMENTVALUE)
						.where(T_MATRIXELEMENTVALUE.FK_VARIABLEGROUPELEMENT.in(DSL.using(c)
							.select(T_MATRIXCOLUMN.PK)
							.from(T_MATRIXCOLUMN)
							.where(T_MATRIXCOLUMN.FK_FUNCTION.eq(functionId))))))
					.or(T_CALCULATIONRESULT.FK_MATRIXCOLUMN.in(DSL.using(c)
						.select(T_MATRIXCOLUMN.PK)
						.from(T_MATRIXCOLUMN)
						.where(T_MATRIXCOLUMN.FK_FUNCTION.eq(functionId))));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());

				DeleteConditionStep<TMatrixelementparameterRecord> sql3 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTPARAMETER)
					.where(T_MATRIXELEMENTPARAMETER.FK_MATRIXELEMENTVALUE.in(DSL.using(c)
						.select(T_MATRIXELEMENTVALUE.PK)
						.from(T_MATRIXELEMENTVALUE)
						.where(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.in(DSL.using(c)
							.select(T_MATRIXCOLUMN.PK)
							.from(T_MATRIXCOLUMN)
							.where(T_MATRIXCOLUMN.FK_FUNCTION.eq(functionId))))));
				// @formatter:on
				LOGGER.debug("{}", sql3.toString());
				lrw.addToInteger(sql3.execute());

				DeleteConditionStep<TMatrixelementvalueRecord> sql4 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXELEMENTVALUE)
					.where(T_MATRIXELEMENTVALUE.FK_MATRIXCOLUMN.in(DSL.using(c)
						.select(T_MATRIXCOLUMN.PK)
						.from(T_MATRIXCOLUMN)
						.where(T_MATRIXCOLUMN.FK_FUNCTION.eq(functionId))));
				// @formatter:on
				LOGGER.debug("{}", sql4.toString());
				lrw.addToInteger(sql4.execute());

				DeleteConditionStep<TMatrixcolumnparameterRecord> sql5 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXCOLUMNPARAMETER)
					.where(T_MATRIXCOLUMNPARAMETER.FK_MATRIXCOLUMN.in(DSL.using(c)
						.select(T_MATRIXCOLUMN.PK)
						.from(T_MATRIXCOLUMN)
						.where(T_MATRIXCOLUMN.FK_FUNCTION.eq(functionId))));
				// @formatter:on
				LOGGER.debug("{}", sql5.toString());
				lrw.addToInteger(sql5.execute());

				DeleteConditionStep<TMatrixcolumnexcludescaleRecord> sql6 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXCOLUMNEXCLUDESCALE)
					.where(T_MATRIXCOLUMNEXCLUDESCALE.FK_MATRIXCOLUMN.in(DSL.using(c)
						.select(T_MATRIXCOLUMN.PK)
						.from(T_MATRIXCOLUMN)
						.where(T_MATRIXCOLUMN.FK_FUNCTION.eq(functionId))));
				// @formatter:on
				LOGGER.debug("{}", sql6.toString());
				lrw.addToInteger(sql6.execute());

				DeleteConditionStep<TMatrixcolumnRecord> sql7 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_MATRIXCOLUMN)
					.where(T_MATRIXCOLUMN.FK_FUNCTION.eq(functionId));
				// @formatter:on
				LOGGER.debug("{}", sql7.toString());
				lrw.addToInteger(sql7.execute());

				DeleteConditionStep<TFunctionscaleRecord> sql8 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTIONSCALE)
					.where(T_FUNCTIONSCALE.FK_FUNCTION.eq(functionId));
				// @formatter:on
				LOGGER.debug("{}", sql8.toString());
				lrw.addToInteger(sql8.execute());

				DeleteConditionStep<TFunctionreferenceRecord> sql9 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTIONREFERENCE)
					.where(T_FUNCTIONREFERENCE.FK_FUNCTION.eq(functionId));
				// @formatter:on
				LOGGER.debug("{}", sql9.toString());
				lrw.addToInteger(sql9.execute());

				DeleteConditionStep<TFunctioninputRecord> sql10 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTIONINPUT)
					.where(T_FUNCTIONINPUT.FK_FUNCTION.eq(functionId));
				// @formatter:on
				LOGGER.debug("{}", sql10.toString());
				lrw.addToInteger(sql10.execute());

				DeleteConditionStep<TFunctionoutputRecord> sql11 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTIONOUTPUT)
					.where(T_FUNCTIONOUTPUT.FK_FUNCTION.eq(functionId));
				// @formatter:on
				LOGGER.debug("{}", sql11.toString());
				lrw.addToInteger(sql11.execute());

				DeleteConditionStep<TFunctioncategoryRecord> sql12 = DSL.using(c)
				// @formatter:off
				  .deleteFrom(T_FUNCTIONCATEGORY)
				  .where(T_FUNCTIONCATEGORY.FK_FUNCTION.eq(functionId));
				// @formatter:on
		    LOGGER.debug("{}", sql12.toString());
        lrw.addToInteger(sql12.execute());

				DeleteConditionStep<TFunctionRecord> sql13 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_FUNCTION)
					.where(T_FUNCTION.PK.eq(functionId));
				// @formatter:on
				LOGGER.debug("{}", sql13.toString());
				lrw.addToInteger(sql13.execute());

				DeleteConditionStep<TUserprivilegeRecord> sql14 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_USERPRIVILEGE)
					.where(T_USERPRIVILEGE.FK_PRIVILEGE.eq(fkPrivilege));
				// @formatter:on
				LOGGER.debug("{}", sql14.toString());
				lrw.addToInteger(sql14.execute());

				DeleteConditionStep<TGroupprivilegeRecord> sql15 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_GROUPPRIVILEGE)
					.where(T_GROUPPRIVILEGE.FK_PRIVILEGE.eq(fkPrivilege));
				// @formatter:on
				LOGGER.debug("{}", sql15.toString());
				lrw.addToInteger(sql15.execute());

				DeleteConditionStep<TPrivilegeRecord> sql16 = DSL.using(c)
				// @formatter:off
					.deleteFrom(T_PRIVILEGE)
					.where(T_PRIVILEGE.PK.eq(fkPrivilege));
				// @formatter:on
				LOGGER.debug("{}", sql16.toString());
				lrw.addToInteger(sql16.execute());
			});
			return lrw.getInteger();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get list of function values (values are default or empty)
	 * 
	 * @param id
	 *          the id of the function
	 * @return the list of function values
	 * 
	 *         if anything went wrong on database side
	 */
	public List<FunctionValue> getFunctionValues(Integer id) {
		try (SelectSeekStep1<Record3<String, JSONB, Integer>, Integer> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTIONINPUT.INPUT_NAME, 
							T_FUNCTIONINPUT.DEFAULT_VALUE,
							T_FUNCTIONINPUT.ORDERING)
			.from(T_FUNCTIONINPUT)
			.where(T_FUNCTIONINPUT.FK_FUNCTION.eq(id))
			.orderBy(T_FUNCTIONINPUT.ORDERING);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<FunctionValue> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				FunctionValue bean = new FunctionValue(r.get(T_FUNCTIONINPUT.INPUT_NAME));
				JSONB standard = r.get(T_FUNCTIONINPUT.DEFAULT_VALUE);
				bean.setValue(standard == null ? null : standard.data());
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get list of functions and their content
	 * 
	 * @return the list of function beans
	 * 
	 *         if anything went wrong on database side
	 */
	public List<FunctionBean> getAllFunctions() {
		try (CloseableDSLContext jooq = getJooq()) {

			SelectSeekStep2<Record10<Integer, Integer, String, JSONB, Integer, String, Integer, String, Integer, String>, Integer, Integer> sql = jooq
			// @formatter:off
				.select(T_FUNCTIONINPUT.PK,
						    T_FUNCTIONINPUT.FK_FUNCTION, 
								T_FUNCTIONINPUT.INPUT_NAME,
								T_FUNCTIONINPUT.DEFAULT_VALUE,
								T_FUNCTIONINPUT.ORDERING, 
								T_FUNCTIONINPUT.DESCRIPTION,
								T_FUNCTIONINPUT.FK_TYPE,
								T_FUNCTIONINPUTTYPE.NAME, 
								T_FUNCTIONINPUTTYPE.FK_LANG,
								T_TRANSLATION.VALUE)
				.from(T_FUNCTIONINPUT)
				.leftJoin(T_FUNCTIONINPUTTYPE).on(T_FUNCTIONINPUTTYPE.PK.eq(T_FUNCTIONINPUT.FK_TYPE))
				.leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_FUNCTIONINPUTTYPE.FK_LANG)).and(T_TRANSLATION.ISOCODE.cast(String.class).eq(getFacesContext().getIsocode()))
				.orderBy(T_FUNCTIONINPUT.FK_FUNCTION, T_FUNCTIONINPUT.ORDERING);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Map<Integer, List<FunctionInputBean>> inputs = new HashMap<>();
			for (Record r : sql.fetch()) {
				FunctioninputtypeBean fkType = new FunctioninputtypeBean(r.get(T_FUNCTIONINPUT.FK_TYPE), null,
						r.get(T_TRANSLATION.VALUE));
				fkType.setName(r.get(T_FUNCTIONINPUTTYPE.NAME));
				Integer functionId = r.get(T_FUNCTIONINPUT.FK_FUNCTION);
				List<FunctionInputBean> list = inputs.get(functionId);
				if (list == null) {
					list = new ArrayList<>();
					inputs.put(functionId, list);
				}
				FunctionInputBean bean = new FunctionInputBean(r.get(T_FUNCTIONINPUT.PK));
				bean.setName(r.get(T_FUNCTIONINPUT.INPUT_NAME));
				JSONB standard = r.get(T_FUNCTIONINPUT.DEFAULT_VALUE);
				bean.setStandard(standard == null ? null : standard.data());
				bean.setDescription(r.get(T_FUNCTIONINPUT.DESCRIPTION));
				bean.setFkType(fkType);
				list.add(bean);
			}
			SelectJoinStep<Record4<Integer, Integer, String, String>> sql0 = jooq
			// @formatter:off
				.select(T_FUNCTIONOUTPUT.PK, 
								T_FUNCTIONOUTPUT.FK_FUNCTION, 
								T_FUNCTIONOUTPUT.NAME,
								T_FUNCTIONOUTPUT.DESCRIPTION)
				.from(T_FUNCTIONOUTPUT);
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Map<Integer, List<FunctionOutputBean>> outputs = new HashMap<>();
			for (Record r : sql0.fetch()) {
				Integer functionId = r.get(T_FUNCTIONOUTPUT.FK_FUNCTION);
				List<FunctionOutputBean> list = outputs.get(functionId);
				if (list == null) {
					list = new ArrayList<>();
					outputs.put(functionId, list);
				}
				FunctionOutputBean bean = new FunctionOutputBean(r.get(T_FUNCTIONOUTPUT.PK));
				bean.setName(r.get(T_FUNCTIONOUTPUT.NAME));
				bean.setDescription(r.get(T_FUNCTIONOUTPUT.DESCRIPTION));
				list.add(bean);
			}
			SelectJoinStep<Record7<Integer, Integer, String, Integer, LocalDateTime, Boolean, String>> sql1 = jooq
			// @formatter:off
				.select(V_FUNCTIONREFERENCE.FUNCTION_PK,
								V_FUNCTIONREFERENCE.REFERENCE_PK,
								V_FUNCTIONREFERENCE.REFERENCE_NAME, 
								V_FUNCTIONREFERENCE.REFERENCE_PRIVILEGE,
								T_FUNCTION.CREATIONDATE,
								T_FUNCTION.USE_INTERVALS,
								T_FUNCTION.VIGNETTE)
				.from(V_FUNCTIONREFERENCE)
				.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(V_FUNCTIONREFERENCE.FUNCTION_PK));
			// @formatter:on
			LOGGER.debug("{}", sql.toString());
			Map<Integer, List<FunctionBean>> references = new HashMap<>();
			for (Record r : sql1.fetch()) {
				Integer functionId = r.get(V_FUNCTIONREFERENCE.FUNCTION_PK);
				List<FunctionBean> list = references.get(functionId);
				if (list == null) {
					list = new ArrayList<>();
					references.put(functionId, list);
				}
				Integer thisFunctionId = r.get(V_FUNCTIONREFERENCE.REFERENCE_PK);
        LocalDateTime ldt = r.get(T_FUNCTION.CREATIONDATE);
        Date creationDate = ldt == null ? null : Date.valueOf(ldt.atZone(ZoneId.systemDefault()).toLocalDate());
				List<FunctionScale> scales = getAllFunctionscales(thisFunctionId);
				FunctionBean bean = new FunctionBean(r.get(V_FUNCTIONREFERENCE.REFERENCE_PRIVILEGE),
						creationDate, r.get(T_FUNCTION.USE_INTERVALS), scales);
				bean.setFunctionId(thisFunctionId);
				bean.setName(r.get(V_FUNCTIONREFERENCE.REFERENCE_NAME));
				bean.setVignette(r.get(T_FUNCTION.VIGNETTE));
				list.add(bean);
			}
			SelectLimitStep<Record> sql2 = jooq
			// @formatter:off
				.select(T_FUNCTION.PK, 
								T_FUNCTION.ACTIVATED, 
								T_FUNCTION.NAME,
								T_FUNCTION.CREATOR,
								T_FUNCTION.CREATIONDATE, 
								T_PERSON.FORENAME,
								T_PERSON.SURNAME, 
								T_PERSON.USERNAME,
								T_FUNCTION.DESCRIPTION, 
								T_FUNCTION.SUMMARY, 
								T_FUNCTION.BEFORE,
								T_FUNCTION.BODY, 
								T_FUNCTION.AFTER,
								T_FUNCTION.TEST,
								T_FUNCTION.VARNAME, 
								T_FUNCTION.VARLIST,
								T_FUNCTION.DATAFRAME, 
								T_FUNCTION.VARDEF, 
								T_FUNCTION.DATASOURCE, 
								T_FUNCTION.FK_PRIVILEGE,
								T_FUNCTION.USE_INTERVALS,
								T_FUNCTION.VIGNETTE,
								V_PRIVILEGE.ACL)
				.from(T_FUNCTION)
				.leftJoin(T_PERSON).on(T_PERSON.USNR.eq(T_FUNCTION.CREATOR))
				.leftJoin(V_PRIVILEGE).on(V_PRIVILEGE.FK_PRIVILEGE.eq(T_FUNCTION.FK_PRIVILEGE).and(V_PRIVILEGE.FK_USNR.eq(getFacesContext().getUsnr())))
				.orderBy(1);
			// @formatter:on
			LOGGER.debug("{}", sql2.toString());
			List<FunctionBean> list = new ArrayList<>();
			for (Record r : sql2.fetch()) {
				Integer functionId = r.get(T_FUNCTION.PK);
				List<FunctionScale> scales = getAllFunctionscales(functionId);
        LocalDateTime ldt = r.get(T_FUNCTION.CREATIONDATE);
        Date creationDate = ldt == null ? null : Date.valueOf(ldt.atZone(ZoneId.systemDefault()).toLocalDate());

				FunctionBean bean = new FunctionBean(r.get(T_FUNCTION.FK_PRIVILEGE), creationDate,
						r.get(T_FUNCTION.USE_INTERVALS), scales);
				bean.setFunctionId(functionId);
				bean.setAcl(new AclBean(r.get(V_PRIVILEGE.ACL)));
				bean.setActivated(r.get(T_FUNCTION.ACTIVATED));
				bean.setName(r.get(T_FUNCTION.NAME));
				ProfileBean creator = new ProfileBean();
				creator.setUsnr(r.get(T_FUNCTION.CREATOR));
				creator.setForename(r.get(T_PERSON.FORENAME));
				creator.setSurname(r.get(T_PERSON.SURNAME));
				creator.setUsername(r.get(T_PERSON.USERNAME));
				bean.setCreator(creator);
				bean.setDescription(r.get(T_FUNCTION.DESCRIPTION));
				bean.setSummary(r.get(T_FUNCTION.SUMMARY));
				bean.setBefore(r.get(T_FUNCTION.BEFORE));
				bean.setBody(r.get(T_FUNCTION.BODY));
				bean.setAfter(r.get(T_FUNCTION.AFTER));
				bean.setTest(r.get(T_FUNCTION.TEST));
				bean.setVarname(r.get(T_FUNCTION.VARNAME));
				bean.setVarlistDbValue(r.get(T_FUNCTION.VARLIST));
				bean.setDatasource(r.get(T_FUNCTION.DATASOURCE));
				bean.setDataframe(r.get(T_FUNCTION.DATAFRAME));
				bean.setVardef(r.get(T_FUNCTION.VARDEF));
				bean.setVignette(r.get(T_FUNCTION.VIGNETTE));
				if (inputs.get(bean.getFunctionId()) != null) {
					bean.getInput().addAll(inputs.get(bean.getFunctionId()));
				}
				if (outputs.get(bean.getFunctionId()) != null) {
					bean.getOutput().addAll(outputs.get(bean.getFunctionId()));
				}
				if (references.get(bean.getFunctionId()) != null) {
					bean.getReference().addAll(references.get(bean.getFunctionId()));
				}
				bean.getCategory().addAll(getFunctioncategories(bean.getFunctionId()));
				list.add(bean);
			}
			list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update function activation
	 * 
	 * @param function
	 *          ID of the function that should be (de)activated
	 * @param activated
	 *          status of activation to be set
	 * @return number of affected rows (should be 1)
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer setFunctionActivated(Integer function, Boolean activated) {
		try (UpdateConditionStep<TFunctionRecord> sql = getJooq()
		// @formatter:off
			.update(T_FUNCTION)
			.set(T_FUNCTION.ACTIVATED, activated)
			.where(T_FUNCTION.PK.eq(function));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all function rights for function referenced by functionId
	 * 
	 * @param functionId
	 *          the id of the function
	 * 
	 * @return the list of user access level beans
	 * 
	 *         if anything went wrong on database side
	 */
	public List<UserAccessLevelBean> getAllFunctionRights(Integer functionId) {
		try (SelectConditionStep<Record4<String, String, Integer, EnumAccesslevel>> sql = getJooq()
		// @formatter:off
			.select(V_FUNCTIONRIGHT.USER_FORENAME, 
							V_FUNCTIONRIGHT.USER_SURNAME,
							V_FUNCTIONRIGHT.USER_USNR,
							V_FUNCTIONRIGHT.USER_ACCESS_LEVEL)
			.from(V_FUNCTIONRIGHT)
			.where(V_FUNCTIONRIGHT.PK.eq(functionId))
			.and(V_FUNCTIONRIGHT.USER_ACCESS_LEVEL.isNotNull());
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<UserAccessLevelBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				UserAccessLevelBean ualb = new UserAccessLevelBean();
				ualb.setForename(r.get(V_FUNCTIONRIGHT.USER_FORENAME));
				ualb.setSurname(r.get(V_FUNCTIONRIGHT.USER_SURNAME));
				ualb.setUsnr(r.get(V_FUNCTIONRIGHT.USER_USNR));
				ualb.getAcl().setAcl(r.get(V_FUNCTIONRIGHT.USER_ACCESS_LEVEL));
				list.add(ualb);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all function rights for all functions
	 * 
	 * @return the list of function rights
	 * 
	 *         if anything went wrong on database side
	 */
	public List<FunctionRightBean> getAllFunctionRights() {
		List<FunctionScale> scales = getAllFunctionscales();

		try (
				SelectJoinStep<Record14<Integer, Integer, String, Boolean, String, Integer, String, String, String, String, Integer, EnumAccesslevel, LocalDateTime, Boolean>> sql = getJooq()
				// @formatter:off
			.select(V_FUNCTIONRIGHT.PK,
							V_FUNCTIONRIGHT.FK_PRIVILEGE, 
							V_FUNCTIONRIGHT.NAME,
							V_FUNCTIONRIGHT.ACTIVATED,
							V_FUNCTIONRIGHT.SUMMARY,
							V_FUNCTIONRIGHT.CREATOR,
							V_FUNCTIONRIGHT.CREATOR_FORENAME,
							V_FUNCTIONRIGHT.CREATOR_SURNAME,
							V_FUNCTIONRIGHT.USER_FORENAME,
							V_FUNCTIONRIGHT.USER_SURNAME,
							V_FUNCTIONRIGHT.USER_USNR,
							V_FUNCTIONRIGHT.USER_ACCESS_LEVEL, 
							T_FUNCTION.CREATIONDATE,
							T_FUNCTION.USE_INTERVALS)
			.from(V_FUNCTIONRIGHT)
			.leftJoin(T_FUNCTION).on(T_FUNCTION.PK.eq(V_FUNCTIONRIGHT.PK));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Map<Integer, FunctionRightBean> tmp = new HashMap<>();
			for (Record r : sql.fetch()) {
				Integer functionId = r.get(V_FUNCTIONRIGHT.PK);
				FunctionRightBean bean = tmp.get(functionId);
				if (bean == null) {
					bean = new FunctionRightBean();
					LocalDateTime ldt = r.get(T_FUNCTION.CREATIONDATE);
	        Date creationDate = ldt == null ? null : Date.valueOf(ldt.atZone(ZoneId.systemDefault()).toLocalDate());
					FunctionBean function = new FunctionBean(r.get(V_FUNCTIONRIGHT.FK_PRIVILEGE), creationDate,
							r.get(T_FUNCTION.USE_INTERVALS), scales); // TODO: check if we need the checked property of this scales
																												// here
					function.setFunctionId(functionId);
					function.setName(r.get(V_FUNCTIONRIGHT.NAME));
					function.setActivated(r.get(V_FUNCTIONRIGHT.ACTIVATED));
					function.setSummary(r.get(V_FUNCTIONRIGHT.SUMMARY));
					function.setCreator(new ProfileBean());
					function.getCreator().setUsnr(r.get(V_FUNCTIONRIGHT.CREATOR));
					function.getCreator().setForename(r.get(V_FUNCTIONRIGHT.CREATOR_FORENAME));
					function.getCreator().setSurname(r.get(V_FUNCTIONRIGHT.CREATOR_SURNAME));
					bean.setFunction(function);
					tmp.put(functionId, bean);
				}
				UserAccessLevelBean ualb = new UserAccessLevelBean();
				ualb.setForename(r.get(V_FUNCTIONRIGHT.USER_FORENAME));
				ualb.setSurname(r.get(V_FUNCTIONRIGHT.USER_SURNAME));
				ualb.setUsnr(r.get(V_FUNCTIONRIGHT.USER_USNR));
				ualb.getAcl().setAcl(r.get(V_FUNCTIONRIGHT.USER_ACCESS_LEVEL));
				bean.getUsers().add(ualb);
			}
			return new ArrayList<>(tmp.values());
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update function referenced by functionId to be of user referenced by usnr
	 * from now on
	 * 
	 * @param functionId
	 *          the id of the function
	 * @param usnr
	 *          the id of the user
	 * @return number of affected database lines
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer switchCreator(Integer functionId, Integer usnr) {
		try (UpdateConditionStep<TFunctionRecord> sql = getJooq()
		// @formatter:off
			.update(T_FUNCTION)
			.set(T_FUNCTION.CREATOR, usnr)
			.where(T_FUNCTION.PK.equal(functionId));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get creator of function referenced by functionId
	 * 
	 * @param functionId
	 *          the id of the function
	 * @return the function bean
	 * 
	 *         if anything went wrong on database side
	 */
	public FunctionBean getFunctionCreator(Integer functionId) {
		List<FunctionScale> scales = getAllFunctionscales(functionId);
		try (SelectConditionStep<Record7<Integer, String, String, Integer, LocalDateTime, String, Boolean>> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTION.CREATOR, 
							T_PERSON.FORENAME,	
							T_PERSON.SURNAME,
							T_FUNCTION.FK_PRIVILEGE,
							T_FUNCTION.CREATIONDATE,
							T_FUNCTION.VIGNETTE,
							T_FUNCTION.USE_INTERVALS)
			.from(T_FUNCTION)
			.leftJoin(T_PERSON).on(T_PERSON.USNR.eq(T_FUNCTION.CREATOR))
			.where(T_FUNCTION.PK.eq(functionId));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			Record r = sql.fetchOne();
      if (r != null) {
        LocalDateTime ldt = r.get(T_FUNCTION.CREATIONDATE);
        Date creationDate = ldt == null ? null : Date.valueOf(ldt.atZone(ZoneId.systemDefault()).toLocalDate());
        FunctionBean bean = new FunctionBean(r.get(T_FUNCTION.FK_PRIVILEGE), creationDate,
            r.get(T_FUNCTION.USE_INTERVALS), scales);
        bean.setCreator(new ProfileBean());
        bean.setFunctionId(functionId);
        bean.getCreator().setForename(r.get(T_PERSON.FORENAME));
        bean.getCreator().setSurname(r.get(T_PERSON.SURNAME));
        bean.getCreator().setUsnr(r.get(T_FUNCTION.CREATOR));
        bean.setVignette(r.get(T_FUNCTION.VIGNETTE));
        return bean;
      }
			throw new DataAccessException(
					"no such function with id = ".concat(functionId == null ? "null" : functionId.toString()));
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all from T_FUNCTIONOUTPUTTYPE
	 * 
	 * @return the list of function output types
	 * 
	 *         if anything went wrong on database side
	 */
	public List<FunctionoutputtypeBean> getAllFunctionoutputtypes() {
		try (SelectSeekStep1<Record6<Integer, String, String, Long, String, String>, String> sql = getJooq()
		// @formatter:off
			.select(V_FUNCTIONOUTPUTTYPE.PK,
							V_FUNCTIONOUTPUTTYPE.NAME,
							V_FUNCTIONOUTPUTTYPE.DESCRIPTION,
							V_FUNCTIONOUTPUTTYPE.REFERENCE_AMOUNT, 
							T_FUNCTIONOUTPUTTYPE.LATEX_BEFORE,
							T_FUNCTIONOUTPUTTYPE.LATEX_AFTER)
			.from(V_FUNCTIONOUTPUTTYPE)
			.leftJoin(T_FUNCTIONOUTPUTTYPE).on(T_FUNCTIONOUTPUTTYPE.PK.eq(V_FUNCTIONOUTPUTTYPE.PK))
			.orderBy(V_FUNCTIONOUTPUTTYPE.NAME);
		// @formatter:on
		) {
			List<FunctionoutputtypeBean> list = new ArrayList<>();
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				FunctionoutputtypeBean bean = new FunctionoutputtypeBean(r.get(V_FUNCTIONOUTPUTTYPE.PK));
				bean.setName(r.get(V_FUNCTIONOUTPUTTYPE.NAME));
				bean.setDescription(r.get(V_FUNCTIONOUTPUTTYPE.DESCRIPTION));
				bean.setReferenceAmount(r.get(V_FUNCTIONOUTPUTTYPE.REFERENCE_AMOUNT));
				bean.setLatexBefore(r.get(T_FUNCTIONOUTPUTTYPE.LATEX_BEFORE));
				bean.setLatexAfter(r.get(T_FUNCTIONOUTPUTTYPE.LATEX_AFTER));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * add function output type; lang_key is defined by default
	 * 
	 * @param bean
	 *          the function output type bean
	 * @return the number of affected database lines
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer addFunctionoutputtype(FunctionoutputtypeBean bean) {
		try (InsertValuesStep4<TFunctionoutputtypeRecord, String, String, String, String> sql = getJooq()
		// @formatter:off
			.insertInto(T_FUNCTIONOUTPUTTYPE,
									T_FUNCTIONOUTPUTTYPE.NAME, 
									T_FUNCTIONOUTPUTTYPE.DESCRIPTION,
									T_FUNCTIONOUTPUTTYPE.LATEX_BEFORE,
									T_FUNCTIONOUTPUTTYPE.LATEX_AFTER)
			.values(bean.getName(), bean.getDescription(), bean.getLatexBefore(), bean.getLatexAfter());
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete function output type
	 * 
	 * @param pk
	 *          the id of the function output type
	 * @return the number of affected database lines
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer deleteFunctionoutputtype(Integer pk) {
		try (DeleteConditionStep<TFunctionoutputtypeRecord> sql = getJooq()
		// @formatter:off
			.deleteFrom(T_FUNCTIONOUTPUTTYPE)
			.where(T_FUNCTIONOUTPUTTYPE.PK.equal(pk));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all available input types from t_functioninputtype and its usages
	 * 
	 * @return the list of function input types
	 * 
	 *         if anything went wrong on database side
	 */
	public List<FunctioninputtypeBean> getAllInputtypes() {
		try (SelectHavingStep<Record4<Integer, String, String, Integer>> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTIONINPUTTYPE.PK,
							T_FUNCTIONINPUTTYPE.NAME,
							T_TRANSLATION.VALUE,
							DSL.field("count(t_functioninput.pk)", Integer.class).as("amount"))
			.from(T_FUNCTIONINPUTTYPE)
			.leftJoin(T_FUNCTIONINPUT).on(T_FUNCTIONINPUT.FK_TYPE.eq(T_FUNCTIONINPUTTYPE.PK))
			.leftJoin(T_TRANSLATION).on(T_TRANSLATION.FK_LANG.eq(T_FUNCTIONINPUTTYPE.FK_LANG)).and(T_TRANSLATION.ISOCODE.cast(String.class).eq(getFacesContext().getIsocode()))
			.groupBy(T_FUNCTIONINPUTTYPE.PK,
							 T_FUNCTIONINPUTTYPE.NAME,
							 T_TRANSLATION.VALUE);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<FunctioninputtypeBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				FunctioninputtypeBean bean = new FunctioninputtypeBean(r.get(T_FUNCTIONINPUTTYPE.PK),
						r.get("amount", Integer.class), r.get(T_TRANSLATION.VALUE));
				bean.setName(r.get(T_FUNCTIONINPUTTYPE.NAME));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update name of current bean due to pk
	 * 
	 * @param bean
	 *          the function input type bean
	 * @return the number of affected database lines
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer updateFunctioninputtype(FunctioninputtypeBean bean) {
		try (UpdateConditionStep<TFunctioninputtypeRecord> sql = getJooq()
		// @formatter:off
			.update(T_FUNCTIONINPUTTYPE)
			.set(T_FUNCTIONINPUTTYPE.NAME, bean.getName())
			.where(T_FUNCTIONINPUTTYPE.PK.eq(bean.getPk()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * load all function categories of function with functionId; if functionId is
	 * null, load the global categories
	 * 
	 * @param functionId
	 *          ID of the function
	 * @return categories of a function or the ones that are global
	 */
	public List<FunctioncategoryBean> getFunctioncategories(Integer functionId) {
		Condition condition = functionId == null ? T_FUNCTIONCATEGORY.FK_FUNCTION.isNull()
				: T_FUNCTIONCATEGORY.FK_FUNCTION.eq(functionId);
		try (SelectConditionStep<Record3<Integer, String, String>> sql = getJooq()
		// @formatter:off
			.select(T_FUNCTIONCATEGORY.PK,
					    T_FUNCTIONCATEGORY.NAME,
					    T_FUNCTIONCATEGORY.DESCRIPTION)
			.from(T_FUNCTIONCATEGORY)
			.where(condition);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			List<FunctioncategoryBean> list = new ArrayList<>();
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_FUNCTIONCATEGORY.PK);
				FunctioncategoryBean bean = new FunctioncategoryBean(pk);
				bean.setName(r.get(T_FUNCTIONCATEGORY.NAME));
				bean.setDescription(r.get(T_FUNCTIONCATEGORY.DESCRIPTION));
				list.add(bean);
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * delete function category
	 * 
	 * @param pk
	 *          the id
	 * @return number of affected database rows, should be 1
	 */
	public Integer deleteFunctioncategory(Integer pk) {
		try (DeleteConditionStep<TFunctioncategoryRecord> sql = getJooq()
		// @formatter:off
			.deleteFrom(T_FUNCTIONCATEGORY)
			.where(T_FUNCTIONCATEGORY.PK.eq(pk));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		} // TODO: think about and discuss, if other function categories with exactly same
			// name should be deleted also
	}

	/**
	 * upsert the function category
	 * 
	 * @param bean
	 *          the function category to be upserted
	 * @return number of affected database rows, should be 1
	 */
	public Integer upsertFunctioncategory(FunctioncategoryBean bean) {
		try (CloseableDSLContext jooq = getJooq()) {
			if (bean.getPk() == null) {
				InsertValuesStep2<TFunctioncategoryRecord, String, String> sql = jooq
			// @formatter:off
				.insertInto(T_FUNCTIONCATEGORY,
						        T_FUNCTIONCATEGORY.NAME,
						        T_FUNCTIONCATEGORY.DESCRIPTION)
				.values(bean.getName(), bean.getDescription());
			// @formatter:on
				LOGGER.debug("{}", sql.toString());
				return sql.execute();
			} else {
				UpdateConditionStep<TFunctioncategoryRecord> sql = jooq
				// @formatter:off
					.update(T_FUNCTIONCATEGORY)
					.set(T_FUNCTIONCATEGORY.NAME, bean.getName())
					.set(T_FUNCTIONCATEGORY.DESCRIPTION, bean.getDescription())
					.where(T_FUNCTIONCATEGORY.NAME.eq(bean.getOldName())); // !!! using name here to also update all existing categories in combination with functions
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				return sql.execute();
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * get all scales from db
	 * 
	 * @return list of scales
	 */
	public List<FunctionScale> getAllFunctionscales() {
		return getAllFunctionscales(null);
	}

	/**
	 * get all scales from db; always list all function scales and mark unused as
	 * unchecked
	 * 
	 * @param functionId
	 *          id of a function; if null, list all possible scales
	 * @return list of scales
	 */
	public List<FunctionScale> getAllFunctionscales(Integer functionId) {
		List<FunctionScale> list = new ArrayList<>();
		try (CloseableDSLContext jooq = getJooq()) {
			if (functionId == null) {
				SelectWhereStep<TScaleRecord> sql = jooq.selectFrom(T_SCALE);
				LOGGER.debug("{}", sql.toString());
				for (TScaleRecord r : sql.fetch()) {
					list.add(new FunctionScale(r.getPk(), r.getName()));
				}
			} else {
				SelectOnConditionStep<Record3<Integer, String, Integer>> sql = jooq
			// @formatter:off
				.select(T_SCALE.PK,
						    T_SCALE.NAME,
						    T_FUNCTIONSCALE.FK_SCALE)
				.from(T_SCALE)
				.leftJoin(T_FUNCTIONSCALE).on(T_FUNCTIONSCALE.FK_SCALE.eq(T_SCALE.PK)).and(T_FUNCTIONSCALE.FK_FUNCTION.eq(functionId));
			// @formatter:on
				LOGGER.debug("{}", sql.toString());
				for (Record r : sql.fetch()) {
					Integer pk = r.get(T_SCALE.PK);
					String name = r.get(T_SCALE.NAME);
					Boolean checked = r.get(T_FUNCTIONSCALE.FK_SCALE) != null;
					list.add(new FunctionScale(pk, name, checked));
				}
			}
			return list;
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
