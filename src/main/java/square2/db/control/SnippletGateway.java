package square2.db.control;

import static de.ship.dbppsquare.square.Tables.T_HTMLCOMPONENT;
import static de.ship.dbppsquare.square.Tables.T_SNIPPLET;
import static de.ship.dbppsquare.square.Tables.T_SNIPPLETTYPE;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.Record;
import org.jooq.Record5;
import org.jooq.SelectSeekStep1;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import de.ship.dbppsquare.square.tables.records.TSnippletRecord;
import de.ship.dbppsquare.square.tables.records.TSnipplettypeRecord;
import square2.db.control.model.LambdaResultWrapper;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.report.design.SnippletBean;
import square2.modules.model.report.design.SnipplettypeBean;

/**
 * 
 * @author henkej
 *
 */
public class SnippletGateway extends JooqGateway {
	private static final Logger LOGGER = LogManager.getLogger(SnippletGateway.class);

	/**
	 * create new sipplet gateway
	 * 
	 * @param facesContext
	 *          the context of this gateway
	 */
	public SnippletGateway(SquareFacesContext facesContext) {
		super(facesContext);
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> getRightMap() throws Exception {
		return super.getPrivilegeMap(getJooq());
	}

	/**
	 * get all snipplet types
	 * 
	 * @return a list of snipplet type beans
	 * 
	 *         if anything went wrong on database side
	 */
	public List<SnipplettypeBean> getAllTypes() {
		List<SnipplettypeBean> list = new ArrayList<>();
		try (SelectSeekStep1<Record5<Integer, Integer, String, String, String>, Integer> sql = getJooq()
		// @formatter:off
			.select(T_SNIPPLETTYPE.PK, 
			        T_SNIPPLETTYPE.ORDER_NR,
							T_SNIPPLETTYPE.TYPE, 
							T_SNIPPLETTYPE.TEMPLATE,
							T_HTMLCOMPONENT.NAME)
			.from(T_SNIPPLETTYPE)
			.leftJoin(T_HTMLCOMPONENT).on(T_HTMLCOMPONENT.PK.eq(T_SNIPPLETTYPE.FK_HTMLCOMPONENT))
			.orderBy(T_SNIPPLETTYPE.ORDER_NR);
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			for (Record r : sql.fetch()) {
				Integer pk = r.get(T_SNIPPLETTYPE.PK);
				Integer orderNr = r.get(T_SNIPPLETTYPE.ORDER_NR);
				String type = r.get(T_SNIPPLETTYPE.TYPE);
				String template = r.get(T_SNIPPLETTYPE.TEMPLATE);
				String htmlComponent = r.get(T_HTMLCOMPONENT.NAME);
				list.add(new SnipplettypeBean(pk, orderNr, type, template, htmlComponent));
			}
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return list;
	}

	/**
	 * update bean referenced by snipplet id
	 * 
	 * @param bean
	 *          the snipplet bean
	 * @return the number of database lines
	 */
	public Integer update(SnippletBean bean) {
		try (UpdateConditionStep<TSnippletRecord> sql = getJooq()
		// @formatter:off
			.update(T_SNIPPLET)
			.set(T_SNIPPLET.MARKDOWNTEXT, bean.getMarkdowntext())
			.set(T_SNIPPLET.FK_FUNCTIONOUTPUT, bean.getFkFunctionoutput())
			.set(T_SNIPPLET.FK_MATRIXCOLUMN, bean.getMatrixcolumnBean().getFkMatrixcolumn())
			.set(T_SNIPPLET.FK_SNIPPLETTYPE, bean.getType().getId())
			.where(T_SNIPPLET.PK.eq(bean.getId()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * switch orderNr of both snipplets by setting to values
	 * 
	 * @param id1
	 *          the id of the first element
	 * @param order1NrNew
	 *          the new order number of the first element
	 * @param id2
	 *          the id of the second element
	 * @param order2NrNew
	 *          the new order number of the second element
	 * @return the number of affected database lines
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer switchOrderNr(Integer id1, Integer order1NrNew, Integer id2, Integer order2NrNew) {
		LambdaResultWrapper lrw = new LambdaResultWrapper();
		try (CloseableDSLContext jooq = getJooq()) {
			jooq.transaction(c -> {
				UpdateConditionStep<TSnippletRecord> sql = DSL.using(c)
				// @formatter:off
					.update(T_SNIPPLET)
					.set(T_SNIPPLET.ORDER_NR, order1NrNew)
					.where(T_SNIPPLET.PK.eq(id1));
				// @formatter:on
				LOGGER.debug("{}", sql.toString());
				lrw.addToInteger(sql.execute());

				UpdateConditionStep<TSnippletRecord> sql2 = DSL.using(c)
				// @formatter:off
					.update(T_SNIPPLET)
					.set(T_SNIPPLET.ORDER_NR, order2NrNew)
					.where(T_SNIPPLET.PK.eq(id2));
				// @formatter:on
				LOGGER.debug("{}", sql2.toString());
				lrw.addToInteger(sql2.execute());
			});
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
		return lrw.getInteger();
	}

	/**
	 * update snipplet type
	 * 
	 * @param bean
	 *          the snipplet bean
	 * @return number of affected rows
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer updateSnippletType(SnippletBean bean) {
		try (UpdateConditionStep<TSnippletRecord> sql = getJooq()
		// @formatter:off
			.update(T_SNIPPLET)
			.set(T_SNIPPLET.FK_SNIPPLETTYPE, bean.getType().getId())
			.where(T_SNIPPLET.PK.eq(bean.getId()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}

	/**
	 * update snipplet type bean
	 * 
	 * @param bean
	 *          contains information about the snipplet type change; references the
	 *          database entry with the primary key of id from the bean
	 * @return number of affected rows, should be 1
	 * 
	 *         if anything went wrong on database side
	 */
	public Integer updateSnippletTypeBean(SnipplettypeBean bean) {
		try (UpdateConditionStep<TSnipplettypeRecord> sql = getJooq()
		// @formatter:off
			.update(T_SNIPPLETTYPE)
			.set(T_SNIPPLETTYPE.TEMPLATE, bean.getSnipplettemplate())
			.set(T_SNIPPLETTYPE.ORDER_NR, bean.getOrderNr())
			.where(T_SNIPPLETTYPE.PK.eq(bean.getId()));
		// @formatter:on
		) {
			LOGGER.debug("{}", sql.toString());
			return sql.execute();
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			throw new DataAccessException(e.getMessage(), e);
		}
	}
}
