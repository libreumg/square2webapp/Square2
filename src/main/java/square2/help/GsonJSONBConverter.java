package square2.help;

import org.jooq.JSONB;

import com.google.gson.Gson;
import com.google.gson.JsonObject;

/**
 * 
 * @author henkej
 *
 */
public class GsonJSONBConverter {

	private JsonObject json;

	/**
	 * @param jsonb the json representation
	 */
	public GsonJSONBConverter(JSONB jsonb) {
		this.json = jsonb == null ? null : new Gson().fromJson(jsonb.data(), JsonObject.class);
	}

	/**
	 * get the element from json referenced by key
	 * 
	 * @param key the key
	 * @return the element
	 */
	public String get(String key) {
		return json == null ? null : (json.get(key) == null ? null : json.get(key).getAsString());
	}
}
