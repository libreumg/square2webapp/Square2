package square2.help;

import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author henkej
 *
 */
public class JsfPhaseListener implements PhaseListener {

	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(JsfPhaseListener.class);

	@Override
	public PhaseId getPhaseId() {
		return PhaseId.ANY_PHASE;
	}

	@Override
	public void beforePhase(final PhaseEvent event) {
		LOGGER.trace("enter " + event.getPhaseId());
	}

	@Override
	public void afterPhase(final PhaseEvent event) {
		LOGGER.trace("leave " + event.getPhaseId());
	}
}
