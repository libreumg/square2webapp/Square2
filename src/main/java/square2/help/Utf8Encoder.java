package square2.help;

import java.io.UnsupportedEncodingException;

/**
 * 
 * @author henkej
 *
 */
public class Utf8Encoder {
	/**
	 * @param buf
	 *          the string builder
	 * @return the string as utf-8 encoded one
	 * @throws UnsupportedEncodingException
	 *           if encoding is not supported
	 */
	public String encode(StringBuilder buf) throws UnsupportedEncodingException {
		return new String(buf.toString().getBytes("UTF-8"));
	}
}
