package square2.help;

/**
 * 
 * @author henkej
 *
 */
public class PropertyFileDefinition {
	/**
	 * the location of the Square² database properties (and more)
	 */
	public static final String SQUARE2_CONFIGFILE = "/etc/tomcatsquare2/square2.properties";
}
