package square2.help;

import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/**
 * 
 * @author henkej
 *
 */
@FacesValidator("square2.help.LimitValidator")
public class LimitValidator implements Validator {
	/**
	 * the pattern for the limit validation
	 */
	public static final String PATTERN = "^[<>]{1}[=]?[-]?[0-9]+(.[0-9]+)?$";

	@Override
	public void validate(FacesContext context, UIComponent component, Object value) {
		if (value != null) {
			String v = value.toString();
			if (!v.equals("")) {
				Pattern pattern = Pattern.compile(PATTERN);
				if (!pattern.matcher(v).matches()) {
					StringBuilder buf = new StringBuilder("The input of ");
					buf.append(value);
					buf.append(" is not allowed.");
					FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_ERROR, "wrong format", buf.toString());
					throw new ValidatorException(msg);
				}
			}
		}
	}
}
