package square2.help;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * 
 * @author henkej
 *
 */
public class HandsetRuleHelper {

	/**
	 * generate a result from the rules of
	 * https://gitlab.com/umg_hgw/sq2/square2/-/issues/910
	 * 
	 * @param functionName the name of the function
	 * @return the acronmy for such a function
	 */
	public static final String generateMatrixColumnAcronym(String functionName) {
		if (functionName == null || functionName.isBlank()) {
			return functionName;
		} else {
			return replaceAsDefined(functionName);
		}
	}

	/**
	 * replace s as defined in the roles of
	 * https://gitlab.com/umg_hgw/sq2/square2/-/issues/910
	 * 
	 * @param s the candidate
	 * @return the replacement
	 */
	private static final String replaceAsDefined(String s) {
		Map<String, String> translationMap = new LinkedHashMap<>();
		translationMap.put("acc_", "A");
		translationMap.put("int_", "I");
		translationMap.put("com_", "M");
		translationMap.put("con_", "C");
		translationMap.put("categorical", "Cat");
		translationMap.put("contradictions", "Contdct");
		translationMap.put("datatype", "Dt");
		translationMap.put("detection", "Det");
		translationMap.put("deviations", "Dev");
		translationMap.put("digits", "Dig");
		translationMap.put("distributions", "Dist");
		translationMap.put("end", "End");
		translationMap.put("inadmissible", "Inadm");
		translationMap.put("item", "It");
		translationMap.put("limit", "Lim");
		translationMap.put("limits", "Lim");
		translationMap.put("loess", "Lss");
		translationMap.put("margins", "Mrg");
		translationMap.put("matrix", "Mat");
		translationMap.put("missing", "Mis");
		translationMap.put("missingness", "Mis");
		translationMap.put("multivariate", "Mv");
		translationMap.put("or", "");
		translationMap.put("outlier", "Ol");
		translationMap.put("robust", "");
		translationMap.put("scale", "Sc");
		translationMap.put("segment", "Seg");
		translationMap.put("shape", "Sh");
		translationMap.put("unit", "Un");
		translationMap.put("univariate", "Uv");
		translationMap.put("varcomp", "Varcomp");
		String[] subwords = s.split("\\_");
		for (String subword : subwords) {
			if (subword != null && !subword.isBlank()) {
				if (!translationMap.keySet().contains(subword)) {
					translationMap.put(subword, subword.substring(0, 1).toUpperCase());
				}
			}
		}
		for (String key : translationMap.keySet()) {
			s = s.replace(key, translationMap.get(key));
		}
		s = s.replace("_", "");
		return s;
	}
}
