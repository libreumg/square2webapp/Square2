package square2.help;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

import square2.modules.model.ProfileBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class ThemeBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String themeName;

	private List<String> getKnownThemes() {
		List<String> list = new ArrayList<>();
		list.add("cerulean");
		list.add("cosmo");
		list.add("cyborg");
		list.add("darkly");
		list.add("default");
		list.add("flatly");
		list.add("journal");
		list.add("lumen");
		list.add("other");
		list.add("paper");
		list.add("patternfly");
		list.add("readable");
		list.add("sandstone");
		list.add("simplex");
		list.add("slate");
		list.add("spacelab");
		list.add("superhero");
		list.add("united");
		list.add("yeti");
		return list;
	}

	private String validThemeName(SquareFacesContext facesContext, String themeName, boolean showMessage) {
		if (getKnownThemes().contains(themeName)) {
			return themeName;
		} else {
			if (showMessage && themeName != null && !themeName.trim().equals("")) {
				StringBuilder buf = new StringBuilder("theme \"");
				buf.append(themeName);
				buf.append("\" not found in list of valid ones wich are: ");
				boolean isFirst = true;
				for (String knownTheme : getKnownThemes()) {
					buf.append(isFirst ? "" : ",");
					buf.append(knownTheme);
					isFirst = false;
				}
				facesContext.notifyWarning("theme error: {0}", buf.toString());
			}
			return "default";
		}
	}

	/**
	 * reset theme
	 */
	public void resetTheme() {
		themeName = null;
	}

	/**
	 * contains current theme for web.xml's theme chooser
	 * 
	 * @return current theme
	 */
	public String getCurrentTheme() {
		SquareFacesContext facesContext = (SquareFacesContext) FacesContext.getCurrentInstance();
		if (themeName != null) {
			return validThemeName(facesContext, themeName, true);
		} else {
			ProfileBean profileBean = facesContext.getProfile();
			if (profileBean == null) {
				return validThemeName(facesContext, null, false);
			} else {
				themeName = (String) profileBean.getConfig("config.common.theme");
				return validThemeName(facesContext, themeName, true);
			}
		}
	}
}
