package square2.help;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.JSONB;
import org.jooq.exception.DataAccessException;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;

import de.jooqFaces.javax.JavaxJooqFacesContext;
import de.ship.dbppsquare.square.enums.EnumIsocode;
import square2.db.control.ParametrizedDataAccessException;
import square2.db.control.TranslationGateway;
import square2.db.control.converter.EnumConverter;
import square2.modules.control.help.Session;
import square2.modules.model.LangBean;
import square2.modules.model.LangKey;
import square2.modules.model.LocaleBean;
import square2.modules.model.ProfileBean;
import square2.modules.model.SquareLocale;
import square2.modules.model.admin.TranslationBean;
import square2.modules.model.study.ImportLanguageBean;

/**
 * 
 * @author henkej
 *
 */
public class SquareFacesContext extends JavaxJooqFacesContext {
	private static final Logger LOGGER = LogManager.getLogger(SquareFacesContext.class);

	/**
	 * @param facesContext the context to be wrapped
	 * @param localeBean the locale bean to be initialized
	 */
	public SquareFacesContext(FacesContext facesContext, LocaleBean localeBean) {
		super(facesContext);
		getELContext().putContext(FacesContext.class, this);
		facesContext.getExternalContext().getSessionMap().put("lang", localeBean);
	}

	/**
	 * show the name of the language defined by langcode in the current locale
	 * 
	 * @param langcode the language code
	 * @param locale the current locale
	 * @return a language name
	 */
	public String displayLanguage(String langcode, Locale locale) {
		String lc = langcode == null ? null : langcode.split("_")[0];
		Locale l = Locale.forLanguageTag(lc);
		if (l == null) {
			LOGGER.error("cannot find locale for langcode {}", langcode);
			return "locale not found: " + (langcode == null ? "null" : langcode);
		} else {
			return locale == null ? l.getDisplayLanguage() : l.getDisplayLanguage(locale);
		}
	}

	/**
	 * translate if condition is true
	 * 
	 * @param condition the condition
	 * @param key the key
	 * @return the result as specified
	 */
	public String translateif(boolean condition, String key) {
		return condition ? translate(key) : "";
	}
	
	/**
	 * notify a facesMessage
	 * 
	 * @param severity of the message
	 * @param summary to be displayed
	 * @param message to be displayed
	 * @param params to be replaced for placeholders
	 */
	private void notifyFacesMessage(Severity severity, String summary, String message, Object... params) {
		addMessage(translate(null, message, params),
				new FacesMessage(severity, translate(summary), translate(null, message, params)));
	}

	/**
	 * generate a notification
	 * 
	 * @param loglevel the loglevel, one of info, warn or error
	 * @param message the message
	 */
	public void notifyMessage(String loglevel, String message) {
		if ("info".equalsIgnoreCase(loglevel)) {
			notifyInformation(message);
		} else if ("warn".equalsIgnoreCase(loglevel)) {
			notifyWarning(message);
		} else if ("error".equalsIgnoreCase(loglevel)) {
			notifyError(message);
		} else {
			notifyException(new Exception(message + ", did not understand loglevel " + loglevel));
		}
	}

	/**
	 * notify information
	 * 
	 * @param message to be used
	 * @param params to be replaced for each {?} where ? is a number
	 */
	public void notifyInformation(String message, Object... params) {
		notifyFacesMessage(FacesMessage.SEVERITY_INFO, "info", message, params);
	}

	/**
	 * notify warning
	 * 
	 * @param message to be used
	 * @param params to be replaced for each {?} where ? is a number
	 */
	public void notifyWarning(String message, Object... params) {
		notifyFacesMessage(FacesMessage.SEVERITY_WARN, "warn", message, params);
	}

	/**
	 * notify the user with an information for found == expected or a warning for
	 * found != expected about database changes
	 * 
	 * @param found number of database changes
	 * @param expected number of expected database changes
	 * @return true for found == number, false otherwise
	 */
	public boolean notifyAffected(Integer found, Integer expected) {
		if (found.equals(expected)) {
			notifyInformation("info.number.of.affected.rows", found);
			return true;
		} else {
			notifyWarning("warn.number.of.affected.rows", found, expected);
			return false;
		}
	}

	/**
	 * notify the user with an information for found about database changes
	 * 
	 * @param found number of database changes
	 */
	public void notifyAffected(Integer found) {
		notifyInformation("info.number.of.affected.rows", found);
	}

	/**
	 * notify error
	 * 
	 * @param message to be used
	 * @param params to be replaced for each {?} where ? is a number
	 */
	public void notifyError(String message, Object... params) {
		notifyFacesMessage(FacesMessage.SEVERITY_ERROR, "error", message, params);
	}

	/**
	 * notify exception
	 * 
	 * @param e exception to be used
	 */
	public void notifyException(Exception e) {
		notifyFacesMessage(FacesMessage.SEVERITY_ERROR, translate("exception"), e.getMessage());
	}

	/**
	 * notify exception
	 * 
	 * @param e exception to be used
	 */
	public void notifyException(DataAccessException e) {
		notifyException(new ParametrizedDataAccessException(e));
	}

	/**
	 * notify exception
	 * 
	 * @param e exception to be used
	 */
	public void notifyException(ParametrizedDataAccessException e) {
		notifyFacesMessage(FacesMessage.SEVERITY_ERROR, translate("exception"), e.getMessage(), e.getParams());
	}

	/**
	 * notify missing value
	 * 
	 * @param valueName to be notified
	 */
	public void notifyMissingValue(String valueName) {
		notifyError(translate("error.missing.input", valueName));
	}

	/**
	 * check if current active panel is NOT name
	 * 
	 * @param name the name
	 * @return true if name is not equal to active panel, false otherwise
	 */
	public boolean hidePanel(String name) {
		if (name == null) {
			return false;
		}
		return !name.equals(getExternalContext().getSessionMap().get(Session.ACTIVE_PANEL.get()));
	}

	/**
	 * translate String to current locale
	 * 
	 * @param key to be translated, must be string key (see T_CORE_LANGUAGEKEY.name)
	 * @return translation or key
	 */
	public String translate(String key) {
		return translate(key, new Object[] {});
	}

	/**
	 * translate String to current locale
	 * 
	 * @param key to be translated, must be string key (see T_CORE_LANGUAGEKEY.name)
	 * @param params to be used for translation
	 * @return translation or key
	 */
	public String translate(String key, Object... params) {
		return translate(null, key, params);
	}

	/**
	 * translate by named key
	 * 
	 * @param key the key
	 * @param params the params
	 * @return the translation or the key
	 */
	public String translateByNamedKey(String key, Object... params) {
		LocaleBean localeBean = getLocaleBean();
		if (localeBean != null) {
			return localeBean.translate(this, null, key, params);
		} else {
			LOGGER.warn("localeBean not found in facesContext's sessionMap for translate('{}', ...)", key);
		}
		return key.toString();
	}

	/**
	 * translate a language key
	 * 
	 * !!! works only if t_languagekey.name is null !!!
	 * 
	 * @param key the key
	 * @param params the params
	 * @return the translation or the key itself
	 */
	public String translateKey(Integer key, Object... params) {
		LocaleBean localeBean = getLocaleBean();
		if (localeBean != null) {
			return localeBean.translateByKey(this, key, params);
		} else {
			LOGGER.warn("localeBean not found in facesContext's sessionMap for translateKey('{}', ...)", key);
		}
		return key == null ? null : key.toString();
	}

	/**
	 * translate String to current locale
	 *
	 * @param key to be translated, must be string key (see T_CORE_LANGUAGEKEY.name)
	 * @param params to be translated each before integrated into key
	 * @return translation or key
	 */
	private String translate(Integer key, String name, Object... params) {
		LocaleBean localeBean = getLocaleBean();
		if (localeBean != null) {
			return localeBean.translate(this, key, name, params);
		} else {
			LOGGER.warn("localeBean not found in facesContext's sessionMap for translate('{}', ...)", key);
		}
		return key == null ? null : key.toString();
	}

	/**
	 * return a list of translation beans
	 * 
	 * @param json the translation object
	 * @return the list
	 */
	@SuppressWarnings("unchecked")
	public List<TranslationBean> listTranslations(JSONB json) {
		if (json == null) {
			return null;
		}
		LinkedTreeMap<String, String> ltr = new Gson().fromJson(json.data(), LinkedTreeMap.class);
		List<TranslationBean> list = new ArrayList<>();
		for (String isocode : ltr.keySet()) {
			String key = "obsolete";
			String value = ltr.get(isocode);
			TranslationBean bean = new TranslationBean(isocode, key, value);
			list.add(bean);
		}
		return list;
	}

	/**
	 * choose the translation of the current locale of the json string in jsonb
	 * 
	 * @param jsonb the jsonb object
	 * @return the translation, null, an error or the json if no locale is set
	 */
	public String selectTranslation(JSONB jsonb) {
		return jsonb == null ? null : selectTranslation(jsonb.data());
	}

	/**
	 * choose the translation of the current locale of the json string in json
	 * 
	 * @param json the json string; typically sth. like "{\"de_DE\": \"Ja\", \"en\":
	 * \"Yes\"}"
	 * @return the translation, null, an error or the json if no locale is set
	 */
	@SuppressWarnings("unchecked")
	public String selectTranslation(String json) {
		LocaleBean localeBean = getLocaleBean();
		if (localeBean != null) {
			String key = localeBean.getLocaleCode();
			try {
				LinkedTreeMap<String, String> ltr = new Gson().fromJson(json, LinkedTreeMap.class);
				return ltr.get(key);
			} catch (Exception e) {
				LOGGER.error(e.getMessage(), e);
				return e.getMessage();
			}
		} else {
			LOGGER.warn("localeBean not found in facesContext's sessionMap for selectTranslation('{}')", json);
			return json;
		}
	}

	/**
	 * get locale bean from context
	 * 
	 * @return localeBean or null if not found
	 */
	public LocaleBean getLocaleBean() {
		ExternalContext externalContext = super.getExternalContext();
		if (externalContext != null) {
			Map<String, Object> sessionMap = externalContext.getSessionMap();
			if (sessionMap != null) {
				return (LocaleBean) sessionMap.get("lang");
			} else {
				LOGGER.warn("sessionMap not found in facesContext");
			}
		} else {
			LOGGER.warn("externalContext not found in facesContext");
		}
		return null;
	}

	/**
	 * reset the translations map for isocode
	 * 
	 * @param isocode the isocode
	 * @param map the translations map
	 */
	public void setTranslations(String isocode, Map<String, String> map) {
		ExternalContext externalContext = getExternalContext();
		if (externalContext == null) {
			LOGGER.warn("externalContext is null");
		} else {
			ServletContext servletContext = (ServletContext) externalContext.getContext();
			if (servletContext == null) {
				LOGGER.warn("servletContext is null");
			} else {
				LangBean bean = (LangBean) servletContext.getAttribute(ContextKey.TRANSLATIONS.get());
				Map<String, Map<String, String>> translationsMap = bean.getMap();
				translationsMap.put(isocode, map);
			}
		}
	}

	/**
	 * get translations map from context
	 * 
	 * @return translations map
	 */
	public Map<String, Map<String, String>> getTranslations() {
		ExternalContext externalContext = getExternalContext();
		if (externalContext == null) {
			LOGGER.warn("externalContext is null");
		} else {
			ServletContext servletContext = (ServletContext) externalContext.getContext();
			if (servletContext == null) {
				LOGGER.warn("servletContext is null");
			} else {
				LangBean bean = (LangBean) servletContext.getAttribute(ContextKey.TRANSLATIONS.get());
				Map<String, Map<String, String>> map = bean.getMap();
				if (map == null) {
					LOGGER.warn("translations map is null");
				} else {
					return map;
				}
			}
		}
		return new HashMap<>();
	}

	/**
	 * reset translations
	 * 
	 * @param newTranslations
	 */
	private void setAllTranslations(Map<String, Map<String, String>> map) {
		ExternalContext externalContext = getExternalContext();
		if (externalContext == null) {
			LOGGER.warn("externalContext is null");
		} else {
			ServletContext servletContext = (ServletContext) externalContext.getContext();
			if (servletContext == null) {
				LOGGER.warn("servletContext is null");
			} else {
				LangBean bean = (LangBean) servletContext.getAttribute(ContextKey.TRANSLATIONS.get());
				bean.setMap(map);
			}
		}
	}

	/**
	 * get isocode of current selected locale
	 * 
	 * @return isocode of current selected locale or null if not found
	 */
	public String getIsocode() {
		LocaleBean localeBean = getLocaleBean();
		if (localeBean != null) {
			return localeBean.getLocaleCode();
		} else {
			LOGGER.warn("localeBean not found in facesContext's sessionMap");
		}
		return null;
	}

	/**
	 * get isocode of current selected locale
	 * 
	 * @return found isocode, or null, if there is no such isocode in enum
	 */
	public EnumIsocode getIsocodeEnum() {
		return new EnumConverter().getEnumIsocode(getIsocode());
	}

	/**
	 * reload translations from db
	 */
	public void reloadTranslations() {
		// TODO: check if this method is too slow
		Map<String, Map<String, String>> translations = getTranslations();
		LangBean bean = new TranslationGateway(this).getAllTranslations(SquareLocale.values());
		Map<String, Map<String, String>> newTranslations = new HashMap<>();
		for (Map.Entry<String, Map<String, String>> entry : translations.entrySet()) {
			String lang = entry.getKey();
			Map<String, String> props = entry.getValue();
			props.putAll(bean.get(lang));
			newTranslations.put(lang, props);
		}
		setAllTranslations(newTranslations);
	}

	/**
	 * reset translation in cache for specific case
	 * 
	 * @param isocode to be used
	 * @param key to be used
	 * @param value new value of translation
	 */
	public void resetTranslation(String isocode, String key, String value) {
		Map<String, String> localeMap = getTranslations().get(isocode);
		if (localeMap == null) {
			LOGGER.error("could not load locale {} from session", isocode);
		} else {
			localeMap.put(LangKey.generateKey(key), value);
			setTranslations(isocode, localeMap);
		}
	}

	/**
	 * reset translation in cache for specific case
	 * 
	 * @param isocode to be used
	 * @param key to be used
	 * @param value new value of translation
	 */
	public void resetTranslation(String isocode, Integer key, String value) {
		Map<String, String> localeMap = getTranslations().get(isocode);
		if (localeMap == null) {
			LOGGER.error("could not load locale {} from session", isocode);
		} else {
			localeMap.put(LangKey.generateKey(key), value);
			setTranslations(isocode, localeMap);
		}
	}

	/**
	 * add translations to map
	 * 
	 * @param languages the languages
	 */
	public void addTranslations(List<ImportLanguageBean> languages) {
		Map<String, Map<String, String>> localeMap = getTranslations();
		Set<String> legalIsocodes = localeMap.keySet();
		for (ImportLanguageBean bean : languages) {
			Map<String, String> map = bean.getMap();
			if (map != null) {
				for (Entry<String, String> entry : map.entrySet()) {
					String isocode = entry.getKey();
					String value = entry.getValue();
					String key = LangKey.generateKey(bean.getFkLang(), bean.getVariable());
					if (legalIsocodes.contains(isocode)) {
						localeMap.get(isocode).put(key, value);
					}
				}
			}
		}
	}

	/**
	 * get current Profile from session map
	 * 
	 * @return profile if found
	 */
	public ProfileBean getProfile() {
		return (ProfileBean) getExternalContext().getSessionMap().get(Session.PROFILE.get());
	}

	/**
	 * get usnr of current profile
	 * 
	 * @return usnr of current profile; null if no profile was found
	 */
	public Integer getUsnr() {
		ProfileBean bean = getProfile();
		if (bean == null) {
			notifyWarning("profile is null");
			return null;
		} else {
			return bean.getUsnr();
		}
	}

	/**
	 * get the username of the current profile
	 * 
	 * @return the username of the current profile; null if no profile was found
	 */
	public String getUsername() {
		ProfileBean bean = getProfile();
		if (bean == null) {
			notifyWarning("profile is null");
			return null;
		} else {
			return bean.getUsername();
		}
	}
	
	/**
	 * remove all messages from the faces context
	 */
	public void clearMessages() {
		Iterator<FacesMessage> i = getMessages();
		while (i.hasNext()) {
			i.next();
			i.remove();
		}
	}

	/**
	 * get the value of the session map or null
	 * 
	 * @param key the key
	 * @return the value or null
	 */
	public Object getSessionMapValue(String key) {
		ExternalContext ec = getExternalContext();
		if (ec != null) {
			Map<String, Object> map = ec.getSessionMap();
			if (map != null) {
				return map.get(key);
			} else {
				notifyError("session map is null on looking up for {0} in session map", key);
			}
		} else {
			notifyError("external context is null on looking up for {0} in session map", key);
		}
		return null;
	}

	/**
	 * @param key the context key
	 * @return the found value or null
	 */
	public Object getApplicationMapValue(ContextKey key) {
		if (key != null) {
			ExternalContext ec = getExternalContext();
			if (ec != null) {
				Map<String, Object> map = ec.getApplicationMap();
				if (map != null) {
					return map.get(key.get());
				}
				notifyError("application map is null on looking up for {0} in session map", key);
			} else {
				notifyError("external context is null on looking up for {0} in session map", key);
			}
		} else {
			notifyError("application map cannot be looked up for key null");
		}
		return null;
	}

	/**
	 * reset the locale bean
	 * 
	 * @param bean the locale bean
	 */
	public void setLocaleBean(LocaleBean bean) {
		ExternalContext ec = getExternalContext();
		if (ec != null) {
			Map<String, Object> map = ec.getSessionMap();
			if (map != null) {
				map.put("lang", bean);
			} else {
				notifyError("session map is null on looking up for lang in session map");
			}
		} else {
			notifyError("external context is null on looking up for lang in session map");
		}
	}

	/**
	 * get the param from the configuration file
	 * 
	 * @param key the key
	 * @param defaultValue the value, if key is not found
	 * @return the value
	 */
	public String getParam(String key, String defaultValue) {
		ExternalContext ec = super.getExternalContext();
		ServletContext servletContext = (ServletContext) ec.getContext();
		if (key == null) {
			return defaultValue;
		}
		String value = servletContext.getInitParameter(key);
		return value == null ? defaultValue : value;
	}
}
