package square2.help;

import java.util.List;
import java.util.Map;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

/**
 * 
 * @author henkej
 *
 */
public abstract class JsDataTable {

	private final String locale;
	private final List<Map<String, Object>> listMap;

	/**
	 * @param listMap the table content
	 * @param locale the locale, may be de_DE or en or whatever
	 */
	public JsDataTable(List<Map<String, Object>> listMap, String locale) {
		this.listMap = listMap;
		this.locale = locale;
	}

	@Override
	public String toString() {
		JsonObject localeDefs = new JsonObject();
		if ("de_DE".equals(locale)) {
			localeDefs.addProperty("search", "suchen");
			localeDefs.addProperty("processing", "Daten werden geladen...");
			localeDefs.addProperty("lengthMenu", "zeige _MENU_ Zeilen");
			localeDefs.addProperty("emptyTable", "es gibt keine Einträge");
			localeDefs.addProperty("info", "Zeige _START_ bis _END_ von _TOTAL_ Einträgen");
			JsonObject paginate = new JsonObject();
			paginate.addProperty("first", "Anfang");
			paginate.addProperty("previous", "zurück");
			paginate.addProperty("next", "weiter");
			paginate.addProperty("last", "Ende");
			localeDefs.add("paginate", paginate);
		}

		JsonArray data = new JsonArray();
		JsonArray columns = new JsonArray();

		if (listMap.size() > 0) {
			for (Map<String, Object> entries : listMap) {
				JsonArray line = new JsonArray();
				for (Object entry : entries.values()) {
					line.add(entry.toString());
				}
				data.add(line);
			}
			for (String title : listMap.get(0).keySet()) {
				JsonObject col = new JsonObject();
				col.addProperty("title", title);
				columns.add(col);
			}
		}

		JsonObject root = new JsonObject();
		root.add("data", data);
		root.add("columns", columns);
		root.add("language", localeDefs);
		return root.toString();
	}
}
