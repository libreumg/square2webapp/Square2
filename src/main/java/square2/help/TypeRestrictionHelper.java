package square2.help;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

/**
 * 
 * @author henkej
 *
 */
@Named(value = "TypeRestrictionHelper")
@RequestScoped
public class TypeRestrictionHelper {

	/**
	 * convert json to a json object; if it is not a json object, return null
	 * 
	 * @param json the json
	 * @return the json object
	 */
	private static final JsonObject toJsonObject(String json) {
		JsonObject result = null;
		if (json != null && !json.isBlank()) {
			try {
				result = new Gson().fromJson(json, JsonObject.class);
			} catch (JsonSyntaxException e) {
			}
		}
		return result;
	}

	/**
	 * find the pair key: value in json and return the value as a boolean; if not
	 * found, return false
	 * 
	 * @param json the json
	 * @param key the key
	 * @return true or false
	 */
	public static final Boolean findBoolean(String json, String key) {
		Boolean result = false;
		JsonObject o = toJsonObject(json);
		if (o != null && o.has(key)) {
			JsonElement valueJson = o.get(key);
			if (valueJson != null) {
				if (valueJson.isJsonArray()) {
					JsonArray array = valueJson.getAsJsonArray();
					if (array.size() == 1) {
						result = array.getAsBoolean();
					}
				} else if (valueJson.isJsonPrimitive() && !valueJson.isJsonNull()) {
					result = valueJson.getAsBoolean();
				}
			}
		}
		return result;
	}

	/**
	 * find the pair key: value in json and return the value as an integer; if not
	 * found, return null
	 * 
	 * @param json the json
	 * @param key the key
	 * @return the found integer or null
	 */
	public static final Integer findInteger(String json, String key) {
		Integer result = null;
		JsonObject o = toJsonObject(json);
		if (o != null && o.has(key)) {
			JsonElement valueJson = o.get(key);
			if (valueJson != null) {
				if (valueJson.isJsonArray()) {
					JsonArray array = valueJson.getAsJsonArray();
					if (array.size() == 1) {
						result = array.getAsInt();
					}
				} else if (valueJson.isJsonPrimitive() && !valueJson.isJsonNull()) {
					result = valueJson.getAsInt();
				}
			}
		}
		return result;
	}

	/**
	 * find the range in json
	 * 
	 * @param json th json
	 * @param fromKey the from
	 * @param toKey the to
	 * @return the range if found; an empty String at least
	 */
	public static final String findRange(String json, String fromKey, String toKey) {
		Integer from = findInteger(json, fromKey);
		Integer to = findInteger(json, toKey);
		StringBuilder buf = new StringBuilder();
		buf.append(from == null ? "-∞" : from);
		buf.append(" - ");
		buf.append(to == null ? "∞" : to);
		return buf.toString();
	}
}
