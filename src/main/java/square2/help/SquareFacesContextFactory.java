package square2.help;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.context.FacesContextFactory;
import javax.faces.lifecycle.Lifecycle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.modules.model.LocaleBean;

/**
 * 
 * @author henkej
 *
 */
public class SquareFacesContextFactory extends FacesContextFactory {
	private static final Logger LOGGER = LogManager.getLogger(SquareFacesContextFactory.class);

	private FacesContextFactory delegate;

	/**
	 * generate new square faces context factory
	 * 
	 * @param facesContextFactory
	 *          the faces context factory
	 */
	public SquareFacesContextFactory(FacesContextFactory facesContextFactory) {
		delegate = facesContextFactory;
	}

	@Override
	public FacesContext getFacesContext(Object context, Object request, Object response, Lifecycle lifecycle) {
		FacesContext facesContext = delegate.getFacesContext(context, request, response, lifecycle);
		ExternalContext externalContext = facesContext.getExternalContext();
		LocaleBean bean = new LocaleBean();
		if (externalContext == null) {
			LOGGER.warn("externalContext is null, resetting it with a plain one defaulting to de_DE");
		} else {
			LocaleBean foundBean = (LocaleBean) externalContext.getSessionMap().get("lang");
			if (foundBean == null) {
				LOGGER.warn("localeBean is null, resetting it with a plain one defaulting to de_DE");
			} else {
				bean = foundBean;
			}
		}
		return new SquareFacesContext(facesContext, bean);
	}
}
