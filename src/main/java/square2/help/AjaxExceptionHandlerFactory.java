package square2.help;

import javax.faces.context.ExceptionHandler;
import javax.faces.context.ExceptionHandlerFactory;

/**
 * 
 * @author henkej
 *
 */
public class AjaxExceptionHandlerFactory extends ExceptionHandlerFactory {
	private ExceptionHandlerFactory parent;

	/**
	 * create new ajax exception handler factory
	 * 
	 * @param parent
	 *          the exception handler factory
	 */
	public AjaxExceptionHandlerFactory(ExceptionHandlerFactory parent) {
		this.parent = parent;
	}

	@Override
	public ExceptionHandler getExceptionHandler() {
		return new AjaxExceptionHandler(parent.getExceptionHandler());
	}
}
