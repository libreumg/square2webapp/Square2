package square2.help;

/**
 * 
 * @author henkej
 *
 */
public class ValuelistConverter {
	/**
	 * convert key value list to json
	 * 
	 * @param isocode
	 *          to be used for conversion
	 * @param keyValueList
	 *          to be used as input
	 * @return json representation
	 */
	public String toJson(String isocode, String keyValueList) {
		// TODO: build converter due to
		// https://shiptask.medizin.uni-greifswald.de/SQUARE/Square2/issues/233
		// CAVE: keyValueList can be null; if so, the output should be null also
		// if the keyValueList is invalid or still a json string, return that instead of
		// a json string
		return keyValueList;
	}

	/**
	 * convert json to key value list of current isocode
	 * 
	 * @param isocode
	 *          of final value list
	 * @param json
	 *          to be used to parse
	 * @return keyValueList
	 */
	public String fromJson(String isocode, String json) {
		// TODO: build converter due to
		// https://shiptask.medizin.uni-greifswald.de/SQUARE/Square2/issues/233
		// CAVE: json can be null; if so, the output should be null also
		// if the json string is invalid json, return it instead of a keyValueList
		return json;
	}
}
