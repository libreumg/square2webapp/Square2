package square2.help;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.exception.DataAccessException;

import de.jooqFaces.EJooqFacesApplicationScope;
import de.jooqFaces.JooqFacesException;
import de.jooqFaces.JooqFacesServletContext;
import de.jooqFaces.javax.JavaxJooqFacesContext;
import de.jooqFaces.javax.JavaxPropertiesDeploymentListener;
import square2.db.control.DeploymentGateway;
import square2.modules.model.LangBean;
import square2.modules.model.SquareLocale;

/**
 * 
 * @author henkej
 *
 */
public class DeploymentListener extends JavaxPropertiesDeploymentListener {
	private static final Logger LOGGER = LogManager.getLogger(DeploymentListener.class);

	@Override
	public void beforeInitialization(JooqFacesServletContext ctx) throws IOException {
		ctx.setInitParameter(EJooqFacesApplicationScope.JOOQ_FACES_PROPERTIES.get(), PropertyFileDefinition.SQUARE2_CONFIGFILE);
		super.beforeInitialization(ctx);
	}
	
	/**
	 * set context attribute from init parameter
	 * 
	 * @param key
	 * @param defaultValue
	 */
	private void setContextAttribute(JooqFacesServletContext context, ContextKey key, String defaultValue) {
		String value = context.getInitParameter(key.get());
		if (value == null) {
			value = defaultValue;
			LOGGER.warn("did not find {} in context, setting it to {}", key.get(), defaultValue);
		} else {
			LOGGER.info("set attribute {} to {}", key.get(), value.strip());
		}
		context.setAttribute(key.get(), value == null ? null : value.strip()); // defaultValue could also be null
	}
	
	@Override
	public void afterInitialization(JooqFacesServletContext context) throws IOException {
		// set defaults
		setContextAttribute(context, ContextKey.LATEXROOT, "tmp");
		setContextAttribute(context, ContextKey.LATEX_BUF_SIZE, "100000");
		setContextAttribute(context, ContextKey.RSERVERACCESS, "/opt/squareconfig.properties");
		setContextAttribute(context, ContextKey.STAGE, "unknown");
		setContextAttribute(context, ContextKey.R_PORT, "6311");
		setContextAttribute(context, ContextKey.R_HOST, "localhost");
		setContextAttribute(context, ContextKey.R_SSL, "false");
		setContextAttribute(context, ContextKey.SHELL, "/bin/bash");
		setContextAttribute(context, ContextKey.SC_OUTPUT_PATH, "/tmp");
		setContextAttribute(context, ContextKey.SR_OUTPUT_PATH, "/tmp");
		setContextAttribute(context, ContextKey.SHIP_CONFIG_SECTION, "shipdb");

		LangBean langBean = new LangBean();
		context.setAttribute(ContextKey.TRANSLATIONS.get(), langBean); // still here so we have a locale even on exceptions

		try {
			String localepath = context.getInitParameter("localepath");
			if (localepath == null) {
				localepath = "/opt/webapps/Square2/resources/locale/";
			}
			for (SquareLocale locale : SquareLocale.values()) {
				Properties properties = new Properties();
				StringBuilder buf = new StringBuilder(localepath);
				buf.append("locale_");
				buf.append(locale.get());
				buf.append(".properties");
				FileInputStream fis = new FileInputStream(buf.toString());
				properties.load(new InputStreamReader(fis, Charset.forName("UTF-8")));
				fis.close();
				langBean.addAll(locale, properties);
			}
		} catch (DataAccessException | IOException | JooqFacesException e) {
			LOGGER.error(e.getMessage(), e);
		}
		try (CloseableDSLContext jooq = JavaxJooqFacesContext.getJooqFromServletContext(context)){
			langBean.add(new DeploymentGateway(jooq).getAllTranslations(SquareLocale.values()));
		} catch (DataAccessException | ClassNotFoundException | SQLException e) {
			LOGGER.error(e.getMessage(), e);
		}
	}
}
