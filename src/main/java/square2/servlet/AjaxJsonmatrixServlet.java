package square2.servlet;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.context.SerializableContextualInstanceImpl;

import square2.json.gson.ShipJsonParseException;
import square2.json.gson.SmiJsonConverter;
import square2.modules.model.report.ReportModel;

/**
 * 
 * @author henkej
 *
 */
@WebServlet("/jsonmatrix/*")
public class AjaxJsonmatrixServlet extends SquareHttpServlet {
  private static final long serialVersionUID = 0L;

  private static final Logger LOGGER = LogManager.getLogger(AjaxJsonmatrixServlet.class);

  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setStatus(400); // error mode
    response.getWriter().write("GET not supported by that servlet");
  }

  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String json = request.getParameter("json");
    String cardinality = request.getParameter("cardinality");
    String targetColumns = request.getParameter("target_columns");
    String targetTable = request.getParameter("target_table");
    String jsonVersion = request.getParameter("version");
    String crud = request.getParameter("crud");
    String ts = request.getParameter("ts");
    LOGGER.debug("read ajax request: {}", json);
    LOGGER.debug(
        "read params: cardinality = {}, targetColumns = {}, targetTable = {}, version = {}, crud = {}, ts = {}",
        cardinality, targetColumns, targetTable, jsonVersion, crud, ts);
    Double index = 0.0;
    String error = null;
    try {
      index = ts == null ? null : Double.valueOf(ts.replace(",", "."));
    } catch (NumberFormatException e) {
      error = new StringBuilder("cannot convert ").append(ts).append(" to double").toString();
    }
    if (!SmiJsonConverter.JSON_VERSION.equals(jsonVersion)) {
      error = new StringBuilder("json version expected: ").append(SmiJsonConverter.JSON_VERSION)
          .append(", json version found not supported: ").append(jsonVersion).toString();
    }
    boolean changeSuccessful = false;
    String ajaxResult = null;
    if (error == null) {
      ajaxResult = new String(json); // clone this to omit json manipulations in the result
      try {
      	ReportModel model = getReportModel(request);
        if (model == null) {
          error = "Could not find the container of the data. Please inform the administrator (add a timestamp, maybe a screenshot and an exact description of how you could generate that bug).";
          LOGGER.error("reportModel is null in the weld bean stack");
        } else {
          changeSuccessful = model.addAjaxRequest(index, json, cardinality, targetColumns, targetTable, crud);
          HttpSession session = request.getSession(); // if session is null, getReportModel would have crashed (see 6 lines above)
          String key = getReportModelKey(); // if key was null, getReportModel would have been crashed
    			SerializableContextualInstanceImpl<?, ?> o = (SerializableContextualInstanceImpl<?, ?>) session.getAttribute(key);
          session.setAttribute(key, o); // if o was null, getReportModel would have been crashed
        }
      } catch (ShipJsonParseException jpe) {
        if (jpe.getJpe().equals(ShipJsonParseException.JSON_PARSE_ERROR.VERSION)) {
          error = "wrong json version in ajax parsed";
        } else {
          error = jpe.getMessage();
        }
      } catch (Exception e) {
				error = e.getMessage();
			}
      response.setContentType("application/json");
      response.setCharacterEncoding("UTF-8");
    }
    if (changeSuccessful && error == null) {
      response.getWriter().write(ajaxResult);
    } else {
      response.setStatus(400); // error mode
      response.getWriter().write(error);
    }
  }
}
