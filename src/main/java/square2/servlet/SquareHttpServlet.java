package square2.servlet;

import java.util.Enumeration;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jboss.weld.context.SerializableContextualInstanceImpl;

import square2.help.SquareFacesContext;
import square2.modules.model.report.ReportModel;

/**
 * 
 * @author henkej
 *
 */
public abstract class SquareHttpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(SquareHttpServlet.class);

	/**
	 * get the init parameter for the servlet context; if not found, use the default
	 * value
	 * 
	 * @param key the key
	 * @param defaultValue the default value
	 * @return the value
	 */
	protected String getServletContextInitParameter(String key, String defaultValue) {
		String value = getServletContext().getInitParameter(key);
		return value == null ? defaultValue : value;
	}

	/**
	 * get the session key of the report model
	 * 
	 * @return the session key
	 */
	protected String getReportModelKey() {
		return "WELD_S#WELD%ManagedBean%STATIC_INSTANCE|/Square2_/WEB-INF/classes|square2.modules.model.report.ReportModel|null|false";
	}

	/**
	 * get the report model from the request's session
	 * 
	 * @param request the request
	 * @return the report model if found
	 * @throws Exception if no report model could be loaded from the session
	 */
	protected ReportModel getReportModel(HttpServletRequest request) throws Exception {
		HttpSession session = request.getSession();
		if (session == null) {
			throw new Exception("session is null");
		} else {
			String key = getReportModelKey();
			SerializableContextualInstanceImpl<?, ?> o = (SerializableContextualInstanceImpl<?, ?>) session.getAttribute(key);
			if (o == null) {
				StringBuilder buf = new StringBuilder();
				Enumeration<String> sessionNames = session.getAttributeNames();
				while (sessionNames.hasMoreElements()) {
					buf.append(sessionNames.nextElement()).append(", ");
				}
				String error = "Could not find the container of the data. Please inform the administrator (add a timestamp, maybe a screenshot and an exact description of how you could generate that bug).";
				LOGGER.error(
						"could not find the container with the ReportModel in the weld managed bean stack; looking for {}, but found only {} for call {}",
						key, buf.toString(), request.getRequestURL());
				throw new Exception(error);
			} else {
				if (o.getInstance() instanceof ReportModel) {
					ReportModel model = (ReportModel) o.getInstance();
					if (model == null) {
						String error = "Could not find the container of the data. Please inform the administrator (add a timestamp, maybe a screenshot and an exact description of how you could generate that bug).";
						LOGGER.error("reportModel is null in the weld bean stack");
						throw new Exception(error);
					} else {
						return model;
					}
				} else {
					LOGGER.error("found ReportModel is not of class ReportModel, but of {}",
							o.getInstance().getClass().getSimpleName());
					throw new Exception(
							"Could not find the container of the data. Please inform the administrator (add a timestamp, maybe a screenshot and an exact description of how you could generate that bug) for call " + request.getRequestURL());
				}
			}
		}
	}
}
