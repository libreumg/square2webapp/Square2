package square2.servlet;

import java.io.IOException;
import java.util.List;

import javax.faces.context.ResponseWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import square2.r.control.SquareControl2Gateway;

/**
 * 
 * @author henkej
 *
 */
@WebServlet("/static/*")
public class RserverServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] report = request.getPathInfo().substring(1).split("/", 2);
		if (report.length == 2) {
			// TODO: get faces context from request or parent
			// TODO: call R package that provides results
//			SquareControl2Gateway gw = new SquareControl2Gateway(facesContext);
			// TODO: setup the report config by the argument's report_id from the request or directly from the session
//			String result = gw.callReport(squareRunEdition, reportconfig, relPath);
//			response.getWriter().append(result);
		} else {
			throw new IOException("invalid path");
		}
	}
}
