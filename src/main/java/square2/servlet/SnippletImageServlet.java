package square2.servlet;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.modules.model.report.NamedReportBean;
import square2.modules.model.report.ReportModel;
import square2.modules.model.report.design.CodelineBean;
import square2.modules.model.report.design.SnippletBean;

/**
 * 
 * @author henkej
 *
 */
@WebServlet("/snippletimage/*")
public class SnippletImageServlet extends HttpServlet {
	private static final long serialVersionUID = -3902390561057797849L;

	private static final Logger LOGGER = LogManager.getLogger(SnippletImageServlet.class);

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String param = request.getParameter("param");
		if (!param.contains("@")) {
			LOGGER.debug("missing @ in param");
			response.sendError(HttpServletResponse.SC_NO_CONTENT, "no @ as separator in param");
		} else {
			Integer separator = param.indexOf("@");
			String uuid = param.substring(0, separator);
			String index = param.substring(separator + 1);
			LOGGER.debug("called with uuid = {} and index = {}", uuid, index);
			response.reset();
			try {
				Integer.valueOf(index);
			} catch (NumberFormatException e) {
				response.sendError(HttpServletResponse.SC_NO_CONTENT, e.getMessage());
			}
			ReportModel rmm = (ReportModel) request.getSession().getAttribute("reportModel");
			LOGGER.debug(rmm != null ? "coming from reportModel" : "not coming from reportModel");
			if (rmm == null) {
				response.sendError(HttpServletResponse.SC_NO_CONTENT, "could not determine current model in session scope");
			} else {
				NamedReportBean reportBean = rmm.getEditReportBean();
				if (reportBean == null) {
					response.sendError(HttpServletResponse.SC_NO_CONTENT, "no report bean found in current model of session");
				} else {
					SnippletBean snip = reportBean.getSnipplet(uuid);
					if (snip == null) {
						response.sendError(HttpServletResponse.SC_NO_CONTENT, "snipplet with uuid " + uuid + " not found");
					} else if (index == null) {
						response.sendError(HttpServletResponse.SC_NO_CONTENT, "snipplet with index " + uuid + " not found");
					} else {
						Integer idx = null;
						try {
							idx = Integer.valueOf(index);
						} catch (NumberFormatException e) {
							LOGGER.error("could not convert " + index + " to number");
						}
						CodelineBean content = findIndexedCodeline(snip.getCodeLines(), idx);
						LOGGER.debug("content = {}", content);
						if (content != null) {
							byte[] bytes = decode(content.getResult(), snip.getUniqueId());
							LOGGER.debug("bytes = {}", bytes);
							response.setContentType("application/png");
							response.setContentLength(bytes == null ? 0 : bytes.length);
							if (bytes != null) {
								response.getOutputStream().write(bytes);
							}
							response.getOutputStream().flush();
						}
					}
				}
			}
		}
	}

	private CodelineBean findIndexedCodeline(List<CodelineBean> codeLines, Integer valueOf) {
		if (valueOf == null) {
			return null;
		} else {
			for (CodelineBean bean : codeLines) {
				if (valueOf.equals(bean.getIndex())) {
					return bean;
				}
			}
		}
		return null;
	}

	private byte[] decode(String input, String fileName) throws IOException {
		// doesn't work due to wrong byte chars
		// Base64.getDecoder().decode(input.getBytes());
		File rawFile = new File("/tmp/" + fileName + ".raw");
		File pngFile = new File("/tmp/" + fileName + ".png");

		FileWriter fw = new FileWriter(rawFile);
		if (input == null) {
			LOGGER.error("input is null of file " + fileName);
		} else {
			fw.write(input);
		}
		fw.flush();
		fw.close();

		while (!rawFile.exists()) {
			LOGGER.error("waiting for file to be written");
		}

		ProcessBuilder proc = new ProcessBuilder("/usr/bin/base64", "-d", rawFile.getAbsolutePath());
		proc.directory(new File("/tmp"));
		proc.redirectOutput(pngFile);

		LOGGER.debug("execute {}", proc.command().toString());
		Process p = proc.start();
		try {
			int pres = p.waitFor();
			if (pres != 0) {
				int len;
				if ((len = p.getErrorStream().available()) > 0) {
					byte[] buf = new byte[len];
					p.getErrorStream().read(buf);
					LOGGER.error("Command error:\t{}", new String(buf));
				}
				return null;
			} else {
				byte[] bytes = Files.readAllBytes(Paths.get(pngFile.getAbsolutePath()));
				return bytes;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			return null;
		}
	}
}
