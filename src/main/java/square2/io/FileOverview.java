package square2.io;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class FileOverview {
	private List<String> files;

	/**
	 * create new file overview
	 * 
	 * @param absolutRExportFolder
	 *          the absolut folder of the R export
	 * @param filter
	 *          the filter for the filename
	 */
	public FileOverview(String absolutRExportFolder, FilenameFilter filter) {
		files = new ArrayList<>();
		File file = new File(absolutRExportFolder);
		if (file.exists() && file.isDirectory()) {
			for (File f : file.listFiles(filter)) {
				files.add(f.getName());
			}
		}
	}

	/**
	 * @return the files
	 */
	public List<String> getFiles() {
		return files;
	}
}
