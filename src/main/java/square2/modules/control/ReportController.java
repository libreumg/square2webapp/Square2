package square2.modules.control;

import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import square2.modules.control.help.Pages;
import square2.modules.model.datamanagement.ReleaseBean;
import square2.modules.model.report.CalculationResultFileBean;
import square2.modules.model.report.NamedReportBean;
import square2.modules.model.report.ReportBean;
import square2.modules.model.report.ReportModel;
import square2.modules.model.report.analysis.MatrixBean;
import square2.modules.model.report.design.ReportPeriodBean;
import square2.modules.model.report.property.ReportPropertyBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@RequestScoped
public class ReportController extends NavigationController {
  @Inject
  @Named(value = "reportModel")
  private ReportModel model;

  /**
   * create a new report controller
   */
  public ReportController() {
    super(Pages.REPORT_WELCOME, Pages.REPORT_ADD, Pages.REPORT_CLONE, Pages.REPORT_EDIT, Pages.REPORT_CALCULATE,
        Pages.REPORT_MATRIX, Pages.REPORT_SHOW, Pages.REPORT_STARTED, Pages.REPORT_PROPERTIES);
  }

  @Override
  public String navigateTo(FacesContext facesContext, Pages subPage) {
    // make sure that top menu still keeps information about main page
    return navigateTo(facesContext, Pages.REPORT_WELCOME, subPage);
  }

  /**
   * navigate to welcome page
   * 
   * @return navigation destination
   */
  public String toWelcome() {
    boolean result = model.toWelcome(getFacesContext());
    // do not set sub page, so use super method
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_WELCOME) : "";
  }

  /**
   * navigate to welcome page and remove all faces messages before (validation stuff)
   * 
   * @return navigation destination
   */
  public String toWelcomeByAbort() {
    getFacesContext().clearMessages();
    return toWelcome();
  }

  /**
   * navigate to add page
   * 
   * @return navigation destination
   */
  public String toAdd() {
    return toAdd(new ReportBean(null));
  }

  /**
   * navigate to add page
   * 
   * @param bean the report bean
   * 
   * @return navigation destination
   */
  private String toAdd(ReportBean bean) {
    boolean result = model.toAdd(getFacesContext(), bean);
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_ADD) : toWelcome();
  }

  /**
   * navigate to clone page
   * 
   * @return navigation destination
   */
  public String toClone() {
    boolean result = model.toClone(getFacesContext());
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_CLONE) : toWelcome();
  }

  /**
   * navigate to show report
   * 
   * @param bean the report to be shown
   * @return navigation destination
   */
  public String toShow(ReportBean bean) {
    boolean result = model.toShow(getFacesContext(), bean);
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_SHOW) : toWelcome();
  }

  /**
   * navigate to show report and remove all messages (validation stuff)
   * 
   * @param bean the report to be shown
   * @param clearMessages clear all messages from faces context
   * @return navigation destination
   */
  public String toShowByAbort(ReportBean bean, boolean clearMessages) {
    if (clearMessages) {
      getFacesContext().clearMessages();
    }
    boolean result = model.toShow(getFacesContext(), bean);
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_SHOW) : toWelcome();
  }

  /**
   * navigate to edit report
   * 
   * @param bean the report to be edited
   * @return navigation destination
   */
  public String toEdit(ReportBean bean) {
    boolean result = model.toEdit(getFacesContext(), bean);
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_EDIT) : "";
  }

  /**
   * navigate to edit report
   * 
   * @param bean the report to be edited
   * @return navigation destination
   */
  public String toEdit(MatrixBean bean) {
    boolean result = model.toEdit(getFacesContext(), bean);
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_EDIT) : "";
  }

  /**
   * navigate to configure page
   * 
   * @param bean the report to be configured
   * @return navigation destination
   */
  public String toConfigure(ReportBean bean) {
    // TODO: load the configure part only
    boolean result = model.toEdit(getFacesContext(), bean) && model.toMatrix(getFacesContext());
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_CONFIGURE) : "";
  }

  /**
   * navigate to configure page
   * 
   * @param bean the report to be configured
   * @return navigation destination
   */
  public String toConfigure(MatrixBean bean) {
    // TODO: see the other toConfigure method
    boolean result = model.toEdit(getFacesContext(), bean) && model.toMatrix(getFacesContext());
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_CONFIGURE) : "";
  }

  /**
   * navigate to report properties list
   * 
   * @param bean the report
   * @return navigation destination
   */
  public String toProperties(ReportBean bean) {
    boolean result = model.toProperties(getFacesContext(), bean);
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_PROPERTIES) : "";
  }

  /**
   * navigate to show report
   * 
   * @param bean the report to be shown
   */
  public void doShow(ReportBean bean) {
    model.toShow(getFacesContext(), bean);
  }

  /**
   * add a property to the list
   * 
   * @return navigation destination
   */
  public String doAddProperty() {
    boolean result = model.addProperty(getFacesContext());
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_PROPERTIES) : "";
  }
  
  /**
   * open the report property editor
   * 
   * @param bean the bean
   * @return  naviation destination
   */
  public String doOpenEditProperty(ReportPropertyBean bean) {
    model.setCurrentProperty(bean.clone());
    return super.navigateTo(getFacesContext(), Pages.REPORT_PROPERTIES);
  }
  
  /**
   * update a property in the list
   * 
   * @return navigation destination
   */
  public String doUpdateProperty() {
    boolean result = model.updateProperty(getFacesContext());
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_PROPERTIES) : "";
  }

  /**
   * remove the report property bean from the list
   * 
   * @param bean the report property bean
   * @return navigation destination
   */
  public String doRemoveProperty(ReportPropertyBean bean) {
    boolean result = model.removeProperty(bean);
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_PROPERTIES) : "";
  }

  /**
   * approve the property changes
   * 
   * @return navigation destination
   */
  public String doApproveProperties() {
    boolean result = model.approveProperies(getFacesContext());
    return result ? toProperties(model.getReport()) : "";
  }

  /**
   * navigate to comment page
   * 
   * @param bean the report to be commented
   * @return navigation destination
   */
  public String toComment(ReportBean bean) {
    // TODO: load the comment part only
    boolean result = model.toEdit(getFacesContext(), bean);
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_COMMENT) : "";
  }

  /**
   * navigate to calculation page
   * 
   * @param bean the report to be calculated
   * @return navigation destination
   */
  public String toCalculate(ReportBean bean) {
    model.toEdit(getFacesContext(), bean);
    return toCalculate(model.getEditReportBean());
  }

  /**
   * navigate to calculation page
   * 
   * @param bean the report to be calculated
   * @return navigation destination
   */
  public String toCalculate(NamedReportBean bean) {
    boolean result = model.toCalculate(getFacesContext(), bean);
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_CALCULATE) : "";
  }

  /**
   * navigate to matrix page
   * 
   * @return navigation destination
   */
  public String toMatrix() {
    boolean result = model.toMatrix(getFacesContext());
    return result ? super.navigateTo(getFacesContext(), Pages.REPORT_MATRIX) : "";
  }

  /**
   * add a report
   * 
   * @return navigation destination
   */
  public String doAddReport() {
    boolean result = model.doAddReport(getFacesContext());
    ReportBean bean = model.getReport();
    return result ? toConfigure(bean) : ""; // do not discard report on errors
  }

  /**
   * clone a report
   * 
   * @return navigation destination
   */
  public String doCloneReport() {
    boolean result = model.doCloneReport(getFacesContext());
    ReportBean bean = model.getReport();
    return result ? toShow(bean) : ""; // do not discard report on errors
  }

  /**
   * update the report
   * 
   * @param closePage if true, close the page and navigate to show page
   * 
   * @return navigation destination
   */
  public String doUpdateReport(boolean closePage) {
    boolean result = model.doUpdateReport(getFacesContext());
    ReportBean bean = model.getEditReportBean();
    return (result && closePage) ? toShow(bean) : toEdit(bean);
  }

  /**
   * update the report comments
   * 
   * @param closePage if true, close the page an navigate to the show page
   * @return navigation destination
   */
  public String doUpdateReportComment(boolean closePage) {
    // TODO: use only the comments functionality from the following operations
    return Pages.REPORT_SHOW.get().equals(doUpdateReport(closePage)) ? Pages.REPORT_SHOW.get()
        : toComment(model.getEditReportBean());
  }

  /**
   * update the report configuration
   * 
   * @param closePage if true, close the page an navigate to the show page
   * @return navigation destination
   */
  public String doUpdateReportConfiguration(boolean closePage) {
    // TODO: use only the configuration functionality from the following operations
    return Pages.REPORT_SHOW.get().equals(doUpdateReport(closePage)) ? Pages.REPORT_SHOW.get()
        : toConfigure(model.getEditReportBean());
  }

  /**
   * remove the report
   * 
   * @param bean the report to be removed
   * @return navigation destination
   */
  public String doRemoveReport(ReportBean bean) {
    model.doRemoveReport(getFacesContext(), bean);
    return toWelcome();
  }

  /**
   * remove a release from the current bean, but do not make it persistent yet
   * 
   * @param releasePk the id of the release
   * @return navigation destination
   */
  public String doRemoveRelease(Integer releasePk) {
    model.doRemoveRelease(getFacesContext(), releasePk);
    ReportBean bean = model.getEditReportBean();
    return toEdit(bean);
  }

  /**
   * remove a release from the new bean, but do not make it persistent yet
   * 
   * @param releasePk the id of the release
   * @return navigation destination
   */
  public String doRemoveRelease2new(Integer releasePk) {
    Iterator<ReleaseBean> i = model.getReport().getReleases().iterator();
    while (i.hasNext()) {
      if (i.next().getPk().equals(releasePk)) {
        i.remove();
      }
    }
    return toAdd(model.getReport());
  }

  /**
   * add a release to this report bean
   * 
   * @param bean the release
   * @return navigation destination
   */
  public String doAddRelease(ReleaseBean bean) {
    model.addRelease(getFacesContext(), bean);
    ReportBean reportBean = model.getEditReportBean();
    return toEdit(reportBean);
  }

  /**
   * add a release to this new report bean
   * 
   * @param bean the release
   * @return navigation destination
   */
  public String doAddRelease2new(ReleaseBean bean) {
    model.getReport().addRelease(bean);
    return toAdd(model.getReport());
  }

  /**
   * remove a calculation interval from the current bean, but do not make it persistent yet
   * 
   * @param bean the calculation interval bean
   * @return navigation destination
   */
  public String doRemoveInterval(ReportPeriodBean bean) {
    model.doRemoveInterval(getFacesContext(), bean);
    ReportBean currentBean = model.getEditReportBean();
    return toEdit(currentBean);
  }

  /**
   * remove a calculation interval from the new bean, but do not make it persistent yet
   * 
   * @param bean the calculation interval bean
   * @return navigation destination
   */
  public String doRemoveInterval2new(ReportPeriodBean bean) {
    Iterator<ReportPeriodBean> i = model.getReport().getIntervals().iterator();
    while (i.hasNext()) {
      ReportPeriodBean b = i.next();
      if (b.getDateFrom().equals(bean.getDateFrom()) && b.getDateUntil().equals(bean.getDateUntil())) {
        i.remove();
      }
    }
    return toAdd(model.getReport());
  }

  /**
   * add a calculation interval to the report
   * 
   * @return navigation destination
   */
  public String doAddInterval() {
    model.doAddInterval(getFacesContext());
    ReportBean bean = model.getEditReportBean();
    return toEdit(bean);
  }

  /**
   * add a calculation interval to the new report
   * 
   * @return navigation destination
   */
  public String doAddInterval2new() {
    model.getReport().getIntervals().add(model.getNewInterval());
    return toAdd(model.getReport());
  }

  /**
   * generate the markdown pdf
   * 
   * @return navigation destination
   */
  public String doGenerateMarkdownResult() {
    ReportBean bean = model.getShowReportBean();
    Boolean result = model.doGenerateMarkdownResult(getFacesContext(), bean.getName());
    return result ? toShow(bean) : ""; // on success, the result is a download stream
  }

  /**
   * download the log file
   * 
   * @return navigation destination
   */
  public String doLogFileDownload() {
    ReportBean bean = model.getShowReportBean();
    model.doLogFileDownload(getFacesContext(), bean.getPk());
    return toShowByAbort(bean, false); // on success, the result is a download stream
  }

  /**
   * download the corresponding file
   * 
   * @param file the file
   * @return the main page on errors, the file otherwise
   */
  public String doDownloadFile(CalculationResultFileBean file) {
    ReportBean bean = model.getShowReportBean();
    boolean result = model.doDownloadFile(getFacesContext(), file);
    return result ? "" : toShowByAbort(bean, false); // on success, the result is a download stream
  }
  
  /**
   * @return the navigation destination
   */
  public String doCalculate() {
  	Boolean result = model.doCalculate(getFacesContext());
    return result ? navigateTo(getFacesContext(), Pages.REPORT_STARTED) : "";
  }

  /**
   * @return the navigation destination
   */
  public String toReset() {
    return navigateTo(getFacesContext(), Pages.REPORT_RESET);
  }

  /**
   * reset the calculation
   * 
   * @return navigation destination
   */
  public String doReset() {
    model.doReset(getFacesContext());
    return toCalculate(model.getCalculateReportBean());
  }

  /**
   * reload the page
   * 
   * @return navigation destination
   */
  public String doReload() {
    model.doReload(getFacesContext());
    return toCalculate(model.getCalculateReportBean());
  }

  /**
   * @return the navigation destination
   */
  public String doResetReportReleases() {
    model.resetReportReleases(getFacesContext());
    return toCalculate(model.getCalculateReportBean());
  }

  /**
   * @param releaseId the id of the release
   * @return the navigation destination
   */
  public String doRemoveReleaseInEdit(Integer releaseId) {
    model.removeRelease(releaseId);
    return toCalculate(model.getCalculateReportBean());
  }

  /**
   * @param bean the release bean
   * @return the navigation destination
   */
  public String doAddReleaseInEdit(ReleaseBean bean) {
    model.addReleaseInEdit(getFacesContext(), bean);
    return toCalculate(model.getCalculateReportBean());
  }

  /**
   * update the json matrix
   * 
   * @return the navigation destination
   */
  public String doUpdateJsonMatrix() {
    boolean result = model.updateJsonMatrix(getFacesContext());
    return result ? toConfigure(model.getMatrixBean()) : "";
  }

  /**
   * delete results of this report from the database
   * 
   * @return navigation destination
   */
  public String doDeleteResults() {
    model.deleteResults(getFacesContext());
    return toMatrix();
  }

  /**
   * update function param defaults
   * 
   * @return navigation destination
   */
  public String doUpdateFunctionparams() {
    boolean result = model.doUpdateFunctionparams(getFacesContext());
    return result ? toConfigure(model.getMatrixBean()) : "";
  }

  /**
   * fill the default of the parameter
   * 
   * @param paramName name of the parameter
   * @param value new value of the parameter
   * @param all if true, fill all fields, if false, fill the empty ones only
   * @return navigation destination
   */
  public String doFillDefaults(String paramName, String value, boolean all) {
    // do not interact with db for changes are not written to it yet
    model.doFillDefaults(paramName, value, all, getFacesContext());
    return "";
  }

  /**
   * @param list the list of report period beans
   * @return true if the report no entries, false otherwise; see https://gitlab.com/umg_hgw/sq2/square2/-/issues/633
   */
  public Boolean onlyOneTimePeriod(List<ReportPeriodBean> list) {
    if (list != null) {
      return list.size() < 1;
    } else {
      return true;
    }
  }

  @Override
  public boolean getIsCurrentController() {
    return getIsCurrentController(getFacesContext());
  }
}
