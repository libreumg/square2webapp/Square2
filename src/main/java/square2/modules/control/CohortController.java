package square2.modules.control;

import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.modules.control.help.Pages;
import square2.modules.model.AclBean;
import square2.modules.model.cohort.CohortModel;
import square2.modules.model.cohort.CohortStudyBean;
import square2.modules.model.cohort.StudygroupBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@RequestScoped
public class CohortController extends SubmenuController {
	private static final Logger LOGGER = LogManager.getLogger(CohortController.class);

	@Inject
	@Named(value = "cohortModel")
	private CohortModel model;

	/**
	 * create a cohort controller
	 */
	public CohortController() {
		super(Pages.COHORT_ADD, Pages.COHORT_EDIT, Pages.COHORT_USER, Pages.COHORT_WELCOME,
				Pages.COHORT_LISTSTUDYGROUP, Pages.COHORT_ADDSTUDYGROUP, Pages.COHORT_EDITSTUDYGROUP);
	}

	/**
	 * navigate to welcome page
	 * 
	 * @return navigation destination
	 */
	public String toWelcome() {
		forceRefresh();
		model.toWelcome(getFacesContext());
		return navigateTo(getFacesContext(), Pages.COHORT_WELCOME);
	}

	/**
	 * navigate to study group list page
	 * 
	 * @return navigation destination
	 */
	public String toListStudygroup() {
		model.toListStudygroup(getFacesContext());
		return navigateTo(getFacesContext(), Pages.COHORT_LISTSTUDYGROUP);
	}

	/**
	 * naviate to add page
	 * 
	 * @return navigation destination
	 */
	public String toAddStudy() {
		model.toAddStudy(getFacesContext());
		return navigateTo(getFacesContext(), Pages.COHORT_ADD);
	}

	/**
	 * naviate to add study group page
	 * 
	 * @return navigation destination
	 */
	public String toAddStudygroup() {
		model.toAddStudygroup(getFacesContext());
		return navigateTo(getFacesContext(), Pages.COHORT_ADDSTUDYGROUP);
	}

	/**
	 * navigate to item page
	 * 
	 * @param s
	 *          the cohort study bean
	 * @return navigation destination
	 */
	public String toUser(CohortStudyBean s) {
		model.toItem(getFacesContext(), s);
		return navigateTo(getFacesContext(), Pages.COHORT_USER);
	}

	/**
	 * @param s
	 *          the cohort study bean
	 */
	public void doShow(CohortStudyBean s) {
		model.toItem(getFacesContext(), s);
	}

	/**
	 * @param s
	 *          the cohort study bean
	 * @return navigation destination
	 */
	public String toEdit(CohortStudyBean s) {
		model.toItem(getFacesContext(), s);
		return navigateTo(getFacesContext(), Pages.COHORT_EDIT);
	}

	/**
	 * @param s
	 *          the study group bean
	 * @return navigation destination
	 */
	public String toEditStudygroup(StudygroupBean s) {
		model.toEditStudygroup(getFacesContext(), s);
		return navigateTo(getFacesContext(), Pages.COHORT_EDITSTUDYGROUP);
	}

	/**
	 * add a study
	 * 
	 * @return navigation destination
	 */
	public String doAddStudy() {
		boolean result = model.doAddStudy(getFacesContext());
		return result ? toWelcome() : navigateTo(getFacesContext(), Pages.COHORT_ADD);
	}

	/**
	 * add a study group
	 * 
	 * @return navigation destination
	 */
	public String doAddStudygroup() {
		boolean result = model.doAddStudygroup(getFacesContext());
		return result ? toListStudygroup() : navigateTo(getFacesContext(), Pages.COHORT_ADDSTUDYGROUP);
	}

	/**
	 * update a study
	 * 
	 * @return navigation destination
	 */
	public String doUpdateStudy() {
		boolean result = model.updateStudy(getFacesContext());
		return result ? toWelcome() : navigateTo(getFacesContext(), Pages.COHORT_EDIT);
	}

	/**
	 * update a study group
	 * 
	 * @return navigation destination
	 */
	public String doUpdateStudygroup() {
		boolean result = model.doUpdateStudygroup(getFacesContext());
		return result ? navigateTo(getFacesContext(), Pages.COHORT_LISTSTUDYGROUP)
				: navigateTo(getFacesContext(), Pages.COHORT_EDITSTUDYGROUP);
	}

	/**
	 * remove a study
	 * 
	 * @param bean
	 *          the bean to be removed
	 * 
	 * @return navigation destination
	 */
	public String doRemoveStudy(CohortStudyBean bean) {
		model.removeDataCollection(getFacesContext(), bean);
		forceRefresh();
		return toWelcome();
	}

	/**
	 * delete a study group and all of its children
	 * 
	 * @param bean
	 *          the study group
	 * @return navigation destination
	 */
	public String doRemoveStudygroup(StudygroupBean bean) {
		model.removeStudygroup(getFacesContext(), bean);
		return toListStudygroup();
	}

	/**
	 * add a privilege for that study
	 * 
	 * @return navigation destination
	 */
	public String doAddPrivilege() {
		model.addPrivilege(getFacesContext());
		forceRefresh();
		return toUser(model.getStudy());
	}

	/**
	 * remove a privilege from that study
	 * 
	 * @return navigation destination
	 */
	public String doDeletePrivilege() {
		model.deletePrivilege(getFacesContext());
		return toUser(model.getStudy());
	}

	/**
	 * change a privilege on that study
	 * 
	 * @return navigation destination
	 */
	public String doEditPrivilege() {
		model.editPrivilege(getFacesContext());
		return toUser(model.getStudy());
	}
	
	/**
	 * add attribute to study
	 */
	public void doAddAttribute() {
		model.addAttribute(getFacesContext());
	}

	/**
	 * remove attribute from study
	 * 
	 * @param key the id of the entry
	 */
	public void doRemoveAttribute(String key) {
		model.removeAttribute(getFacesContext(), key);
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> refreshMap() {
		try {
			return model.getRightMap(getFacesContext());
		} catch (Exception e) {
			getFacesContext().notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
		return null;
	}

  /**
   * @return true if the cancel button was hit
   */
  public boolean getCancelling() {
    return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().containsKey("form:btn_cancel");
  }

	public boolean getIsCurrentController() {
		return getIsCurrentController(getFacesContext());
	}

	@Override
	public boolean getIsCurrentSubController() {
		return getIsCurrentSubController(getFacesContext());
	}
}
