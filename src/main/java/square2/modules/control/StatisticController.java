package square2.modules.control;

import java.util.List;
import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Inject;
import javax.inject.Named;

import square2.help.SquareFacesContext;
import square2.modules.control.help.Pages;
import square2.modules.control.help.ValidationException;
import square2.modules.model.AclBean;
import square2.modules.model.statistic.FunctionBean;
import square2.modules.model.statistic.FunctionInputBean;
import square2.modules.model.statistic.StatisticModel;

/**
 * 
 * @author henkej
 * 
 */
@Named
@RequestScoped
public class StatisticController extends SubmenuController {
  @Inject
  @Named(value = "statisticModel")
  private StatisticModel model;

  /**
   * create a new statistic controller
   */
  public StatisticController() {
    super(Pages.STATISTIC_WELCOME, Pages.STATISTIC_ADD, Pages.STATISTIC_EDIT, Pages.STATISTIC_SHOW,
        Pages.STATISTIC_PACKAGES, Pages.STATISTIC_UPLOAD, Pages.STATISTIC_USER);
  }

  /**
   * navigate to statistic main
   * 
   * @return the result of toWelcome(true)
   */
  public String toWelcome() {
    return toWelcome(true);
  }

  private String toWelcome(boolean reload) {
    if (reload) {
      model.reload(getFacesContext());
      model.getRPackageInformation(getFacesContext());
    }
    toAdd();
    return navigateTo(getFacesContext(), Pages.STATISTIC_WELCOME);
  }

  /**
   * @param bean the function bean
   * @return navigation destination
   */
  public String toShow(FunctionBean bean) {
    model.editForceReload();
    model.loadEditPageContent(bean.getFunctionId(), getFacesContext());
    return navigateTo(getFacesContext(), Pages.STATISTIC_SHOW);
  }

  /**
   * @param bean the function bean
   * @return navigation destination
   */
  public String toUser(FunctionBean bean) {
    model.editForceReload();
    model.loadEditPageContent(bean.getFunctionId(), getFacesContext());
    model.toRights(bean.getFunctionId(), getFacesContext());
    return navigateTo(getFacesContext(), Pages.STATISTIC_USER);
  }

  /**
   * @return navigation destination
   */
  public String toUpload() {
    return navigateTo(getFacesContext(), Pages.STATISTIC_UPLOAD);
  }

  /**
   * @return navigation destination
   */
  public String toPackages() {
    return navigateTo(getFacesContext(), Pages.STATISTIC_PACKAGES);
  }
  
  /**
   * @return navigation destination
   */
  public String toFunctionRegistrator() {
  	model.initFunctionRegistrator();
  	return navigateTo(getFacesContext(), Pages.STATISTIC_FUNCTIONREGISTRATOR);
  }

  /**
   * navigate to edit page
   * 
   * @param functionId the id of the function
   * @return navigation destination
   */
  public String toEdit(Integer functionId) {
    model.editForceReload();
    model.loadEditPageContent(functionId, getFacesContext());
    model.toRights(functionId, getFacesContext());
    return navigateTo(getFacesContext(), Pages.STATISTIC_EDIT);
  }

  /**
   * navigate to statistic add; reset input before
   * 
   * @return navigation destination
   */
  public String toAdd() {
    model.add(getFacesContext());
    return toAddFunction();
  }

  /**
   * navigate to statistic add
   * 
   * @return navigation destination
   */
  public String toAddFunction() {
    model.toAdd(getFacesContext());
    return navigateTo(getFacesContext(), Pages.STATISTIC_ADD);
  }

  /**
   * navigate to statistic performance
   * 
   * @return navigation destination
   */
  public String toPerformance() {
    model.toPerformance(getFacesContext());
    return navigateTo(getFacesContext(), Pages.STATISTIC_WELCOME);
  }

  /**
   * @return navigation destination
   */
  public String doDeactivateFunction() {
    model.doSetState(model.getFunctionBean().getFunctionId(), false, model.getFunctionBean().getName(),
        getFacesContext());
    return toEdit(model.getFunctionBean().getFunctionId());
  }

  /**
   * @return navigation destination
   */
  public String doUpdateAndActivateFunction() {
    model.getFunctionBean().setActivated(true);
    return doUpdateFunction();
  }

  /**
   * download function as xml file
   * 
   * @param functionId of function to be used
   * @return empty String; response stream is overwritten
   */
  public String doDownloadFunction(Integer functionId) {
    model.downloadFunction(getFacesContext(), functionId);
    return "";
  }

  /**
   * download the R function as an RData file for uploading it
   * 
   * @param functionId of function to be used
   * @return empty String; response stream is overwritten
   */
  public String doExportFunction(Integer functionId) {
    model.exportFunction(getFacesContext(), functionId);
    return "";
  }

  /**
   * @return navigation destination
   */
  public String doUploadFunction() {
    FunctionBean bean = model.uploadFunction(getFacesContext());
    if (bean != null) {
      getFacesContext().notifyInformation("info.statistic.uploaded.function", bean.getName());
    }
    return bean == null ? toUpload() : toShow(bean);
  }

  /**
   * add user or group to privileges for this release
   * 
   * @return navigation destination
   */
  public String doAddUserGroupPrivilege() {
    model.addUserGroupPrivilege(getFacesContext());
    forceRefresh();
    return toUser(model.getFunctionBean());
  }

  /**
   * change user rights
   *
   * @return navigation destination
   */
  public String doEditPrivilege() {
    model.editPrivilege(getFacesContext());
    return toUser(model.getFunctionBean());
  }

  /**
   * delete privilege from db
   * 
   * @return navigation destination
   */
  public String doDeletePrivilege() {
    model.deletePrivilege(getFacesContext());
    return toUser(model.getFunctionBean());
  }

  /**
   * switch creator of function in db
   * 
   * @return result of {@link #toEdit(Integer)}
   */
  public String doSwitchCreator() {
    model.switchCreator(getFacesContext());
    return toEdit(model.getFunctionBean().getFunctionId());
  }

  /**
   * update the function
   * 
   * @return the info panel or null on errors
   */
  public String doUpdateFunction() {
    boolean result = model.updateFunction(getFacesContext());
    return result ? toShow(model.getFunctionBean()) : "";
  }

  /**
   * add function to the database
   * 
   * @param dest destination of sub page
   * 
   * @return the welcome page
   */
  public String doAddFunction(String dest) {
    boolean result = model.addFunction(dest, getFacesContext());
    return result ? toEdit(model.getFunction()) : null;
  }

  /**
   * delete function
   * 
   * @param functionId id of the function to be deleted
   * @param rightId id of the privilege for that function
   * @return the welcome page
   */
  public String doDelete(Integer functionId, Integer rightId) {
    model.deleteFunction(functionId, rightId, getFacesContext());
    return toWelcome();
  }

  /**
   * add function input to bean
   * 
   * @return the edit page
   */
  public String doAjaxAddFunctionInput() {
    SquareFacesContext facesContext = getFacesContext();
    try {
      model.addFunctionInput(facesContext);
      model.setAjaxInputMessage("");
    } catch (ValidationException e) {
      model.setAjaxInputMessage(e.getMessage());
    }
    return navigateTo(facesContext, Pages.STATISTIC_EDIT);
  }

  /**
   * remove function input from bean
   * 
   * @param bean the function bean
   * @return navigation destination
   */
  public String doAjaxRemoveFunctionInput(FunctionInputBean bean) {
    model.removeFunctionInput(bean);
    return navigateTo(getFacesContext(), Pages.STATISTIC_EDIT);
  }

  /**
   * add function output to bean
   * 
   * @return navigation destination
   */
  public String doAjaxAddFunctionOutput() {
    SquareFacesContext facesContext = getFacesContext();
    try {
      model.addFunctionOutput(facesContext);
      model.setAjaxOutputMessage("");
    } catch (ValidationException e) {
      model.setAjaxOutputMessage(e.getMessage());
    }
    return navigateTo(facesContext, Pages.STATISTIC_EDIT);
  }

  /**
   * remove function output from bean
   * 
   * @param output the output
   * @return navigation destination
   */
  public String doAjaxRemoveFunctionOutput(String output) {
    model.removeFunctionOutput(output);
    return navigateTo(getFacesContext(), Pages.STATISTIC_EDIT);
  }

  /**
   * add function reference to function bean
   * 
   * @param dest the panel
   * 
   * @return navigation destination
   */
  public String addFunctionReference(String dest) {
    model.addFunctionReference();
    toAddFunction();
    return navigate(dest);

  }

  /**
   * remove function reference from function bean
   * 
   * @param functionId id of the function
   * @param dest destination of navigation
   * @return result of {@link #navigate(String)}
   */
  public String doRemoveFunctionReference(Integer functionId, String dest) {
    model.removeFunctionReference(functionId);
    toAddFunction();
    return navigate(dest);
  }

  /**
   * navigate to dest
   * 
   * @param dest the panel
   * @return navigation destination
   */
  private String navigate(String dest) {
    if ("add".equals(dest)) {
      return Pages.STATISTIC_ADD.get();
    } else if ("editFunction".equals(dest)) {
      return Pages.STATISTIC_EDIT.get();
    } else if ("edit".equals(dest)) {
      return Pages.STATISTIC_EDIT.get();
    } else {
      return "";
    }
  }

  /**
   * set state on function
   * 
   * @param functionId the id of the function
   * @param activate the flag to activate or deactivate the function
   * @param functionName the name of the function
   * @return navigation destination
   */
  public String doSetState(Integer functionId, Boolean activate, String functionName) {
    model.doSetState(functionId, activate, functionName, getFacesContext());
    return toWelcome();
  }
  
  /**
   * call the function registrator to recreate the function wrappers
   * 
   * @return navigation destination
   */
  public String doCallFunctionRegistrator() {
  	model.doCallFunctionRegistrator(getFacesContext());
  	return toWelcome();
  }

  /**
   * listen function dropdown change
   * 
   * @param event the value change event from the ajax request
   */
  public void listenFunctionChange(ValueChangeEvent event) {
    refreshCurrentFunction((Integer) event.getNewValue());
  }

  /**
   * refresh current function for display
   * 
   * @param functionId the id of the function
   */
  private void refreshCurrentFunction(Integer functionId) {
    model.refreshCurrentFunction(functionId);
  }

  /**
   * check, if current user has right for this function to edit it
   * 
   * @param functionId the id of the function
   * @return the acl bean
   */
  public AclBean getUserRightLevel(Integer functionId) {
    return model.getUserRightLevel(functionId, getFacesContext().getUsnr());
  }

  /**
   * @return the function types
   */
  public List<String> getFunctiontypes() {
    return model.getFunctiontypes();
  }

  @Override
  public Map<Integer, Map<Integer, AclBean>> refreshMap() {
    return null;
  }

  @Override
  public boolean getIsCurrentSubController() {
    return getIsCurrentSubController(getFacesContext());
  }

  @Override
  public boolean getIsCurrentController() {
    return getIsCurrentController(getFacesContext());
  }
}
