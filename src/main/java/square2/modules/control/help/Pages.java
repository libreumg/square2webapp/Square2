package square2.modules.control.help;

/**
 * 
 * @author henkej
 * 
 */
public enum Pages {
	/**
	 * main.jsf
	 */
	MAIN("/pages/main.jsf"),

	/**
	 * impressum.jsf
	 */
	IMPRESSUM("/pages/impressum.jsf"),

	/**
	 * login.jsf
	 */
	LOGIN("/pages/login.jsf"),

	/**
	 * profile.jsf
	 */
	PROFILE("/pages/profile.jsf"),

	/**
	 * password.jsf
	 */
	PASSWORD("/pages/password.jsf"),

	/**
	 * admin/welcome.jsf
	 */
	ADMIN_WELCOME("/pages/admin/welcome.jsf"),
	/**
	 * admin/applrights.jsf
	 */
	ADMIN_APPLRIGHTS("/pages/admin/applrights.jsf"),
	/**
	 * admin/applroles.jsf
	 */
	ADMIN_APPLROLES("/pages/admin/applroles.jsf"),
	/**
	 * admin/users.jsf
	 */
	ADMIN_USERS("/pages/admin/users.jsf"),
	/**
	 * admin/usergroup.jsf
	 */
	ADMIN_USERGROUP("/pages/admin/usergroup.jsf"),
	/**
	 * admin/session.jsf
	 */
	ADMIN_SESSION("/pages/admin/session.jsf"),
	/**
	 * admin/logins.jsf
	 */
	ADMIN_LOGINS("/pages/admin/logins.jsf"),
	/**
	 * admin/functioninputtype.jsf
	 */
	ADMIN_FUNCTIONINPUTTYPE("/pages/admin/functioninputtype.jsf"),
	/**
	 * admin/functionoutputtype.jsf
	 */
	ADMIN_FUNCTIONOUTPUTTYPE("/pages/admin/functionoutputtype.jsf"),
	/**
	 * admin/snippletadmin.jsf
	 */
	ADMIN_SNIPPLET("/pages/admin/snippletadmin.jsf"),
	/**
	 * admin/maintenance/list.jsf
	 */
	ADMIN_MAINTENANCE_LIST("/pages/admin/maintenance/list.jsf"),
	/**
	 * admin/maintenance/edit.jsf
	 */
	ADMIN_MAINTENANCE_EDIT("/pages/admin/maintenance/edit.jsf"),
	/**
	 * admin/fileupload.jsf
	 */
	ADMIN_FILEUPLOAD("/pages/admin/fileupload.jsf"),
	/**
	 * admin/impressum.jsf
	 */
	ADMIN_IMPRESSUM("/pages/admin/impressum.jsf"),
	/**
	 * admin/functioncategory/list.jsf
	 */
	ADMIN_FUNCTIONCATEGORY_LIST("/pages/admin/functioncategory/list.jsf"),
	/**
	 * admin/functioncategory/list.jsf
	 */
	ADMIN_FUNCTIONCATEGORY_EDIT("/pages/admin/functioncategory/edit.jsf"),
	/**
	 * admin/metadatatype/list.jsf
	 */
	ADMIN_METADATATYPE_LIST("/pages/admin/metadatatype/list.jsf"),
	/**
	 * admin/metadatatype/edit.jsf
	 */
	ADMIN_METADATATYPE_EDIT("/pages/admin/metadatatype/edit.jsf"),

	/**
	 * statistic/welcome.jsf
	 */
	STATISTIC_WELCOME("/pages/statistic/welcome.jsf"),
	/**
	 * statistic/add.jsf
	 */
	STATISTIC_ADD("/pages/statistic/add.jsf"),
	/**
	 * statistic/edit.jsf
	 */
	STATISTIC_EDIT("/pages/statistic/edit.jsf"),
	/**
	 * statistic/upload.jsf
	 */
	STATISTIC_UPLOAD("/pages/statistic/upload.jsf"),
	/**
	 * statistic/show.jsf
	 */
	STATISTIC_SHOW("/pages/statistic/show.jsf"),
	/**
	 * statistic/user.jsf
	 */
	STATISTIC_USER("/pages/statistic/user.jsf"),
	/**
	 * statistic/packages.jsf
	 */
	STATISTIC_PACKAGES("/pages/statistic/packages.jsf"), 
	/**
	 * statistic/functionregistrator.jsf
	 */
	STATISTIC_FUNCTIONREGISTRATOR("/pages/statistic/functionregistrator.jsf"),

	/**
	 * datamanagement/welcome.jsf
	 */
	DATAMANAGEMENT_WELCOME("/pages/datamanagement/welcome.jsf"),
	/**
	 * datamanagement/add.jsf
	 */
	DATAMANAGEMENT_ADD("/pages/datamanagement/add.jsf"),
	/**
	 * datamanagement/user.jsf
	 */
	DATAMANAGEMENT_USER("/pages/datamanagement/user.jsf"),
	/**
	 * datamanagement/upload.jsf
	 */
	DATAMANAGEMENT_UPLOAD("/pages/datamanagement/upload.jsf"),
	/**
	 * datamanagement/edit.jsf
	 */
	DATAMANAGEMENT_EDIT("/pages/datamanagement/edit.jsf"),

	/**
	 * cohort/welcome.js
	 */
	COHORT_WELCOME("/pages/cohort/welcome.jsf"),
	/**
	 * cohort/user.jsf
	 */
	COHORT_USER("/pages/cohort/user.jsf"),
	/**
	 * cohort/add.jsf
	 */
	COHORT_ADD("/pages/cohort/add.jsf"),
	/**
	 * cohort/edit.jsf
	 */
	COHORT_EDIT("/pages/cohort/edit.jsf"),
	/**
	 * cohort/studygroup/list.jsf
	 */
	COHORT_LISTSTUDYGROUP("/pages/cohort/studygroup/list.jsf"), 
	/**
	 * cohort/studygroup/add.jsf
	 */
	COHORT_ADDSTUDYGROUP("/pages/cohort/studygroup/add.jsf"),
	/**
	 * cohort/studygroup/edit.jsf
	 */
	COHORT_EDITSTUDYGROUP("/pages/cohort/studygroup/edit.jsf"),

	/**
	 * study/welcome.jsf
	 */
	STUDY_WELCOME("/pages/study/welcome.jsf"),
	/**
	 * study/show.jsf
	 */
	STUDY_SHOW("/pages/study/show.jsf"),
	/**
	 * study/edit.jsf
	 */
	STUDY_EDIT("/pages/study/edit.jsf"),
	/**
	 * study/user.jsf
	 */
	STUDY_USER("/pages/study/user.jsf"),
	/**
	 * study/metadata.jsf
	 */
	STUDY_METADATA("/pages/study/metadata.jsf"),
	/**
	 * study/addDepartment.jsf
	 */
	STUDY_ADD_DEPARTMENT("/pages/study/addDepartment.jsf"),
	/**
	 * study/addVariable.jsf
	 */
	STUDY_ADD_VARIABLE("/pages/study/addVariable.jsf"),
	/**
	 * study/editVariable.jsf
	 */
	STUDY_EDIT_VARIABLE("/pages/study/editVariable.jsf"),
	/**
	 * study/editDepartment.jsf
	 */
	STUDY_EDIT_DEPARTMENT("/pages/study/editDepartment.jsf"),
	/**
	 * study/listStudies.jsf
	 */
	STUDY_LIST_STUDIES("/pages/study/listStudies.jsf"),
	/**
	 * study/deptmeta.jsf
	 */
	STUDY_DEPTMETA("/pages/study/deptmeta.jsf"),
	/**
	 * study/editDeptmeta.jsf
	 */
	STUDY_EDITDEPTMETA("/pages/study/editDeptmeta.jsf"),
	/**
	 * study/missinglist/welcome.jsf
	 */
	STUDY_MISSINGLIST("/pages/study/missinglist/welcome.jsf"),
  /**
   * study/missinglist/edit.jsf
   */
  STUDY_MISSINGLIST_EDIT("/pages/study/missinglist/edit.jsf"), 
  /**
   * study/studyvars/list.jsf
   */
  STUDY_STUDYVARS("/pages/study/studyvars/edit.jsf"),

	/**
	 * vargroup/welcome.jsf
	 */
	VARGROUP_WELCOME("/pages/vargroup/welcome.jsf"),
	/**
	 * vargroup/add.jsf
	 */
	VARGROUP_ADD("/pages/vargroup/add.jsf"),
	/**
	 * vargroup/show.jsf
	 */
	VARGROUP_SHOW("/pages/vargroup/show.jsf"),
	/**
	 * vargroup/edit.jsf
	 */
	VARGROUP_EDIT("/pages/vargroup/edit.jsf"),
	/**
	 * vargroup/user.jsf
	 */
	VARGROUP_USER("/pages/vargroup/user.jsf"),
	/**
	 * vargroup/definition.jsf
	 */
	VARGROUP_DEFINITION("/pages/vargroup/definition.jsf"),
	/**
	 * vargroup/content.jsf
	 */
	VARGROUP_CONTENT("/pages/vargroup/content.jsf"),

	/**
	 * report/welcome.jsf
	 */
	REPORT_WELCOME("/pages/report/welcome.jsf"),
	/**
	 * report/add.jsf
	 */
	REPORT_ADD("/pages/report/add.jsf"),
	/**
	 * report/clone.jsf
	 */
	REPORT_CLONE("/pages/report/clone.jsf"),
	/**
	 * report/show.jsf
	 */
	REPORT_SHOW("/pages/report/show.jsf"),
	/**
	 * report/edit.jsf
	 */
	REPORT_EDIT("/pages/report/edit.jsf"),
  /**
   * report/properties.jsf
   */
  REPORT_PROPERTIES("/pages/report/properties.jsf"),
	/**
	 * report/configure.jsf
	 */
	REPORT_CONFIGURE("/pages/report/configure.jsf"),
	/**
	 * report/comment.jsf
	 */
	REPORT_COMMENT("/pages/report/comment.jsf"),
	/**
	 * report/calculate.jsf
	 */
	REPORT_CALCULATE("/pages/report/calculate.jsf"),
	/**
	 * report/started.jsf
	 */
	REPORT_STARTED("/pages/report/started.jsf"),
	/**
	 * report/reset.jsf
	 */
	REPORT_RESET("/pages/report/reset.jsf"),
	/**
	 * report/matrix.jsf
	 */
	REPORT_MATRIX("/pages/report/matrix.jsf"),

	/**
	 * template/welcome.jsf
	 */
	TEMPLATE_WELCOME("/pages/template/welcome.jsf"),
	/**
	 * pages/functionselection/welcome.jsf
	 */
	FUNCTIONSELECTION_WELCOME("/pages/functionselection/welcome.jsf"),
	/**
	 * pages/functionselection/item.jsf
	 */
	FUNCTIONSELECTION_ADD("/pages/functionselection/add.jsf"),
	/**
	 * pages/functionselection/item.jsf
	 */
	FUNCTIONSELECTION_CLONE("/pages/functionselection/clone.jsf"),
	/**
	 * pages/functionselection/show.jsf
	 */
	FUNCTIONSELECTION_SHOW("/pages/functionselection/show.jsf"),
	/**
	 * pages/functionselection/show.jsf
	 */
	FUNCTIONSELECTION_EDIT("/pages/functionselection/edit.jsf"),
	/**
	 * pages/functionselection/subitem.jsf
	 */
	FUNCTIONSELECTION_SUBITEM("/pages/functionselection/subitem.jsf"),
	/**
	 * template/report/welcome.jsf
	 */
	TEMPLATE_REPORT_WELCOME("/pages/template/report/welcome.jsf"),
	/**
	 * template/report/edit.jsf
	 */
	TEMPLATE_REPORT_EDIT("/pages/template/report/edit.jsf"),
	/**
	 * template/report/add.jsf
	 */
	TEMPLATE_REPORT_ADD("/pages/template/report/add.jsf"),
	/**
	 * template/report/copy.jsf
	 */
	TEMPLATE_REPORT_COPY("/pages/template/report/copy.jsf"),
	/**
	 * template/report/info.jsf
	 */
	TEMPLATE_REPORT_INFO("/pages/template/report/info.jsf");

	private final String s;

	private Pages(String s) {
		this.s = s;
	}

	/**
	 * get the values
	 * 
	 * @return the value
	 */
	public String get() {
		return s;
	}
}
