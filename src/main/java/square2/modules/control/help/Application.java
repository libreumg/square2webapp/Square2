package square2.modules.control.help;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.help.ContextKey;
import square2.help.SquareFacesContext;

/**
 * 
 * @author henkej
 * 
 */
@Named(value = "appl")
@ApplicationScoped
public class Application {
	private static final Logger LOGGER = LogManager.getLogger(Application.class);

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder(this.getClass().getSimpleName());
		builder.append("@{}");
		return builder.toString();
	}

	/**
	 * get application version from pom file
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * 
	 * @return application version (deployment)
	 */
	public String getVersion(SquareFacesContext facesContext) {
		final Properties prop = new Properties();
		try {
			InputStream is = facesContext.getExternalContext().getResourceAsStream("/META-INF/MANIFEST.MF");
			prop.load(is);
			if (is != null) {
				is.close();
			}
			return prop.getProperty("Implementation-Version");
		} catch (final IOException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return e.getMessage();
		}
	}

	/**
	 * get the stage of the application
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return the stage
	 */
	public String getStage(SquareFacesContext facesContext) {
		return (String) facesContext.getExternalContext().getApplicationMap().get(ContextKey.STAGE.get());
	}
}
