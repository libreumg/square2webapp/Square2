package square2.modules.control;

import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import square2.modules.control.help.Pages;
import square2.modules.model.AclBean;
import square2.modules.model.study.VariablegroupBean;
import square2.modules.model.vargroup.VargroupModel;

/**
 * 
 * @author henkej
 *
 */
@Named
@RequestScoped
public class VargroupController extends SubmenuController {
	@Inject
	@Named(value = "vargroupModel")
	private VargroupModel model;

	/**
	 * generate new variable group controller
	 */
	public VargroupController() {
		super(Pages.VARGROUP_WELCOME, Pages.VARGROUP_ADD, Pages.VARGROUP_SHOW, Pages.VARGROUP_EDIT, Pages.VARGROUP_USER,
				Pages.VARGROUP_DEFINITION, Pages.VARGROUP_CONTENT);
	}

	/**
	 * @return navigation destination
	 */
	public String toWelcome() {
		model.resetWelcome(false, getFacesContext());
		return navigateTo(getFacesContext(), Pages.VARGROUP_WELCOME);
	}

	/**
	 * @return navigation destination
	 */
	public String toAdd() {
		return navigateTo(getFacesContext(), Pages.VARGROUP_ADD);
	}

	/**
	 * @param bean
	 *          the bean
	 * @return navigation destination
	 */
	public String toShow(VariablegroupBean bean) {
		model.resetInfo(bean.getVariablegroupId(), getFacesContext());
		return navigateTo(getFacesContext(), Pages.VARGROUP_SHOW);
	}

	/**
	 * @param bean
	 *          the bean
	 * @return navigation destination
	 */
	public String toUser(VariablegroupBean bean) {
		model.resetInfo(bean.getVariablegroupId(), getFacesContext());
		model.resetEditUser(bean.getVariablegroupId(), getFacesContext());
		model.resetVariablegroup(bean.getVariablegroupId(), getFacesContext());
		model.resetVariablegroupContent(bean.getVariablegroupId(), null, null, true, getFacesContext());
		model.setCurrentList(getFacesContext(), bean.getVariablegroupId());
		return navigateTo(getFacesContext(), Pages.VARGROUP_USER);
	}

	/**
	 * navigate to items page
	 * 
	 * @param bean
	 *          the selected variable group bean
	 * @return navigation destination
	 */
	public String toEdit(VariablegroupBean bean) {
		return toEdit(bean, "hierarchy");
	}

	private String toEdit(VariablegroupBean bean, String accordionPage) {
		model.resetInfo(bean.getVariablegroupId(), getFacesContext());
		model.resetEditUser(bean.getVariablegroupId(), getFacesContext());
		model.resetVariablegroup(bean.getVariablegroupId(), getFacesContext());
		model.resetVariablegroupContent(bean.getVariablegroupId(), null, null, true, getFacesContext());
		model.setCurrentList(getFacesContext(), bean.getVariablegroupId());
		return navigateTo(getFacesContext(), Pages.VARGROUP_EDIT);
	}

	/**
	 * @return navigation destination
	 */
	public String toDefinition() {
		return navigateTo(getFacesContext(), Pages.VARGROUP_DEFINITION);
	}

	/**
	 * @return navigation destination
	 */
	public String toContent() {
		return navigateTo(getFacesContext(), Pages.VARGROUP_CONTENT);
	}

	/**
	 * update the variable group order
	 * 
	 * @return navigation destination
	 */
	public String doUpdateVariablegroupOrder() {
		model.updateCurrentList(getFacesContext());
		return toEdit(model.getVariablegroupBean(), "sortorder");
	}

	/**
	 * set the auto order
	 * 
	 * @return navigation destination
	 */
	public String doSetAutoOrder() {
		model.doAutoOrder(getFacesContext());
		return navigateTo(getFacesContext(), Pages.VARGROUP_EDIT); // do not reload as database is not yet changed
	}

	/**
	 * download metadata overview
	 * 
	 * @return navigation destination
	 */
	public String doDownloadVariableAttributes() {
		model.downloadVariableAttributes(getFacesContext());
		return toShow(model.getVariablegroupBean()); // return show panel on errors only
	}

	/**
	 * remove privilege
	 * 
	 * @return navigation destination
	 */
	public String doDeletePrivilege() {
		model.deletePrivilege(getFacesContext());
		return toUser(model.getVariablegroupBean());
	}

	/**
	 * add user or group privilege
	 * 
	 * @return navigation destination
	 */
	public String doAddUserGroupPrivilege() {
		model.addUserGroupPrivilege(getFacesContext());
		forceRefresh();
		return toUser(model.getVariablegroupBean());
	}

	/**
	 * edit user or group privilege
	 * 
	 * @return navigation destination
	 */
	public String doEditPrivilege() {
		model.editPrivilege(getFacesContext());
		return toUser(model.getVariablegroupBean());
	}

	/**
	 * change the hierarchy
	 * 
	 * @param hierarchyPosition
	 *          the new position in the hierarchy tree
	 * 
	 * @return navigation destination
	 */
	public String doChangeHierarchy(Integer hierarchyPosition) {
		return doChangeHierarchy(hierarchyPosition, true);
	}

	/**
	 * change the hierarchy
	 * 
	 * @param hierarchyPosition
	 *          the new position in the hierarchy tree
	 * @param resetVariablegroupContent
	 *          flag to determine if the variable group content must be reloaded
	 * 
	 * @return navigation destination
	 */
	private String doChangeHierarchy(Integer hierarchyPosition, Boolean resetVariablegroupContent) {
		if (resetVariablegroupContent) {
			model.resetVariablegroupContent(model.getVariablegroupBean().getVariablegroupId(), hierarchyPosition,
					model.getVariables(), false, getFacesContext());
		}
		// do not reload item to not forget the new hierarchy position
		return navigateTo(getFacesContext(), Pages.VARGROUP_CONTENT);
	}

	/**
	 * change the list content
	 * 
	 * @return navigation destination
	 */
	public String doChangeListContent() {
		model.doChangeListContent(getFacesContext());
		return toEdit(model.getVariablegroupBean());
	}

	/**
	 * revert the list changes
	 * 
	 * @return navigation destination
	 */
	public String doRevertListContent() {
		model.resetVariablegroupContent(model.getVariablegroupBean().getVariablegroupId(), model.getHierarchyPosition(),
				null, true, getFacesContext());
		return toEdit(model.getVariablegroupBean());
	}

	/**
	 * select all current items
	 * 
	 * @param chosen
	 *          flag to set or unset all current items
	 * @return navigation destination
	 */
	public String doSelectAllCurrent(Boolean chosen) {
		model.doSelectAllCurrent(chosen);
		return doChangeHierarchy(model.getHierarchyPosition(), true);
	}

	/**
	 * select all items under elementId
	 * 
	 * @param elementId
	 *          id of the parent element
	 * @param chosen
	 *          flag to set or unset all items under elementId
	 * @return navigation destination
	 */
	public String doSelectAllUnder(Integer elementId, Boolean chosen) {
		model.setChildrenVariables(elementId, chosen);
		return doChangeHierarchy(model.getHierarchyPosition(), true);
	}

	/**
	 * execute selection
	 * 
	 * @param elementId
	 *          the id of the element
	 * @param add
	 *          flag to add or remove the element from the selection
	 * @return navigation destination
	 */
	public String doSelect(Integer elementId, boolean add) {
		if (add) {
			model.addToSelection(elementId);
		} else {
			model.removeFromSelection(elementId);
		}
		return doChangeHierarchy(model.getHierarchyPosition(), true);
	}

	/**
	 * update he definition
	 * 
	 * @return navigation destination
	 */
	public String doUpdateDefinition() {
		model.updateDefinition(getFacesContext());
		return toEdit(model.getVariablegroupBean(), "definition");
	}

	/**
	 * create a new variable group
	 * 
	 * @return navigation destination
	 */
	public String doAddVariablegroup() {
		boolean result = model.addVariablegroup(getFacesContext());
		return result ? toEdit(model.getVariablegroupBean(), "hierarchy") : toWelcome();
	}

	/**
	 * remove the variable group
	 * 
	 * @param id
	 *          id of the variable group
	 * @param rightId
	 *          privilege id of the variable group
	 * @return navigation destination
	 */
	public String doDeleteVariablegroup(Integer id, Integer rightId) {
		model.deleteVariablegroup(id, rightId, getFacesContext());
		return toWelcome();
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> refreshMap() {
		return null;
	}

	@Override
	public boolean getIsCurrentSubController() {
		return getIsCurrentSubController(getFacesContext());
	}

	@Override
	public boolean getIsCurrentController() {
		return getIsCurrentController(getFacesContext());
	}
}
