package square2.modules.control;

import java.util.Map;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.modules.control.help.Pages;
import square2.modules.model.AclBean;
import square2.modules.model.datamanagement.DatamanagementModel;
import square2.modules.model.datamanagement.ReleaseBean;
import square2.modules.model.datamanagement.ReleaseInfoBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@RequestScoped
public class DatamanagementController extends SubmenuController {
	private static final Logger LOGGER = LogManager.getLogger(DatamanagementController.class);

	@Inject
	@Named(value = "datamanagementModel")
	private DatamanagementModel model;

	/**
	 * create new datamanagement controller
	 */
	public DatamanagementController() {
		super(Pages.DATAMANAGEMENT_WELCOME, Pages.DATAMANAGEMENT_ADD, Pages.DATAMANAGEMENT_EDIT, Pages.DATAMANAGEMENT_USER,
				Pages.DATAMANAGEMENT_UPLOAD);
	}

	/**
	 * navigate to main view
	 * 
	 * @return navigation destination
	 */
	public String toWelcome() {
		boolean result = model.prepareWelcomePage(getFacesContext());
		return result ? navigateTo(getFacesContext(), Pages.DATAMANAGEMENT_WELCOME) : "";
	}

	/**
	 * navigate to user page
	 * 
	 * @param release
	 *          the release
	 * @return navigation destinations
	 */
	public String toUser(ReleaseBean release) {
		forceRefresh();
		model.prepareItemPage(release, getFacesContext());
		return navigateTo(getFacesContext(), Pages.DATAMANAGEMENT_USER);
	}

	/**
	 * navigate to add page
	 * 
	 * @return navigation destination
	 */
	public String toAdd() {
		return navigateTo(getFacesContext(), Pages.DATAMANAGEMENT_ADD);
	}

	/**
	 * navigate to upload page
	 * 
	 * @param release
	 *          the release
	 * @return navigation destination
	 */
	public String toUpload(ReleaseBean release) {
		forceRefresh();
		model.prepareItemPage(release, getFacesContext());
		return navigateTo(getFacesContext(), Pages.DATAMANAGEMENT_UPLOAD);
	}

	/**
	 * navigate to edit page
	 * 
	 * @param release
	 *          the release
	 * @return navigation destination
	 */
	public String toEdit(ReleaseBean release) {
		forceRefresh();
		model.prepareItemPage(release, getFacesContext());
		return navigateTo(getFacesContext(), Pages.DATAMANAGEMENT_EDIT);
	}
	
	/**
	 * load all info for the release found in release bean
	 * 
	 * @param bean the release bean
	 */
	public void doInfo(ReleaseBean bean) {
		model.setInfoBean(new ReleaseInfoBean(bean));
		model.loadExtraInfo(getFacesContext());
	}

	/**
	 * add release to db
	 * 
	 * @return navigation destination
	 */
	public String doAddRelease() {
		boolean result = model.doAddRelease(getFacesContext());
		return result ? toEdit(model.getRelease()) : "";
	}

	/**
	 * edit release and return to list
	 * 
	 * @return navigation destination
	 */
	public String doEditRelease() {
		model.doEditRelease(getFacesContext());
		return toWelcome();
	}

	/**
	 * remove release from db
	 * 
	 * @param bean
	 *          the release bean
	 * 
	 * @return navigation destination
	 */
	public String doDeleteRelease(ReleaseBean bean) {
		model.doDeleteRelease(getFacesContext(), bean);
		return toWelcome();
	}

	/**
	 * add user or group to privileges for this release
	 * 
	 * @return navigation destination
	 */
	public String doAddUserGroupPrivilege() {
		model.addPrivilege(getFacesContext());
		forceRefresh();
		return toUser(model.getRelease());
	}

	/**
	 * change user rights
	 *
	 * @return navigation destination
	 */
	public String doEditPrivilege() {
		model.editPrivilege(getFacesContext());
		return toUser(model.getRelease());
	}

	/**
	 * delete privilege from db
	 * 
	 * @return navigation destination
	 */
	public String doDeletePrivilege() {
		model.deletePrivilege(getFacesContext());
		return toUser(model.getRelease());
	}

	@Override
	public Map<Integer, Map<Integer, AclBean>> refreshMap() {
		try {
			return model.getRightMap(getFacesContext());
		} catch (Exception e) {
			getFacesContext().notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
		return null;
	}

	@Override
	public boolean getIsCurrentSubController() {
		return getIsCurrentSubController(getFacesContext());
	}

	@Override
	public boolean getIsCurrentController() {
		return getIsCurrentController(getFacesContext());
	}
}
