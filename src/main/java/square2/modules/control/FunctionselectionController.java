package square2.modules.control;

import java.util.List;
import java.util.Locale;

import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import square2.modules.control.help.Pages;
import square2.modules.model.functionselection.FunctionselectionBean;
import square2.modules.model.functionselection.FunctionselectionModel;
import square2.modules.model.report.analysis.AnalysisFunctionBean;
import square2.modules.model.statistic.FunctionBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@RequestScoped
public class FunctionselectionController extends NavigationController {
	@Inject
	@Named(value = "functionselectionModel")
	private FunctionselectionModel model;
	
  private List<AnalysisFunctionBean> analysisFunctionBeansFilter;
	
	/**
	 * create new template analysis controller
	 */
	public FunctionselectionController() {
		super(Pages.FUNCTIONSELECTION_WELCOME, Pages.FUNCTIONSELECTION_EDIT, Pages.FUNCTIONSELECTION_SUBITEM,
				Pages.FUNCTIONSELECTION_ADD, Pages.FUNCTIONSELECTION_CLONE, Pages.FUNCTIONSELECTION_SHOW);
	}

	/**
	 * navigate to welcome
	 * 
	 * @return navigation destination
	 */
	public String toWelcome() {
		model.toTemplatelist(getFacesContext());
		return navigateTo(getFacesContext(), Pages.FUNCTIONSELECTION_WELCOME);
	}

	/**
	 * navigate to add
	 * 
	 * @return navigation destination
	 */
	public String toAdd() {
		return navigateTo(getFacesContext(), Pages.FUNCTIONSELECTION_ADD);
	}

	/**
	 * navigate to clone
	 * 
	 * @return navigation destination
	 */
	public String toClone() {
		return navigateTo(getFacesContext(), Pages.FUNCTIONSELECTION_CLONE);
	}

	/**
	 * navigate to show
	 * 
	 * @param chosen
	 *          the bean to show
	 * @return navigation destination
	 */
	public String toShow(FunctionselectionBean chosen) {
		model.loadTemplateBean(chosen.getFkFunctionlist(), getFacesContext());
		model.loadUsage(getFacesContext(), chosen.getFkFunctionlist());
		return navigateTo(getFacesContext(), Pages.FUNCTIONSELECTION_SHOW);
	}

	/**
	 * navigate to edit
	 * 
	 * @param chosen
	 *          the bean to edit
	 * @return navigation destination
	 */
	public String toEdit(FunctionselectionBean chosen) {
		model.loadTemplateBean(chosen.getFkFunctionlist(), getFacesContext());
		model.toEditTemplateContent(chosen.getFkFunctionlist(), getFacesContext());
		return navigateTo(getFacesContext(), Pages.FUNCTIONSELECTION_EDIT);
	}

	/**
	 * navigate to sub item
	 * 
	 * @param afb
	 *          the analysis function bean
	 * @return navigation destination
	 */
	public String toSubitem(AnalysisFunctionBean afb) {
		model.toEditTemplateContentFunction(afb, getFacesContext());
		return navigateTo(getFacesContext(), Pages.FUNCTIONSELECTION_SUBITEM);
	}

	/**
	 * approve the preferences
	 * 
	 * @return navigation destination
	 */
	public String doApprovePreferences() {
		boolean ready = model.approvePreferences(getFacesContext(), model.getReportId(), model.getTemplateBean().getFkFunctionlist());
		return ready ? toEdit(model.getTemplateBean()) : toSubitem(model.getPreference().getBean());
	}

	/**
	 * approve the reordering of chosen functions
	 * 
	 * @return navigation destination
	 */
	public String doApproveReordering() {
		model.approveReordering(getFacesContext());
		return toEdit(model.getTemplateBean());
	}

	/**
	 * create a template
	 * 
	 * @return navigation destination
	 */
	public String doCreateTemplate() {
		model.addTemplate(getFacesContext());
		return toEdit(model.getTemplateBean());
	}

	/**
	 * copy a template
	 * 
	 * @return navigation destination
	 */
	public String doCopyTemplate() {
		model.copyTemplate(getFacesContext());
		return toEdit(model.getTemplateBean());
	}

	/**
	 * remove a template
	 * 
	 * @param bean
	 *          the bean
	 * 
	 * @return navigation destination
	 */
	public String doRemoveTemplate(FunctionselectionBean bean) {
		model.removeTemplate(getFacesContext(), bean);
		return toWelcome();
	}

	/**
	 * update a template
	 * 
	 * @return navigation destination
	 */
	public String doUpdateTemplate() {
		model.updateTemplateDefinition(getFacesContext());
		return toEdit(model.getTemplateBean());
	}

	/**
	 * add a function to the template
	 * 
	 * @param bean
	 *          the function
	 * @return navigation destination
	 */
	public String doAddFunctionToTemplate(FunctionBean bean) {
		Integer fkFunctionlist = model.getTemplate().getFkFunctionlist();
		Integer orderNr = 0;
		for (AnalysisFunctionBean afb : model.getTemplate().getChosen()) {
			if (orderNr < afb.getOrderNr()) {
				orderNr = afb.getOrderNr();
			}
		}
		orderNr += 10; // increase by 10 to allow easy resort
		model.addFunctionToTemplate(fkFunctionlist, bean.getFunctionId(), bean.getTitle(), orderNr, bean.getName(), getFacesContext());
		return toEdit(model.getTemplateBean());
	}

	/**
	 * remove a function from the template
	 * 
	 * @param anaparfun
	 *          the id of the function
	 * @return navigation destination
	 */
	public String doRemoveFunctionFromTemplate(Integer anaparfun) {
		model.removeFunctionFromTemplate(anaparfun, getFacesContext());
		return toEdit(model.getTemplateBean());
	}

	/**
	 * filter function to check if the value fits
	 * 
	 * @param value the value
	 * @param filter the filter
	 * @param locale the locale
	 * @return true or false
	 */
	public boolean filterByName(String value, String filter, Locale locale) {
		if (value == null) {
			return filter == null;
		} else {
			return filter == null ? true : value.toLowerCase().contains(filter.toLowerCase());
		}
	}
	
  /**
   * @return true if the cancel button was hit
   */
  public boolean getCancelling() {
    return FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap()
        .containsKey("form:btn_cancel");
  }
	
	/**
	 * @return a list of all category names
	 */
	public List<String> getCategoryNames(){
	  return model.getCategoryNames(getFacesContext());
	}
		
	/**
	 * @return the analysisFunctionBeansFilter
	 */
	public List<AnalysisFunctionBean> getAnalysisFunctionBeansFilter() {
		return analysisFunctionBeansFilter;
	}

	/**
	 * @param analysisFunctionBeansFilter the analysisFunctionBeansFilter to set
	 */
	public void setAnalysisFunctionBeansFilter(List<AnalysisFunctionBean> analysisFunctionBeansFilter) {
		this.analysisFunctionBeansFilter = analysisFunctionBeansFilter;
	}

	@Override
	public boolean getIsCurrentController() {
		return getIsCurrentController(getFacesContext());
	}
}
