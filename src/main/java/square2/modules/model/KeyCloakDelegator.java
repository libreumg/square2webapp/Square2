package square2.modules.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.NotAuthorizedException;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.ProcessingException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.token.TokenManager;
import org.keycloak.representations.AccessTokenResponse;
import org.keycloak.representations.idm.ClientMappingsRepresentation;
import org.keycloak.representations.idm.MappingsRepresentation;
import org.keycloak.representations.idm.RoleRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

/**
 * 
 * @author henkej
 *
 */
public class KeyCloakDelegator {
	private static final Logger LOGGER = LogManager.getLogger(KeyCloakDelegator.class);

	private final Keycloak instance;
	private final String username;
	private final String realm;
	private final String clientId;

	/**
	 * constructor to be used for logout only; username is set to null
	 * 
	 * @param url the urlto keycloak
	 * @param realm the realm
	 * @param clientId the id of the client
	 * @param token the token
	 */
	public KeyCloakDelegator(String url, String realm, String clientId, String token) {
		instance = Keycloak.getInstance(url, realm, clientId, token);
		this.username = null; // doesn't matter here, we use this for the logout only
		this.realm = realm;
		this.clientId = clientId;
	}

	/**
	 * create a new key cloak delegator
	 * 
	 * @param username the username
	 * @param password the password
	 * @param url the url to keycloak
	 * @param realm the realm
	 * @param clientId the id of the client
	 */
	public KeyCloakDelegator(String username, String password, String url, String realm, String clientId) {
		instance = Keycloak.getInstance(url, realm, username, password, clientId);
		this.username = username;
		this.realm = realm;
		this.clientId = clientId;
	}

	/**
	 * create a new key cloak delegator
	 * 
	 * @param username the username
	 * @param password the password
	 * @param url the url to keycloak
	 * @param realm the realm
	 * @param clientId the id of the client
	 * @param secret the secret
	 */
	public KeyCloakDelegator(String username, String password, String url, String realm, String clientId, String secret) {
		instance = Keycloak.getInstance(url, realm, username, password, clientId, secret);
		this.username = username;
		this.realm = realm;
		this.clientId = clientId;
	}

	/**
	 * @return the instance
	 */
	public Keycloak getInstance() {
		return this.instance;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return this.username;
	}

	private UserRepresentation getUser() {
		// TODO: on misconfigured keycloak, this can lead to  javax.ws.rs.ForbiddenException: HTTP 403 Forbidden] with root cause
		List<UserRepresentation> list = getInstance().realm(realm).users().search(username);
		return list.size() > 0 ? list.get(0) : null;
	}

	/**
	 * @return the id of the user
	 */
	public String getUserId() {
		return getUser().getId();
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return getUser().getFirstName();
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return getUser().getLastName();
	}

	/**
	 * @return the usnr
	 */
	public Integer getUsnr() {
		Map<String, List<String>> attributes = getUser().getAttributes();
		if (attributes == null || attributes.get("usnr") == null) {
			LOGGER.error("missing attribute usnr for keycloak user {}", getUser().getUsername());
			throw new ProcessingException("no usnr");
		}
		return Integer.valueOf(attributes.get("usnr").get(0));
	}

	/**
	 * @return the access token
	 */
	public AccessTokenResponse getToken() {
		TokenManager tokenManager = getInstance().tokenManager();
		return tokenManager.getAccessToken();
	}

	/**
	 * try to login with username and password; if successful, return the access
	 * token if login was successful
	 * 
	 * @return access token
	 * @throws NotFoundException if the keycloak server is not available
	 * @throws NotAuthorizedException if the login is invalid
	 */
	public String login() throws NotFoundException, NotAuthorizedException {
		TokenManager tokenmanager = getInstance().tokenManager();
		String token = tokenmanager.getAccessTokenString();
		LOGGER.debug("got token : {}", token);
		return token;
	}

	/**
	 * logout from keycloak
	 */
	public void logout() {
		getInstance().tokenManager().invalidate(getInstance().tokenManager().getAccessToken().getToken());
	}

	/**
	 * @return the roles for this user's session
	 */
	public List<String> getRoles() {
		MappingsRepresentation mr = getInstance().realm(realm).users().get(getUserId()).roles().getAll();
		List<String> roles = new ArrayList<>();
		for (Entry<String, ClientMappingsRepresentation> roleMapEntry : mr.getClientMappings().entrySet()) {
			if (roleMapEntry.getKey().equals(clientId)) {
				for (RoleRepresentation r : roleMapEntry.getValue().getMappings()) {
					roles.add(r.getName());
				}
			}
		}
		return roles;
	}
}
