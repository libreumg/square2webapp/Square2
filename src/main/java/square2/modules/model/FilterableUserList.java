package square2.modules.model;

import java.util.ArrayList;
import java.util.List;

import ship.jsf.components.selectFilterableMenu.BeanWrapperInterface;
import ship.jsf.components.selectFilterableMenu.FilterableListInterface;

/**
 * 
 * @author henkej
 *
 */
public class FilterableUserList extends ArrayList<BeanWrapperInterface> implements FilterableListInterface {
	private static final long serialVersionUID = 1L;

	/**
	 * create new filterable users list
	 * 
	 * @param list
	 *          the list of users
	 */
	public FilterableUserList(List<ProfileBean> list) {
		super(convertToWrapper(list));
	}

	private static List<BeanWrapperInterface> convertToWrapper(List<ProfileBean> list) {
		List<BeanWrapperInterface> l = new ArrayList<>();
		for (ProfileBean bean : list) {
			l.add(new ProfileFilterWrapper(bean));
		}
		return l;
	}
}
