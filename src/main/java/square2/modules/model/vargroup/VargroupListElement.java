package square2.modules.model.vargroup;

import org.jooq.JSONB;

import square2.modules.model.study.ListElement;

/**
 * 
 * @author henkej
 *
 */
public class VargroupListElement extends ListElement {
	private static final long serialVersionUID = 1L;

	private final Integer amountNonVariables;
	private final Integer amountVariables;
	private final Integer amountChosenVariables;

	/**
	 * create new variable group list element
	 * 
	 * @param fkElement
	 *          id of the element
	 * @param translation
	 *          the translation
	 * @param name
	 *          the name
	 * @param chosen
	 *          the chosen flag
	 * @param dirty
	 *          the dirty flag
	 * @param amountNonVariables
	 *          the number of elements that are not a variable
	 * @param amountVariables
	 *          the number of elements that are variables
	 * @param amountChosenVariables
	 *          the number of chosen variables
	 */
	public VargroupListElement(Integer fkElement, JSONB translation, String name, Boolean chosen, Boolean dirty,
			Integer amountNonVariables, Integer amountVariables, Integer amountChosenVariables) {
		super(fkElement, translation, name, chosen, dirty);
		this.amountNonVariables = amountNonVariables;
		this.amountVariables = amountVariables;
		this.amountChosenVariables = amountChosenVariables;
	}

	/**
	 * @return the number of non variables
	 */
	public Integer getAmountNonVariables() {
		return amountNonVariables;
	}

	/**
	 * @return the number of variables
	 */
	public Integer getAmountVariables() {
		return amountVariables;
	}

	/**
	 * @return the number of chosen variables
	 */
	public Integer getAmountChosenVariables() {
		return amountChosenVariables;
	}

}
