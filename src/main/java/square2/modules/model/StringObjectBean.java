package square2.modules.model;

/**
 * 
 * @author henkej
 *
 */
public class StringObjectBean {
	private String key;
	private Object value;

	/**
	 * generate new string object bean
	 * 
	 * @param key
	 *          the key
	 * @param value
	 *          the value
	 */
	public StringObjectBean(String key, Object value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *          the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param value
	 *          the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}
}
