package square2.modules.model.study;

import java.io.Serializable;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import square2.converter.JsonConverter;

/**
 * 
 * @author henkej
 *
 */
public class VariableAttributeRoleBean implements Serializable, Comparable<VariableAttributeRoleBean>, JsonConverter<VariableAttributeRoleBean> {
  private static final long serialVersionUID = 1L;

  private final Integer pk;
  private String name;
  private String inputwidget;
  private String regexpattern;
  private Boolean hasPrimaryAttribute;
  private Boolean hasLocale;

  /**
   * @param pk the id
   * @param name the name
   * @param inputwidget the input widget
   * @param regexpattern the regex pattern
   * @param hasPrimaryAttribute the has primary attribute flag
   * @param hasLocale the has locale flag
   */
  public VariableAttributeRoleBean(Integer pk, String name, String inputwidget, String regexpattern, Boolean hasPrimaryAttribute, Boolean hasLocale) {
    super();
    this.pk = pk;
    this.name = name;
    this.inputwidget = inputwidget;
    this.regexpattern = regexpattern;
    this.hasPrimaryAttribute = hasPrimaryAttribute;
    this.hasLocale = hasLocale;
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder();
    buf.append("VariableAttributeRoleBean@{pk=").append(pk);
    buf.append(", name=").append(name);
    buf.append(", inputwidget=").append(inputwidget);
    buf.append(", regexpattern=").append(regexpattern);
    buf.append(", hasPrimaryAttribute=").append(hasPrimaryAttribute);
    buf.append(", hasLocale=").append(hasLocale);
    buf.append("}");
    return buf.toString();
  }
  
  @Override
  public VariableAttributeRoleBean fromJson(String json) throws JsonSyntaxException {
    return new Gson().fromJson(json, VariableAttributeRoleBean.class);
  }

  @Override
  public String toJson() {
    return new Gson().toJson(this);
  }
  
  @Override
  public int compareTo(VariableAttributeRoleBean bean) {
    return name == null || bean == null ? 0 : name.compareTo(bean.getName());
  }

  /**
   * @return the hasPrimaryAttribute
   */
  public Boolean getHasPrimaryAttribute() {
    return hasPrimaryAttribute;
  }

  /**
   * @param hasPrimaryAttribute the hasPrimaryAttribute to set
   */
  public void setHasPrimaryAttribute(Boolean hasPrimaryAttribute) {
    this.hasPrimaryAttribute = hasPrimaryAttribute;
  }

  /**
   * @return the hasLocale
   */
  public Boolean getHasLocale() {
    return hasLocale;
  }

  /**
   * @param hasLocale the hasLocale to set
   */
  public void setHasLocale(Boolean hasLocale) {
    this.hasLocale = hasLocale;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the inputwidget
   */
  public String getInputwidget() {
    return inputwidget;
  }

  /**
   * @param inputwidget the inputwidget to set
   */
  public void setInputwidget(String inputwidget) {
    this.inputwidget = inputwidget;
  }

  /**
   * @return the regexpattern
   */
  public String getRegexpattern() {
    return regexpattern;
  }

  /**
   * @param regexpattern the regexpattern to set
   */
  public void setRegexpattern(String regexpattern) {
    this.regexpattern = regexpattern;
  }

  /**
   * @return the pk
   */
  public Integer getPk() {
    return pk;
  }
}
