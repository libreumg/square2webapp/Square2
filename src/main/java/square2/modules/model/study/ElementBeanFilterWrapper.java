package square2.modules.model.study;

import ship.jsf.components.selectFilterableMenu.BeanWrapperInterface;

/**
 * 
 * @author henkej
 *
 */
public class ElementBeanFilterWrapper implements BeanWrapperInterface {
	private ElementBean bean;

	/**
	 * create new element bean filter wrapper
	 * 
	 * @param bean
	 *          the bean
	 */
	public ElementBeanFilterWrapper(ElementBean bean) {
		this.bean = bean;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("{key=").append(getKey());
		buf.append(",value=").append(getValue());
		buf.append("}");
		return buf.toString();
	}

	@Override
	public Integer getKey() {
		return bean.getId();
	}

	@Override
	public String getValue() {
		return bean.getName();
	}
}
