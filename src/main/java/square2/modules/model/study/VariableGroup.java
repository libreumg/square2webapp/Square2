package square2.modules.model.study;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class VariableGroup {
	private List<VariableBean> variables;

	/**
	 * create new empty variable group
	 */
	public VariableGroup() {
		this.variables = new ArrayList<>();
	}

	/**
	 * @return the variables
	 */
	public List<VariableBean> getVariables() {
		return variables;
	}
}
