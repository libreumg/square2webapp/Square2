package square2.modules.model.study;

import java.io.Serializable;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class MissinglistBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Integer id;
	private final String name;
	private final String description;
	private final List<MissingBean> list;

	/**
	 * @param id the id of the list; null for a new list
	 * @param name the name
	 * @param description the description
	 * @param list the list
	 */
	public MissinglistBean(Integer id, String name, String description, List<MissingBean> list) {
	  this.id = id;
		this.name = name;
		this.description = description;
		this.list = list;
	}
	
	/**
	 * get a summary of the missings - simply list them
	 * 
	 * @return a list of missings; an empty string at least
	 */
	public String getSummary() {
	  StringBuilder buf = new StringBuilder();
	  boolean first = true;
	  for (MissingBean bean : list) {
	    buf.append(first ? "" : ", ").append(bean.getNameInStudy());
	    first = false;
	  }
	  String s = buf.toString();
	  return s == null ? s : (s.length() > 32 ? s.substring(0, 32).concat("...") : s); 
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the list
	 */
	public List<MissingBean> getList() {
		return list;
	}

  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }
}
