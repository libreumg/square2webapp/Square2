package square2.modules.model.study;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.JSONB;
import org.jooq.exception.DataAccessException;

import de.ship.dbppsquare.square.enums.EnumControlvar;
import de.ship.dbppsquare.square.enums.EnumMetadatatype;
import ship.jsf.components.selectFilterableMenu.FilterableListInterface;
import square2.db.control.GatewayException;
import square2.db.control.MissinglistGateway;
import square2.db.control.StudyGateway;
import square2.db.control.converter.EnumConverter;
import square2.db.control.model.LambdaResultWrapper;
import square2.help.ContextKey;
import square2.help.PrimefacesHelper;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.GroupPrivilegeBean;
import square2.modules.model.ModelException;
import square2.modules.model.SquareModel;
import square2.modules.model.admin.UsergroupBean;
import square2.modules.model.cohort.StudygroupBean;
import square2.modules.model.datamanagement.ReleaseBean;
import square2.modules.model.study.io.OpalBean;
import square2.modules.model.study.io.ShipBean;
import square2.r.control.OpalGateway;
import square2.r.control.RMessage;
import square2.r.control.ShipGateway;
import square2.r.control.SmiGateway;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class StudyModel extends SquareModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(StudyModel.class);

	private Integer activeIndex;
	private Integer activeIndexMeta;

	private List<ElementBean> elements;
	private List<ElementBean> parents;
	private List<ElementBean> children;
	private String chosenElementName;
	private String missinglistName;
	private String missinglistDescription;
	private MissingBean missingBean;
	private List<MissingBean> missinglist;
	private ElementBean element;
	private Integer rightId;
	private VariableBean variable;
	private VariablegrouplistBean variablegrouplist;
	private VariablegroupBean variablegroupBean;
	private ListElement newListVariable;
	private List<ListElement> inverseList;
	private List<MetadatatypeBean> metadatatypes;
	private List<MetadatatypeBean> unusedMetadatatypes;
	private ConfounderBean newMetadataBean;
	private List<ReleaseBean> releases;
	private Integer newTopElement;
	private List<ListElement> topElements;
	private List<ElementBean> referenceVariables;
	private List<ElementBean> allVariables;
	private UploadBean uploadBean;
	private UploadBean uploadMetaref;
	private List<ElementBean> executeables;
	private List<VariableBean> neighbours;
	private List<DeptmetaBean> deptmeta;
	private DeptmetaBean deptmetaBean;
	private Integer variablelistParentId;
	private Boolean orderByShip;
	private AclBean studyAcl;

	private List<GroupPrivilegeBean> groupPrivileges;
	private List<UsergroupBean> allGroups;
	private Integer newGroup;
	private Integer newUsnr;
	private AclBean newAcl;
	private GroupPrivilegeBean activePrivilege;
	private Integer fkStudygroup;
	private OpalBean opalBean;
	private ShipBean shipBean;

	private Boolean includeSections = false; // by default, do not include sections

	private Integer deptmetaAffected;

	private List<VariableusageBean> variableusages;

	private List<String> smitypes;

	private List<VariableAttributeRoleBean> variableattributeroles;

	private Integer chosenId;

	private List<String> missingtypes;

	private List<MissinglistBean> missinglists;

	private List<IdLabelBean> possibleTimeVars;
	private StudyvarBean studyvarBean;

	/**
	 * create a new study model
	 */
	public StudyModel() {
		super();
		this.orderByShip = false;
	}

	/**
	 * reset welcome site
	 * 
	 * @param facesContext context of this function call
	 * @return true for success, false otherwise
	 */
	public boolean resetWelcome(SquareFacesContext facesContext) {
		activeIndex = 0;
		activeIndexMeta = 0;
		element = null;
		try (SmiGateway gw = new SmiGateway(facesContext)) {
			smitypes = smitypes == null || smitypes.isEmpty() ? gw.getSmitypes() : smitypes;
		} catch (Exception e) {
			facesContext.notifyWarning("warn.could.not.run.R", "squaremetadatainterface could not be loaded");
			LOGGER.error(e.getMessage());
		}
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			elements = gw.getAllStudyElementsOf(facesContext.getUsnr());
			variableusages = gw.getAllVariableusages();
			return true;
		} catch (DataAccessException e) {
			elements = new ArrayList<>();
			variableusages = new ArrayList<>();
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * navigate to the edit page
	 * 
	 * @param facesContext the faces context
	 * @param bean the bean of the element (should contain the id at least)
	 * @return true or false
	 */
	public boolean toEdit(SquareFacesContext facesContext, ElementBean bean) {
		try {
			Integer fkElement = bean.getId();
			StudyGateway gw = new StudyGateway(facesContext);
			this.variable = gw.getVariable(fkElement);
			this.element = new ElementBean(variable.getUniqueName(), fkElement, variable.getRightId(),
					variable.getKeyParentElement(), variable.getTranslation(), variable.getLabel(), variable.getVarOrder(),
					variable.getShipOrder(), variable.getStudy(), variable.getStudygroup(), variable.getFkMissinglist(),
					variable.getLocales());
			this.element.setName(variable.getName());
			this.element.setIsVariable(true);
			this.element.getAccessLevel().setAcl(variable.getAcl());
			this.parents = gw.getAllParentElements(element.getId());
			this.variableattributeroles = gw.getAllVariableAttributeRoles();
			// for the dropdowns, set the variableattributeroles from
			// this.variableattributeroles
			this.variable.resetVariableattributeroles(variableattributeroles);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * navigate to the items page
	 * 
	 * @param id the id of the element
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean toItem(Integer id, SquareFacesContext facesContext) {
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			this.variable = gw.getVariable(id);
			String label = facesContext.selectTranslation(variable.getTranslation());
			this.element = new ElementBean(variable.getUniqueName(), id, variable.getRightId(),
					variable.getKeyParentElement(), variable.getTranslation(), label, variable.getVarOrder(),
					variable.getShipOrder(), variable.getStudy(), variable.getStudygroup(), variable.getFkMissinglist(),
					variable.getLocales());
			this.element.setName(variable.getName()); // for panel titles
			this.element.setIsVariable(true);
			this.element.getAccessLevel().setAcl(variable.getAcl());
			this.element.setMissinglistReadable(gw.getMissinglistReadable(variable.getFkMissinglist()));
			this.metadatatypes = gw.getMetadatatypes();
			this.unusedMetadatatypes = getAllUnusedMetadatatypes(variable.getMetadatalist());
			this.newMetadataBean = new ConfounderBean();
			this.allVariables = gw.getAllVariables(true);
			this.referenceVariables = gw.getAllNeighbourVariables(id);
			this.parents = gw.getAllParentElements(element.getId());
			this.variablelistParentId = element.getId();
			loadPrivilegePageLists(variable.getRightId(), gw, facesContext);
			return true;
		} catch (DataAccessException | ModelException | ClassNotFoundException | SQLException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * navigate to add variable
	 * 
	 * @param facesContext the context of this function call
	 */
	public void toAddVariable(SquareFacesContext facesContext) {
		StudyGateway gw = new StudyGateway(facesContext);
		resetVariable(element.getId(), JSONB.valueOf("[]"));
		loadReferenceVariablesByParent(gw, element.getId(), facesContext);
		loadNeighbourVariables(gw, element.getId(), facesContext);
		metadatatypes = gw.getMetadatatypes();
		missinglists = new MissinglistGateway(facesContext).getAllMissinglists(facesContext.getUsnr(),
				element.getStudygroup());
	}

	/**
	 * navigate to add department
	 * 
	 * @param facesContext the context of this function call
	 */
	public void toDepartment(SquareFacesContext facesContext) {
		resetVariable(element.getId(), element.getLocales());
		loadReferenceVariablesByParent(new StudyGateway(facesContext), element.getId(), facesContext);
		missinglists = new MissinglistGateway(facesContext).getAllMissinglists(facesContext.getUsnr(),
				element.getStudygroup());
	}

	/**
	 * load the privilege page list
	 * 
	 * @param fkPrivilege the privilege id
	 * @param gw the gateway
	 * @param facesContext the context of this function call
	 * @throws DataAccessException if a database error occurs
	 * @throws ModelException if a model exception occurs
	 */
	public void loadPrivilegePageLists(Integer fkPrivilege, StudyGateway gw, SquareFacesContext facesContext)
			throws DataAccessException, ModelException {
		newGroup = Integer.valueOf(0);
		setAllSquareUsers(facesContext, fkPrivilege);
		newAcl = new AclBean().setString("rwx");
		groupPrivileges = gw.getAllGroupPrivileges(fkPrivilege);
		allGroups = gw.getAllGroupsExceptOf(groupPrivileges);
	}

	@Override
	public void setAllSquareUsers(SquareFacesContext facesContext, Object... params)
			throws ModelException, DataAccessException {
		if (params == null || params.length < 1 || !(params[0] instanceof Integer)) {
			throw new ModelException("at least one param is needed that is an integer of the right_id");
		}
		Integer privilegeId = (Integer) params[0];
		super.resetAllSquareusers(new StudyGateway(facesContext).getAllSquareUsersWithoutUserPrivilege(privilegeId));
	}

	/**
	 * load the reference variables
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean loadReferenceVariables(SquareFacesContext facesContext) {
		return loadReferenceVariables(new StudyGateway(facesContext), variable.getKeyElement(), facesContext);
	}

	private boolean loadReferenceVariables(StudyGateway gw, Integer id, SquareFacesContext facesContext) {
		try {
			allVariables = gw.getAllVariables(false);
			referenceVariables = gw.getAllNeighbourVariables(id);
			parents = gw.getAllParentElements(element.getId());
			variablelistParentId = element.getId();
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			allVariables = new ArrayList<>();
			referenceVariables = new ArrayList<>();
			return false;
		}
	}

	/**
	 * load the reference variables of the parent
	 * 
	 * @param gw the gateway
	 * @param id the id of the parent
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean loadReferenceVariablesByParent(StudyGateway gw, Integer id, SquareFacesContext facesContext) {
		try {
			allVariables = gw.getAllVariables(false);
			referenceVariables = gw.getAllReferenceVariablesByParent(id);
			parents = gw.getAllParentElements(element.getId());
			variablelistParentId = id;
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			allVariables = new ArrayList<>();
			referenceVariables = new ArrayList<>();
			return false;
		}
	}

	/**
	 * @return the reference variables
	 */
	public List<ElementBean> getAllReferenceVariables() {
		return referenceVariables;
	}

	/**
	 * @return all variables
	 */
	public List<ElementBean> getAllVariables() {
		return allVariables;
	}

	/**
	 * reset element site
	 * 
	 * @param elementId id of parent element
	 * @param usnr user restrictions
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean resetItem(Integer elementId, Integer usnr, SquareFacesContext facesContext) {
		activeIndex = 0;
		activeIndexMeta = 0;
		for (ElementBean bean : elements) {
			if (bean.getId().equals(elementId)) {
				element = bean;
			}
		}
		// if element is not found in elements stack, seek in parents to find it
		if (elementId == null) {
		} else if (element == null) {
		} else if (!elementId.equals(element.getId())) {
			for (ElementBean bean : parents) {
				if (bean.getId().equals(elementId)) {
					element = bean;
				}
			}
		}
		if (element == null) {
			facesContext.notifyWarning("element with id = {0} not found", " " + elementId);
			element = new ElementBean(null, elementId, null, null, null, null, null, null, null, null, null);
			element.setName("error on element ".concat(elementId == null ? "null" : elementId.toString()));
		}
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			studyAcl = gw.getStudyAcl(elementId, usnr);
			elements = gw.getAllElementsOfParent(elementId);
			parents = gw.getAllParentElements(elementId);
			groupPrivileges = new StudyGateway(facesContext).getAllGroupPrivileges(element.getRightId());
			elements.sort((o1, o2) -> o1 == null ? 0 : o1.compareByNameTo(o2));
			return true;
		} catch (DataAccessException e) {
			elements = new ArrayList<>();
			parents = new ArrayList<>();
			element = null;
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * reset the upload bean
	 * 
	 * @param facesContext the context of this call
	 */
	public void resetUploadBean(SquareFacesContext facesContext) {
		uploadBean = new UploadBean();
		uploadMetaref = new UploadBean();
		shipBean = new ShipBean(); // TODO: add a list of possible roots?
		try (OpalGateway gw = new OpalGateway(facesContext)) {
			Object opalConfigFile = facesContext.getApplicationMapValue(ContextKey.OR_CONFIG_PATH);
			opalBean = new OpalBean(gw.getOpalUrls((String) opalConfigFile));
		} catch (Exception e) {
			facesContext.notifyException(e);
			opalBean = new OpalBean(new ArrayList<>());
		}
	}

	/**
	 * get all metadata types that are not in list
	 * 
	 * @param list the list of metadata beans
	 * @return the list of unused metadatatypes
	 */
	private List<MetadatatypeBean> getAllUnusedMetadatatypes(List<ConfounderBean> list) {
		Set<MetadatatypeBean> found = new HashSet<>();
		for (ConfounderBean bean : list) {
			found.add(bean.getMetadataType());
		}
		List<MetadatatypeBean> result = metadatatypes;
		result.removeAll(found);
		return result;
	}

	/**
	 * reset the opalBean projects to the list of found ones
	 * 
	 * @param facesContext the faces context
	 */
	public void resetOpalProjects(SquareFacesContext facesContext) {
		opalBean.getProjects().clear();
		Object opalConfigFile = facesContext.getApplicationMapValue(ContextKey.OR_CONFIG_PATH);
		try (OpalGateway gw = new OpalGateway(facesContext)) {
			List<String> projects = gw.getOpalProjects(opalConfigFile, opalBean.getUrl());
			opalBean.getProjects().addAll(projects);
		} catch (Exception e) {
			facesContext.notifyException(e);
			// TODO: ajax error response to the user
		}
	}

	/**
	 * reset the opalBean tables to the list of found ones
	 * 
	 * @param facesContext the faces context
	 */
	public void resetOpalTables(SquareFacesContext facesContext) {
		opalBean.getTables().clear();
		Object opalConfigFile = facesContext.getApplicationMapValue(ContextKey.OR_CONFIG_PATH);
		try (OpalGateway gw = new OpalGateway(facesContext)) {
			List<String> tables = gw.getOpalTables(opalConfigFile, opalBean.getUrl(), opalBean.getProject());
			opalBean.getTables().addAll(tables);
		} catch (Exception e) {
			facesContext.notifyException(e);
			// TODO: ajax error response to the user
		}
	}

	/**
	 * reload a department
	 * 
	 * @param keyElement the id of the element
	 * @param facesContext the context of this function call
	 * @return true or false
	 * @throws ModelException if a model exception occurs
	 */
	public boolean reloadDepartment(Integer keyElement, SquareFacesContext facesContext) throws ModelException {
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			this.element = gw.getDepartment(keyElement);
			loadPrivilegePageLists(element.getRightId(), gw, facesContext);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * reset a variable
	 * 
	 * @param keyParentElement the id of the parent element
	 * @param locales the locales
	 * @return true
	 */
	public boolean resetVariable(Integer keyParentElement, JSONB locales) {
		this.variable = new VariableBean(null, null, null, null, null, null, keyParentElement, null, null, null, null, null,
				null, locales);
		this.newMetadataBean = new ConfounderBean();
		return true;
	}

	/**
	 * @return the element bean
	 */
	public ElementBean getElement() {
		return element;
	}

	/**
	 * @return the element beans
	 */
	public List<ElementBean> getElements() {
		return elements;
	}

	/**
	 * @return the privilege id
	 */
	public Integer getRightId() {
		return rightId;
	}

	/**
	 * @return the variable bean
	 */
	public VariableBean getVariable() {
		return variable;
	}

	/**
	 * @return the parent beans
	 */
	public List<ElementBean> getParents() {
		return parents;
	}

	/**
	 * get the parents of key
	 * 
	 * @param key the id of the element
	 * @return the list of parents, ordered by its hierarchy
	 */
	public List<ElementBean> getParents(Integer key) {
		Map<Integer, ElementBean> map = new HashMap<>();
		for (ElementBean bean : executeables) {
			map.put(bean.getId(), bean);
		}
		List<ElementBean> beans = new ArrayList<>(map.values());
		List<ElementBean> list = new ArrayList<>();
		Map<Integer, ElementBean> cache = new LinkedHashMap<>();
		beans.sort((o1, o2) -> o1 == null ? 0 : o1.compareByIdTo(o2));
		if (key != null) {
			for (ElementBean bean : beans) {
				if (key.equals(bean.getId()) || cache.containsKey(bean.getId())) {
					cache.put(bean.getParent(), bean);
				}
			}
		}
		for (Map.Entry<Integer, ElementBean> entry : cache.entrySet()) {
			list.add(entry.getValue());
		}
		Collections.reverse(list);
		return list;
	}

	/**
	 * @return the variable group list
	 */
	public VariablegrouplistBean getVariablegrouplist() {
		return variablegrouplist;
	}

	/**
	 * @return the variable group bean
	 */
	public VariablegroupBean getVariablegroupBean() {
		return variablegroupBean;
	}

	/**
	 * @return he new list variable
	 */
	public ListElement getNewListVariable() {
		return newListVariable;
	}

	/**
	 * @return the inverse list
	 */
	public FilterableListInterface getInverseList() {
		return new FilterableListVariableList(inverseList);
	}

	/**
	 * @return the meta data types
	 */
	public List<MetadatatypeBean> getMetadatatypes() {
		return metadatatypes;
	}

	/**
	 * @return the release beans
	 */
	public List<ReleaseBean> getReleases() {
		return releases;
	}

	/**
	 * @return the new top element
	 */
	public Integer getNewTopElement() {
		return newTopElement;
	}

	/**
	 * @param newTopElement the new top element
	 */
	public void setNewTopElement(Integer newTopElement) {
		this.newTopElement = newTopElement;
	}

	/**
	 * @return the list of top elements
	 */
	public List<ListElement> getTopElements() {
		return topElements;
	}

	/**
	 * get all elements of current user
	 * 
	 * @param facesContext the context of this function call
	 */
	public void getAllElementsOf(SquareFacesContext facesContext) {
		try {
			executeables = new StudyGateway(facesContext).getAllElementsOf(facesContext.getUsnr());
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	private boolean loadNeighbourVariables(StudyGateway gw, Integer id, SquareFacesContext facesContext) {
		try {
			this.neighbours = gw.getAllVariablesOf(id);
			return true;
		} catch (DataAccessException e) {
			this.neighbours = new ArrayList<>();
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * @return the upload bean
	 */
	public UploadBean getUploadBean() {
		return uploadBean;
	}

	/**
	 * @return the upload metaref bean
	 */
	public UploadBean getUploadMetaref() {
		return uploadMetaref;
	}

	/**
	 * delete metadata
	 * 
	 * @param facesContext the context of this function call
	 * @param metadatatype the metadata type
	 * @throws DataAccessException if a database error occurs
	 */
	public void deleteMetadata(SquareFacesContext facesContext, String metadatatype) throws DataAccessException {
		new StudyGateway(facesContext).deleteMetadata(metadatatype, variable.getKeyElement());
	}

	/**
	 * add metadata
	 */
	public void addMetadata() {
		variable.getMetadatalist().add(newMetadataBean.clone());
		newMetadataBean = new ConfounderBean();
	}

	/**
	 * remove metadata
	 * 
	 * @param referenceVariable the reference variable
	 * @param facesContext the context of this function call
	 */
	public void removeMetadata(Integer referenceVariable, SquareFacesContext facesContext) {
		if (referenceVariable == null) {
			facesContext.notifyError("id of reference variable is null, aborting...");
		} else {
			Iterator<ConfounderBean> iterator = variable.getMetadatalist().iterator();
			while (iterator.hasNext()) {
				ConfounderBean bean = iterator.next();
				if (referenceVariable.equals(bean.getFkReferenceElement())) {
					iterator.remove();
				}
			}
		}
	}

	/**
	 * remove metadata
	 * 
	 * @param referenceVariable the reference variable
	 * @param type the type
	 * @param parent the id of the parent element
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean removeMetadata(Integer referenceVariable, String type, Integer parent,
			SquareFacesContext facesContext) {
		EnumMetadatatype enumType = new EnumConverter().getEnumMetadatatype(type);
		boolean persisted = true;
		Iterator<DeptmetaBean> i = deptmeta.iterator();
		while (i.hasNext()) {
			DeptmetaBean bean = i.next();
			if (bean.getPk().equals(referenceVariable)) {
				persisted = bean.getPersisted();
			}
		}
		if (referenceVariable == null) {
			facesContext.notifyError("id of reference variable is null, aborting...");
			return false;
		} else if (parent == null) {
			facesContext.notifyError("parent is null, aborting...");
			return false;
		} else if (type == null) {
			facesContext.notifyError("type is null, aborting...");
			return false;
		} else if (!persisted) {
			// not yet persisted, so only remove it from the current list
			i.remove();
			return false; // do not reload from db
		} else {
			try {
				Integer affected = new StudyGateway(facesContext).deleteAllMetadata(referenceVariable, enumType, parent);
				facesContext.notifyInformation("{0} lines removed from the database", affected);
				return true;
			} catch (DataAccessException e) {
				facesContext.notifyException(e);
				LOGGER.error(e.getMessage(), e);
				return false;
			}
		}
	}

	/**
	 * add a variable
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean addVariable(SquareFacesContext facesContext) {
		if (variable.getName() == null || variable.getName().trim().isEmpty()) {
			facesContext.notifyError("name must not be empty");
			return false;
		}
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			Integer fkElement = gw.addVariable(variable, facesContext.getUsnr());
			variable = gw.getVariable(fkElement);
			reloadProfilePrivileges(facesContext);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * get the name of an element
	 * 
	 * @param id theid of the element
	 * @return the name of the element
	 */
	public String getNameOf(Integer id) {
		return getNameOf(id, true);
	}

	/**
	 * get the name of an element
	 * 
	 * @param id the id of the element
	 * @param showError the flag to determine if errors should be visible
	 * @return the mane if found, an empty string otherwise or an error message
	 */
	public String getNameOf(Integer id, boolean showError) {
		for (ElementBean bean : getAllVariables()) {
			if (bean.getId().equals(id)) {
				return bean.getName();
			}
		}
		if (!showError) {
			return "";
		}
		return id == null ? "! id in StudyModel.getNameOf is null !" : ("no name found for variable " + id);
	}

	/**
	 * update a variable
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean updateVariable(SquareFacesContext facesContext) {
		try {
			new StudyGateway(facesContext).updateVariable(variable);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * update an element
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean updateElement(SquareFacesContext facesContext) {
		try {
			new StudyGateway(facesContext).updateElement(element);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * add an element
	 * 
	 * @param facesContext the context of this function call
	 * @return the id of the new element
	 */
	public Integer addElement(SquareFacesContext facesContext) {
		Integer usnr = facesContext.getUsnr();
		LambdaResultWrapper langKey = new LambdaResultWrapper();
		Integer pk = null;
		try {
			pk = new StudyGateway(facesContext).addElement(variable.getName(), variable.getTranslation(), usnr,
					variable.getKeyParentElement(), langKey, variable.getFkMissinglist());
			resetItem(pk, usnr, facesContext);
			reloadProfilePrivileges(facesContext);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
		}
		return pk;
	}

	/**
	 * save metadata of variable referenced by bean
	 * 
	 * @param bean the variable bean
	 * @param facesContext the context of this function call
	 */
	public void saveMetadata(VariableBean bean, SquareFacesContext facesContext) {
		try {
			new StudyGateway(facesContext).saveMetadata(bean.getMetadatalist(), bean.getKeyElement());
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * summarize all metadata information of parent's children
	 * 
	 * @param parent element of parent
	 * @param variablelistParentId id of parent element for loading the variable
	 * list; if null, use parent's id
	 * @param facesContext the context of this function call
	 */
	public void summarizeMetadata(ElementBean parent, Integer variablelistParentId, SquareFacesContext facesContext) {
		try {
			element = parent; // use element as parent
			StudyGateway gw = new StudyGateway(facesContext);
			parents = gw.getAllParentElements(element.getId());
			deptmeta = gw.getSummarizedMetadata(parent.getId());
			newMetadataBean = new ConfounderBean();
			this.variablelistParentId = variablelistParentId == null ? parent.getId() : variablelistParentId;
			children = gw.getAllStudyVariablesUnder(parent.getId());
			referenceVariables = gw.getAllVariablesUnder(this.variablelistParentId, false);
			allVariables = gw.getAllVariables(false);
			metadatatypes = gw.getMetadatatypes();
			deptmetaAffected = children == null ? 0 : children.size();
		} catch (DataAccessException | GatewayException e) {
			deptmeta = new ArrayList<>();
			deptmetaAffected = 0;
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * prepare the page study/studyvars/list
	 * 
	 * @param studyId the id of the study wave
	 * @param studyName the name of the study
	 * @param facesContext the context of this function call
	 */
	public void prepareStudyVars(Integer studyId, String studyName, SquareFacesContext facesContext) {
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			studyvarBean = new StudyvarBean(studyId, studyName);
			possibleTimeVars = gw.getPossibleTimevars(studyId, facesContext.getIsocodeEnum());
			studyvarBean.setTimevar(gw.getCurrentTimeVarOfStudy(studyId));
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * update the time var for this study
	 * 
	 * @param facesContext the current context
	 * @return true for a successful update, false otherwise
	 */
	public Boolean doUpdateTimeVar(SquareFacesContext facesContext) {
		try {
			LOGGER.info("update timevar to {} for study {} ({})",  studyvarBean.getTimevar(), studyvarBean.getId(), studyvarBean.getName());
			Integer affected = new StudyGateway(facesContext).updateTimeVar(studyvarBean.getId(), studyvarBean.getTimevar());
			if (affected != 1) {
				facesContext.notifyWarning("expected database rows: 1, but affected: {0}", affected);
			}
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage());
			return false;
		}
	}

	/**
	 * count affected variables
	 * 
	 * @return the number of reference variables
	 */
	public Integer getAffected() {
		return referenceVariables == null ? 0 : referenceVariables.size();
	}

	/**
	 * @return the unique names of reference variables
	 */
	public List<String> getAffectedUniqueNames() {
		List<String> list = new ArrayList<>();
		for (ElementBean bean : referenceVariables) {
			list.add(bean.getUniqueName());
		}
		return list;
	}

	/**
	 * get number of variables in this department
	 * 
	 * @return number of variables in this department
	 */
	public Integer getDeptmetaAffected() {
		return deptmetaAffected;
	}

	/**
	 * set info panel content
	 * 
	 * @param facesContext the context of this function call
	 */
	public void setInfo(SquareFacesContext facesContext) {
		StudyGateway gw = new StudyGateway(facesContext);
		Integer id = this.element.getId();
		this.element.setNumberOfSections(gw.getNumberOfSections(id));
		this.element.setNumberOfVariables(gw.getNumberOfVariables(id));
		this.element.setPercentageOfCovered(gw.getPercentageOfCovered(id));
	}

	/**
	 * load all variables under parentId
	 * 
	 * @param parentId the id of the parent
	 * @param facesContext the context of this function call
	 */
	public void loadVariablesUnder(Integer parentId, SquareFacesContext facesContext) {
		try {
			this.variablelistParentId = parentId;
			StudyGateway gw = new StudyGateway(facesContext);
			referenceVariables = gw.getAllVariablesUnder(parentId, false);
			allVariables = gw.getAllVariables(true);
			parents = gw.getAllParentElements(element.getId());
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * save the file to the database and execute the smi run call
	 * 
	 * @param facesContext the faces context
	 * @param interpretation the interpretation
	 * @param bean the upload bean
	 * @return true or false
	 */
	public boolean smiRun(SquareFacesContext facesContext, String interpretation, UploadBean bean) {
		try (SmiGateway rgw = new SmiGateway(facesContext)) {
			StudyGateway gw = new StudyGateway(facesContext);
			Integer fkIostack = gw.insertIoStackFile(bean, interpretation);
			if (facesContext.getProfile().isAdmin()) {
				for (RMessage rmsg : rgw.runSmi(fkIostack)) {
					facesContext.notifyMessage(rmsg.getLoglevel(), rmsg.getMessage());
				}
			}
			gw.selectIoMessage(fkIostack);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * load complete dept meta
	 * 
	 * @param parent the parent element bean
	 * @param dmBean the dept meta bean
	 * @param facesContext the context of this function call
	 */
	public void loadCompleteDeptmeta(ElementBean parent, DeptmetaBean dmBean, SquareFacesContext facesContext) {
		this.deptmetaBean = dmBean;
		this.element = parent;
	}

	/**
	 * add meta data to all child variables if not yet set
	 * 
	 * @param element the element bean
	 * @param facesContext the context of this function call
	 */
	public void addDeptmeta(ElementBean element, SquareFacesContext facesContext) {
		try {
			List<ElementBean> selectedVars = new ArrayList<>();
			List<String> affectedUniqueNames = new ArrayList<>();
			List<Integer> affectedPks = new ArrayList<>();
			for (ElementBean child : children) {
				if (child.getIsSelected()) {
					selectedVars.add(child);
					affectedUniqueNames.add(child.getUniqueName());
					affectedPks.add(child.getId());
				}
			}
			newMetadataBean.getSelectedVars().addAll(selectedVars);
			String varName = "unique name of " + newMetadataBean.getFkReferenceElement() + " not found";
			for (ElementBean bean : getAllReferenceVariables()) {
				if (bean.getId().equals(newMetadataBean.getFkReferenceElement())) {
					varName = bean.getUniqueName();
				}
			}
			String affected = new StringBuilder().append(selectedVars.size()).append(" / ").append(deptmetaAffected)
					.toString();
			DeptmetaBean b = new DeptmetaBean(newMetadataBean.getFkReferenceElement(), varName,
					newMetadataBean.getMetadataType().getName(), affected, affectedUniqueNames,
					newMetadataBean.getMetadataType().getPk(), affectedPks, false);
			deptmeta.add(b);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * approve changes on the database for this deptmeta
	 * 
	 * @param facesContext the context of this function call
	 * @param eBean the element bean
	 * @return true or false
	 */
	public boolean appoveDeptmeta(SquareFacesContext facesContext, ElementBean eBean) {
		try {
			// TODO: iterate over deptmeta beans to be upserted; all that are not in the
			// list, must be removed; all other must be added / updated
			Integer affected = new StudyGateway(facesContext).upsertMetadata(deptmeta);
//			Integer affected = new StudyGateway(facesContext).addMetadataToAll(newMetadataBean);
			facesContext.notifyAffected(affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * delete an element
	 * 
	 * @param id the id of the element
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean deleteElement(Integer id, SquareFacesContext facesContext) {
		try {
			Integer amount = new StudyGateway(facesContext).deleteElement(id);
			facesContext.notifyInformation("info.affected.remove.studyelem", amount);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * delete a variable
	 * 
	 * @param id the id of the element
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean deleteVariable(Integer id, SquareFacesContext facesContext) {
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			List<VariablegroupBean> variablegroups = gw.getVariablegroupsOfVariable(id);
			if (variablegroups.size() > 0) {
				String usedBy = facesContext.translate("label.used.by");
				StringBuilder buf = new StringBuilder();
				boolean first = true;
				for (VariablegroupBean bean : variablegroups) {
					buf.append(first ? "" : ", ");
					first = false;
					buf.append(bean.getName());
					buf.append(" (").append(bean.getDescription()).append(" ");
					buf.append(usedBy).append(" ").append(bean.getUsers());
					buf.append(")");
				}
				facesContext.notifyError("error.delete.variable.isingroup", buf.toString());
				return false;
			}
			Integer amount = gw.deleteVariable(id);
			facesContext.notifyInformation("info.affected.remove.studyelem", amount);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * remove a privilege
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean removePrivilege(SquareFacesContext facesContext) {
		try {
			Set<Integer> privileges = new HashSet<>();
			privileges.add(activePrivilege.getFkPrivilege());
			StudyGateway gw = new StudyGateway(facesContext);
			privileges.addAll(gw.getAllChildrenPrivilegeIds(activePrivilege.getFkPrivilege()));
			Integer affected = gw.removeUserGroupPrivilege(activePrivilege.getFkUsnr(), activePrivilege.getFkGroup(),
					privileges);
			facesContext.notifyInformation("info.affected.remove", affected);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * add a privilege
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean addPrivilege(SquareFacesContext facesContext) {
		try {
			List<Integer> privileges = new ArrayList<>();
			StudyGateway gw = new StudyGateway(facesContext);
			privileges.add(element.getRightId());
			privileges.addAll(gw.getAllChildrenPrivilegeIds(element.getRightId()));
			Integer affected = gw.addUserGroupPrivilegeForStudy(newUsnr, newGroup, privileges, newAcl);
			facesContext.notifyInformation("info.affected.add", affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * update a privilege
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public Boolean updatePrivilege(SquareFacesContext facesContext) {
		try {
			List<Integer> privileges = new ArrayList<>();
			StudyGateway gw = new StudyGateway(facesContext);
			privileges.add(element.getRightId());
			privileges.addAll(gw.getAllChildrenPrivilegeIds(element.getRightId()));
			Integer affected = gw.updateUserGroupPrivileges(activePrivilege, privileges);
			facesContext.notifyInformation("info.affected.add", affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * download variable attributes
	 * 
	 * @param facesContext the context of this function call
	 * @param parentId the id of the parent
	 * @return true or false
	 */
	public boolean downloadVariableAttributes(SquareFacesContext facesContext, Integer parentId) {
		try (SmiGateway gw = new SmiGateway(facesContext)) {
			byte[] theOdsFile = gw.getVariableAttributes(parentId, includeSections);
			return generateDownloadStreamFromByteArray(facesContext, "variable_attributes.xlsx",
					"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", theOdsFile);
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * download metaref
	 * 
	 * @param facesContext the context of this function call
	 * @param parentId the id of the parent
	 * @return true or false
	 */
	public boolean downloadMetaref(SquareFacesContext facesContext, Integer parentId) {
		try (SmiGateway gw = new SmiGateway(facesContext)) {
			byte[] theOdsFile = gw.getProcessVariables(parentId);
			return generateDownloadStreamFromByteArray(facesContext, "process_variables.ods",
					"application/vnd.oasis.opendocument.spreadsheet", theOdsFile);
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * get the missing list
	 * 
	 * @param facesContext the faces context
	 */
	public void toMissinglist(SquareFacesContext facesContext) {
		missinglists = new MissinglistGateway(facesContext).getAllMissinglists(facesContext.getUsnr(), null);
	}

	/**
	 * @param facesContext the faces context
	 * @param bean the missinglist bean
	 */
	public void toMissinglistEdit(SquareFacesContext facesContext, MissinglistBean bean) {
		if (bean == null) { // if adding a missinglist, the bean must be null
			bean = new MissinglistBean(null, "", "", new ArrayList<>());
		}
		missinglist = bean.getList();
		missinglistName = bean.getName();
		missinglistDescription = bean.getDescription();
		missingtypes = new MissinglistGateway(facesContext).getAllMissingtypenames();
		missingBean = new MissingBean(null, "", "", null, "");
	}

	/**
	 * add the current missing bean to the missing list
	 * 
	 * @param facesContext the faces context
	 */
	public void addMissing(SquareFacesContext facesContext) {
		boolean abort = false;
		if (missingBean.getCode() == null || missingBean.getCode().isBlank()) {
			facesContext.notifyError("error.mustnotbeempty", "label.missingcode");
			abort = true;
		} else if (missingBean.getTypeName() == null || missingBean.getTypeName().isBlank()) {
			facesContext.notifyError("error.mustnotbeempty", "label.missingtypename");
			abort = true;
		}
		for (MissingBean bean : missinglist) {
			if (missingBean.getCode().equals(bean.getCode())) {
				facesContext.notifyError("error.missing.codeexistsinlist", missingBean.getCode());
				abort = true;
			}
		}
		if (!abort) {
			missinglist.add(missingBean);
			missingBean = new MissingBean(null, "", "", null, "");
		}
	}

	/**
	 * remove the bean from the missing list; the reference is the code
	 * 
	 * @param bean the bean
	 */
	public void removeMissing(MissingBean bean) {
		Iterator<MissingBean> i = missinglist.iterator();
		while (i.hasNext()) {
			MissingBean current = i.next();
			if (current.getCode().equals(bean.getCode())) {
				i.remove();
			}
		}
	}

	/**
	 * store the current missinglist in the database
	 * 
	 * @param facesContext the faces context
	 * @return true or false
	 */
	public boolean approveMissings(SquareFacesContext facesContext) {
		try {
			Integer found = new MissinglistGateway(facesContext).saveMissinglist(missinglist, missinglistName,
					missinglistDescription, fkStudygroup);
			facesContext.notifyAffected(found);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * remove the chosen attributeBean from the variabe attributes
	 * 
	 * @param variableBean the variable bean
	 * @param attributeBean the attribute bean
	 */
	public void removeAttributeFromVariable(VariableBean variableBean, VariableAttributeBean attributeBean) {
		Iterator<VariableAttributeBean> i = variableBean.getAttributes().iterator();
		while (i.hasNext()) {
			VariableAttributeBean found = i.next();
			if (found.getName() == null) {
				if (attributeBean.getName() == null) {
					i.remove();
					return;
				}
			} else if (found.getName().equals(attributeBean.getName())) {
				i.remove();
				return;
			}
		}
	}

	/**
	 * add attribute to variable bean
	 * 
	 * @param variableBean the variable bean
	 */
	public void addAttribute(VariableBean variableBean) {
		VariableAttributeBean bean = new VariableAttributeBean();
		bean.setIsPrimaryAttribute(false);
		bean.setRole(variableattributeroles.get(0)); // omit NPEs
		variableBean.getAttributes().add(bean);
	}

	/**
	 * prepare the edit missinglist form
	 * 
	 * @param facesContext the faces context
	 * @param bean the missinglist bean
	 */
	public void prepareEditMissinglist(SquareFacesContext facesContext, MissinglistBean bean) {
		missingBean = new MissingBean(null, "", "", null, "");
		toMissinglistEdit(facesContext, bean);
	}

	/**
	 * remove missinglist from db
	 * 
	 * @param facesContext the faces context
	 * @param bean the missinglist bean to be removed
	 * @return true or false
	 */
	public boolean removeMissinglist(SquareFacesContext facesContext, MissinglistBean bean) {
		try {
			Integer affected = new MissinglistGateway(facesContext).removeMissinglist(bean.getId());
			facesContext.notifyAffected(affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * check, if the parent is a study group
	 * 
	 * @param facesContext the faces context
	 * @param bean the element bean
	 * @return the id of the parent element or null, if the parent is a study
	 */
	public Integer getNullForStudyGroupParent(SquareFacesContext facesContext, ElementBean bean) {
		try {
			return new StudyGateway(facesContext).checkIfParentIsStudy(bean.getId()) ? null : bean.getParent();
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			return null;
		}
	}

	/**
	 * import the opal variables
	 * 
	 * @param facesContext the context of the call
	 * 
	 * @return the ID of the parent element
	 */
	public Integer doImportOpal(SquareFacesContext facesContext) {
		try (OpalGateway gw = new OpalGateway(facesContext)) {
			Object opalConfigFile = facesContext.getApplicationMapValue(ContextKey.OR_CONFIG_PATH);
			opalBean.checkNotEmpty(facesContext);
			gw.importFrom(opalConfigFile, opalBean);
			for (ElementBean bean : elements) {
				if (bean.getUniqueName().equals(opalBean.getParentUniqueName())) {
					return bean.getId();
				}
			}
		} catch (Exception e) {
			facesContext.notifyException(e);
		}
		return null;
	}

	/**
	 * import the SHIP variables and sections
	 * 
	 * @param facesContext the context of the call
	 * @return the ID of the parent element
	 */
	public Integer doImportShip(SquareFacesContext facesContext) {
		try (ShipGateway gw = new ShipGateway(facesContext)) {
			Object shipConfigSection = facesContext.getApplicationMapValue(ContextKey.SHIP_CONFIG_SECTION);
			gw.importFrom(shipConfigSection, shipBean);
			return new StudyGateway(facesContext).getIdOfUniqueName(shipBean.getDestUniqueName());
		} catch (Exception e) {
			facesContext.notifyException(e);
		}
		return null;
	}

	/**
	 * @param value the value
	 * @return a list of suggestions; an empty one at least
	 */
	public List<String> completeTypeName(String value) {
		return PrimefacesHelper.filter(missingtypes, value, true);
	}

	/**
	 * @return true if one of the children of deptmeta is dirty
	 */
	public Boolean hasDirtyDeptmeta() {
		for (DeptmetaBean child : deptmeta) {
			if (!child.getPersisted()) {
				return true; // abort seeking all the elements to reduce time
			}
		}
		return false;
	}

	/**
	 * @return all elements that are not variables
	 */
	public List<ElementBean> getElementsThatAreSections() {
		List<ElementBean> list = new ArrayList<>();
		for (ElementBean bean : elements) {
			if (bean != null && !bean.getIsVariable()) {
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * @return true if there are no sections as children of this element, false
	 * otherwise
	 */
	public Boolean getHasNoSections() {
		return getElementsThatAreSections().isEmpty();
	}

	/**
	 * @param facesContext the faces context
	 * @return the list of study groups for that user
	 */
	public List<StudygroupBean> getStudyGroupsOfUser(SquareFacesContext facesContext) {
		return new MissinglistGateway(facesContext).getAllStudyGroupElementsOf(facesContext.getUsnr(), null);
	}

	/**
	 * @return the control types
	 */
	public List<EnumControlvar> getControltypes() {
		return Arrays.asList(EnumControlvar.values());
	}

	/**
	 * @return the neighbours
	 */
	public List<VariableBean> getNeighbours() {
		return neighbours;
	}

	/**
	 * @return the new metadata bean
	 */
	public ConfounderBean getNewMetadataBean() {
		return newMetadataBean;
	}

	/**
	 * @return the unused metadata types
	 */
	public List<MetadatatypeBean> getUnusedMetadatatypes() {
		return unusedMetadatatypes;
	}

	/**
	 * @return the deptmeta beans
	 */
	public List<DeptmetaBean> getDeptmeta() {
		return deptmeta;
	}

	/**
	 * @return the deptmeta bean
	 */
	public DeptmetaBean getDeptmetaBean() {
		return deptmetaBean;
	}

	/**
	 * @param deptmetaBean the deptmeta bean
	 */
	public void setDeptmetaBean(DeptmetaBean deptmetaBean) {
		this.deptmetaBean = deptmetaBean;
	}

	/**
	 * @return the variable list parent id
	 */
	public Integer getVariablelistParentId() {
		return variablelistParentId;
	}

	/**
	 * @param variablelistParentId the variable list parent id
	 */
	public void setVariablelistParentId(Integer variablelistParentId) {
		this.variablelistParentId = variablelistParentId;
	}

	/**
	 * @return the flag if ordering is done the SHIP way
	 */
	public Boolean getOrderByShip() {
		return orderByShip;
	}

	/**
	 * @param orderByShip the flag if ordering is done the SHIP way
	 */
	public void setOrderByShip(Boolean orderByShip) {
		this.orderByShip = orderByShip;
	}

	/**
	 * @return the acl bean
	 */
	public AclBean getStudyAcl() {
		return studyAcl;
	}

	/**
	 * @param activePrivilege the active privilege
	 */
	public void setActivePrivilege(GroupPrivilegeBean activePrivilege) {
		this.activePrivilege = activePrivilege;
	}

	/**
	 * @return the new group
	 */
	public Integer getNewGroup() {
		return newGroup;
	}

	/**
	 * @param newGroup the new group
	 */
	public void setNewGroup(Integer newGroup) {
		this.newGroup = newGroup;
	}

	/**
	 * @return all groups
	 */
	public List<UsergroupBean> getAllGroups() {
		return allGroups;
	}

	/**
	 * @return the id of the new user
	 */
	public Integer getNewUsnr() {
		return newUsnr;
	}

	/**
	 * @param newUsnr the id of the new user
	 */
	public void setNewUsnr(Integer newUsnr) {
		this.newUsnr = newUsnr;
	}

	/**
	 * @return the new acl
	 */
	public AclBean getNewAcl() {
		return newAcl;
	}

	/**
	 * @return the list of group privilege beans
	 */
	public List<GroupPrivilegeBean> getGroupPrivileges() {
		return groupPrivileges;
	}

	/**
	 * @return the active page index
	 */
	public Integer getActiveIndex() {
		return activeIndex;
	}

	/**
	 * @param activeIndex the active page index
	 */
	public void setActiveIndex(Integer activeIndex) {
		this.activeIndex = activeIndex;
	}

	/**
	 * @return the active meta page index
	 */
	public Integer getActiveIndexMeta() {
		return activeIndexMeta;
	}

	/**
	 * @param activeIndexMeta the active meta page index
	 */
	public void setActiveIndexMeta(Integer activeIndexMeta) {
		this.activeIndexMeta = activeIndexMeta;
	}

	/**
	 * @return the list of children elements
	 */
	public List<ElementBean> getChildren() {
		return children;
	}

	/**
	 * @return the variableusages
	 */
	public List<VariableusageBean> getVariableusages() {
		return variableusages;
	}

	/**
	 * @return all types from squaremetadatainterface
	 */
	public List<String> getSmitypes() {
		return smitypes;
	}

	/**
	 * @return the includeSections
	 */
	public Boolean getIncludeSections() {
		return includeSections;
	}

	/**
	 * @param includeSections the includeSections to set
	 */
	public void setIncludeSections(Boolean includeSections) {
		this.includeSections = includeSections;
	}

	/**
	 * @return the missingBean
	 */
	public MissingBean getMissingBean() {
		return missingBean;
	}

	/**
	 * @param missingBean the missingBean to set
	 */
	public void setMissingBean(MissingBean missingBean) {
		this.missingBean = missingBean;
	}

	/**
	 * @return the missinglistName
	 */
	public String getMissinglistName() {
		return missinglistName;
	}

	/**
	 * @param missinglistName the missinglistName to set
	 */
	public void setMissinglistName(String missinglistName) {
		this.missinglistName = missinglistName;
	}

	/**
	 * @return the missinglistDescription
	 */
	public String getMissinglistDescription() {
		return missinglistDescription;
	}

	/**
	 * @param missinglistDescription the missinglistDescription to set
	 */
	public void setMissinglistDescription(String missinglistDescription) {
		this.missinglistDescription = missinglistDescription;
	}

	/**
	 * @return the variableattributeroles
	 */
	public List<VariableAttributeRoleBean> getVariableattributeroles() {
		return variableattributeroles;
	}

	/**
	 * @return the chosenElementName
	 */
	public String getChosenElementName() {
		return chosenElementName;
	}

	/**
	 * @param chosenElementName the chosenElementName to set
	 */
	public void setChosenElementName(String chosenElementName) {
		this.chosenElementName = chosenElementName;
	}

	/**
	 * @return the chosenId
	 */
	public Integer getChosenId() {
		return chosenId;
	}

	/**
	 * @param chosenId the chosenId to set
	 */
	public void setChosenId(Integer chosenId) {
		this.chosenId = chosenId;
	}

	/**
	 * @return the fkStudygroup
	 */
	public Integer getFkStudygroup() {
		return fkStudygroup;
	}

	/**
	 * @param fkStudygroup the fkStudygroup to set
	 */
	public void setFkStudygroup(Integer fkStudygroup) {
		this.fkStudygroup = fkStudygroup;
	}

	/**
	 * @return the missinglist
	 */
	public List<MissingBean> getMissinglist() {
		return missinglist;
	}

	/**
	 * @return the missinglists
	 */
	public List<MissinglistBean> getMissinglists() {
		return missinglists;
	}

	/**
	 * @param missinglists the missinglists to set
	 */
	public void setMissinglists(List<MissinglistBean> missinglists) {
		this.missinglists = missinglists;
	}

	/**
	 * @return the opalBean
	 */
	public OpalBean getOpalBean() {
		return opalBean;
	}

	/**
	 * @param opalBean the opalBean to set
	 */
	public void setOpalBean(OpalBean opalBean) {
		this.opalBean = opalBean;
	}

	/**
	 * @return the shipBean
	 */
	public ShipBean getShipBean() {
		return shipBean;
	}

	/**
	 * @param shipBean the shipBean to set
	 */
	public void setShipBean(ShipBean shipBean) {
		this.shipBean = shipBean;
	}

	/**
	 * @return the possibleTimeVars
	 */
	public List<IdLabelBean> getPossibleTimeVars() {
		return possibleTimeVars;
	}

	/**
	 * @return the studyvarBean
	 */
	public StudyvarBean getStudyvarBean() {
		return studyvarBean;
	}
}