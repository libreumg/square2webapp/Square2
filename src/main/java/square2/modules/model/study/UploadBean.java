package square2.modules.model.study;

import java.io.IOException;
import java.io.InputStream;

import javax.servlet.http.Part;

/**
 * 
 * @author henkej
 *
 */
public class UploadBean {
	private Integer elementId;
	private Integer parent;
	private String parentName;
	private Integer parser;
	private Part file;
	private String separator;
	private String delimiter;
	private String type;

	/**
	 * create new upload bean with default comma separator and ' delimiter
	 */
	public UploadBean() {
		separator = ",";
		delimiter = "'";
		type = "ods"; // default io type
	}

	/**
	 * reads the file and returns the corresponding byte array
	 * 
	 * @return the byte array of the file
	 * @throws IOException
	 *           on io errors
	 */
	public byte[] getBytes() throws IOException {
		byte[] result = null;
		if (file != null) {
			try (InputStream is = file.getInputStream()) {
				byte content[] = new byte[(int) file.getSize()];
				is.read(content);
				result = content;
			} catch (IOException e) {
				throw e;
			}
		}
		return result;
	}

	/**
	 * @return the elementId
	 */
	public Integer getElementId() {
		return elementId;
	}

	/**
	 * @param elementId
	 *          the elementId to set
	 */
	public void setElementId(Integer elementId) {
		this.elementId = elementId;
	}

	/**
	 * @return the parent
	 */
	public Integer getParent() {
		return parent;
	}

	/**
	 * @param parent
	 *          the parent to set
	 */
	public void setParent(Integer parent) {
		this.parent = parent;
	}

	/**
	 * @return the parentName
	 */
	public String getParentName() {
		return parentName;
	}

	/**
	 * @param parentName
	 *          the parentName to set
	 */
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}

	/**
	 * @return the parser
	 */
	public Integer getParser() {
		return parser;
	}

	/**
	 * @param parser
	 *          the parser to set
	 */
	public void setParser(Integer parser) {
		this.parser = parser;
	}

	/**
	 * @return the file
	 */
	public Part getFile() {
		return file;
	}

	/**
	 * @param file
	 *          the file to set
	 */
	public void setFile(Part file) {
		this.file = file;
	}

	/**
	 * @return the separator
	 */
	public String getSeparator() {
		return separator;
	}

	/**
	 * @param separator
	 *          the separator to set
	 */
	public void setSeparator(String separator) {
		this.separator = separator;
	}

	/**
	 * @return the delimiter
	 */
	public String getDelimiter() {
		return delimiter;
	}

	/**
	 * @param delimiter
	 *          the delimiter to set
	 */
	public void setDelimiter(String delimiter) {
		this.delimiter = delimiter;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *          the type
	 */
	public void setType(String type) {
		this.type = type;
	}
}
