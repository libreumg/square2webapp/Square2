package square2.modules.model.study;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class IdLabelBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String label;

	/**
	 * @param id the id
	 * @param label the label
	 */
	public IdLabelBean(Integer id, String label) {
		super();
		this.id = id;
		this.label = label;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
}
