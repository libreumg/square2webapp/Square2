package square2.modules.model.study;

import java.util.HashMap;
import java.util.Map;

import de.ship.dbppsquare.square.enums.EnumMetareftype;

/**
 * 
 * @author henkej
 *
 */
public class MetarefBean {
	private Map<EnumMetareftype, Integer> map;
	private EnumMetareftype newKey;
	private Integer newValue;

	/**
	 * create new metaref bean
	 */
	public MetarefBean() {
		this.map = new HashMap<>();
	}

	/**
	 * remove an entry
	 * 
	 * @param key
	 *          the key
	 */
	public void remove(EnumMetareftype key) {
		map.remove(key);
	}

	/**
	 * add an entry
	 * 
	 * @param clearAfterIntegration
	 *          flag to determine if a reset is needed
	 */
	public void integrateNewValues(boolean clearAfterIntegration) {
		map.put(newKey, newValue);
		if (clearAfterIntegration) {
			resetNew();
		}
	}

	/**
	 * force reset
	 */
	public void resetNew() {
		this.newKey = null;
		this.newValue = null;
	}

	/**
	 * @return the new key
	 */
	public EnumMetareftype getNewKey() {
		return newKey;
	}

	/**
	 * @param newKey
	 *          the new key
	 */
	public void setNewKey(EnumMetareftype newKey) {
		this.newKey = newKey;
	}

	/**
	 * @return the new value
	 */
	public Integer getNewValue() {
		return newValue;
	}

	/**
	 * @param newValue
	 *          the new value
	 */
	public void setNewValue(Integer newValue) {
		this.newValue = newValue;
	}

	/**
	 * @return the map
	 */
	public Map<EnumMetareftype, Integer> getMap() {
		return map;
	}
}
