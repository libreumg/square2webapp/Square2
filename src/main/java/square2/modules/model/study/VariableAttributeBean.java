package square2.modules.model.study;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class VariableAttributeBean implements Serializable, Comparable<VariableAttributeBean> {
  private static final long serialVersionUID = 1L;

  private String name;
  private String value;
  private VariableAttributeRoleBean role;
  private Boolean isPrimaryAttribute;
  private String locale;

  /**
   * @param bean the bean
   * @return this class
   */
  public VariableAttributeBean withRoleBean(VariableAttributeRoleBean bean) {
    this.role = bean;
    return this;
  }
  
  /**
   * @param isPrimaryAttribute the value
   * @return this class
   */
  public VariableAttributeBean withPrimaryAttribute(boolean isPrimaryAttribute) {
    isPrimaryAttribute = this.isPrimaryAttribute;
    return this;
  }
    
  /**
   * @param json the json representation
   */
  public void setRoleJson(String json) {
    role = new VariableAttributeRoleBean(null, null, null, null, null, null).fromJson(json);
  }
  
  /**
   * @return the json representation of role
   */
  public String getRoleJson() {
    return role == null ? null : role.toJson();
  }

  @Override
  public int compareTo(VariableAttributeBean bean) {
    return name == null || bean == null ? 0 : name.compareTo(bean.getName());
  }

  /**
   * check if this attribute is a label (its role's name is label)
   * 
   * @return true or false
   */
  public boolean isLabel() {
    String roleName = role == null ? "" : role.getName();
    return "label".equals(roleName);
  }

  /**
   * get pk of role
   * 
   * @return the id of the role or null
   */
  public Integer getRolePk() {
    return role == null ? null : role.getPk();
  }

  /**
   * return true if hasInputWidget is not empty
   * 
   * @return true or false
   */
  public Boolean getHasInputWidget() {
    return role == null ? false : (role.getInputwidget() != null && !role.getInputwidget().isBlank());
  }

  /**
   * return true if regexpattern is not empty
   * 
   * @return true or false
   */
  public Boolean getHasRegexpattern() {
    return role == null ? false : (role.getRegexpattern() != null && !role.getRegexpattern().isBlank());
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value the value to set
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * @return the isPrimaryAttribute
   */
  public Boolean getIsPrimaryAttribute() {
    return isPrimaryAttribute;
  }

  /**
   * @param isPrimaryAttribute the isPrimaryAttribute to set
   */
  public void setIsPrimaryAttribute(Boolean isPrimaryAttribute) {
    this.isPrimaryAttribute = isPrimaryAttribute;
  }

  /**
   * @return the locale
   */
  public String getLocale() {
    return locale;
  }

  /**
   * @param locale the locale to set
   */
  public void setLocale(String locale) {
    this.locale = locale;
  }

  /**
   * @return the role
   */
  public VariableAttributeRoleBean getRole() {
    return role;
  }

  /**
   * @param role the role to set
   */
  public void setRole(VariableAttributeRoleBean role) {
    this.role = role;
  }
}
