package square2.modules.model.study;

import ship.jsf.components.selectFilterableMenu.BeanWrapperInterface;

/**
 * 
 * @author henkej
 *
 */
public class ListVariableFilterWrapper implements BeanWrapperInterface {
	private ListElement bean;

	/**
	 * create new list variable filter wrapper
	 * 
	 * @param bean
	 *          the list element bean
	 */
	public ListVariableFilterWrapper(ListElement bean) {
		this.bean = bean;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("{key=").append(getKey());
		buf.append(",value=").append(getValue());
		buf.append("}");
		return buf.toString();
	}

	@Override
	public Integer getKey() {
		return bean.getFkElement();
	}

	@Override
	public String getValue() {
		return bean.getName();
	}
}
