package square2.modules.model.study;

/**
 * 
 * @author henkej
 *
 */
public class MetadataVariableReferenceBean {
	private String dn;
	private String starttimeDn;
	private String endtimeDn;
	private String deviceDn;

	/**
	 * @return the dn
	 */
	public String getDn() {
		return dn;
	}

	/**
	 * @param dn
	 *          the dn to set
	 */
	public void setDn(String dn) {
		this.dn = dn;
	}

	/**
	 * @return the starttimeDn
	 */
	public String getStarttimeDn() {
		return starttimeDn;
	}

	/**
	 * @param starttimeDn
	 *          the starttimeDn to set
	 */
	public void setStarttimeDn(String starttimeDn) {
		this.starttimeDn = starttimeDn;
	}

	/**
	 * @return the endtimeDn
	 */
	public String getEndtimeDn() {
		return endtimeDn;
	}

	/**
	 * @param endtimeDn
	 *          the endtimeDn to set
	 */
	public void setEndtimeDn(String endtimeDn) {
		this.endtimeDn = endtimeDn;
	}

	/**
	 * @return the deviceDn
	 */
	public String getDeviceDn() {
		return deviceDn;
	}

	/**
	 * @param deviceDn
	 *          the deviceDn to set
	 */
	public void setDeviceDn(String deviceDn) {
		this.deviceDn = deviceDn;
	}
}
