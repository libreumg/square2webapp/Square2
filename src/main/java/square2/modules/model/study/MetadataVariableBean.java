package square2.modules.model.study;

/**
 * 
 * @author henkej
 *
 */
public class MetadataVariableBean {
	private final Integer keyElement;
	private String studyDn;
	private String parentDn;
	private String tree;
	private String shortName;
	private String dn;
	private String uniqueName;

	private MetadataVariableReferenceBean mdtObserver;
	private MetadataVariableReferenceBean mdtReader;
	private MetadataVariableReferenceBean mdtMta;
	private MetadataVariableReferenceBean mdtStudycenter;
	private MetadataVariableReferenceBean mdtInterventiongroup;

	/**
	 * create new metadata variable bean
	 * 
	 * @param keyElement
	 *          the id of the element
	 */
	public MetadataVariableBean(Integer keyElement) {
		super();
		this.keyElement = keyElement;
	}

	/**
	 * @return the studyDn
	 */
	public String getStudyDn() {
		return studyDn;
	}

	/**
	 * @param studyDn
	 *          the studyDn to set
	 */
	public void setStudyDn(String studyDn) {
		this.studyDn = studyDn;
	}

	/**
	 * @return the parentDn
	 */
	public String getParentDn() {
		return parentDn;
	}

	/**
	 * @param parentDn
	 *          the parentDn to set
	 */
	public void setParentDn(String parentDn) {
		this.parentDn = parentDn;
	}

	/**
	 * @return the tree
	 */
	public String getTree() {
		return tree;
	}

	/**
	 * @param tree
	 *          the tree to set
	 */
	public void setTree(String tree) {
		this.tree = tree;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName
	 *          the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the dn
	 */
	public String getDn() {
		return dn;
	}

	/**
	 * @param dn
	 *          the dn to set
	 */
	public void setDn(String dn) {
		this.dn = dn;
	}

	/**
	 * @return the mdtObserver
	 */
	public MetadataVariableReferenceBean getMdtObserver() {
		return mdtObserver;
	}

	/**
	 * @param mdtObserver
	 *          the mdtObserver to set
	 */
	public void setMdtObserver(MetadataVariableReferenceBean mdtObserver) {
		this.mdtObserver = mdtObserver;
	}

	/**
	 * @return the mdtReader
	 */
	public MetadataVariableReferenceBean getMdtReader() {
		return mdtReader;
	}

	/**
	 * @param mdtReader
	 *          the mdtReader to set
	 */
	public void setMdtReader(MetadataVariableReferenceBean mdtReader) {
		this.mdtReader = mdtReader;
	}

	/**
	 * @return the mdtMta
	 */
	public MetadataVariableReferenceBean getMdtMta() {
		return mdtMta;
	}

	/**
	 * @param mdtMta
	 *          the mdtMta to set
	 */
	public void setMdtMta(MetadataVariableReferenceBean mdtMta) {
		this.mdtMta = mdtMta;
	}

	/**
	 * @return the mdtStudycenter
	 */
	public MetadataVariableReferenceBean getMdtStudycenter() {
		return mdtStudycenter;
	}

	/**
	 * @param mdtStudycenter
	 *          the mdtStudycenter to set
	 */
	public void setMdtStudycenter(MetadataVariableReferenceBean mdtStudycenter) {
		this.mdtStudycenter = mdtStudycenter;
	}

	/**
	 * @return the mdtInterventiongroup
	 */
	public MetadataVariableReferenceBean getMdtInterventiongroup() {
		return mdtInterventiongroup;
	}

	/**
	 * @param mdtInterventiongroup
	 *          the mdtInterventiongroup to set
	 */
	public void setMdtInterventiongroup(MetadataVariableReferenceBean mdtInterventiongroup) {
		this.mdtInterventiongroup = mdtInterventiongroup;
	}

	/**
	 * @return the keyElement
	 */
	public Integer getKeyElement() {
		return keyElement;
	}

	/**
	 * @return the uniqueName
	 */
	public String getUniqueName() {
		return uniqueName;
	}

	/**
	 * @param uniqueName the uniqueName to set
	 */
	public void setUniqueName(String uniqueName) {
		this.uniqueName = uniqueName;
	}
}
