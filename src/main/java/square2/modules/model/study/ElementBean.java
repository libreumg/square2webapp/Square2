package square2.modules.model.study;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.jooq.JSONB;

import square2.modules.model.AclBean;

/**
 * 
 * @author henkej
 *
 */
public class ElementBean implements Serializable, Comparable<ElementBean> {
  private static final long serialVersionUID = 1L;

  private final Integer id;
  private String uniqueName;
  private final Integer rightId;
  private final Integer orderNr;
  private final Integer shipOrder;
  private final JSONB locales;
  private String name;
  private Integer timevar;
  /**
   * use translation instead
   */
  @Deprecated
  private String label;
  private AclBean accessLevel;
  private Boolean isVariable;
  private List<ElementBean> children;
  private List<VariableBean> variables;
  private Integer parent;
  private JSONB translation;
  private String study;
  private String studygroup;
  private Boolean isSelected;
  private String title;
  private Integer fkMissinglist;
  private String missinglistReadable;

  private Integer numberOfSections;
  private Integer numberOfVariables;
  private Integer percentageOfCovered;

  /**
   * create a new element bean
   * 
   * @param uniqueName the unique name
   * @param id the primary key of this element
   * @param rightId the privilege id of this element
   * @param parent the parent id of this element, might be null (for study groups)
   * @param translation the translation of this element
   * @param label the label
   * @param orderNr the order number of this element
   * @param shipOrder the SHIP order number, deprecated, will be removed soon
   * @param study the name of the study for this element
   * @param studygroup the name of the study group for this element
   * @param fkMissinglist the missing list
   * @param locales the locales
   */
  public ElementBean(String uniqueName, Integer id, Integer rightId, Integer parent, JSONB translation, String label,
      Integer orderNr, Integer shipOrder, String study, String studygroup, Integer fkMissinglist, JSONB locales) {
    this.id = id;
    this.uniqueName = uniqueName;
    this.rightId = rightId;
    this.orderNr = orderNr;
    this.shipOrder = shipOrder;
    this.parent = parent;
    this.translation = translation;
    this.children = new ArrayList<>();
    this.variables = new ArrayList<>();
    this.accessLevel = new AclBean();
    this.label = label;
    this.study = study;
    this.studygroup = studygroup;
    this.isSelected = true;
    this.fkMissinglist = fkMissinglist;
    this.locales = locales;
  }

  /**
   * create a new element bean
   * 
   * @param id the primary key of this element
   * @param uniqueName the unique name
   * @param translation the translation of this element
   * @param name the name of this element
   * @param label the label
   * @param orderNr the order number of this element
   * @param shipOrder the SHIP order number, deprecated, will be removed soon
   * @param study the name of the study for this element
   * @param studygroup the name of the study group for this element
   * @param fkMissinglist the missing list
   * @param locales the locales
   */
  public ElementBean(String uniqueName, Integer id, JSONB translation, String name, String label, Integer orderNr,
      Integer shipOrder, String study, String studygroup, Integer fkMissinglist, JSONB locales) {
    this.id = id;
    this.uniqueName = uniqueName;
    this.orderNr = orderNr;
    this.shipOrder = shipOrder;
    this.translation = translation;
    this.name = name;
    this.rightId = null;
    this.children = new ArrayList<>();
    this.variables = new ArrayList<>();
    this.accessLevel = new AclBean();
    this.label = label;
    this.study = study;
    this.studygroup = studygroup;
    this.isSelected = true;
    this.fkMissinglist = fkMissinglist;
    this.locales = locales;
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
    buf.append("@{id=").append(id);
    buf.append(",uniqueName=").append(uniqueName);
    buf.append(",rightId=").append(rightId);
    buf.append(",name=").append(name);
    buf.append(",accessLevel=").append(accessLevel);
    buf.append(",isVariable=").append(isVariable);
    buf.append(",children=").append(children);
    buf.append(",variables=").append(variables);
    buf.append(",parent=").append(parent);
    buf.append(",translation=").append(translation);
    buf.append(",study=").append(study);
    buf.append(",studygroup=").append(studygroup);
    buf.append(",label=").append(label);
    buf.append(",locales=").append(locales);
    buf.append("}");
    return buf.toString();
  }

  /**
   * @deprecated use compareByIdTo instead
   */
  @Override
  @Deprecated
  public int compareTo(ElementBean o) {
    return compareByIdTo(o);
  }

  /**
   * compare this element's id with the given element's id
   * 
   * @param o the given element
   * @return the comparison, 0 for equality
   */
  public int compareByIdTo(ElementBean o) {
    return id == null || o == null || o.id == null ? 0 : id.compareTo(o.getId());
  }

  /**
   * compare this element's name with the given element's name
   * 
   * @param o the given element
   * @return the comparison, 0 for equality
   */
  public int compareByNameTo(ElementBean o) {
    return name == null || o == null || o.name == null ? 0 : name.compareTo(o.getName());
  }

  /**
   * check if this element should be disabled in the breadcrumbs; study groups and variables are such elements
   * 
   * @return true or false
   */
  public Boolean getDisableBreadcrumb() {
    boolean check = isVariable == null ? false : isVariable;
    return check || parent == null;
  }

  /**
   * @return the locales json string
   */
  public String getLocalesJson() {
  	return locales == null ? "{}" : locales.data();
  }
  
  /**
   * @return the json translation string
   */
  public String getTranslationJson() {
  	return translation == null ? "{}" : translation.data();
  }
  
  /**
   * @param json the translation string
   */
  public void setTranslationJson(String json) {
  	this.translation = json == null ? JSONB.valueOf("{}") : JSONB.valueOf(json);
  }
  
  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the children of this element
   */
  public List<ElementBean> getChildren() {
    return children;
  }

  /**
   * @return the variable bean for this element if any
   */
  public List<VariableBean> getVariables() {
    return variables;
  }

  /**
   * @return the privileges
   */
  public AclBean getAccessLevel() {
    return accessLevel;
  }

  /**
   * @return the id in the hierarchy tree
   */
  public Integer getId() {
    return id;
  }

  /**
   * @return the privilege id
   */
  public Integer getRightId() {
    return rightId;
  }

  /**
   * @return true if this is a variable
   */
  public Boolean getIsVariable() {
    return isVariable;
  }

  /**
   * @param isVariable the is variable flag
   */
  public void setIsVariable(Boolean isVariable) {
    this.isVariable = isVariable;
  }

  /**
   * @return the parent id
   */
  public Integer getParent() {
    return parent;
  }

  /**
   * @param parent the id of the parent element
   */
  public void setParent(Integer parent) {
    this.parent = parent;
  }

  /**
   * @return the order number
   */
  public Integer getOrderNr() {
    return orderNr;
  }

  /**
   * @return the SHIP order number
   */
  public Integer getShipOrder() {
    return shipOrder;
  }

  /**
   * @return the study name
   */
  public String getStudy() {
    return study;
  }

  /**
   * @param study the study
   */
  public void setStudy(String study) {
    this.study = study;
  }

  /**
   * @return the study group name
   */
  public String getStudygroup() {
    return studygroup;
  }

  /**
   * @param studygroup the study group
   */
  public void setStudygroup(String studygroup) {
    this.studygroup = studygroup;
  }

  /**
   * @return the numberOfSections
   */
  public Integer getNumberOfSections() {
    return numberOfSections;
  }

  /**
   * @param numberOfSections the numberOfSections to set
   */
  public void setNumberOfSections(Integer numberOfSections) {
    this.numberOfSections = numberOfSections;
  }

  /**
   * @return the numberOfVariables
   */
  public Integer getNumberOfVariables() {
    return numberOfVariables;
  }

  /**
   * @param numberOfVariables the numberOfVariables to set
   */
  public void setNumberOfVariables(Integer numberOfVariables) {
    this.numberOfVariables = numberOfVariables;
  }

  /**
   * @return the percentageOfCovered
   */
  public Integer getPercentageOfCovered() {
    return percentageOfCovered;
  }

  /**
   * @param percentageOfCovered the percentageOfCovered to set
   */
  public void setPercentageOfCovered(Integer percentageOfCovered) {
    this.percentageOfCovered = percentageOfCovered;
  }

  /**
   * @return the uniqueName
   */
  public String getUniqueName() {
    return uniqueName;
  }

  /**
   * @param uniqueName the uniqueName to set
   */
  public void setUniqueName(String uniqueName) {
    this.uniqueName = uniqueName;
  }

  /**
   * @return true or false
   */
  public Boolean getIsSelected() {
    return isSelected;
  }

  /**
   * @param isSelected true or false
   */
  public void setIsSelected(Boolean isSelected) {
    this.isSelected = isSelected;
  }

  /**
   * @return the title
   */
  public String getTitle() {
    return title;
  }

  /**
   * @param title the title to set
   */
  public void setTitle(String title) {
    this.title = title;
  }

  /**
   * @return the fkMissinglist
   */
  public Integer getFkMissinglist() {
    return fkMissinglist;
  }

  /**
   * @param fkMissinglist the fkMissinglist to set
   */
  public void setFkMissinglist(Integer fkMissinglist) {
    this.fkMissinglist = fkMissinglist;
  }

  /**
   * @return the missinglistReadable
   */
  public String getMissinglistReadable() {
    return missinglistReadable;
  }

  /**
   * @param missinglistReadable the missinglistReadable to set
   */
  public void setMissinglistReadable(String missinglistReadable) {
    this.missinglistReadable = missinglistReadable;
  }

	/**
	 * @return the translation
	 */
	public JSONB getTranslation() {
		return translation;
	}

	/**
	 * @param translation the translation to set
	 */
	public void setTranslation(JSONB translation) {
		this.translation = translation;
	}

	/**
	 * @return the locales
	 */
	public JSONB getLocales() {
		return locales;
	}

	/**
	 * @return the timevar
	 */
	public Integer getTimevar() {
		return timevar;
	}

	/**
	 * @param timevar the timevar to set
	 */
	public void setTimevar(Integer timevar) {
		this.timevar = timevar;
	}
}
