package square2.modules.model.study;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ship.jsf.components.dataTable.FilterableBean;
import square2.modules.model.AclBean;
import square2.modules.model.report.design.ReportDesignBean;

/**
 * 
 * @author henkej
 *
 */
public class VariablegroupBean extends FilterableBean implements Serializable, Comparable<VariablegroupBean> {
	private static final long serialVersionUID = 1L;

	private Integer variablegroupId;
	private String name;
	private String description;
	private Integer fkRight;
	private AclBean acl;
	private Long elements;
	private List<ListElement> listVariables;
	private List<String> dependings;
	private List<ReportDesignBean> reports;
	private String users;

	/**
	 * create new variable group bean
	 */
	public VariablegroupBean() {
		super();
		this.acl = new AclBean();
		this.listVariables = new ArrayList<>();
		this.dependings = new ArrayList<>();
		this.reports = new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{variablegroupId=").append(variablegroupId);
		buf.append(",name=").append(name);
		buf.append(",description=").append(description);
		buf.append(",fkRight=").append(fkRight);
		buf.append(",accessLevel=").append(acl);
		buf.append(",elements=").append(elements);
		buf.append(",listVariables=").append(listVariables);
		buf.append(",dependings=").append(dependings);
		buf.append("}");
		return buf.toString();
	}

	@Override
	public int compareTo(VariablegroupBean o) {
		return name == null || o == null || o.getName() == null ? 0
				: name.toLowerCase().compareTo(o.getName().toLowerCase());
	}

	/**
	 * @return the variablegroupId
	 */
	public Integer getVariablegroupId() {
		return variablegroupId;
	}

	/**
	 * @param variablegroupId
	 *          the variablegroupId to set
	 */
	public void setVariablegroupId(Integer variablegroupId) {
		this.variablegroupId = variablegroupId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the fkRight
	 */
	public Integer getFkRight() {
		return fkRight;
	}

	/**
	 * @param fkRight
	 *          the fkRight to set
	 */
	public void setFkRight(Integer fkRight) {
		this.fkRight = fkRight;
	}

	/**
	 * @return the acl
	 */
	public AclBean getAcl() {
		return acl;
	}

	/**
	 * @param acl
	 *          the acl to set
	 */
	public void setAcl(AclBean acl) {
		this.acl = acl;
	}

	/**
	 * @return the elements
	 */
	public Long getElements() {
		return elements;
	}

	/**
	 * @param elements
	 *          the elements to set
	 */
	public void setElements(Long elements) {
		this.elements = elements;
	}

	/**
	 * @return the listVariables
	 */
	public List<ListElement> getListVariables() {
		return listVariables;
	}

	/**
	 * @return the dependings
	 */
	public List<String> getDependings() {
		return dependings;
	}

	/**
	 * @return the reports
	 */
	public List<ReportDesignBean> getReports() {
		return reports;
	}

	/**
	 * @return the users
	 */
	public String getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(String users) {
		this.users = users;
	}
}
