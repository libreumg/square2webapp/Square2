package square2.modules.model.study.io;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class ShipBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String srcUniqueName;
	private String destUniqueName;
	private String locale;

	/**
	 * @return the srcUniqueName
	 */
	public String getSrcUniqueName() {
		return srcUniqueName;
	}

	/**
	 * @param srcUniqueName the srcUniqueName to set
	 */
	public void setSrcUniqueName(String srcUniqueName) {
		this.srcUniqueName = srcUniqueName;
	}

	/**
	 * @return the destUniqueName
	 */
	public String getDestUniqueName() {
		return destUniqueName;
	}

	/**
	 * @param destUniqueName the destUniqueName to set
	 */
	public void setDestUniqueName(String destUniqueName) {
		this.destUniqueName = destUniqueName;
	}

	/**
	 * @return the locale
	 */
	public String getLocale() {
		return locale;
	}

	/**
	 * @param locale the locale to set
	 */
	public void setLocale(String locale) {
		this.locale = locale;
	}
}
