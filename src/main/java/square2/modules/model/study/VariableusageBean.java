package square2.modules.model.study;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class VariableusageBean implements Serializable, Comparable<VariableusageBean> {
	private static final long serialVersionUID = 1L;
	private Integer pk;
	private String name;
	private Integer fkLang;

	/**
	 * create a new variable usage bean
	 * 
	 * @param pk
	 *          the id of the usage bean
	 */
	public VariableusageBean(Integer pk) {
		this.pk = pk;
	}

	@Override
	public int compareTo(VariableusageBean o) {
		return o == null || name == null || o.getName() == null ? 0 : name.compareTo(o.getName());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fkLang
	 */
	public Integer getFkLang() {
		return fkLang;
	}

	/**
	 * @param fkLang
	 *          the fkLang to set
	 */
	public void setFkLang(Integer fkLang) {
		this.fkLang = fkLang;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
		this.name = null; // a pk change requires to change name and fkLang also
		this.fkLang = null;
	}
}
