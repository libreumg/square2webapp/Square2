package square2.modules.model.study;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jooq.JSONB;
import org.jooq.exception.DataAccessException;

import de.ship.dbppsquare.square.enums.EnumControlvar;
import square2.modules.model.AclBean;

/**
 * 
 * @author henkej
 *
 */
public class VariableBean implements Serializable, Comparable<VariableBean> {
  private static final long serialVersionUID = 1L;

  private Integer keyElement;
  private String columnName;
  private String datatype;
  private Boolean idvariable;
  private String uniqueName;
  private Integer varOrder;
  private final Integer shipOrder;
  private Integer keyParentElement;
  private String name;
  private JSONB translation;
  private final JSONB locales;
  private Integer rightId;
  private String valuelist;
  private List<ConfounderBean> metadatalist;
  private EnumControlvar controltype;
  private String study;
  private String studygroup;
  private AclBean acl;
  private VariableusageBean variableusage;
  private Integer fkVariableusage;
  private final List<VariableAttributeBean> attributes;
  private Integer fkMissinglist;

  /**
   * create new variable bean
   * 
   * @param columnName the column name
   * @param datatype the datatype
   * @param idvariable if this is an id variable
   * @param uniqueName the unique name
   * @param name the name
   * @param keyParentElement id of the parent element
   * @param translation the current translation
   * @param shipOrder the order number
   * @param study the study
   * @param studygroup the study group
   * @param variableusage the variable usage
   * @param fkMissinglist the missinglist
   * @param locales the locales for the translation
   */
  public VariableBean(String columnName, String datatype, Boolean idvariable, String uniqueName, String name,
      Integer keyParentElement, JSONB translation, Integer shipOrder, String study, String studygroup,
      VariableusageBean variableusage, Integer fkMissinglist, JSONB locales) {
    super();
    this.columnName = columnName;
    this.datatype = datatype;
    this.idvariable = idvariable;
    this.uniqueName = uniqueName;
    this.name = name;
    this.shipOrder = shipOrder;
    this.keyElement = null;
    this.keyParentElement = keyParentElement;
    metadatalist = new ArrayList<>();
    this.translation = translation;
    this.study = study;
    this.studygroup = studygroup;
    this.variableusage = variableusage;
    this.attributes = new ArrayList<>();
    this.fkMissinglist = fkMissinglist;
    this.locales = locales;
  }

  /**
   * create a new variable bean
   * 
   * @param columnName the column name
   * @param datatype the datatype
   * @param idvariable if this is an id variable
   * @param uniqueName the unique name
   * @param name the name
   * @param keyElement the id of the element
   * @param keyParentElement the id of the parent element
   * @param translation the current translation
   * @param shipOrder the order number
   * @param study the study
   * @param studygroup the study group
   * @param variableusage the variable usage
   * @param fkMissinglist the missinglist
   * @param locales the locales for the translation
  */
  public VariableBean(String columnName, String datatype, Boolean idvariable, String uniqueName, String name,
      Integer keyElement, Integer keyParentElement, JSONB translation, Integer shipOrder, String study,
      String studygroup, VariableusageBean variableusage, Integer fkMissinglist, JSONB locales) {
    super();
    this.columnName = columnName;
    this.datatype = datatype;
    this.idvariable = idvariable;
    this.uniqueName = uniqueName;
    this.shipOrder = shipOrder;
    this.keyElement = keyElement;
    this.keyParentElement = keyParentElement;
    metadatalist = new ArrayList<>();
    this.translation = translation;
    this.study = study;
    this.studygroup = studygroup;
    this.variableusage = variableusage;
    this.attributes = new ArrayList<>();
    this.fkMissinglist = fkMissinglist;
    this.locales = locales;
  }

  /**
   * create new variable bean
   * 
   * @param columnName the column name
   * @param datatype the datatype
   * @param idvariable if this is an id variable
   * @param uniqueName the unique name
   * @param keyElement the id of the element
   * @param varOrder the variable order
   * @param keyParentElement the id of the parent element
   * @param name the name
   * @param rightId the id of the privilege
   * @param valuelist the value list
   * @param scale the scale
   * @param limitHardLow the lower hard limit
   * @param limitHardUp the upper hard limit
   * @param limitSoftLow the lower soft limit
   * @param limitSoftUp the upper soft limit
   * @param controltype the control type
   * @param varLabel the variable label
   * @param varShortLabel the variable short label
   * @param translation the current translation
   * @param shipOrder the order number
   * @param study the study
   * @param studygroup the study group
   * @param variableusage the variable usage
   * @param fkMissinglist the missinglist
   * @param locales the locales for the translation
   */
  public VariableBean(String columnName, String datatype, Boolean idvariable, String uniqueName, Integer keyElement,
      Integer varOrder, Integer keyParentElement, String name, Integer rightId, String valuelist, String scale,
      String limitHardLow, String limitHardUp, String limitSoftLow, String limitSoftUp, EnumControlvar controltype,
      String varLabel, String varShortLabel, JSONB translation, Integer shipOrder, String study,
      String studygroup, VariableusageBean variableusage, Integer fkMissinglist, JSONB locales) {
    super();
    this.columnName = columnName;
    this.datatype = datatype;
    this.idvariable = idvariable;
    this.uniqueName = uniqueName;
    this.keyElement = keyElement;
    this.varOrder = varOrder;
    this.shipOrder = shipOrder;
    this.keyParentElement = keyParentElement;
    this.name = name;
    this.rightId = rightId;
    this.valuelist = valuelist;
    this.controltype = controltype;
    metadatalist = new ArrayList<>();
    this.translation = translation;
    this.study = study;
    this.studygroup = studygroup;
    this.variableusage = variableusage;
    attributes = new ArrayList<>();
    this.fkMissinglist = fkMissinglist;
    this.locales = locales;
  }

  @Override
  public int compareTo(VariableBean o) {
    return varOrder == null || o == null || o.getVarOrder() == null ? 0 : varOrder.compareTo(o.getVarOrder());
  }

  /**
   * reset the variable attributes beans to make them fit in dropdowns for object references
   * 
   * @param variableattributeroles the list of valid variable attribute roles
   */
  public void resetVariableattributeroles(List<VariableAttributeRoleBean> variableattributeroles) {
    Map<Integer, VariableAttributeRoleBean> map = new HashMap<>();
    for (VariableAttributeRoleBean bean : variableattributeroles) {
      map.put(bean.getPk(), bean);
    }

    for (VariableAttributeBean bean : attributes) {
      VariableAttributeRoleBean rBean = map.get(bean.getRolePk());
      if (rBean != null) {
        bean.setRole(rBean);
      } else {
        StringBuilder buf = new StringBuilder("variable attribute role with id ");
        buf.append(bean.getRolePk());
        buf.append(" not found in list of valid variable attribute roles with ids ");
        buf.append(map.keySet().toString());
        throw new DataAccessException(buf.toString());
      }
    }
  }

  /**
   * get the first label that is marked as preferred
   * 
   * @return the preferred label
   */
  public String getLabel() {
    for (VariableAttributeBean vab : attributes) {
      if (vab.isLabel()) {
        if (vab.getIsPrimaryAttribute()) {
          return vab.getValue();
        }
      }
    }
    return null;
  }

  /**
   * @return the json string
   */
  public String getLocalesJson() {
  	return locales == null ? "{}" : locales.data();
  }

  /**
   * get language key of element
   * 
   * @param keyElement the id of the element
   * @return the string variable. + the id of the element
   */
  @Deprecated
  public static final String getLangKey(Integer keyElement) {
    return new StringBuilder("variable.").append(keyElement).toString();
  }

  /**
   * get pk or null
   * 
   * @return variableusage pk or null
   */
  public Integer getVariableusagePk() {
    return variableusage == null ? null : variableusage.getPk();
  }

  /**
   * @return the varOrder
   */
  public Integer getVarOrder() {
    return varOrder;
  }

  /**
   * @param varOrder the varOrder to set
   */
  public void setVarOrder(Integer varOrder) {
    this.varOrder = varOrder;
  }

  /**
   * @return the keyParentElement
   */
  public Integer getKeyParentElement() {
    return keyParentElement;
  }

  /**
   * @param keyParentElement the keyParentElement to set
   */
  public void setKeyParentElement(Integer keyParentElement) {
    this.keyParentElement = keyParentElement;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the json string
   */
  public String getTranslationJson() {
  	return translation == null ? "{}" : translation.data();
  }
  
  /**
   * @param json the json string
   */
  public void setTranslationJson(String json) {
  	this.translation = json == null ? JSONB.valueOf("{}") : JSONB.valueOf(json);
  }
  
  /**
   * @return the translation
   */
  public JSONB getTranslation() {
    return translation;
  }

  /**
   * @param translation the translation to set
   */
  public void setTranslation(JSONB translation) {
    this.translation = translation;
  }

  /**
   * @return the rightId
   */
  public Integer getRightId() {
    return rightId;
  }

  /**
   * @param rightId the rightId to set
   */
  public void setRightId(Integer rightId) {
    this.rightId = rightId;
  }

  /**
   * @return the valuelist
   */
  public String getValuelist() {
    return valuelist;
  }

  /**
   * @param valuelist the valuelist to set
   */
  public void setValuelist(String valuelist) {
    this.valuelist = valuelist;
  }

  /**
   * @return the metadatalist
   */
  public List<ConfounderBean> getMetadatalist() {
    return metadatalist;
  }

  /**
   * @param metadatalist the metadatalist to set
   */
  public void setMetadatalist(List<ConfounderBean> metadatalist) {
    this.metadatalist = metadatalist;
  }

  /**
   * @return the controltype
   */
  public EnumControlvar getControltype() {
    return controltype;
  }

  /**
   * @param controltype the controltype to set
   */
  public void setControltype(EnumControlvar controltype) {
    this.controltype = controltype;
  }

  /**
   * @return the study
   */
  public String getStudy() {
    return study;
  }

  /**
   * @param study the study to set
   */
  public void setStudy(String study) {
    this.study = study;
  }

  /**
   * @return the studygroup
   */
  public String getStudygroup() {
    return studygroup;
  }

  /**
   * @param studygroup the studygroup to set
   */
  public void setStudygroup(String studygroup) {
    this.studygroup = studygroup;
  }

  /**
   * @return the acl
   */
  public AclBean getAcl() {
    return acl;
  }

  /**
   * @param acl the acl to set
   */
  public void setAcl(AclBean acl) {
    this.acl = acl;
  }

  /**
   * @return the keyElement
   */
  public Integer getKeyElement() {
    return keyElement;
  }

  /**
   * @return the shipOrder
   */
  public Integer getShipOrder() {
    return shipOrder;
  }

  /**
   * @return the uniqueName
   */
  public String getUniqueName() {
    return uniqueName;
  }

  /**
   * @param uniqueName the uniqueName to set
   */
  public void setUniqueName(String uniqueName) {
    this.uniqueName = uniqueName;
  }

  /**
   * @return the variableusage
   */
  public VariableusageBean getVariableusage() {
    return variableusage;
  }

  /**
   * @param variableusage the variableusage to set
   */
  public void setVariableusage(VariableusageBean variableusage) {
    this.variableusage = variableusage;
  }

  /**
   * @return the column name
   */
  public String getColumnName() {
    return columnName;
  }

  /**
   * @param columnName the column name
   */
  public void setColumnName(String columnName) {
    this.columnName = columnName;
  }

  /**
   * @return the attributes
   */
  public List<VariableAttributeBean> getAttributes() {
    return attributes;
  }

  /**
   * @return the datatype
   */
  public String getDatatype() {
    return datatype;
  }

  /**
   * @param datatype the datatype to set
   */
  public void setDatatype(String datatype) {
    this.datatype = datatype;
  }

  /**
   * @return the idvariable
   */
  public Boolean getIdvariable() {
    return idvariable;
  }

  /**
   * @param idvariable the idvariable to set
   */
  public void setIdvariable(Boolean idvariable) {
    this.idvariable = idvariable;
  }

  /**
   * @return the fkMissinglist
   */
  public Integer getFkMissinglist() {
    return fkMissinglist;
  }

  /**
   * @param fkMissinglist the fkMissinglist to set
   */
  public void setFkMissinglist(Integer fkMissinglist) {
    this.fkMissinglist = fkMissinglist;
  }

  /**
   * @return the fkVariableusage
   */
  public Integer getFkVariableusage() {
    return fkVariableusage;
  }

  /**
   * @param fkVariableusage the fkVariableusage to set
   */
  public void setFkVariableusage(Integer fkVariableusage) {
    this.fkVariableusage = fkVariableusage;
  }

	/**
	 * @return the locales
	 */
	public JSONB getLocales() {
		return locales;
	}
}
