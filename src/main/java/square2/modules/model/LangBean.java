package square2.modules.model;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 * 
 * @author henkej
 *
 */
public class LangBean {

	private Map<String, Map<String, String>> map;

	/**
	 * create a new lang bean
	 */
	public LangBean() {
		this.map = new HashMap<>();
	}

	/**
	 * create a new lang bean with predefined locales
	 * 
	 * @param locales
	 *          the locales
	 */
	public LangBean(SquareLocale[] locales) {
		this.map = new HashMap<>();
		for (SquareLocale locale : locales) {
			map.put(locale.get(), new HashMap<>());
		}
	}

	/**
	 * add the properties of this locale to the translations
	 * 
	 * @param locale
	 *          the locale
	 * @param properties
	 *          the properties
	 */
	public void addAll(SquareLocale locale, Properties properties) {
		Map<String, String> current = get(locale); // out of for loop to be a bit faster
		if (current == null) {
			current = new HashMap<>();
			put(locale, current);
		}
		for (Object pKey : properties.keySet()) {
			String key = (String) pKey;
			String value = properties.getProperty(key);
			current.put(key, value);
		}
	}

	/**
	 * put the map of this locale to the translations
	 * 
	 * @param locale
	 *          the locale
	 * @param map
	 *          the map
	 */
	public void put(SquareLocale locale, Map<String, String> map) {
		this.map.put(locale.get(), map);
	}

	/**
	 * get all translations for the locale
	 * 
	 * @param locale
	 *          the locale
	 * @return the translations
	 */
	public Map<String, String> get(SquareLocale locale) {
		return this.map.get(locale.get());
	}

	/**
	 * get all translations for the locale
	 * 
	 * @param locale
	 *          the locale
	 * @return the translations
	 */
	public Map<String, String> get(String locale) {
		return this.map.get(locale);
	}

	/**
	 * add a translation
	 * 
	 * @param locale
	 *          the locale
	 * @param key
	 *          the key
	 * @param value
	 *          the value
	 */
	public void add(SquareLocale locale, String key, String value) {
		Map<String, String> map = get(locale);
		String mapKey = LangKey.generateKey(key);
		map.put(mapKey, value);
	}

	/**
	 * add a translation
	 * 
	 * @param locale
	 *          the locale
	 * @param key
	 *          the key
	 * @param value
	 *          the value
	 */
	public void add(SquareLocale locale, Integer key, String value) {
		Map<String, String> map = get(locale);
		String mapKey = LangKey.generateKey(key);
		map.put(mapKey, value);
	}

	/**
	 * add a translation
	 * 
	 * @param locale
	 *          the locale
	 * @param key
	 *          the key
	 * @param value
	 *          the value
	 */
	public void add(String locale, Integer key, String value) {
		Map<String, String> map = get(locale);
		String mapKey = LangKey.generateKey(key);
		map.put(mapKey, value);
	}

	/**
	 * add a translation
	 * 
	 * @param locale
	 *          the locale
	 * @param key
	 *          the key
	 * @param value
	 *          the value
	 */
	public void add(String locale, String key, String value) {
		Map<String, String> map = get(locale);
		String mapKey = LangKey.generateKey(key);
		map.put(mapKey, value);
	}

	/**
	 * @return the translations map
	 */
	public Map<String, Map<String, String>> getMap() {
		return map;
	}

	/**
	 * @param map
	 *          the map to set
	 */
	public void setMap(Map<String, Map<String, String>> map) {
		this.map = map;
	}

	/**
	 * set values of this langBean
	 * 
	 * @param bean
	 *          the bean
	 */
	public void set(LangBean bean) {
		this.map = bean.getMap();
	}
	
	/**
	 * add the bean to the translations map
	 * 
	 * @param bean the bean
	 */
	public void add(LangBean bean) {
		for (String key : bean.getMap().keySet()) {
			Map<String, String> map = this.map.get(key);
			if (map == null) {
				map = bean.getMap().get(key);
				this.map.put(key, map);
			} else {
				map.putAll(bean.getMap().get(key));
			}
		}
	}
}
