package square2.modules.model;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.servlet.http.Part;

/**
 * 
 * @author henkej
 * 
 */
@Named
@SessionScoped
public class ListBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private String input;
	private String mean;
	private String median;
	private Part file;

	/**
	 * @return the input
	 */
	public String getInput() {
		return input;
	}

	/**
	 * @param input
	 *          the input to set
	 */
	public void setInput(String input) {
		this.input = input;
	}

	/**
	 * @return the mean
	 */
	public String getMean() {
		return mean;
	}

	/**
	 * @param mean
	 *          the mean to set
	 */
	public void setMean(String mean) {
		this.mean = mean;
	}

	/**
	 * @return the median
	 */
	public String getMedian() {
		return median;
	}

	/**
	 * @param median
	 *          the median to set
	 */
	public void setMedian(String median) {
		this.median = median;
	}

	/**
	 * @return the file
	 */
	public Part getFile() {
		return file;
	}

	/**
	 * @param file
	 *          the file to set
	 */
	public void setFile(Part file) {
		this.file = file;
	}
}
