package square2.modules.model;

import ship.jsf.components.selectFilterableMenu.BeanWrapperInterface;

/**
 * 
 * @author henkej
 *
 */
public class ProfileFilterWrapper implements BeanWrapperInterface {
	private final ProfileBean bean;

	/**
	 * create new profile bean wrapper
	 * 
	 * @param bean
	 *          the profile bean
	 */
	public ProfileFilterWrapper(ProfileBean bean) {
		this.bean = bean;
	}

	@Override
	public Integer getKey() {
		return bean.getUsnr();
	}

	@Override
	public String getValue() {
		return bean.getFullname();
	}
}
