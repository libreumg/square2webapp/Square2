package square2.modules.model;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;

/**
 * 
 * @author henkej
 * 
 */
public class ProfileBean implements Serializable, Comparable<ProfileBean> {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(ProfileBean.class);

	private String forename;
	private String surname;
	private String username;
	private Integer usnr;
	private Date expireOn;
	private List<PrivilegeBean> privileges;
	private List<ProfileConfigBean> config;
	private String keycloakAccessToken;
	private KeyCloakDelegator keyCloakDelegator;

	/**
	 * create new profile bean
	 */
	public ProfileBean() {
		config = new ArrayList<>();
		privileges = new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append(forename).append(" ").append(surname).append(" (").append(username);
		buf.append(", ").append(usnr).append(", expire on ");
		buf.append(expireOn == null ? "null" : new SimpleDateFormat("dd.MM.yyyy HH:mm").format(expireOn));
		buf.append("), using keycloak = ").append(hasKeycloakAccessToken()).append(", ");
		buf.append(privileges).append(config);
		return buf.toString();
	}

	@Override
	public int compareTo(ProfileBean o) {
		return surname == null || o == null || o.getSurname() == null ? 0 : surname.compareTo(o.getSurname());
	}

	/**
	 * @param role the keycloak role
	 * @return true or false
	 */
	public boolean hasKeycloakRole(String role) {
		boolean result = false;
		for (PrivilegeBean bean : privileges) {
			if (bean != null && bean.getName().equals(role)) {
				result = true;
			}
		}
		return result;
	}

	/**
	 * check if current user has privilege
	 * 
	 * @param privilegeName the name of the privilege
	 * @return true or false
	 */
	public boolean hasPrivilege(String privilegeName) {
		for (PrivilegeBean bean : privileges) {
			if (bean == null) {
				LOGGER.fatal("PrivilegeBean in session's privileges is null");
			} else if (privilegeName == null) {
				LOGGER.fatal("requested privilegeName in xhtml-file is null");
			} else if (privilegeName.equals(bean.getName())) {
				return true;
			} // backwards compatibility
			else if (new StringBuilder("role.square.").append(privilegeName).toString().equals(bean.getName())) {
				return true;
			}
		}
		return false;
	}

	/**
	 * also set the access token
	 * 
	 * @param keyCloakDelegator the keycloak delegator
	 */
	public void setKeyCloakDelegator(KeyCloakDelegator keyCloakDelegator) {
		this.keyCloakDelegator = keyCloakDelegator;
		this.keycloakAccessToken = keyCloakDelegator.login();
	}

	/**
	 * set roles from keycloak
	 * 
	 * @param roles the roles
	 * @param map the map of role as name to fkPrivilege
	 */
	public void setRoles(List<String> roles, Map<String, Integer> map) {
		for (String role : roles) {
			privileges.add(new PrivilegeBean(map.get(role), role, EnumAccesslevel.rwx));
		}
	}

	/**
	 * check if this user has the role <b>role.square.admin</b>
	 * 
	 * @return true or false
	 */
	public boolean isAdmin() {
		return hasPrivilege("role.square.admin") || hasKeycloakRole("admin");
	}

	/**
	 * check if the keycloak access token has been set
	 * 
	 * @return true if keycloakAccessToken is not null
	 */
	public boolean hasKeycloakAccessToken() {
		return keycloakAccessToken != null;
	}

	/**
	 * check if the user has one of the privileges
	 * 
	 * @param privileges the array of privileges
	 * @return true or false
	 */
	public boolean hasOneOfPrivileges(String... privileges) {
		for (String privilege : privileges) {
			if (hasPrivilege(privilege) || hasKeycloakRole(privilege)) {
				return true;
			} // otherwise, seek on, therefore do not simply return hasPrivilege(privilege)
		}
		return false;
	}

	/**
	 * @return get full name of profile user
	 */
	public String getFullname() {
		StringBuilder buf = new StringBuilder();
		buf.append(forename);
		buf.append(" ");
		buf.append(surname);
		return buf.toString();
	}

	/**
	 * get the configuration for key
	 * 
	 * @param key the key
	 * @return the configuration value or null
	 */
	public Object getConfig(String key) {
		for (ProfileConfigBean bean : config) {
			if (bean.getKey().equals(key)) {
				return bean.getValue();
			}
		}
		return null;
	}

	/**
	 * @param key the key
	 * @param defaultValue the default value
	 * @return the value if found, defaultValue on errors
	 */
	public Integer getConfigInteger(String key, Integer defaultValue) {
		Object o = getConfig(key);
		if (o instanceof Integer) {
			return (Integer) o;
		} else if (o instanceof String) {
			try {
				return Integer.valueOf((String) o);
			} catch (NumberFormatException e) {
				return defaultValue;
			}
		} else {
			return defaultValue;
		}
	}

	/**
	 * get the configuration for key
	 * 
	 * @param key the key
	 * @param defaultValue the default value
	 * @return the value or the default value
	 */
	public Object getConfig(String key, Object defaultValue) {
		Object value = getConfig(key);
		return value == null ? defaultValue : value;
	}

	/**
	 * add the key value pair to to configuration
	 * 
	 * @param key the key
	 * @param value the value
	 */
	public void putConfig(String key, Object value) {
		for (ProfileConfigBean bean : config) {
			if (bean.getKey().equals(key)) {
				bean.setValue(value);
			}
		}
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename the forename
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname the surname
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username the username
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the privileges
	 */
	public List<PrivilegeBean> getPrivileges() {
		return privileges;
	}

	/**
	 * @return the id of the user
	 */
	public Integer getUsnr() {
		return usnr;
	}

	/**
	 * @param usnr the id of the user
	 */
	public void setUsnr(Integer usnr) {
		this.usnr = usnr;
	}

	/**
	 * @return the configuration
	 */
	public List<ProfileConfigBean> getConfig() {
		return config;
	}

	/**
	 * @return the login expiration date
	 */
	public Date getExpireOn() {
		return expireOn;
	}

	/**
	 * @param expireOn the login expiration date
	 */
	public void setExpireOn(Date expireOn) {
		this.expireOn = expireOn;
	}

	/**
	 * @return the keycloakAccessToken
	 */
	public String getKeycloakAccessToken() {
		return keycloakAccessToken;
	}

	/**
	 * @param keycloakAccessToken the keycloakAccessToken to set
	 */
	public void setKeycloakAccessToken(String keycloakAccessToken) {
		this.keycloakAccessToken = keycloakAccessToken;
	}

	/**
	 * logout from keycloak
	 */
	public void logoutFromKeycloakIfConnected() {
		if (hasKeycloakAccessToken()) {
			this.keyCloakDelegator.logout();
		}
	}
}
