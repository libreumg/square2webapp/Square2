package square2.modules.model.template;

/**
 * 
 * @author henkej
 *
 */
public class UsageBean {
	/**
	 * label.report.generate.anamat
	 */
	public static final String REPORT = "label.report.generate.anamat";
	/**
	 * label.report.analysistemplate
	 */
	public static final String FUNCTIONLIST = "label.report.analysistemplate";
	private String name;
	private String type;

	/**
	 * create new usage bean
	 * 
	 * @param name
	 *          the name
	 * @param type
	 *          the type;
	 */
	public UsageBean(String name, String type) {
		this.name = name;
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *          the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
}
