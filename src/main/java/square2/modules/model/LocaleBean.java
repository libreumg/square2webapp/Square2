package square2.modules.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;

import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIInput;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;
import javax.servlet.ServletContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.help.ContextKey;
import square2.help.SquareFacesContext;

/**
 * 
 * @author henkej
 * 
 */
@Named(value = "lang")
@SessionScoped
public class LocaleBean implements Serializable {
	private static final Logger LOGGER = LogManager.getLogger(LocaleBean.class);

	private static final long serialVersionUID = -2920852271046380884L;

	private String localeCode;

	private static Map<String, Locale> countries;

	static {
		countries = new LinkedHashMap<>();
		countries.put("English", Locale.ENGLISH);
		countries.put("German", Locale.GERMANY);
		countries.put("Romanian", Locale.forLanguageTag("ro"));
		countries.put("Spanish", Locale.forLanguageTag("es"));
		countries.put("languagekey", Locale.CHINA); // use valid languages
	}

	/**
	 * generate new locale bean with default locale de_DE
	 */
	public LocaleBean() {
		super();
		localeCode = "de_DE"; // defaults to german
	}

	/**
	 * @param localeCode
	 *          the default locale code, e.g. de_DE
	 */
	public LocaleBean(String localeCode) {
		super();
		this.localeCode = localeCode;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{localeCode=").append(localeCode);
		buf.append(",countries=").append(countries);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * replace params and translate key
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param key
	 *          the key of the translation
	 * @param params
	 *          the parameters for that translation
	 * @return the translation if possible, the key otherwise
	 */
	public String translateParametrized(FacesContext facesContext, String key, Object... params) {
		int i = 0;
		if (key != null) {
			for (Object o : params) {
				if (o != null) {
					key = key.replaceAll("\\{" + i + "\\}",
							Matcher.quoteReplacement(translate(facesContext, null, o.toString())));
				}
				i++;
			}
		}
		return translate(facesContext, null, key);
	}

	/**
	 * translate key and replace params
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param key
	 *          the key of the translation
	 * @param params
	 *          the parameters for that translation
	 * @return the translation if possible, the key otherwise
	 */
	public String translate(FacesContext facesContext, Integer key, Object... params) {
		return translate(facesContext, key, null, params);
	}

	/**
	 * translate key and replace params
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param key
	 *          the numerical key of the translation
	 * @param name
	 *          the string key of the translation
	 * @param params
	 *          the parameters for that translation
	 * @return the translation if possible, the key otherwise
	 */
	public String translate(FacesContext facesContext, Integer key, String name, Object... params) {
		String translated = translate(facesContext, key, name);
		int i = 0;
		if (translated != null) {
			for (Object o : params) {
				if (o != null) {
					translated = translated.replaceAll("\\{" + i + "\\}",
							Matcher.quoteReplacement(translate(facesContext, null, o.toString())));
				}
				i++;
			}
		}
		return translated;
	}

	/**
	 * translate key
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param key
	 *          the key of the translation
	 * @param params
	 *          the parameters for that translation
	 * @return the translation if possible, the key otherwise
	 */
	public String translate(FacesContext facesContext, String key, Object... params) {
		return translate(facesContext, null, key, params);
	}

	/**
	 * translate key; if no translation is found (in current locale), return the key
	 * itself
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param key
	 *          the numerical key of the translation
	 * @param name
	 *          the string key of the translation
	 * @return the translation if possible, the key otherwise
	 */
	public String translate(FacesContext facesContext, Integer key, String name) {
		Map<String, String> p = null;
		String k = LangKey.generateKey(key, name);
		if (facesContext == null) {
			LOGGER.warn("facesContext is null in translate(facesContext, '{}')", k);
		} else {
			ExternalContext externalContext = facesContext.getExternalContext();
			if (externalContext == null) {
				LOGGER.warn("externalContext is null in translate(facesContext, '{}')", k);
			} else {
				ServletContext servletContext = (ServletContext) externalContext.getContext();
				if (servletContext == null) {
					LOGGER.warn("servletContext is null in translate(facesContext, '{}')", k);
				} else {
					LangBean bean = (LangBean) servletContext.getAttribute(ContextKey.TRANSLATIONS.get());
					Map<String, Map<String, String>> map = bean.getMap();
					if (map == null) {
						LOGGER.warn("attributeMap is null in translate(facesContext, '{}')", k);
					} else {
						p = map.get(localeCode);
					}
				}
			}
		}
		if (p == null) {
			LOGGER.error("locale " + localeCode + " is not supported");
			return k.toString();
		} else {
			return LocaleBean.getTranslation(p, key, name);
		}
	}

	/**
	 * @return the countries list
	 */
	public List<Locale> getCountries() {
		return new ArrayList<>(getCountriesInMap().values());
	}

	/**
	 * @return the countries map
	 */
	public Map<String, Locale> getCountriesInMap() {
		return countries;
	}

	/**
	 * @return the locale code, e.g. de_DE
	 */
	public String getLocaleCode() {
		return localeCode;
	}

	/**
	 * @return the simple locale code, e.g. de
	 */
	public String getSimpleLocaleCode() {
		return localeCode == null ? null
				: (localeCode.contains("_") ? localeCode.substring(0, localeCode.indexOf("_")) : localeCode);
	}

	/**
	 * @param localeCode
	 *          the locale code, e.g. de_DE
	 */
	public void setLocaleCode(String localeCode) {
		this.localeCode = localeCode;
	}

	/**
	 * set locale code and update view root
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param localeCode
	 *          the new locale code
	 * @return the same page
	 */
	public String setLocaleCodeTo(SquareFacesContext facesContext, String localeCode) {
		setLocaleCode(localeCode);
		for (Map.Entry<String, Locale> entry : countries.entrySet()) {
			if (entry.getValue().toString().equals(localeCode)) {
				facesContext.getViewRoot().setLocale(entry.getValue());
				facesContext.setLocaleBean(this);
			}
		}
		return "";
	}

	/**
	 * switch the current locale
	 * 
	 * @param e
	 *          the value change event
	 */
	public void countryLocaleCodeChanged(ValueChangeEvent e) {
		SquareFacesContext facesContext = (SquareFacesContext) ((UIInput) e.getSource()).getAttributes()
				.get("facesContext");
		String newLocaleValue = e.getNewValue().toString();
		for (Map.Entry<String, Locale> entry : countries.entrySet()) {
			if (entry.getValue().toString().equals(newLocaleValue)) {
				facesContext.getViewRoot().setLocale(entry.getValue());
				facesContext.setLocaleBean(this);
			}
		}
	}

	/**
	 * get the translation of the element by name, ignoring the numerical key
	 * 
	 * @param facesContext
	 *          of this current function call
	 * @param name
	 *          to be used as key
	 * @param params
	 *          to be used in translation
	 * @return the translation if found, the name otherwise
	 */
	public String translateByName(SquareFacesContext facesContext, String name, Object... params) {
		Map<String, String> p = new HashMap<>();
		if (facesContext == null) {
			LOGGER.warn("facesContext is null in translate(facesContext, '{}')", name);
		} else {
			ExternalContext externalContext = facesContext.getExternalContext();
			if (externalContext == null) {
				LOGGER.warn("externalContext is null in translate(facesContext, '{}')", name);
			} else {
				ServletContext servletContext = (ServletContext) externalContext.getContext();
				if (servletContext == null) {
					LOGGER.warn("servletContext is null in translate(facesContext, '{}')", name);
				} else {
					LangBean bean = (LangBean) servletContext.getAttribute(ContextKey.TRANSLATIONS.get());
					if (bean == null) {
						LOGGER.warn("translation bean is null or not found in servlet context (key translations)");
					} else {
						Map<String, Map<String, String>> map = bean.getMap();
						if (map == null) {
							LOGGER.warn("translation map is null in translate(facesContext, '{}')", name);
						} else {
							p = map.get(localeCode);
						}
					}
				}
			}
		}
		return getTranslation(p, null, name);
	}

	/**
	 * get the translation of the element by name, ignoring the numerical key
	 * 
	 * !!! works only if t_languagekey.name is null !!!
	 * 
	 * @param facesContext
	 *          of this current function call
	 * @param key
	 *          to be used as key
	 * @param params
	 *          to be used in translation
	 * @return the translation if found, the name otherwise
	 */
	public String translateByKey(SquareFacesContext facesContext, Integer key, Object... params) {
		Map<String, String> p = new HashMap<>();
		if (facesContext == null) {
			LOGGER.warn("facesContext is null in translate(facesContext, '{}')", key);
		} else {
			ExternalContext externalContext = facesContext.getExternalContext();
			if (externalContext == null) {
				LOGGER.warn("externalContext is null in translate(facesContext, '{}')", key);
			} else {
				ServletContext servletContext = (ServletContext) externalContext.getContext();
				if (servletContext == null) {
					LOGGER.warn("servletContext is null in translate(facesContext, '{}')", key);
				} else {
					LangBean bean = (LangBean) servletContext.getAttribute(ContextKey.TRANSLATIONS.get());
					if (bean == null) {
						LOGGER.warn("translation bean is null or not found in servlet context (key translations)");
					} else {
						Map<String, Map<String, String>> map = bean.getMap();
						if (map == null) {
							LOGGER.warn("attributeMap is null in translate(facesContext, '{}')", key);
						} else {
							p = map.get(localeCode);
						}
					}
				}
			}
		}
		return getTranslation(p, key, null);
	}

	/**
	 * get the translation of the key or name in map
	 * 
	 * !!! works only if t_languagekey.name is null !!!
	 * 
	 * @param map
	 *          contains all translations
	 * @param key
	 *          is the integer value of a possible json string
	 * @param name
	 *          is the string value of a possible json string
	 * @return the translation if found, the key otherwise
	 */
	public static final String getTranslation(Map<String, String> map, Integer key, String name) {
		String langKey = LangKey.generateKey(key, name);
		return map.containsKey(langKey) ? map.get(langKey) : langKey;
	}
	
	/**
	 * get locale of current localeCode
	 * 
	 * @return the locale; if any error occurs, return Locale.ENGLISH
	 */
	public Locale getLocale() {
	  Locale defaultLocale = Locale.ENGLISH;
	  try {
	    if ("de_DE".equals(localeCode)) {
	      return Locale.GERMAN;
	    } else if ("en".contentEquals(localeCode)) {
	      return Locale.ENGLISH;
	    } else {
	      return defaultLocale;
	    }
	  } catch (Exception e) {
	    LOGGER.warn("error on looking up locale {}: {}", localeCode, e.getMessage());
	    return defaultLocale;
	  }
	}
}
