package square2.modules.model.statistic;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.inject.Named;
import javax.servlet.http.Part;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.myfaces.shared.renderkit.html.util.HttpPartWrapper;
import org.jooq.exception.DataAccessException;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;

import de.ship.dbppsquare.square.tables.records.TScaleRecord;
import square2.db.control.StatisticGateway;
import square2.help.SquareFacesContext;
import square2.modules.control.help.ValidationException;
import square2.modules.control.r.RServerCall;
import square2.modules.model.AclBean;
import square2.modules.model.GroupPrivilegeBean;
import square2.modules.model.ModelException;
import square2.modules.model.SquareModel;
import square2.modules.model.UserAccessLevelBean;
import square2.modules.model.admin.FunctioncategoryBean;
import square2.modules.model.admin.UserBean;
import square2.modules.model.admin.UsergroupBean;
import square2.r.control.FunctionRegistratorGateway;
import square2.r.control.RGateway;
import square2.r.control.SmiGateway;
import square2.r.model.RPackage;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class StatisticModel extends SquareModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger("Square2");

	private HttpPartWrapper uploadFile;
	private Integer reference;
	private Integer function;
	private Boolean activated;
	private String release;
	private String functionCall;
	private UserAccessLevelBean newUser;
	private TScaleRecord functionscale;
	private FunctionBean functionBean;
	private FunctionBean testFunction;
	private FunctionBean currentFunction;
	private List<FunctionBean> dataModel;
	private FunctionInputBean functionInputBean;
	private FunctionOutputBean functionOutputBean;
	private List<FunctionBean> references;
	private List<FunctioncategoryBean> categories;
	private List<FunctionBean> functions;
	private List<String> functionLines;
	private List<String> testResultLines;
	private List<FunctionRightBean> functionRights;
	private List<UserAccessLevelBean> rights;
	private String ajaxInputMessage;
	private String ajaxOutputMessage;
	private List<FunctioninputtypeBean> inputtypes;
	private List<FunctionoutputtypeBean> outputtypes;
	private List<UserBean> users;

	private List<GroupPrivilegeBean> groupPrivileges;
	private List<UsergroupBean> allGroups;
	private Integer newGroup;
	private Integer newUsnr;
	private AclBean newAcl;
	private GroupPrivilegeBean activePrivilege;

	private String packageName;
	private String baseUrl;
	private String isocodes;

	private List<RPackage> packages;

	private String performance;

	@Override
	public void setAllSquareUsers(SquareFacesContext facesContext, Object... params)
			throws ModelException, DataAccessException {
		if (params == null || params.length < 1 || (!(params[0] instanceof Integer))) {
			throw new ModelException("params is null, < 1 or not an instance of Integer");
		}
		resetAllSquareusers(new StatisticGateway(facesContext).getAllSquareUsersWithoutUserPrivilege((Integer) params[0]));
	}

	/**
	 * reload
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean reload(SquareFacesContext facesContext) {
		dataModel = new ArrayList<>();
		categories = new ArrayList<>();
		functionRights = new ArrayList<>();
		inputtypes = new ArrayList<>();
		try {
			StatisticGateway gw = new StatisticGateway(facesContext);
			dataModel = gw.getAllFunctions(false);
			categories = gw.getFunctioncategories(null);
			functionRights = gw.getAllFunctionRights();
			inputtypes = gw.getAllInputtypes();
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * add
	 * 
	 * @param facesContext the context of this function call
	 * @return true
	 */
	public boolean add(SquareFacesContext facesContext) {
		functionBean = null;
		functionInputBean = null;
		functionOutputBean = null;
		functionscale = new TScaleRecord();
		loadFunctionOutputtypes(facesContext);
		return true;
	}

	private void loadFunctionOutputtypes(SquareFacesContext facesContext) {
		try {
			outputtypes = new StatisticGateway(facesContext).getAllFunctionoutputtypes();
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			outputtypes = new ArrayList<>();
		}
	}

	/**
	 * prepare the add panel
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean toAdd(SquareFacesContext facesContext) {
		try {
			StatisticGateway gw = new StatisticGateway(facesContext);
			List<FunctionScale> scales = gw.getAllFunctionscales();
			references = gw.getAllFunctions(true);
			categories = gw.getFunctioncategories(null);
			// keep when coming from [add|remove]Function[Input|Output|Reference]
			functionBean = functionBean == null ? new FunctionBean(null, null, true, scales) : functionBean;
			functionInputBean = functionInputBean == null ? new FunctionInputBean(null) : functionInputBean;
			functionOutputBean = functionOutputBean == null ? new FunctionOutputBean(null) : functionOutputBean;
			functionscale = functionscale == null ? new TScaleRecord() : functionscale;
			references = new ArrayList<>();
			categories = new ArrayList<>();
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * load performance
	 * 
	 * @param facesContext the context of the function call
	 */
	public void toPerformance(SquareFacesContext facesContext) {
		performance = new RServerCall(facesContext).callPerformance();

		ExternalContext context = facesContext.getExternalContext();
		context.responseReset();
		context.setResponseBufferSize((int) performance.length());
		context.setResponseContentType("text/html");
		context.setResponseHeader("Content-Disposition", "inline");
		try {
			context.getResponseOutputWriter().write(performance);
		} catch (IOException e) {
			facesContext.notifyError(e.getMessage());
		}
		facesContext.responseComplete();
	}

	/**
	 * check if the current user can edit this function
	 * 
	 * @param functionId the id of the function
	 * @param activated the activated flag
	 * @param isAdmin the admin flag
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public Boolean getCanEdit(Integer functionId, Boolean activated, Boolean isAdmin, SquareFacesContext facesContext) {
		Boolean checkResult = isAdmin || getUserRightLevel(functionId, facesContext.getUsnr()).getWrite();
		checkResult = !activated && checkResult;
		return checkResult;
	}

	/**
	 * load the edit panel
	 * 
	 * @param functionId the function id
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean loadEditPageContent(Integer functionId, SquareFacesContext facesContext) {
		// keep when coming from [add|remove]Function[Input|Output|Reference]
		functionInputBean = functionInputBean == null ? new FunctionInputBean(null) : functionInputBean;
		functionOutputBean = functionOutputBean == null ? new FunctionOutputBean(null) : functionOutputBean;
		try {
			StatisticGateway gw = new StatisticGateway(facesContext);
			functionBean = functionBean == null ? gw.getFunctionBean(functionId) : functionBean;
			references = gw.getAllFunctions(true);
			categories = gw.getFunctioncategories(null);
			loadFunctionOutputtypes(facesContext);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * force a reload
	 * 
	 * @return true
	 */
	public boolean editForceReload() {
		functionBean = null;
		functionInputBean = null;
		return true;
	}

	/**
	 * prepare the test panel
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean toTest(SquareFacesContext facesContext) {
		try {
			StatisticGateway gw = new StatisticGateway(facesContext);
			List<FunctionScale> scales = gw.getAllFunctionscales();
			testFunction = new FunctionBean(null, null, true, scales);
			functions = new StatisticGateway(facesContext).getAllFunctions(true);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * prepare the privileges panel
	 * 
	 * @param functionId the function id
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean toRights(Integer functionId, SquareFacesContext facesContext) {
		try {
			StatisticGateway gw = new StatisticGateway(facesContext);
			rights = gw.getAllFunctionRights(functionId);
			FunctionBean creatorFunctionBean = gw.getFunctionCreator(functionId);
			functionBean.setCreator(creatorFunctionBean.getCreator());
			newUser = new UserAccessLevelBean();
			setAllSquareUsers(facesContext, functionBean.getRight());
			users = gw.getAllSquareUsersWithoutUserPrivilege(null);
			newGroup = Integer.valueOf(0);
			newAcl = new AclBean().setString("rwx");
			groupPrivileges = gw.getAllGroupPrivileges(functionBean.getRight());
			allGroups = gw.getAllGroupsExceptOf(groupPrivileges);
			return true;
		} catch (DataAccessException | ModelException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * set user privilege due to definition; read and write is atomic, and read and
	 * execute also
	 * 
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean addUserGroupPrivilege(SquareFacesContext facesContext) {
		try {
			List<Integer> privileges = new ArrayList<>();
			privileges.add(functionBean.getRight());
			Integer affected = new StatisticGateway(facesContext).addUserGroupPrivilege(newUsnr, newGroup, privileges,
					newAcl);
			facesContext.notifyInformation("info.affected.add", affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * edit privileges
	 * 
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean editPrivilege(SquareFacesContext facesContext) {
		try {
			Integer affected = new StatisticGateway(facesContext).upsertUserGroupPrivilege(activePrivilege);
			facesContext.notifyInformation("info.affected.add", affected);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * delete privilege from db
	 * 
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean deletePrivilege(SquareFacesContext facesContext) {
		try {
			GroupPrivilegeBean bean = activePrivilege;
			Set<Integer> privileges = new HashSet<>();
			privileges.add(bean.getFkPrivilege());
			Integer affected = new StatisticGateway(facesContext).removeUserGroupPrivilege(bean.getFkUsnr(),
					bean.getFkGroup(), privileges);
			facesContext.notifyInformation("info.affected.remove", affected);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * change the creator of a function
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean switchCreator(SquareFacesContext facesContext) {
		try {
			new StatisticGateway(facesContext).switchCreator(functionBean.getFunctionId(),
					functionBean.getCreator().getUsnr());
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	protected Set<Integer> checkFunctionHasNoCallLoops(FunctionBean bean, Set<Integer> set) throws DataAccessException {
		if (set.contains(bean.getFunctionId())) {
			throw new DataAccessException(
					"recursive function call detected, aborting. Name of duplicated function is " + bean.getName());
		} else {
			set.add(bean.getFunctionId());
			if (bean.getReference() != null) {
				for (FunctionBean f : bean.getReference()) {
					set.addAll(checkFunctionHasNoCallLoops(f, set));
				}
			}
		}
		return set;
	}

	/**
	 * update functionBean in the data base
	 * 
	 * @param facesContext the context of this function call
	 * @return true if update was successful, false otherwise
	 */
	public boolean updateFunction(SquareFacesContext facesContext) {
		try {
			String functionName = functionBean.getName();
			if (functionBean.getSummary() == null || functionBean.getSummary().trim().equals("")) {
				facesContext.notifyError("error.missing.input", "label.statistic.summary");
				return false;
			}
			checkFunctionHasNoCallLoops(functionBean, new HashSet<Integer>());
			new StatisticGateway(facesContext).updateFunction(functionBean);
			facesContext.notifyInformation("statistic.info.function.updated", functionName);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * add a function
	 * 
	 * @param dest panel destination
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean addFunction(String dest, SquareFacesContext facesContext) {
		if (functionBean == null) {
			facesContext.notifyError("functionBean is null in StatisticModel.addFunction");
			return false;
		} else if (functionBean.getName() == null || functionBean.getName().trim().equals("")) {
			facesContext.notifyError("error.missing.input", "label.statistic.functionname");
			return false;
		} else if (functionBean.getVarname() == null || functionBean.getVarname().trim().equals("")) {
			facesContext.notifyError("error.missing.input", "label.statistic.varname");
			return false;
		} else if (functionBean.getSummary() == null || functionBean.getSummary().trim().equals("")) {
			facesContext.notifyError("error.missing.input", "label.statistic.summary");
			return false;
		}
		try {
			String functionName = functionBean.getName();
			checkFunctionHasNoCallLoops(functionBean, new HashSet<Integer>());
			function = new StatisticGateway(facesContext).addFunction(functionBean);
			functionInputBean = null;
			functionOutputBean = null;
			functionscale = null;
			facesContext.notifyInformation("statistic.info.function.added", functionName);
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
		return true;
	}

	/**
	 * delete a function
	 * 
	 * @param functionId the id of the function
	 * @param rightId the id of the privilege
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean deleteFunction(Integer functionId, Integer rightId, SquareFacesContext facesContext) {
		try {
			if (rightId == null) {
				throw new DataAccessException("rightId must not be null");
			}
			new StatisticGateway(facesContext).deleteFunction(functionId, rightId);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
		return true;
	}

	/**
	 * add a function input
	 * 
	 * @param facesContext the context of this function call
	 * @throws ValidationException if a validation error occurs
	 */
	public void addFunctionInput(SquareFacesContext facesContext) throws ValidationException {
		if (functionInputBean.getName() == null || functionInputBean.getName().isBlank()) {
			throw new ValidationException(facesContext.translateByNamedKey("error.mustnotbeempty", "label.name"));
		} else if (functionInputBean.getDescription() == null || functionInputBean.getDescription().isBlank()) {
			throw new ValidationException(facesContext.translateByNamedKey("error.mustnotbeempty", "label.description"));
		}
		Boolean valid = true;
		for (FunctionInputBean b : functionBean.getInput()) {
			if (b.getName().equals(functionInputBean.getName())) {
				valid = false;
			}
		}
		if (valid) {
			functionBean.getInput().add(functionInputBean);
			functionInputBean = new FunctionInputBean(null);
		} else {
			throw new ValidationException("name is already in use");
		}
	}

	/**
	 * add a function output
	 * 
	 * @param facesContext the context of this function call
	 * @throws ValidationException if a validation error occurs
	 */
	public void addFunctionOutput(SquareFacesContext facesContext) throws ValidationException {
		if (functionOutputBean.getName() == null || functionOutputBean.getName().isEmpty()) {
			throw new ValidationException(
					facesContext.translate(null, "error.mustnotbeempty", "label.statistic.help.functionname"));
		}
		Boolean valid = true;
		for (FunctionOutputBean b : functionBean.getOutput()) {
			if (b.getName().equals(functionOutputBean.getName())) {
				valid = false;
			}
		}
		if (valid) {
			functionBean.getOutput().add(functionOutputBean);
			functionOutputBean = new FunctionOutputBean(null);
		} else {
			throw new ValidationException("name is already in use");
		}
	}

	/**
	 * remove a function input
	 * 
	 * @param bean the function input bean
	 */
	public void removeFunctionInput(FunctionInputBean bean) {
		Iterator<FunctionInputBean> i = functionBean.getInput().iterator();
		while (i.hasNext()) {
			FunctionInputBean current = i.next();
			if (current.equals(bean)) {
				i.remove();
			}
		}
	}

	/**
	 * remove a function output
	 * 
	 * @param output the output name
	 */
	public void removeFunctionOutput(String output) {
		Iterator<FunctionOutputBean> i = functionBean.getOutput().iterator();
		while (i.hasNext()) {
			FunctionOutputBean current = i.next();
			if (current.getName().equals(output)) {
				i.remove();
			}
		}
	}

	/**
	 * add a function reference
	 */
	public void addFunctionReference() {
		for (FunctionBean bean : references) {
			if (bean.getFunctionId().equals(reference)) {
				functionBean.getReference().add(bean);
			}
		}
		reference = null;
	}

	/**
	 * remove a function reference
	 * 
	 * @param functionId the function id
	 */
	public void removeFunctionReference(Integer functionId) {
		Iterator<FunctionBean> i = functionBean.getReference().iterator();
		while (i.hasNext()) {
			FunctionBean current = i.next();
			if (current.getFunctionId().equals(functionId)) {
				i.remove();
			}
		}
	}

	/**
	 * set the function activated state
	 * 
	 * @param functionId the id of the function
	 * @param activated the activated flag
	 * @param functionName the function name
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean doSetState(Integer functionId, Boolean activated, String functionName,
			SquareFacesContext facesContext) {
		try {
			new StatisticGateway(facesContext).setFunctionActivated(functionId, activated);
			facesContext.notifyInformation("statisticadmin.info.functionstatus.updated", functionName,
					activated ? "label.statisticadmin.allowed" : "label.statisticadmin.forbidden");
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * create default entries for the input mask
	 */
	public void initFunctionRegistrator() {
		this.packageName = "dataquieR";
		this.baseUrl = "https://dataquality.ship-med.uni-greifswald.de/";
		this.isocodes = "c(\"de_DE\", \"en\")";
	}

	/**
	 * call the functionregistrator import routine
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean doCallFunctionRegistrator(SquareFacesContext facesContext) {
		StringBuilder buf = new StringBuilder("list(");
		buf.append("forename = \"").append(facesContext.getProfile().getForename());
		buf.append("\", surname = \"").append(facesContext.getProfile().getSurname());
		buf.append("\", username = \"").append(facesContext.getUsername()).append("\")");
		String configFile = facesContext.getParam("squaredbconnectorfile", "/etc/squaredbconnector.properties");
		String configSection = facesContext.getParam("squaredbconnectorsection", "prod");
		try (FunctionRegistratorGateway gw = new FunctionRegistratorGateway(facesContext)) {
			String result = gw.callRegisterFunctions(configFile, configSection, buf.toString(), this.packageName,
					this.baseUrl, this.isocodes);
			if (result != null) {
				facesContext.notifyInformation(result);
			}
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * refresh current function
	 * 
	 * @param functionId the function id
	 */
	public void refreshCurrentFunction(Integer functionId) {
		for (FunctionBean bean : functions) {
			if (bean.getFunctionId().equals(functionId)) {
				currentFunction = bean;
				activated = currentFunction.getActivated();
			}
		}
	}

	/**
	 * get the user privileges
	 * 
	 * @param functionId the id of the function
	 * @param usnr the id of the user
	 * @return the acl bean
	 */
	public AclBean getUserRightLevel(Integer functionId, Integer usnr) {
		for (FunctionRightBean bean : functionRights) {
			if (bean.getFunction().getFunctionId().equals(functionId)) {
				if (bean.getFunction().getCreator().getUsnr().equals(usnr)) {
					return new AclBean(true, true, true); // creator has all rights by default
				} else {
					for (UserAccessLevelBean u : bean.getUsers()) {
						if (u != null && usnr.equals(u.getUsnr())) {
							return u.getAcl();
						}
					}
				}
			}
		}
		return new AclBean(false, false, false); // no such right
	}

	/**
	 * @return the function types
	 */
	public List<String> getFunctiontypes() {
		return EFunctiontype.getStringList();
	}

	/**
	 * download a function
	 * 
	 * @param facesContext the context of this function call
	 * @param functionId the id of the function
	 * @return true or false
	 */
	public boolean downloadFunction(SquareFacesContext facesContext, Integer functionId) {
		try (SmiGateway gw = new SmiGateway(facesContext)) {
			FunctionBean bean = new StatisticGateway(facesContext).getFunctionBean(functionId);
			StringBuilder filename = new StringBuilder(bean.getName()).append(".R");
			String content = gw.exportRfunction(functionId);
			generateDownloadStreamFromString(facesContext, content, filename.toString(), "text");
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * export a function
	 * 
	 * @param facesContext the context of this function call
	 * @param functionId the id of the function
	 * @return true or false
	 */
	public boolean exportFunction(SquareFacesContext facesContext, Integer functionId) {
		try (SmiGateway gw = new SmiGateway(facesContext)){
			byte[] content = gw.exportRfunctionAsRData(functionId);
			String filename = new StringBuilder("square_function_").append(functionId).append(".RData").toString();
			generateDownloadStreamFromByteArray(facesContext, content.length, filename, "application/x-binary", content);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * upload a function
	 * 
	 * @param facesContext the context of this function call
	 * @return the new function bean of the uploaded function
	 */
	public FunctionBean uploadFunction(SquareFacesContext facesContext) {
		Part part = uploadFile.getWrapped();
		try (SmiGateway gw = new SmiGateway(facesContext)){
			InputStream is = part.getInputStream();
			Integer functionId = gw.importRfunctionFromRData(is);
			if (functionId == null) {
				throw new IOException("the uploaded function does not have an ID");
			}
			Thread.sleep(250l, 0); // give the database some time to act
			return new StatisticGateway(facesContext).getFunctionBean(functionId);
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return null;
		}
	}

	/**
	 * get R package informations
	 * 
	 * @param facesContext the context of this function call
	 */
	public void getRPackageInformation(SquareFacesContext facesContext) {
		if (packages == null) // this makes the cache as this bean is session scoped
		{
			try {
				packages = new RGateway(facesContext).initRPackagePool();
			} catch (IOException | REngineException | REXPMismatchException e) {
				StringBuilder buf = new StringBuilder();
				for (StackTraceElement ste : e.getStackTrace()) {
					buf.append("\n by ");
					buf.append(ste.getClassName());
					buf.append(".");
					buf.append(ste.getMethodName());
					buf.append(" (line ");
					buf.append(ste.getLineNumber());
					buf.append(")");
				}
				facesContext.notifyError(buf.toString());
				LOGGER.error(buf.toString(), e);
				packages = new ArrayList<>();
			}
		}
	}

	/**
	 * get all categories from db that this function does not belong to
	 * 
	 * @return list of categories; an empty one at least
	 */
	public List<FunctioncategoryBean> getUnchoosedCategoryOfBean() {
		List<String> names = new ArrayList<>();
		for (FunctioncategoryBean bean : functionBean.getCategory()) {
			names.add(bean.getName());
		}
		List<FunctioncategoryBean> list = new ArrayList<>();
		for (FunctioncategoryBean bean : categories) {
			if (!names.contains(bean.getName())) {
				list.add(bean);
			}
		}
		return list;
	}

	/**
	 * remove function category from list
	 * 
	 * @param bean category to be removed
	 */
	public void removeCategoryFromBean(FunctioncategoryBean bean) {
		Iterator<FunctioncategoryBean> i = functionBean.getCategory().iterator();
		while (i.hasNext()) {
			FunctioncategoryBean b = i.next();
			if (b.getName().equals(bean.getName())) {
				i.remove();
				return;
			}
		}
	}

	/**
	 * add function category to list
	 * 
	 * @param bean category to be added
	 */
	public void addCategoryToBean(FunctioncategoryBean bean) {
		functionBean.getCategory().add(bean);
	}

	/**
	 * check if the current user can read the current function
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public Boolean canRead(SquareFacesContext facesContext) {
		return functionBean.getAcl().getRead() || functionBean.getCreator().getUsnr().equals(facesContext.getUsnr());
	}

	/**
	 * check if the current user can write the current function
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public Boolean canWrite(SquareFacesContext facesContext) {
		return functionBean.getAcl().getWrite() || functionBean.getCreator().getUsnr().equals(facesContext.getUsnr());
	}

	/**
	 * check if the current user can use the current function
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public Boolean canUse(SquareFacesContext facesContext) {
		return functionBean.getAcl().getExecute() || functionBean.getCreator().getUsnr().equals(facesContext.getUsnr());
	}

	/**
	 * @return true if ajaxInputMessage is not empty; false otherwise
	 */
	public boolean getHasAjaxInputMessage() {
		return ajaxInputMessage != null && !ajaxInputMessage.isBlank();
	}

	/**
	 * @return true if ajaxInputMessage is not empty; false otherwise
	 */
	public boolean getHasAjaxOutputMessage() {
		return ajaxOutputMessage != null && !ajaxOutputMessage.isBlank();
	}

	/**
	 * @return the function id
	 */
	public Integer getFunction() {
		return function;
	}

	/**
	 * @param function the function id
	 */
	public void setFunction(Integer function) {
		this.function = function;
	}

	/**
	 * @return the activated flag
	 */
	public Boolean getActivated() {
		return activated;
	}

	/**
	 * @param activated the activated flag
	 */
	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	/**
	 * @return the function beans
	 */
	public List<FunctionBean> getFunctions() {
		return functions;
	}

	/**
	 * @return the current function bean
	 */
	public FunctionBean getCurrentFunction() {
		return currentFunction;
	}

	/**
	 * @return the function bean
	 */
	public FunctionBean getFunctionBean() {
		return functionBean;
	}

	/**
	 * @param functionBean the function bean
	 */
	public void setFunctionBean(FunctionBean functionBean) {
		this.functionBean = functionBean;
	}

	/**
	 * @return the function input bean
	 */
	public FunctionInputBean getFunctionInputBean() {
		return functionInputBean;
	}

	/**
	 * @param functionInputBean function input bean
	 */
	public void setFunctionInputBean(FunctionInputBean functionInputBean) {
		this.functionInputBean = functionInputBean;
	}

	/**
	 * @return function output bean
	 */
	public FunctionOutputBean getFunctionOutputBean() {
		return functionOutputBean;
	}

	/**
	 * @param functionOutputBean function output bean
	 */
	public void setFunctionOutputBean(FunctionOutputBean functionOutputBean) {
		this.functionOutputBean = functionOutputBean;
	}

	/**
	 * @return the reference
	 */
	public Integer getReference() {
		return reference;
	}

	/**
	 * @param reference the reference
	 */
	public void setReference(Integer reference) {
		this.reference = reference;
	}

	/**
	 * @return the reference beans
	 */
	public List<FunctionBean> getReferences() {
		return references;
	}

	/**
	 * @return the categories
	 */
	public List<FunctioncategoryBean> getCategories() {
		return categories;
	}

	/**
	 * @return the release
	 */
	public String getRelease() {
		return release;
	}

	/**
	 * @param release the release
	 */
	public void setRelease(String release) {
		this.release = release;
	}

	/**
	 * @return the test function bean
	 */
	public FunctionBean getTestFunction() {
		return testFunction;
	}

	/**
	 * @return the upload file
	 */
	public HttpPartWrapper getUploadFile() {
		return uploadFile;
	}

	/**
	 * @param uploadFile the upload file
	 */
	public void setUploadFile(HttpPartWrapper uploadFile) {
		this.uploadFile = uploadFile;
	}

	/**
	 * @return the function rights
	 */
	public List<FunctionRightBean> getFunctionRights() {
		return functionRights;
	}

	/**
	 * @return the data model
	 */
	public List<FunctionBean> getDataModel() {
		return dataModel;
	}

	/**
	 * @return the function call
	 */
	public String getFunctionCall() {
		return functionCall;
	}

	/**
	 * @return the function lines
	 */
	public List<String> getFunctionLines() {
		return functionLines;
	}

	/**
	 * @return the privileges
	 */
	public List<UserAccessLevelBean> getRights() {
		return rights;
	}

	/**
	 * @return the new user
	 */
	public UserAccessLevelBean getNewUser() {
		return newUser;
	}

	/**
	 * @return the function scale bean
	 */
	public TScaleRecord getFunctionscale() {
		return functionscale;
	}

	/**
	 * @return the test result lines
	 */
	public List<String> getTestResultLines() {
		return testResultLines;
	}

	/**
	 * @return the ajax messages
	 */
	public String getAjaxInputMessage() {
		return ajaxInputMessage;
	}

	/**
	 * @param ajaxMessage the ajax messages
	 */
	public void setAjaxInputMessage(String ajaxMessage) {
		this.ajaxInputMessage = ajaxMessage;
	}

	/**
	 * @return the function output types
	 */
	public List<FunctionoutputtypeBean> getOutputtypes() {
		return outputtypes;
	}

	/**
	 * @return the users
	 */
	public List<UserBean> getUsers() {
		return users;
	}

	/**
	 * @return the function input types
	 */
	public List<FunctioninputtypeBean> getInputtypes() {
		return inputtypes;
	}

	/**
	 * @param inputtypes the function input types
	 */
	public void setInputtypes(List<FunctioninputtypeBean> inputtypes) {
		this.inputtypes = inputtypes;
	}

	/**
	 * @return the group privileges
	 */
	public List<GroupPrivilegeBean> getGroupPrivileges() {
		return groupPrivileges;
	}

	/**
	 * @return the new group
	 */
	public Integer getNewGroup() {
		return newGroup;
	}

	/**
	 * @param newGroup the new group
	 */
	public void setNewGroup(Integer newGroup) {
		this.newGroup = newGroup;
	}

	/**
	 * @return all groups
	 */
	public List<UsergroupBean> getAllGroups() {
		return allGroups;
	}

	/**
	 * @return the id of the new user
	 */
	public Integer getNewUsnr() {
		return newUsnr;
	}

	/**
	 * @param newUsnr the id of the new user
	 */
	public void setNewUsnr(Integer newUsnr) {
		this.newUsnr = newUsnr;
	}

	/**
	 * @return the new acl
	 */
	public AclBean getNewAcl() {
		return newAcl;
	}

	/**
	 * @param activePrivilege the active privilege
	 */
	public void setActivePrivilege(GroupPrivilegeBean activePrivilege) {
		this.activePrivilege = activePrivilege;
	}

	/**
	 * @return the R packages
	 */
	public List<RPackage> getPackages() {
		return packages;
	}

	/**
	 * @return the performance
	 */
	public String getPerformance() {
		return performance;
	}

	/**
	 * @param performance the performance to set
	 */
	public void setPerformance(String performance) {
		this.performance = performance;
	}

	/**
	 * @return the ajaxOutputMessage
	 */
	public String getAjaxOutputMessage() {
		return ajaxOutputMessage;
	}

	/**
	 * @param ajaxOutputMessage the ajaxOutputMessage to set
	 */
	public void setAjaxOutputMessage(String ajaxOutputMessage) {
		this.ajaxOutputMessage = ajaxOutputMessage;
	}

	/**
	 * @return the packageName
	 */
	public String getPackageName() {
		return packageName;
	}

	/**
	 * @param packageName the packageName to set
	 */
	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	/**
	 * @return the baseUrl
	 */
	public String getBaseUrl() {
		return baseUrl;
	}

	/**
	 * @param baseUrl the baseUrl to set
	 */
	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	/**
	 * @return the isocodes
	 */
	public String getIsocodes() {
		return isocodes;
	}

	/**
	 * @param isocodes the isocodes to set
	 */
	public void setIsocodes(String isocodes) {
		this.isocodes = isocodes;
	}
}
