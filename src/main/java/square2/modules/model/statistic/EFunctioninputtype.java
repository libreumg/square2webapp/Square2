package square2.modules.model.statistic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

/**
 * 
 * @author henkej
 *
 */
public enum EFunctioninputtype {
	/**
	 * numeric parameter
	 */
	NUMPAR("numeric parameter"),
	/**
	 * variable itself
	 */
	VARSELF("variable itself"),
	/**
	 * list of variables
	 */
	VARLIST("list of variables"),
	/**
	 * variable start
	 */
	VARSTART("variable start"),
	/**
	 * variable end
	 */
	VAREND("variable end"),
	/**
	 * variable observer
	 */
	VAROBS("variable observer"),
	/**
	 * variable devic
	 */
	VARDEV("variable device"),
	/**
	 * variable reader
	 */
	VARREAD("variable reader"),
	/**
	 * text parameter
	 */
	TEXTPAR("text parameter"),
	/**
	 * variable other
	 */
	VAROTHER("variable other"),
	/**
	 * list other
	 */
	LISTOTHER("list other");

	private Logger LOGGER = LogManager.getLogger(this.getClass().getSimpleName());

	private final String value;

	private EFunctioninputtype(String value) {
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String get() {
		return value;
	}

	/**
	 * get the enum for s
	 * 
	 * @param s
	 *          the enum value
	 * @return the enum
	 * @throws DataAccessException
	 *           if no such enum has been found
	 */
	public final EFunctioninputtype getEnum(String s) throws DataAccessException {
		if (s == null) {
			return null;
		} else if (s.equals(NUMPAR.get())) {
			return NUMPAR;
		} else if (s.equals(VARSELF.get())) {
			return VARSELF;
		} else if (s.equals(VARLIST.get())) {
			return VARLIST;
		} else if (s.equals(VARSTART.get())) {
			return VARSTART;
		} else if (s.equals(VAREND.get())) {
			return VAREND;
		} else if (s.equals(VAROBS.get())) {
			return VAROBS;
		} else if (s.equals(VARDEV.get())) {
			return VARDEV;
		} else if (s.equals(VARREAD.get())) {
			return VARREAD;
		} else if (s.equals(TEXTPAR.get())) {
			return TEXTPAR;
		} else if (s.equals(VAROTHER.get())) {
			return VAROTHER;
		} else if (s.equals(LISTOTHER.get())) {
			return LISTOTHER;
		} else {
			String message = new StringBuilder("enum value '").append(s).append("' not found in enum EnumFunctininputtype")
					.toString();
			LOGGER.error(message);
			throw new DataAccessException(message);
		}
	}
}
