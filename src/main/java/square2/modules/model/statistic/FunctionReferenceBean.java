package square2.modules.model.statistic;

/**
 * 
 * @author henkej
 *
 */
public class FunctionReferenceBean {
	private String functionName;
	private Integer functionId;
	private String varname;
	private String varlist;
	private String dataframe;
	private String referenceName;
	private Integer referenceId;

	/**
	 * @return the functionName
	 */
	public String getFunctionName() {
		return functionName;
	}

	/**
	 * @param functionName
	 *          the functionName to set
	 */
	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	/**
	 * @return the functionId
	 */
	public Integer getFunctionId() {
		return functionId;
	}

	/**
	 * @param functionId
	 *          the functionId to set
	 */
	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}

	/**
	 * @return the varname
	 */
	public String getVarname() {
		return varname;
	}

	/**
	 * @param varname
	 *          the varname to set
	 */
	public void setVarname(String varname) {
		this.varname = varname;
	}

	/**
	 * @return the varlist
	 */
	public String getVarlist() {
		return varlist;
	}

	/**
	 * @param varlist
	 *          the varlist to set
	 */
	public void setVarlist(String varlist) {
		this.varlist = varlist;
	}

	/**
	 * @return the dataframe
	 */
	public String getDataframe() {
		return dataframe;
	}

	/**
	 * @param dataframe
	 *          the dataframe to set
	 */
	public void setDataframe(String dataframe) {
		this.dataframe = dataframe;
	}

	/**
	 * @return the referenceName
	 */
	public String getReferenceName() {
		return referenceName;
	}

	/**
	 * @param referenceName
	 *          the referenceName to set
	 */
	public void setReferenceName(String referenceName) {
		this.referenceName = referenceName;
	}

	/**
	 * @return the referenceId
	 */
	public Integer getReferenceId() {
		return referenceId;
	}

	/**
	 * @param referenceId
	 *          the referenceId to set
	 */
	public void setReferenceId(Integer referenceId) {
		this.referenceId = referenceId;
	}
}
