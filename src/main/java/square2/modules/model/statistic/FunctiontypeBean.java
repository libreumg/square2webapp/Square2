package square2.modules.model.statistic;

/**
 * 
 * @author henkej
 *
 */
public class FunctiontypeBean {
	private final Integer functiontypeId;
	private String functiontypeName;

	/**
	 * create new function type bean
	 * 
	 * @param functiontypeId
	 *          the id
	 * @param functiontypeName
	 *          the name
	 */
	public FunctiontypeBean(Integer functiontypeId, String functiontypeName) {
		super();
		this.functiontypeId = functiontypeId;
		this.functiontypeName = functiontypeName;
	}

	/**
	 * @return the functiontypeName
	 */
	public String getFunctiontypeName() {
		return functiontypeName;
	}

	/**
	 * @param functiontypeName
	 *          the functiontypeName to set
	 */
	public void setFunctiontypeName(String functiontypeName) {
		this.functiontypeName = functiontypeName;
	}

	/**
	 * @return the functiontypeId
	 */
	public Integer getFunctiontypeId() {
		return functiontypeId;
	}
}
