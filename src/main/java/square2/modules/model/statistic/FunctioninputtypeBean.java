package square2.modules.model.statistic;

import java.io.Serializable;

import com.google.gson.Gson;

import square2.converter.JsonConverter;
import square2.converter.Selectable;

/**
 * 
 * @author henkej
 *
 */
public class FunctioninputtypeBean implements Serializable, Selectable<FunctioninputtypeBean>, JsonConverter<FunctioninputtypeBean> {
  private static final long serialVersionUID = 1L;

  private final Integer pk;
  private String name;
  private final String translation;
  private final Integer usage;

  /**
   * create a new empty function input type bean (prevent NullPointerException)
   */
  public FunctioninputtypeBean() {
    this.pk = null;
    this.usage = null;
    this.translation = null;
  }

  /**
   * create new function input type bean
   * 
   * @param pk the id
   * @param usage the usage
   * @param translation the translation
   */
  public FunctioninputtypeBean(Integer pk, Integer usage, String translation) {
    super();
    this.pk = pk;
    this.usage = usage;
    this.translation = translation;
  }

  @Override
  public String toJson() {
    return new Gson().toJson(this);
  }

  @Override
  public FunctioninputtypeBean fromJson(String json) {
    return new Gson().fromJson(json, FunctioninputtypeBean.class);
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((pk == null) ? 0 : pk.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    FunctioninputtypeBean other = (FunctioninputtypeBean) obj;
    if (pk == null) {
      if (other.pk != null)
        return false;
    } else if (!pk.equals(other.pk))
      return false;
    return true;
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
    buf.append("@{pk=").append(pk);
    buf.append(", name=").append(name);
    buf.append(", translation=").append(translation);
    buf.append(", usage=").append(usage);
    buf.append("}");
    return buf.toString();
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * @return the pk
   */
  public Integer getPk() {
    return pk;
  }

  /**
   * @return the usage
   */
  public Integer getUsage() {
    return usage;
  }

  /**
   * @return the translation
   */
  public String getTranslation() {
    return translation;
  }
}
