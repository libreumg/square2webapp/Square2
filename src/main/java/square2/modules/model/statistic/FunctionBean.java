package square2.modules.model.statistic;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jooq.exception.DataAccessException;

import de.ship.dbppsquare.square.enums.EnumDatasource;
import square2.db.control.converter.EnumConverter;
import square2.db.control.model.EnumVarlist;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.ProfileBean;
import square2.modules.model.admin.FunctioncategoryBean;

/**
 * 
 * @author henkej
 * 
 */
public class FunctionBean implements Serializable, Comparable<FunctionBean> {
	private static final long serialVersionUID = 1L;

	private Integer functionId;
	private ProfileBean creator;
	private final Date creationdate;
	private String name;
	private String body;
	private String before;
	private String after;
	private String test;
	private String summary;
	private String description;
	private Boolean activated;
	private List<FunctioncategoryBean> category;
	private List<FunctionInputBean> input;
	private List<FunctionOutputBean> output;
	private List<FunctionBean> reference;
	private List<FunctionScale> scales;
	private String varname;
	private EnumVarlist varlist;
	private String dataframe;
	private String vardef;
	private Integer right;
	private String datasource;
	private String forR;
	private String vignette;
	private Boolean useIntervals;
	private Date lastchange;
	private AclBean acl;

	/**
	 * create new FunctionBean with predefined right, creationdate and useIntervals
	 * 
	 * @param right
	 *          id of privilege
	 * @param creationdate
	 *          date of function creation
	 * @param useIntervals
	 *          flag to determine if report intervals are used by this function or
	 *          not
	 * @param scales
	 *          list of all possible function scales (from T_SCALE); activated ones
	 *          have property checked = true
	 */
	public FunctionBean(Integer right, Date creationdate, Boolean useIntervals, List<FunctionScale> scales) {
		super();
		this.right = right;
		this.creationdate = creationdate;
		input = new ArrayList<>();
		output = new ArrayList<>();
		reference = new ArrayList<>();
		this.scales = scales;
		this.varlist = EnumVarlist.NULL;
		this.useIntervals = useIntervals;
		this.category = new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{functionId=").append(functionId);
		buf.append(", creator=").append(creator);
		buf.append(", creationdate=").append(creationdate);
		buf.append(", name=").append(name);
		buf.append(", body=").append(body);
		buf.append(", before=").append(before);
		buf.append(", after=").append(after);
		buf.append(", test=").append(test);
		buf.append(", summary=").append(summary);
		buf.append(", description=").append(description);
		buf.append(", activated=").append(activated);
		buf.append(", category=").append(category);
		buf.append(", input=").append(input);
		buf.append(", output=").append(output);
		buf.append(", reference=").append(reference);
		buf.append(", right=").append(right);
		buf.append(", functionscales=").append(scales);
		buf.append(", varname=").append(varname);
		buf.append(", varlist=").append(varlist);
		buf.append(", dataframe=").append(dataframe);
		buf.append(", vardef=").append(vardef);
		buf.append(", datasource=").append(datasource);
		buf.append(", useIntervals=").append(useIntervals);
		buf.append(", vignette=").append(vignette);
		buf.append("}");
		return buf.toString();
	}

	@Override
	public int compareTo(FunctionBean o) {
		return name == null || o == null || o.getName() == null ? 0
				: name.toLowerCase().compareTo(o.getName().toLowerCase());
	}

	/**
	 * check if current user is the creator of this function
	 * 
	 * @param facesContext
	 *          context of this function call
	 * @return true or false
	 */
	public boolean isCreator(SquareFacesContext facesContext) {
		return creator == null || creator.getUsnr() == null ? false : creator.getUsnr().equals(facesContext.getUsnr());
	}

	/**
	 * get all possible function scales
	 * 
	 * @return list of function scales
	 */
	public List<FunctionScale> getScales() {
		return scales;
	}

	/**
	 * get function input names; this are the name fields of function input beans
	 * 
	 * @return list of function input names
	 */
	public List<String> getFunctionInputNames() {
		List<String> list = new ArrayList<>();
		for (FunctionInputBean fib : input) {
			list.add(fib.getName());
		}
		return list;
	}

	/**
	 * get function output names; this are the name fields of function output beans
	 * 
	 * @return list of function output names
	 */
	public List<String> getFunctionOutputNames() {
		List<String> list = new ArrayList<>();
		for (FunctionOutputBean fob : output) {
			list.add(fob.getName());
		}
		return list;
	}

	/**
	 * get list of ids of referenced functions
	 * 
	 * @return list of ids
	 */
	public List<Integer> getReferencedFunctions() {
		List<Integer> list = new ArrayList<>();
		for (FunctionBean frb : reference) {
			list.add(frb.getFunctionId());
		}
		return list;
	}

	/**
	 * @param value
	 *          the value
	 */
	public void setDatasource(EnumDatasource value) {
		setDatasource(value == null ? null : value.getLiteral());
	}

	/**
	 * get the data source as enum
	 * 
	 * @return the enum of the data source
	 * @throws DataAccessException
	 *           if anything went wrong on database side
	 */
	public EnumDatasource getDatasourceAsEnum() throws DataAccessException {
		EnumDatasource e = new EnumConverter().getEnumDatasource(datasource);
		if (datasource == null) {
			return null;
		} else if (e == null) {
			throw new DataAccessException("enum not found: " + datasource);
		} else {
			return e;
		}
	}

	/**
	 * get the title (aka summary)
	 * 
	 * @return the title
	 */
	public String getTitle() {
	  return summary;
	}
	
	/**
	 * get the list of referenced function beans
	 * 
	 * @return list of function beans
	 */
	public List<FunctionBean> getReference() {
		return reference;
	}

	/**
	 * null the privilege id<br>
	 * do this only for function beans that are not yet in the database
	 */
	public void nullRightId() {
		right = null;
	}

	/**
	 * @return the name of this function
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name of this function
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the body of this function
	 */
	public String getBody() {
		return body;
	}

	/**
	 * @param body
	 *          the body of this function
	 */
	public void setBody(String body) {
		this.body = body;
	}

	/**
	 * @return the summary of this function
	 */
	public String getSummary() {
		return summary;
	}

	/**
	 * @param summary
	 *          the summary of this function
	 */
	public void setSummary(String summary) {
		this.summary = summary;
	}

	/**
	 * @return the description of this function
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description of this function
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the input of this function
	 */
	public List<FunctionInputBean> getInput() {
		return input;
	}

	/**
	 * @return the output of this function
	 */
	public List<FunctionOutputBean> getOutput() {
		return output;
	}

	/**
	 * @return the id of this function
	 */
	public Integer getFunctionId() {
		return functionId;
	}

	/**
	 * @param functionId
	 *          the id of this function
	 */
	public void setFunctionId(Integer functionId) {
		this.functionId = functionId;
	}

	/**
	 * @return the creator of this function
	 */
	public ProfileBean getCreator() {
		return creator;
	}

	/**
	 * @param creator
	 *          the creator of this function
	 */
	public void setCreator(ProfileBean creator) {
		this.creator = creator;
	}

	/**
	 * @return the activated flag of this function
	 */
	public Boolean getActivated() {
		return activated;
	}

	/**
	 * @param activated
	 *          the activated flag of this function
	 */
	public void setActivated(Boolean activated) {
		this.activated = activated;
	}

	/**
	 * @return the category of this function
	 */
	public List<FunctioncategoryBean> getCategory() {
		return category;
	}

	/**
	 * @param category
	 *          the category of this function
	 */
	public void setCategory(List<FunctioncategoryBean> category) {
		this.category = category;
	}

	/**
	 * @return the privilege id of this function
	 */
	public Integer getRight() {
		return right;
	}

	/**
	 * set the privilege id if it is null
	 * 
	 * @param rightId
	 *          the privilege id of this function
	 */
	public void setRightIdIfNull(Integer rightId) {
		right = right == null ? rightId : right;
	}

	/**
	 * reset the privilege id by rightId
	 * 
	 * @param rightId
	 *          the privilege id of this function
	 */
	public void resetRightId(Integer rightId) {
		right = rightId;
	}

	/**
	 * @return the before block of this function
	 */
	public String getBefore() {
		return before;
	}

	/**
	 * @param before
	 *          the before block of this function
	 */
	public void setBefore(String before) {
		this.before = before;
	}

	/**
	 * @return the after block of this function
	 */
	public String getAfter() {
		return after;
	}

	/**
	 * @param after
	 *          the after block of this function
	 */
	public void setAfter(String after) {
		this.after = after;
	}

	/**
	 * @return the test block of this function
	 */
	public String getTest() {
		return test;
	}

	/**
	 * @param test
	 *          the test block of this function
	 */
	public void setTest(String test) {
		this.test = test;
	}

	/**
	 * @return the varname of this function
	 */
	public String getVarname() {
		return varname;
	}

	/**
	 * @param varname
	 *          the varname of this function
	 */
	public void setVarname(String varname) {
		this.varname = varname;
	}

	/**
	 * @return the varlist of this function
	 */
	public String getVarlist() {
		return varlist.getJsfValue();
	}

	/**
	 * @return the varlist of this function
	 */
	public Boolean getVarlistDbvalue() {
		return varlist.getDbValue();
	}

	/**
	 * @param varlist
	 *          the varlist of this function
	 */
	public void setVarlist(String varlist) {
		this.varlist = EnumVarlist.NULL.fromJsf(varlist);
	}

	/**
	 * @param varlist
	 *          the varlist of this function
	 */
	public void setVarlistDbValue(Boolean varlist) {
		this.varlist = EnumVarlist.NULL.fromDb(varlist);
	}

	/**
	 * @return the data frame of this function
	 */
	public String getDataframe() {
		return dataframe;
	}

	/**
	 * @param dataframe
	 *          the data frame of this function
	 */
	public void setDataframe(String dataframe) {
		this.dataframe = dataframe;
	}

	/**
	 * @return the vardef of this function
	 */
	public String getVardef() {
		return vardef;
	}

	/**
	 * @param vardef
	 *          the vardef of this function
	 */
	public void setVardef(String vardef) {
		this.vardef = vardef;
	}

	/**
	 * @return the data source of this function
	 */
	public String getDatasource() {
		return datasource;
	}

	/**
	 * @param datasource
	 *          the data source of this function
	 */
	public void setDatasource(String datasource) {
		this.datasource = datasource;
	}

	/**
	 * @return the for R flag of this function
	 */
	public String getForR() {
		return forR;
	}

	/**
	 * @param forR
	 *          the for R flag of this function
	 */
	public void setForR(String forR) {
		this.forR = forR;
	}

	/**
	 * @param timestamp
	 *          the timestamp of this functions last change
	 */
	public void setLastchange(Timestamp timestamp) {
		this.lastchange = timestamp == null ? null : new Date(timestamp.getTime());
	}

	/**
	 * @return the date of this functions last change
	 */
	public Date getLastchange() {
		return lastchange;
	}

	/**
	 * @return the acl for the current user of this function
	 */
	public AclBean getAcl() {
		return acl;
	}

	/**
	 * @param acl
	 *          the acl for the current user of this function
	 */
	public void setAcl(AclBean acl) {
		this.acl = acl;
	}

	/**
	 * @return the creation date of this function
	 */
	public Date getCreationdate() {
		return creationdate;
	}

	/**
	 * @return the use intervals flag of this function
	 */
	public Boolean getUseIntervals() {
		return useIntervals;
	}

	/**
	 * @param useIntervals
	 *          the use intervals flag of this function
	 */
	public void setUseIntervals(Boolean useIntervals) {
		this.useIntervals = useIntervals;
	}

  /**
   * @return the vignette
   */
  public String getVignette() {
    return vignette;
  }

  /**
   * @param vignette the vignette to set
   */
  public void setVignette(String vignette) {
    this.vignette = vignette;
  }
}
