package square2.modules.model.statistic;

import java.io.Serializable;

/**
 * 
 * @author henkej
 * 
 */
public class FunctionInputBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Integer pk;
	private String name;
	private String standard;
	private String description;
	private FunctioninputtypeBean fkType;

	/**
	 * @param pk
	 *          the id
	 */
	public FunctionInputBean(Integer pk) {
		super();
		this.pk = pk;
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{pk=").append(pk);
	  buf.append(", name=").append(name);
		buf.append(", standard=").append(standard);
		buf.append(", description=").append(description);
		buf.append(", fkType=").append(fkType);
		buf.append("}");
		return buf.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FunctionInputBean other = (FunctionInputBean) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the standard
	 */
	public String getStandard() {
		return standard;
	}

	/**
	 * @param standard
	 *          the standard to set
	 */
	public void setStandard(String standard) {
		this.standard = standard;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the fkType
	 */
	public FunctioninputtypeBean getFkType() {
		return fkType;
	}

	/**
	 * @param fkType
	 *          the fkType to set
	 */
	public void setFkType(FunctioninputtypeBean fkType) {
		this.fkType = fkType;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}
}
