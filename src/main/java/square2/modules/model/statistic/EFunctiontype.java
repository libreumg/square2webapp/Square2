package square2.modules.model.statistic;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

/**
 * 
 * @author henkej
 *
 */
public enum EFunctiontype {
	/**
	 * inner routine
	 */
	INNER("inner routine"),
	/**
	 * wrapper
	 */
	WRAPPER("wrapper"),
	/**
	 * latex export
	 */
	LATEX("latex export");

	private static final Logger LOGGER = LogManager.getLogger(EFunctiontype.class);

	private final String value;

	private EFunctiontype(String value) {
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String get() {
		return value;
	}

	/**
	 * @return list of enums as string
	 */
	public static final List<String> getStringList() {
		List<String> list = new ArrayList<>();
		list.add(INNER.get());
		list.add(WRAPPER.get());
		list.add(LATEX.get());
		return list;
	}

	/**
	 * get the enum
	 * 
	 * @param s
	 *          the string
	 * @return the enum if found
	 * @throws DataAccessException
	 *           if no such enum is found
	 */
	public final EFunctiontype getEnum(String s) {
		if (s == null) {
			return null;
		} else if (s.equals(INNER.get())) {
			return INNER;
		} else if (s.equals(WRAPPER.get())) {
			return WRAPPER;
		} else if (s.equals(LATEX.get())) {
			return LATEX;
		} else {
			String message = new StringBuilder("enum value '").append(s).append("' not found in enum EnumFunctiontype")
					.toString();
			LOGGER.error(message);
			throw new DataAccessException(message);
		}
	}
}
