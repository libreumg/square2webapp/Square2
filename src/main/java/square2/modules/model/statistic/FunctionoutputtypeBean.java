package square2.modules.model.statistic;

import java.io.Serializable;

import com.google.gson.Gson;

import square2.converter.Selectable;

/**
 * 
 * @author henkej
 *
 */
public class FunctionoutputtypeBean implements Serializable, Selectable<FunctionoutputtypeBean> {
  private static final long serialVersionUID = 1L;
  
  private final Integer pk;
	private String name;
	private String description;
	private Long referenceAmount;
	private String latexBefore;
	private String latexAfter;

	/**
	 * create new function output type bean
	 * 
	 * @param pk
	 *          the id
	 */
	public FunctionoutputtypeBean(Integer pk) {
		this.pk = pk;
	}
	
  @Override
  public String toJson() {
    return new Gson().toJson(this);
  }

  @Override
  public FunctionoutputtypeBean fromJson(String json) {
    return new Gson().fromJson(json, FunctionoutputtypeBean.class);
  }


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((pk == null) ? 0 : pk.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FunctionoutputtypeBean other = (FunctionoutputtypeBean) obj;
		if (pk == null) {
			if (other.pk != null)
				return false;
		} else if (!pk.equals(other.pk))
			return false;
		return true;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the referenceAmount
	 */
	public Long getReferenceAmount() {
		return referenceAmount;
	}

	/**
	 * @param referenceAmount
	 *          the referenceAmount to set
	 */
	public void setReferenceAmount(Long referenceAmount) {
		this.referenceAmount = referenceAmount;
	}

	/**
	 * @return the latexBefore
	 */
	public String getLatexBefore() {
		return latexBefore;
	}

	/**
	 * @param latexBefore
	 *          the latexBefore to set
	 */
	public void setLatexBefore(String latexBefore) {
		this.latexBefore = latexBefore;
	}

	/**
	 * @return the latexAfter
	 */
	public String getLatexAfter() {
		return latexAfter;
	}

	/**
	 * @param latexAfter
	 *          the latexAfter to set
	 */
	public void setLatexAfter(String latexAfter) {
		this.latexAfter = latexAfter;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}
}
