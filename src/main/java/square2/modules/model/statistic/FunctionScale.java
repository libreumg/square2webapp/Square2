package square2.modules.model.statistic;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class FunctionScale implements Serializable {
	private static final long serialVersionUID = 1L;
  private final Integer pk;
	private final String name;
	private Boolean checked;

	/**
	 * @param pk
	 *          the id of the scale
	 * @param name
	 *          the name of the scale
	 */
	public FunctionScale(Integer pk, String name) {
		super();
		this.pk = pk;
		this.name = name;
		this.checked = false;
	}

	/**
	 * @param pk
	 *          the id of the scale
	 * @param name
	 *          the name of the scale
	 * @param checked
	 *          true if this scale is valid for the function that holds this scale
	 */
	public FunctionScale(Integer pk, String name, Boolean checked) {
		super();
		this.pk = pk;
		this.name = name;
		this.checked = checked;
	}

	/**
	 * @return the checked
	 */
	public Boolean getChecked() {
		return checked;
	}

	/**
	 * @param checked
	 *          the checked to set
	 */
	public void setChecked(Boolean checked) {
		this.checked = checked;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
