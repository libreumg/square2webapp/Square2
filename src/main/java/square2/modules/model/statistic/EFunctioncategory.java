package square2.modules.model.statistic;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

/**
 * 
 * @author henkej
 *
 */
public enum EFunctioncategory {
	/**
	 * unknown
	 */
	UNKNOWN("unknown"),
	/**
	 * missing value
	 */
	MIS("missing values"),
	/**
	 * extreme values
	 */
	EXTR("extreme values"),
	/**
	 * time trends
	 */
	TIME("time trends"),
	/**
	 * observer/device/reader differences
	 */
	OBS("observer/device/reader differences"),
	/**
	 * descriptive statistics
	 */
	DES("descriptive statistics");

	private static final Logger LOGGER = LogManager.getLogger(EFunctioncategory.class);

	private final String value;

	private EFunctioncategory(String value) {
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String get() {
		return value;
	}

	/**
	 * @return the list of enums as string
	 */
	public static final List<String> getStringList() {
		List<String> list = new ArrayList<>();
		list.add(UNKNOWN.get());
		list.add(MIS.get());
		list.add(EXTR.get());
		list.add(TIME.get());
		list.add(OBS.get());
		list.add(DES.get());
		return list;
	}

	/**
	 * get the enum for s
	 * 
	 * @param s
	 *          the enum string
	 * @return the enum
	 * @throws DataAccessException
	 *           if no such enum has been found
	 */
	public final EFunctioncategory getEnum(String s) throws DataAccessException {
		if (s == null) {
			return null;
		} else if (s.equals(UNKNOWN.get())) {
			return UNKNOWN;
		} else if (s.equals(MIS.get())) {
			return MIS;
		} else if (s.equals(EXTR.get())) {
			return EXTR;
		} else if (s.equals(TIME.get())) {
			return TIME;
		} else if (s.equals(OBS.get())) {
			return OBS;
		} else if (s.equals(DES.get())) {
			return DES;
		} else {
			String message = new StringBuilder("enum value '").append(s).append("' not found in enum EnumFunctincategory")
					.toString();
			LOGGER.error(message);
			throw new DataAccessException(message);
		}
	}
}
