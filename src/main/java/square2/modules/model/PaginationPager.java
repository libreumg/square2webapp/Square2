package square2.modules.model;

/**
 * 
 * @author henkej
 *
 */
public class PaginationPager extends Pager {
	private Integer maxPagesOnButtonList = 20; // TODO: as configuration parameter
	private Integer paginatorPageIndex = 1;

	/**
	 * check if the page button should be rendered
	 * 
	 * @param page
	 *          the page
	 * @return true or false
	 */
	public boolean renderPageButton(Integer page) {
		Integer upperPageBorder = paginatorPageIndex * maxPagesOnButtonList;
		Integer lowerPageBorder = upperPageBorder - maxPagesOnButtonList;
		return upperPageBorder > page && lowerPageBorder <= page;
	}

	/**
	 * decrease the paginator page by one
	 */
	public void decreasePaginationPage() {
		paginatorPageIndex--;
		if (paginatorPageIndex < 1) {
			paginatorPageIndex = 1;
		}
	}

	/**
	 * increase the paginator page by one
	 * 
	 * @param itemSize
	 *          the number of items
	 */
	public void increasePaginationPage(Integer itemSize) {
		paginatorPageIndex++;
		if (paginatorPageIndex > getMaxPaginatorPages(itemSize)) {
			paginatorPageIndex = getMaxPaginatorPages(itemSize);
		}
	}

	/**
	 * get the maximum number of paginator pages
	 * 
	 * @param itemSize
	 *          the number of items
	 * 
	 * @return the maximum number
	 */
	public Integer getMaxPaginatorPages(Integer itemSize) {
		return ((getPages(itemSize).size() - 1) / maxPagesOnButtonList) + 1;
	}

	/**
	 * tell me if there are too many paginator pages and we are not on the first one
	 * 
	 * @return true or false
	 */
	public boolean getTooManyPagesLeft() {
		return paginatorPageIndex > 1;
	}

	/**
	 * tell me if there are more paginator pages and we are not on the last one
	 *
	 * @param itemSize
	 *          the number of items
	 * 
	 * @return true or false
	 */
	public boolean getTooManyPagesRight(Integer itemSize) {
		return paginatorPageIndex < getMaxPaginatorPages(itemSize);
	}

	/**
	 * @param maxPagesOnButtonList
	 *          the maxPagesOnButtonList to set
	 */
	public void setMaxPagesOnButtonList(Integer maxPagesOnButtonList) {
		this.maxPagesOnButtonList = maxPagesOnButtonList;
	}
}
