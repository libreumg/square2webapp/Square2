package square2.modules.model.test;

/**
 * 
 * @author henkej
 *
 */
public class FunctionValue {
	private final String name;
	private String value;

	/**
	 * create new function value
	 * 
	 * @param name
	 *          the name
	 */
	public FunctionValue(String name) {
		this.name = name;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *          the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
