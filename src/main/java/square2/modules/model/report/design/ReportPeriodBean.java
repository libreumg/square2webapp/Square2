package square2.modules.model.report.design;

import java.io.Serializable;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

/**
 * 
 * @author henkej
 *
 */
public class ReportPeriodBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Integer pk;
	private Date dateFrom;
	private Date dateUntil;

	/**
	 * create new calculation interval bean
	 * 
	 * @param pk
	 *          the id
	 */
	public ReportPeriodBean(Integer pk) {
		this.pk = pk;
	}
	
	/**
	 * @return a string representation
	 */
	public String getFromAsString() {
	  StringBuilder buf = new StringBuilder();
	  buf.append(dateFrom == null ? "" : new SimpleDateFormat("dd.MM.yyyy").format(dateFrom));
    return buf.toString();
	}

  /**
   * @return a string representation
   */
  public String getUntilAsString() {
    StringBuilder buf = new StringBuilder();
    buf.append(dateUntil == null ? "" : new SimpleDateFormat("dd.MM.yyyy").format(dateUntil));
    return buf.toString();
  }

	/**
	 * @return a string representation
	 */
	public String getString() {
		return getFromAsString().concat(" - ").concat(getUntilAsString());
	}

	/**
	 * @return the id
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the date from as localdatetime
	 */
	public LocalDateTime getLdtDateFrom() {
	  return dateFrom == null ? null : dateFrom.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

  /**
   * @return the date from as localdatetime
   */
  public LocalDateTime getLdtDateUntil() {
    return dateUntil == null ? null : dateUntil.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();    
  }
	
	/**
	 * @return the data from timestamp
	 */
	@Deprecated
	public Timestamp getTSDateFrom() {
		return dateFrom == null ? null : new Timestamp(dateFrom.getTime());
	}

	/**
	 * @return the date until timestamp
	 */
	@Deprecated
	public Timestamp getTSDateUntil() {
		return dateUntil == null ? null : new Timestamp(dateUntil.getTime());
	}

	/**
	 * @return the date from
	 */
	public Date getDateFrom() {
		return dateFrom;
	}

	/**
	 * @param dateFrom
	 *          the date from
	 */
	public void setDateFrom(Date dateFrom) {
		this.dateFrom = dateFrom;
	}

	/**
	 * @return the date until
	 */
	public Date getDateUntil() {
		return dateUntil;
	}

	/**
	 * @param dateUntil
	 *          the date until
	 */
	public void setDateUntil(Date dateUntil) {
		this.dateUntil = dateUntil;
	}
}
