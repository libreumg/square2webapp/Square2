package square2.modules.model.report.design;

/**
 * 
 * @author henkej
 *
 */
public class AnaparfunBean {
	private final Integer anaparfunPk;
	private final Integer functionPk;
	private final String functionName;

	/**
	 * create new anaparfun bean
	 * 
	 * @param anaparfunPk
	 *          the id of the anaparfun
	 * @param functionPk
	 *          the id of the function
	 * @param functionName
	 *          the name of the function
	 */
	public AnaparfunBean(Integer anaparfunPk, Integer functionPk, String functionName) {
		super();
		this.anaparfunPk = anaparfunPk;
		this.functionPk = functionPk;
		this.functionName = functionName;
	}

	/**
	 * @return the anaparfun id
	 */
	public Integer getAnaparfunPk() {
		return anaparfunPk;
	}

	/**
	 * @return the function id
	 */
	public Integer getFunctionPk() {
		return functionPk;
	}

	/**
	 * @return the function name
	 */
	public String getFunctionName() {
		return functionName;
	}
}
