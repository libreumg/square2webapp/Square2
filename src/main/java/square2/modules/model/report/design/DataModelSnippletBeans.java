package square2.modules.model.report.design;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.DataModel;

/**
 * 
 * @author henkej
 *
 */
public class DataModelSnippletBeans extends DataModel<SnippletBean> {
	private List<SnippletBean> snipplets;
	private Integer rowIndex;

	/**
	 * create new data model for snipplet beans
	 * 
	 * @param snipplets
	 *          the snipplets
	 */
	public DataModelSnippletBeans(List<SnippletBean> snipplets) {
		this.snipplets = snipplets == null ? new ArrayList<>() : snipplets;
	}

	@Override
	public int getRowCount() {
		return snipplets.size();
	}

	@Override
	public SnippletBean getRowData() {
		return snipplets.get(rowIndex);
	}

	@Override
	public int getRowIndex() {
		return rowIndex;
	}

	@Override
	public Object getWrappedData() {
		return snipplets;
	}

	@Override
	public boolean isRowAvailable() {
		try {
			return snipplets.get(rowIndex) != null;
		} catch (IndexOutOfBoundsException e) {
			return false;
		}
	}

	@Override
	public void setRowIndex(int rowIndex) {
		this.rowIndex = rowIndex;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setWrappedData(Object snipplets) {
		this.snipplets = (List<SnippletBean>) snipplets;
	}
}
