package square2.modules.model.report.design;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class SnipplettypeBean implements Serializable {
  private static final long serialVersionUID = 1L;

  /**
   * snipplettype.rexport
   */
  public static final String REXPORT = "snipplettype.rexport";
  /**
   * snipplettype.rexport.nolinebreak
   */
  public static final String REXPORTNOLINEBREAK = "snipplettype.rexport.nolinebreak";
  /**
   * snipplettype.fixedtext
   */
  public static final String FIXEDTEXT = "snipplettype.fixedtext";
  /**
   * snipplettype.base64
   */
  public static final String BASE64NOLINEBREAK = "snipplettype.base64.nolinebreak";
  /**
   * snipplettype.base64.nolinebreak
   */
  public static final String BASE64 = "snipplettype.base64";
  /**
   * snipplettype.graph2x2
   */
  public static final String GRAPH2X2 = "snipplettype.graph2x2";
  /**
   * snipplettype.graph2x2.nolinebreak
   */
  public static final String GRAPH2X2NOLINEBREAK = "snipplettype.graph2x2.nolinebreak";
  /**
   * snipplettype.text
   */
  public static final String TEXT = "snipplettype.text";
  /**
   * snipplettype.comment
   */
  public static final String COMMENT = "snipplettype.comment";
  /**
   * snipplettype.markdown
   */
  public static final String MARKDOWN = "snipplettype.markdown";

  private Integer id;
  private Integer orderNr;
  private String snipplettype;
  private String snipplettemplate;
  private String htmlComponent;

  /**
   * create new snipplet type bean
   * 
   * @param id the id of the snipplet type
   * @param orderNr the order number
   * @param snipplettype the snipplet type
   * @param snipplettemplate the templatre
   * @param htmlComponent the html component
   */
  public SnipplettypeBean(Integer id, Integer orderNr, String snipplettype, String snipplettemplate,
      String htmlComponent) {
    this.id = id;
    this.setOrderNr(orderNr);
    this.snipplettype = snipplettype;
    this.snipplettemplate = snipplettemplate;
    this.htmlComponent = htmlComponent;
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder();
    buf.append(snipplettype).append("(").append(id).append("){").append(snipplettemplate).append("}");
    return buf.toString();
  }

  /**
   * @return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return the id
   */
  public String getSnipplettype() {
    return snipplettype;
  }

  /**
   * @param snipplettype the type
   */
  public void setSnipplettype(String snipplettype) {
    this.snipplettype = snipplettype;
  }

  /**
   * @return the template
   */
  public String getSnipplettemplate() {
    return snipplettemplate;
  }

  /**
   * @param snipplettemplate the template
   */
  public void setSnipplettemplate(String snipplettemplate) {
    this.snipplettemplate = snipplettemplate;
  }

  /**
   * @return the html component
   */
  public String getHtmlComponent() {
    return htmlComponent;
  }

  /**
   * @param htmlComponent the html component
   */
  public void setHtmlComponent(String htmlComponent) {
    this.htmlComponent = htmlComponent;
  }

  /**
   * @return the orderNr
   */
  public Integer getOrderNr() {
    return orderNr;
  }

  /**
   * @param orderNr the orderNr to set
   */
  public void setOrderNr(Integer orderNr) {
    this.orderNr = orderNr;
  }
}
