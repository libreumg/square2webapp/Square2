package square2.modules.model.report.design;

import java.io.Serializable;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import square2.converter.MatrixcolumnBeanConverter;
import square2.modules.model.template.report.MatrixcolumnParameterBean;

/**
 * 
 * @author henkej
 *
 */
public class SnippletBean implements Serializable, Comparable<SnippletBean> {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private Integer orderNr;
	private Integer orderNrNew;
	private SnipplettypeBean type;
	private String markdowntext;
	private Integer fkFunctionoutput;
	private Boolean escape;
	private List<CodelineBean> codeLines;
	private Integer rightId;
	private final String uuid;
	private MatrixcolumnParameterBean matrixcolumnBean;

	/**
	 * create new snipplet bean
	 * 
	 * @param id the id of the snipplet
	 * @param orderNr the order number of the snipplet
	 * @param markdowntext the content of the snipplet if any as markdowntext
	 * @param fkFunctionoutput the function output reference of the snipplet if any
	 * @param matrixcolumnBean the matrix column bean
	 */
	public SnippletBean(Integer id, Integer orderNr, String markdowntext, Integer fkFunctionoutput, MatrixcolumnParameterBean matrixcolumnBean) {
		String md5Code = "errorOnGeneratingMd5Code";
		try {
			String oldcode = new StringBuilder("").append(matrixcolumnBean == null ? null : matrixcolumnBean.getFkMatrixcolumn()).append(":").append(fkFunctionoutput)
					.toString();
			MessageDigest md5 = MessageDigest.getInstance("MD5");
			md5.update(StandardCharsets.UTF_8.encode(oldcode));
			md5Code = String.format("%032x", new BigInteger(1, md5.digest()));
		} catch (NoSuchAlgorithmException e) {
			md5Code = e.getMessage().replace(" ", "");
		}
		uuid = new StringBuilder("snipplet-").append(id).append("-").append(orderNr).append("-").append(md5Code)
				.toString();
		this.id = id;
		this.orderNr = orderNr;
		this.markdowntext = markdowntext;
		this.fkFunctionoutput = fkFunctionoutput;
		this.matrixcolumnBean = matrixcolumnBean;
		this.escape = true;
		codeLines = new ArrayList<>();
	}

	/**
	 * @return true if this is a markdown snipplet, false otherwise
	 */
	public boolean getIsMarkdown() {
	  return type == null ? false : SnipplettypeBean.MARKDOWN.equals(type.getSnipplettype());
	}
	
	/**
	 * @return the content
	 */
	public String getContent() {
		StringBuilder buf = new StringBuilder();
		buf.append(markdowntext);
		for (CodelineBean clb : codeLines) {
			buf.append(clb.getResult());
		}
		return buf.toString();
	}

	/**
	 * get unique id of this bean
	 * 
	 * @return the unique id
	 */
	public String getUniqueId() {
		return uuid;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(id).append("[");
		builder.append(orderNr).append(",").append(type);
		builder.append("]:").append(matrixcolumnBean == null ? null : matrixcolumnBean.getFkMatrixcolumn()).append(":").append(fkFunctionoutput);
		return builder.toString();
	}

	@Override
	public int compareTo(SnippletBean o) {
		return orderNr == null || o == null || o.getOrderNr() == null ? 0 : orderNr.compareTo(o.getOrderNr());
	}
  
  /**
   * @param json the json representation of the bean
   */
  public void setMatrixcolumnBeanJson(String json) {
    this.matrixcolumnBean = new MatrixcolumnBeanConverter().getAsObject(null, null, json);
  }
  
  /**
   * @return the json representation of matrixcolumnbean
   */
  public String getMatrixcolumnBeanJson() {
    return matrixcolumnBean == null ? null : matrixcolumnBean.toJson();
  }

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the order number
	 */
	public Integer getOrderNr() {
		return orderNr;
	}

	/**
	 * @param orderNr the order number
	 */
	public void setOrderNr(Integer orderNr) {
		this.orderNr = orderNr;
	}

	/**
	 * @return the type
	 */
	public SnipplettypeBean getType() {
		return type;
	}

	/**
	 * @param type the type
	 */
	public void setType(SnipplettypeBean type) {
		this.type = type;
	}

	/**
	 * @param markdowntext the markdown text
	 * @param escape the escape
	 */
	public void setMarkdowntext(String markdowntext, boolean escape) {
		this.markdowntext = markdowntext;
		this.escape = escape;
	}

	/**
	 * @return the right id
	 */
	public Integer getRightId() {
		return rightId;
	}

	/**
	 * @return the code lines
	 */
	public List<CodelineBean> getCodeLines() {
		return codeLines;
	}

	/**
	 * @return the orderNrNew
	 */
	public Integer getOrderNrNew() {
		return orderNrNew;
	}

	/**
	 * @param orderNrNew the orderNrNew to set
	 */
	public void setOrderNrNew(Integer orderNrNew) {
		this.orderNrNew = orderNrNew;
	}

	/**
	 * @return the escape
	 */
	public Boolean getEscape() {
		return escape;
	}

	/**
	 * @param escape the escape to set
	 */
	public void setEscape(Boolean escape) {
		this.escape = escape;
	}

	/**
	 * @return the markdowntext
	 */
	public String getMarkdowntext() {
		return markdowntext;
	}

	/**
	 * @return the function output
	 */
	public Integer getFkFunctionoutput() {
		return fkFunctionoutput;
	}

	/**
	 * @param markdowntext the markdowntext to set
	 */
	public void setMarkdowntext(String markdowntext) {
		this.markdowntext = markdowntext;
	}

	/**
	 * @param fkFunctionoutput the fkFunctionoutput to set
	 */
	public void setFkFunctionoutput(Integer fkFunctionoutput) {
		this.fkFunctionoutput = fkFunctionoutput;
	}

  /**
   * @return the matrixcolumnBean
   */
  public MatrixcolumnParameterBean getMatrixcolumnBean() {
    return matrixcolumnBean;
  }

  /**
   * @param matrixcolumnBean the matrixcolumnBean to set
   */
  public void setMatrixcolumnBean(MatrixcolumnParameterBean matrixcolumnBean) {
    this.matrixcolumnBean = matrixcolumnBean;
  }
}
