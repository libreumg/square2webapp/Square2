package square2.modules.model.report.design;

import de.ship.dbppsquare.square.enums.EnumAccesslevel;
import square2.modules.model.AclBean;
import square2.modules.model.ProfileBean;

/**
 * 
 * @author henkej
 *
 */
public class AccessBean extends ProfileBean {
	private static final long serialVersionUID = 1L;

	private AclBean acl;

	/**
	 * create new access bean
	 * 
	 * @param read
	 *          the read
	 * @param write
	 *          the write
	 * @param execute
	 *          the execute
	 */
	public AccessBean(Boolean read, Boolean write, Boolean execute) {
		this.acl = new AclBean(read, write, execute);
	}

	/**
	 * create new access bean
	 * 
	 * @param acl
	 *          the acl
	 */
	public AccessBean(String acl) {
		this.acl = new AclBean(acl);
	}

	/**
	 * create new access bean
	 * 
	 * @param acl
	 *          the acl
	 */
	public AccessBean(EnumAccesslevel acl) {
		this.acl = new AclBean(acl == null ? "" : acl.getLiteral());
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append(super.toString()).append("access=").append(acl);
		return buf.toString();
	}

	/**
	 * @return the acl
	 */
	public AclBean getAcl() {
		return acl;
	}
}
