package square2.modules.model.report.design;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import square2.db.control.AnalysisGateway;
import square2.db.control.ReportGateway;
import square2.db.control.SnippletGateway;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.ModelException;
import square2.modules.model.SquareModel;
import square2.modules.model.report.analysis.AnalysisMatrix;
import square2.modules.model.report.analysis.AnalysisTemplate;
import square2.modules.model.template.UsageBean;
import square2.modules.model.template.report.MatrixcolumnParameterBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class ReportDesignModel extends SquareModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(ReportDesignModel.class);

	private Map<Integer, AccessBean> newUsers;
	private List<ReportDesignBean> reports;
	private List<SnipplettypeBean> snipplettypes;
	private List<MatrixcolumnParameterBean> rExports;
	private List<AnalysisMatrix> analysisMatrixList;
	private List<AnalysisTemplate> templates;
	private AccessBean newUser;
	private ReportDesignBean report;
	private Integer tabindex;
	private String newReportName;
	private Integer newSnippletTypeId;
	private List<UsageBean> usage;

	@Override
	public void setAllSquareUsers(SquareFacesContext facesContext, Object... params)
			throws ModelException, DataAccessException {
		if (params == null || params.length < 1 || (!(params[0] instanceof Integer))) {
			throw new ModelException("params must contain privilegeId as first element");
		} else {
			resetAllSquareusers(new ReportGateway(facesContext).getAllSquareUsersWithoutUserPrivilege((Integer) params[0]));
		}
	}

	/**
	 * reset reports
	 * 
	 * @param respectVariableGroupPrivileges flag to determine if variable group
	 * privileges should be used
	 * @param chooseTemplates flag to determine if templates should be chosen
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean resetReports(Boolean respectVariableGroupPrivileges, Boolean chooseTemplates,
			SquareFacesContext facesContext) {
		try {
			newUsers = new HashMap<>();
			Integer pagelength = facesContext.getProfile().getConfigInteger("config.template.paginator.pagelength", 20);
			report = new ReportDesignBean(new AclBean(), null, pagelength, report == null ? null : report.getLastchange(),
					report == null ? 0 : report.getOffset());
			ReportGateway gw = new ReportGateway(facesContext);
			AnalysisGateway agw = new AnalysisGateway(facesContext);
			analysisMatrixList = agw.getAllAnalysisMatrixes(null);
			templates = agw.getAllExecuteableAnalysisTemplates();
			if (chooseTemplates) {
				reports = gw.getAllReportTemplates();
			} else {
				reports = gw.getAllReportsOld(chooseTemplates, respectVariableGroupPrivileges);
//				gw.getAllReportTemplates();
			}
			for (ReportDesignBean bean : reports) {
				newUsers.put(bean.getId(), new AccessBean(false, false, false));
				// keep offset
				bean.setOffset(report.getOffset());
			}
			return true;
		} catch (DataAccessException e) {
			reports = new ArrayList<>();
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * reset the edit panel
	 * 
	 * @param reportId the report id
	 * @param tabindex the tab index
	 * @param facesContext the context of this function call
	 * @param warnAboutMissingCalculations if true, send a warning if there are no
	 * calculations for this report
	 * @param useTemplatesOnly if true, load templates, if not, load report
	 * @param offset the offset for the paginator
	 * @return true or false
	 */
	public boolean resetPageEdit(Integer reportId, Integer tabindex, SquareFacesContext facesContext,
			boolean warnAboutMissingCalculations, boolean useTemplatesOnly, Integer offset) {
		this.tabindex = tabindex;
		newUsers = new HashMap<>();
		try {
			snipplettypes = new SnippletGateway(facesContext).getAllTypes();
			try {
				if (useTemplatesOnly) {
					report = new ReportGateway(facesContext).getReportTemplate(reportId);
				} else {
					report = new ReportGateway(facesContext).getReportWithResults(reportId);
				}
			} catch (DataAccessException e) {
				facesContext.notifyException(e);
				LOGGER.error(e.getMessage(), e);
				throw new DataAccessException("error.report.edit.calculation.still.running");
			}
			if (report == null) {
				throw new DataAccessException("no report with id = " + reportId);
			} else {
				report.setOffset(offset);
			}
			rExports = new AnalysisGateway(facesContext).getMatrixcolumnParameterBeans(report.getId());
			analysisMatrixList = new AnalysisGateway(facesContext).getAllAnalysisMatrixes(null);
			for (SnippletBean bean : report.getSnipplets()) {
				newUsers.put(bean.getId(), new AccessBean(false, false, false));
			}
			newUser = new AccessBean(false, false, false);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * create a report template
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean createReport(SquareFacesContext facesContext) {
		try {
			Integer id = new ReportGateway(facesContext).createReportTemplate(report.getName(), report.getFkFunctionlist(),
					report.getDescription());
			report.setId(id);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * remove a report
	 * 
	 * @param reportId the report id
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean removeReport(Integer reportId, SquareFacesContext facesContext) {
		try {
			new ReportGateway(facesContext).removeReport(reportId);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * get the analysis matrix name
	 * 
	 * @param analysisMatrixId the id of the report
	 * @param facesContext the context of this function call
	 * @return the name of the report or the string "???"
	 */
	public String getAnalysisMatrixName(Integer analysisMatrixId, SquareFacesContext facesContext) {
		for (AnalysisMatrix bean : analysisMatrixList) {
			if (bean.getId().equals(analysisMatrixId)) {
				return bean.getName();
			}
		}
		facesContext.notifyError("analysis matrix {0} not found", analysisMatrixId);
		return "???";
	}

	/**
	 * remove a snipplet
	 * 
	 * @param snipplet the snipplet
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean removeSnipplet(SnippletBean snipplet, SquareFacesContext facesContext) {
		Iterator<SnippletBean> i = getReport().getSnipplets().iterator();
		while (i.hasNext()) {
			SnippletBean current = i.next();
			if (current.equals(snipplet)) {
				i.remove();
				return true;
			}
		}
		return false;
	}

	/**
	 * update a snipplet
	 * 
	 * @param snippletId the id of the snipplet
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean editSnipplet(Integer snippletId, SquareFacesContext facesContext) {
		SnippletBean bean = findSnippletById(snippletId);
		if (bean == null) {
			DataAccessException e = new DataAccessException("snipplet not found with id = " + snippletId);
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
		try {
			new SnippletGateway(facesContext).update(bean);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * add a snipplet
	 * 
	 * @param facesContext the context of this function call
	 * @param snippletBean the snipplet bean to be used as reference
	 * @param prepend if true, add the snipplet before bean, if false, append it
	 * after; if null, append it at the end bean
	 * @return true or false
	 */
	public boolean doAddSnipplet(SquareFacesContext facesContext, SnippletBean snippletBean, Boolean prepend) {
		Integer orderNr = report.getNextSnippletOrderNr();
		if (prepend != null) {
			if (prepend) {
				orderNr = snippletBean.getOrderNr();
			} else {
				orderNr = snippletBean.getOrderNr() + 1;
			}
			report.createOrderGap(orderNr);
		}
		SnippletBean bean = new SnippletBean(null, orderNr, "", null,
				new MatrixcolumnParameterBean(null, null, null, null));
		SnipplettypeBean type = null;
		for (SnipplettypeBean t : snipplettypes) {
			if (t.getId().equals(newSnippletTypeId)) {
				type = t;
			}
		}
		bean.setType(type);
		report.getSnipplets().add(bean);
		if (prepend == null) {
			report.setLastPage();
		}
		report.getSnipplets().sort((o1, o2) -> o1 == null || o2 == null ? 0 : o1.compareTo(o2));
		return true;
	}

	/**
	 * add a user to all snipplets
	 * 
	 * @param usnr the id of the user
	 * @param facesContext the context of this function call
	 * @return true
	 */
	@Deprecated
	public boolean addUserToAllSnipplets(Integer usnr, SquareFacesContext facesContext) {
		// try
		// {
		facesContext.notifyWarning("feature deactivated to disable snipplet usermanagement");
		// new SnippletGateway().addUserToAllSnippletsOfReport(report.getId(), usnr);
		return true;
		// }
		// catch (DataAccessException e)
		// {
		// notifyException(e);
		// return false;
		// }
	}

	/**
	 * move a snipplet to newPosition
	 * 
	 * @param bean the snipplet bean
	 * @return true or false
	 */
	public boolean moveSnipplet(SnippletBean bean) {
		report.reorderSnipplets(bean);
		return true;
	}

	/**
	 * update a report with all of its snipplets and its definition
	 * 
	 * @param facesContext the context of this function call
	 * @param definitionOnly if true, do updates on name and description of the
	 * report only; if false, do update on the snipplets also
	 * @return true or false
	 */
	public boolean updateReport(SquareFacesContext facesContext, Boolean definitionOnly) {
		try {
			for (SnippletBean bean : report.getSnipplets()) {
				if ("rexport".equals(bean.getType().getHtmlComponent())) {
					Integer fkMatrixcolumn = bean.getMatrixcolumnBean() == null ? null
							: bean.getMatrixcolumnBean().getFkMatrixcolumn();
					if (fkMatrixcolumn == null) {
						facesContext.notifyError("error.template.report.missingmatrixcolumn", bean.getOrderNr());
						return false;
					} else if (bean.getFkFunctionoutput() == null) {
						String s = new StringBuilder("function ").append(fkMatrixcolumn).toString();
						for (MatrixcolumnParameterBean b : getrExports()) {
							if (b.getFkMatrixcolumn().equals(fkMatrixcolumn)) {
								s = b.getName();
							}
						}
						facesContext.notifyError("error.template.report.missingfunctionoutput", s, bean.getOrderNr());
						return false;
					}
				}
			}
			Integer amount = new ReportGateway(facesContext).updateReport(report, definitionOnly);
			facesContext.notifyAffected(amount);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	protected SnippletBean findSnippletById(Integer snippletId) {
		for (SnippletBean bean : report.getSnipplets()) {
			if (bean.getId().equals(snippletId)) {
				return bean;
			}
		}
		return null;
	}

	/**
	 * find a snipplet by its order number
	 * 
	 * @param orderNr the order number
	 * @param downwards flag to determine the search direction
	 * @return the snipplet bean
	 */
	public SnippletBean findSnippletByOrderNr(Integer orderNr, Boolean downwards) {
		SnippletBean needle = null;
		for (SnippletBean bean : report.getSnipplets()) {
			if (downwards) {
				if (bean.getOrderNr() > orderNr) {
					if (needle == null) {
						needle = bean;
					} else if (bean.getOrderNr() < needle.getOrderNr()) {
						needle = bean;
					}
				}
			} else {
				if (bean.getOrderNr() < orderNr) {
					if (needle == null) {
						needle = bean;
					} else if (bean.getOrderNr() > needle.getOrderNr()) {
						needle = bean;
					}
				}
			}
		}
		return needle;
	}

	/**
	 * set the snipplet type of a snipplet bean
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the bean
	 */
	public void setSnippletTypeOfBean(SquareFacesContext facesContext, SnippletBean bean) {
		try {
			new SnippletGateway(facesContext).updateSnippletType(bean);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * set the report to the bean from json
	 * 
	 * @param json the json
	 */
	public void setReportJson(String json) {
		this.report = json == null ? null : new ReportDesignBean(json);
	}

	/**
	 * get the json
	 * 
	 * @return the json
	 */
	public String getReportJson() {
		return report == null ? null : report.toJson();
	}

	/**
	 * @return the new users
	 */
	public Map<Integer, AccessBean> getNewUsers() {
		return newUsers;
	}

	/**
	 * @return the reports
	 */
	public List<ReportDesignBean> getReports() {
		return reports;
	}

	/**
	 * @return the snipplet types
	 */
	public List<SnipplettypeBean> getSnipplettypes() {
		return snipplettypes;
	}

	/**
	 * @return the tab index
	 */
	public Integer getTabindex() {
		return tabindex;
	}

	/**
	 * @param tabindex the tab index
	 */
	public void setTabindex(Integer tabindex) {
		this.tabindex = tabindex;
	}

	/**
	 * @return the R exports
	 */
	public List<MatrixcolumnParameterBean> getrExports() {
		return rExports;
	}

	/**
	 * @return the report bean
	 */
	public ReportDesignBean getReport() {
		return report;
	}

	/**
	 * @param report the report bean
	 */
	public void setReport(ReportDesignBean report) {
		this.report = report;
	}

	/**
	 * @return the analysis matrix list
	 */
	public List<AnalysisMatrix> getAnalysisMatrixList() {
		return analysisMatrixList;
	}

	/**
	 * @return the new user
	 */
	public AccessBean getNewUser() {
		return newUser;
	}

	/**
	 * @return the new report name
	 */
	public String getNewReportName() {
		return newReportName;
	}

	/**
	 * @param newReportName the new report name
	 */
	public void setNewReportName(String newReportName) {
		this.newReportName = newReportName;
	}

	/**
	 * @return the new snipplet type id
	 */
	public Integer getNewSnippletTypeId() {
		return newSnippletTypeId;
	}

	/**
	 * @param newSnippletTypeId the new snipplet type id
	 */
	public void setNewSnippletTypeId(Integer newSnippletTypeId) {
		this.newSnippletTypeId = newSnippletTypeId;
	}

	/**
	 * @return the templates
	 */
	public List<AnalysisTemplate> getTemplates() {
		return templates;
	}

	/**
	 * @return the usages
	 */
	public List<UsageBean> getUsage() {
		return usage;
	}

	/**
	 * @param usage the usages
	 */
	public void setUsage(List<UsageBean> usage) {
		this.usage = usage;
	}
}
