package square2.modules.model.report.factory;

import java.util.Date;

import square2.modules.model.ProfileBean;
import square2.modules.model.datamanagement.ReleaseBean;
import square2.modules.model.report.design.ReportDesignBean;

/**
 * 
 * @author henkej
 *
 */
public class LatexresultBean {
	private Integer id;
	private ReportDesignBean report;
	private ReleaseBean release;
	private Date executionDate;
	private ProfileBean user;

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *          the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the report
	 */
	public ReportDesignBean getReport() {
		return report;
	}

	/**
	 * @param report
	 *          the report to set
	 */
	public void setReport(ReportDesignBean report) {
		this.report = report;
	}

	/**
	 * @return the release
	 */
	public ReleaseBean getRelease() {
		return release;
	}

	/**
	 * @param release
	 *          the release to set
	 */
	public void setRelease(ReleaseBean release) {
		this.release = release;
	}

	/**
	 * @return the executionDate
	 */
	public Date getExecutionDate() {
		return executionDate;
	}

	/**
	 * @param executionDate
	 *          the executionDate to set
	 */
	public void setExecutionDate(Date executionDate) {
		this.executionDate = executionDate;
	}

	/**
	 * @return the user
	 */
	public ProfileBean getUser() {
		return user;
	}

	/**
	 * @param user
	 *          the user to set
	 */
	public void setUser(ProfileBean user) {
		this.user = user;
	}
}
