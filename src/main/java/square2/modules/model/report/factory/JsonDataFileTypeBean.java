package square2.modules.model.report.factory;

/**
 * 
 * @author henkej
 *
 */
public class JsonDataFileTypeBean {
	private final String key;
	private final String mimeType;
	private final String description;

	/**
	 * @param key
	 *          the key
	 * @param mimeType
	 *          the mime type
	 * @param description
	 *          the description
	 */
	public JsonDataFileTypeBean(String key, String mimeType, String description) {
		super();
		this.key = key;
		this.mimeType = mimeType;
		this.description = description;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
}
