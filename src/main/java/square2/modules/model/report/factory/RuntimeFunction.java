package square2.modules.model.report.factory;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 
 * @author henkej
 *
 */
public class RuntimeFunction {
	private String name;
	private final Map<String, Object> params;

	/**
	 * create new runtime function
	 */
	public RuntimeFunction() {
		params = new LinkedHashMap<>();
	}

	/**
	 * get the param of key
	 * 
	 * @param key
	 *          the key
	 * @return the param
	 */
	public Object getParam(String key) {
		return params.get(key);
	}

	/**
	 * set the key value pair to params
	 * 
	 * @param key
	 *          the key
	 * @param value
	 *          the value
	 */
	public void setParam(String key, Object value) {
		params.put(key, value);
	}

	/**
	 * get all the parameter keys
	 * 
	 * @return the list of parameter keys
	 */
	public List<String> getParamKeys() {
		return new ArrayList<>(params.keySet());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the params
	 */
	public Map<String, Object> getParams() {
		return params;
	}
}
