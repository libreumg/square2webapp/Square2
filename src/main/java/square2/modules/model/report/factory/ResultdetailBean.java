package square2.modules.model.report.factory;

import java.util.Date;

/**
 * 
 * @author henkej
 *
 */
public class ResultdetailBean {
	private Integer resultdetailId;
	private Integer creator;
	private Integer fkVargroup;
	private Integer fkReport;
	private Integer fkRelease;
	private Date jobStart;
	private Date jobEnd;
	private Boolean jobFailed;

	/**
	 * @return the resultdetailId
	 */
	public Integer getResultdetailId() {
		return resultdetailId;
	}

	/**
	 * @param resultdetailId
	 *          the resultdetailId to set
	 */
	public void setResultdetailId(Integer resultdetailId) {
		this.resultdetailId = resultdetailId;
	}

	/**
	 * @return the creator
	 */
	public Integer getCreator() {
		return creator;
	}

	/**
	 * @param creator
	 *          the creator to set
	 */
	public void setCreator(Integer creator) {
		this.creator = creator;
	}

	/**
	 * @return the fkVargroup
	 */
	public Integer getFkVargroup() {
		return fkVargroup;
	}

	/**
	 * @param fkVargroup
	 *          the fkVargroup to set
	 */
	public void setFkVargroup(Integer fkVargroup) {
		this.fkVargroup = fkVargroup;
	}

	/**
	 * @return the fkReport
	 */
	public Integer getFkReport() {
		return fkReport;
	}

	/**
	 * @param fkReport
	 *          the fkReport to set
	 */
	public void setFkReport(Integer fkReport) {
		this.fkReport = fkReport;
	}

	/**
	 * @return the fkRelease
	 */
	public Integer getFkRelease() {
		return fkRelease;
	}

	/**
	 * @param fkRelease
	 *          the fkRelease to set
	 */
	public void setFkRelease(Integer fkRelease) {
		this.fkRelease = fkRelease;
	}

	/**
	 * @return the jobStart
	 */
	public Date getJobStart() {
		return jobStart;
	}

	/**
	 * @param jobStart
	 *          the jobStart to set
	 */
	public void setJobStart(Date jobStart) {
		this.jobStart = jobStart;
	}

	/**
	 * @return the jobEnd
	 */
	public Date getJobEnd() {
		return jobEnd;
	}

	/**
	 * @param jobEnd
	 *          the jobEnd to set
	 */
	public void setJobEnd(Date jobEnd) {
		this.jobEnd = jobEnd;
	}

	/**
	 * @return the jobFailed
	 */
	public Boolean getJobFailed() {
		return jobFailed;
	}

	/**
	 * @param jobFailed
	 *          the jobFailed to set
	 */
	public void setJobFailed(Boolean jobFailed) {
		this.jobFailed = jobFailed;
	}
}
