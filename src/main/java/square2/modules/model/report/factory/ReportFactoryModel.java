package square2.modules.model.report.factory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.servlet.http.Part;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;
import org.rosuda.REngine.REXP;
import org.rosuda.REngine.REXPMismatchException;
import org.rosuda.REngine.REngineException;
import org.rosuda.REngine.Rserve.RConnection;
import org.rosuda.REngine.Rserve.RserveException;

import com.google.gson.Gson;

import square2.db.control.AnalysisGateway;
import square2.db.control.CalculationGateway;
import square2.db.control.DatamanagementGateway;
import square2.db.control.ReportGateway;
import square2.db.control.VargroupGateway;
import square2.help.ContextKey;
import square2.help.SquareFacesContext;
import square2.modules.model.ModelException;
import square2.modules.model.SquareModel;
import square2.modules.model.datamanagement.ReleaseBean;
import square2.modules.model.report.analysis.AnalysisMatrix;
import square2.modules.model.report.design.AccessBean;
import square2.modules.model.report.design.ReportPeriodBean;
import square2.r.control.ExceptionRaiserForLog;
import square2.r.control.ExternalDataFile;
import square2.r.control.RExecutor;
import square2.r.control.RRunnable;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class ReportFactoryModel extends SquareModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(ReportFactoryModel.class);

	private List<ReleaseBean> filteredReleases;
	private List<ReleaseBean> releases;
	private List<AnalysisMatrix> reports;
	private List<AnalysisMatrix> emptyReports;
	private List<CalculationBean> calculations;
	private CalculationBean calculation;
	private Integer releaseId;
	private String refreshtime = "10000";
	private AccessBean newUser;
	private Boolean debugMode;
	private Map<Integer, List<Integer>> releaseReportMap;
	private String squareRunEdition;
	private List<String> squareRunEditions;
	private String isocode;
	private List<JsonDataFileTypeBean> externalReleaseTypes;
	private String externalReleaseType;
	private Part externalReleaseFile;
	private Part externalMetadataFile;

	@Override
	public void setAllSquareUsers(SquareFacesContext facesContext, Object... params)
			throws ModelException, DataAccessException {
		if (params == null || params.length < 1 || !(params[0] instanceof Integer)) {
			throw new ModelException("at least one param is needed that is an integer of the right_id");
		}
		Integer privilegeId = (Integer) params[0];
		super.resetAllSquareusers(new CalculationGateway(facesContext).getAllSquareUsersWithoutUserPrivilege(privilegeId));
	}

	/**
	 * load all threads from statistics module and set calculation bean
	 * 
	 * @param facesContext
	 *          the faces context of this function call
	 * @param calculation
	 *          the calculation bean
	 */
	public void toItem(SquareFacesContext facesContext, CalculationBean calculation) {
		this.calculation = calculation;
		squareRunEditions = new ArrayList<>();
		externalReleaseTypes = new ArrayList<>();
		try {
			List<String> orphanedFunctionNames = new ReportGateway(facesContext)
					.checkUnusedFunctions(calculation.getReport());
			if (orphanedFunctionNames.size() > 0) {
				facesContext.notifyWarning("warn.report.factory.orphanedfunctions", orphanedFunctionNames.toString());
			}
			RExecutor exec = new RExecutor(facesContext);
			exec.runTryEval("library(squareControl)");

			REXP result = exec.runTryEval("square.run.editions()");
			String[] strings = result.asStrings();
			for (String s : strings) {
				LOGGER.debug("found square.run.edition {}", s);
				squareRunEditions.add(s);
			}

			try {
				result = exec.runTryEval("square.supported_data_file_types()");
				String json = result.asString();
				for (JsonDataFileTypeBean s : parseJsonDataFileTypes(json)) {
					LOGGER.debug("found square data file type {}", s.getDescription());
					externalReleaseTypes.add(s);
				}
			} catch (REXPMismatchException e) {
				facesContext.notifyWarning("error on loading supported data file types: {0}", e.getMessage());
			}
		} catch (REngineException e) {
			StringBuilder buf = new StringBuilder();
			for (StackTraceElement ste : e.getStackTrace()) {
				buf.append("\n by ");
				buf.append(ste.getClassName());
				buf.append(".");
				buf.append(ste.getMethodName());
				buf.append(" (line ");
				buf.append(ste.getLineNumber());
				buf.append(")");
			}
			facesContext.notifyError(buf.toString());
			LOGGER.error(buf.toString(), e);
		} catch (IOException | REXPMismatchException e) {
			facesContext.notifyError(e.getMessage());
		}
	}

	/**
	 * parse json data file type
	 * 
	 * @param json
	 *          a json string
	 * @return the beans from the json
	 */
	private List<JsonDataFileTypeBean> parseJsonDataFileTypes(String json) {
		if (json == null || json.trim().length() < 1) {
			return new ArrayList<JsonDataFileTypeBean>();
		}
		JsonDataFileTypeBean[] array = new Gson().fromJson(json, JsonDataFileTypeBean[].class);
		List<JsonDataFileTypeBean> list = new ArrayList<>();
		for (JsonDataFileTypeBean bean : array) {
			list.add(bean);
		}
		return list;
	}

	/**
	 * reset the build list
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param reportId
	 *          the report id
	 * @return true or false
	 */
	public boolean resetBuildlist(SquareFacesContext facesContext, Integer reportId) {
		facesContext.notifyInformation("information.report.factory.create");
		try {
			CalculationGateway gw = new CalculationGateway(facesContext);
			releases = new DatamanagementGateway(facesContext).getAllReleases(true, false);
			releaseReportMap = gw.getReportReleaseMap();
			reports = new AnalysisGateway(facesContext).getAllAnalysisMatrixes(reportId);
			Integer variablegroup = new VargroupGateway(facesContext).getVariablegroupIdByReport(reportId);
			filteredReleases = filterReleasesByVariablegroup(variablegroup, releases, releaseReportMap);
			calculations = gw.getAllCalculations(reportId);
			Date lastchange = null;
			if (calculations.size() > 0) { // hack to get a lastchange of the current report
				lastchange = calculations.get(0).getLastchange();
			}
			emptyReports = gw.getAllReportsWithoutCalculation();
			calculation = new CalculationBean(reportId, lastchange);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	private List<ReleaseBean> filterReleasesByVariablegroup(Integer variablegroup, List<ReleaseBean> releases,
			Map<Integer, List<Integer>> reportReleaseMap) {
		List<ReleaseBean> filteredReleases = new ArrayList<>();
		filteredReleases.addAll(releases);
		if (variablegroup != null) {
			List<Integer> releaseIds = reportReleaseMap.get(variablegroup);
			Iterator<ReleaseBean> i = filteredReleases.iterator();
			while (i.hasNext()) {
				ReleaseBean bean = i.next();
				if (releaseIds == null) {
					i.remove();
				} else if (!releaseIds.contains(bean.getPk())) {
					i.remove();
				}
			}
		}
		return filteredReleases;
	}

	/**
	 * add the calculation interval
	 */
	public void addCalcuInterval() {
		calculation.getIntervals().add(calculation.getNewInterval());
		calculation.resetNewInterval();
	}

	/**
	 * remove the calculation interval
	 * 
	 * @param pk
	 *          the id
	 */
	public void removeCalcuInterval(Integer pk) {
		Iterator<ReportPeriodBean> iterator = calculation.getIntervals().iterator();
		while (iterator.hasNext()) {
			ReportPeriodBean bean = iterator.next();
			if ((bean.getPk() == null && pk == null) || bean.getPk().equals(pk)) {
				iterator.remove();
			}
		}
	}

	/**
	 * execute the calculation
	 * 
	 * @param facesContext
	 *          the faces context of this function call
	 * @return true if the calculation could be started, false otherwise
	 */
	public boolean calculate(SquareFacesContext facesContext) {
		try {
			Integer reportId = calculation.getReport();
			new CalculationGateway(facesContext).setCalculationStartTimeToNow(reportId);
			facesContext.notifyInformation("info.analysis.matrix.params.replaced",
					new AnalysisGateway(facesContext).ensureCorrectMatrixElementParameterByReport(reportId));
			List<ExternalDataFile> externalFiles = new ArrayList<>();
			StringBuilder buf = new StringBuilder("");
			if (debugMode) {
				buf.append("library(futile.logger)\n"); // otherwise, DEBUG as parameter for loglevel is unknown
			}
			String externalReleaseFileName = null;
			if (externalReleaseFile != null) {
				externalReleaseFileName = "external.release.file";
				externalFiles.add(new ExternalDataFile(externalReleaseFileName, externalReleaseFile.getInputStream()));
			}
			if (externalMetadataFile != null) {
				externalFiles.add(new ExternalDataFile("additional_data.zip", externalMetadataFile.getInputStream()));
			}
			buf.append("square.run(report = \"");
			buf.append(reportId);
			buf.append("\", dbconfigfile = \"");
			String configFile = (String) facesContext.getExternalContext().getApplicationMap()
					.get(ContextKey.RSERVERACCESS.get());
			buf.append(configFile);
			if (squareRunEdition != null) {
				buf.append("\", square.run.to.use = \"").append(squareRunEdition);
			}
			buf.append("\", isocode = \"").append(isocode);
			if (externalReleaseType != null && externalReleaseType.trim().length() > 0) {
				buf.append("\", data_file_type = \"").append(externalReleaseType);
				buf.append("\", data_file = \"").append(externalReleaseFileName);
			}
			if (debugMode) {
				buf.append("\", loglevel = futile.logger::DEBUG, usecache = FALSE)");
			} else {
				buf.append("\")");
			}

			List<String> list = new ArrayList<>();
			list.add("library(squareControl)");
			list.add(buf.toString());

			RExecutor executor = new RExecutor(facesContext);
			RConnection connection = executor.getConnection();
			RRunnable runnable = new RRunnable(connection, list, externalFiles, new ExceptionRaiserForLog());
			Thread t = new Thread(runnable);
			t.start();

			facesContext.notifyInformation("info.report.calculation.start");
			refreshtime = "10";
			return true;
		} catch (DataAccessException | NumberFormatException | RserveException | IOException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * get logfile from t_qs_calculation.runlog and return it as download stream
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 * 
	 */
	public boolean doLogFileDownload(SquareFacesContext facesContext) {
		String runlog;
		try {
			runlog = new CalculationGateway(facesContext).getRunlog(calculation.getReport());
			if (runlog == null || runlog.length() < 1) {
				throw new DataAccessException("no log available");
			}
			return generateDownloadStreamFromInputStream(facesContext, runlog.length(), "run.log", "text",
					new ByteArrayInputStream(runlog.getBytes(StandardCharsets.UTF_8)));
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * remove a release
	 * 
	 * @param id
	 *          the id
	 * @return true
	 */
	public boolean removeRelease(Integer id) {
		Iterator<ReleaseBean> iterator = calculation.getReleases().iterator();
		while (iterator.hasNext()) {
			ReleaseBean bean = iterator.next();
			if (bean.getPk().equals(id)) {
				iterator.remove();
			}
		}
		return true;
	}

	/**
	 * add release
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param bean
	 *          the release bean
	 * @return true
	 */
	public boolean addRelease(SquareFacesContext facesContext, ReleaseBean bean) {
		boolean forbid = false;
		for (ReleaseBean releaseBean : calculation.getReleases()) {
			if (releaseBean.getPk().equals(bean.getPk())) {
				forbid = true;
			}
		}
		if (!forbid) {
			calculation.getReleases().add(bean);
		} else {
			facesContext.notifyInformation("release {0} is still in list", bean.getName());
		}
		return true;
	}

	/**
	 * add a calculation
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean addCalculation(SquareFacesContext facesContext) {
		try {
			if (calculation.getReleases() == null || calculation.getReleases().size() < 1) {
				facesContext.notifyWarning("warning.reportfactory.add.calculation.norelease");
			}
			Integer affected = new CalculationGateway(facesContext).addCalculation(calculation);
			if (affected < 1) {
				facesContext.notifyWarning("warning.reportfactory.add.calculation.nochange");
			}
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * reset the report releases
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean resetReportReleases(SquareFacesContext facesContext) {
		try {
			new CalculationGateway(facesContext).resetReleases(calculation.getReport(), calculation.getReleases());
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * reload the calculaltion
	 * 
	 * @param facesContext
	 *          the context of this function call
	 */
	public void reloadCalculation(SquareFacesContext facesContext) {
		try {
			calculation = new CalculationGateway(facesContext).getCalculation(calculation.getReport(), false);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}

	/**
	 * @return the filteredReleases
	 */
	public List<ReleaseBean> getFilteredReleases() {
		return filteredReleases;
	}

	/**
	 * @param filteredReleases
	 *          the filteredReleases to set
	 */
	public void setFilteredReleases(List<ReleaseBean> filteredReleases) {
		this.filteredReleases = filteredReleases;
	}

	/**
	 * @return the releases
	 */
	public List<ReleaseBean> getReleases() {
		return releases;
	}

	/**
	 * @param releases
	 *          the releases to set
	 */
	public void setReleases(List<ReleaseBean> releases) {
		this.releases = releases;
	}

	/**
	 * @return the reports
	 */
	public List<AnalysisMatrix> getReports() {
		return reports;
	}

	/**
	 * @param reports
	 *          the reports to set
	 */
	public void setReports(List<AnalysisMatrix> reports) {
		this.reports = reports;
	}

	/**
	 * @return the emptyReports
	 */
	public List<AnalysisMatrix> getEmptyReports() {
		return emptyReports;
	}

	/**
	 * @param emptyReports
	 *          the emptyReports to set
	 */
	public void setEmptyReports(List<AnalysisMatrix> emptyReports) {
		this.emptyReports = emptyReports;
	}

	/**
	 * @return the calculations
	 */
	public List<CalculationBean> getCalculations() {
		return calculations;
	}

	/**
	 * @param calculations
	 *          the calculations to set
	 */
	public void setCalculations(List<CalculationBean> calculations) {
		this.calculations = calculations;
	}

	/**
	 * @return the calculation
	 */
	public CalculationBean getCalculation() {
		return calculation;
	}

	/**
	 * @param calculation
	 *          the calculation to set
	 */
	public void setCalculation(CalculationBean calculation) {
		this.calculation = calculation;
	}

	/**
	 * @return the releaseId
	 */
	public Integer getReleaseId() {
		return releaseId;
	}

	/**
	 * @param releaseId
	 *          the releaseId to set
	 */
	public void setReleaseId(Integer releaseId) {
		this.releaseId = releaseId;
	}

	/**
	 * @return the refreshtime
	 */
	public String getRefreshtime() {
		return refreshtime;
	}

	/**
	 * @param refreshtime
	 *          the refreshtime to set
	 */
	public void setRefreshtime(String refreshtime) {
		this.refreshtime = refreshtime;
	}

	/**
	 * @return the newUser
	 */
	public AccessBean getNewUser() {
		return newUser;
	}

	/**
	 * @param newUser
	 *          the newUser to set
	 */
	public void setNewUser(AccessBean newUser) {
		this.newUser = newUser;
	}

	/**
	 * @return the debugMode
	 */
	public Boolean getDebugMode() {
		return debugMode;
	}

	/**
	 * @param debugMode
	 *          the debugMode to set
	 */
	public void setDebugMode(Boolean debugMode) {
		this.debugMode = debugMode;
	}

	/**
	 * @return the releaseReportMap
	 */
	public Map<Integer, List<Integer>> getReleaseReportMap() {
		return releaseReportMap;
	}

	/**
	 * @param releaseReportMap
	 *          the releaseReportMap to set
	 */
	public void setReleaseReportMap(Map<Integer, List<Integer>> releaseReportMap) {
		this.releaseReportMap = releaseReportMap;
	}

	/**
	 * @return the squareRunEdition
	 */
	public String getSquareRunEdition() {
		return squareRunEdition;
	}

	/**
	 * @param squareRunEdition
	 *          the squareRunEdition to set
	 */
	public void setSquareRunEdition(String squareRunEdition) {
		this.squareRunEdition = squareRunEdition;
	}

	/**
	 * @return the squareRunEditions
	 */
	public List<String> getSquareRunEditions() {
		return squareRunEditions;
	}

	/**
	 * @param squareRunEditions
	 *          the squareRunEditions to set
	 */
	public void setSquareRunEditions(List<String> squareRunEditions) {
		this.squareRunEditions = squareRunEditions;
	}

	/**
	 * @return the isocode
	 */
	public String getIsocode() {
		return isocode;
	}

	/**
	 * @param isocode
	 *          the isocode to set
	 */
	public void setIsocode(String isocode) {
		this.isocode = isocode;
	}

	/**
	 * @return the externalReleaseTypes
	 */
	public List<JsonDataFileTypeBean> getExternalReleaseTypes() {
		return externalReleaseTypes;
	}

	/**
	 * @param externalReleaseTypes
	 *          the externalReleaseTypes to set
	 */
	public void setExternalReleaseTypes(List<JsonDataFileTypeBean> externalReleaseTypes) {
		this.externalReleaseTypes = externalReleaseTypes;
	}

	/**
	 * @return the externalReleaseType
	 */
	public String getExternalReleaseType() {
		return externalReleaseType;
	}

	/**
	 * @param externalReleaseType
	 *          the externalReleaseType to set
	 */
	public void setExternalReleaseType(String externalReleaseType) {
		this.externalReleaseType = externalReleaseType;
	}

	/**
	 * @return the externalReleaseFile
	 */
	public Part getExternalReleaseFile() {
		return externalReleaseFile;
	}

	/**
	 * @param externalReleaseFile
	 *          the externalReleaseFile to set
	 */
	public void setExternalReleaseFile(Part externalReleaseFile) {
		this.externalReleaseFile = externalReleaseFile;
	}

	/**
	 * @return the externalMetadataFile
	 */
	public Part getExternalMetadataFile() {
		return externalMetadataFile;
	}

	/**
	 * @param externalMetadataFile
	 *          the externalMetadataFile to set
	 */
	public void setExternalMetadataFile(Part externalMetadataFile) {
		this.externalMetadataFile = externalMetadataFile;
	}
}
