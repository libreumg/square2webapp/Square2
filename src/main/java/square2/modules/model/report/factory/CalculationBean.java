package square2.modules.model.report.factory;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import square2.modules.model.datamanagement.ReleaseBean;
import square2.modules.model.report.PerformanceBean;
import square2.modules.model.report.design.ReportPeriodBean;

/**
 * 
 * @author henkej
 *
 */
public class CalculationBean implements Serializable, Comparable<CalculationBean> {
	private static final long serialVersionUID = 1L;

	private final Integer report;
	private final Date lastchange;
	private final List<ReleaseBean> releases;
	private final List<ReportPeriodBean> intervals;
	private final List<PerformanceBean> performance;
	private Date started;
	private Date stopped;
	private ReportPeriodBean newInterval;
	private String reportName;

	/**
	 * create new calculation bean
	 * 
	 * @param report
	 *          the id of the report
	 * @param lastchange
	 *          the lastchange
	 */
	public CalculationBean(Integer report, Date lastchange) {
		this.report = report;
		this.lastchange = lastchange;
		this.releases = new ArrayList<>();
		this.intervals = new ArrayList<>();
		this.performance = new ArrayList<>();
		resetNewInterval();
	}

	@Override
	public int compareTo(CalculationBean o) {
		return report == null || o == null || o.getReport() == null ? 0 : report.compareTo(o.getReport());
	}

	/**
	 * reset the new interval
	 */
	public void resetNewInterval() {
		this.newInterval = new ReportPeriodBean(intervals.size());
	}

	/**
	 * @return the report
	 */
	public Integer getReport() {
		return report;
	}

	/**
	 * @return the started
	 */
	public Date getStarted() {
		return started;
	}

	/**
	 * @param started
	 *          the started to set
	 */
	public void setStarted(Date started) {
		this.started = started;
	}

	/**
	 * @return the stopped
	 */
	public Date getStopped() {
		return stopped;
	}

	/**
	 * @param stopped
	 *          the stopped to set
	 */
	public void setStopped(Date stopped) {
		this.stopped = stopped;
	}

	/**
	 * @return the newInterval
	 */
	public ReportPeriodBean getNewInterval() {
		return newInterval;
	}

	/**
	 * @param newInterval
	 *          the newInterval to set
	 */
	public void setNewInterval(ReportPeriodBean newInterval) {
		this.newInterval = newInterval;
	}

	/**
	 * @return the reportName
	 */
	public String getReportName() {
		return reportName;
	}

	/**
	 * @param reportName
	 *          the reportName to set
	 */
	public void setReportName(String reportName) {
		this.reportName = reportName;
	}

	/**
	 * @return the releases
	 */
	public List<ReleaseBean> getReleases() {
		return releases;
	}

	/**
	 * @return the intervals
	 */
	public List<ReportPeriodBean> getIntervals() {
		return intervals;
	}

	/**
	 * @return the performance
	 */
	public List<PerformanceBean> getPerformance() {
		return performance;
	}

	/**
	 * @return the lastchange
	 */
	public Date getLastchange() {
		return lastchange;
	}
}
