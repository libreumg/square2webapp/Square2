package square2.modules.model.report;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.ValidatorException;
import javax.inject.Named;
import javax.servlet.http.Part;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.JSONB;
import org.jooq.exception.DataAccessException;
import org.rosuda.REngine.REngineException;

import com.google.gson.JsonParseException;

import square2.db.control.AnalysisGateway;
import square2.db.control.CalculationGateway;
import square2.db.control.DatamanagementGateway;
import square2.db.control.ReportGateway;
import square2.db.control.converter.JSONBConverter;
import square2.help.ContextKey;
import square2.help.SquareFacesContext;
import square2.json.gson.ShipJsonParseException;
import square2.json.gson.SmiJsonConverter;
import square2.modules.model.ModelException;
import square2.modules.model.SquareModel;
import square2.modules.model.datamanagement.ReleaseBean;
import square2.modules.model.report.ajax.AjaxRequestBean;
import square2.modules.model.report.ajax.JsonBean;
import square2.modules.model.report.analysis.AnalysisFunctionBean;
import square2.modules.model.report.analysis.AnalysisFunctionPageBean;
import square2.modules.model.report.analysis.AnalysisVariablePageBean;
import square2.modules.model.report.analysis.AnalysisVariableParameterBean;
import square2.modules.model.report.analysis.AnalysisVarlistsPageBean;
import square2.modules.model.report.analysis.AnaparfunBean;
import square2.modules.model.report.analysis.AnaparfunFlagBean;
import square2.modules.model.report.analysis.MatrixBean;
import square2.modules.model.report.analysis.VariablegroupBean;
import square2.modules.model.report.design.ReportPeriodBean;
import square2.modules.model.report.design.SnippletBean;
import square2.modules.model.report.factory.JsonDataFileTypeBean;
import square2.modules.model.report.property.ReportPropertyBean;
import square2.modules.model.report.property.WidgetbasedBean;
import square2.r.control.SmiGateway;
import square2.r.control.SquareControl2Gateway;
import square2.r.control.SquareReportRendererGateway;
import square2.r.control.config.ConfigurationInterface;
import square2.r.control.config.SquareControl2Configuration;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class ReportModel extends SquareModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(ReportModel.class);

	private boolean anamatIsEmpty;
	private List<ReportBean> reports;
	private ReportBean report;
	private NamedReportBean showReportBean;
	private EditReportBean editReportBean;
	private NamedReportBean calculateReportBean;
	private String name;
	private List<VariablegroupBean> variablegroups;
	private List<ReportBean> parents;
	private List<ReleaseBean> filteredReleases;
	private ReportPeriodBean newInterval;
	private List<PerformanceBean> performance;
	private List<CalculationResultFileBean> calculationResults;
	private String isocode;
	private String squareRunEdition;
	private List<String> squareRunEditions;
	private List<JsonDataFileTypeBean> externalReleaseTypes;
	private String externalReleaseType;
	private Part externalReleaseFile;
	private Part externalMetadataFile;
	private Boolean debugMode;
	private MatrixBean matrixBean;
	private AnalysisFunctionPageBean function;
	private AnalysisVarlistsPageBean varlists;
	private String vargroupName;
	private Boolean hasNoResults;
	private String calculationTime;
	private String jsonMatrix;
	private AnaparfunFlagBean anaparfunFlags;
	private AnalysisFunctionBean analysisFunctionBean;
	private List<AnaparfunBean> anaparfuns;
	private AnalysisVariablePageBean variable;
	private Boolean asTemplate;
	private Set<String> usedReportNames;
	private List<SquareReportRendererFormatBean> formats;
	private MarkdownDownloadBean mdb;
	private List<AjaxRequestBean> ajaxRequests;
	private ReportPropertyBean newProperty;
	private ReportPropertyBean currentProperty;
	private Map<String, ReportPropertyBean> reportProperties;
	private List<WidgetbasedBean> reportPropertyWidgets;
	private Integer deleteCandidates;
	private List<FunctionSelectionBean> functionSelections;

	@Override
	public void setAllSquareUsers(SquareFacesContext facesContext, Object... params)
			throws ModelException, DataAccessException {
		// do nothing here
	}

	/**
	 * prepare the welcome page of reports module
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean toWelcome(SquareFacesContext facesContext) {
		return loadReports(facesContext);
	}

	/**
	 * prepare the add page
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the report bean
	 * @return true or false
	 */
	public boolean toAdd(SquareFacesContext facesContext, ReportBean bean) {
		try {
			loadLists(facesContext);
			report = bean;
			newInterval = new ReportPeriodBean(null);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.debug("{}", e.getMessage());
			return false;
		}
	}

	/**
	 * prepare the clone page
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean toClone(SquareFacesContext facesContext) {
		asTemplate = false;
		return loadReports(facesContext);
	}

	/**
	 * prepare the show page
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the report bean to be shown
	 * @return true or false
	 */
	public boolean toShow(SquareFacesContext facesContext, ReportBean bean) {
		// from https://gitlab.com/libreumg/square2webapp/Square2/-/issues/40
		String conffile = facesContext.getParam("squaredbconnectorfile", "/etc/squaredbconnector.properties");
		String section = facesContext.getParam("squaredbconnectorsection", "prod");
		String outputPath = asString(facesContext.getApplicationMapValue(ContextKey.SC_OUTPUT_PATH));
		ConfigurationInterface conf = new SquareControl2Configuration(conffile, section, bean.getPk(), outputPath);
		try (SquareReportRendererGateway srrg = new SquareReportRendererGateway(facesContext)) {
			ReportGateway gw = new ReportGateway(facesContext);
			Map<EnumShowNames, String> map = gw.getAllReportReferenceNames(bean);
			showReportBean = new NamedReportBean(bean, map.get(EnumShowNames.FUNCTIONLISTNAME),
					map.get(EnumShowNames.VARIABLEGROUPNAME), null);
			performance = gw.getPerformance(showReportBean.getPk());
			calculationResults = gw.getCalculationResults(showReportBean.getPk());
			formats = srrg.getFormats();
			mdb = new MarkdownDownloadBean();
			mdb.setFkReport(showReportBean.getPk());
			mdb.setDebug(false);
			mdb.setFilePath(true);
			mdb.setShowErrors(false);
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.debug("{}", e.getMessage());
			return false;
		}
		try(SquareControl2Gateway gw = new SquareControl2Gateway(facesContext)){
			String squareRunEdition = "SquareControlImpl"; // TODO: replace by a more flexible selection
			List<String> reportpaths = gw.callReports(squareRunEdition, conf);
			showReportBean.setReportPaths(reportpaths);
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.debug("{}", e.getMessage());
			return false;
		}
		try (SquareControl2Gateway gw = new SquareControl2Gateway(facesContext)) {
			showReportBean.setStats(gw.getStats(conf));
		} catch (Exception e) {
			facesContext.notifyException(new IOException("could not load statistics for this report"));
			LOGGER.debug("{}", e.getMessage());
		}
		return true;
	}

	/**
	 * prepare editing the report bean
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the report bean to be edited
	 * @return true or false
	 */
	public boolean toEdit(SquareFacesContext facesContext, ReportBean bean) {
		try {
			ReportGateway gw = loadLists(facesContext);
			Map<EnumShowNames, String> map = gw.getAllReportReferenceNames(bean);
			List<SnippletBean> snipplets = gw.getAllSnipplets(bean);
			Integer numberOfSelected = gw.getNumberOfSelectedVariables(bean.getPk());
			Integer numberOfTotal = gw.getNumberOfTotalVariables(bean.getFkVariablegroupdef());
			Integer pageLength = facesContext.getProfile().getConfigInteger("config.template.paginator.pagelength", 20);
			editReportBean = new EditReportBean(bean, map.get(EnumShowNames.FUNCTIONLISTNAME),
					map.get(EnumShowNames.VARIABLEGROUPNAME), snipplets, pageLength, 0, numberOfSelected, numberOfTotal);

			// other stuff
			List<ReleaseBean> releases = new DatamanagementGateway(facesContext).getAllReleases(true, false);
			Map<Integer, List<Integer>> reportReleaseMap = new CalculationGateway(facesContext).getReportReleaseMap();
			filteredReleases = filterReleasesByVariablegroup(editReportBean.getFkVariablegroupdef(), releases,
					reportReleaseMap);
			newInterval = new ReportPeriodBean(null);

			// the matrix stuff
			AnalysisGateway agw = new AnalysisGateway(facesContext);
			matrixBean = agw.getAnalysisMatrix(bean.getPk());
			variablegroups = agw.getAllVariablegroups(false, false, true);
			anamatIsEmpty = agw.getAnamatIsEmpty(bean.getPk());
			// deprecated code due to the new analysis matrix
			// function = new AnalysisFunctionPageBean(matrixBean,
			// agw.getAllFunctionBeansByMatrix(bean.getPk()));
			// function.setAnaparfunOutput(facesContext,
			// agw.getAllAnalysisFunctionOutputs(bean.getPk()));

			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * @return true or false
	 */
	public boolean getAnamatIsEmpty() {
		return anamatIsEmpty;
	}

	/**
	 * prepare editing the report bean
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the report bean to be edited
	 * @return true or false
	 */
	public boolean toEdit(SquareFacesContext facesContext, MatrixBean bean) {
		Integer pk = bean.getReport();
		try {
			ReportBean b = new ReportGateway(facesContext).getReport(pk);
			return toEdit(facesContext, b);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * prepare calculation page
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the report bean
	 * @return true or false
	 */
	public boolean toCalculate(SquareFacesContext facesContext, NamedReportBean bean) {
		calculateReportBean = bean;
		squareRunEditions = new ArrayList<>();
		externalReleaseTypes = new ArrayList<>();
		externalReleaseTypes.add(new JsonDataFileTypeBean("xlsx", "application/xlsx", "Excel"));
		// TODO: get possible types from SquareControl2 later, for now, only xlsx is
		// supported
		List<String> orphanedFunctionNames = new ReportGateway(facesContext).checkUnusedFunctions(bean.getPk());
		if (!orphanedFunctionNames.isEmpty()) {
			facesContext.notifyWarning("warn.report.factory.orphanedfunctions", orphanedFunctionNames.toString());
		}
		boolean result = false;
		try (SquareControl2Gateway gw = new SquareControl2Gateway(facesContext)) {
			Boolean listAll = facesContext.getProfile().isAdmin();
			JSONB wantedImplGroups = null;
			squareRunEditions = gw.getSquareRunEditions(listAll, wantedImplGroups);
			// default squareRunEdition is first hit
			if (squareRunEditions.size() > 0) {
				squareRunEdition = squareRunEditions.get(0);
			} else {
				facesContext.notifyWarning("warn.report.factory.zeroruneditions");
			}
			// TODO: implement or refactor?
			// externalReleaseTypes = gw.getExternalReleaseTypes();
			result = true;
		} catch (REngineException e) {
			StringBuilder buf = new StringBuilder();
			for (StackTraceElement ste : e.getStackTrace()) {
				buf.append("\n by ");
				buf.append(ste.getClassName());
				buf.append(".");
				buf.append(ste.getMethodName());
				buf.append(" (line ");
				buf.append(ste.getLineNumber());
				buf.append(")");
			}
			facesContext.notifyError(buf.toString());
			LOGGER.error(buf.toString(), e);
		} catch (Exception e) {
			facesContext.notifyWarning("error on loading supported data file types: {0}", e.getMessage());
			facesContext.notifyError(e.getMessage());
		}
		return result;
	}

	/**
	 * prepare the matrix page
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean toMatrix(SquareFacesContext facesContext) {
		varlists = new AnalysisVarlistsPageBean();
		if (ajaxRequests == null) {
			ajaxRequests = new ArrayList<>();
		} else {
			ajaxRequests.clear();
		}
		try(SmiGateway sgw = new SmiGateway(facesContext)) {
			// TODO: refactor for optimized speed
			varlists.getMatrix().addAll(new AnalysisGateway(facesContext)
					.getVarlistsMatrix(matrixBean.getVariablegroup().getId(), matrixBean.getReport()));
			vargroupName = matrixBean.getVariablegroup().getName();
			CalculationGateway gw = new CalculationGateway(facesContext);
			calculationTime = gw.getCalculationTimes(matrixBean.getReport());
			hasNoResults = !gw.hasResults(matrixBean.getReport());
			Integer id = matrixBean.getReport();
			JsonMatrixContainerBean jmb = sgw.getJsonMatrix(id);
			jsonMatrix = jmb.getJsonMatrix();
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * prepare the report layout properties page
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the report bean (need the report id)
	 * @return true or false
	 */
	public boolean toProperties(SquareFacesContext facesContext, ReportBean bean) {
		try {
			ReportGateway gw = new ReportGateway(facesContext);
			report = bean;
			reportProperties = gw.getAllProperties(bean.getPk());
			reportPropertyWidgets = gw.getAllWidgetbased();
			newProperty = new ReportPropertyBean(null, null).makeDirty();
			deleteCandidates = 0;
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * add the new property to the list and reset new property
	 * 
	 * @param facesContext the context of this function call
	 * 
	 * @return true or false
	 */
	public boolean addProperty(SquareFacesContext facesContext) {
		for (WidgetbasedBean rpw : reportPropertyWidgets) {
			if (rpw.getPk().equals(newProperty.getFkWidgetbased())) {
				newProperty.setKey(rpw.getName());
			}
		}
		List<Exception> exceptions = new ArrayList<>();
		if (!JSONBConverter.validateJSONB(exceptions, newProperty.getValue())) {
			for (Exception e : exceptions) {
				facesContext.notifyException(e);
			}
			return false;
		} else {
			reportProperties.put(newProperty.getKey(), newProperty);
			newProperty = new ReportPropertyBean(null, null).makeDirty();
			return true;
		}
	}

	/**
	 * update the current property in the list (and make it dirty)
	 * 
	 * @param facesContext the context of the function call
	 * @return true or false
	 */
	public boolean updateProperty(SquareFacesContext facesContext) {
		List<Exception> exceptions = new ArrayList<>();
		if (!JSONBConverter.validateJSONB(exceptions, currentProperty.getValue())) {
			for (Exception e : exceptions) {
				facesContext.notifyException(e);
			}
			return false;
		} else {
			currentProperty.makeDirty();
			reportProperties.put(currentProperty.getKey(), currentProperty);
			currentProperty = null;
			return true;
		}
	}

	/**
	 * remove a property from the list
	 * 
	 * @param bean the report property bean to be removed
	 * 
	 * @return true
	 */
	public boolean removeProperty(ReportPropertyBean bean) {
		Iterator<Entry<String, ReportPropertyBean>> iterator = reportProperties.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<String, ReportPropertyBean> found = iterator.next();
			if (found.getKey().equals(bean.getKey())) {
				iterator.remove();
				deleteCandidates += 1;
			}
		}
		return true;
	}

	/**
	 * approve the changes to the database
	 * 
	 * @param facesContext the contect of this function call
	 * @return true or false
	 */
	public boolean approveProperies(SquareFacesContext facesContext) {
		try {
			Integer amount = new ReportGateway(facesContext).upsertProperties(report.getPk(), reportProperties.values());
			facesContext.notifyAffected(amount);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * get the number of dirty properties
	 * 
	 * @return the number of dirty beans
	 */
	public int countDirtyProperties() {
		deleteCandidates = deleteCandidates == null ? 0 : deleteCandidates;
		Integer amount = deleteCandidates;
		if (reportProperties != null) {
			for (ReportPropertyBean p : reportProperties.values()) {
				amount += p.isDirty() ? 1 : 0;
			}
		}
		return amount;
	}

	/**
	 * add a report
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean doAddReport(SquareFacesContext facesContext) {
		try {
			if (report == null) {
				throw new DataAccessException("report is null");
			}
			String reportName = report.getName();
			String description = report.getDescription();
			Integer fkFunctionlist = report.getFkFunctionlist();
			Integer vg = report.getFkVariablegroupdef();
			ReportGateway gw = new ReportGateway(facesContext);
			Integer newId = gw.createReport(reportName, description, fkFunctionlist, vg);
			gw.addReleasesToReport(newId, report.getReleases());
			gw.addIntervalsToReport(newId, report.getIntervals());
			report = gw.getReport(newId);
			// go directly to the analysis matrix tab
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * clone a report
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean doCloneReport(SquareFacesContext facesContext) {
		try {
			ReportGateway gw = new ReportGateway(facesContext);
			Integer newId = gw.cloneReport(report, name, asTemplate);
			report = newId == null ? report : gw.getReport(newId); // if cloning to template, go back to the parent
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * update the report bean in editReportBean
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean doUpdateReport(SquareFacesContext facesContext) {
		try {
			Integer affected = new ReportGateway(facesContext).updateReport(editReportBean);
			facesContext.notifyAffected(affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * remove the report
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the report to be removed
	 * @return true or false
	 */
	public boolean doRemoveReport(SquareFacesContext facesContext, ReportBean bean) {
		try {
			ReportGateway gw = new ReportGateway(facesContext);
			List<ReportBean> children = gw.getAllReportsOfParent(bean.getPk());
			if (!children.isEmpty()) {
				for (ReportBean child : children) {
					facesContext.notifyError("error.report.delete.becauseofchild", bean.getName(), child.getName());
				}
				return false;
			}
			Integer affected = gw.removeReport(bean.getPk());
			facesContext.notifyAffected(affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * remove release from report without writing to database
	 * 
	 * @param facesContext the context of this function call
	 * @param releasePk the id of the release
	 * @return true or false
	 */
	public boolean doRemoveRelease(SquareFacesContext facesContext, Integer releasePk) {
		Iterator<ReleaseBean> i = editReportBean.getReleases().iterator();
		while (i.hasNext()) {
			if (i.next().getPk().equals(releasePk)) {
				i.remove();
			}
		}
		return true;
	}

	/**
	 * remove interval from report without writing to database
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the interval bean
	 * @return true or false
	 */
	public boolean doRemoveInterval(SquareFacesContext facesContext, ReportPeriodBean bean) {
		Iterator<ReportPeriodBean> i = editReportBean.getIntervals().iterator();
		while (i.hasNext()) {
			ReportPeriodBean b = i.next();
			if (b.getDateFrom().equals(bean.getDateFrom()) && b.getDateUntil().equals(bean.getDateUntil())) {
				i.remove();
			}
		}
		return true;
	}

	/**
	 * add interval to report without writing it to database
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean doAddInterval(SquareFacesContext facesContext) {
		editReportBean.getIntervals().add(newInterval);
		newInterval = new ReportPeriodBean(null); // just in case...
		return true;
	}

	/**
	 * add release
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the release bean
	 * @return true
	 */
	public boolean addRelease(SquareFacesContext facesContext, ReleaseBean bean) {
		for (ReleaseBean b : editReportBean.getReleases()) {
			if (b.getPk().equals(bean.getPk())) {
				facesContext.notifyWarning("release {0} is still in list", bean.getName());
			}
		}
		editReportBean.getReleases().add(bean);
		return true;
	}

	/**
	 * add release
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the release bean
	 * @return true
	 */
	public boolean addReleaseInEdit(SquareFacesContext facesContext, ReleaseBean bean) {
		boolean forbid = false;
		for (ReleaseBean releaseBean : calculateReportBean.getReleases()) {
			if (releaseBean.getPk().equals(bean.getPk())) {
				forbid = true;
			}
		}
		if (!forbid) {
			calculateReportBean.getReleases().add(bean);
		} else {
			facesContext.notifyInformation("release {0} is still in list", bean.getName());
		}
		return true;
	}

	private boolean loadReports(SquareFacesContext facesContext) {
		try {
			ReportGateway gw = new ReportGateway(facesContext);
			reports = gw.getAllReportsOf(facesContext.getUsnr());
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.debug("{}", e.getMessage());
			return false;
		}
	}

	private ReportGateway loadLists(SquareFacesContext facesContext) {
		ReportGateway gw = new ReportGateway(facesContext);
		parents = gw.getAllReports(true, true);
		usedReportNames = gw.getAllReportNames();
		variablegroups = new AnalysisGateway(facesContext).getAllVariablegroups(false, false, true);
		functionSelections = gw.getFunctionSelections();
		return gw;
	}

	protected List<ReleaseBean> filterReleasesByVariablegroup(Integer variablegroup, List<ReleaseBean> releases,
			Map<Integer, List<Integer>> reportReleaseMap) {
		List<ReleaseBean> list = new ArrayList<>();
		list.addAll(releases);
		if (variablegroup != null) {
			List<Integer> releaseIds = reportReleaseMap.get(variablegroup);
			Iterator<ReleaseBean> i = list.iterator();
			while (i.hasNext()) {
				ReleaseBean bean = i.next();
				if (releaseIds == null || !releaseIds.contains(bean.getPk())) {
					LOGGER.debug("remove release {} because releaseId is {}", bean.getPk(), releaseIds);
					i.remove();
				}
			}
		} else {
			list = new ArrayList<>(); // in case of no variable group, empty the list
		}
		return list;
	}

	/**
	 * generate markdown pdf
	 * 
	 * @param facesContext the context of this function call
	 * @param name the report name for the filename generation
	 * @return true or false
	 */
	public boolean doGenerateMarkdownResult(SquareFacesContext facesContext, String name) {
		try (SquareControl2Gateway gw = new SquareControl2Gateway(facesContext)) {
			gw.squareRender(mdb);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			e.printStackTrace();
			return false;
		}
	}

//	private boolean doGenerateByR(SquareFacesContext facesContext, String name) {
//		try {
//			SquareControl2Gateway gw = new SquareControl2Gateway(facesContext);	
//			byte[] byteArray = gw.squareRender(mdb);
//			StringBuilder buf = new StringBuilder();
//			if (name == null) {
//				name = "nameless_report";
//			}
//			buf.append(name.replaceAll("\\W+", "")).append(".").append(mdb.getFormat().getFileExtension());
//			generateDownloadStreamFromByteArray(facesContext, buf.toString(), mdb.getFormat().getMimeType(), byteArray);
//			return true;
//		} catch (DataAccessException | IOException | REXPMismatchException | REngineException e) {
//			facesContext.notifyException(e);
//			return false;
//		}
//	}

	/**
	 * get logfile from t_qs_calculation.runlog and return it as download stream
	 * 
	 * @param facesContext the context of this function call
	 * @param reportId the id of the report
	 * @return true or false
	 */
	public boolean doLogFileDownload(SquareFacesContext facesContext, Integer reportId) {
		String runlog;
		try {
			runlog = new CalculationGateway(facesContext).getRunlog(reportId);
			if (runlog == null || runlog.length() < 1) {
				facesContext.notifyError("no log available");
				return false;
			} else {
				return generateDownloadStreamFromInputStream(facesContext, runlog.length(), "run.log", "text",
						new ByteArrayInputStream(runlog.getBytes(StandardCharsets.UTF_8)));
			}
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * @param facesContext the context of this function call
	 * @param file the file container that contains the file
	 * @return true or false
	 */
	public boolean doDownloadFile(SquareFacesContext facesContext, CalculationResultFileBean file) {
		if (file == null) {
			facesContext.notifyError("the file container is empty");
			return false;
		} else if (file.getFileContent() == null) {
			facesContext.notifyError("file is empty");
			return false;
		} else {
			return generateDownloadStreamFromBase64String(facesContext, file.getFileContent().length(), file.getName(),
					file.getMimetype(), file.getFileContent());
		}
	}

	/**
	 * execute the calculation
	 * 
	 * @param facesContext the faces context of this function call
	 * @return true if the calculation could be started, false otherwise
	 */
	public boolean doCalculate(SquareFacesContext facesContext) {
		Integer reportId = calculateReportBean.getPk();
		String conffile = facesContext.getParam("squaredbconnectorfile", "/etc/squaredbconnector.properties");
		String section = facesContext.getParam("squaredbconnectorsection", "prod");
		String outputPath = asString(facesContext.getApplicationMapValue(ContextKey.SC_OUTPUT_PATH));
		ConfigurationInterface reportconfig = new SquareControl2Configuration(conffile, section, reportId, outputPath);
		if (calculateReportBean.getHasNoReleases()) {
			facesContext.notifyError(facesContext.translate("error.report.missing.release"));
			LOGGER.error(facesContext.translate("error.report.missing.release"));
			return false;
		}
		try (SquareControl2Gateway gw = new SquareControl2Gateway(facesContext)) {
			new CalculationGateway(facesContext).setCalculationStartTimeToNow(reportId);
			gw.callCalculation(squareRunEdition, reportconfig, externalReleaseFile);
			facesContext.notifyInformation("info.report.calculation.start");
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * @param o the object
	 * @return the string value of o or null
	 */
	private final String asString(Object o) {
		return o == null ? null : o.toString();
	}

	/**
	 * refresh flags of anaparfun
	 * 
	 * @param facesContext the context of this function call
	 * 
	 * @return true for a valid update, false otherwise
	 */
	public boolean doUpdateFunctionparams(SquareFacesContext facesContext) {
		try {
			analysisFunctionBean.synchronizeRequiredExecutions();
			Integer affected = new AnalysisGateway(facesContext).updateFunctionparams(analysisFunctionBean, anaparfunFlags,
					variable.getAnavarpars());
			facesContext.notifyAffected(affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * fill the defaults
	 * 
	 * @param paramName the name of the parameter
	 * @param value the value
	 * @param fillAll if true, fill all fields, if false, fill the empty ones only
	 * @param facesContext the context of this function call
	 * @return true
	 */
	public boolean doFillDefaults(String paramName, String value, boolean fillAll, SquareFacesContext facesContext) {
		int counter = 0;
		for (AnalysisVariableParameterBean b : variable.getAnavarpars()) {
			boolean overwrite = fillAll || (b.getValue() == null || b.getValue().isEmpty());
			if (overwrite && b.getName().equals(paramName)) {
				b.setValue(value);
				counter++;
			}
		}
		facesContext.notifyInformation("report.anamat.default.result", counter);
		return true;
	}

	/**
	 * delete all results of the report from t_qs_calculationresult
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean deleteResults(SquareFacesContext facesContext) {
		try {
			Integer affected = new CalculationGateway(facesContext).deleteResults(matrixBean.getReport());
			facesContext.notifyInformation("info.report.delete.results", affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * update jsonMatrix in the database
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean updateJsonMatrix(SquareFacesContext facesContext) {
		try {
			Integer reportId = matrixBean.getReport();
			Integer affected = new AnalysisGateway(facesContext).handleAjaxRequestBeans(ajaxRequests, reportId);
			facesContext.notifyInformation("info.affected", affected);
			return true;
		} catch (DataAccessException e) {
			LOGGER.error(e.getMessage(), e);
			facesContext.notifyException(e);
			return false;
		} catch (JsonParseException e) {
			facesContext.notifyException(e);
			LOGGER.error("json is: {}", jsonMatrix);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * reset the report releases
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean resetReportReleases(SquareFacesContext facesContext) {
		try {
			new CalculationGateway(facesContext).resetReleases(calculateReportBean.getPk(),
					calculateReportBean.getReleases());
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * remove a release
	 * 
	 * @param id the id
	 * @return true
	 */
	public boolean removeRelease(Integer id) {
		Iterator<ReleaseBean> iterator = calculateReportBean.getReleases().iterator();
		while (iterator.hasNext()) {
			ReleaseBean bean = iterator.next();
			if (bean.getPk().equals(id)) {
				iterator.remove();
			}
		}
		return true;
	}

	/**
	 * reload report bean from database
	 * 
	 * @param facesContext the context of this function call
	 * @param reportBean the bean that contains the id of the report
	 * @return the found named report bean or null
	 */
	public NamedReportBean reloadReport(SquareFacesContext facesContext, ReportBean reportBean) {
		try {
			return new ReportGateway(facesContext).getReport(reportBean.getPk());
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error("{} on loading report of bean {}", e.getMessage(), reportBean);
			return null;
		}
	}

	/**
	 * reload the calculation times
	 * 
	 * @param facesContext the context of this function call
	 */
	public void doReload(SquareFacesContext facesContext) {
		try {
			ReportBean bean = new ReportGateway(facesContext).getReportTimes(calculateReportBean.getPk());
			calculateReportBean.setStarted(bean.getStarted());
			calculateReportBean.setStopped(bean.getStopped());
			LocalDateTime now = LocalDateTime.now();
			String day = now.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
			String time = now.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
			facesContext.notifyInformation("info.update.with.time", day, time);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error("{} on loading calculateReportBean", e.getMessage());
		}
	}

	/**
	 * reset the calculation
	 * 
	 * @param facesContext the context of this function call
	 */
	public void doReset(SquareFacesContext facesContext) {
		try {
			new ReportGateway(facesContext).resetCalculation(calculateReportBean);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error("{} on resetting calculation", e.getMessage());
		}
	}

	/**
	 * return the generated report base path with all its prefixed folders
	 * 
	 * @param bean the report bean with its ID
	 * @param prefix the prefix
	 * @param suffix the suffix
	 * @return the path
	 */
	public String getReportPath(NamedReportBean bean, String prefix, String suffix) {
		StringBuilder buf = new StringBuilder();
		try {
			String path = ReportResultHelper.generateIdPaths(bean.getPk());
			buf.append(prefix).append(path).append(suffix);
			return buf.toString();
		} catch (NullPointerException e) {
			return "missing/report/id"; // dummy hack to make website working
		}
	}

	/**
	 * validate the report name
	 * 
	 * @param context the faces context of this function call
	 * @param component the component
	 * @param value the new value
	 */
	public void validateReportName(FacesContext context, UIComponent component, Object value) {
		SquareFacesContext fc = (SquareFacesContext) context;
		if (usedReportNames.contains(value.toString())) {
			throw new ValidatorException(new FacesMessage(FacesMessage.SEVERITY_ERROR, fc.translate("error.validation"),
					fc.translate("error.validation.unique", value)));
		}
	}

	/**
	 * add the json ajax request to the stack of requests
	 * 
	 * @param index the index of the ajax request - use for ordering only
	 * @param json the json ajax request
	 * @param cardinality the cardinality
	 * @param targetColumns the target columns
	 * @param targetTable the target table
	 * @param crud the db operation, one of insert, update or delete
	 * @return true if the parsing did work, false otherwise
	 * @throws ShipJsonParseException on parse exceptions
	 */
	public boolean addAjaxRequest(Double index, String json, String cardinality, String targetColumns, String targetTable,
			String crud) throws ShipJsonParseException {
		JsonBean jsonBean = new SmiJsonConverter().toAjaxRequestBean(json, cardinality, targetColumns, targetTable, crud,
				index);
		AjaxRequestBean bean = new AjaxRequestBean(jsonBean, cardinality, targetColumns, targetTable, crud, index);
		if (ajaxRequests == null) {
			ajaxRequests = new ArrayList<>();
		}
		ajaxRequests.add(bean);
		return true;
	}

	/**
	 * get all report property widgets that are not yet used in this list
	 * 
	 * @return the list of unused widgets; an empty one at least
	 */
	public List<WidgetbasedBean> getUnusedReportPropertyWidgets() {
		List<WidgetbasedBean> list = new ArrayList<>();
		for (WidgetbasedBean widget : reportPropertyWidgets) {
			if (!reportProperties.containsKey(widget.getName())) {
				list.add(widget);
			}
		}
		return list;
	}

	/**
	 * @param facesContext the faces contexet
	 * @return a list of releases
	 */
	public List<ReleaseBean> getFilteredReleasesByVariablegroup(SquareFacesContext facesContext) {
		List<ReleaseBean> releases = new DatamanagementGateway(facesContext).getAllReleases(true, false);
		Map<Integer, List<Integer>> reportReleaseMap = new CalculationGateway(facesContext).getReportReleaseMap();
		return filterReleasesByVariablegroup(report.getFkVariablegroupdef(), releases, reportReleaseMap);
	}

	/**
	 * @param facesContext the faces context
	 * @return the entry of squarev_url; an empty string if not found
	 */
	public String getSquarevUrl(SquareFacesContext facesContext) {
		return facesContext.getParam("squarev_url", "");
	}
	
	/**
	 * @return true if the list of run editions is > 1, false otherwise
	 */
	public boolean getHasMoreRunEditions() {
		return squareRunEditions != null ? squareRunEditions.size() > 1 : false;
	}

	/**
	 * @param json the json representation of the report
	 */
	public void setReportjson(String json) {
		this.report = new ReportBean(null).fromJson(json);
	}

	/**
	 * @return the json representation of the report
	 */
	public String getReportjson() {
		return report == null ? null : report.toJson();
	}

	/**
	 * @return the reports
	 */
	public List<ReportBean> getReports() {
		return reports;
	}

	/**
	 * @return the showReportBean
	 */
	public NamedReportBean getShowReportBean() {
		return showReportBean;
	}

	/**
	 * @return the editReportBean
	 */
	public EditReportBean getEditReportBean() {
		return editReportBean;
	}

	/**
	 * @return the variablegroups
	 */
	public List<VariablegroupBean> getVariablegroups() {
		return variablegroups;
	}

	/**
	 * @return the report
	 */
	public ReportBean getReport() {
		return report;
	}

	/**
	 * @param report the report to set
	 */
	public void setReport(ReportBean report) {
		this.report = report;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the parents
	 */
	public List<ReportBean> getParents() {
		return parents;
	}

	/**
	 * @return the filteredReleases
	 */
	public List<ReleaseBean> getFilteredReleases() {
		return filteredReleases;
	}

	/**
	 * @return the newInterval
	 */
	public ReportPeriodBean getNewInterval() {
		return newInterval;
	}

	/**
	 * @return the performance
	 */
	public List<PerformanceBean> getPerformance() {
		return performance;
	}

	/**
	 * @return the calculationResults
	 */
	public List<CalculationResultFileBean> getCalculationResults() {
		return calculationResults;
	}

	/**
	 * @return the calculateReportBean
	 */
	public NamedReportBean getCalculateReportBean() {
		return calculateReportBean;
	}

	/**
	 * @return the isocode
	 */
	public String getIsocode() {
		return isocode;
	}

	/**
	 * @param isocode the isocode to set
	 */
	public void setIsocode(String isocode) {
		this.isocode = isocode;
	}

	/**
	 * @return the squareRunEdition
	 */
	public String getSquareRunEdition() {
		return squareRunEdition;
	}

	/**
	 * @param squareRunEdition the squareRunEdition to set
	 */
	public void setSquareRunEdition(String squareRunEdition) {
		this.squareRunEdition = squareRunEdition;
	}

	/**
	 * @return the squareRunEditions
	 */
	public List<String> getSquareRunEditions() {
		return squareRunEditions;
	}

	/**
	 * @return the externalReleaseTypes
	 */
	public List<JsonDataFileTypeBean> getExternalReleaseTypes() {
		return externalReleaseTypes;
	}

	/**
	 * @return the debugMode
	 */
	public Boolean getDebugMode() {
		return debugMode;
	}

	/**
	 * @param debugMode the debugMode to set
	 */
	public void setDebugMode(Boolean debugMode) {
		this.debugMode = debugMode;
	}

	/**
	 * @return the externalReleaseType
	 */
	public String getExternalReleaseType() {
		return externalReleaseType;
	}

	/**
	 * @param externalReleaseType the externalReleaseType to set
	 */
	public void setExternalReleaseType(String externalReleaseType) {
		this.externalReleaseType = externalReleaseType;
	}

	/**
	 * @return the externalReleaseFile
	 */
	public Part getExternalReleaseFile() {
		return externalReleaseFile;
	}

	/**
	 * @param externalReleaseFile the externalReleaseFile to set
	 */
	public void setExternalReleaseFile(Part externalReleaseFile) {
		this.externalReleaseFile = externalReleaseFile;
	}

	/**
	 * @return the externalMetadataFile
	 */
	public Part getExternalMetadataFile() {
		return externalMetadataFile;
	}

	/**
	 * @param externalMetadataFile the externalMetadataFile to set
	 */
	public void setExternalMetadataFile(Part externalMetadataFile) {
		this.externalMetadataFile = externalMetadataFile;
	}

	/**
	 * @return the matrixBean
	 */
	public MatrixBean getMatrixBean() {
		return matrixBean;
	}

	/**
	 * @return the function
	 */
	public AnalysisFunctionPageBean getFunction() {
		return function;
	}

	/**
	 * @return the varlists
	 */
	public AnalysisVarlistsPageBean getVarlists() {
		return varlists;
	}

	/**
	 * @return the vargroupName
	 */
	public String getVargroupName() {
		return vargroupName;
	}

	/**
	 * @param vargroupName the vargroupName to set
	 */
	public void setVargroupName(String vargroupName) {
		this.vargroupName = vargroupName;
	}

	/**
	 * @return the hasNoResults
	 */
	public Boolean getHasNoResults() {
		return hasNoResults;
	}

	/**
	 * @param hasNoResults the hasNoResults to set
	 */
	public void setHasNoResults(Boolean hasNoResults) {
		this.hasNoResults = hasNoResults;
	}

	/**
	 * @return the calculationTime
	 */
	public String getCalculationTime() {
		return calculationTime;
	}

	/**
	 * @param calculationTime the calculationTime to set
	 */
	public void setCalculationTime(String calculationTime) {
		this.calculationTime = calculationTime;
	}

	/**
	 * @return the jsonMatrix
	 */
	public String getJsonMatrix() {
		return jsonMatrix;
	}

	/**
	 * @param jsonMatrix the jsonMatrix to set
	 */
	public void setJsonMatrix(String jsonMatrix) {
		this.jsonMatrix = jsonMatrix;
	}

	/**
	 * @return the anaparfuns
	 */
	public List<AnaparfunBean> getAnaparfuns() {
		return anaparfuns;
	}

	/**
	 * @param anaparfuns the anaparfuns to set
	 */
	public void setAnaparfuns(List<AnaparfunBean> anaparfuns) {
		this.anaparfuns = anaparfuns;
	}

	/**
	 * @return the analysis function flag bean
	 */
	public AnaparfunFlagBean getAnaparfunFlags() {
		return anaparfunFlags;
	}

	/**
	 * @return the analysis function bean
	 */
	public AnalysisFunctionBean getAnalysisFunctionBean() {
		return analysisFunctionBean;
	}

	/**
	 * @param analysisFunctionBean the analysis function bean
	 */
	public void setAnalysisFunctionBean(AnalysisFunctionBean analysisFunctionBean) {
		this.analysisFunctionBean = analysisFunctionBean;
	}

	/**
	 * @return the variable
	 */
	public AnalysisVariablePageBean getVariable() {
		return variable;
	}

	/**
	 * @return the asTemplate
	 */
	public Boolean getAsTemplate() {
		return asTemplate;
	}

	/**
	 * @param asTemplate the asTemplate to set
	 */
	public void setAsTemplate(Boolean asTemplate) {
		this.asTemplate = asTemplate;
	}

	/**
	 * @return the formats
	 */
	public List<SquareReportRendererFormatBean> getFormats() {
		return formats;
	}

	/**
	 * @return the mdb
	 */
	public MarkdownDownloadBean getMdb() {
		return mdb;
	}

	/**
	 * @param mdb the mdb to set
	 */
	public void setMdb(MarkdownDownloadBean mdb) {
		this.mdb = mdb;
	}

	/**
	 * @return the reportProperties
	 */
	public Collection<ReportPropertyBean> getReportProperties() {
		return reportProperties.values();
	}

	/**
	 * @return the newProperty
	 */
	public ReportPropertyBean getNewProperty() {
		return newProperty;
	}

	/**
	 * @param newProperty the newProperty to set
	 */
	public void setNewProperty(ReportPropertyBean newProperty) {
		this.newProperty = newProperty;
	}

	/**
	 * @return the reportPropertyWidgets
	 */
	public List<WidgetbasedBean> getReportPropertyWidgets() {
		return reportPropertyWidgets;
	}

	/**
	 * @return the deleteCandidates
	 */
	public Integer getDeleteCandidates() {
		return deleteCandidates;
	}

	/**
	 * @return the currentProperty
	 */
	public ReportPropertyBean getCurrentProperty() {
		return currentProperty;
	}

	/**
	 * @param currentProperty the currentProperty to set
	 */
	public void setCurrentProperty(ReportPropertyBean currentProperty) {
		this.currentProperty = currentProperty;
	}

	/**
	 * @return the functionSelections
	 */
	public List<FunctionSelectionBean> getFunctionSelections() {
		return functionSelections;
	}
}
