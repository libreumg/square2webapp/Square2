package square2.modules.model.report;

import java.math.BigDecimal;

/**
 * 
 * @author henkej
 *
 */
public class PerformanceBean {
	private String functionname;
	private Integer varmagnitude;
	private BigDecimal userself;
	private BigDecimal sysself;
	private BigDecimal elapsed;
	private BigDecimal userchild;
	private BigDecimal syschild;

	/**
	 * @return the functionname
	 */
	public String getFunctionname() {
		return functionname;
	}

	/**
	 * @param functionname
	 *          the functionname to set
	 */
	public void setFunctionname(String functionname) {
		this.functionname = functionname;
	}

	/**
	 * @return the varmagnitude
	 */
	public Integer getVarmagnitude() {
		return varmagnitude;
	}

	/**
	 * @param varmagnitude
	 *          the varmagnitude to set
	 */
	public void setVarmagnitude(Integer varmagnitude) {
		this.varmagnitude = varmagnitude;
	}

	/**
	 * @return the userself
	 */
	public BigDecimal getUserself() {
		return userself;
	}

	/**
	 * @param userself
	 *          the userself to set
	 */
	public void setUserself(BigDecimal userself) {
		this.userself = userself;
	}

	/**
	 * @return the sysself
	 */
	public BigDecimal getSysself() {
		return sysself;
	}

	/**
	 * @param sysself
	 *          the sysself to set
	 */
	public void setSysself(BigDecimal sysself) {
		this.sysself = sysself;
	}

	/**
	 * @return the elapsed
	 */
	public BigDecimal getElapsed() {
		return elapsed;
	}

	/**
	 * @param elapsed
	 *          the elapsed to set
	 */
	public void setElapsed(BigDecimal elapsed) {
		this.elapsed = elapsed;
	}

	/**
	 * @return the userchild
	 */
	public BigDecimal getUserchild() {
		return userchild;
	}

	/**
	 * @param userchild
	 *          the userchild to set
	 */
	public void setUserchild(BigDecimal userchild) {
		this.userchild = userchild;
	}

	/**
	 * @return the syschild
	 */
	public BigDecimal getSyschild() {
		return syschild;
	}

	/**
	 * @param syschild
	 *          the syschild to set
	 */
	public void setSyschild(BigDecimal syschild) {
		this.syschild = syschild;
	}
}
