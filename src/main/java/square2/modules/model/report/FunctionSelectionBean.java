package square2.modules.model.report;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class FunctionSelectionBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Integer pk;
	private final String name;

	/**
	 * @param pk the id of the function selection
	 * @param name the name of the function selection
	 */
	public FunctionSelectionBean(Integer pk, String name) {
		super();
		this.pk = pk;
		this.name = name;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
