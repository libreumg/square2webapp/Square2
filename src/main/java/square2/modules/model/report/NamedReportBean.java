package square2.modules.model.report;

import java.util.ArrayList;
import java.util.List;

import javax.faces.model.DataModel;

import square2.modules.model.report.design.DataModelSnippletBeans;
import square2.modules.model.report.design.SnippletBean;

/**
 * 
 * @author henkej
 *
 */
public class NamedReportBean extends ReportBean {
	private static final long serialVersionUID = 1L;

	private final String functionlistName;
	private final String variablegroupName;
	private DataModel<SnippletBean> snipplets;
	private final List<String> reportPaths;
	private ReportStatsBean stats;

	/**
	 * @param bean
	 *          the report
	 * @param functionlistName
	 *          the function list name
	 * @param variablegroupName
	 *          the variablegroup name
	 * @param snipplets
	 *          the snipplets
	 */
	public NamedReportBean(ReportBean bean, String functionlistName, String variablegroupName,
			List<SnippletBean> snipplets) {
		super(bean.getPk());
		super.setDescription(bean.getDescription());
		super.setFkFunctionlist(bean.getFkFunctionlist());
		super.setFkLatexdef(bean.getFkLatexdef());
		super.setFkParent(bean.getFkParent());
		super.setFkVariablegroupdef(bean.getFkVariablegroupdef());
		super.setLastchange(bean.getLastchange());
		super.setLatexcode(bean.getLatexcode());
		super.setName(bean.getName());
		super.setRunlog(bean.getRunlog());
		super.setRunsum(bean.getRunsum());
		super.setStarted(bean.getStarted());
		super.setStopped(bean.getStopped());
		super.getReleases().addAll(bean.getReleases());
		super.getIntervals().addAll(bean.getIntervals());
		this.functionlistName = functionlistName;
		this.variablegroupName = variablegroupName;
		this.snipplets = new DataModelSnippletBeans(snipplets == null ? new ArrayList<>() : snipplets);
		this.reportPaths = new ArrayList<>();
	}

	/**
	 * get sipplet bean with correct uuid
	 * 
	 * @param uuid
	 *          the id to seek for
	 * @return found snipplet bean or null
	 */
	public SnippletBean getSnipplet(String uuid) {
		for (SnippletBean bean : snipplets) {
			if (bean.getUniqueId().equals(uuid)) {
				return bean;
			}
		}
		return null;
	}

	/**
	 * @return the functionlistName
	 */
	public String getFunctionlistName() {
		return functionlistName;
	}

	/**
	 * @return the variablegroupName
	 */
	public String getVariablegroupName() {
		return variablegroupName;
	}

	/**
	 * @return the snipplets
	 */
	public DataModel<SnippletBean> getSnipplets() {
		return snipplets;
	}

	/**
	 * reset the report paths
	 * 
	 * @param reportPaths the new paths
	 */
	public void setReportPaths(List<String> reportPaths) {
		this.reportPaths.clear();
		this.reportPaths.addAll(reportPaths);
	}
	
	/**
	 * @return the reportPaths
	 */
	public List<String> getReportPaths() {
		return reportPaths;
	}

	/**
	 * @return the stats
	 */
	public ReportStatsBean getStats() {
		return stats;
	}

	/**
	 * @param stats the stats to set
	 */
	public void setStats(ReportStatsBean stats) {
		this.stats = stats;
	}
}
