package square2.modules.model.report;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class MarkdownDownloadBean implements Serializable {
  private static final long serialVersionUID = 1L;
  private Integer fkReport;
  private SquareReportRendererFormatBean format;
  private Boolean debug;
  private Boolean filePath;
  private Boolean showErrors;

  /**
   * :)
   */
  public MarkdownDownloadBean() {
    super();
  }

  /**
   * @param fkReport the id of the report
   * @param format the format of the output
   * @param debug if debug is on
   * @param dontsave if the result is not saved
   * @param showErrors if errors should be rendered
   */
  public MarkdownDownloadBean(Integer fkReport, SquareReportRendererFormatBean format, Boolean debug, Boolean dontsave,
      Boolean showErrors) {
    this.fkReport = fkReport;
    this.format = format;
    this.debug = debug;
    this.filePath = dontsave;
    this.showErrors = showErrors;
  }

  /**
   * @return the fkReport
   */
  public Integer getFkReport() {
    return fkReport;
  }

  /**
   * @param fkReport the fkReport to set
   */
  public void setFkReport(Integer fkReport) {
    this.fkReport = fkReport;
  }

  /**
   * @return the json representation of the format bean
   */
  public String getFormatJson() {
  	return format == null ? "[]" : format.toJson();
  }
  
  /**
   * @param json the json representation of the format
   */
  public void setFormatJson(String json) {
  	format = json == null || json.isEmpty() ? null : new SquareReportRendererFormatBean(null, null, null, null).fromJson(json);
  }
  
  /**
   * @return the format
   */
  public SquareReportRendererFormatBean getFormat() {
    return format;
  }

  /**
   * @param format the format to set
   */
  public void setFormat(SquareReportRendererFormatBean format) {
    this.format = format;
  }

  /**
   * @return the debug
   */
  public Boolean getDebug() {
    return debug;
  }

  /**
   * @param debug the debug to set
   */
  public void setDebug(Boolean debug) {
    this.debug = debug;
  }

  /**
   * @return the dontsave
   */
  public Boolean getFilePath() {
    return filePath;
  }

  /**
   * @param filePath the dontsave to set
   */
  public void setFilePath(Boolean filePath) {
    this.filePath = filePath;
  }

  /**
   * @return the showErrors
   */
  public Boolean getShowErrors() {
    return showErrors;
  }

  /**
   * @param showErrors the showErrors to set
   */
  public void setShowErrors(Boolean showErrors) {
    this.showErrors = showErrors;
  }
}
