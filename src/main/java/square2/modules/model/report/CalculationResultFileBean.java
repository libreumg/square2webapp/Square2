package square2.modules.model.report;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class CalculationResultFileBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private String name;
	private String fileContent;
	private String mimetype;

	/**
	 * get the size of file content
	 * 
	 * @return the size
	 */
	public Integer getSize() {
		return fileContent == null ? 0 : fileContent.length();
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fileContent
	 */
	public String getFileContent() {
		return fileContent;
	}

	/**
	 * @param fileContent
	 *          the fileContent to set
	 */
	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}

	/**
	 * @return the mimetype
	 */
	public String getMimetype() {
		return mimetype;
	}

	/**
	 * @param mimetype
	 *          the mimetype to set
	 */
	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}
}
