package square2.modules.model.report.analysis;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import square2.help.SquareFacesContext;
import square2.modules.model.AnaparfunOutputBean;
import square2.modules.model.RequiredExecutionsBean;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisFunctionPageBean {
	private static final Logger LOGGER = LogManager.getLogger(AnalysisFunctionPageBean.class);
	
	private MatrixBean matrix;
	private List<AnalysisFunctionBean> list;

	/**
	 * generate new AnalysisFunctionPageBean
	 * 
	 * @param matrix
	 *          the matrix for the analysis matrix subpage
	 * @param list
	 *          the list of all concrete functions (anaparfuns)
	 */
	public AnalysisFunctionPageBean(MatrixBean matrix, List<AnalysisFunctionBean> list) {
		super();
		this.matrix = matrix;
		this.list = list;
	}

	/**
	 * @return the list of the anaparfuns
	 */
	public List<AnalysisFunctionBean> getList() {
		return list;
	}

	/**
	 * @return the analysis matrix
	 */
	public MatrixBean getMatrix() {
		return matrix;
	}

	/**
	 * set all analysis function output beans
	 * 
	 * @param facesContext
	 *          the faces context of this function call
	 * @param analysisFunctionOutputs
	 *          the analysis function outputs from the db
	 */
	public void setAnaparfunOutput(SquareFacesContext facesContext, List<AnaparfunOutputBean> analysisFunctionOutputs) {
		for (AnalysisFunctionBean bean : list) {
			Map<Integer, Map<Integer, Boolean>> presets = bean.getRequiredExecutionsAsMap(facesContext);
			List<RequiredExecutionsBean> l = new ArrayList<>();
			for (AnaparfunOutputBean b : analysisFunctionOutputs) {
				if (!b.getFkAnaparfun().equals(bean.getAnaparfun())) {
					RequiredExecutionsBean newBean = new RequiredExecutionsBean(b.getFkAnaparfun(), b.getFkFunctionoutput(),
							b.getAnaparfunName(), b.getFunctionoutputName(), b.getOrderNr());
					if (presets != null) {
						Map<Integer, Boolean> foMap = presets.get(newBean.getFkAnaparfun());
						if (foMap != null) {
							Boolean chosen = foMap.get(newBean.getFkFunctionoutput());
							newBean.setChosen(chosen == null ? false : chosen);
						}
					}
					l.add(newBean);
				}
			}
			LOGGER.debug("current afb: {}, list is {}", bean.getName(), l.toString());
			bean.setRequiredExecutionsList(l);
		}
	}
}
