package square2.modules.model.report.analysis;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisVariable {
	private final String variableName;
	private final String translation;
	private final Integer anavar;
	private List<AnalysisVariableParameterBean> anavarpars;

	/**
	 * create new analysis variable
	 * 
	 * @param variableName
	 *          the name
	 * @param anavar
	 *          the id of the analysis variable
	 * @param translation
	 *          the translation
	 */
	public AnalysisVariable(String variableName, Integer anavar, String translation) {
		this.variableName = variableName;
		this.anavar = anavar;
		this.translation = translation;
		anavarpars = new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{variableName=").append(variableName);
		buf.append(", translation=").append(translation);
		buf.append(", anavar=").append(anavar);
		buf.append(", anavarpars=").append(anavarpars);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * @param bean
	 *          the bean
	 */
	public void setAnavarpar(AnalysisVariableParameterBean bean) {
		boolean found = false;
		for (AnalysisVariableParameterBean b : anavarpars) {
			if (b.getAnaparfun().equals(bean.getAnaparfun()) && b.getName().equals(bean.getName())
					&& b.getAnavar().equals(bean.getAnavar())) {
				b.setValue(bean.getValue());
				found = true;
			}
		}
		if (!found) {
			anavarpars.add(bean);
		}
	}

	/**
	 * @return the anavarpars
	 */
	public List<AnalysisVariableParameterBean> getAnavarpars() {
		return anavarpars;
	}

	/**
	 * @param anavarpars
	 *          the anavarpars to set
	 */
	public void setAnavarpars(List<AnalysisVariableParameterBean> anavarpars) {
		this.anavarpars = anavarpars;
	}

	/**
	 * @return the variableName
	 */
	public String getVariableName() {
		return variableName;
	}

	/**
	 * @return the translation
	 */
	public String getTranslation() {
		return translation;
	}

	/**
	 * @return the anavar
	 */
	public Integer getAnavar() {
		return anavar;
	}
}
