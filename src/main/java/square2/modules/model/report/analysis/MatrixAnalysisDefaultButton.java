package square2.modules.model.report.analysis;

import square2.modules.model.report.analysis.matrix.MatrixObject;

/**
 * 
 * @author henkej
 *
 */
public class MatrixAnalysisDefaultButton implements MatrixObject {
	private final AnalysisDefaultBean bean;

	/**
	 * create new matrix analysis default button
	 * 
	 * @param bean
	 *          the analysis default bean
	 */
	public MatrixAnalysisDefaultButton(AnalysisDefaultBean bean) {
		this.bean = bean;
	}

	/**
	 * @return the bean
	 */
	public AnalysisDefaultBean getBean() {
		return bean;
	}

	@Override
	public boolean getIsButton() {
		return true;
	}

	@Override
	public boolean getIsLabel() {
		return false;
	}

	@Override
	public boolean getIsInput() {
		return false;
	}

	@Override
	public boolean getIsIconButton() {
		return false;
	}

	@Override
	public boolean getIsDisabled() {
		return false;
	}

	@Override
	public boolean getIsTitle() {
		return false;
	}

	@Override
	public boolean getIsVariable() {
		return false;
	}
}
