package square2.modules.model.report.analysis;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.ship.dbppsquare.square.tables.records.TScaleRecord;
import square2.modules.model.statistic.FunctionBean;

/**
 * 
 * @author henkej
 *
 */
public class ParameterFunction implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final FunctionBean functionBean;
	private List<DefaultParameter> defaults;
	private Map<TScaleRecord, Boolean> vargroup;

	/**
	 * create new parameter function
	 * 
	 * @param functionBean
	 *          the function bean
	 */
	public ParameterFunction(FunctionBean functionBean) {
		this.functionBean = functionBean;
		defaults = new ArrayList<>();
		vargroup = new HashMap<>();
	}

	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{functionBean=").append(functionBean);
		buf.append(",defaults=").append(defaults);
		buf.append(",vargroup=").append(vargroup);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * @return the function bean
	 */
	public FunctionBean getFunctionBean() {
		return functionBean;
	}

	/**
	 * @return the defaults list
	 */
	public List<DefaultParameter> getDefaults() {
		return defaults;
	}

	/**
	 * @return the variable group
	 */
	public Map<TScaleRecord, Boolean> getVargroup() {
		return vargroup;
	}
}
