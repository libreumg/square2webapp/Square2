package square2.modules.model.report.analysis;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import ship.jsf.components.selectFilterableMenu.FilterableListInterface;
import square2.db.control.AnalysisGateway;
import square2.db.control.CalculationGateway;
import square2.db.control.ReportGateway;
import square2.help.SquareFacesContext;
import square2.modules.model.ModelException;
import square2.modules.model.ProfileBean;
import square2.modules.model.SquareModel;
import square2.modules.model.functionselection.FunctionselectionBean;
import square2.modules.model.report.analysis.matrix.MatrixCheckbox;
import square2.modules.model.report.analysis.matrix.MatrixObject;
import square2.modules.model.template.UsageBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class AnalysisModel extends SquareModel implements Serializable {
  private static final long serialVersionUID = 1L;

  private static final Logger LOGGER = LogManager.getLogger("Square2");

  private MatrixBean matrixBean;
  private AnalysisWelcomePageBean welcome;
  private AnalysisMainPageBean main;
  private AnalysisFunctionPageBean function;
  private AnalysisMatrixPageBean matrix;
  private AnalysisPrivilegePageBean privilege;
  private AnalysisTemplatePageBean template;
  private FunctionselectionBean templateBean;
  private AnalysisPreferencePageBean preference;
  private AnalysisVariablePageBean variable;
  private AnalysisVarlistsPageBean varlists;
  private FilterableListInterface templates;
  private List<VariablegroupBean> variablegroups;
  private List<ProfileBean> users;
  private Boolean comeFromTemplate;
  private String vargroupName;
  private List<AnalysisMatrixBean> list;
  private String currentBtnId;
  private Integer reportId;
  private List<UsageBean> usage;
  private String matrixfilter;
  private AnaparfunFlagBean anaparfunFlags;
  private AnalysisFunctionBean analysisFunctionBean;
  private List<AnavarparBean> anavarpars;
  private List<AnaparfunBean> anaparfuns;
  private Boolean hasNoResults;
  private String calculationTime;
  private String jsonMatrix;

  @Override
  public void setAllSquareUsers(SquareFacesContext facesContext, Object... params) throws ModelException {
    // not yet implemented
  }

  /**
   * create variable bean; used whenever a navigation to variable.jsf is needed
   * 
   * @param report id of report
   * @param anaparfun the id of the analysis function
   * @return true
   */
  public boolean createVariable(Integer report, Integer anaparfun) {
    variable = new AnalysisVariablePageBean(report, anaparfun);
    return true;
  }

  /**
   * create preference bean; used whenever a navigation to preference.jsf is needed
   * 
   * @param bean the analysis function bean
   * @return true
   */
  public boolean createPreference(AnalysisFunctionBean bean) {
    preference = new AnalysisPreferencePageBean(bean);
    return true;
  }

  /**
   * create template bean; used whenever a navigation to template.jsf is needed
   * 
   * @param pk ID of template
   * @param facesContext of this current function call
   * @return true if template could be loaded, false otherwise
   * @throws DataAccessException if anything went wrong on database side
   */
  public boolean createTemplate(Integer pk, SquareFacesContext facesContext) {
    try {
      template = new AnalysisTemplatePageBean(new AnalysisGateway(facesContext).getAnalysisTemplate(pk));
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage());
      return false;
    }

  }

  /**
   * (re-)load template bean from db
   * 
   * @param pk the id of the template
   * @param facesContext context of this function call
   * @return true for success, false otherwise
   * @throws DataAccessException if anything went wrong on database side
   */
  public boolean loadTemplateBean(Integer pk, SquareFacesContext facesContext) {
    try {
      templateBean = new AnalysisGateway(facesContext).getAnalysisTemplateBean(pk);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * load all analysis matrixes
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean loadAllAnalysisMatrixes(SquareFacesContext facesContext) {
    try {
      AnalysisGateway gw = new AnalysisGateway(facesContext);
      this.list = gw.getAnalysismatrixBeans();
      this.main = new AnalysisMainPageBean();
      this.variablegroups = gw.getAllVariablegroups(false, false, true);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * load all analysis templates
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean loadAllAnalysisTemplates(SquareFacesContext facesContext) {
    try {
      AnalysisGateway gw = new AnalysisGateway(facesContext);
      this.templates = gw.getAllTemplatesFilterable();
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * load matrix referenced by anamat
   * 
   * @param report id of the report to be used
   * @param facesContext of this function call
   * @return true if loading was successful, false otherwise
   */
  public boolean loadMatrixBean(Integer report, SquareFacesContext facesContext) {
    try {
      AnalysisGateway gw = new AnalysisGateway(facesContext);
      templates = gw.getAllTemplatesFilterable();
      matrixBean = gw.getAnalysisMatrix(report);
      variablegroups = gw.getAllVariablegroups(false, false, true);
      function = new AnalysisFunctionPageBean(matrixBean, gw.getAllFunctionBeansByMatrix(report));
      function.setAnaparfunOutput(facesContext, gw.getAllAnalysisFunctionOutputs(report));
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * copy a template
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean copyTemplate(SquareFacesContext facesContext) {
    try {
      Integer pk = new AnalysisGateway(facesContext).copyTemplate(templateBean.getParent(), templateBean.getName(),
          templateBean.getDescription());
      templateBean.resetPk(pk);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * navigate to template list
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean toTemplatelist(SquareFacesContext facesContext) {
    try {
      matrix = new AnalysisMatrixPageBean(null); // on template side, there is no report
      matrix.getTemplateList().addAll(new AnalysisGateway(facesContext).getAllTemplates());
      templateBean = new FunctionselectionBean(null);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * update the matrix bean
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean updateMatrixBean(SquareFacesContext facesContext) {
    try {
      new AnalysisGateway(facesContext).updateAnalysisMatrix(matrixBean);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * update the matrix bean template
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean updateMatrixBeanTemplate(SquareFacesContext facesContext) {
    try {
      new AnalysisGateway(facesContext).updateAnalysisMatrixTemplate(matrixBean);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * update the template definition
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean updateTemplateDefinition(SquareFacesContext facesContext) {
    try {
      new AnalysisGateway(facesContext).updateTemplateDefinition(templateBean);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      return false;
    }
  }

  /**
   * navigate to the matrix
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean toMatrix(SquareFacesContext facesContext) {
    try {
      matrix.getTemplateList().addAll(new AnalysisGateway(facesContext).getAllTemplates());
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * refresh flags of anaparfun
   * 
   * @param facesContext the context of this function call
   * 
   * @return true for a valid update, false otherwise
   */
  public boolean doRefreshFlags(SquareFacesContext facesContext) {
    try {
      Integer affected = new AnalysisGateway(facesContext).updateAnaparfunflags(anaparfunFlags);
      facesContext.notifyAffected(affected);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * get the matrix of the report
   * 
   * @param reportId the report id
   * @param facesContext the context of this function call
   * @return the matrix bean
   */
  public MatrixBean getMatrixBeanOfReport(Integer reportId, SquareFacesContext facesContext) {
    loadMatrixBean(reportId, facesContext);
    return matrixBean;
  }

  /**
   * add a template
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean addTemplate(SquareFacesContext facesContext) {
    try {
      templateBean.resetPk(new AnalysisGateway(facesContext).createTemplate(templateBean));
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      return false;
    }
  }

  /**
   * remove a template
   * 
   * @param facesContext the context of this function call
   * @param bean the bean
   * @return true
   */
  public boolean removeTemplate(SquareFacesContext facesContext, FunctionselectionBean bean) {
    try {
      new AnalysisGateway(facesContext).removeTemplate(bean.getFkFunctionlist());
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
    } finally {
      matrix.getTemplateList().clear(); // make sure that no double entries can occur
    }
    return true;
  }

  /**
   * navigate to edit a template's content
   * 
   * @param pk the id of the template
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean toEditTemplateContent(Integer pk, SquareFacesContext facesContext) {
    createTemplate(pk, facesContext);
    try {
      if (template == null) {
        throw new DataAccessException("template is null in AnalysisModel");
      }
      List<AnalysisFunctionBean> l = template.getChosen();
      if (l == null) {
        throw new DataAccessException("no template chosen");
      }
      AnalysisGateway gw = new AnalysisGateway(facesContext);
      l.addAll(gw.getAllFunctionBeansByTemplate(pk));
      template.getAll().addAll(gw.getAllFunctionBeans());
      // by default, all function beans are available due to decision of COS; no usnr
      // check anymore
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * navigate to edit template content function
   * 
   * @param afb the analysis function bean
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean toEditTemplateContentFunction(AnalysisFunctionBean afb, SquareFacesContext facesContext) {
    createPreference(afb);
    try {
      AnalysisGateway gw = new AnalysisGateway(facesContext);
      preference.getAnadefs().addAll(gw.getAllAnalysisDefaults(afb, false));
      preference.getAnascales().addAll(gw.getAllAnalysisScales(afb.getAnaparfun()));
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * remove a user's privilege
   * 
   * @param usnr the id of the user
   * @param privilegeId the id of the privilege
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean removeUserPrivilege(Integer usnr, Integer privilegeId, SquareFacesContext facesContext) {
    try {
      Set<Integer> privs = new HashSet<>();
      privs.add(privilegeId);
      new AnalysisGateway(facesContext).removeUserGroupPrivilege(usnr, null, privs);
      return true;
    } catch (Exception e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * add a privilege
   * 
   * @param privilegeId the id of the privilege
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean addUserCombi(Integer privilegeId, SquareFacesContext facesContext) {
    Integer usnr = privilege.getUser().getUsnr();
    try {
      List<Integer> privs = new ArrayList<>();
      privs.add(privilegeId);
      new AnalysisGateway(facesContext).addUserGroupPrivilege(usnr, null, privs, privilege.getUser().getAcl());
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * add a function to the template
   * 
   * @param fkFunctionlist the id of the function list
   * @param functionId the function id
   * @param title the new title of the anaparfun
   * @param orderNr the order number for the analysis matrix
   * @param functionName the name of the function (for the acronym generation)
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean addFunctionToTemplate(Integer fkFunctionlist, Integer functionId, String title, Integer orderNr, String functionName,
      SquareFacesContext facesContext) {
    try {
      new AnalysisGateway(facesContext).addFunctionToTemplate(fkFunctionlist, functionId, title, orderNr, functionName);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * remove a function from the template
   * 
   * @param anaparfun the id of an analysis function
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean removeFunctionFromTemplate(Integer anaparfun, SquareFacesContext facesContext) {
    try {
      new AnalysisGateway(facesContext).removeFunctionFromTemplate(anaparfun);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * remove a analysis matrix (that is remove a report)
   * 
   * @param anamat the id of the report
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean removeMatrix(Integer anamat, SquareFacesContext facesContext) {
    try {
      new ReportGateway(facesContext).removeReport(anamat);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * approve preferences
   * 
   * @param facesContext the context of this function call
   * @param fkReport the report id
   * @param fkFunctionList the function list
   * @return true
   */
  public boolean approvePreferences(SquareFacesContext facesContext, Integer fkReport, Integer fkFunctionList) {
    List<AnalysisDefaultBean> updateableDefaults = new ArrayList<>();
    List<MatrixColumnExcludeScaleBean> updateableScales = new ArrayList<>();
    for (AnalysisDefaultBean b : preference.getAnadefs()) {
      if (b.getAnaparfun().equals(preference.getBean().getAnaparfun())) {
        updateableDefaults.add(b);
      }
    }
    for (MatrixColumnExcludeScaleBean b : preference.getAnascales()) {
      if (b.getAnaparfun().equals(preference.getBean().getAnaparfun())) {
        updateableScales.add(b);
      }
    }
    try {
      AnalysisGateway gw = new AnalysisGateway(facesContext);
      facesContext.notifyAffected(gw.updateAnascaledef(updateableDefaults, updateableScales));
      Integer affected = gw.updateAnaparfunNameAndDescription(getPreference().getBean(), fkFunctionList);
      if (affected != 1) {
        throw new DataAccessException("number of affected database rows: " + affected);
      } else {
        facesContext.notifyInformation("info.template.functionlist.alias.newname",
            getPreference().getBean().getAlias());
      }
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * update anaparfunBean's required_execution
   * 
   * @param facesContext of this function call
   * @return true for success, false otherwise
   */
  public boolean updateAnaparfun(SquareFacesContext facesContext) {
    try {
      analysisFunctionBean.synchronizeRequiredExecutions();
      Integer affected = new AnalysisGateway(facesContext).updateAnaparfun(analysisFunctionBean);
      return facesContext.notifyAffected(affected, 1);
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * update defaults by the matrix
   * 
   * @param facesContext the context of this function call
   * @return true or false
   * 
   * @deprecated no usage found
   */
  @Deprecated
  public boolean updateDefaultsByMatrix(SquareFacesContext facesContext) {
    try {
      new AnalysisGateway(facesContext).upsertAnavarpars(variable.getAnavarpars());
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * prepare the edit matrix content variable lists
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean prepareEditMatrixContentVarlists(SquareFacesContext facesContext) {
    varlists = new AnalysisVarlistsPageBean();
    try {
      varlists.getMatrix().addAll(new AnalysisGateway(facesContext)
          .getVarlistsMatrix(matrixBean.getVariablegroup().getId(), matrixBean.getReport()));
      vargroupName = matrixBean.getVariablegroup().getName();
      CalculationGateway gw = new CalculationGateway(facesContext);
      calculationTime = gw.getCalculationTimes(matrixBean.getReport());
      hasNoResults = !gw.hasResults(matrixBean.getReport());
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * toggle a variable list entry
   * 
   * @param id the id of the current button
   * @return true
   */
  public boolean toggleVarlistEntry(String id) {
    this.currentBtnId = id;
    for (MatrixLine l : varlists.getMatrix()) {
      for (MatrixObject o : l.getCells()) {
        if (o instanceof MatrixCheckbox) {
          MatrixCheckbox c = (MatrixCheckbox) o;
          if (c.getId().equals(id)) {
            c.toggle();
          }
        }
      }
    }
    return true;
  }

  /**
   * set the value of a variable in the variable list
   * 
   * @param id the id of the current button
   * @param value the value
   * @return true
   */
  public boolean setVarlistVariablefunctions(String id, Boolean value) {
    this.currentBtnId = id;
    for (MatrixLine line : varlists.getMatrix()) {
      for (MatrixObject o : line.getCells()) {
        if (o instanceof MatrixCheckbox) {
          MatrixCheckbox c = (MatrixCheckbox) o;
          if (c.getId().substring(0, c.getId().indexOf(':')).equals(id)) {
            if (value == null) {
              c.toggle();
            } else {
              c.setValue(value);
            }
          }
        }
      }
    }
    return true;
  }

  /**
   * set the variable list function variables
   * 
   * @param id the id of the current button
   * @param value the value
   * @return true
   */
  public boolean setVarlistFunctionvariables(String id, Boolean value) {
    this.currentBtnId = id;
    for (MatrixLine line : varlists.getMatrix()) {
      for (MatrixObject o : line.getCells()) {
        if (o instanceof MatrixCheckbox) {
          MatrixCheckbox c = (MatrixCheckbox) o;
          if (c.getId().substring(c.getId().indexOf(':') + 1).equals(id)) {
            if (value == null) {
              c.toggle();
            } else {
              c.setValue(value);
            }
          }
        }
      }
    }
    return true;
  }

  /**
   * set the variable list function variables to value
   * 
   * @param value the value
   */
  public void setVarlistFunctionvariables(boolean value) {
    for (MatrixLine line : varlists.getMatrix()) {
      for (MatrixObject o : line.getCells()) {
        if (o instanceof MatrixCheckbox) {
          MatrixCheckbox c = (MatrixCheckbox) o;
          c.setValue(value);
        }
      }
    }
  }

  /**
   * delete all results of the report from t_qs_calculationresult
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean deleteResults(SquareFacesContext facesContext) {
    try {
      Integer affected = new CalculationGateway(facesContext).deleteResults(matrixBean.getReport());
      facesContext.notifyInformation("info.report.delete.results", affected);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * save all dirty variable list entries
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean saveAllDirtyVarlistEntries(SquareFacesContext facesContext) {
    List<FunctionVariablePairBean> thisList = new ArrayList<>();
    for (MatrixLine line : varlists.getMatrix()) {
      for (MatrixObject o : line.getCells()) {
        if (o instanceof MatrixCheckbox) {
          MatrixCheckbox c = (MatrixCheckbox) o;
          String[] s = c.getId().split(":");
          Integer vId = Integer.valueOf(s[0]);
          Integer fId = Integer.valueOf(s[1]);
          if (c.getValue()) {
            thisList.add(new FunctionVariablePairBean(fId, vId));
          }
        }
      }
    }
    try {
      Integer affected = new AnalysisGateway(facesContext).updateAnalysisVariables(thisList, matrixBean.getReport());
      LOGGER.debug("affected db operations: {}", affected);
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * fill the defaults
   * 
   * @param paramName the name of the parameter
   * @param value the value
   * @param fillAll if true, fill all fields, if false, fill the empty ones only
   * @param facesContext the context of this function call
   * @return true
   */
  public boolean doFillDefaults(String paramName, String value, boolean fillAll, SquareFacesContext facesContext) {
    int counter = 0;
    for (AnalysisVariableParameterBean b : variable.getAnavarpars()) {
      boolean overwrite = fillAll || (b.getValue() == null || b.getValue().isEmpty());
      if (overwrite && b.getName().equals(paramName)) {
        b.setValue(value);
        counter++;
      }
    }
    facesContext.notifyInformation("report.anamat.default.result", counter);
    return true;
  }

  /**
   * get the size of the functions list
   * 
   * @param facesContext the context of this function call
   * 
   * @return the size of the functions list; 0 for any errors
   */
  public Integer getFunctionListSize(SquareFacesContext facesContext) {
    AnalysisFunctionPageBean bean = getFunction();
    List<AnalysisFunctionBean> thisList = null;
    if (bean != null) {
      thisList = bean.getList();
    } else {
      facesContext.notifyWarning("function is null, counting number of analysis matrix functions aborted");
    }
    return thisList == null ? 0 : thisList.size();
  }

  /**
   * @return the from template
   */
  public Boolean getFromTemplate() {
    return comeFromTemplate;
  }

  /**
   * @return the analysis function page bean
   */
  public AnalysisFunctionPageBean getFunction() {
    return function;
  }

  /**
   * @return the analysis main page bean
   */
  public AnalysisMainPageBean getMain() {
    return main;
  }

  /**
   * @return the analysis matrix page bean
   */
  public AnalysisMatrixPageBean getMatrix() {
    return matrix;
  }

  /**
   * @return the analysis privilege page bean
   */
  public AnalysisPrivilegePageBean getPrivilege() {
    return privilege;
  }

  /**
   * @return the analysis template page bean
   */
  public AnalysisTemplatePageBean getTemplate() {
    return template;
  }

  /**
   * @return the templates
   */
  public FilterableListInterface getTemplates() {
    return templates;
  }

  /**
   * @return the analysis preference page bean
   */
  public AnalysisPreferencePageBean getPreference() {
    return preference;
  }

  /**
   * @return the analysis variable page bean
   */
  public AnalysisVariablePageBean getVariable() {
    return variable;
  }

  /**
   * @return the analysis welcome page bean
   */
  public AnalysisWelcomePageBean getWelcome() {
    return welcome;
  }

  /**
   * @return the matrix bean
   */
  public MatrixBean getMatrixBean() {
    return matrixBean;
  }

  /**
   * @return the variable group beans
   */
  public List<VariablegroupBean> getVariablegroups() {
    return variablegroups;
  }

  /**
   * @return the template bean
   */
  public FunctionselectionBean getTemplateBean() {
    return templateBean;
  }

  /**
   * @param templateBean the template bean
   */
  public void setTemplateBean(FunctionselectionBean templateBean) {
    this.templateBean = templateBean;
  }

  /**
   * @return the analysis varlists page bean
   */
  public AnalysisVarlistsPageBean getVarlists() {
    return varlists;
  }

  /**
   * @return the users
   */
  public List<ProfileBean> getUsers() {
    return users;
  }

  /**
   * @return the variable group name
   */
  public String getVargroupName() {
    return vargroupName;
  }

  /**
   * @return the analysis matrix beans
   */
  public List<AnalysisMatrixBean> getList() {
    return list;
  }

  /**
   * @return the current button id
   */
  public String getCurrentBtnId() {
    return currentBtnId;
  }

  /**
   * @param currentBtnId the current button id
   */
  public void setCurrentBtnId(String currentBtnId) {
    this.currentBtnId = currentBtnId;
  }

  /**
   * @return the report id
   */
  public Integer getReportId() {
    return reportId;
  }

  /**
   * @param reportId the report id
   */
  public void setReportId(Integer reportId) {
    this.reportId = reportId;
  }

  /**
   * @return the usage
   */
  public List<UsageBean> getUsage() {
    return usage;
  }

  /**
   * @param usage the usage
   */
  public void setUsage(List<UsageBean> usage) {
    this.usage = usage;
  }

  /**
   * @return the matrix filter
   */
  public String getMatrixfilter() {
    return matrixfilter;
  }

  /**
   * @param matrixfilter the matrix filter
   */
  public void setMatrixfilter(String matrixfilter) {
    this.matrixfilter = matrixfilter;
  }

  /**
   * @return the analysis function flag bean
   */
  public AnaparfunFlagBean getAnaparfunFlags() {
    return anaparfunFlags;
  }

  /**
   * @return the analysis function bean
   */
  public AnalysisFunctionBean getAnalysisFunctionBean() {
    return analysisFunctionBean;
  }

  /**
   * @param analysisFunctionBean the analysis function bean
   */
  public void setAnalysisFunctionBean(AnalysisFunctionBean analysisFunctionBean) {
    this.analysisFunctionBean = analysisFunctionBean;
  }

  /**
   * @return the anavarpars
   */
  public List<AnavarparBean> getAnavarpars() {
    return anavarpars;
  }

  /**
   * @param anavarpars the anavarpars to set
   */
  public void setAnavarpars(List<AnavarparBean> anavarpars) {
    this.anavarpars = anavarpars;
  }

  /**
   * @return the anaparfuns
   */
  public List<AnaparfunBean> getAnaparfuns() {
    return anaparfuns;
  }

  /**
   * @param anaparfuns the anaparfuns to set
   */
  public void setAnaparfuns(List<AnaparfunBean> anaparfuns) {
    this.anaparfuns = anaparfuns;
  }

  /**
   * @return the hasNoResults
   */
  public Boolean getHasNoResults() {
    return hasNoResults;
  }

  /**
   * @param hasNoResults the hasNoResults to set
   */
  public void setHasNoResults(Boolean hasNoResults) {
    this.hasNoResults = hasNoResults;
  }

  /**
   * @return the calculationTime
   */
  public String getCalculationTime() {
    return calculationTime;
  }

  /**
   * @param calculationTime the calculationTime to set
   */
  public void setCalculationTime(String calculationTime) {
    this.calculationTime = calculationTime;
  }

  /**
   * @return the jsonMatrix
   */
  public String getJsonMatrix() {
    return jsonMatrix;
  }

  /**
   * @param jsonMatrix the jsonMatrix to set
   */
  public void setJsonMatrix(String jsonMatrix) {
    this.jsonMatrix = jsonMatrix;
  }
}
