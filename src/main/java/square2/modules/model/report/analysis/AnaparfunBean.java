package square2.modules.model.report.analysis;

/**
 * 
 * @author henkej
 *
 */
public class AnaparfunBean {
	private final String pk;
	private final String name;

	/**
	 * generate new anaparfun bean
	 * 
	 * @param pk
	 *          id of the bean
	 * @param name
	 *          name of the bean
	 */
	public AnaparfunBean(String pk, String name) {
		super();
		this.pk = pk;
		this.name = name;
	}

	/**
	 * @return the pk
	 */
	public String getPk() {
		return pk;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
