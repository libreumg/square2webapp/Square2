package square2.modules.model.report.analysis;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.JSONB;
import org.jooq.exception.DataAccessException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;

import de.ship.dbppsquare.square.enums.EnumAnadef;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisDefaultBean implements Serializable, Comparable<AnalysisDefaultBean> {
	private static final Logger LOGGER = LogManager.getLogger(AnalysisDefaultBean.class);

	private static final long serialVersionUID = 1L;

	private final Integer anaparfun;
	private final Integer fkFunctioninput;
	private final String name;
	private final String description;
	private final Boolean expectUniqueName;
	private String value;
	private EnumAnadef fixed;
	private Boolean refersMetadata;
	private String typeRestriction;
	private final String typeName;

	/**
	 * create new analysis default bean
	 * 
	 * @param anaparfun the id of the analysis function
	 * @param fkFunctioninput the id of the function input
	 * @param name the name
	 * @param description the description
	 * @param fixed the fixed
	 * @param expectUniqueName true if the expected value must be in
	 * T_ELEMENT.unique_name, false otherwise
	 * @param refersMetadata if true, refers to metadata, if false, refers to
	 * variable names
	 * @param typeRestriction the type restriction, may be empty
	 * @param typeName the name of the type, found in t_functioninputtype.name
	 */
	public AnalysisDefaultBean(Integer anaparfun, Integer fkFunctioninput, String name, String description,
			EnumAnadef fixed, Boolean expectUniqueName, Boolean refersMetadata, JSONB typeRestriction, String typeName) {
		super();
		this.anaparfun = anaparfun;
		this.fkFunctioninput = fkFunctioninput;
		this.name = name;
		this.description = description;
		this.fixed = fixed == null ? EnumAnadef.report : fixed;
		this.expectUniqueName = expectUniqueName;
		this.refersMetadata = refersMetadata;
		this.typeRestriction = typeRestriction == null ? null : typeRestriction.data();
		this.typeName = typeName;
	}

	/**
	 * set the type restriction child; if this is empty, create a new object and add
	 * the key value pair
	 * 
	 * @param key the key of the child
	 * @param value the value of the child
	 */
	public void setTypeRestrictionChild(String key, JsonArray value) {
		if (typeRestriction == null || typeRestriction.isBlank()) {
			typeRestriction = "{}";
		}
		JsonObject o = new Gson().fromJson(typeRestriction, JsonObject.class);
		JsonElement e = o.get(key);
		if (e != null) {
			LOGGER.warn("{} is already in typeRestriction: value {} is overwritten by {}", key, e.toString(), value);
			o.remove(key);
		}
		o.add(key, value);
		typeRestriction = new Gson().toJson(o);
	}

	/**
	 * get the value in JSONB format
	 * 
	 * @return the value
	 */
	public JSONB getJSONBValue() {
		if (value != null) {
			if (value.isBlank()) {
				value = null;
			} else if (value == "null") {
				value = null;
			} else {
				try {
					new Gson().fromJson(value, Object.class);
				} catch (JsonSyntaxException e) {
					LOGGER.error("cannot convert {} to json, error is: {}", value, e.getMessage());
					throw new DataAccessException(e.getMessage());
				}
			}
		}
		return value == null ? null : JSONB.valueOf(value);
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{anaparfun=").append(anaparfun);
		buf.append(",fkFunctioninput=").append(fkFunctioninput);
		buf.append(",name=").append(name);
		buf.append(",description=").append(description);
		buf.append(",value=").append(value);
		buf.append(",fixed=").append(fixed);
		buf.append(",typeRestriction=").append(typeRestriction);
		buf.append(",typeName=").append(typeName);
		buf.append("}");
		return buf.toString();
	}

	@Override
	public int compareTo(AnalysisDefaultBean o) {
		return name == null || o == null || o.getName() == null ? 0 : name.compareTo(o.getName());
	}

	/**
	 * @return the forced
	 */
	public Boolean getForced() {
		return fixed.equals(EnumAnadef.report);
	}

	/**
	 * @param forced the forced
	 */
	public void setForced(Boolean forced) {
		fixed = forced ? EnumAnadef.report : EnumAnadef.matrix;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value
	 */
	public void setValue(String value) {
		this.value = value;
	}

	/**
	 * @return the id of the analysis function
	 */
	public Integer getAnaparfun() {
		return anaparfun;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the fixed
	 */
	public EnumAnadef getFixed() {
		return fixed;
	}

	/**
	 * @param fixed the fixed to set
	 */
	public void setFixed(EnumAnadef fixed) {
		this.fixed = fixed;
	}

	/**
	 * @return the expectUniqueName
	 */
	public Boolean getExpectUniqueName() {
		return expectUniqueName;
	}

	/**
	 * @return the fkFunctioninput
	 */
	public Integer getFkFunctioninput() {
		return fkFunctioninput;
	}

	/**
	 * @return the refersMetadata
	 */
	public Boolean getRefersMetadata() {
		return refersMetadata;
	}

	/**
	 * @param refersMetadata the refersMetadata to set
	 */
	public void setRefersMetadata(Boolean refersMetadata) {
		this.refersMetadata = refersMetadata;
	}

	/**
	 * @return the typeRestriction
	 */
	public String getTypeRestriction() {
		return typeRestriction;
	}

	/**
	 * @return the typeName
	 */
	public String getTypeName() {
		return typeName;
	}
}
