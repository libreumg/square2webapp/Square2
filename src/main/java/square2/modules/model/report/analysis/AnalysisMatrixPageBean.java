package square2.modules.model.report.analysis;

import java.util.ArrayList;
import java.util.List;

import square2.modules.model.functionselection.FunctionselectionBean;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisMatrixPageBean {
	private final Integer report;
	private Integer newAnatemp;
	private final List<VariablegroupBean> variablegroupList;
	private VariablegroupBean variablegroup;
	private final List<FunctionselectionBean> templateList;
	private FunctionselectionBean template;

	/**
	 * create new analysis matrix page bean
	 * 
	 * @param report
	 *          the id of the report
	 */
	public AnalysisMatrixPageBean(Integer report) {
		super();
		this.report = report;
		variablegroupList = new ArrayList<>();
		templateList = new ArrayList<>();
		template = new FunctionselectionBean(null);
	}

	/**
	 * choose a variable group
	 * 
	 * @param vargroupId
	 *          the id of the variable group
	 * @return true
	 */
	public boolean chooseVariablegroup(Integer vargroupId) {
		for (VariablegroupBean bean : variablegroupList) {
			if (bean.getId().equals(vargroupId)) {
				variablegroup = bean;
				break;
			}
		}
		return true;
	}

	/**
	 * choose a template
	 * 
	 * @param fkFunctionlist
	 *          the id of the function list
	 */
	public void chooseTemplate(Integer fkFunctionlist) {
		for (FunctionselectionBean bean : templateList) {
			if (bean.getFkFunctionlist().equals(fkFunctionlist)) {
				template = bean;
				break;
			}
		}
	}

	/**
	 * @return the newAnatemp
	 */
	public Integer getNewAnatemp() {
		return newAnatemp;
	}

	/**
	 * @param newAnatemp
	 *          the newAnatemp to set
	 */
	public void setNewAnatemp(Integer newAnatemp) {
		this.newAnatemp = newAnatemp;
	}

	/**
	 * @return the variablegroup
	 */
	public VariablegroupBean getVariablegroup() {
		return variablegroup;
	}

	/**
	 * @param variablegroup
	 *          the variablegroup to set
	 */
	public void setVariablegroup(VariablegroupBean variablegroup) {
		this.variablegroup = variablegroup;
	}

	/**
	 * @return the template
	 */
	public FunctionselectionBean getTemplate() {
		return template;
	}

	/**
	 * @param template
	 *          the template to set
	 */
	public void setTemplate(FunctionselectionBean template) {
		this.template = template;
	}

	/**
	 * @return the report
	 */
	public Integer getReport() {
		return report;
	}

	/**
	 * @return the variablegroupList
	 */
	public List<VariablegroupBean> getVariablegroupList() {
		return variablegroupList;
	}

	/**
	 * @return the templateList
	 */
	public List<FunctionselectionBean> getTemplateList() {
		return templateList;
	}
}
