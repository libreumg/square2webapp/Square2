package square2.modules.model.report.analysis.matrix;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class MatrixFunctionButton implements MatrixObject, Serializable, Comparable<MatrixFunctionButton> {
	private static final long serialVersionUID = 1L;

	private final String id;
	private final String name;
	private final String description;
	private final List<String> scales;
	private final Integer order;

	/**
	 * create new matrix function button
	 * 
	 * @param id
	 *          the id
	 * @param name
	 *          the name
	 * @param description
	 *          the description
	 * @param order
	 *          the order of that anaparfun
	 */
	public MatrixFunctionButton(Integer id, String name, String description, Integer order) {
		super();
		this.id = id == null ? null : id.toString();
		this.name = name;
		this.scales = new ArrayList<>();
		this.description = javaScriptSave(description);
		this.order = order;
	}

	/**
	 * create new matrix function button
	 * 
	 * @param id
	 *          the id
	 * @param name
	 *          the name
	 * @param description
	 *          the description
	 * @param order
	 *          the order of that anaparfun
	 */
	public MatrixFunctionButton(String id, String name, String description, Integer order) {
		super();
		this.id = id;
		this.name = name;
		this.scales = new ArrayList<>();
		this.description = javaScriptSave(description);
		this.order = order;
	}

	private final String javaScriptSave(String s) {
		return s.replaceAll("\\r\\n|\\r|\\n", " ").replaceAll("\"", "`").replaceAll("'", "`");
	}

	@Override
	public int compareTo(MatrixFunctionButton o) {
		return order == null || o == null || o.getOrder() == null ? 0 : order.compareTo(o.getOrder());
	}

	@Override
	public boolean getIsButton() {
		return true;
	}

	@Override
	public boolean getIsLabel() {
		return false;
	}

	@Override
	public boolean getIsInput() {
		return false;
	}

	@Override
	public boolean getIsIconButton() {
		return false;
	}

	@Override
	public boolean getIsDisabled() {
		return false;
	}

	@Override
	public boolean getIsTitle() {
		return false;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the scales
	 */
	public List<String> getScales() {
		return scales;
	}

	@Override
	public boolean getIsVariable() {
		return false;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @return the order
	 */
	public Integer getOrder() {
		return order;
	}
}
