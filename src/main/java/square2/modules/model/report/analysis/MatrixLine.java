package square2.modules.model.report.analysis;

import java.util.ArrayList;
import java.util.List;

import square2.modules.model.report.analysis.matrix.MatrixObject;

/**
 * 
 * @author henkej
 *
 */
public class MatrixLine {
	private final VariableBean variable;
	private List<MatrixObject> cells;

	/**
	 * create new matrix line
	 * 
	 * @param variable
	 *          the variable bean
	 */
	public MatrixLine(VariableBean variable) {
		this.variable = variable;
		this.cells = new ArrayList<>();
	}

	/**
	 * @return the variable name
	 */
	public String getVarname() {
		return variable == null ? "" : variable.getName();
	}

	/**
	 * @return the dn
	 */
	public String getDn() {
		return variable == null ? "" : variable.getDn();
	}

	/**
	 * @return the cells
	 */
	public List<MatrixObject> getCells() {
		return cells;
	}

	/**
	 * @return the variable bean
	 */
	public VariableBean getVariable() {
		return variable;
	}
}
