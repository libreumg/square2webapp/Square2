package square2.modules.model.report.analysis;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import square2.help.SquareFacesContext;
import square2.modules.model.RequiredExecutionsBean;
import square2.modules.model.admin.FunctioncategoryBean;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisFunctionBean implements Serializable, Comparable<AnalysisFunctionBean> {
  private static final long serialVersionUID = 1L;

  private final Integer id;
  private final String name;
  private String alias;
  private String acronym;
  private final String summary;
  private final String functionDescription;
  private final String vignette;
  private String description;
  private final Integer anaparfun;
  private List<FunctioncategoryBean> category;
  private final Boolean varlist;
  private Integer orderNr;
  private String requiredExecutions;
  private List<RequiredExecutionsBean> requiredExecutionsList;
  private final Integer affected;

  /**
   * only for jsf datatable filter, an empty constructor must exist
   */
  public AnalysisFunctionBean() {
  	this.id = null;
  	this.name = null;
  	this.summary = null;
  	this.functionDescription = null;
  	this.vignette = null;
  	this.anaparfun = null;
  	this.varlist = null;
  	this.affected =  null;
  }
  
  /**
   * create new analysis function bean
   * 
   * @param anaparfun id of the concrete function in the matrix
   * @param id if the function of statistic (there may be multiple anaparfun for one function)
   * @param name name of the function
   * @param alias name of the anaparfun
   * @param summary summary of the function
   * @param description description of the function
   * @param category category of the function
   * @param varlist flag to determine for what input this function works
   * @param requiredExecutions list of required anaparfun pks to run before execution
   * @param orderNr the order number for the analysis matrix
   * @param vignette the vignette
   * @param functionDescription the description of the function
   * @param affected the number of affected variables
   * @param acronym the acronym
   */
  public AnalysisFunctionBean(Integer anaparfun, Integer id, String name, String alias, String summary,
      String description, List<FunctioncategoryBean> category, Boolean varlist, String requiredExecutions,
      Integer orderNr, String vignette, String functionDescription, Integer affected, String acronym) {
    super();
    this.anaparfun = anaparfun;
    this.id = id;
    this.name = name;
    this.alias = alias;
    this.summary = summary;
    this.functionDescription = functionDescription;
    this.description = description;
    this.category = category;
    this.varlist = varlist;
    this.requiredExecutions = requiredExecutions;
    this.orderNr = orderNr;
    this.vignette = vignette;
    this.affected = affected;
    this.acronym = acronym;
  }

  /**
   * fill requiredExecutions by the values of requiredExecutionsList
   */
  public void synchronizeRequiredExecutions() {
    StringBuilder buf = new StringBuilder();
    buf.append("[");
    boolean isFirst = true;
    for (RequiredExecutionsBean bean : requiredExecutionsList) {
      if (bean.getChosen()) {
        if (isFirst) {
          isFirst = false;
        } else {
          buf.append(",");
        }
        buf.append("{\"fkAnaparfun\":\"");
        buf.append(bean.getFkAnaparfun());
        buf.append("\", \"fkFunctionoutput\":\"");
        buf.append(bean.getFkFunctionoutput());
        buf.append("\"}");
      }
    }
    buf.append("]");
    requiredExecutions = buf.toString();
  }

  /**
   * get required executions as map&lt;fkAnaparfun, map&lt;fkFunctionoutput, boolean&gt;&gt;
   * 
   * @param facesContext the context of this function call
   * 
   * @return the map
   */
  public Map<Integer, Map<Integer, Boolean>> getRequiredExecutionsAsMap(SquareFacesContext facesContext) {
    try {
      AnaparfunFunctionoutputsBean[] array = new Gson().fromJson(requiredExecutions,
          AnaparfunFunctionoutputsBean[].class);
      Map<Integer, Map<Integer, Boolean>> map = new HashMap<>();
      if (array != null) {
        for (AnaparfunFunctionoutputsBean bean : array) {
          Integer fkAnaparfun = Integer.valueOf(bean.getFkAnaparfun());
          Integer fkFunctionoutput = Integer.valueOf(bean.getFkFunctionoutput());
          Map<Integer, Boolean> innerMap = map.get(fkAnaparfun);
          if (innerMap == null) {
            innerMap = new HashMap<>();
            map.put(fkAnaparfun, innerMap);
          }
          innerMap.put(fkFunctionoutput, true);
        }
        return map;
      } else {
        return new HashMap<>();
      }
    } catch (NumberFormatException e) {
      facesContext.notifyWarning(e.getMessage() + " for {0}", requiredExecutions);
      return new HashMap<>();
    } catch (JsonSyntaxException e) {
      facesContext.notifyWarning(e.getMessage() + " for {0}", requiredExecutions);
      return new HashMap<>();
    }
  }

  @Override
  public int compareTo(AnalysisFunctionBean o) {
    return orderNr == null || o == null || o.getOrderNr() == null ? 0 : orderNr.compareTo(o.getOrderNr());
  }

  /**
   * @return the function id
   */
  public Integer getId() {
    return id;
  }

  /**
   * dummy to be compatible for composite component selectOneFunctionBean
   * 
   * @return id id of the function
   */
  public Integer getFunctionId() {
    return id;
  }

  /**
   * @return name of the function
   */
  public String getName() {
    return name;
  }

  /**
   * @return summary of the function
   */
  public String getSummary() {
    return summary;
  }

  /**
   * @return description of the function
   */
  public String getDescription() {
    return description;
  }

  /**
   * @return id of the concrete function
   */
  public Integer getAnaparfun() {
    return anaparfun;
  }

  /**
   * @return category of the function
   */
  public List<FunctioncategoryBean> getCategory() {
    return category;
  }

  /**
   * @return varlist flag of the function
   */
  public Boolean getVarlist() {
    return varlist;
  }

  /**
   * @return the requiredExecutions
   */
  public String getRequiredExecutions() {
    return requiredExecutions;
  }

  /**
   * @param requiredExecutions the requiredExecutions to set
   */
  public void setRequiredExecutions(String requiredExecutions) {
    this.requiredExecutions = requiredExecutions;
  }

  /**
   * @return the alias the name of the anaparfun
   */
  public String getAlias() {
    return alias;
  }

  /**
   * @param alias the alias to set
   */
  public void setAlias(String alias) {
    this.alias = alias;
  }

  /**
   * @return the requiredExecutionsList
   */
  public List<RequiredExecutionsBean> getRequiredExecutionsList() {
    return requiredExecutionsList;
  }

  /**
   * @param requiredExecutionsList the requiredExecutionsList to set
   */
  public void setRequiredExecutionsList(List<RequiredExecutionsBean> requiredExecutionsList) {
    this.requiredExecutionsList = requiredExecutionsList;
  }

  /**
   * @return the orderNr
   */
  public Integer getOrderNr() {
    return orderNr;
  }

  /**
   * @param orderNr the orderNr to set
   */
  public void setOrderNr(Integer orderNr) {
    this.orderNr = orderNr;
  }

  /**
   * @param description the description to set
   */
  public void setDescription(String description) {
    this.description = description;
  }

  /**
   * @return the vignette
   */
  public String getVignette() {
    return vignette;
  }

  /**
   * @return the functionDescription
   */
  public String getFunctionDescription() {
    return functionDescription;
  }

  /**
   * @return the affected
   */
  public Integer getAffected() {
    return affected;
  }

	/**
	 * @return the acronym
	 */
	public String getAcronym() {
		return acronym;
	}

	/**
	 * @param acronym the acronym to set
	 */
	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}
}
