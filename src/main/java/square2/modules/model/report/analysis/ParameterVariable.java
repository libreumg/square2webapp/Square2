package square2.modules.model.report.analysis;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author henkej
 *
 */
public class ParameterVariable {
	private VariableBean variableBean;
	private Map<Integer, Map<String, String>> variableParameters;

	/**
	 * create new parameter variable
	 * 
	 * @param name
	 *          the name
	 * @param langKey
	 *          the translation key
	 * @param valuelist
	 *          the value list
	 */
	public ParameterVariable(String name, String langKey, String valuelist) {
		variableBean = new VariableBean(name, langKey, valuelist);
		variableParameters = new HashMap<>();
	}

	/**
	 * @return the variable bean
	 */
	public VariableBean getVariableBean() {
		return variableBean;
	}

	/**
	 * @return the variable parameters
	 */
	public Map<Integer, Map<String, String>> getVariableParameters() {
		return variableParameters;
	}
}
