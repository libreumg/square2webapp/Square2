package square2.modules.model.report.analysis.matrix;

/**
 * 
 * @author henkej
 *
 */
public class MatrixDisabled implements MatrixObject {
	private final String varScale;
	private final String functionScale;

	/**
	 * create new disabled element in the matrix
	 * 
	 * @param varScale
	 *          the scale of the variable
	 * @param functionScale
	 *          the scale of the function
	 */
	public MatrixDisabled(String varScale, String functionScale) {
		this.varScale = varScale;
		this.functionScale = functionScale;
	}

	@Override
	public boolean getIsButton() {
		return false;
	}

	@Override
	public boolean getIsLabel() {
		return false;
	}

	@Override
	public boolean getIsInput() {
		return false;
	}

	@Override
	public boolean getIsIconButton() {
		return false;
	}

	@Override
	public boolean getIsDisabled() {
		return true;
	}

	@Override
	public boolean getIsTitle() {
		return false;
	}

	/**
	 * @return the scale of the variable
	 */
	public String getVarScale() {
		return varScale;
	}

	/**
	 * @return the scale of the function
	 */
	public String getFunctionScale() {
		return functionScale;
	}

	@Override
	public boolean getIsVariable() {
		return false;
	}
}
