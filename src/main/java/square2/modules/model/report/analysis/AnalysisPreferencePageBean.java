package square2.modules.model.report.analysis;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisPreferencePageBean {
	private final AnalysisFunctionBean bean;
	private final List<AnalysisDefaultBean> anadefs;
	private final List<MatrixColumnExcludeScaleBean> anascales;

	/**
	 * create new analysis preference page bean
	 * 
	 * @param bean
	 *          the analysis function bean
	 */
	public AnalysisPreferencePageBean(AnalysisFunctionBean bean) {
		super();
		this.bean = bean;
		this.anadefs = new ArrayList<>();
		this.anascales = new ArrayList<>();
	}

	/**
	 * @return the bean
	 */
	public AnalysisFunctionBean getBean() {
		return bean;
	}

	/**
	 * @return the anadefs
	 */
	public List<AnalysisDefaultBean> getAnadefs() {
		return anadefs;
	}

	/**
	 * @return the anascales
	 */
	public List<MatrixColumnExcludeScaleBean> getAnascales() {
		return anascales;
	}
}
