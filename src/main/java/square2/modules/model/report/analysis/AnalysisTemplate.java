package square2.modules.model.report.analysis;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisTemplate implements Serializable, Comparable<AnalysisTemplate> {
	private static final long serialVersionUID = 1L;

	private final Integer fkFunctionlist;
	private String name;
	private String description;
	private final List<ParameterFunction> parameterFunctions;

	/**
	 * create new analysis template bean
	 *
	 * @param fkFunctionlist
	 *          the id of the function list
	 */
	public AnalysisTemplate(Integer fkFunctionlist) {
		super();
		this.fkFunctionlist = fkFunctionlist;
		this.parameterFunctions = new ArrayList<>();
	}

	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{fkFunctionlist=").append(fkFunctionlist);
		buf.append(",name=").append(name);
		buf.append(",parameterFunctions=").append(parameterFunctions);
		buf.append("}");
		return buf.toString();
	}

	@Override
	public int compareTo(AnalysisTemplate o) {
		return name == null || o == null || o.getName() == null ? 0
				: name.toLowerCase().compareTo(o.getName().toLowerCase());
	}

	/**
	 * @return fkFunctionlist
	 */
	public Integer getId() {
		return fkFunctionlist;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the fkFunctionlist
	 */
	public Integer getFkFunctionlist() {
		return fkFunctionlist;
	}

	/**
	 * @return the parameterFunctions
	 */
	public List<ParameterFunction> getParameterFunctions() {
		return parameterFunctions;
	}
}
