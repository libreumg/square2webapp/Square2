package square2.modules.model.report.analysis.matrix;

/**
 * 
 * @author henkej
 *
 */
public interface MatrixObject {
	/**
	 * true if this implementation is a button
	 * 
	 * @return true if this object is a button
	 */
	public boolean getIsButton();

	/**
	 * true if this implementation is a label
	 * 
	 * @return true if this object is a label
	 */
	public boolean getIsLabel();

	/**
	 * true if this implementation is an input
	 * 
	 * @return true if this object is an input
	 */
	public boolean getIsInput();

	/**
	 * true if this implementation is an icon button (with no text)
	 * 
	 * @return true if this object is an icon button
	 */
	public boolean getIsIconButton();

	/**
	 * true if this implementation is a disabled object
	 * 
	 * @return true if this object is a disabled one
	 */
	public boolean getIsDisabled();

	/**
	 * true if this implementation is a title object
	 * 
	 * @return true if this object is a title
	 */
	public boolean getIsTitle();

	/**
	 * true if this implementation is a variable object
	 * 
	 * @return true if this object is a variable
	 */
	public boolean getIsVariable();
}
