package square2.modules.model.report.analysis;

/**
 * 
 * @author henkej
 *
 */
public class AnaparfunFlagBean {
	private final Integer anaparfun;
	private Boolean useIntervals;
	private final Boolean functionValue;
	private Integer orderNr;

	/**
	 * the analysis function flags
	 * 
	 * @param anaparfun
	 *          the id of the analysis function
	 * @param useIntervals
	 *          the use intervals flag
	 * @param functionValue
	 *          the function value flag
	 * @param orderNr
	 *          the order number for the analysis matrix
	 */
	public AnaparfunFlagBean(Integer anaparfun, Boolean useIntervals, Boolean functionValue, Integer orderNr) {
		super();
		this.anaparfun = anaparfun;
		this.useIntervals = useIntervals;
		this.functionValue = functionValue;
		this.orderNr = orderNr;
	}

	/**
	 * @return the useIntervals
	 */
	public Boolean getUseIntervals() {
		return useIntervals;
	}

	/**
	 * @param useIntervals
	 *          the useIntervals to set
	 */
	public void setUseIntervals(Boolean useIntervals) {
		this.useIntervals = useIntervals;
	}

	/**
	 * @return the anaparfun
	 */
	public Integer getAnaparfun() {
		return anaparfun;
	}

	/**
	 * @return the functionValue
	 */
	public Boolean getFunctionValue() {
		return functionValue;
	}

	/**
	 * @return the orderNr
	 */
	public Integer getOrderNr() {
		return orderNr;
	}

	/**
	 * @param orderNr
	 *          the orderNr to set
	 */
	public void setOrderNr(Integer orderNr) {
		this.orderNr = orderNr;
	}
}
