package square2.modules.model.report.analysis.matrix;

/**
 * 
 * @author henkej
 *
 */
public class MatrixVariableButton implements MatrixObject {
	private final String id;

	/**
	 * create new matrix variable button
	 * 
	 * @param id
	 *          the id
	 */
	public MatrixVariableButton(String id) {
		super();
		this.id = id;
	}

	@Override
	public boolean getIsLabel() {
		return false;
	}

	@Override
	public boolean getIsIconButton() {
		return true;
	}

	@Override
	public boolean getIsButton() {
		return false;
	}

	@Override
	public boolean getIsInput() {
		return false;
	}

	@Override
	public boolean getIsTitle() {
		return false;
	}

	@Override
	public boolean getIsDisabled() {
		return false;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	@Override
	public boolean getIsVariable() {
		return false;
	}
}
