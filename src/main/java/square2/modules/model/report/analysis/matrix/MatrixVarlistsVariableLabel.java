package square2.modules.model.report.analysis.matrix;

import java.io.Serializable;

import square2.modules.model.report.analysis.VariableBean;

/**
 * 
 * @author henkej
 *
 */
public class MatrixVarlistsVariableLabel
		implements MatrixObject, Serializable, Comparable<MatrixVarlistsVariableLabel> {
	private static final long serialVersionUID = 1L;

	private final Integer id;
	private final String scale;
	private final String name;
	private final Integer varorder;
	private final String translation;
	private final VariableBean variable;

	/**
	 * create new matrx variable lists variable label
	 * 
	 * @param id
	 *          the id
	 * @param scale
	 *          the scale
	 * @param name
	 *          the name
	 * @param varorder
	 *          the varorder
	 * @param translation
	 *          the translation
	 * @param variable
	 *          the variable
	 */
	public MatrixVarlistsVariableLabel(Integer id, String scale, String name, Integer varorder, String translation,
			VariableBean variable) {
		super();
		this.id = id;
		this.scale = scale;
		this.name = name;
		this.varorder = varorder;
		this.translation = translation;
		this.variable = variable;
	}

	@Override
	public int compareTo(MatrixVarlistsVariableLabel o) {
		return varorder == null || o == null || o.getVarorder() == null ? 0 : varorder.compareTo(o.getVarorder());
	}

	@Override
	public boolean getIsButton() {
		return false;
	}

	@Override
	public boolean getIsLabel() {
		return false;
	}

	@Override
	public boolean getIsInput() {
		return false;
	}

	@Override
	public boolean getIsIconButton() {
		return false;
	}

	@Override
	public boolean getIsDisabled() {
		return false;
	}

	@Override
	public boolean getIsTitle() {
		return false;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the scale
	 */
	public String getScale() {
		return scale;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the variable order
	 */
	public Integer getVarorder() {
		return varorder;
	}

	@Override
	public boolean getIsVariable() {
		return true;
	}

	/**
	 * @return the translation
	 */
	public String getTranslation() {
		return translation;
	}

	/**
	 * @return the variable
	 */
	public VariableBean getVariable() {
		return variable;
	}
}
