package square2.modules.model.report.analysis;

/**
 * 
 * @author henkej
 *
 */
public class AnaparfunFunctionoutputsBean {
	private String fkAnaparfun;
	private String fkFunctionoutput;

	/**
	 * @return the fkAnaparfun
	 */
	public String getFkAnaparfun() {
		return fkAnaparfun;
	}

	/**
	 * @param fkAnaparfun
	 *          the fkAnaparfun to set
	 */
	public void setFkAnaparfun(String fkAnaparfun) {
		this.fkAnaparfun = fkAnaparfun;
	}

	/**
	 * @return the fkFunctionoutput
	 */
	public String getFkFunctionoutput() {
		return fkFunctionoutput;
	}

	/**
	 * @param fkFunctionoutput
	 *          the fkFunctionoutput to set
	 */
	public void setFkFunctionoutput(String fkFunctionoutput) {
		this.fkFunctionoutput = fkFunctionoutput;
	}
}
