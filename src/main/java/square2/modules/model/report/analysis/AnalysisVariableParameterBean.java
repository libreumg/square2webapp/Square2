package square2.modules.model.report.analysis;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisVariableParameterBean implements Serializable, Comparable<AnalysisVariableParameterBean> {
  private static final long serialVersionUID = 1L;

  private final Integer anaparfun;
  private final String name;
  private final Integer anavar;
  private final Integer functioninput;
  private final Boolean expectUniqueName;
  private Boolean refersMetadata;
  private String value;

  /**
   * create new analysis variable parameter bean
   * 
   * @param anaparfun the analysis function id
   * @param name the name
   * @param anavar the analysis variable id
   * @param value the value
   * @param functioninput the function input id
   * @param expectUniqueName true if the expected value must be a valid unique name from t_hierarchy, false otherwise
   * @param refersMetadata true or false to define if a value field is to be interpreted or not
   */
  public AnalysisVariableParameterBean(Integer anaparfun, String name, Integer anavar, String value,
      Integer functioninput, Boolean expectUniqueName, Boolean refersMetadata) {
    super();
    this.anaparfun = anaparfun;
    this.name = name;
    this.anavar = anavar;
    this.value = value;
    this.functioninput = functioninput;
    this.expectUniqueName = expectUniqueName;
    this.refersMetadata = refersMetadata;
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
    buf.append("@{anaparfun=").append(anaparfun);
    buf.append(",name=").append(name);
    buf.append(",anavar=").append(anavar);
    buf.append(",value=").append(value);
    buf.append(",functioninput=").append(functioninput);
    buf.append(",refersMetadata=").append(refersMetadata);
    buf.append("}");
    return buf.toString();
  }

  @Override
  public int compareTo(AnalysisVariableParameterBean o) {
    return name == null || o == null || o.getName() == null ? 0 : name.compareTo(o.getName());
  }

  /**
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * @param value the value to set
   */
  public void setValue(String value) {
    this.value = value;
  }

  /**
   * @return the anaparfun
   */
  public Integer getAnaparfun() {
    return anaparfun;
  }

  /**
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * @return the anavar
   */
  public Integer getAnavar() {
    return anavar;
  }

  /**
   * @return the functioninput
   */
  public Integer getFunctioninput() {
    return functioninput;
  }

  /**
   * @return the expectUniqueName
   */
  public Boolean getExpectUniqueName() {
    return expectUniqueName;
  }

  /**
   * @return the refersMetadata
   */
  public Boolean getRefersMetadata() {
    return refersMetadata;
  }

  /**
   * @param refersMetadata the refersMetadata to set
   */
  public void setRefersMetadata(Boolean refersMetadata) {
    this.refersMetadata = refersMetadata;
  }
}
