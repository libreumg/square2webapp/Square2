package square2.modules.model.report.analysis;

import java.util.ArrayList;
import java.util.List;

import square2.modules.model.UserApplRightBean;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisPrivilegePageBean {
	private final String name;
	private final List<UserApplRightBean> users;
	private final UserApplRightBean user;
	private final Integer currentPrivilegeId;

	/**
	 * create new analysis privilege page bean
	 * 
	 * @param name
	 *          the name
	 * @param currentPrivilegeId
	 *          the current privilege id
	 */
	public AnalysisPrivilegePageBean(String name, Integer currentPrivilegeId) {
		super();
		this.name = name;
		users = new ArrayList<>();
		user = new UserApplRightBean();
		this.currentPrivilegeId = currentPrivilegeId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the users
	 */
	public List<UserApplRightBean> getUsers() {
		return users;
	}

	/**
	 * @return the user
	 */
	public UserApplRightBean getUser() {
		return user;
	}

	/**
	 * @return the currentPrivilegeId
	 */
	public Integer getCurrentPrivilegeId() {
		return currentPrivilegeId;
	}
}
