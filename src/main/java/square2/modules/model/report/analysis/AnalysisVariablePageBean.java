package square2.modules.model.report.analysis;

import java.util.ArrayList;
import java.util.List;

import square2.help.SquareFacesContext;
import square2.modules.model.report.analysis.matrix.MatrixEmptyCell;
import square2.modules.model.report.analysis.matrix.MatrixLabel;
import square2.modules.model.report.analysis.matrix.MatrixObject;
import square2.modules.model.report.analysis.matrix.MatrixVariableParameterInput;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisVariablePageBean {
	private final Integer report;
	private final Integer anaparfun;
	private List<List<MatrixObject>> matrix;
	private List<AnalysisVariable> variables;

	/**
	 * create new analysis variable page bean
	 * 
	 * @param report
	 *          the id of the report
	 * @param anaparfun
	 *          the id of the analysis function
	 */
	public AnalysisVariablePageBean(Integer report, Integer anaparfun) {
		super();
		this.report = report;
		this.anaparfun = anaparfun;
		this.matrix = new ArrayList<>();
	}

	/**
	 * fill matrix by its defaults and header lines
	 * 
	 * @param list
	 *          the list of analysis defaults
	 */
	public void fillMatrixDefaults(List<AnalysisDefaultBean> list) {
		matrix.clear();
		List<MatrixObject> defaultsLine = new ArrayList<>();
		List<MatrixObject> headLine = new ArrayList<>();
		defaultsLine.add(new MatrixEmptyCell());
		headLine.add(new MatrixEmptyCell());
		for (AnalysisDefaultBean bean : list) {
			defaultsLine.add(new MatrixAnalysisDefaultButton(bean));
			headLine.add(new MatrixLabel(null, bean.getName(), bean.getDescription()));
		}
		matrix.add(headLine);
		matrix.add(defaultsLine);
	}

	/**
	 * fill matrix by variable and its params
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param list
	 *          the list of analysis variables
	 */
	public void fillMatrixVariables(SquareFacesContext facesContext, List<AnalysisVariable> list) {
		this.variables = list;
		for (AnalysisVariable bean : list) {
			List<MatrixObject> line = new ArrayList<>();
			line.add(new MatrixLabel(null, bean.getVariableName(), bean.getTranslation()));
			for (AnalysisVariableParameterBean p : bean.getAnavarpars()) {
				line.add(new MatrixVariableParameterInput(p));
			}
			matrix.add(line);
		}
	}

	/**
	 * get list of anavarpars
	 * 
	 * @return the list of analysis variable parameters
	 */
	public List<AnalysisVariableParameterBean> getAnavarpars() {
		List<AnalysisVariableParameterBean> list = new ArrayList<>();
		for (AnalysisVariable bean : variables) {
			list.addAll(bean.getAnavarpars());
		}
		return list;
	}

	/**
	 * @return the report
	 */
	public Integer getReport() {
		return report;
	}

	/**
	 * @return the anaparfun
	 */
	public Integer getAnaparfun() {
		return anaparfun;
	}

	/**
	 * @return the matrix
	 */
	public List<List<MatrixObject>> getMatrix() {
		return matrix;
	}

	/**
	 * @return the variables
	 */
	public List<AnalysisVariable> getVariables() {
		return variables;
	}
}
