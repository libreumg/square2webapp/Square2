package square2.modules.model.report.analysis;

import de.ship.dbppsquare.square.tables.records.TScaleRecord;

/**
 * 
 * @author henkej
 *
 */
public class MatrixColumnExcludeScaleBean {
	private final Integer anaparfun;
	private final Boolean disabled;
	private TScaleRecord excludeScale;
	private Boolean value;

	/**
	 * create new analysis scale bean
	 * 
	 * @param anaparfun
	 *          the id of the function
	 * @param disabled
	 *          true if the R function forbids using this scale, false otherwise
	 */
	public MatrixColumnExcludeScaleBean(Integer anaparfun, Boolean disabled) {
		super();
		this.anaparfun = anaparfun;
		this.disabled = disabled;
	}

	/**
	 * @return the value
	 */
	public Boolean getValue() {
		return value;
	}

	/**
	 * @param value
	 *          the value
	 */
	public void setValue(Boolean value) {
		this.value = value;
	}

	/**
	 * @return the id of the function
	 */
	public Integer getAnaparfun() {
		return anaparfun;
	}

	/**
	 * @return the excludeScale
	 */
	public TScaleRecord getExcludeScale() {
		return excludeScale;
	}

	/**
	 * @param excludeScale
	 *          the excludeScale to set
	 */
	public void setExcludeScale(TScaleRecord excludeScale) {
		this.excludeScale = excludeScale;
	}

	/**
	 * @return the disabled
	 */
	public Boolean getDisabled() {
		return disabled;
	}
}
