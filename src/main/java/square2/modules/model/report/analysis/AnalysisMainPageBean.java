package square2.modules.model.report.analysis;

import ship.jsf.components.selectFilterableMenu.FilterableListInterface;

/**
 * 
 * @author henkej
 *
 */
public class AnalysisMainPageBean {
	private String name;
	private String description;
	private Integer variablegroup;
	private FilterableListInterface list;

	/**
	 * create new analysis matrix page bean
	 * 
	 * @param analysisMatrixBeans
	 *          the analysis matrix beans
	 */
	public AnalysisMainPageBean(FilterableListInterface analysisMatrixBeans) {
		list = analysisMatrixBeans;
	}

	/**
	 * create new analysis matrix page bean
	 */
	public AnalysisMainPageBean() {
	}

	/**
	 * @return the list
	 */
	public FilterableListInterface getList() {
		return list;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the variablegroup
	 */
	public Integer getVariablegroup() {
		return variablegroup;
	}

	/**
	 * @param variablegroup
	 *          the variablegroup to set
	 */
	public void setVariablegroup(Integer variablegroup) {
		this.variablegroup = variablegroup;
	}
}
