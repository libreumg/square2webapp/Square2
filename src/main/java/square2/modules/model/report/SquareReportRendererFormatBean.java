package square2.modules.model.report;

import java.io.Serializable;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import square2.converter.JsonConverter;

/**
 * 
 * @author henkej
 *
 */
public class SquareReportRendererFormatBean implements Serializable, JsonConverter<SquareReportRendererFormatBean> {
	private static final long serialVersionUID = 1L;
	private final String label;
	private final String format;
	private final String mimeType;
	private final String fileExtension;

	/**
	 * @param label the label
	 * @param format the format
	 * @param mimeType the mime type
	 * @param fileExtension the file extension
	 */
	public SquareReportRendererFormatBean(String label, String format, String mimeType, String fileExtension) {
		super();
		this.label = label;
		this.format = format;
		this.mimeType = mimeType;
		this.fileExtension = fileExtension;
	}

	@Override
	public SquareReportRendererFormatBean fromJson(String json) throws JsonSyntaxException {
		return new Gson().fromJson(json, SquareReportRendererFormatBean.class);
	}

	@Override
	public String toJson() {
		return new Gson().toJson(this);
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append("SquareReportRendererFormatBean{label=").append(label);
		buf.append(", format=").append(format);
		buf.append(", mimeType=").append(mimeType);
		buf.append(", fileExtension=").append(fileExtension);
		buf.append("]");
		return buf.toString();
	}

	/**
	 * @return the label
	 */
	public String getLabel() {
		return label;
	}

	/**
	 * @return the format
	 */
	public String getFormat() {
		return format;
	}

	/**
	 * @return the mimeType
	 */
	public String getMimeType() {
		return mimeType;
	}

	/**
	 * @return the fileExtension
	 */
	public String getFileExtension() {
		return fileExtension;
	}
}
