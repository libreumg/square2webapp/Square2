package square2.modules.model.report;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class JsonMatrixContainerBean implements Serializable {
  private static final long serialVersionUID = 1L;

  private final String jsonMatrix;

  /**
   * @param jsonMatrix the jsonMatrix string
   */
  public JsonMatrixContainerBean(String jsonMatrix) {
    super();
    this.jsonMatrix = jsonMatrix;
  }

  /**
   * @return the jsonMatrix
   */
  public String getJsonMatrix() {
    return jsonMatrix;
  }
}
