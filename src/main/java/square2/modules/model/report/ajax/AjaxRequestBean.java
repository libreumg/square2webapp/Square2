package square2.modules.model.report.ajax;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class AjaxRequestBean extends JsonBean implements Serializable, Comparable<AjaxRequestBean> {
  private static final long serialVersionUID = 1L;
  private final String cardinality;
  private final String targetColumns;
  private final String targetTable;
  private final String crud;
  private final Double index;

  /**
   * @param jsonBean the json bean
   * @param cardinality the cardinality, can be one of single or multiple
   * @param targetColumns the target columns, can be one of selected or params
   * @param targetTable the target table, can be one of element or column
   * @param crud the sql operation, one of insert, update or delete
   * @param index the index, used for ordering only
   */
  public AjaxRequestBean(JsonBean jsonBean, String cardinality, String targetColumns, String targetTable, String crud, Double index) {
    super(jsonBean.getVersion(), jsonBean.getMatrixElement());
    super.getUniqueNames().addAll(jsonBean.getUniqueNames());
    super.getMatrixcolumnNames().addAll(jsonBean.getMatrixcolumnNames());
    this.cardinality = cardinality;
    this.targetColumns = targetColumns;
    this.targetTable = targetTable;
    this.crud = crud;
    this.index = index;
  }

  @Override
  public int compareTo(AjaxRequestBean bean) {
    return bean == null || index == null ? 0 : index.compareTo(bean.getIndex());
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
    buf.append("@{cardinality=").append(cardinality);
    buf.append(", targetColumn=").append(targetColumns);
    buf.append(", targetTable=").append(targetTable);
    buf.append(", crud=").append(crud);
    buf.append(", index=").append(index);
    buf.append("}");
    return buf.toString();
  }

  /**
   * @return the cardinality
   */
  public String getCardinality() {
    return cardinality;
  }

  /**
   * @return the targetColumns
   */
  public String getTargetColumns() {
    return targetColumns;
  }

  /**
   * @return the targetTable
   */
  public String getTargetTable() {
    return targetTable;
  }

  /**
   * @return the crud
   */
  public String getCrud() {
    return crud;
  }

  /**
   * @return the index
   */
  public Double getIndex() {
    return index;
  }
}
