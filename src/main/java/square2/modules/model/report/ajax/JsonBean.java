package square2.modules.model.report.ajax;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class JsonBean implements Serializable {
  private static final long serialVersionUID = 1L;
  private final String version;
  private final List<String> uniqueNames;
  private final List<String> matrixcolumnNames;
  private final MatrixElementBean matrixElement;

  /**
   * @param version the version
   * @param matrixElement the matrixElement
   */
  public JsonBean(String version, MatrixElementBean matrixElement) {
    super();
    this.version = version;
    this.uniqueNames = new ArrayList<>();
    this.matrixcolumnNames = new ArrayList<>();
    this.matrixElement = matrixElement;
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
    buf.append("@{version=").append(version);
    buf.append(",uniqueNames=").append(uniqueNames);
    buf.append(",matrixcolumnNames=").append(matrixcolumnNames);
    buf.append(",matrixElement=").append(matrixElement);
    buf.append("}");
    return buf.toString();
  }

  /**
   * @return the version
   */
  public String getVersion() {
    return version;
  }

  /**
   * @return the uniqueNames
   */
  public List<String> getUniqueNames() {
    return uniqueNames;
  }

  /**
   * @return the matrixcolumnNames
   */
  public List<String> getMatrixcolumnNames() {
    return matrixcolumnNames;
  }

  /**
   * @return the matrixElement
   */
  public MatrixElementBean getMatrixElement() {
    return matrixElement;
  }
}
