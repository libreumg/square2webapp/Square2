package square2.modules.model.report.ajax;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author henkej
 *
 */
public class MatrixElementBean implements Serializable {
  private static final long serialVersionUID = 1L;
  private final Boolean selected;
  private final Map<String, ParamBean> params;

  /**
   * @param selected the selected
   */
  public MatrixElementBean(Boolean selected) {
    super();
    this.selected = selected;
    params = new HashMap<>();
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
    buf.append("@{selected=").append(selected);
    buf.append(",params=").append(params);
    buf.append("}");
    return buf.toString();
  }

  /**
   * @return the selected
   */
  public Boolean getSelected() {
    return selected;
  }

  /**
   * @return the params
   */
  public Map<String, ParamBean> getParams() {
    return params;
  }
}
