package square2.modules.model.report.ajax;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class ParamBean implements Serializable {
  private static final long serialVersionUID = 1L;

  private final String value;
  private final Boolean refersMetadata;
  
  /**
   * @param value the value
   * @param refersMetadata the refers metadata
   */
  public ParamBean(String value, Boolean refersMetadata) {
    super();
    this.value = value;
    this.refersMetadata = refersMetadata;
  }

  @Override
  public String toString() {
    StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
    buf.append("@{value=").append(value);
    buf.append(",refersMetadata=").append(refersMetadata);
    buf.append("}");
    return buf.toString();
  }

  /**
   * @return the value
   */
  public String getValue() {
    return value;
  }

  /**
   * @return the refersMetadata
   */
  public Boolean getRefersMetadata() {
    return refersMetadata;
  }
}
