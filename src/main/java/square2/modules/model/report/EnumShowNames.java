package square2.modules.model.report;

/**
 * 
 * @author henkej
 *
 */
public enum EnumShowNames {
	/**
	 * the function list name
	 */
	FUNCTIONLISTNAME,
	/**
	 * the variablegroup name
	 */
	VARIABLEGROUPNAME;
}
