package square2.modules.model.cohort;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.FacesValidator;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

import square2.db.control.StudyGateway;
import square2.help.SquareFacesContext;

/**
 * 
 * @author henkej
 *
 */
@FacesValidator("square.modules.model.cohort.StudyNameValidator")
public class StudyNameValidator implements Validator<String> {

  @Override
  public void validate(FacesContext context, UIComponent component, String value) throws ValidatorException {
    String allowed = (String) component.getAttributes().get("allowed"); // edit fields must allow the old value
    SquareFacesContext facesContext = (SquareFacesContext) context;
    List<CohortStudyBean> studyBeans = new StudyGateway(facesContext).getAllStudies();
    Set<String> studyNames = new HashSet<>();
    for (CohortStudyBean bean : studyBeans) {
      studyNames.add(bean.getName());
    }
    if (allowed != null && !allowed.isBlank()) {
      studyNames.remove(allowed);
    }
    if (studyNames.contains(value)) {
      FacesMessage msg = new FacesMessage();
      msg.setSeverity(FacesMessage.SEVERITY_ERROR);
      msg.setSummary(facesContext.translate("error"));
      msg.setDetail(facesContext.translate("error.cohort.add.study.namenotunique", value));
      throw new ValidatorException(msg);
    }
  }
}
