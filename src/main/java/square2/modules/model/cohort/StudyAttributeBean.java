package square2.modules.model.cohort;

import java.io.Serializable;

import org.jooq.JSONB;

/**
 * 
 * @author henkej
 *
 */
public class StudyAttributeBean implements Serializable, Comparable<StudyAttributeBean> {
	private static final long serialVersionUID = 1L;

	private final String key;
	private final String inputtype;
	private final JSONB typerestriction;
	private JSONB value;
	
	/**
	 * create a new study attribute bean
	 * 
	 * @param key the key of the attribute
	 * @param inputtype the name of the input type
	 * @param typerestriction the type restriction
	 */
	public StudyAttributeBean(String key, String inputtype, JSONB typerestriction) {
		this.key = key;
		this.inputtype = inputtype;
		this.typerestriction = typerestriction;
	}
	
	@Override
	public int compareTo(StudyAttributeBean bean) {
		return bean == null || bean.getKey() == null ? 0 : bean.getKey().compareTo(key);
	}
	
	@Override
	public boolean equals(Object o) {
		return (o instanceof StudyAttributeBean) && ((StudyAttributeBean) o).getKey().equals(key);
	}

	/**
	 * @return the json representation of the value
	 */
	public String getJsonValue() {
		return value == null ? null : value.data();
	}
	
	/**
	 * @param value the value to be set
	 */
	public void setJsonValue(String value) {
		this.value = value == null ? null : JSONB.jsonb(value);
	}
	
	/**
	 * @return the json representation of the type restriction
	 */
	public String getJsonTyperestriction() {
		return typerestriction == null ? null : typerestriction.data();
	}
	
	/**
	 * @return the value
	 */
	public JSONB getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(JSONB value) {
		this.value = value;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @return the inputtype
	 */
	public String getInputtype() {
		return inputtype;
	}

	/**
	 * @return the typerestriction
	 */
	public JSONB getTyperestriction() {
		return typerestriction;
	}
}
