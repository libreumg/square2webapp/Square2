package square2.modules.model.cohort;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.SessionScoped;
import javax.faces.event.ValueChangeEvent;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.JSONB;
import org.jooq.exception.DataAccessException;

import square2.db.control.StudyGateway;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.GroupPrivilegeBean;
import square2.modules.model.ModelException;
import square2.modules.model.SquareModel;
import square2.modules.model.UserApplRightBean;
import square2.modules.model.admin.UsergroupBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class CohortModel extends SquareModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(CohortModel.class);

	private List<CohortStudyBean> studies;
	private CohortStudyBean study;
	private StudygroupBean studygroup;
	private UserApplRightBean newUser;
	private List<StudygroupBean> studyGroups;
	private List<GroupPrivilegeBean> groupPrivileges;
	private List<UsergroupBean> allGroups;
	private Integer newGroup;
	private Integer newUsnr;
	private AclBean newAcl;
	private GroupPrivilegeBean activePrivilege;
	private String newAttributeKey;

	/**
	 * load all database content for welcome page
	 * 
	 * @param facesContext of this current function call
	 * @return true for successful loading, false otherwise
	 */
	public boolean toWelcome(SquareFacesContext facesContext) {
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			studies = gw.getAllStudies();
			studyGroups = gw.getAllStudygroups();
			return true;
		} catch (DataAccessException e) {
			studies = new ArrayList<>();
			studyGroups = new ArrayList<>();
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * load all study groups
	 * 
	 * @param facesContext the faces context
	 * @return true or false
	 */
	public boolean toListStudygroup(SquareFacesContext facesContext) {
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			studyGroups = gw.getAllStudygroups();
			return true;
		} catch (DataAccessException e) {
			studyGroups = new ArrayList<>();
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * prepare editing a study group
	 * 
	 * @param facesContext the faces context
	 * @param bean the bean
	 * @return true or false
	 */
	public boolean toEditStudygroup(SquareFacesContext facesContext, StudygroupBean bean) {
		studygroup = bean;
		return true;
	}

	/**
	 * load all for the info panel
	 * 
	 * @param facesContext the faces context
	 * @param cohortStudyBean the bean
	 * @return true or false
	 */
	public boolean doShow(SquareFacesContext facesContext, CohortStudyBean cohortStudyBean) {
		try {
			study = new StudyGateway(facesContext).getStudy(cohortStudyBean.getId());
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * load all database content for item page
	 * 
	 * @param facesContext the context of this fucntion call
	 * @param cohortStudyBean study bean of cohort
	 * @return true if successful, false otherwise
	 */
	public boolean toItem(SquareFacesContext facesContext, CohortStudyBean cohortStudyBean) {
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			study = gw.getStudy(cohortStudyBean.getId());
			newUser = new UserApplRightBean();
			setAllSquareUsers(facesContext, study.getRight());
			newGroup = Integer.valueOf(0);
			newAcl = new AclBean().setString("rwx");
			groupPrivileges = gw.getAllGroupPrivileges(cohortStudyBean.getRight());
			allGroups = gw.getAllGroupsExceptOf(groupPrivileges);
			activePrivilege = new GroupPrivilegeBean(null, null, cohortStudyBean.getRight());
			return true;
		} catch (DataAccessException | ModelException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * navigate to add study form
	 * 
	 * @param facesContext the contect of this function call
	 * @return true
	 */
	public boolean toAddStudy(SquareFacesContext facesContext) {
		String defaultEnglish = "{\"en\": \"English\"}";
		study = new CohortStudyBean(new ArrayList<>(), JSONB.valueOf(defaultEnglish)); // TODO: load all available
																																										// attributes as soon as we want to
																																										// add them on creation
		return true;
	}

	/**
	 * navigate to add study group form
	 * 
	 * @param facesContext the context of this function call
	 * @return true
	 */
	public boolean toAddStudygroup(SquareFacesContext facesContext) {
	  // as specified in https://gitlab.com/umg_hgw/sq2/square2/-/issues/899
		JSONB defaultLocale = JSONB.valueOf("{\"en\": \"English\"}"); 
		studygroup = new StudygroupBean(null, "", JSONB.valueOf("{}"), "", defaultLocale);
		return true;
	}

	/**
	 * @param facesContext the context of this fucntion call
	 * @return a map of rights
	 * @throws Exception if anything goes wrong
	 */
	public Map<Integer, Map<Integer, AclBean>> getRightMap(SquareFacesContext facesContext) throws Exception {
		return new StudyGateway(facesContext).getRightMap();
	}

	@Override
	public void setAllSquareUsers(SquareFacesContext facesContext, Object... params)
			throws ModelException, DataAccessException {
		if (params == null || params.length < 1 || !(params[0] instanceof Integer)) {
			throw new ModelException("at least one param is needed that is an integer of the right_id");
		}
		Integer privilegeId = (Integer) params[0];
		super.resetAllSquareusers(new StudyGateway(facesContext).getAllSquareUsersWithoutUserPrivilege(privilegeId));
	}

	/**
	 * delete the active privilege
	 * 
	 * @param facesContext the context of this fucntion call
	 * @return true or false
	 */
	public boolean deletePrivilege(SquareFacesContext facesContext) {
		try {
			Integer affected = new StudyGateway(facesContext).removeUserGroupPrivilege(activePrivilege);
			facesContext.notifyInformation("info.affected.remove", affected);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * edit the active privilege
	 * 
	 * @param facesContext the context of this fucntion call
	 * @return true or false
	 */
	public boolean editPrivilege(SquareFacesContext facesContext) {
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			Set<Integer> childrenPrivs = gw.getAllChildrenPrivilegeIds(activePrivilege.getFkPrivilege());
			childrenPrivs.add(gw.getParentPrivilegeId(activePrivilege.getFkPrivilege()));
			List<GroupPrivilegeBean> list = new ArrayList<>();
			list.add(activePrivilege); // TODO: still in childrenPrivs, could be removed (is just a duplicate now)
			for (Integer childrenPriv : childrenPrivs) {
				GroupPrivilegeBean bean = new GroupPrivilegeBean(null, activePrivilege.getIsGroup(), childrenPriv);
				bean.setFkGroup(activePrivilege.getFkGroup());
				bean.setFkUsnr(activePrivilege.getFkUsnr());
				bean.getAcl().setAcl(activePrivilege.getAcl());
				list.add(bean);
			}
			Integer affected = gw.upsertUserGroupPrivileges(list);
			facesContext.notifyInformation("info.affected.add", affected);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * set user privilege due to definition; read and write is atomic, and read and
	 * execute also
	 * 
	 * @param facesContext of this current function call
	 * @return true or false
	 */
	public boolean addPrivilege(SquareFacesContext facesContext) {
		// atomic read and write as read and execute
		newAcl.setRead(newAcl.getWrite() || newAcl.getExecute());
		try {
			List<Integer> privileges = new ArrayList<>();
			privileges.add(study.getRight());
			StudyGateway gw = new StudyGateway(facesContext);
			privileges.addAll(gw.getAllChildrenPrivilegeIds(study.getRight()));
			privileges.add(gw.getParentPrivilegeId(activePrivilege.getFkPrivilege()));
			Integer affected = gw.addUserGroupPrivilegeForStudy(newUsnr, newGroup, privileges, newAcl);
			facesContext.notifyInformation("info.affected.add", affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * completely remove a study/data collection from the database
	 * 
	 * @param facesContext the context of the function call
	 * @param bean the study bean to be removed; id and right must be set
	 * @return true in any case
	 */
	public boolean removeDataCollection(SquareFacesContext facesContext, CohortStudyBean bean) {
		try {
			new StudyGateway(facesContext).deleteDataCollection(bean);
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
		reloadProfilePrivileges(facesContext);
		return true;
	}

	/**
	 * completely remove a studygroup with all of its studies from the database
	 * 
	 * @param facesContext the context of this function call
	 * @param bean the study group bean to removed; id must be set
	 * @return true or false
	 */
	public boolean removeStudygroup(SquareFacesContext facesContext, StudygroupBean bean) {
		try {
			new StudyGateway(facesContext).deleteStudygroup(bean);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * update the current study
	 * 
	 * @param facesContext the context of this fucntion call
	 * @return true or false
	 */
	public boolean updateStudy(SquareFacesContext facesContext) {
		try {
			new StudyGateway(facesContext).updateStudy(study, facesContext.getIsocode());
			facesContext.notifyInformation("message.updated", study.getName());
			facesContext.reloadTranslations(); // possible new translations available
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * prepare the study add panel
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean doAddStudy(SquareFacesContext facesContext) {
		try {
			StudyGateway gw = new StudyGateway(facesContext);
			AclBean acl = new AclBean();
			acl.setRead(true);
			acl.setWrite(true);
			acl.setExecute(true);
			Integer id = gw.insertStudy(study, acl);
			reloadProfilePrivileges(facesContext);
			JSONB translation = study.getTranslation(); // rescue translation of current language
			study = gw.getStudy(id);
			study.setTranslation(translation); // as the translation is loaded from the database and outdated, replace
			// it
			newUser = new UserApplRightBean();
			setAllSquareUsers(facesContext, study.getRight());
			newGroup = Integer.valueOf(0);
			newAcl = new AclBean().setString("rwx");
			groupPrivileges = gw.getAllGroupPrivileges(study.getRight());
			allGroups = gw.getAllGroupsExceptOf(groupPrivileges);
			return true;
		} catch (DataAccessException | ModelException e) {
			LOGGER.error(e.getMessage(), e);
			facesContext.notifyException(e);
			return false;
		}
	}

	/**
	 * validate the studygroup name
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	private boolean validateStudygroupName(SquareFacesContext facesContext) {
		for (StudygroupBean sg : studyGroups) {
			if (sg.getName().equalsIgnoreCase(studygroup.getName()) && !sg.getPk().equals(studygroup.getPk())) {
				facesContext.notifyError("error.studygroup.name.still.used", studygroup.getName());
				return false;
			}
		}
		return true;
	}

	/**
	 * update the study group name
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean doUpdateStudygroup(SquareFacesContext facesContext) {
		if (validateStudygroupName(facesContext)) {
			try {
				StudyGateway gw = new StudyGateway(facesContext);
				gw.updateStudygroup(studygroup);
				return true;
			} catch (DataAccessException e) {
				facesContext.notifyException(e);
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * add the study group
	 * 
	 * @param facesContext the context of this function call
	 * @return true or false
	 */
	public boolean doAddStudygroup(SquareFacesContext facesContext) {
		if (validateStudygroupName(facesContext)) {
			try {
				StudyGateway gw = new StudyGateway(facesContext);
				gw.addStudygroup(studygroup);
				return true;
			} catch (DataAccessException e) {
				facesContext.notifyException(e);
				return false;
			}
		} else {
			return false;
		}
	}

	/**
	 * @return the study groups
	 */
	public List<StudygroupBean> getStudygroups() {
		return studyGroups;
	}

	/**
	 * @return the studies
	 */
	public List<CohortStudyBean> getStudies() {
		return studies;
	}

	/**
	 * @return the study
	 */
	public CohortStudyBean getStudy() {
		return study;
	}

	/**
	 * @param study the study
	 */
	public void setStudy(CohortStudyBean study) {
		this.study = study;
	}

	/**
	 * @return the new user
	 */
	public UserApplRightBean getNewUser() {
		return newUser;
	}

	/**
	 * @param newUser the new user
	 */
	public void setNewUser(UserApplRightBean newUser) {
		this.newUser = newUser;
	}

	/**
	 * @return the group privileges
	 */
	public List<GroupPrivilegeBean> getGroupPrivileges() {
		return groupPrivileges;
	}

	/**
	 * @return the new group
	 */
	public Integer getNewGroup() {
		return newGroup;
	}

	/**
	 * @param newGroup the new group
	 */
	public void setNewGroup(Integer newGroup) {
		this.newGroup = newGroup;
	}

	/**
	 * @return the all groups
	 */
	public List<UsergroupBean> getAllGroups() {
		return allGroups;
	}

	/**
	 * @return the new user id
	 */
	public Integer getNewUsnr() {
		return newUsnr;
	}

	/**
	 * @param newUsnr the new user id
	 */
	public void setNewUsnr(Integer newUsnr) {
		this.newUsnr = newUsnr;
	}

	/**
	 * @return the new acl
	 */
	public AclBean getNewAcl() {
		return newAcl;
	}

	/**
	 * @param activePrivilege the active privilege
	 */
	public void setActivePrivilege(GroupPrivilegeBean activePrivilege) {
		this.activePrivilege = activePrivilege;
	}

	/**
	 * @return the studygroup
	 */
	public StudygroupBean getStudygroup() {
		return studygroup;
	}

	/**
	 * @param studygroup the studygroup to set
	 */
	public void setStudygroup(StudygroupBean studygroup) {
		this.studygroup = studygroup;
	}

	/**
	 * add the attribute newAttribute to the current study (no database operation
	 * yet)
	 * 
	 * @param facesContext the faces context
	 */
	public void addAttribute(SquareFacesContext facesContext) {
		study.addAttribute(newAttributeKey);
	}

	/**
	 * remove the attribute referenced by key from the study attributes in the list
	 * (no database operation yet)
	 * 
	 * @param facesContext the faces context
	 * @param key the id of the attribute
	 */
	public void removeAttribute(SquareFacesContext facesContext, String key) {
		Iterator<StudyAttributeBean> i = study.getAttributes().iterator();
		while (i.hasNext()) {
			if (i.next().getKey().equals(key)) {
				i.remove();
				break; // one found is enough, as key should be unique
			}
		}
	}

	/**
	 * set the locales of the study to the values of the parent study group
	 * 
	 * @param event the new value
	 */
	public void updateLocales(ValueChangeEvent event) {
		for (StudygroupBean bean : studyGroups) {
			if (bean.getPk().equals(event.getNewValue())) {
				study.setLocales(bean.getLocales());
			}
		}
	}

	/**
	 * @return the newAttributeKey
	 */
	public String getNewAttributeKey() {
		return newAttributeKey;
	}

	/**
	 * @param newAttributeKey the newAttributeKey to set
	 */
	public void setNewAttributeKey(String newAttributeKey) {
		this.newAttributeKey = newAttributeKey;
	}
}
