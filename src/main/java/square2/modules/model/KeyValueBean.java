package square2.modules.model;

/**
 * 
 * @author henkej
 * 
 */
public class KeyValueBean {
	private Integer key;
	private String value;

	/**
	 * create new key value bean
	 * 
	 * @param key
	 *          the key
	 * @param value
	 *          the value
	 */
	public KeyValueBean(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * @return the key
	 */
	public Integer getKey() {
		return key;
	}

	/**
	 * @param key
	 *          the key to set
	 */
	public void setKey(Integer key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *          the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
