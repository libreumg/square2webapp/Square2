package square2.modules.model.admin;

import java.io.Serializable;
import java.util.Date;

/**
 * 
 * @author henkej
 * 
 */
public class UserBean implements Serializable, Comparable<UserBean> {
	private static final long serialVersionUID = 1L;
	private String forename;
	private String surname;
	private String username;
	private Integer usnr;
	private Date expire;

	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append(forename).append(" ").append(surname);
		buf.append(" (").append(username).append(", ");
		buf.append(usnr).append(")");
		buf.append(expire);
		return buf.toString();
	}

	@Override
	public int compareTo(UserBean o) {
		String sortname = getSortname();
		return o == null ? 0 : sortname.compareTo(o.getSortname());
	}

	/**
	 * @return true if expire is before the current date
	 */
	public Boolean getIsExpired() {
		return expire == null ? null : expire.before(new Date());
	}

	private String getSortname() {
		return new StringBuilder().append(surname).append(forename).toString().toLowerCase();
	}

	/**
	 * get the full name of the user
	 * 
	 * @return the full name
	 */
	public String getFullname() {
		return new StringBuilder().append(forename).append(" ").append(surname).toString();
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename
	 *          the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname
	 *          the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}

	/**
	 * @param username
	 *          the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the usnr
	 */
	public Integer getUsnr() {
		return usnr;
	}

	/**
	 * @param usnr
	 *          the usnr to set
	 */
	public void setUsnr(Integer usnr) {
		this.usnr = usnr;
	}

	/**
	 * @return the expire
	 */
	public Date getExpire() {
		return expire;
	}

	/**
	 * @param expire
	 *          the expire to set
	 */
	public void setExpire(Date expire) {
		this.expire = expire;
	}
}
