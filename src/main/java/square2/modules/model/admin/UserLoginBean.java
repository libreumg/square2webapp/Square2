package square2.modules.model.admin;

import java.text.SimpleDateFormat;
import java.util.Date;

import square2.modules.model.ProfileBean;

/**
 * 
 * @author henkej
 *
 */
public class UserLoginBean {
	private String forename;
	private String surname;
	private String username;
	private Integer usnr;
	private Date start;

	/**
	 * create new use rlogin bean
	 * 
	 * @param bean
	 *          the profile bean
	 */
	public UserLoginBean(ProfileBean bean) {
		this.forename = bean.getForename();
		this.surname = bean.getSurname();
		this.username = bean.getUsername();
		this.usnr = bean.getUsnr();
		this.start = new Date();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append(forename).append(" ").append(surname);
		buf.append(" (").append(username).append(", ").append(usnr).append("): ");
		buf.append(new SimpleDateFormat("dd.MM.yyyy HH:mm").format(start));
		return buf.toString();
	}

	/**
	 * @return the unique id
	 */
	public String getUniqueId() {
		StringBuilder buf = new StringBuilder();
		buf.append(usnr).append("_");
		buf.append(new SimpleDateFormat("yyyyMMddHHmm").format(start));
		return buf.toString();
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename
	 *          the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname
	 *          the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the usnr
	 */
	public Integer getUsnr() {
		return usnr;
	}

	/**
	 * @return the start
	 */
	public Date getStart() {
		return start;
	}
}
