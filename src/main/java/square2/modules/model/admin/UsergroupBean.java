package square2.modules.model.admin;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import square2.modules.model.ProfileBean;

/**
 * 
 * @author henkej
 *
 */
public class UsergroupBean implements Serializable, Comparable<UsergroupBean> {
	private static final long serialVersionUID = 1L;

	private final Integer pk;
	private String name;
	private List<ProfileBean> users;

	/**
	 * create a new user group bean
	 * 
	 * @param pk
	 *          the id of the user group
	 * @param name
	 *          the name of the group bean
	 * @param users
	 *          the list of users
	 */
	public UsergroupBean(Integer pk, String name, List<ProfileBean> users) {
		super();
		this.pk = pk;
		this.name = name;
		this.users = new ArrayList<>();
		if (users != null) {
			this.users.addAll(users);
		}
	}

	@Override
	public int compareTo(UsergroupBean o) {
		return name == null || o == null || o.getName() == null ? 0 : name.compareTo(o.getName());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the users
	 */
	public List<ProfileBean> getUsers() {
		return users;
	}
}
