package square2.modules.model.admin;

import java.util.Locale;

import de.ship.dbppsquare.square.enums.EnumIsocode;

/**
 * 
 * @author henkej
 *
 */
public class ImpressumBean {
	private final Locale locale;
	private final String isocode;
	private String content;

	/**
	 * @param isocodeEnum
	 *          isocode enum of this translation
	 * @param content
	 *          the translation
	 */
	public ImpressumBean(EnumIsocode isocodeEnum, String content) {
		super();
		this.isocode = isocodeEnum == null ? "" : (isocodeEnum.getLiteral() == null ? "" : isocodeEnum.getLiteral());
		this.locale = Locale.forLanguageTag(isocode.toLowerCase().replaceAll("\\_", "-"));
		this.content = content == null ? "" : content;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content
	 *          the content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the locale
	 */
	public Locale getLocale() {
		return locale;
	}

	/**
	 * @return the isocode
	 */
	public String getIsocode() {
		return isocode;
	}
}
