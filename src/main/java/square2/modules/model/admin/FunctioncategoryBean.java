package square2.modules.model.admin;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class FunctioncategoryBean implements Serializable, Comparable<FunctioncategoryBean> {
	private static final long serialVersionUID = 1L;

	private final Integer pk;
	private String name;
	private String description;
	
	private String oldName;

	/**
	 * @param pk
	 *          the id of the function category
	 */
	public FunctioncategoryBean(Integer pk) {
		super();
		this.pk = pk;
	}

	@Override
	public int compareTo(FunctioncategoryBean o) {
		return o == null || name == null || o.getName() == null ? 0 : name.compareTo(o.getName());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the oldName
	 */
	public String getOldName() {
		return oldName;
	}

	/**
	 * @param oldName the oldName to set
	 */
	public void setOldName(String oldName) {
		this.oldName = oldName;
	}
}
