package square2.modules.model;

/**
 * 
 * @author henkej
 *
 */
public enum SquareLocale {
	/**
	 * de_DE
	 */
	GERMAN("de_DE"),
	/**
	 * en
	 */
	ENGLISH("en"),
	/**
	 * ro
	 */
	ROMANIAN("ro"),
	/**
	 * es
	 */
	SPANISH("es"),
	/**
	 * languagekey (not a real language, but shows the translation keys; abusing chinese code)
	 */
	LANGUAGEKEY("zh_CN");

	private final String value;

	private SquareLocale(String value) {
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public final String get() {
		return this.value;
	}
}
