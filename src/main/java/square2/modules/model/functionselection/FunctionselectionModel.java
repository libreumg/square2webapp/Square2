package square2.modules.model.functionselection;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import square2.db.control.AnalysisGateway;
import square2.db.control.ReportGateway;
import square2.help.SquareFacesContext;
import square2.modules.model.report.analysis.AnalysisFunctionBean;
import square2.modules.model.report.analysis.AnalysisModel;
import square2.modules.model.template.UsageBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class FunctionselectionModel extends AnalysisModel implements Serializable {
  private static final long serialVersionUID = 1L;
  private static final Logger LOGGER = LogManager.getLogger(FunctionselectionModel.class);
  private List<String> cachedCategoryNames;

  /**
   * load information about usage of this template
   * 
   * @param facesContext of this current function call
   * @param fkFunctionlist id of the function list
   * @return true if no error occurred, false otherwise
   */
  public boolean loadUsage(SquareFacesContext facesContext, Integer fkFunctionlist) {
    try {
      List<String> reportList = new ReportGateway(facesContext).getAllReportNamesOfTemplate(fkFunctionlist);
      List<String> functionlistNames = new AnalysisGateway(facesContext).getAllAnatempNamesOfTemplate(fkFunctionlist);
      setUsage(new ArrayList<>());
      for (String item : reportList) {
        getUsage().add(new UsageBean(item, UsageBean.REPORT));
      }
      for (String item : functionlistNames) {
        getUsage().add(new UsageBean(item, UsageBean.FUNCTIONLIST));
      }
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * get pagelength for the chosen analysis functions table
   * 
   * @param facesContext the context of this function call
   * 
   * @return the pagelength; if none is found, use the default 20
   */
  public Integer getChosenPagelength(SquareFacesContext facesContext) {
    return facesContext.getProfile().getConfigInteger("config.template.paginator.anaparfun.pagelength", 20);
  }

  /**
   * get the pagelength menu for the chosen analysis functions table
   * 
   * @param facesContext the context of this function call
   * @return the page length menu; [[10,20,50,100,-1],[10,20,50,100,'all']] as default
   */
  public String getPagelengthMenu(SquareFacesContext facesContext) {
    StringBuilder buf = new StringBuilder();
    buf.append("[[").append(getChosenPagelength(facesContext)).append(",10,20,50,100,-1],['");
    buf.append("default").append("',10,20,50,100,'").append("all").append("']]");
    return buf.toString();
  }

  /**
   * approve changes to the order of chosen functions, including a renumbering in decimal steps
   * 
   * @param facesContext the context of this function call
   * @return true or false
   */
  public boolean approveReordering(SquareFacesContext facesContext) {
    try {
      List<AnalysisFunctionBean> list = getTemplate().getChosen();
      list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
      Integer counter = 0;
      for (AnalysisFunctionBean bean : list) {
        counter += 10;
        bean.setOrderNr(counter);
      }
      new AnalysisGateway(facesContext).updateAnaparfunOrder(getTemplate().getChosen());
      return true;
    } catch (DataAccessException e) {
      facesContext.notifyException(e);
      LOGGER.error(e.getMessage(), e);
      return false;
    }
  }

  /**
   * @param facesContext the faces context
   * @return a list of all categories
   */
  public List<String> getCategoryNames(SquareFacesContext facesContext) {
    if (cachedCategoryNames == null) {
      cachedCategoryNames = new AnalysisGateway(facesContext).getCategoryNames();
    }
    return cachedCategoryNames;
  }
}
