package square2.modules.model;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Base64;
import java.util.List;

import javax.faces.context.ExternalContext;
import javax.servlet.ServletContext;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.CloseableDSLContext;
import org.jooq.exception.DataAccessException;

import square2.db.control.LoginGateway;
import square2.help.ContextKey;
import square2.help.SquareFacesContext;
import square2.modules.model.admin.UserBean;

/**
 * 
 * @author henkej
 *
 */
public abstract class SquareModel {
	private static final Logger LOGGER = LogManager.getLogger("Square2");

	private List<UserBean> allSquareUsers;

	/**
	 * get the latex root folder
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return the folder or /tmp if no folder is found
	 */
	public String getLatexRoot(SquareFacesContext facesContext) {
		ServletContext context = (ServletContext) facesContext.getExternalContext().getContext();
		String latexRoot = (String) context.getAttribute(ContextKey.LATEXROOT.get());
		if (latexRoot == null || latexRoot.trim().equals("")) {
			LOGGER.warn("no latexroot defined in config file, using /tmp instead");
			latexRoot = "/tmp";
		}
		return latexRoot;
	}

	/**
	 * get the latex buffer size
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return the buffer size of 100000 if none is found
	 */
	public int getLatexBufSize(SquareFacesContext facesContext) {
		ServletContext context = (ServletContext) facesContext.getExternalContext().getContext();
		String latexBufSize = (String) context.getAttribute(ContextKey.LATEX_BUF_SIZE.get());
		if (latexBufSize == null || latexBufSize.trim().equals("")) {
			LOGGER.warn("no latexBufSize defined in config file, using 100000 instead");
			latexBufSize = "100000";
		}
		int result;
		try {
			result = Integer.parseInt(latexBufSize);
		} catch (NumberFormatException e) {
			LOGGER.warn("latexBufSize=" + latexBufSize + " defined in config file is not a number, using 100000 instead", e);
			result = 100000;
		}
		return result;
	}

	/**
	 * get the r config file location
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return the config file location
	 */
	public String getRConfigFile(SquareFacesContext facesContext) {
		ServletContext context = (ServletContext) facesContext.getExternalContext().getContext();
		return (String) context.getAttribute(ContextKey.RSERVERACCESS.get());
	}

	/**
	 * generate download stream with content
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param resultLength
	 *          length of resulting file
	 * @param filename
	 *          of resulting file
	 * @param contentType
	 *          of resulting file
	 * @param inputStream
	 *          to be used for file content
	 * @return true in any case; exceptions are catched by notifications
	 */
	public boolean generateDownloadStreamFromInputStream(SquareFacesContext facesContext, long resultLength,
			String filename, String contentType, InputStream inputStream) {
		ExternalContext context = facesContext.getExternalContext();
		context.responseReset();
		// context.setResponseBufferSize((int) resultLength);
		context.setResponseContentType(contentType);
		// context.setResponseHeader("Content-Length", String.valueOf(resultLength));
		context.setResponseHeader("Content-Disposition", "attachment; filename=\"".concat(filename).concat("\""));
		try (BufferedOutputStream output = new BufferedOutputStream(context.getResponseOutputStream(), 10240)) {
			byte[] buffer = new byte[10240];
			int length;
			while ((length = inputStream.read(buffer)) > 0) {
				output.write(buffer, 0, length);
			}
			inputStream.close();
		} catch (IOException e) {
			facesContext.notifyError(e.getMessage());
		}
		facesContext.responseComplete();
		return true;
	}

	/**
	 * generate a download stream from a string
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param filename
	 *          the name of the file
	 * @param contentType
	 *          the content type
	 * @param byteArray
	 *          the byte array
	 * @return true
	 */
	public boolean generateDownloadStreamFromByteArray(SquareFacesContext facesContext, String filename,
			String contentType, byte[] byteArray) {
		ExternalContext context = facesContext.getExternalContext();
		context.responseReset();
		context.setResponseContentType(contentType);
		context.setResponseHeader("Content-Disposition",
				"attachment; filename=\"".concat(filename == null ? "null" : filename).concat("\""));
		try {
			context.getResponseOutputStream().write(byteArray);
		} catch (IOException e) {
			facesContext.notifyError(e.getMessage());
		}
		facesContext.responseComplete();
		return true;
	}

	/**
	 * generate a download stream from a byte array
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param resultLength
	 *          the resulting length
	 * @param filename
	 *          the name of the file
	 * @param contentType
	 *          the content type
	 * @param bytes
	 *          the bytes
	 * @return true
	 */
	public boolean generateDownloadStreamFromByteArray(SquareFacesContext facesContext, long resultLength,
			String filename, String contentType, byte[] bytes) {
		ExternalContext context = facesContext.getExternalContext();
		context.responseReset();
		// context.setResponseBufferSize((int) resultLength);
		context.setResponseContentType(contentType);
		// context.setResponseHeader("Content-Length", String.valueOf(resultLength));
		context.setResponseHeader("Content-Disposition",
				"attachment; filename=\"".concat(filename == null ? "null" : filename).concat("\""));
		try {
			context.getResponseOutputStream().write(bytes);
		} catch (IOException e) {
			facesContext.notifyError(e.getMessage());
		}
		facesContext.responseComplete();
		return true;
	}

	/**
	 * generate a decoded download stream from a base64 encoded string
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param resultLength
	 *          the resulting length
	 * @param filename
	 *          the name of the file
	 * @param contentType
	 *          the content type
	 * @param s
	 *          the string
	 * @return true
	 */
	public boolean generateDownloadStreamFromBase64String(SquareFacesContext facesContext, long resultLength,
			String filename, String contentType, String s) {
		byte[] bytes = Base64.getMimeDecoder().decode(s);
		return generateDownloadStreamFromByteArray(facesContext, resultLength, filename, contentType, bytes);
	}

	/**
	 * generate and return download stream to jsf
	 * 
	 * @param facesContext
	 *          of this function call
	 * @param absoluteFileName
	 *          absolute file name to reference file
	 * @param relativeFileName
	 *          relative file name to put to the output stream
	 * @param responseContent
	 *          content of response to be manipulated for a download instead of a
	 *          jsf page result
	 * @return result of
	 *         {@link #generateDownloadStreamFromInputStream(SquareFacesContext, long, String, String, InputStream)
	 *         generateDownloadStreamFromInputStream}
	 * @throws FileNotFoundException
	 *           if no file has been found
	 */
	public boolean generateDownloadStreamFromLocalFile(SquareFacesContext facesContext, String absoluteFileName,
			String relativeFileName, String responseContent) throws FileNotFoundException {
		File file = new File(absoluteFileName);
		if (file.exists()) {
			return generateDownloadStreamFromInputStream(facesContext, file.length(), relativeFileName, responseContent,
					new BufferedInputStream(new FileInputStream(file), 10240));
		}
		throw new FileNotFoundException("file ".concat(absoluteFileName).concat(" not available or not readable"));
	}

	/**
	 * generate and return download stream of string to jsf
	 * 
	 * @param facesContext
	 *          of this function call
	 * @param content
	 *          the content to be streamed
	 * @param filename
	 *          the filename of the download file
	 * @param contentType
	 *          the content type
	 * @return true or false
	 */
	public boolean generateDownloadStreamFromString(SquareFacesContext facesContext, String content, String filename,
			String contentType) {
		ExternalContext context = facesContext.getExternalContext();
		context.responseReset();
		context.setResponseContentType(contentType);
		context.setResponseHeader("Content-Disposition",
				"attachment; filename=\"".concat(filename == null ? "null" : filename).concat("\""));
		try {
			context.getResponseOutputWriter().write(content);
		} catch (IOException e) {
			facesContext.notifyError(e.getMessage());
		}
		facesContext.responseComplete();
		return true;
	}

	/**
	 * get pdf file from absolute file position
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param file
	 *          the file
	 * @return the pdf content if found, null otherwise
	 * @throws IOException
	 *           on io exceptions
	 */
	public static final byte[] getPdfFromFile(SquareFacesContext facesContext, File file) throws IOException {
		byte[] result = null;
		if (file != null && file.exists()) {
			ByteArrayOutputStream bytesOut = new ByteArrayOutputStream();
			try (BufferedOutputStream output = new BufferedOutputStream(bytesOut);
					InputStream inputStream = new BufferedInputStream(new FileInputStream(file), 10240)) {
				byte[] buffer = new byte[10240];
				int length;
				while ((length = inputStream.read(buffer)) > 0) {
					output.write(buffer, 0, length);
				}
			}
			bytesOut.close();
			result = bytesOut.toByteArray();
		} else {
			throw new IOException("file not found: " + (file == null ? "null" : file.getAbsolutePath()));
		}
		return result;
	}

	/**
	 * set all Square² users due to conditions defined in params
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param params
	 *          contains conditions to set all Square² users
	 * @throws DataAccessException
	 *           if anything went wrong on database side
	 * @throws ModelException
	 *           if that exception happened
	 */
	public abstract void setAllSquareUsers(SquareFacesContext facesContext, Object... params)
			throws ModelException, DataAccessException;

	/**
	 * set allSquareUsers
	 * 
	 * @param list
	 *          the list of users
	 */
	public void resetAllSquareusers(List<UserBean> list) {
		allSquareUsers = list;
	}

	/**
	 * get all Square² users from db
	 * 
	 * @return list of users
	 */
	public List<UserBean> getAllSquareUsers() {
		return allSquareUsers;
	}

	/**
	 * reload profile privileges
	 * 
	 * @param facesContext
	 *          current context of this function call
	 */
	public void reloadProfilePrivileges(SquareFacesContext facesContext) {
		try {
			LoginGateway gw = new LoginGateway(facesContext);
			try (CloseableDSLContext jooq = gw.getJooq()) {
				List<PrivilegeBean> privileges = gw.getPrivileges(jooq,
						facesContext.getProfile().getUsnr());
				facesContext.getProfile().getPrivileges().clear();
				facesContext.getProfile().getPrivileges().addAll(privileges);
			} catch (DataAccessException | ClassNotFoundException | SQLException e) {
				throw new DataAccessException(e.getMessage(), e);
			}
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
		}
	}
}
