package square2.modules.model;

/**
 * 
 * @author henkej
 *
 */
public class UserAccessLevelBean {
	private String forename;
	private String surname;
	private Integer usnr;
	private AclBean acl;

	/**
	 * create new user access level bean
	 */
	public UserAccessLevelBean() {
		this.acl = new AclBean();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{forename=").append(forename);
		buf.append(",surname=").append(surname);
		buf.append(",usnr=").append(usnr);
		buf.append(",acl=").append(acl);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * @return the full name (forename + surname)
	 */
	public String getFullname() {
		return new StringBuilder(forename).append(" ").append(surname).toString();
	}

	/**
	 * @return the forename
	 */
	public String getForename() {
		return forename;
	}

	/**
	 * @param forename
	 *          the forename to set
	 */
	public void setForename(String forename) {
		this.forename = forename;
	}

	/**
	 * @return the surname
	 */
	public String getSurname() {
		return surname;
	}

	/**
	 * @param surname
	 *          the surname to set
	 */
	public void setSurname(String surname) {
		this.surname = surname;
	}

	/**
	 * @return the usnr
	 */
	public Integer getUsnr() {
		return usnr;
	}

	/**
	 * @param usnr
	 *          the usnr to set
	 */
	public void setUsnr(Integer usnr) {
		this.usnr = usnr;
	}

	/**
	 * @return the acl
	 */
	public AclBean getAcl() {
		return acl;
	}
}
