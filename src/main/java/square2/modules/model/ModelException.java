package square2.modules.model;

/**
 * 
 * @author henkej
 *
 */
public class ModelException extends Exception {
	private static final long serialVersionUID = 1L;

	/**
	 * create new model exception
	 * 
	 * @param message
	 *          the message
	 */
	public ModelException(String message) {
		super(message);
	}
}
