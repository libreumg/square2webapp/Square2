package square2.modules.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class GroupPrivilegeBean implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private final Integer pk;
	private final Boolean isGroup;
	private final Integer fkPrivilege;
	private Integer fkGroup;
	private Integer fkUsnr;
	private String name;
	private List<ProfileBean> users;
	private AclBean acl;

	/**
	 * generate new group privilege bean
	 * 
	 * @param pk
	 *          the id
	 * @param isGroup
	 *          the flag if this is a group
	 * @param fkPrivilege
	 *          the privilege id
	 */
	public GroupPrivilegeBean(Integer pk, Boolean isGroup, Integer fkPrivilege) {
		this.pk = pk;
		this.isGroup = isGroup;
		this.fkPrivilege = fkPrivilege;
		this.acl = new AclBean();
		this.users = new ArrayList<>();
	}

	/**
	 * return true if usnr is in list of users
	 * 
	 * @param usnr
	 *          to be checked
	 * @return true or false
	 */
	public boolean containsUsnr(Integer usnr) {
		for (ProfileBean bean : users) {
			if (bean.getUsnr().equals(usnr)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * get comma separated list of full names
	 * 
	 * @return list of full names
	 */
	public String getFullnames() {
		StringBuilder buf = new StringBuilder();
		Iterator<ProfileBean> iterator = users.iterator();
		while (iterator.hasNext()) {
			buf.append(iterator.next().getFullname());
			buf.append(iterator.hasNext() ? ", " : "");
		}
		return buf.toString();
	}

	/**
	 * @return the fkGroup
	 */
	public Integer getFkGroup() {
		return fkGroup;
	}

	/**
	 * @param fkGroup
	 *          the fkGroup to set
	 */
	public void setFkGroup(Integer fkGroup) {
		this.fkGroup = fkGroup;
	}

	/**
	 * @return the fkUsnr
	 */
	public Integer getFkUsnr() {
		return fkUsnr;
	}

	/**
	 * @param fkUsnr
	 *          the fkUsnr to set
	 */
	public void setFkUsnr(Integer fkUsnr) {
		this.fkUsnr = fkUsnr;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the isGroup
	 */
	public Boolean getIsGroup() {
		return isGroup;
	}

	/**
	 * @return the fkPrivilege
	 */
	public Integer getFkPrivilege() {
		return fkPrivilege;
	}

	/**
	 * @return the users
	 */
	public List<ProfileBean> getUsers() {
		return users;
	}

	/**
	 * @return the acl
	 */
	public AclBean getAcl() {
		return acl;
	}
}
