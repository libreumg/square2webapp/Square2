package square2.modules.model;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import square2.db.control.VersionGateway;
import square2.help.SquareFacesContext;
import square2.r.control.FunctionRegistratorGateway;
import square2.r.control.RGateway;
import square2.r.control.SmiGateway;
import square2.r.control.SquareControl2Gateway;
import square2.r.control.SquareReportRendererGateway;

/**
 * 
 * @author henkej
 * 
 */
@Named
@RequestScoped
public class VersionBean {

  @Inject
  @Named(value = "sessionVersionBean")
  private SessionVersionBean sessionVersionBean;

  private static final Logger LOGGER = LogManager.getLogger("Square2");

  /**
   * @param facesContext the faces context
   * @return the db version
   */
  public String getDbVersion(SquareFacesContext facesContext) {
    sessionVersionBean.setDbVersion(null, "76");
    if (sessionVersionBean.getDbVersion() == null) {
      String dbVersion = null;
      try {
        dbVersion = new VersionGateway(facesContext).getVersion();
      } catch (DataAccessException e) {
        LOGGER.error(e.getMessage(), e);
        dbVersion = e.getMessage();
      }
      sessionVersionBean.setDbVersion(dbVersion);
    }
    return sessionVersionBean.getDbVersion(facesContext);
  }

  /**
   * @return the java version
   */
  public String getJavaVersion() {
    if (sessionVersionBean.getJavaVersion() == null) {
      String javaVersion = System.getProperty("java.version");
      String javaVendor = System.getProperty("java.vendor");
      StringBuilder buf = new StringBuilder();
      buf.append(javaVersion);
      buf.append(" (").append(javaVendor).append(")");
      sessionVersionBean.setJavaVersion(javaVersion);
    }
    return sessionVersionBean.getJavaVersion();
  }

  /**
   * @param facesContext the faces context
   * @return the R version
   */
  public String getRVersion(SquareFacesContext facesContext) {
    if (sessionVersionBean.getrVersion() == null) {
      reloadVersionsFromR(facesContext);
    }
    return sessionVersionBean.getrVersion();
  }

  private void reloadVersionsFromR(SquareFacesContext facesContext) {
    sessionVersionBean.setSquaremetadatainterfaceVersion("?", SmiGateway.VERSION);
    sessionVersionBean.setrVersion("?");
    sessionVersionBean.setSquareControl2Version("?", SquareControl2Gateway.VERSION);
    sessionVersionBean.setSquarereportrendererVersion("?", SquareReportRendererGateway.VERSION);
    sessionVersionBean.setFunctionRegistratorVersion("?", FunctionRegistratorGateway.VERSION);
    try (RGateway gw = new RGateway(facesContext)) {
      sessionVersionBean.setSquaremetadatainterfaceVersion(gw.getVersion(SmiGateway.LIBNAME));
      sessionVersionBean.setSquarereportrendererVersion(gw.getVersion(SquareReportRendererGateway.LIBNAME));
      sessionVersionBean.setrVersion(gw.getVersion(null));
      sessionVersionBean.setSquareControl2Version(gw.getVersion(SquareControl2Gateway.LIBNAME));
    	sessionVersionBean.setFunctionRegistratorVersion(gw.getVersion(FunctionRegistratorGateway.LIBNAME));
    } catch (Exception e) {
      LOGGER.error(e.getMessage());
      facesContext.notifyException(e);
    }
  }

  /**
   * @param facesContext the faces context
   * @return get the squareControl version
   */
  public String getSquareControl2Version(SquareFacesContext facesContext) {
    if (sessionVersionBean.getSquareControl2Version(facesContext) == null) {
      reloadVersionsFromR(facesContext);
    }
    return sessionVersionBean.getSquareControl2Version(facesContext);
  }

  /**
   * @param facesContext the faces context
   * @return the squaremetadatainterface version
   */
  public String getSquaremetadatainterfaceVersion(SquareFacesContext facesContext) {
    if (sessionVersionBean.getSquaremetadatainterfaceVersion(facesContext) == null) {
      reloadVersionsFromR(facesContext);
    }
    return sessionVersionBean.getSquaremetadatainterfaceVersion(facesContext);
  }

  /**
   * @param facesContext the faces context
   * @return the squarereportrenderer version
   */
  public String getSquarereportrendererVersion(SquareFacesContext facesContext) {
    if (sessionVersionBean.getSquarereportrendererVersion(facesContext) == null) {
      reloadVersionsFromR(facesContext);
    }
    return sessionVersionBean.getSquarereportrendererVersion(facesContext);
  }

  /**
   * @param facesContext the faces context
   * @return the functionregistrator version
   */
  public String getFunctionRegistratorVersion(SquareFacesContext facesContext) {
    if (sessionVersionBean.getFunctionRegistratorVersion(facesContext) == null) {
      reloadVersionsFromR(facesContext);
    }
    return sessionVersionBean.getFunctionRegistratorVersion(facesContext);
  }

  /**
   * @param sessionVersionBean the session version bean
   */
  public void setSessionVersionBean(SessionVersionBean sessionVersionBean) {
    this.sessionVersionBean = sessionVersionBean;
  }
}
