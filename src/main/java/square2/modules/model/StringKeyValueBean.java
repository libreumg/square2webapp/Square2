package square2.modules.model;

/**
 * 
 * @author henkej
 *
 */
public class StringKeyValueBean {
	private String key;
	private String value;

	/**
	 * create new key value bean of strings
	 * 
	 * @param key
	 *          the key
	 * @param value
	 *          the value
	 */
	public StringKeyValueBean(String key, String value) {
		this.key = key;
		this.value = value;
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *          the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value
	 *          the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
