package square2.modules.model;

import java.io.Serializable;

/**
 * 
 * @author henkej
 *
 */
public class AnaparfunOutputBean implements Serializable, Comparable<AnaparfunOutputBean> {
	private static final long serialVersionUID = 1L;

	private final Integer fkAnaparfun;
	private final Integer fkFunctionoutput;
	private final String anaparfunName;
	private final String functionoutputName;
	private final Integer orderNr;

	/**
	 * create new anaparfun output bean
	 * 
	 * @param fkAnaparfun
	 *          the id of the anaparfun
	 * @param fkFunctionoutput
	 *          the id of the function output
	 * @param anaparfunName
	 *          the name of the anaparfun
	 * @param functionoutputName
	 *          the name of the function output
	 * @param orderNr
	 *          the order of the anaparfun
	 */
	public AnaparfunOutputBean(Integer fkAnaparfun, Integer fkFunctionoutput, String anaparfunName,
			String functionoutputName, Integer orderNr) {
		super();
		this.fkAnaparfun = fkAnaparfun;
		this.fkFunctionoutput = fkFunctionoutput;
		this.anaparfunName = anaparfunName;
		this.functionoutputName = functionoutputName;
		this.orderNr = orderNr;
	}

	public String toString() {
		StringBuilder buf = new StringBuilder();
		buf.append(fkAnaparfun).append(":").append(fkFunctionoutput).append(" = ");
		buf.append(anaparfunName).append(":").append(functionoutputName);
		buf.append(", orderNr=").append(orderNr);
		return buf.toString();
	}

	@Override
	public int compareTo(AnaparfunOutputBean o) {
		return orderNr == null || o == null || o.getOrderNr() == null ? 0 : orderNr.compareTo(o.getOrderNr());
	}

	/**
	 * @return the fkAnaparfun
	 */
	public Integer getFkAnaparfun() {
		return fkAnaparfun;
	}

	/**
	 * @return the fkFunctionoutput
	 */
	public Integer getFkFunctionoutput() {
		return fkFunctionoutput;
	}

	/**
	 * @return the anaparfunName
	 */
	public String getAnaparfunName() {
		return anaparfunName;
	}

	/**
	 * @return the functionoutputName
	 */
	public String getFunctionoutputName() {
		return functionoutputName;
	}

	/**
	 * @return the orderNr
	 */
	public Integer getOrderNr() {
		return orderNr;
	}
}
