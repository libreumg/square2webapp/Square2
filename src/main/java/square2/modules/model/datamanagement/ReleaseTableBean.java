package square2.modules.model.datamanagement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class ReleaseTableBean implements Serializable, Comparable<ReleaseTableBean> {
	private static final long serialVersionUID = 1L;

	private final String name;
	private final Integer rowcount;
	private List<ColumnBean> columns;

	/**
	 * create new release table bean
	 * 
	 * @param name
	 *          the name
	 * @param rowcount
	 *          the row count
	 */
	public ReleaseTableBean(String name, Integer rowcount) {
		this.name = name;
		this.rowcount = rowcount;
		columns = new ArrayList<>();
	}

	@Override
	public int compareTo(ReleaseTableBean o) {
		return name == null || o == null || o.getName() == null ? 0 : name.compareTo(o.getName());
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return the columns
	 */
	public List<ColumnBean> getColumns() {
		return columns;
	}

	/**
	 * @return the row count
	 */
	public Integer getRowcount() {
		return rowcount;
	}
}
