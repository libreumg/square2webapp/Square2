package square2.modules.model.datamanagement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.servlet.http.Part;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.exception.DataAccessException;

import square2.db.control.DatamanagementGateway;
import square2.db.control.StudyGateway;
import square2.help.SquareFacesContext;
import square2.modules.model.AclBean;
import square2.modules.model.GroupPrivilegeBean;
import square2.modules.model.ModelException;
import square2.modules.model.SquareModel;
import square2.modules.model.UserApplRightBean;
import square2.modules.model.admin.UsergroupBean;
import square2.modules.model.study.VariableBean;

/**
 * 
 * @author henkej
 *
 */
@Named
@SessionScoped
public class DatamanagementModel extends SquareModel implements Serializable {
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LogManager.getLogger(DatamanagementModel.class);

	private List<ReleaseBean> releases;
	private ReleaseBean release;
	private ReleaseInfoBean infoBean;
	private UserApplRightBean newUser;
	private List<ElementBean> elements;

	private List<GroupPrivilegeBean> groupPrivileges;
	private List<UsergroupBean> allGroups;
	private Integer newGroup;
	private Integer newUsnr;
	private AclBean newAcl;
	private GroupPrivilegeBean activePrivilege;

	private List<VariableBean> techvars;
	private List<ReleaseTableBean> releaseTables;
	private Part file;
	private String tableName;
	private char separator;

	@Override
	public void setAllSquareUsers(SquareFacesContext facesContext, Object... params)
			throws ModelException, DataAccessException {
		if (params == null || params.length < 1 || (!(params[0] instanceof Integer))) {
			throw new ModelException("params is null, empty or not of class Integer, but should contain the privilegeId");
		}
		Integer privilegeId = (Integer) params[0];
		super.resetAllSquareusers(
				new DatamanagementGateway(facesContext).getAllSquareUsersWithoutUserPrivilege(privilegeId));
	}

	/**
	 * load all releases for the welcome page releases list and prepare an empty new
	 * release that can be added
	 * 
	 * @param facesContext
	 *          of this current function call
	 * @return true if database access was successful, false otherwise
	 */
	public boolean prepareWelcomePage(SquareFacesContext facesContext) {
		releases = new ArrayList<>();
		release = new ReleaseBean(null);
		release.setRolling(true); // default
		release.setActive(true); // default
		try {
			DatamanagementGateway gw = new DatamanagementGateway(facesContext);
			releases = gw.getAllReleases(false, true);
			elements = gw.getElements();
			techvars = new StudyGateway(facesContext).getAllIdVariables();
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * load release from bean to display its contents in item page
	 * 
	 * @param bean
	 *          the release bean
	 * @param facesContext
	 *          of this current function call
	 * @return true, if loading was successful, false otherwise
	 */
	public boolean prepareItemPage(ReleaseBean bean, SquareFacesContext facesContext) {
		DatamanagementGateway gw;
		try {
			gw = new DatamanagementGateway(facesContext);
			release = gw.getRelease(bean.getPk()); // reload release for possible database update
			release.getUserRights().addAll(gw.getUserPrivileges(release.getFkPrivilege()));
			newGroup = Integer.valueOf(0);
			newAcl = new AclBean().setString("rwx");
			groupPrivileges = gw.getAllGroupPrivileges(release.getFkPrivilege());
			allGroups = gw.getAllGroupsExceptOf(groupPrivileges);
			setAllSquareUsers(facesContext, release.getFkPrivilege());
			elements = gw.getElements();
			techvars = new StudyGateway(facesContext).getAllIdVariables();
			releaseTables = gw.loadReleaseTables(release.getPk());
			return true;
		} catch (DataAccessException | ModelException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * add a release
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean doAddRelease(SquareFacesContext facesContext) {
		try {
			boolean result = new DatamanagementGateway(facesContext).addRelease(release, facesContext.getUsnr());
			if (result) {
				reloadProfilePrivileges(facesContext);
				return true;
			} else {
				return false;
			}
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * edit a release
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return true or false
	 */
	public boolean doEditRelease(SquareFacesContext facesContext) {
		try {
			Integer affected = new DatamanagementGateway(facesContext).updateRelease(release);
			return affected < 0 ? false : true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * delete a release
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @param bean
	 *          the bean
	 * @return true or false
	 */
	public boolean doDeleteRelease(SquareFacesContext facesContext, ReleaseBean bean) {
		try {
			new DatamanagementGateway(facesContext).deleteRelease(bean.getPk(), bean.getFkPrivilege());
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * set user privilege due to definition; read, write and execute is atomic
	 * 
	 * @param facesContext
	 *          of this current function call
	 * @return true if successful, false on execption
	 */
	public boolean addPrivilege(SquareFacesContext facesContext) {
		// atomic read, write and execute
		newAcl.setRead(newAcl.getWrite());
		newAcl.setExecute(newAcl.getExecute());
		try {
			List<Integer> privileges = new ArrayList<>();
			privileges.add(release.getFkPrivilege());
			Integer affected = new DatamanagementGateway(facesContext).addUserGroupPrivilege(newUsnr, newGroup, privileges,
					newAcl);
			facesContext.notifyInformation("info.affected.add", affected);
			return true;
		} catch (DataAccessException e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * edit privileges; read, write and execute is atomic
	 * 
	 * @param facesContext
	 *          of this current function call
	 * @return true if successful, false on execption
	 */
	public boolean editPrivilege(SquareFacesContext facesContext) {
		try {
			// atomic read, write and execute
			activePrivilege.getAcl().setRead(activePrivilege.getAcl().getWrite());
			activePrivilege.getAcl().setExecute(activePrivilege.getAcl().getWrite());
			Integer affected = new DatamanagementGateway(facesContext).upsertUserGroupPrivilege(activePrivilege);
			facesContext.notifyInformation("info.affected.add", affected);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * delete privilege from db
	 * 
	 * @param facesContext
	 *          of this current function call
	 * @return true or false
	 */
	public boolean deletePrivilege(SquareFacesContext facesContext) {
		try {
			GroupPrivilegeBean bean = activePrivilege;
			Set<Integer> privileges = new HashSet<>();
			privileges.add(bean.getFkPrivilege());
			Integer affected = new DatamanagementGateway(facesContext).removeUserGroupPrivilege(bean.getFkUsnr(),
					bean.getFkGroup(), privileges);
			facesContext.notifyInformation("info.affected.remove", affected);
			return true;
		} catch (Exception e) {
			facesContext.notifyException(e);
			LOGGER.error(e.getMessage(), e);
			return false;
		}
	}

	/**
	 * load special information into the info bean
	 * 
	 * @param facesContext
	 *          of this current function call
	 */
	public void loadExtraInfo(SquareFacesContext facesContext) {
		DatamanagementGateway gw = new DatamanagementGateway(facesContext);
		infoBean.setMergecolumnname(gw.getUniqueName(infoBean.getMergecolumn()));
		infoBean.getVargroupNames().clear();
		infoBean.getVargroupNames().addAll(gw.getVariablegroupNamesUsingRelease(infoBean.getPk()));
	}
	
	/**
	 * if index is in textSet, ensure to encapsulate this by '
	 * 
	 * @param s
	 *          the string
	 * @param textSet
	 *          the text set
	 * @param index
	 *          the index
	 * @return the corrected string
	 */
	protected static final String toText(String s, Set<Integer> textSet, Integer index) {
		if (textSet.contains(index)) {
			if (s == null || s.trim().isEmpty()) {
				return "null";
			} else {
				if (s.trim().startsWith("'")) {
					s = s.trim().substring(1);
				}
				if (s.trim().endsWith("'")) {
					s = s.trim().substring(0, s.trim().length() - 1);
				}
				StringBuilder buf = new StringBuilder("'");
				buf.append(s.replaceAll("'", "\""));
				buf.append("'");
				return buf.toString();
			}
		} else {
			return s.replaceAll(";", "");
		}
	}

	/**
	 * get the rights map
	 * 
	 * @param facesContext
	 *          the context of this function call
	 * @return the rights map
	 * @throws Exception
	 *           if anything goes wrong
	 */
	public Map<Integer, Map<Integer, AclBean>> getRightMap(SquareFacesContext facesContext) throws Exception {
		return new DatamanagementGateway(facesContext).getRightMap();
	}

	/**
	 * @return the releases
	 */
	public List<ReleaseBean> getReleases() {
		return releases;
	}

	/**
	 * @return the release bean
	 */
	public ReleaseBean getRelease() {
		return release;
	}

	/**
	 * @param release
	 *          the release bean
	 */
	public void setRelease(ReleaseBean release) {
		this.release = release;
	}

	/**
	 * @return the new user
	 */
	public UserApplRightBean getNewUser() {
		return newUser;
	}

	/**
	 * @return the elements
	 */
	public List<ElementBean> getElements() {
		return elements;
	}

	/**
	 * @return the release tables
	 */
	public List<ReleaseTableBean> getReleaseTables() {
		return releaseTables;
	}

	/**
	 * @return the file
	 */
	public Part getFile() {
		return file;
	}

	/**
	 * @param file
	 *          the file
	 */
	public void setFile(Part file) {
		this.file = file;
	}

	/**
	 * @return the table name
	 */
	public String getTableName() {
		return tableName;
	}

	/**
	 * @param tableName
	 *          the table name
	 */
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	/**
	 * @return the separator
	 */
	public char getSeparator() {
		return separator;
	}

	/**
	 * @param separator
	 *          the separator
	 */
	public void setSeparator(char separator) {
		this.separator = separator;
	}

	/**
	 * @return the tech vars
	 */
	public List<VariableBean> getTechvars() {
		return techvars;
	}

	/**
	 * @param techvars
	 *          the tech vars
	 */
	public void setTechvars(List<VariableBean> techvars) {
		this.techvars = techvars;
	}

	/**
	 * @return the group privileges
	 */
	public List<GroupPrivilegeBean> getGroupPrivileges() {
		return groupPrivileges;
	}

	/**
	 * @return the new group
	 */
	public Integer getNewGroup() {
		return newGroup;
	}

	/**
	 * @param newGroup
	 *          the new group
	 */
	public void setNewGroup(Integer newGroup) {
		this.newGroup = newGroup;
	}

	/**
	 * @return all groups
	 */
	public List<UsergroupBean> getAllGroups() {
		return allGroups;
	}

	/**
	 * @return the new user id
	 */
	public Integer getNewUsnr() {
		return newUsnr;
	}

	/**
	 * @param newUsnr
	 *          the new user id
	 */
	public void setNewUsnr(Integer newUsnr) {
		this.newUsnr = newUsnr;
	}

	/**
	 * @return the new acl
	 */
	public AclBean getNewAcl() {
		return newAcl;
	}

	/**
	 * @param activePrivilege
	 *          the active privilege
	 */
	public void setActivePrivilege(GroupPrivilegeBean activePrivilege) {
		this.activePrivilege = activePrivilege;
	}

	/**
	 * @return the infoBean
	 */
	public ReleaseInfoBean getInfoBean() {
		return infoBean;
	}

	/**
	 * @param infoBean the infoBean to set
	 */
	public void setInfoBean(ReleaseInfoBean infoBean) {
		this.infoBean = infoBean;
	}
}
