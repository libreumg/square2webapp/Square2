package square2.modules.model.datamanagement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import square2.modules.model.AclBean;
import square2.modules.model.UserApplRightBean;

/**
 * 
 * @author henkej
 *
 */
public class ReleaseBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private Integer fkElement;
	private String studyName;
	private String name;
	private String shortname;
	private String description;
	private Boolean rolling;
	private Date releasedate;
	private String location;
	private Boolean backup;
	private Integer fkPrivilege;
	private Boolean active;
	private Integer mergecolumn;
	private AclBean acl;
	private final List<UserApplRightBean> userRights;

	/**
	 * create new release bean
	 * 
	 * @param pk
	 *          the id
	 */
	public ReleaseBean(Integer pk) {
		this.pk = pk;
		userRights = new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder("ReleaseBean{");
		buf.append("pk=").append(pk);
		buf.append(",name=").append(name);
		buf.append("}");
		return buf.toString();
	}
	
	/**
	 * @param pk the pk
	 */
	public void resetPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the fkElement
	 */
	public Integer getFkElement() {
		return fkElement;
	}

	/**
	 * @param fkElement
	 *          the fkElement to set
	 */
	public void setFkElement(Integer fkElement) {
		this.fkElement = fkElement;
	}

	/**
	 * @return the studyName
	 */
	public String getStudyName() {
		return studyName;
	}

	/**
	 * @param studyName
	 *          the studyName to set
	 */
	public void setStudyName(String studyName) {
		this.studyName = studyName;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *          the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the shortname
	 */
	public String getShortname() {
		return shortname;
	}

	/**
	 * @param shortname
	 *          the shortname to set
	 */
	public void setShortname(String shortname) {
		this.shortname = shortname;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *          the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the rolling
	 */
	public Boolean getRolling() {
		return rolling;
	}

	/**
	 * @param rolling
	 *          the rolling to set
	 */
	public void setRolling(Boolean rolling) {
		this.rolling = rolling;
	}

	/**
	 * @return the releasedate
	 */
	public Date getReleasedate() {
		return releasedate;
	}

	/**
	 * @param releasedate
	 *          the releasedate to set
	 */
	public void setReleasedate(Date releasedate) {
		this.releasedate = releasedate;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location
	 *          the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the backup
	 */
	public Boolean getBackup() {
		return backup;
	}

	/**
	 * @param backup
	 *          the backup to set
	 */
	public void setBackup(Boolean backup) {
		this.backup = backup;
	}

	/**
	 * @return the fkPrivilege
	 */
	public Integer getFkPrivilege() {
		return fkPrivilege;
	}

	/**
	 * @param fkPrivilege
	 *          the fkPrivilege to set
	 */
	public void setFkPrivilege(Integer fkPrivilege) {
		this.fkPrivilege = fkPrivilege;
	}

	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * @param active
	 *          the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @return the mergecolumn
	 */
	public Integer getMergecolumn() {
		return mergecolumn;
	}

	/**
	 * @param mergecolumn
	 *          the mergecolumn to set
	 */
	public void setMergecolumn(Integer mergecolumn) {
		this.mergecolumn = mergecolumn;
	}

	/**
	 * @return the acl
	 */
	public AclBean getAcl() {
		return acl;
	}

	/**
	 * @param acl
	 *          the acl to set
	 */
	public void setAcl(AclBean acl) {
		this.acl = acl;
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @return the userRights
	 */
	public List<UserApplRightBean> getUserRights() {
		return userRights;
	}
}
