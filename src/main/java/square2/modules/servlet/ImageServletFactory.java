package square2.modules.servlet;

/**
 * 
 * @author henkej
 *
 */
public interface ImageServletFactory {
	/**
	 * get bytes of image to be produced
	 * 
	 * @return the bytes
	 */
	public byte[] getBytes();

	/**
	 * set image id
	 * 
	 * @param id
	 *          the id
	 */
	public void setImageId(Integer id);
}
