package ship.jsf.components.selectFilterableMenu;

/**
 * 
 * @author henkej
 *
 */
public class KeyValueBeanWrapper implements BeanWrapperInterface {
	private final Integer key;
	private final String value;

	/**
	 * create a new KeyValueBeanWrapper
	 * 
	 * @param key
	 *          the key
	 * @param value
	 *          the value
	 */
	public KeyValueBeanWrapper(Integer key, String value) {
		this.key = key;
		this.value = value;
	}

	public Integer getKey() {
		return key;
	}

	public String getValue() {
		return value;
	}
}
