package ship.jsf.components.filteredSelect;

import java.util.*;

/**
 * 
 * @author henkej
 * 
 */
public interface ListFilterElem {
	/**
	 * get html representation of internal elements; read only
	 * 
	 * @return html
	 */
	public String getHtml();

	/**
	 * get list representation on internal elements; read only
	 * 
	 * @return list of strings
	 */
	public List<String> getList();

	/**
	 * checks if needle exists in internal values, ignoring case sensitive
	 * 
	 * @param needle
	 *          to be used
	 * 
	 * @return true if needle is found; false otherwise
	 */
	public Boolean containsIgnoreCase(String needle);

	/**
	 * get unique id of this list
	 * 
	 * @return id
	 */
	public Integer getId();

	/**
	 * define render result
	 * 
	 * @return true if html is to be rendered; getHtml is used
	 */
	public Boolean getRenderHtml();

	/**
	 * define render result
	 * 
	 * @return true if a data table is to be rendered; getList is used
	 */
	public Boolean getRenderDatatable();

	/**
	 * define render result
	 * 
	 * @return true if a ui repeat is to be rendered; getList is used
	 */
	public Boolean getRenderRepeat();
}
