package ship.jsf.components.filteredSelect;

import java.util.*;

/**
 * 
 * @author henkej
 * 
 */
public interface FilterInterface {
	/**
	 * filter unfilteredList due to internal filter
	 * 
	 * @param unfilteredList
	 *          list of elements
	 * @return filtered list
	 */
	public List<ListFilterElem> doFilter(List<ListFilterElem> unfilteredList);

	/**
	 * get filter options
	 * 
	 * @return filter
	 */
	public String getFilter();

	/**
	 * set filter options
	 * 
	 * @param filter
	 *          to be used
	 */
	public void setFilter(String filter);
}
