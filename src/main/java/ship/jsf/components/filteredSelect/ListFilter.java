package ship.jsf.components.filteredSelect;

import java.util.*;

/**
 * 
 * @author henkej
 * 
 */
public class ListFilter {
	private List<ListFilterElem> list;
	private FilterInterface filterInterface;

	/**
	 * create a new ListFilter
	 * 
	 * @param filterInterface
	 *          the interface
	 */
	public ListFilter(FilterInterface filterInterface) {
		this.filterInterface = filterInterface;
		list = new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{filterInterface=");
		buf.append(filterInterface);
		buf.append(", list=");
		buf.append(list);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * add elem to internal list
	 * 
	 * @param elem
	 *          element to be added
	 */
	public void addListElem(ListFilterElem elem) {
		list.add(elem);
	}

	/**
	 * add all elems to internal list
	 * 
	 * @param elems
	 *          elements to be added
	 */
	public void addListElems(List<ListFilterElem> elems) {
		list.addAll(elems);
	}

	/**
	 * get filtered list of internal elements (which can be lists again)
	 * 
	 * @return list
	 */
	public List<ListFilterElem> getList() {
		return filterInterface.doFilter(list);
	}

	/**
	 * set internal filter contents
	 * 
	 * @param filter
	 *          content
	 */
	public void setFilter(String filter) {
		filterInterface.setFilter(filter);
	}

	/**
	 * get internal filter content
	 * 
	 * @return filter content
	 */
	public String getFilter() {
		return filterInterface.getFilter();
	}
}
