package ship.jsf.components.dataTable;

/**
 * 
 * @author henkej
 *
 */
public abstract class FilterableBean {
	private Boolean display;

	/**
	 * set display of bean in filtered list
	 * 
	 * @param display
	 *          may be true or false
	 */
	public void setDisplay(boolean display) {
		this.display = display;
	}

	/**
	 * get status of display in filtered list
	 * 
	 * @return true or false
	 */
	public boolean getDisplay() {
		return display == null ? true : display;
	}
}
