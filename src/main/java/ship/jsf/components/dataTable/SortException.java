package ship.jsf.components.dataTable;

/**
 * 
 * @author henkej
 *
 */
public class SortException extends Exception {
	private static final long serialVersionUID = -5483365009450381583L;

	/**
	 * create a new SortException
	 * 
	 * @param message
	 *          the message
	 */
	public SortException(String message) {
		super(message);
	}
}
