package ship.commandline;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author henkej
 * 
 */
public class ConsoleCommand {
	/**
	 * the secret place holder
	 */
	public static final String SECRET = "##SECRET##";

	private String commandWithSecret;
	private String command;
	private File executeFolder;
	private List<String> arguments;

	/**
	 * Generate new console command. If a secret text is to be submitted (as a
	 * password or such a thing), command must contain the place holder for this.
	 * This place holder is defined in the class intern static final String SECRET.
	 * 
	 * @param command
	 *          to be generated; may contain SECRET
	 * @param secret
	 *          to replace SECRET in command String
	 * @param executeFolder
	 *          folder for command to be executed
	 */
	public ConsoleCommand(String command, String secret, File executeFolder) {
		this.command = command;
		this.commandWithSecret = secret == null ? command : command.replace(SECRET, secret);
		this.executeFolder = executeFolder;
		arguments = new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{command=");
		buf.append(command);
		buf.append(", executeFolder=");
		buf.append(executeFolder);
		buf.append(", arguments=");
		buf.append(arguments);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * Generate command string; <b>BEWARE!!!</b> Do never log this result due to
	 * decoded secret passages!
	 * 
	 * @return command string
	 */
	public String toCommandString() {
		StringBuilder buf = new StringBuilder(commandWithSecret);
		for (String argument : arguments) {
			buf.append(" ");
			buf.append(argument);
		}
		return buf.toString();
	}

	/**
	 * @return the command
	 */
	public String getCommand() {
		return command;
	}

	/**
	 * @param command
	 *          the command
	 */
	public void setCommand(String command) {
		this.command = command;
	}

	/**
	 * @return the arguments
	 */
	public List<String> getArguments() {
		return arguments;
	}

	/**
	 * @return the execute folder
	 */
	public File getExecuteFolder() {
		return executeFolder;
	}
}