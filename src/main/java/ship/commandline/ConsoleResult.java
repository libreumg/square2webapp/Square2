package ship.commandline;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 
 * @author henkej
 * 
 */
public class ConsoleResult {
	private ConsoleCommand consoleCommand;
	private List<String> resultLines;
	private List<String> errorLines;

	/**
	 * Capsulate console command execution result
	 * 
	 * @param consoleCommand
	 *          command that has been executed
	 * @param e
	 *          any ConsoleException that may have been thrown
	 */
	public ConsoleResult(ConsoleCommand consoleCommand, ConsoleException e) {
		this.consoleCommand = consoleCommand;
		this.resultLines = new ArrayList<>();
		this.errorLines = new ArrayList<>();
		errorLines.add(e.getMessage());
		errorLines.add(Arrays.toString(e.getStackTrace()));
	}

	/**
	 * Capsulate console command execution result
	 * 
	 * @param consoleCommand
	 *          command that has been executed
	 */
	public ConsoleResult(ConsoleCommand consoleCommand) {
		this.consoleCommand = consoleCommand;
		this.resultLines = new ArrayList<>();
		this.errorLines = new ArrayList<>();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder(this.getClass().getSimpleName());
		buf.append("@{consoleCommand=");
		buf.append(consoleCommand);
		buf.append(", resultLines=");
		buf.append(resultLines);
		buf.append(", errorLines=");
		buf.append(errorLines);
		buf.append("}");
		return buf.toString();
	}

	/**
	 * add result of execution to result set
	 * 
	 * @param consoleResult
	 *          the console result
	 */
	public void addResult(ConsoleResult consoleResult) {
		if (!this.consoleCommand.equals(consoleResult.getConsoleCommand())) {
			this.consoleCommand = consoleResult.getConsoleCommand();
		}
		this.resultLines.addAll(consoleResult.getResultLines());
		this.errorLines.addAll(consoleResult.getErrorLines());
	}

	/**
	 * @return the result lines
	 */
	public List<String> getResultLines() {
		return resultLines;
	}

	/**
	 * @return the error lines
	 */
	public List<String> getErrorLines() {
		return errorLines;
	}

	/**
	 * @return the console command
	 */
	public ConsoleCommand getConsoleCommand() {
		return consoleCommand;
	}

	/**
	 * @return true if there are errors
	 */
	public boolean hasErrors() {
		return !errorLines.isEmpty();
	}

	/**
	 * @return true if there are result lines
	 */
	public boolean hasOutput() {
		return !resultLines.isEmpty();
	}
}
