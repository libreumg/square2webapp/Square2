package ship.commandline.thread;

import ship.commandline.ConsoleCommand;
import ship.commandline.ConsoleException;
import ship.commandline.ConsoleResult;

/**
 * 
 * @author henkej
 *
 */
public class QuietThreadProcessor implements ThreadProcessor {
	private ConsoleCommand consoleCommand;

	/**
	 * @param consoleCommand
	 *          the command
	 */
	public QuietThreadProcessor(ConsoleCommand consoleCommand) {
		this.consoleCommand = consoleCommand;
	}

	public void executeAfterRun(ConsoleResult consoleResult) {
		// just be quiet; do nothing
	}

	public ConsoleResult getConsoleResult() {
		return new ConsoleResult(consoleCommand);
	}

	public ConsoleCommand executeBeforeRun() throws ConsoleException {
		return consoleCommand;
	}
}
