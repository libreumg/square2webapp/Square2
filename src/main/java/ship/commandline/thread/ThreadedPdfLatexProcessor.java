package ship.commandline.thread;

import java.io.File;

import ship.commandline.ConsoleCommand;
import ship.commandline.ConsoleException;
import ship.commandline.ConsoleResult;

/**
 * 
 * @author henkej
 *
 */
public class ThreadedPdfLatexProcessor implements ThreadProcessor {
	private enum Extension {
		PDF(".pdf"), //
		LOG(".log"), //
		AUX(".aux");

		private final String ext;

		Extension(String e) {
			this.ext = e;
		}

		public String getExt() {
			return ext;
		}
	}

	private final File texFile;
	private final ConsoleCommand consoleCommand;
	private ConsoleResult consoleResult;

	/**
	 * if the file doesn't exist in the execute-folder, use
	 * ThreadedPdfLatexProcessor(String, File)
	 * 
	 * @param fileInsideExecuteFolder
	 *          the file inside of the execute folder
	 */
	public ThreadedPdfLatexProcessor(File fileInsideExecuteFolder) {
		this(fileInsideExecuteFolder.getName(), new File(fileInsideExecuteFolder.getParent()), null, 1);
	}

	/**
	 * if the file doesn't exist in the execute-folder, use
	 * ThreadedPdfLatexProcessor(String, File)
	 * 
	 * @param fileInsideExecuteFolder
	 *          the file inside of the execute folder
	 * @param flags
	 *          the flags
	 * @param runMagnitude
	 *          the run magnitude
	 */
	public ThreadedPdfLatexProcessor(File fileInsideExecuteFolder, String flags, Integer runMagnitude) {
		this(fileInsideExecuteFolder.getName(), new File(fileInsideExecuteFolder.getParent()), flags, runMagnitude);
	}

	/**
	 * create a new latex processor thread
	 * 
	 * @param latexFileName
	 *          the latex file name
	 * @param executeFolder
	 *          the execute folder
	 * @param flags
	 *          the flags
	 * @param runMagnitude
	 *          the run magnitude
	 */
	public ThreadedPdfLatexProcessor(String latexFileName, File executeFolder, String flags, Integer runMagnitude) {
		if (flags == null) {
			flags = "";
		}
		StringBuilder buf = new StringBuilder();
		if (!latexFileName.equals("-version")) { // TODO: hack, until markdown is introduced
			String prefix = latexFileName.substring(0, latexFileName.indexOf(".tex"));
			buf.append("mv " + latexFileName + " rescue." + latexFileName + ";");
			buf.append("rm " + prefix + ".*" + ";");
			buf.append("mv rescue." + latexFileName + " " + latexFileName + ";");
		}
		for (int i = 1; i <= runMagnitude; i++) {
			buf.append("pdflatex -interaction=batchmode ");
			buf.append(flags).append(" ");
			buf.append(latexFileName);
			buf.append(";");
		}
		String commands = buf.toString();
		String command = commands.substring(0, commands.length() - 1); // remove trailing semicolon
		this.consoleCommand = new ConsoleCommand(command, null, executeFolder);
		this.texFile = new File(executeFolder, latexFileName);
	}

	/**
	 * @return the output file if exists, else null
	 */
	public File getLatexOutputPdf() {
		return getLatexFile(Extension.PDF);
	}

	/**
	 * @return the output file if exists, else null
	 */
	public File getLatexOutputAux() {
		return getLatexFile(Extension.AUX);
	}

	/**
	 * @return the output file if exists, else null
	 */
	public File getLatexOutputLog() {
		return getLatexFile(Extension.LOG);
	}

	private File getLatexFile(Extension ext) {
		File ret = null;
		if (consoleResult.getResultLines() != null) {
			// try to get file with the desired extension
			String name = texFile.getName();
			if (name.contains(".")) {
				name = name.substring(0, name.lastIndexOf('.'));
			}
			ret = new File(new File(texFile.getParent()), name + ext.getExt());
			if (!ret.exists()) {
				ret = null;
			}
		}
		return ret;
	}

	public void executeAfterRun(ConsoleResult consoleResult) {
		this.consoleResult = consoleResult;
	}

	public ConsoleCommand executeBeforeRun() throws ConsoleException {
		return consoleCommand;
	}

	public ConsoleResult getConsoleResult() {
		return consoleResult;
	}
}
