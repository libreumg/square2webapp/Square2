package ship.commandline.thread;

import ship.commandline.ConsoleController;
import ship.commandline.ConsoleException;
import ship.commandline.ConsoleResult;

/**
 * 
 * @author henkej
 *
 */
public class ThreadConsoleController extends ConsoleController implements Runnable {
	private ThreadProcessor threadProcessor;

	/**
	 * create new thread controller
	 * 
	 * @param shell
	 *          the shell
	 * @param threadProcessor
	 *          the processor
	 */
	public ThreadConsoleController(String shell, ThreadProcessor threadProcessor) {
		super(shell);
		this.threadProcessor = threadProcessor;
	}

	public void run() {
		try {
			threadProcessor.executeAfterRun(super.executeOnShell(threadProcessor.executeBeforeRun()));
		} catch (ConsoleException e) {
			ConsoleResult consoleResult = threadProcessor.getConsoleResult();
			consoleResult.getErrorLines().add(e.getMessage());
			threadProcessor.executeAfterRun(consoleResult);
		}
	}
}
