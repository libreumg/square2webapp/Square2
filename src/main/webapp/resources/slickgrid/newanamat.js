function newanamat(inputDef, configuration) {
	function AnaMat(inputDef, configuration) {
		var self = this;

		self.require = function(script) { // https://stackoverflow.com/a/7352694
		$.ajax({
			url: script,
			dataType: "script",
			async: false,           // <-- This is the key
			success: function () {
				// all good...
			},
			error: function () {
				throw new Error("Could not load script " + script);
			}
		});
	  }
	   if (!(typeof zip === "object")) require("zip.js") ;

		self.zipBlob = function(filename, blob, callback) {
		  // use a zip.BlobWriter object to write zipped data into a Blob object
		  zip.createWriter(new zip.BlobWriter("application/zip"), function(zipWriter) {
			// use a BlobReader object to read the data stored into blob variable
			zipWriter.add(filename, new zip.BlobReader(blob), function() {
			  // close the writer and calls callback function
			  zipWriter.close(callback);
			});
		  }, onerror);
		}

		self.unzipBlob = function(blob, callback) {
		  // use a zip.BlobReader object to read zipped data stored into blob variable
		  zip.createReader(new zip.BlobReader(blob), function(zipReader) {
			// get entries from the zip file
			zipReader.getEntries(function(entries) {
			  // get data from the first file
			  entries[0].getData(new zip.BlobWriter("text/plain"), function(data) {
				// close the reader and calls callback function with uncompressed data as parameter
				zipReader.close();
				callback(data);
			  });
			});
		  }, onerror);
		}
	  // zip.workerScriptsPath = "/lib/";

	  var bootstrap_enabled = (typeof $().modal == 'function'); // https://stackoverflow.com/a/14768682

	  // ------------------------------------ FUNCTION STARTS HERE ---------------------------- //
	  var initCallback = configuration["initCallback"];

	  if (typeof(initCallback) == "function") {
		o = initCallback(configuration);
		if (typeof(o) == "object" && o.initCallback != null) {
			configuration = o
		}
	  }

	  var divId = configuration["divId"]
	  var fixCol = configuration["fixCol"]
	  var varColWidth = configuration["varColWidth"]
	  var searchInputId = configuration["searchInputId"]
	  var colFilterInputId = configuration["colFilterInputId"]
	  var localStorageDiv = configuration["localStorageDiv"]

	  var ajaxEndpoint = configuration["ajaxEndpoint"]
	  var matrixData = new libs.MatrixObject(inputDef, null, ajaxEndpoint);

	  self.filterHint = function() {

	  }
	  
	  self.colFilterHint = function() {

	  }

	  var grid;

	  var colFilter = "" ;

	  var lockedAnamat = false
	  var searchString = ""

	  if (fixCol == undefined)
		fixCol = true;

	  if (varColWidth == undefined)
		varColWidth = 120

	  var controller = []
	  self.removeAllTooltips = function() {
		var tooltips = $(".ui-tooltip")
		tooltips.remove()
		var tooltips = $(".tooltip")
		tooltips.remove()
	  }
	  self.renderCheckmark = function(cellNode, row, dataContext, columnDef) {
		var v = dataContext.var_id;
		var fkt = columnDef.name;
		var c = cellNode;
		var value = dataContext[columnDef.name]
		c = $(c)
		if (value == null) {
		  c.addClass("locked")
		  c.attr("title", "Locked " + fkt + ":" + v)
		} else {
		  c.removeClass("locked")
		  c.attr("title", fkt + ":" + v)
		}
		c.tooltip({
		trigger : 'hover' // https://stackoverflow.com/a/33585981
		});
	  }
	  function renderTitleCol(cellNode, row, dataContext, columnDef) {
		var c = cellNode;
		c = $(c)
		var htmlCode = dataContext.var_id;
		var vid = dataContext.var_id
		var v_name = vid
		if (matrixData.getMatrixData().rowDef != undefined)
		  if (matrixData.getMatrixData().rowDef[vid] != null)
			if (matrixData.getMatrixData().rowDef[vid].name != null)
			  v_name = matrixData.getMatrixData().rowDef[vid].name
		var v_tooltip = v_name
		if (v_tooltip != vid) {
			v_tooltip = v_tooltip + " (<span style=\"font-family: monospace;\">" + vid + "</span>)"
		} else {
			v_tooltip = "<span style=\"font-family: monospace;\">" + v_tooltip + "</span>"
		}
		var varname = $("<span style='height:20px; float: left; word-space: nowrap; width:" +
		  (columnDef.width - 40 - 5) +
		  "px; overflow: hidden;' class='slick-varname'>" + v_name + "</span>")
		c.html("")
		if (matrixData.getMatrixData().rowDef != undefined)
		  if (matrixData.getMatrixData().rowDef[vid] != null)
			if (matrixData.getMatrixData().rowDef[vid].description != null)
			  v_tooltip = "<h5>" + matrixData.getMatrixData().rowDef[vid].description + "</h5>" + v_tooltip
		c.attr("title", v_tooltip)
		c.tooltip({
			trigger : 'hover', // https://stackoverflow.com/a/33585981
			content: function() { // https://stackoverflow.com/a/13068935
				return $(this).attr('title');
			}
		})
		var unselectButton = 
		  $("<span style='height:20px; width:20px;margin:0;float:right;' class='slick-header-button select-none' title='Select none'></span>")
		var selectButton = 
		  $("<span style='height:20px; width:20px;margin:0;float:right;' class='slick-header-button select-all' title='Select all'></span>")
		c.append(unselectButton)
		c.append(selectButton)
		c.append(varname);
	  //  selectButton.tooltip()
	  //  unselectButton.tooltip()
		selectButton.on("click", function() {
		  self.setRow(vid, true);
		})
		unselectButton.on("click", function() {
		  self.setRow(vid, false);
		})
	  }
	  function CheckmarkFormatter(row, cell, value, columnDef, dataContext) {
		var v = dataContext.var_id;
		var fkt = columnDef.name;
		if (value == null) {
		  return('<span style="font-size: 8pt;">&#x1f512;</span>');
		} else {
		  return value ? '<span style="font-size: 8pt;">&#10003;</span>' : "";
		}
	  }
	  $.extend(true, window, {
		"Slick": {
		  "Formatters": {
			"Checkmark": CheckmarkFormatter
		 }
		}
	  });

	  function CheckboxEditor(args) {
		var scope = this;
		var value = null;

		var autoCommit = function() {
			if (!self.grid.getEditorLock().commitCurrentEdit()) {
				  return;
			}
		}
		
		this.init = function () {
			if (lockedAnamat) return;
			window.setTimeout(autoCommit, 1);
		};

		this.destroy = function () {
		};

		this.focus = function () {
		};

		this.loadValue = function (item) {
		  if (item[args.column.field] == null) {
			value = null;
		  } else {
			if (lockedAnamat) return;
			value = !item[args.column.field]
		  }
		};

		this.preClick = function () {
			if (lockedAnamat) return;
		};

		this.serializeValue = function () {
		  return value;
		};

		this.applyValue = function (item, state) {
			if (matrixData.getMatrixData().data_records[args.item.var_id] == null) {
				return ;
			}
			if (matrixData.getMatrixData().data_records[args.item.var_id][args.column.field] == null) {
				return ;
			}
			if (matrixData.getMatrixData().data_records[args.item.var_id][args.column.field].selected == null) {
				return ;
			}
			matrixData.setArguments(args.item.var_id, args.column.field, {selected: !matrixData.getMatrixData().data_records[args.item.var_id][args.column.field].selected}, 
			function(x) { 
				item[args.column.field] = state;
				$(self.grid.invalidate)
				self.removeAllTooltips()
			}, self.ajaxAlert)
		};

		this.isValueChanged = function () {
		  return !lockedAnamat
		};

		this.validate = function () {
		  return {
			valid: !lockedAnamat,
			msg: null
		  };
		};

		this.init();
	  }

	  $.extend(true, window, {
		"Slick": {
		  "Editors": {
			"Checkbox": CheckboxEditor
		  }
		}
	  });

	  var data = [];
	  var columns = [
		{
		  id: "var_id", 
		  name: "Variable", 
		  field: "var_id", 
		  behavior: "select",
	//      cssClass: "cell-selection",
		  cannotTriggerInsert: true,
		  resizable: false,
		  unselectable: true,
		  width: varColWidth, 
		  cssClass: "cell-title", 
		  headerCssClass: "cell-title cell-title-header",
		  rerenderOnResize: true,
		  asyncPostRender: renderTitleCol
		}
	  ];

	  self.initMatrixCols = function() {
		var calls = matrixData.getOrderedColnames() ;
		for (var calll_index in calls) {
		  var call = calls[calll_index];
		  var col = matrixData.getMatrixData().colDef[call];
		  var name = call;
		  var id = call;
		  var description = call;
		  if (col.id != null) {
			id = col.id;
		  }
		  if (col.description != null) {
			description = col.description;
		  }
		  columns.push(
			{
			  id: id, 
			  field: id, 
			  name: name, 
			  toolTip: description,
			  width: 20, 
			  minWidth: 20, 
			  maxWidth: 20, 
			  cssClass: "cell-function", 
			  formatter: Slick.Formatters.Checkmark, 
			  editor: Slick.Editors.Checkbox,
			  asyncPostRender: self.renderCheckmark,
			  header: {
				buttons: [
				  {
					cssClass: "slick-header-button-anamat select-all",
					command: "setCol",
					tooltip: "Select all (except variables filtered out)"
				  },
				  {
					cssClass: "slick-header-button-anamat select-none",
					command: "unsetCol",
					tooltip: "Select none (except variables filtered out)"
				  },
				  {
					cssClass: "slick-header-button-anamat pencil",
					command: "configCol",
					tooltip: "Configure analyses"
				  },
				  {
					cssClass: "slick-header-button-anamat help",
					command: "helpCol",
					tooltip: "About this column"
				  }
				]
			  }
			}
		  )
		}
		// https://github.com/twbs/bootstrap/issues/15359 -- data-selector="true"
	  }

	  self.initMatrixCols()

	  var options = {
		editable: true,
		enableAddRow: false,
		enableCellNavigation: true,
		asyncEditorLoading: false,
		enableAsyncPostRender: true,
		enableColumnReorder: false,
		autoEdit: true,
		explicitInitialization: true,
		frozenColumn: fixCol ? 0 : -1
	  };

	  self.adjustHeight = function() {
		var initHeight = configuration["initHeight"]
		if (initHeight != undefined) {
		  if (initHeight.startsWith("window-")) {
			var remainder = initHeight.substr("window-".length)
			initHeight = ($( window ).height()-remainder) + "px"
		  } else if (initHeight.startsWith("screen-")) {
			var remainder = initHeight.substr("screen-".length)
			initHeight = (screen.height-remainder) + "px"
		  }
		  $("#" + divId).css("height", initHeight);
		}
	  }

	  $(function () {

		self.adjustHeight()

		self.myFilter = function(item, args) {
		  if (args.searchString == "")
			return(true);

		var vid = item.var_id
		var v_name = vid
		var v_tooltip = ""
		if (matrixData.getMatrixData().rowDef != undefined)
		  if (matrixData.getMatrixData().rowDef[vid] != null)
			if (matrixData.getMatrixData().rowDef[vid].name != null)
			  v_name = matrixData.getMatrixData().rowDef[vid].name
		var v_tooltip = v_name
		if (matrixData.getMatrixData().rowDef != undefined)
		  if (matrixData.getMatrixData().rowDef[vid] != null)
			if (matrixData.getMatrixData().rowDef[vid].description != null)
			  v_tooltip = matrixData.getMatrixData().rowDef[vid].description

		  if  (
				vid.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1 &&
				v_tooltip.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1 &&
				v_name.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1
			  ) {
			return false;
		  }
		  return true;
		}

		self.dataView = new Slick.Data.DataView()

		window.setTimeout(function(divId) {
				$("#" + divId).css("position", "relative")
			},
			20,
			divId
		)

		self.grid = new Slick.Grid("#" + divId, self.dataView, columns, options);

		self.updateFilter = function() {
			self.dataView.setFilterArgs({
			  searchString: searchString
			});
			self.dataView.refresh();
			self.grid.invalidate();
		}

		if ($("#" + localStorageDiv).length == 1) {
		  /////////////// local storage
		  var lsd = $("#" + localStorageDiv);
	//      lsd.css("background-color", "skyblue");
		  lsd.append("<strong>Load from local file</strong>")
		  var load = $("<input type=\"file\" accept=\"text/plain, application/json\">");
		  var save = $("<button>Save to local file</button>");
		  var saveZip = $("<button>Save to local ZIP file</button>");
		  lsd.append(load).append(save).append(saveZip);

			// see https://stackoverflow.com/a/21016088
			// and https://stackoverflow.com/a/42088825
			// Thanks to Useless Code and Balazs Vago
			self.loadFile = function() {
				var fr = new FileReader();
				fr.onload = function(e) {
						showDataFile(e, this);
					};
				if (this.files.length > 0)
				  fr.readAsText(this.files[0]);
			}

			load.change(self.loadFile);

			self.showDataFile = function(e, o) {
				load.val("")
				setData(e.target.result);
			}

			self.updateForm = function(fileInput, onLoadend, onError, onProgress) {
				throw "Not yet implemented in favour of Ajax requests" ;
			  var zipFile = null,
			  makeZipFile = function (text) {
				// create the blob object storing the data to compress
				var blob = new Blob([ text ], {
				  type : "application/json"
				});
				// creates a zip storing the file "analysis_matrix.json" with blob as data
				// the zip will be stored into a Blob object (zippedBlob)
				zipBlob("analysis_matrix.json", blob, function(zippedBlob) {
					var reader = new FileReader();
					 reader.readAsDataURL(zippedBlob); 
					 reader.onloadend = function() {
						var inputEl = $(fileInput);
						inputEl.val(blob);
						console.log(blob);
						console.log(inputEl);
						if (typeof onLoadend === "function") onLoadend(blob);
					 }
					 if (typeof onError === "function") 
						 reader.onerror = onError;
					 if (typeof onProgress === "function") 
						 reader.onprogress = onProgress;
				})
			  };
			  makeZipFile(getData(false));
			}
	/*        $(testUpload).hide();
			$(testUploadBtn).bind("click", function() {
				updateForm("testUpload", 
				function(blob) { console.log("loaded") },
				console.log,
				console.log
				)
			})
	*/
			self.makeDownloadZipped = function() {
			  var zipFile = null,
			  makeZipFile = function (text, link) {
				// create the blob object storing the data to compress
				var blob = new Blob([ text ], {
				  type : "application/json"
				});
				// creates a zip storing the file "analysis_matrix.json" with blob as data
				// the zip will be stored into a Blob object (zippedBlob)
				zipBlob("analysis_matrix.json", blob, function(zippedBlob) {
					var reader = new FileReader();
					 reader.readAsDataURL(zippedBlob); 
					 reader.onloadend = function() {
						link.href = reader.result;
						document.body.appendChild(link);
						// wait for the link to be added to the document
						window.requestAnimationFrame(function () {
						  var event = new MouseEvent('click');
						  link.dispatchEvent(event);
						  document.body.removeChild(link);
						});
					 }
				})
			  };

			  var create = $(saveZip)[0];

			  create.addEventListener('click', function () {
				var link = document.createElement('a');
				link.setAttribute('download', 'analysis_matrix.zip');
				makeZipFile(getData(false), link);
			  }, false);
			}

			self.makeDownload = function() {
			  var textFile = null,
			  makeTextFile = function (text) {
				var data = new Blob([text], {type: 'application/json'});

				// If we are replacing a previously generated file we need to
				// manually revoke the object URL to avoid memory leaks.
				if (textFile !== null) {
				  window.URL.revokeObjectURL(textFile);
				}

				textFile = window.URL.createObjectURL(data);

				// returns a URL you can use as a href
				return textFile;
			  };

			  var create = $(save)[0];

			  create.addEventListener('click', function () {
				var link = document.createElement('a');
				link.setAttribute('download', 'analysis_matrix.json');
				link.href = makeTextFile(getData(false));
				document.body.appendChild(link);

				// wait for the link to be added to the document
				window.requestAnimationFrame(function () {
				  var event = new MouseEvent('click');
				  link.dispatchEvent(event);
				  document.body.removeChild(link);
				});

			  }, false);
			}

			$(self.makeDownload);
			$(self.makeDownloadZipped);

		} // localStorageDiv

		//// local storage end
        
        // colFilterInputId colFilterInputId colFilterInputId

		if ($("#" + colFilterInputId).length == 1) {
		  $("#" + colFilterInputId).prop("title", "Filter for columns. Not that 'select all' and 'select column' will only apply for rows not filtered out.")
		  $("#" + colFilterInputId).tooltip({
			trigger : 'hover' // https://stackoverflow.com/a/33585981
		})
		  $("#" + colFilterInputId).keyup(function (e) {
			  Slick.GlobalEditorLock.cancelCurrentEdit();
			  // clear on Esc
			  if (e.which == 27) {
				this.value = "";
			  }
			  self.setColFilter(this.value);
			  self.updateMatrixFull()
		  });
		  var old_colorColFilter = $("#" + colFilterInputId).css("background-color")
		  self.colFilterHint = function() {
			var sb = $("#" + colFilterInputId)
			sb.css("background-color", "yellow")
			setTimeout(function() {
			  if (bootstrap_enabled) {
				sb.tooltip('show') 
			  setTimeout(function() {
				  sb.css(("background-color"), old_colorColFilter)
				sb.tooltip('hide') 
			  }, 2000)
			  } else {
				sb.tooltip('open') 
				setTimeout(function() {
				  sb.css(("background-color"), old_colorColFilter)
				  sb.tooltip('close') 
				}, 2000)
			  }
			  $(function() {
				for(var i = 1; i <= 2; i++) sb.fadeTo('slow', 0.1).fadeTo('fast', 1.0);
			  })
			}, 1)
		  }
		}

        //// colFilterInputId colFilterInputId colFilterInputId

		if ($("#" + searchInputId).length == 1) {
		  $("#" + searchInputId).prop("title", "Filter for rows. Not that 'select all' and 'select column' will only apply for rows not filtered out.")
		  $("#" + searchInputId).tooltip({
			trigger : 'hover' // https://stackoverflow.com/a/33585981
		})
		  $("#" + searchInputId).keyup(function (e) {
			  Slick.GlobalEditorLock.cancelCurrentEdit();
			  // clear on Esc
			  if (e.which == 27) {
				this.value = "";
			  }
			  searchString = this.value;
			  self.updateFilter();
		  });
		  var old_colorRowFilter = $("#" + searchInputId).css("background-color")
		  self.filterHint = function() {
			var sb = $("#" + searchInputId)
			sb.css("background-color", "yellow")
			setTimeout(function() {
			  if (bootstrap_enabled) {
				sb.tooltip('show') 
			  setTimeout(function() {
				  sb.css(("background-color"), old_colorRowFilter)
				sb.tooltip('hide') 
			  }, 2000)
			  } else {
				sb.tooltip('open') 
				setTimeout(function() {
				  sb.css(("background-color"), old_colorRowFilter)
				  sb.tooltip('close') 
				}, 2000)
			  }
			  $(function() {
				for(var i = 1; i <= 2; i++) sb.fadeTo('slow', 0.1).fadeTo('fast', 1.0);
			  })
			}, 1)
		  }
		}
		self.dataView.beginUpdate();
		self.dataView.setItems(matrixData.getGridData(), "var_id");
		self.dataView.setFilterArgs({
		  searchString: searchString
		});
		self.dataView.setFilter(self.myFilter);
		self.dataView.endUpdate();

		self.grid.init()
		  $(function() {
			setTimeout(self.grid.resizeCanvas, 200);
			var resizeDelay = 200;
			var resizeTimeout = null;
			$(window).resize(function() {
			  if (event.srcElement instanceof Window) { // jQuery dialog boxes also trigger strange window-resize events.
						   if (resizeTimeout == null)
								   resizeTimeout = setTimeout(function() {
									 resizeTimeout = null;
									 self.adjustHeight()
									 self.grid.resizeCanvas();
								   }, resizeDelay);
						   }
			  });
		  });

		self.grid.onContextMenu.subscribe(function (e) {
		  e.preventDefault();
		  var cell = self.grid.getCellFromEvent(e);
		  if (cell.cell == 0) return ;
		  console.log(cell);
		  var columnAliasName = self.grid.getColumns()[cell.cell].name;
		  console.log(columnAliasName);
		  var var_id = self.dataView.getFilteredItems()[cell.row].var_id;
		  var dr = matrixData.getArguments(var_id, null) ;
		  console.log(var_id);
		  var cd = matrixData.getMatrixData().colDef[matrixData.getColname(cell.cell - 1)];
		  if (cd.params != null) console.log(cd.params);
		  var rd = matrixData.getMatrixData().rowDef[var_id];
		  console.log(rd.variable_attributes);
		  console.log(dr[var_id][columnAliasName]);
		  if (dr[var_id][columnAliasName] == null) return ;
		  // console.log(e);
		  var onOk = function(dlg) {
			var modifiedData = {};
			modifiedData[var_id] = {};
			modifiedData[var_id][columnAliasName] = {};
			modifiedData[var_id][columnAliasName].params = {};
			var params = matrixData.getMatrixData().colDef[columnAliasName].params;
			var args_in_order = Object.keys(params).sort((a,b)=>params[a].fi.order - params[b].fi.order);
			for (var i = 0; i < args_in_order.length; i++) {
				var arg = args_in_order[i];
				var arg_props = params[arg];
				if (!arg_props.fixed) {
					if (Object.keys(dlg.widgets).includes(arg) ) {
						if (dlg.widgets["class:" + arg] != "locked") {
							var value ;
							if (dlg.widgets["class:" + arg] == "checkbox") {
								value = dlg.widgets[arg].prop("checked");
							}  else if (dlg.widgets["class:" + arg] == "number") {
								value = parseFloat(dlg.widgets[arg].val());
							}  else if (dlg.widgets["class:" + arg] == "datePicker") {
								value = new Date(dlg.widgets[arg].val());
							} else {
								value = dlg.widgets[arg].val();
							}
							var use_meta = null;
							if (Object.keys(dlg.widgets).includes(arg + "_use_meta")) {
								use_meta = dlg.widgets[arg + "_use_meta"].prop("checked");
							}
							console.log(matrixData.getArguments(var_id, columnAliasName));
							modifiedData[var_id][columnAliasName].params[arg] = {};
							modifiedData[var_id][columnAliasName].params[arg].value = value;
							if (use_meta != null) {
								modifiedData[var_id][columnAliasName].params[arg].refers_metadata = use_meta;
							}
						}
					}
				}
			}
//			modifiedData[var_id][columnAliasName].selected = 
//			  matrixData.getArguments(var_id, columnAliasName)[var_id][columnAliasName].selected;
			matrixData.setArguments(var_id, columnAliasName, modifiedData[var_id][columnAliasName], function(req) {
					dlg.dialog.dialog('close');
			}, self.ajaxAlert)
			return false;
		  }
		  if (matrixData.matrixData.data_records[var_id][columnAliasName].params == null)
			  return; // locked field
		  try {
			  var anaConfig  = new libs.AnaConfig(columnAliasName, var_id, matrixData);
			  anaConfig.show(onOk);
			  anaConfig = undefined;
		  } catch (e) {
			libs.Utils.showAlert(e, "danger");
		  }
		});

		var headerButtonsPlugin = new Slick.Plugins.HeaderButtons();

		headerButtonsPlugin.onCommand.subscribe(function(e, args) {
		  var column = args.column;
		  var columnAliasName = column.name;
		  var button = args.button;
		  var command = args.command;
		  if (command == "setCol") {
			self.setCol(column.id, true)
		  } else if (command == "unsetCol") {
			self.setCol(column.id, false)
		  } else if (command == "configCol") {
			  var onOk = function(dlg) {
				var modifiedData = {};
				modifiedData = {};
				modifiedData[columnAliasName] = {};
				modifiedData[columnAliasName].params = {};
				var params = matrixData.getMatrixData().colDef[columnAliasName].params;
				var args_in_order = Object.keys(params).sort((a,b)=>params[a].fi.order - params[b].fi.order);
				for (var i = 0; i < args_in_order.length; i++) {
					var arg = args_in_order[i];
					var arg_props = params[arg];
					if (!arg_props.fixed) {
						if (Object.keys(dlg.widgets).includes(arg) ) {
							if (dlg.widgets["class:" + arg] != "locked") {
								var value ;
								if (dlg.widgets["class:" + arg] == "checkbox") {
									value = dlg.widgets[arg].prop("checked");
								}  else if (dlg.widgets["class:" + arg] == "number") {
									value = parseFloat(dlg.widgets[arg].val());
								}  else if (dlg.widgets["class:" + arg] == "datePicker") {
									value = new Date(dlg.widgets[arg].val());
								} else {
									value = dlg.widgets[arg].val();
								}
								var use_meta = null;
								if (Object.keys(dlg.widgets).includes(arg + "_use_meta")) {
									use_meta = dlg.widgets[arg + "_use_meta"].prop("checked");
								}
								console.log(matrixData.getArguments(null, columnAliasName));
								modifiedData[columnAliasName].params[arg] = {};
								modifiedData[columnAliasName].params[arg].value = value;
								if (use_meta != null) {
									modifiedData[columnAliasName].params[arg].refers_metadata = use_meta;
								}
							}
						}
					}
				}
// not needed any more				modifiedData[columnAliasName].selected = true; // to ease server side parsing.
				matrixData.setArguments(null, columnAliasName, modifiedData[columnAliasName], function(req) {
						dlg.dialog.dialog('close'); //TODO: Add a second button that loops over visible checkboxes and sends only those
				}, self.ajaxAlert)
				return false;
			  }
			try {
				var anaConfig  = new libs.AnaConfig(column.id, null, matrixData);
				anaConfig.show(onOk);
				anaConfig = undefined;
			} catch (e) {
				libs.Utils.showAlert(e, "danger");
			}
		  } else if (command == "helpCol") {
			var description = matrixData.getMatrixData().colDef[column.id].description ;
			if (description == null)
				description = "No further explanation yet. Sorry." ;
			libs.Utils.showAlert("<h3>" + column.id + "</h3>"+ description, "info");
		  }
		});

		self.grid.registerPlugin(headerButtonsPlugin);

		self.grid.setSelectionModel(new Slick.CellSelectionModel());

		$(".slick-header-button").tooltip({
		  tooltipClass: "header-button-tooltip-styling",
		  trigger: "hover" // https://stackoverflow.com/a/33585981
		});

		// https://stackoverflow.com/a/34670530
		var interval;
		$(document).on('mousemove keyup keypress', function() {
			clearTimeout(interval);
			// clear it as soon as any event occurs
			// do any process and then call the function again
			self.settimeout(); // call it again
		})

		self.settimeout = function() {
		  interval=setTimeout(self.removeAllTooltips, 4000);
		}

		$(window).click(self.removeAllTooltips);

		$(".slick-header-column").tooltip({
		  cssClass: "header-tooltip-styling",
		  trigger: "hover" // https://stackoverflow.com/a/33585981
		})

		$(function() {
		  self.grid.autosizeColumns()
		})

	  })

	  self.decompress_compressed_col = function(bytes) {
			var i;
			var bits = "";
			var hex;
			for (i = 0; i < bytes.length; i++) {
				hex = bytes.charAt(i);
				var dec = parseInt(hex, 16);
				bits += ("0000" + dec.toString(2)).slice(-4);
			}
			return(bits)
	  }

	  self.get_compressed_data = function() {
		var data = matrixData.getMatrixData().data_records;
		var fields = [];
		for (var fk = 0; fk < columns.length; fk ++) {
		  fields.push(self.grid.getColumns()[fk]["field"]);
		}
		var l = data.length
		var res = {}
		for (var c = 1; c < fields.length; c++) {
		  var s = ""
		  var t = ""
		  for (var r = 0; r < l; r++) {
			if (data[r][fields[c]] != null)
			  s += data[r][fields[c]] ? "1" : "0"
			 else
			  s += "0"
			if (r % 4 == 3) {
			  t = t + parseInt(s, 2).toString(16)
			  s = ""
			}
		  }
		  res[fields[c]] = t
		  if (s != "")
			res[fields[c]] += parseInt((s + "0000").slice(0, 4), 2).toString(16)
		}
		var vars = []
		for (var r = 0; r < l; r++) {
		  vars.push(data[r][fields[0]])
		}
		return(JSON.stringify({"res": res, "fields": fields, "vars": vars}))
	  }

	  self.getData = function() {
		return(JSON.stringify(matrixData.getArguments(null, null))); // TODO: This is too large for bein uploaded!!
	  }

	  self.updateMatrixFull = function() {
		self.dataView.beginUpdate();
		self.dataView.setItems(matrixData.getGridData(), "var_id");
		self.dataView.setFilterArgs({
		  searchString: searchString
		});
		self.dataView.setFilter(self.myFilter);
		self.dataView.endUpdate();

		$(self.grid.invalidate);
	  }

	  self.setData = function(new_data) {
		if (lockedAnamat) return;
		matrixData.setArguments(null, null, new_data, self.updateMatrixFull, self.ajaxAlert)
	  }

      // TODO: Convert to lower ECMAscript standard w/o need of fully supported array and mapping, or compile this class too.
	  self.setRow = function(r, v) {
		if (lockedAnamat) return; 
		if (self.colFilter != null && typeof self.colFilter == "string" && self.colFilter != "") {
    		matrixData.setArguments([r], self.getFilteredColumns().map(x => x.name).splice(1), {selected: v}, self.updateMatrixFull, self.ajaxAlert)
			self.colFilterHint();
		} else {
			matrixData.setArguments([r], null, {selected: v}, self.updateMatrixFull, self.ajaxAlert)
		}
	  }

	  self.setCol = function(c, v) {
		if (lockedAnamat) return;
		if (self.dataView.getFilteredItems().length != self.grid.getData().getItems().length) {
    		matrixData.setArguments(self.dataView.getFilteredItems().map(x => x.var_id), [c], {selected: v}, self.updateMatrixFull, self.ajaxAlert)
		    self.filterHint()
		} else {
    		matrixData.setArguments(null, [c], {selected: v}, self.updateMatrixFull, self.ajaxAlert)
		}
	  }

	  self.setAll = function(v) {
		if (lockedAnamat) return;
		if     ((self.colFilter == null || typeof self.colFilter != "string" || self.colFilter == "")
		    && (self.dataView.getFilteredItems().length == self.grid.getData().getItems().length)) {
    		matrixData.setArguments(null, null, {selected: v}, self.updateMatrixFull, self.ajaxAlert)
    		return ;
		}
		if     ((self.colFilter == null || typeof self.colFilter != "string" || self.colFilter == "")
		    && (self.dataView.getFilteredItems().length != self.grid.getData().getItems().length)) {
    		matrixData.setArguments(self.dataView.getFilteredItems().map(x => x.var_id), null, {selected: v}, self.updateMatrixFull, self.ajaxAlert)
    		return ;
		}
		if     ((self.colFilter != null && typeof self.colFilter == "string" || self.colFilter != "")
		    && (self.dataView.getFilteredItems().length == self.grid.getData().getItems().length)) {
    		matrixData.setArguments(null, self.getFilteredColumns().map(x => x.name).splice(1), {selected: v}, self.updateMatrixFull, self.ajaxAlert)
    		return ;
		}
		matrixData.setArguments(self.dataView.getFilteredItems().map(x => x.var_id), self.getFilteredColumns().map(x => x.name).splice(1), {selected: v}, self.updateMatrixFull, self.ajaxAlert)
		if (self.colFilter != null && typeof self.colFilter == "string" && self.colFilter != "") {
			self.colFilterHint();
		}
		if (self.dataView.getFilteredItems().length != self.grid.getData().getItems().length) {
		  self.filterHint()
		}
	  }

	  self.selectAll = function() {
		if (lockedAnamat) return;
		self.setAll(true);
	  }

	  self.deSelectAll = function() {
		if (lockedAnamat) return;
		self.setAll(false);
	  }

	  self.lock = function() { // https://stackoverflow.com/a/14461824
		lockedAnamat = true;
		$("#" + divId).append('<div id="lock4' + divId + '" title="The analysis matrix is locked." style="position: absolute;top:0;left:0;width: 100%;height:100%;z-index:2;opacity:0.4;filter: alpha(opacity = 50); cursor: not-allowed;"></div>');
		$("#" + divId).fadeTo('slow',.6);
		$($("#lock4" + divId).tooltip({
		  tooltipClass: "lock-tooltip-styling",
		  trigger: "hover" // https://stackoverflow.com/a/33585981
		}));
		$("#" + localStorageDiv + " input").prop( "disabled", true );
	  }

	  self.unlock = function() {
		lockedAnamat = false;
		$("#" + divId).fadeTo('slow',1.0);
		$("#lock4" + divId).remove();
		$("#" + localStorageDiv + " input").prop( "disabled", false );
	  }


	  self.myFilter = function(item, args) { // TODO: now it became possible to filter for columns
		if (args.searchString == "")
		  return(true);

		var vid = item.var_id
		var v_name = vid
		var v_tooltip = ""
		if (matrixData.getMatrixData().rowDef != undefined)
		  if (matrixData.getMatrixData().rowDef[vid] != null)
			if (matrixData.getMatrixData().rowDef[vid].name != null)
			  v_name = matrixData.getMatrixData().rowDef[vid].name
		var v_tooltip = v_name
		if (matrixData.getMatrixData().rowDef != undefined)
		  if (matrixData.getMatrixData().rowDef[vid] != null)
			if (matrixData.getMatrixData().rowDef[vid].description != null)
			  v_tooltip = matrixData.getMatrixData().rowDef[vid].description

		  if  (
				vid.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1 &&
				v_tooltip.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1 &&
				v_name.toLowerCase().indexOf(args.searchString.toLowerCase()) == -1
			  ) {
			return false;
		  }
		  return true;
	  }

	  self.ajaxAlert = function(x) {
		if (x.state() == "rejected" && (x.responseText == "" || x.responseText == undefined))
			libs.Utils.showAlert("Rejected", "danger")
		else
			libs.Utils.showAlert(x.responseText, "danger");
	  }
	  
	  self.getColumns = function() {
		return columns;
	  }

	  self.resetColFilter = function() {
		self.colFilter = "";
		self.grid.setColumns(columns);
	  }

	  self.getFilteredColumns = function() {
		return $.grep(columns, function(x, i) {
			if (i == 0) return true ; // never filter out the title column
			if (self.colFilter == null)
			    return true; // no filter set.
			return x.id.toLowerCase().indexOf(self.colFilter.toLowerCase()) > -1 ||
				   x.name.toLowerCase().indexOf(self.colFilter.toLowerCase()) > -1 ||
				   x.toolTip.toLowerCase().indexOf(self.colFilter.toLowerCase()) > -1 ;
		});
	  }

	  self.setColFilter = function(filter) {
		if (typeof filter != "string") {
			console.log("column filter must be a string. ignore this.")
		}
		self.colFilter = filter;
		self.grid.setColumns(this.getFilteredColumns());
	  }

	  controller.getData = self.getData ;
	  controller.setData = self.setData ;
	  controller.selectAll = self.selectAll;
	  controller.deSelectAll = self.deSelectAll;
	  controller.lock = self.lock;
	  controller.unlock = self.unlock;
	  controller.setColFilter = self.setColFilter;
	  controller.resetColFilter = self.resetColFilter;

	  controller.matrixData = self.matrixData; // TODO : remove me -- for testing only
	  controller.updateMatrixFull = self.updateMatrixFull; // TODO : remove me -- for testing only
	  controller.showAlert = libs.Utils.showAlert; // TODO : remove me -- for testing only
	  controller.setColumns = function(c) { $(self.grid.setColumns(c)); } // TODO : remove me -- for testing only
	  controller.getColumns = self.getColumns; // TODO : remove me -- for testing only
	  controller.getFilteredColumns = self.getFilteredColumns; // TODO : remove me -- for testing only

      self.controller = controller;
	  return(this);
	}
	return new AnaMat(inputDef, configuration).controller;
}
