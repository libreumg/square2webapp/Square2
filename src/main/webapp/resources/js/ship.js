ship = {
	showModal : function showModal(id) {
		$("[id = '" + id + "']").removeClass("fade").show();
	},
	hideModal : function hideModal(id) {
		$("[id = '" + id + "']").addClass("fade").hide();
	},
	toggle : function toggle(id) {
		$("[id = '" + id + "']").toggle();
	},
	toggleCss : function(id, oldClass, newClass) {
		$("[id = '" + id + "']").removeClass(oldClass).addClass(newClass);
	},
	/**
	 * register a toggle button event on activationElement that toggles
	 * toggableElement
	 * 
	 * @param activationElement
	 *            element that activates toggle event
	 * @param toggableElement
	 *            element to be toggled; hidden on startup
	 */
	registerToggle : function registerToggle(activationElement, toggableElement) {
		$(document).ready(function() {
			// Prevent multiple registering
			$("[id='" + activationElement + "']").off("click");

			$("[id='" + toggableElement + "']").css({
				'display' : 'none'
			});
			$("[id='" + activationElement + "']").css({
				'cursor' : 'pointer'
			});
			$("[id='" + activationElement + "']").click(function() {
				$("[id='" + toggableElement + "']").toggle(500);
			});
		});
	},

	doShow : function doShow(id) {
		$("[id='" + id + "']").show(500);
	},
	
	processingCloseable : function(zin) {
		zin = zin || 1000;
		$("body").append("<div id=\"divcloseable\" class=\"loading\" style=\"z-index: " + zin + " !important\"><button onclick=\"$('#divcloseable').remove();\">" + 
		                 "<i class=\"fa fa-spinner fa-pulse\"></i></button></div>");
	},
	
	processing : function(zin) {
	  zin = zin || 1000;
	  $("body").append("<div id=\"ajaxrequestopen\" class=\"loading\" style=\"z-index: " + zin + " !important\"><i class=\"fa fa-spinner fa-pulse\"></i></div>");
	},
	
	processed : function() {
	  $("#ajaxrequestopen").remove();
	}
	
};

shipbootstrap = {
	infoInDiv : function infoInDiv(divId, content) {
		detailDiv = '<div class="alert alert-info fadein ">';
		detailDiv += '<button class="close" type="button" data-dismiss="alert">×</button>';
		detailDiv += '<span class="bficon bficon-info"></span>'
		detailDiv += content;
		detailDiv += "</div>";
		$('[id="' + divId + '"]').empty().append(detailDiv);
	},

	errorInDiv : function errorInDiv(id) {
		errorDiv = '<div class="alert alert-danger fadein">';
		errorDiv += '<button class="close" type="button" data-dismiss="alert">×</button>';
		errorDiv += '<span class="bficon bficon-error"></span>';
		errorDiv += "an error has occurred";
		errorDiv += '</div>';
		$('[id="' + id + '"]').empty().append(errorDiv);
	}
};

shipprimefaces = {
	markAsDirty : function markAsDirty(domelement) {
		$(domelement).addClass("dirty");
	},

	markAsWarning : function markAsWarning(domelementid) {
		$("[id='" + domelementid + "']").addClass("attention");
	}
};
