# Project Square²
* Project master: @henkej
* main repository: gitlab, since May 2017

# Build
* Buildsystem: gradle
* Adjust the build.gradle to use your own repository that keeps the persistence libraries of jooqFaces and squareJooq
* build with `gradle clean build`

# Installation
Copy the Square2.war file to a tomcat (&lt;10) and wait until tomcat has redeployed it.

# Software architecture
This is a java server faces web application.

## Common inner architecture
Following the mvc model, Square² separates between view (the jsf view or xhtml files), the model (a set of managed beans, we call them Model classes) and control (a set of managed beans, we call them Controller classes). For an easier assignment, all Controller classes are found in `square2.modules.control`, as all models are found in `square2.control.model`. The views are completely under `webapp/pages`.

All Controller classes are `Managed Beans` and `Request Scoped`. All Model classes are `Managed Beans` and `Session Scoped`. This way, the `facesContext` is available in the Controller classes by the `ManagedPropery` facesContext and the corresponding setter. As the controllers are the only interface for the views on actions, the facesContext is given to the sessionscoped Models on function calls. Doing so, one does not need to use the static FacesContext.getCurrentInstance() method that makes unit tests ugly.

Database access to the used postgresql database is done by `Gateways`, found under `square2.db.control`. These Gateways extend the abstract `JooqGateway` that knows the entry point `getJooq()` for the DSL context. From this point, sql statements can be build using the `jooq` library.

## Library usages
### myfaces
This web application uses the myfaces implementation, as it has been faster in comparison to mojarra on setup time.

### primefaces
For a user friendly look and feel, primefaces has been chosen.

### jooq
As a persistence library, jooq was the choice. Other libraries are not close enough to sql in development speed.

### jooqFaces
Using a web application, jooqFaces opens a database connection in the render response phase of jsf and closes this one in the restore view phase. As this web application is like a frontend to the database, mostly every page request contains a database query, and therefore, this library makes sense here.

### squareJooq
All database objects can be created as a class that can be used by jooq. This library contains all the tables that we need for database operations.

### RServ
With this library, a connection to the R server can be done. The R gateway and all of its content is located at folder `square2.r`.
